﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель должности
    /// </summary>
    public class PositionModel
    {
        /// <summary>
        /// Id должности
        /// </summary>
        public int PositionId { get; set; }
        
        /// <summary>
        /// Название должности
        /// </summary>
        public string PositionName { get; set; }
    }
}
