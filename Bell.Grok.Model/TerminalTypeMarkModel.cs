﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель для корреляции типов и моделей терминалов
    /// </summary>
    public class TerminalTypeMarkModel
    {
        /// <summary>
        /// Идентификатор типа терминала, к которому относится модель
        /// </summary>
        public int? TypeId { get; set; }

        /// <summary>
        /// Идентификатор модели терминала
        /// </summary>
        public int ModelId { get; set; } 
    }
}
