﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель типов терминалов
    /// </summary>
    public class TerminalTypeModel
    {
        /// <summary>
        /// Идентификатор типа терминала
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название типа терминала
        /// </summary>
        public string Name { get; set; }
    }
}
