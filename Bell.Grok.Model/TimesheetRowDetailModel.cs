﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки для внутренней таблицы табелей, 
    /// в которой отображаются коды явок и количество
    /// отработанных часов
    /// </summary>
    public class TimesheetRowDetailModel
    {
        /// <summary>
        /// Индекс
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Флаг, относится ли данная строка к первой половине месяца
        /// </summary>
        public bool IsPrepayment { get; set; }

        /// <summary>
        /// Флаг, показывающий, содержится ли в этой строке общее количество часов 
        /// или же количество ночных часов
        /// </summary>
        public bool? IsTotalHours { get; set; }

        /// <summary>
        /// Идентификатор табеля сотрудника, который редактируется
        /// </summary>
        public int TimesheetDetailId { get; set; }

        /// <summary>
        /// Коды явок или количество отработанных часов (всех или ночных).
        /// Строки имеют разную структуру, поэтому в данном свойстве могут быть 
        /// столь разные данные: коды или количество часов
        /// </summary>
        public List<float> Days { get; set; }

        /// <summary>
        /// Для строк с количеством часов - количество сверхурочных часов
        /// для каждого дня
        /// </summary>
        public List<float> Overtimes { get; set; }        

        /// <summary>
        /// Флаг для строк, где хранятся часы, можно ли вводить часы для данного кода явки
        /// </summary>
        public List<bool> DoesHoursInputHaveForDay { get; set; }

        /// <summary>
        /// Флаг для строк, где хранятся все часы, можно ли вводить сверхурочные часы для данного кода явки
        /// </summary>
        public List<bool> DoesOvertimeContain { get; set; }

        /// <summary>
        /// Все возможные коды явок (только для строк, содержащих коды в 
        /// списке Days)
        /// </summary>
        public List<PayMarkModel> Codes { get; set; }

        /// <summary>
        /// Количество дней (зависит от половины месяца и самого месяца)
        /// </summary>
        public int DaysCount { get; set; }
    }
}
