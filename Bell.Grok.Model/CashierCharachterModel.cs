﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель ветки-первой буквы фамилии кассиров в дереве направлений
    /// </summary>
    public class CashierCharachterModel
    {
        /// <summary>
        /// Название ветки (первая буква фамилии кассира)
        /// </summary>
        public string ObjectName { get; set; }

        /// <summary>
        /// Идентификатор элемента дерева
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// Единый идентификатор узла в дереве
        /// </summary>
        public string UnifiedTreeId { get; set; }

        /// <summary>
        /// Признак, содержит ли узел вложенные элементы
        /// </summary>
        public bool HasChildren { get; set; }
    }
}
