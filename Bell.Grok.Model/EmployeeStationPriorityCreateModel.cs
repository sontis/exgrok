﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель для создания приоритета станции для кассира
    /// </summary>
    public class EmployeeStationPriorityCreateModel
    {
        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int StationId { get; set; }

        /// <summary>
        /// Идентификатор кассира
        /// </summary>
        public int EmployeeId { get; set; }

        /// <summary>
        /// Время кассира в пути из дома на работу, единица измерения неизвестна
        /// </summary>
        public byte TransferTime { get; set; }  
    }
}
