﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель представления смены пароля
    /// </summary>
    public class ChangePasswordModel : IValidatableObject
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }
        
        /// <summary>
        /// Пароль
        /// </summary>
        public virtual string Password { get; set; }
        
        /// <summary>
        /// Подтверждение пароля
        /// </summary>
        public virtual string PasswordConfirmation { get; set; }

        /// <summary>
        /// Метод IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            if (!Password.Equals(PasswordConfirmation))
            {
                yield return new ValidationResult("Пароли не совпадают");
            }
        }
    }
}