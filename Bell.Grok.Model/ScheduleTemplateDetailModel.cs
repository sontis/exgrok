﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель колонки таблицы excel-документа, содержащего информацию
    /// о режиме графика кассиров
    /// </summary>
    public class ScheduleTemplateDetailModel
    {
        /// <summary>
        /// Дата
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Год
        /// </summary>
        public short Year { get; set; }

        /// <summary>
        /// Месяц
        /// </summary>
        public byte Month { get; set; }

        /// <summary>
        /// Количество рабочих часов
        /// </summary>
        public float? DayWorkingHours { get; set; }

        /// <summary>
        /// Количество ночных рабочих часов
        /// </summary>
        public float? NightWorkingHours { get; set; }

        /// <summary>
        /// Тип дня (будний, праздник, выходной)
        /// </summary>
        public int DayType { get; set; }

        /// <summary>
        /// Флаг разделения рабочего дня
        /// </summary>
        public bool IsSplitted { get; set; }
    }
}
