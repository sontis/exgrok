﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель сектора
    /// </summary>
    public class SectorModel
    {
        /// <summary>
        /// Id сектора
        /// </summary>
        public int SectorId { get; set; }
        
        /// <summary>
        /// Номер сектора
        /// </summary>
        public int SectorNumber { get; set; }
        
        /// <summary>
        /// Id направления
        /// </summary>
        public int DirectionId { get; set; }
        
        /// <summary>
        /// Название сектора
        /// </summary>
        public string SectorName { get; set; }

        /// <summary>
        /// Название направления, к которому принадлежит участок
        /// </summary>
        public string DirectionName { get; set; }
    }
}
