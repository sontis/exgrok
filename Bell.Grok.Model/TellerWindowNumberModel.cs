﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель для фильтрации кассовых окон по номеру
    /// </summary>
    public class TellerWindowNumberModel
    {
        /// <summary>
        /// Идентификатор кассового окна
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Номер кассового окна
        /// </summary>
        public int Number { get; set; }
    }
}
