﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы планов по месячным выручкам станции
    /// </summary>
    public class ProceedsPlanGridModel
    {
        /// <summary>
        /// Идентификатор месячного плана 
        /// </summary>
        public int StationMonthProceedsId { get; set; }

        /// <summary>
        /// Идентификатор станции, для которой составлен месячный план
        /// </summary>
        public int SectorStationId { get; set; }

        /// <summary>
        /// Дата в строчном виде
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Год
        /// </summary>
        public int Year { get; set; }
        
        /// <summary>
        /// Месяц
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Показатель общего плана за месяц
        /// </summary>
        public decimal CommonPlan { get; set; }

        /// <summary>
        /// Показатель платного плана за месяц
        /// </summary>
        public decimal PaidPlan { get; set; }
    }
}
