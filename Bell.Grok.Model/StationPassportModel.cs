﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель паспорта станции
    /// </summary>
    public class StationPassportModel
    {
        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int StationId { get; set; }

        /// <summary>
        /// Номер станции
        /// </summary>
        [Display(Name = "Код станции")]        
        public int StationNumber { get; set; }

        /// <summary>
        /// Название станции
        /// </summary>
        [Display(Name = "Наименование")]
        public string StationName { get; set; }

        /// <summary>
        /// Название направления, на котором находится станция
        /// </summary>
        [Display(Name = "Направление/Рег. центр")]
        public string DirectionName { get; set; }

        /// <summary>
        /// Название участка, на котором расположена станция
        /// </summary>
        [Display(Name = "Участок производства")]
        public string SectorName { get; set; }

        /// <summary>
        /// Выделенные номера окон (для фильтра с возможностью многократного выбора на вкладке "Кассовые окна")
        /// </summary>
        public List<TellerWindowNumberModel> SelectedWindowTellerNumbers { get; set; }

        /// <summary>
        /// Выделенные номера окон (для фильтра с возможностью многократного выбора на вкладке "ККТ")
        /// </summary>
        public List<TellerWindowNumberModel> SelectedWindowTellerNumbersInTerminalTab { get; set; }        

        /// <summary>
        /// Выделенные номера терминалов (для фильтра с возможностью многократного выбора)
        /// </summary>
        public List<TerminalNumberModel> SelectedTerminalNumbers { get; set; }

        /// <summary>
        /// Список номеров терминалов
        /// </summary>
        public List<TerminalNumberModel> TerminalNumbers { get; set; } 

        /// <summary>
        /// Выделенные типы терминалов (для фильтра с возможностью многократного выбора)
        /// </summary>
        public List<TerminalTypeModel> SelectedTerminalTypes { get; set; }

        /// <summary>
        /// Список типов терминалов
        /// </summary>
        public List<TerminalTypeModel> TerminalTypes { get; set; }

        /// <summary>
        /// Дата начала периода (для фильтра на вкладке "Кассовые окна")
        /// </summary>
        public virtual string TellerWindowBeginExploitationPeriod { get; set; }

        /// <summary>
        /// Дата окончания периода (для фильтра на вкладке "Кассовые окна")
        /// </summary>
        public virtual string TellerWindowEndExploitationPeriod { get; set; }

        /// <summary>
        /// Дата начала периода (для фильтра на вкладке "ККТ")
        /// </summary>        
        public virtual string TerminalBeginExploitationPeriod { get; set; }

        /// <summary>
        /// Дата окончания периода (для фильтра на вкладке ККТ)
        /// </summary>
        public virtual string TerminalEndExploitationPeriod { get; set; }

        /// <summary>
        /// В случае установки значения будут показываться только те терминалы, 
        /// которые действуют в данное время
        /// </summary>
        public bool IsShowCurrent { get; set; }
    }
}
