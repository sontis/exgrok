﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель календарного дня
    /// </summary>
    public class CalendarModel
    {
        /// <summary>
        /// Дата
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Номер дня недели
        /// </summary>
        public int WeekDayNumber { get; set; }

        /// <summary>
        /// Название дня недели
        /// </summary>
        public string WeekDayName { get; set; }

        /// <summary>
        /// Является ли день праздничным
        /// </summary>
        public bool IsHoliday { get; set; }

        /// <summary>
        /// Является ли день выходным
        /// </summary>
        public bool IsWeekend { get; set; }
    }
}
