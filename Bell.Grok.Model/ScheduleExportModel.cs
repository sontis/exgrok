﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// модель данных для экспорта в Excel Планового графика кассиров билетных
    /// </summary>
    public class ScheduleExportModel
    {
        public ScheduleExportModel()
        {
            Rows = new List<ScheduleRowModel>();
            TwsRows = new List<TellerWindowScheduleRowModel>();
            ReconcileYear = DateTime.Now.Year.ToString();
        }

        /// <summary>
        /// Имя участка
        /// </summary>
        public string SectorName { get; set; }
        /// <summary>
        /// ФИО профгруппорга
        /// </summary>
        public string GroupOrganizerName { get; set; }
        /// <summary>
        /// ФИО начальника участка
        /// </summary>
        public string ChiefName { get; set; }
        /// <summary>
        /// Название станции
        /// </summary>
        public string StationName { get; set; }
        /// <summary>
        /// Название месяца
        /// </summary>
        public string Month { get; set; }
        /// <summary>
        /// Год
        /// </summary>
        public string Year { get; set; }
        /// <summary>
        /// список данных по индивидуальным графикам кассиров
        /// </summary>
        public List<ScheduleRowModel> Rows { get; set; }
        /// <summary>
        /// Год согласования
        /// </summary>
        public string ReconcileYear { get; set; }
        /// <summary>
        /// Дата согласования
        /// </summary>
        public string ReconcileDate{ get; set; }
        /// <summary>
        /// список режимов работы работников по КО
        /// </summary>
        public List<TellerWindowScheduleRowModel> TwsRows { get; set; }
        /// <summary>
        /// Исполнитель
        /// </summary>
        public string Implementer { get; set; }

    }
}
