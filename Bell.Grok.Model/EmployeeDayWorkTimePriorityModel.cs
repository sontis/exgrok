﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель временного интервала приоритета станции
    /// </summary>
    public class EmployeeDayWorkTimePriorityModel
    {        
        /// <summary>
        /// Идентификатор дня недели
        /// </summary>
        public int WeekDayId { get; set; }
        /// <summary>
        /// Начальная граница интервала
        /// </summary>
        public TimeSpan BeginTime { get; set; }
        /// <summary>
        /// Конечная граница интервала
        /// </summary>
        public TimeSpan EndTime { get; set; }
    }
}
