﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель сезона работы кассовых окон
    /// </summary>
    public class SeasonModel
    {
        /// <summary>
        /// Идентификатор сезона в базе
        /// </summary>
        public int SeasonId { get; set; }
        /// <summary>
        /// Название сезона
        /// </summary>
        public string SeasonName { get; set; }
    }
}
