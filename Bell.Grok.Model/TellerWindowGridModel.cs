﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы, содержащей информацию о кассовых окнах
    /// </summary>
    public class TellerWindowGridModel
    {
        /// <summary>
        /// Идентификатор кассового окна
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Номер кассового окна
        /// </summary>
        public int? Number { get; set; }

        /// <summary>
        /// Место расположения кассового окна
        /// </summary>
        public string Placement { get; set; }

        /// <summary>
        /// Профиль кассового окна
        /// </summary>
        public string ProfileName { get; set; }

        /// <summary>
        /// Тип терминала, связанного с кассовым окном
        /// </summary>
        public string TerminalType { get; set; }

        /// <summary>
        /// Номер терминала, связанного с кассовым окном
        /// </summary>
        public string TerminalNumber { get; set; }

        public int Shiftness { get; set; }
        public string WorkDays { get; set; }
        public string WorkTime { get; set; }
        public string Breaks { get; set; }
        public string BreakTime { get; set; }

        /// <summary>
        /// Специализация кассового окна
        /// </summary>
        public string Specialization { get; set; }

        /// <summary>
        /// Статус кассового окна
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Способ оплаты, который предоставляет кассовое окно
        /// </summary>
        public string Payment { get; set; }

        /// <summary>
        /// Дата установки статуса/специализации/способа оплаты
        /// в кассовом окне
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Дата отмены статуса/специализации/способа оплаты
        /// в кассовом окне
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Описание кассового окна
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Url страницы редактирования кассового окна
        /// </summary>
        public string EditUrl { get; set; }
    }
}
