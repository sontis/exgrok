﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы, содержащей информацию о терминалах
    /// </summary>
    public class TerminalGridModel
    {
        /// <summary>
        /// Идентификатор терминала
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Номер терминала
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Тип терминала
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Модель терминала
        /// </summary>
        public string ModelName { get; set; }

        /// <summary>
        /// Агент, через которого работает терминал
        /// </summary>
        public string AgentName { get; set; }

        /// <summary>
        /// Место расположения терминала
        /// </summary>
        public string Placement { get; set; }

        /// <summary>
        /// Профиль терминала
        /// </summary>
        public string ProfileName { get; set; }

        /// <summary>
        /// Назначение терминала
        /// </summary>
        public string PurposeName { get; set; }

        /// <summary>
        /// Дата ввода
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Дата вывода
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Статус терминала
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// Url страницы редактирования терминала
        /// </summary>
        public string EditUrl { get; set; }

        /// <summary>
        /// Идентификатор привязки терминала к станции
        /// </summary>
        public int SectorStationTerminalId { get; set; }
    }
}
