﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель кодов начислений и неявок
    /// </summary>
    public class PayMarkModel
    {
        /// <summary>
        /// Числовой код начисления или неявки
        /// </summary>
        public int PayCode { get; set; }

        /// <summary>
        /// Буквенный код начисления или неявки
        /// </summary>
        public string PayMark { get; set; }

        /// <summary>
        /// Флаг, показывающий, используется ли код для начислений или явок
        /// </summary>
        public bool IsPresence { get; set; }

        /// <summary>
        /// Флаг, показывающий, проставляются ли для данного кода часы
        /// </summary>
        public bool IsAccount { get; set; }

        /// <summary>
        /// Флаг, показывающий, проставляются ли для данного кода сверхурочные часы
        /// </summary>
        public bool HasOvertime { get; set; }
    }
}
