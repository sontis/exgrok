﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы, содержащей информацию о временных интервалах
    /// приоритета станции
    /// </summary>
    public class EmployeeWorkTimePriorityGridModel
    {
        /// <summary>
        /// Идентификатор временного интервала приоритета станции
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Название дня недели
        /// </summary>
        public string DayName { get; set; }
        /// <summary>
        /// Время работы ('начало'-'конец')
        /// </summary>
        public string WorkTime { get; set; }
    }
}
