﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель представления редактирования пользователя
    /// </summary>
    public class UserModel 
    {        
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>        
        public virtual string EmployeeFirstName { get; set; }

        /// <summary>
        /// Отчество пользователя
        /// </summary>       
        public virtual string EmployeeMiddleName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>       
        public virtual string EmployeeLastName { get; set; }

        /// <summary>
        /// Логин пользователя
        /// </summary>        
        public virtual string Login { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>        
        public virtual string Password { get; set; }

        /// <summary>
        /// Подтверждение пароля пользователя
        /// </summary>
        public virtual string PasswordConfirmation { get; set; }

        /// <summary>
        /// Примечание пользователя
        /// </summary>        
        public virtual string Description { get; set; }

        /// <summary>
        /// Доступны ли для редактирования имя, отчество и фамилия.
        /// Если пользователь создан из справочника сотрудников, то именные поля только для чтения
        /// </summary>
        public bool AreNameFieldsReadonly { get; set; }

        /// <summary>
        /// Список моделей роли пользователя (назначенные на пользователя роли)
        /// </summary>
        public virtual UserRoleModel UserRole { get; set; }        
    }    
}