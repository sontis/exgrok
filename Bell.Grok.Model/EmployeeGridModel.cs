﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bell.Grok.Model
{    
    /// <summary>
    /// Модель строки таблицы, содержащей информацию о сотрудниках
    /// </summary>
    public class EmployeeGridModel
    {
        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public int EmployeeId { get; set; }
        /// <summary>
        /// Табельный номер сотрудника
        /// </summary>
        public string EmployeeNumber { get; set; }
        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string EmployeeFirstName { get; set; }
        /// <summary>
        /// Отчество сотрудника
        /// </summary>
        public string EmployeeMiddleName { get; set; }
        /// <summary>
        /// Фамилия сотрудника
        /// </summary>
        public string EmployeeLastName { get; set; }
        /// <summary>
        /// Название участка, к которому относится сотрудник
        /// </summary>
        public string Sector { get; set; }
        /// <summary>
        /// Название должности, которую занимает сотрудник
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// {Производственный участок, должность} из таблицы employee
        /// </summary>
        public string Description { get; set; }
    }
}