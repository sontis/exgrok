﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    public class EmployeeShiftModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
