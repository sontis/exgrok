﻿namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель работы кассового окна
    /// </summary>
    public class TellerWindowWorkTimeModel
    {
        /// <summary>
        /// Идентификатор режима
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Начало работы
        /// </summary>
        public string BeginTime { get; set; }

        /// <summary>
        /// Конец работы 
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// Идентификатор дня недели 
        /// </summary>
        public int WeekDayId { get; set; }
    }
}
