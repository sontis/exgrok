﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель метаданных файла, прикрепляемого к табелю
    /// </summary>
    public class TimesheetFileInformationModel
    {
        /// <summary>
        /// Идентификатор файла
        /// </summary>
        public int fileId { get; set;}

        /// <summary>
        /// Аннотация к файлу, данная пользователем
        /// </summary>
        public string name { get; set; }
    }
}
