﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    public class EmployeeShiftScheduleModel
    {
        public int Id { get; set; }
        public int WeekDay { get; set; }
        public string WeekDayName { get; set; }
        public string BeginWorkTime { get; set; }
        public string EndWorkTime { get; set; }
        public string BeginBreakTime { get; set; }
        public string EndBreakTime { get; set; }
        public string WorkTime { get; set; }
        public string BreakTime { get; set; }
    }
}
