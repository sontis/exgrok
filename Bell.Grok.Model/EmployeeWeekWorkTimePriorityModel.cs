﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель всех временных интервалов приоритета станции
    /// </summary>
    public class EmployeeWeekWorkTimePriorityModel
    {
        /// <summary>
        /// Идентификатор приоритета станции
        /// </summary>
        public int StationPriorityId { get; set; }
        /// <summary>
        /// Список временных интервалов отдельных дней
        /// </summary>
        public List<EmployeeDayWorkTimePriorityModel> EmployeeDayWorkTimePriorities { get; set; }
    }
}
