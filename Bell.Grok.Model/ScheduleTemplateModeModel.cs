﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель режима графика кассиров
    /// </summary>
    public class ScheduleTemplateModeModel
    {
        /// <summary>
        /// Название режима графика кассиров
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Список содержимого колонок таблицы excel-документа, содержащего информацию о режиме
        /// </summary>
        public List<ScheduleTemplateDetailModel> ScheduleTemplateDetails { get; set; }
    }
}
