﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель типа часа в срезе расписаний работы кассовых окон
    /// </summary>
    public class HourTypeModel
    {
        /// <summary>
        /// Идентификатор типа часа в базе
        /// </summary>
        public int HourTypeId { get; set; }        

        /// <summary>
        /// Код (буквенный) типа часа
        /// </summary>
        public string HourTypeCode { get; set; }
    }
}
