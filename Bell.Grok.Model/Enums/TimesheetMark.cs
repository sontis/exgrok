﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Перечисление отметок о явках и неявках
    /// </summary>
    public enum TimesheetMark
    {
        Undefined = 0,
        Presence = 1,
        NightsHours = 2,
        Holidays = 3,
        Overtime = 4,
        Watch = 5,        	
        BusinessTrip = 6,
        Training = 7,
        TrainingInOtherAreas = 8,
        Vacation = 9,
        AdditionalVacation = 10,
        PaidEducationalVacation = 11,
        ShorterWorkingDayForStudents = 12,
        NotPaidEducationalVacation = 13,
        MaternityVacation = 14,
        CareForChildVacation = 15,
        WithoutPayToResolveEmployerVacation = 16,
        WithoutPayUnderTheLaw = 17,
        AdditionalAnnualVacationWithoutPay = 18,
        Sick = 19,
        UnpaidSick = 20,
        ShorterWorkingDay = 21,
        PeriodOfEnforcedDismissalIdleness = 22,
        StateOrPublicDutiesAbsenteeism = 23,
        Hooky = 24,
        PartTimeWorkDuration = 25,
        Weekend = 26,
        AdditionalPaidWeekend = 27,
        AdditionalNotPaidWeekend = 28,
        Strike = 29,
        UnknownReasonsHooky = 30,
        EmployerFaultDowntime = 31,
        NotEmployerFaultDowntime = 32,
        Downtime = 33,
        PaidWorkDischarge = 34,
        NotPaidWorkDischarge = 35,
        DelayPaymentsDowntime = 36,
        EveningHours = 37,
        PresenceWithOvertime = 38
    }
    
    public static class TimesheetMarkExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление отметки о явке или неявке
        /// </summary>
        /// <param name="timesheetMarkExtensions">Отметка о явке или неявке</param>
        /// <returns>Текстовое представление отметки о явке или неявке</returns>
        public static string ToText(this TimesheetMark timesheetMarkExtensions)
        {
            switch (timesheetMarkExtensions)
            {
                case TimesheetMark.Undefined:
                    return string.Empty;
                case TimesheetMark.Presence:
                    return "Явка";
                case TimesheetMark.NightsHours:
                    return "Ночные часы";
                case TimesheetMark.Holidays:
                    return "Праздники";
                case TimesheetMark.Overtime:
                    return "Сверхурочно";
                case TimesheetMark.Watch:
                    return "Вахта";
                case TimesheetMark.BusinessTrip:
                    return "Командировка";
                case TimesheetMark.Training:
                    return "Повышение квалификации";
                case TimesheetMark.TrainingInOtherAreas:
                    return "Повышение квалификации в другой местности";
                case TimesheetMark.Vacation:
                    return "Отпуск";
                case TimesheetMark.AdditionalVacation:
                    return "Дополнительный отпуск";
                case TimesheetMark.PaidEducationalVacation:
                    return "Отпуск дополнительный (оплачиваемый учебный)";
                case TimesheetMark.ShorterWorkingDayForStudents:
                    return "Сокращенная продолжительность рабочего для обучающихся";
                case TimesheetMark.NotPaidEducationalVacation:
                    return "Отпуск дополнительный (неоплачиваемый учебный)";
                case TimesheetMark.MaternityVacation:
                    return "Отпуск по беременности и родам";
                case TimesheetMark.CareForChildVacation:
                    return "Отпуск по уходу за ребенком";
                case TimesheetMark.WithoutPayToResolveEmployerVacation:
                    return "Отпуск без сохранения заработной платы по разрешению работодателя";
                case TimesheetMark.WithoutPayUnderTheLaw:
                    return "Отпуск без сохранения заработной платы по законодательству";
                case TimesheetMark.AdditionalAnnualVacationWithoutPay:
                    return "Ежегодный дополнительный отпуск без сохранения заработной платы";
                case TimesheetMark.Sick:
                    return "Больничный";
                case TimesheetMark.UnpaidSick:
                    return "Больничный неоплачиваемый";
                case TimesheetMark.ShorterWorkingDay:
                    return "Сокращенная продолжительность рабочего времени";
                case TimesheetMark.PeriodOfEnforcedDismissalIdleness:
                    return "Время вынужденного прогула в случае признания увольнения";
                case TimesheetMark.StateOrPublicDutiesAbsenteeism:
                    return "Невыходы на время исполнения государственных или общественных обязанностей";
                case TimesheetMark.Hooky:
                    return "Прогул";
                case TimesheetMark.PartTimeWorkDuration:
                    return "Продолжительность работы в режиме неполного рабочего времени";
                case TimesheetMark.Weekend:
                    return "Выходные дни (еженедельный отпуск) и нерабочие праздничные";
                case TimesheetMark.AdditionalPaidWeekend:
                    return "Дополнительные выходные дни (оплачиваемые)";
                case TimesheetMark.AdditionalNotPaidWeekend:
                    return "Дополнительные выходные дни (без сохранения заработной платы)";
                case TimesheetMark.Strike:
                    return "Забастовка";
                case TimesheetMark.UnknownReasonsHooky:
                    return "Неявки по невыясненным причинам";
                case TimesheetMark.EmployerFaultDowntime:
                    return "Время простоя по вине работодателя";
                case TimesheetMark.NotEmployerFaultDowntime:
                    return "Время простоя по причинам, не зависящим от работодателя";
                case TimesheetMark.Downtime:
                    return "Простой";
                case TimesheetMark.PaidWorkDischarge:
                    return "Отстранение от работы (недопущение к работе) с оплатой";
                case TimesheetMark.NotPaidWorkDischarge:
                    return "Отстранение от работы без начисления заработной платы";
                case TimesheetMark.DelayPaymentsDowntime:
                    return "Время приостановки работы в случае задержки выплат";
                case TimesheetMark.EveningHours:
                    return "Вечерние часы";
                case TimesheetMark.PresenceWithOvertime:
                    return "Явка со сверхурочными";
                default:
                    return timesheetMarkExtensions.ToString();
            }
        }

        /// <summary>
        /// Метод, возвращающий буквенный код отметки о явке или неявке
        /// </summary>
        /// <param name="timesheetMarkExtensions">Отметка о явке или неявке</param>
        /// <returns>Буквенный код отметки о явке или неявке</returns>        
        public static string ToTextCode(this TimesheetMark timesheetMarkExtensions)
        {
            switch (timesheetMarkExtensions)
            {
                case TimesheetMark.Undefined:
                    return "-";
                case TimesheetMark.Presence:
                    return "Я";
                case TimesheetMark.NightsHours:
                    return "Н";
                case TimesheetMark.Holidays:
                    return "РВ";
                case TimesheetMark.Overtime:
                    return "С";
                case TimesheetMark.Watch:
                    return "ВМ";
                case TimesheetMark.BusinessTrip:
                    return "К";
                case TimesheetMark.Training:
                    return "ПК";
                case TimesheetMark.TrainingInOtherAreas:
                    return "ПМ";
                case TimesheetMark.Vacation:
                    return "ОТ";
                case TimesheetMark.AdditionalVacation:
                    return "ОД";
                case TimesheetMark.PaidEducationalVacation:
                    return "У";
                case TimesheetMark.ShorterWorkingDayForStudents:
                    return "УВ";
                case TimesheetMark.NotPaidEducationalVacation:
                    return "УД";
                case TimesheetMark.MaternityVacation:
                    return "Р";
                case TimesheetMark.CareForChildVacation:
                    return "ОЖ";
                case TimesheetMark.WithoutPayToResolveEmployerVacation:
                    return "ДО";
                case TimesheetMark.WithoutPayUnderTheLaw:
                    return "ОЗ";
                case TimesheetMark.AdditionalAnnualVacationWithoutPay:
                    return "ДБ";
                case TimesheetMark.Sick:
                    return "Б";
                case TimesheetMark.UnpaidSick:
                    return "Т";
                case TimesheetMark.ShorterWorkingDay:
                    return "ЛЧ";
                case TimesheetMark.PeriodOfEnforcedDismissalIdleness:
                    return "ПВ";
                case TimesheetMark.StateOrPublicDutiesAbsenteeism:
                    return "Г";
                case TimesheetMark.Hooky:
                    return "ПР";
                case TimesheetMark.PartTimeWorkDuration:
                    return "НС";
                case TimesheetMark.Weekend:
                    return "В";
                case TimesheetMark.AdditionalPaidWeekend:
                    return "ОВ";
                case TimesheetMark.AdditionalNotPaidWeekend:
                    return "НВ";
                case TimesheetMark.Strike:
                    return "ЗБ";
                case TimesheetMark.UnknownReasonsHooky:
                    return "НН";
                case TimesheetMark.EmployerFaultDowntime:
                    return "РП";
                case TimesheetMark.NotEmployerFaultDowntime:
                    return "НП";
                case TimesheetMark.Downtime:
                    return "ВП";
                case TimesheetMark.PaidWorkDischarge:
                    return "НО";
                case TimesheetMark.NotPaidWorkDischarge:
                    return "НБ";
                case TimesheetMark.DelayPaymentsDowntime:
                    return "НЗ";
                case TimesheetMark.EveningHours:
                    return "ВЧ";
                case TimesheetMark.PresenceWithOvertime:
                    return "Я/c";
                default:
                    return timesheetMarkExtensions.ToString();
            }
        }
    }
}
