﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Перечисление типов часов в срезах работы кассовых окон
    /// </summary>
    public enum TellerWindowScheduleHourTypes
    {
        Rest = 1,
        Work = 2,
        DinnerBreak = 3,
        TechnologicalBreak = 4,        
    }

    public static class TellerWindowScheduleHoursExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление типов часов
        /// в срезах работы кассовых окон
        /// </summary>
        /// <param name="tellerWindowScheduleHourTypes">Тип часа в срезах работы кассовых окон</param>
        /// <returns>Текстовое представление типов часов
        ///          в срезах работы кассовых окон</returns>
        public static string ToText(this TellerWindowScheduleHourTypes tellerWindowScheduleHourTypes)
        {
            switch (tellerWindowScheduleHourTypes)
            {
                case TellerWindowScheduleHourTypes.Rest:
                    return "Отдых";
                case TellerWindowScheduleHourTypes.Work:
                    return "Работа";
                case TellerWindowScheduleHourTypes.TechnologicalBreak:
                    return "Технологический перерыв";
                case TellerWindowScheduleHourTypes.DinnerBreak:
                    return "Обеденный перерыв";
                default:
                    return tellerWindowScheduleHourTypes.ToString();
            }
        }

        /// <summary>
        /// Метод, возвращающий буквенный типов часов
        /// в срезах работы кассовых окон
        /// </summary>
        /// <param name="tellerWindowScheduleHourTypes">Тип часа в срезах работы кассовых окон</param>
        /// <returns>Буквенный код типов часов
        ///          в срезах работы кассовых окон</returns>
        public static string ToTextCode(this TellerWindowScheduleHourTypes tellerWindowScheduleHourTypes)
        {
            switch (tellerWindowScheduleHourTypes)
            {
                case TellerWindowScheduleHourTypes.Rest:
                    return string.Empty;
                case TellerWindowScheduleHourTypes.Work:
                    return string.Empty;
                case TellerWindowScheduleHourTypes.TechnologicalBreak:
                    return "Т";
                case TellerWindowScheduleHourTypes.DinnerBreak:
                    return "О";
                default:
                    return tellerWindowScheduleHourTypes.ToString();
            }
        }
    }
}
