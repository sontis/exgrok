﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model.Enums
{
    /// <summary>
    /// Перечисление сезонов расписаний работы кассовых окон
    /// </summary>
    public enum TellerWindowScheduleSeasons
    {
        Summer = 1,
        Winter = 2
    }
    
    public static class TellerWindowScheduleSeasonsExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление сезонов
        /// расписаний работы кассовых окон
        /// </summary>
        /// <param name="tellerWindowScheduleSeason"></param>
        /// <returns></returns>
        public static string ToText(this TellerWindowScheduleSeasons tellerWindowScheduleSeason)
        {
            switch (tellerWindowScheduleSeason)
            {
                case TellerWindowScheduleSeasons.Summer:
                    return "Летний";
                case TellerWindowScheduleSeasons.Winter:
                    return "Зимний";
                default:
                    return tellerWindowScheduleSeason.ToString();
            }
        }
    }
}
