﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Перечисление ролей
    /// </summary>
    public enum Roles
    {
        DirectionHead = 1,
        SectorHead = 2,
        SeniorTicketCashier = 3,
        Technologist = 4,
        OOPEmployee = 5,
        COEmployee = 6,
        Administrator = 7,
        OTZPEmployee = 8,
        ProfessionalGroupOrganizer = 9
    }

    /// <summary>
    /// Текстовое представление перечисления ролей
    /// </summary>
    public static class RolesExtensions
    {
        public static string ToText(this Roles role)
        {
            switch (role)
            {
                case Roles.DirectionHead:
                    return @"Руководитель направления\Центра";
                case Roles.SectorHead:
                    return "Начальник участка";
                case Roles.SeniorTicketCashier:
                    return "Старший билетный кассир";
                case Roles.Technologist:
                    return "Технолог";
                case Roles.OOPEmployee:
                    return "Сотрудник ООП";
                case Roles.COEmployee:
                    return "Сотрудник ЦО";
                case Roles.Administrator:
                    return "Администратор";
                case Roles.OTZPEmployee:
                    return "Сотрудник ОТиЗП";
                case Roles.ProfessionalGroupOrganizer:
                    return "Профгруппорг";
                default:
                    return role.ToString();
            }
        }
    }
}
