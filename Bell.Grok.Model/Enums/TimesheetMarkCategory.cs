﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Перечисление категорий сверхурочных
    /// </summary>
    public enum TimesheetMarkCategory
    {
        OneAndHalfOvertime = 1,
        DoubleOvertime = 2
    }

    public static class TimesheetMarkCategoryExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление категорий сверхурочных
        /// </summary>
        /// <param name="timesheetMarkCategory">Категория сверхурочных</param>
        /// <returns>Текстовое представление категории сверхурочных</returns>
        public static string ToText(this TimesheetMarkCategory timesheetMarkCategory)
        {
            switch (timesheetMarkCategory)
            {
                case TimesheetMarkCategory.OneAndHalfOvertime:
                    return "04/1,5р";
                case TimesheetMarkCategory.DoubleOvertime:
                    return "04/2р";
                default:
                    return timesheetMarkCategory.ToString();
            }
        }
    }
}
