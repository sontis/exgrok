﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Копия таблицы DataObject БД.
    /// Перечисление информационных сущностей, над которыми
    /// проводят действия пользователи
    /// </summary>
    public enum DataObjects
    {
        StationSchedule = 1,
        StationStandardLoadTW = 2,
        StationPlannedRevenue = 3,
        StationEmployeeShiftSchedule = 4,
        Timesheet = 5,
        Terminal = 6,
        TellerWindow = 7
    }

    public static class DataObjectsExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления информационных сущностей, 
        /// над которыми проводят действия пользователи
        /// </summary>
        public static string ToText(this DataObjects dataObject)
        {
            switch (dataObject)
            {
                case DataObjects.StationSchedule:
                    return "Плановые графики кассиров билетных";
                case DataObjects.StationStandardLoadTW:
                    return "Нормативы значений нормальной нагрузки на кассу, БПА";
                case DataObjects.StationPlannedRevenue:
                    return "Планы по выручке";
                case DataObjects.StationEmployeeShiftSchedule:
                    return "Режим работы работника";
                case DataObjects.Timesheet:
                    return "Табель учета рабочего времени";
                case DataObjects.Terminal:
                    return "Контрольно-кассовая техника";
                case DataObjects.TellerWindow:
                    return "Кассовые окна";
                default:
                    return dataObject.ToString();
            }
        }
    }
}
