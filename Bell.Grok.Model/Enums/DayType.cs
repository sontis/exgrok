﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Перечисление типов календарных дней
    /// </summary>
    public enum DayType
    {
        WD = 1,
        HD = 2,
        VD = 3,
        VVD = 4
    }

    /// <summary>
    /// Расширение перечисления типов календарных дней
    /// </summary>
    public static class DayTypeExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления типов календарных дней
        /// </summary>
        public static string ToChar(this DayType dayType)
        {
            switch (dayType)
            {                
                case DayType.HD:
                    return "п";
                case DayType.VD:
                    return "в";
                case DayType.VVD:
                    return "в*";
                case DayType.WD:
                    return string.Empty;
                default:
                    return dayType.ToString();
            }
        }

        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления типов календарных дней
        /// </summary>
        public static string DayTypeToString(this DayType dayType)
        {
            switch (dayType)
            {
                case DayType.WD:
                    return "Рабочий день";
                case DayType.HD:
                    return "Праздничный день";
                case DayType.VD:
                    return "Выходной день";
                case DayType.VVD:
                    return "Выходной за работу в который предоставляется дополнительный день отдыха к очередному отпуску";
                default:
                    return dayType.ToString();
            }
        }
    }
}
