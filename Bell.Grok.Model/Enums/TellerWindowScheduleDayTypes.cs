﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model.Enums
{
    public enum TellerWindowScheduleDayTypes
    {
        Weekdays = 1,
        Holidays = 2
    }

    public static class TellerWindowScheduleDayTypesExtensions
    {
        public static string ToText(this TellerWindowScheduleDayTypes tellerWindowScheduleDayType)
        {
            switch (tellerWindowScheduleDayType)
            {
                case TellerWindowScheduleDayTypes.Weekdays:
                    return "Будни";
                case TellerWindowScheduleDayTypes.Holidays:
                    return "Выходные и праздники";
                default:
                    return tellerWindowScheduleDayType.ToString();
            }
        }
    }
}
