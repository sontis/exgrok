﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Копия таблицы RoleObjectRight БД.
    /// Перечисление действий, проводимых над информационными сущностями
    /// </summary>
    public enum UserActions
    {
        Read = 1,
        Add = 2,
        Delete = 3,
        Export = 4,
        ApprovalSubmitting = 5,
        ApprovalCancel = 6,
        ApprovConfirmChief = 7,
        ApprovConfirmTU = 8,
        Update = 9,
        AttachDoc = 12,
        DeleteAttachedDoc = 13,
        ViewAttachedDoc = 14,
        FormTimesheet = 15
    }

    /// <summary>
    /// Метод, возвращающий текстовое представление элементов перечисления действий, 
    /// проводимых над информационными сущностями
    /// </summary>
    public static class UserActionsExtensions
    {
        public static string ToText(this UserActions userAction)
        {
            switch (userAction)
            {
                case UserActions.Read:
                    return "Просмотр";
                case UserActions.Add:
                    return "Добавление";
                case UserActions.Delete:
                    return "Удаление";
                case UserActions.Export:
                    return "Экспорт";
                case UserActions.ApprovalSubmitting:
                    return "Отправка на согласование";
                case UserActions.ApprovalCancel:
                    return "Отмена согласования";
                case UserActions.ApprovConfirmChief:
                    return "Подтверждение согласования начальником участка";
                case UserActions.ApprovConfirmTU:
                    return "Подтверждение согласования профгруппоргом";
                case UserActions.Update:
                    return "Изменение";
                case UserActions.AttachDoc:
                    return "Присоединение документа";
                case UserActions.DeleteAttachedDoc:
                    return "Удаление присоединенного документа";
                case UserActions.ViewAttachedDoc:
                    return "Просмотр присоединенного документа";
                case UserActions.FormTimesheet:
                    return "Первичное формирование табеля";
                default:
                    return userAction.ToString();
            }
        }
    }
}
