﻿using System.Collections.Generic;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель пользовательской роли
    /// </summary>
    public class UserRoleModel
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Идентификатор роли
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// Уровень доступа, соответствующий роли
        /// </summary>
        public int AccessLevelId { get; set; }        

        /// <summary>
        /// Список идентификаторов направлений (используется для ролей с уровнем доступа "Направление")
        /// </summary>
        public IEnumerable<int> DirectionIds { get; set; }

        /// <summary>
        /// Идентификатор участка (используется для ролей с уровнем доступа "Участок производства")
        /// </summary>
        public int? SectorId { get; set; }
        /// <summary>
        /// Идентификатор станции (используется для ролей с уровнем доступа "Станция")
        /// </summary>
        public int? StationId { get; set; }

        /// <summary>
        /// Название роли
        /// </summary>
        public string RoleName { get; set; }               
    }
}
