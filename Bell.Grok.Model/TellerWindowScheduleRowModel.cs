﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель данных отображения расписания работы окон
    /// </summary>
    public class TellerWindowScheduleRowModel
    {
        /// <summary>
        /// Номера касс
        /// </summary>
        public string Numbers { get; set; }

        private readonly string[] _daySchedules;
        /// <summary>
        /// Расписание по дням недели + праздники (0..7)
        /// </summary>
        public string[] DaySchedules
        {
            get { return _daySchedules; }
        }

        #region Расписание по дням недели
        public string Monday { get { return DaySchedules[0]; } }
        public string Tuesday { get { return DaySchedules[1]; } }
        public string Wednesday { get { return DaySchedules[2]; } }
        public string Thursday { get { return DaySchedules[3]; } }
        public string Friday { get { return DaySchedules[4]; } }
        public string Saturday { get { return DaySchedules[5]; } }
        public string Sunday { get { return DaySchedules[6]; } }
        public string Holiday { get { return DaySchedules[7]; } }
        #endregion

        public TellerWindowScheduleRowModel()
        {
            _daySchedules = new string[8];
        }
    }
}
