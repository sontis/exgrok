﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы, содержащей информацию о привязках терминалов к кассовым окнам
    /// </summary>
    public class TellerWindowTerminalGridModel
    {
        /// <summary>
        /// Идентификатор привязки
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Номер кассового окна
        /// </summary>
        public string TellerWindowNumber { get; set; }

        /// <summary>
        /// Дата установки
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Дата снятия
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Url страницы редактирования привякзи
        /// </summary>
        public string EditUrl { get; set; }

        /// <summary>
        /// Номер терминала
        /// </summary>
        public string TerminalNumber { get; set; }
    }
}
