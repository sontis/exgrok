﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель редактирования терминала
    /// </summary>
    public abstract class TerminalModel : IValidatableObject
    {                               
        /// <summary>
        /// Номер
        /// </summary>
        [Display(Name = "№")]
        public virtual string Number { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Тип")]
        public int Type { get; set; }

        /// <summary>
        /// Модель. 
        /// Называется mark, потому что mvc неадекватно реагирует на свойство model и не биндит модель
        /// </summary>
        [Display(Name = "Модель")]
        public int? Mark { get; set; }

        /// <summary>
        /// Профиль
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Профиль")]
        public int Profile { get; set; }

        /// <summary>
        /// Название профиля ккт
        /// </summary>
        public string ProfileName { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Статус")]
        public int Status { get; set; }

        /// <summary>
        /// Назначение
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Назначение")]
        public int Purpose { get; set; }

        /// <summary>
        /// Агент
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Агент")]
        public int Agent { get; set; }

        /// <summary>
        /// Дата ввода
        /// </summary>
        public virtual string BeginTime { get; set; }

        /// <summary>
        /// Дата вывода
        /// </summary>
        public virtual string EndTime { get; set; }

        /// <summary>
        /// Место расположения
        /// </summary>        
        [Required(ErrorMessage = "Поле является обязательным для заполнения")] 
        public int PlacementName { get; set; }

        /// <summary>
        /// Номер места расположения
        /// </summary>        
        public int? PlacementNumericValue { get; set; }

        /// <summary>
        /// Описание места расположения
        /// </summary>
        [Display(Name = "Примечание")]
        public string PlacementComment { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        [Display(Name = "Примечание")]
        public string Comment { get; set; }

        /// <summary>
        /// Идектификатор участка
        /// </summary>
        public int SectorId { get; set; }

        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int StationId { get; set; }

        /// <summary>
        /// Дата для фильтрации кассиров по времени их контракта
        /// </summary>
        public string CurrentDate { get; set; }        

        /// <summary>
        /// Идентификатор привязки терминала к станции
        /// </summary>
        public int SectorStationTerminalId { get; set; }

        /// <summary>
        /// Флаг, есть ли у терминала привязки к кассовомым окнам (в настоящем или будущем)
        /// </summary>
        public bool IsInTellerWindow { get; set; }

        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract IEnumerable<ValidationResult> Validate(ValidationContext context);
    }
}
