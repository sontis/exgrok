﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель редактирования привязки терминала к кассовому окну
    /// </summary>
    public abstract class TerminalToTellerWindowModel : IValidatableObject
    {
        /// <summary>
        /// Идентификатор привязки терминала к кассовому окну
        /// </summary>
        public int TellerWindowTerminalId { get; set; }

        /// <summary>
        /// Номер терминала
        /// </summary>
        [Display(Name = "№ терминала")]
        public string TerminalNumber { get; set; }

        /// <summary>
        /// Профиль терминала
        /// </summary>
        public int TerminalProfile { get; set; }

        /// <summary>
        /// Идентификатор кассового окна
        /// </summary>
        [Required]
        [Display(Name = "№ окна")]
        public int TellerWindow { get; set; }

        /// <summary>
        /// Номер окна
        /// </summary>
        public string TellerWindowNumber { get; set; }
        
        /// <summary>
        /// Дата установки
        /// </summary>
        public virtual string BeginDate { get; set; }
        
        /// <summary>
        /// Дата снятия
        /// </summary>
        public virtual string EndDate { get; set; }

        /// <summary>
        /// Идентификатор участка
        /// </summary>
        public int SectorId { get; set; }

        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int StationId { get; set; }

        /// <summary>
        /// Идентификатор привязки терминала к станции
        /// </summary>
        public int SectorStationTerminalId { get; set; }        

        /// <summary>
        /// Дата для фильтрации кассиров по времени их контракта
        /// </summary>
        public string CurrentDate { get; set; }        

        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract IEnumerable<ValidationResult> Validate(ValidationContext context);
    }
}
