﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель содержимого раcписания работы кассового окна реального или виртуальго
    /// </summary>
    public class TellerWindowScheduleContentModel
    {
        public TellerWindowScheduleContentModel()
        {
            HourTypes = new List<HourTypeModel>();
            DoesWorkByDefault = new List<bool>();
            CanBreakHave = new List<bool>();
        }

        /// <summary>
        /// Идентификатор содержимого раcписания работы кассового окна
        /// </summary>
        public int TellerWindowScheduleContentModelId { get; set; }

        /// <summary>
        /// Идентификатор группы раcписаний работы кассовых окон
        /// </summary>
        public int TellerWindowScheduleId { get; set; }

        /// <summary>
        /// Идентификатор типа дней (рабочие или выходные)
        /// </summary>
        public int TellerWindowScheduleDayTypeId { get; set; }

        /// <summary>
        /// Идентификатор кассивого окна ГРОК
        /// </summary>
        public int TellerWindowId { get; set; }

        /// <summary>
        /// Номер кассового окна или название пункта продажи билетов (виртуальное кассовое окно)
        /// </summary>
        public string TellerWindowNumber { get; set; }

        /// <summary>
        /// Список типов часов для каждого часового интервала дня (была ли работа в этот час или нет)
        /// </summary>
        public List<HourTypeModel> HourTypes { get; set; }

        /// <summary>
        /// Копия HourTypes: для отслеживания изменений на клиенте
        /// </summary>
        public List<HourTypeModel> OldHourTypes 
        { 
            get
            {
                var result = new List<HourTypeModel>();
                HourTypes.ForEach(h => result.Add(new HourTypeModel()
                    {
                        HourTypeId = h.HourTypeId,
                        HourTypeCode = h.HourTypeCode
                    }));

                return result;
            } 
        }

        /// <summary>
        /// Список флагов (для каждого часа): работает ли данное окно по данным ЭСАПРа
        /// </summary>
        public List<bool> DoesWorkByDefault { get; set; }

        /// <summary>
        /// Список флагов (для каждого часа): может ли быть перерыв в это час
        /// </summary>
        public List<bool> CanBreakHave { get; set; } 

        /// <summary>
        /// Строка с перечислением периодов работы кассового окна
        /// </summary>
        public string WorkPeriods { get; set; }
    }
}
