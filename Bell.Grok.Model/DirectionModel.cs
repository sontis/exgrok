﻿
namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель направления
    /// </summary>
    public class DirectionModel
    {
        /// <summary>
        /// Идентификатор направления
        /// </summary>
        public int DirectionId { get; set; }
        /// <summary>
        /// Номер направления
        /// </summary>
        public string DirectionNumber { get; set; }
        /// <summary>
        /// Название направления
        /// </summary>
        public string DirectionName { get; set; }
    }
}
