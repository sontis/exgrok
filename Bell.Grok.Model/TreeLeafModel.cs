﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель листового элемента в дереве навигации
    /// </summary>
    public class TreeLeafModel
    {
        /// <summary>
        /// Название листового элемента (кассира или станции)
        /// </summary>
        public string ObjectName { get; set; }
        /// <summary>
        /// Идентификатор листового элемента (кассира или станции)
        /// </summary>
        public int ObjectId { get; set; }
        /// <summary>
        /// Url на страницу редактирования кассира или станции
        /// </summary>
        public string ObjectUrl { get; set; }
        /// <summary>
        /// Единый идентификатор узла в дереве
        /// </summary>
        public string UnifiedTreeId { get; set; }
        /// <summary>
        /// Ссылка на изображение в дереве
        /// </summary>
        public string ImgUrl { get; set; }
    }
}
