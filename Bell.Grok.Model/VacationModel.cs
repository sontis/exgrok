﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    
    /// <summary>
    /// Модель отпуска
    /// </summary>
    public class VacationModel
    {
        /// <summary>
        /// Id периода отпуска работника
        /// </summary>
        public int VacationPeriodId { get; set; }
        
        /// <summary>
        /// Id работника
        /// </summary>
        public int EmployeeId { get; set; }
        
        /// <summary>
        /// Дата начала отпуска
        /// </summary>
        public string BeginDate { get; set; }
        
        /// <summary>
        /// Дата окончания отпуска
        /// </summary>
        public string EndDate { get; set; }
        
        /// <summary>
        /// Примечание к отпуску
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Дата начала действия договора кассира (с формы а не из БД)
        /// </summary>
        public string CashierContractBeginDate { get; set; }

        /// <summary>
        /// Дата окончания действия договора кассира (с формы а не из БД)
        /// </summary>
        public string CashierContractEndDate { get; set; }
    }
}
