﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Простая модель месяца
    /// </summary>
    public class SimpleMonthModel
    {
        /// <summary>
        /// Год
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Номер месяца
        /// </summary>
        public int Month { get; set; }
    }
}
