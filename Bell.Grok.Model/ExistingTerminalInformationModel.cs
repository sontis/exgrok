﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель информации о существующем терминале
    /// </summary>
    public class ExistingTerminalInformationModel
    {
        /// <summary>
        /// Тип терминала
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Модель терминала
        /// </summary>
        public int Model { get; set; }

        /// <summary>
        /// Профиль терминала
        /// </summary>
        public int Profile { get; set; }

        /// <summary>
        /// Статус терминал
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Назначение терминала
        /// </summary>
        public int Purpose { get; set; }

        /// <summary>
        /// Агент терминала
        /// </summary>
        public int Agent { get; set; }
    }
}
