﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель профессионального навыка
    /// </summary>
    public class ProfessionalLevelModel
    {
        /// <summary>
        /// Id профессионального навыка
        /// </summary>
        public int ProfessionalLevelId { get; set; }

        /// <summary>
        /// Название профессионального навыка
        /// </summary>
        public string ProfessionalLevelName { get; set; }
    }
}
