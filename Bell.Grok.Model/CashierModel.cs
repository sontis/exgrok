﻿using System.ComponentModel.DataAnnotations;


namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель кассира
    /// </summary>
    public class CashierModel
    {
        /// <summary>
        /// Id кассира
        /// </summary>
        public int EmployeeId { get; set; }
        
        /// <summary>
        /// Табельный номер кассира
        /// </summary>
        [Display(Name = "Табельный номер")]
        public string PersonalNumber { get; set; }
        
        /// <summary>
        /// Имя кассира
        /// </summary>
        [Display(Name = "Имя")]       
        public string FirstName { get; set; }

        /// <summary>
        /// Имя кассира из 1С
        /// </summary>
        public string FirstName1C { get; set; }

        /// <summary>
        /// Отчество кассира
        /// </summary>        
        [Display(Name = "Отчество")]        
        public string MiddleName { get; set; }

        /// <summary>
        /// Отчество кассира из 1С
        /// </summary>
        public string MiddleName1C { get; set; }

        /// <summary>
        /// Фамилия кассира
        /// </summary>
        [Display(Name = "Фамилия")]        
        public string LastName { get; set; }

        /// <summary>
        /// Фамилия кассира из 1С
        /// </summary>
        public string LastName1C { get; set; }

        /// <summary>
        /// Ставка кассира
        /// </summary>        
        public virtual decimal? Rate { get; set; }

        /// <summary>
        /// Ставка кассира из 1С
        /// </summary>        
        public decimal? Rate1C { get; set; }

        /// <summary>
        /// Id Должности кассира
        /// </summary>
        [Display(Name = "Должность")]
        public int? PositionId { get; set; }

        /// <summary>
        /// Название должности кассира
        /// </summary>        ]
        public string Position { get; set; }

        /// <summary>
        /// Должность кассира из 1С
        /// </summary>        ]
        public string Position1С { get; set; }

        /// <summary>
        /// Специализация кассира
        /// </summary>
        [Display(Name = "Специализация")]
        public int[] SpecializationIds { get; set; }

        /// <summary>
        /// Id участока производства кассира
        /// </summary>
        [Display(Name = "Участок производства")]
        public int? SectorId { get; set; }

        /// <summary>
        /// Участок кассира из 1С
        /// </summary>
        public int? SectorId1C { get; set; }

        /// <summary>
        /// Название участка кассира из 1С
        /// </summary>
        public string SectorName1C { get; set; }

        /// <summary>
        /// Название участока производства кассира
        /// </summary>        
        public string Sector { get; set; }

        /// <summary>
        /// Участок производства кассира из 1С
        /// </summary>        
        public string Sector1C { get; set; }

        /// <summary>
        /// Станция, на которой работает кассир
        /// </summary>
        [Display(Name = "Станция производства")]
        public int? StationId { get; set; }

        /// <summary>
        /// Дата начала договора
        /// </summary>               
        public virtual string ContractEffectiveFrom { get; set; }

        /// <summary>
        /// Дата начала договора из 1С
        /// </summary>        
        public string ContractEffectiveFrom1C { get; set; }

        /// <summary>
        /// Дата окончания договора
        /// </summary>        
        public virtual string ContractEffectiveTo { get; set; }

        /// <summary>
        /// Дата окончания договора из 1С
        /// </summary>        
        public string ContractEffectiveTo1C { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        [Display(Name = "Примечание")]
        public string Note { get; set; }

        /// <summary>
        /// Максимальное количество часов в неделю работы кассира
        /// </summary>
        [Display(Name = "Макс. кол-во ч/нед.")]
        public int? MaximumHoursPerWeek { get; set; }

        /// <summary>
        /// Максимальная продолжительность смены кассира
        /// </summary>
        [Display(Name = "Макс. продолж. смены")]
        public int? MaximumShiftLength { get; set; }

        /// <summary>
        /// Возможна ли работа кассира в ночное время
        /// </summary>
        [Display(Name = "Работа в ночное время")]
        public bool IsNighttimeAccepted { get; set; }

        /// <summary>
        /// Возможна ли сверхурочная работа кассира
        /// </summary>
        [Display(Name = "Сверхурочная работа")]
        public bool IsOvertimeAccepted { get; set; }

        /// <summary>
        /// Возможна ли работа кассира в выходные
        /// </summary>
        [Display(Name = "Работа в выходные")]
        public bool IsHolidayAccepted { get; set; }

        /// <summary>
        /// Дата для фильтрации кассиров по времени их контракта
        /// </summary>
        public string CurrentDate { get; set; }

        /// <summary>
        /// Профессиональный уровень кассира
        /// </summary>
        [Display(Name = "Профессиональный уровень")]
        public int[] ProfessionalLevelIds { get; set; }        
    }
}
