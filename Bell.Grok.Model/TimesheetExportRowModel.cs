﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы экспортного excel-файла
    /// </summary>
    public class TimesheetExportRowModel : TimesheetRowModel
    {
        /// <summary>
        /// Список отметок о явках и неявках за каждый день месяца
        /// </summary>
        public IEnumerable<int?> Marks { get; set; }

        /// <summary>
        /// Список буквенных обозначений отметок о явках и неявках за каждый день месяца
        /// </summary>
        public IEnumerable<string> MarkLabels 
        {
            get
            {
                return Marks.Select(m => m.HasValue
                                             ? ((TimesheetMark) m.Value).ToTextCode()
                                             : string.Empty);
            }
        }

        /// <summary>
        /// Список рабочих часов за каждый день месяца
        /// </summary>
        public IEnumerable<float?> Hours { get; set; }

        /// <summary>
        /// Список ночных рабочих часов за каждый день месяца
        /// </summary>
        public IEnumerable<float?> NightHours { get; set; }

        /// <summary>
        /// Список сверхурочных часов за каждый день месяца
        /// </summary>
        public IEnumerable<float?> OvertimeHours { get; set; } 

        /// <summary>
        /// Коды затрат для начисления заработной платы
        /// </summary>
        public IEnumerable<string> PaymentViewCodes { get; set; }

        /// <summary>
        /// Часы для каждого кода затрат
        /// </summary>
        public IEnumerable<float> PaymentViewHoursNumber { get; set; }

        /// <summary>
        /// Дни для каждого кода затрат
        /// </summary>
        public IEnumerable<byte> PaymentViewDaysNumber { get; set; }

        /// <summary>
        /// Коды причин неявок
        /// </summary>
        public IEnumerable<string> HookyViewCodes { get; set; }

        /// <summary>
        /// Часы для каждой причины неявки
        /// </summary>
        public IEnumerable<float> HookyViewHoursNumber { get; set; }

        /// <summary>
        /// Дни для каждой причины неявки
        /// </summary>
        public IEnumerable<byte> HookyViewDaysNumber { get; set; }

        /// <summary>
        /// Нижеперечисленные суррогаты необходимы для экспорта в excel-документ
        /// </summary>
        #region Коды оплаты
        public string MarkLabels1 { get { return MarkLabels.ElementAt(0); } }
        public string MarkLabels2 { get { return MarkLabels.ElementAt(1); } }
        public string MarkLabels3 { get { return MarkLabels.ElementAt(2); } }
        public string MarkLabels4 { get { return MarkLabels.ElementAt(3); } }
        public string MarkLabels5 { get { return MarkLabels.ElementAt(4); } }
        public string MarkLabels6 { get { return MarkLabels.ElementAt(5); } }
        public string MarkLabels7 { get { return MarkLabels.ElementAt(6); } }
        public string MarkLabels8 { get { return MarkLabels.ElementAt(7); } }
        public string MarkLabels9 { get { return MarkLabels.ElementAt(8); } }
        public string MarkLabels10 { get { return MarkLabels.ElementAt(9); } }
        public string MarkLabels11 { get { return MarkLabels.ElementAt(10); } }
        public string MarkLabels12 { get { return MarkLabels.ElementAt(11); } }
        public string MarkLabels13 { get { return MarkLabels.ElementAt(12); } }
        public string MarkLabels14 { get { return MarkLabels.ElementAt(13); } }
        public string MarkLabels15 { get { return MarkLabels.ElementAt(14); } }
        public string MarkLabels16 { get { return MarkLabels.ElementAt(15); } }
        public string MarkLabels17 { get { return MarkLabels.ElementAt(16); } }
        public string MarkLabels18 { get { return MarkLabels.ElementAt(17); } }
        public string MarkLabels19 { get { return MarkLabels.ElementAt(18); } }
        public string MarkLabels20 { get { return MarkLabels.ElementAt(19); } }
        public string MarkLabels21 { get { return MarkLabels.ElementAt(20); } }
        public string MarkLabels22 { get { return MarkLabels.ElementAt(21); } }
        public string MarkLabels23 { get { return MarkLabels.ElementAt(22); } }
        public string MarkLabels24 { get { return MarkLabels.ElementAt(23); } }
        public string MarkLabels25 { get { return MarkLabels.ElementAt(24); } }
        public string MarkLabels26 { get { return MarkLabels.ElementAt(25); } }
        public string MarkLabels27 { get { return MarkLabels.ElementAt(26); } }
        public string MarkLabels28 { get { return MarkLabels.ElementAt(27); } }
        public string MarkLabels29 { get { return MarkLabels.Count() > 28 ? MarkLabels.ElementAt(28) : string.Empty; } }
        public string MarkLabels30 { get { return MarkLabels.Count() > 29 ? MarkLabels.ElementAt(29) : string.Empty; } }
        public string MarkLabels31 { get { return MarkLabels.Count() > 30 ? MarkLabels.ElementAt(30) : string.Empty; } }
        #endregion

        #region Часы
        public float? Hours1 { get { return Hours.ElementAt(0); } }
        public float? Hours2 { get { return Hours.ElementAt(1); } }
        public float? Hours3 { get { return Hours.ElementAt(2); } }
        public float? Hours4 { get { return Hours.ElementAt(3); } }
        public float? Hours5 { get { return Hours.ElementAt(4); } }
        public float? Hours6 { get { return Hours.ElementAt(5); } }
        public float? Hours7 { get { return Hours.ElementAt(6); } }
        public float? Hours8 { get { return Hours.ElementAt(7); } }
        public float? Hours9 { get { return Hours.ElementAt(8); } }
        public float? Hours10 { get { return Hours.ElementAt(9); } }
        public float? Hours11 { get { return Hours.ElementAt(10); } }
        public float? Hours12 { get { return Hours.ElementAt(11); } }
        public float? Hours13 { get { return Hours.ElementAt(12); } }
        public float? Hours14 { get { return Hours.ElementAt(13); } }
        public float? Hours15 { get { return Hours.ElementAt(14); } }
        public float? Hours16 { get { return Hours.ElementAt(15); } }
        public float? Hours17 { get { return Hours.ElementAt(16); } }
        public float? Hours18 { get { return Hours.ElementAt(17); } }
        public float? Hours19 { get { return Hours.ElementAt(18); } }
        public float? Hours20 { get { return Hours.ElementAt(19); } }
        public float? Hours21 { get { return Hours.ElementAt(20); } }
        public float? Hours22 { get { return Hours.ElementAt(21); } }
        public float? Hours23 { get { return Hours.ElementAt(22); } }
        public float? Hours24 { get { return Hours.ElementAt(23); } }
        public float? Hours25 { get { return Hours.ElementAt(24); } }
        public float? Hours26 { get { return Hours.ElementAt(25); } }
        public float? Hours27 { get { return Hours.ElementAt(26); } }
        public float? Hours28 { get { return Hours.ElementAt(27); } }
        public float? Hours29 { get { return Hours.Count() > 28 ? Hours.ElementAt(28) : null; } }
        public float? Hours30 { get { return Hours.Count() > 29 ? Hours.ElementAt(29) : null; } }
        public float? Hours31 { get { return Hours.Count() > 30 ? Hours.ElementAt(30) : null; } }
        #endregion

        #region Ночные часы
        public float? NightHours1 { get { return NightHours.ElementAt(0); } }
        public float? NightHours2 { get { return NightHours.ElementAt(1); } }
        public float? NightHours3 { get { return NightHours.ElementAt(2); } }
        public float? NightHours4 { get { return NightHours.ElementAt(3); } }
        public float? NightHours5 { get { return NightHours.ElementAt(4); } }
        public float? NightHours6 { get { return NightHours.ElementAt(5); } }
        public float? NightHours7 { get { return NightHours.ElementAt(6); } }
        public float? NightHours8 { get { return NightHours.ElementAt(7); } }
        public float? NightHours9 { get { return NightHours.ElementAt(8); } }
        public float? NightHours10 { get { return NightHours.ElementAt(9); } }
        public float? NightHours11 { get { return NightHours.ElementAt(10); } }
        public float? NightHours12 { get { return NightHours.ElementAt(11); } }
        public float? NightHours13 { get { return NightHours.ElementAt(12); } }
        public float? NightHours14 { get { return NightHours.ElementAt(13); } }
        public float? NightHours15 { get { return NightHours.ElementAt(14); } }
        public float? NightHours16 { get { return NightHours.ElementAt(15); } }
        public float? NightHours17 { get { return NightHours.ElementAt(16); } }
        public float? NightHours18 { get { return NightHours.ElementAt(17); } }
        public float? NightHours19 { get { return NightHours.ElementAt(18); } }
        public float? NightHours20 { get { return NightHours.ElementAt(19); } }
        public float? NightHours21 { get { return NightHours.ElementAt(20); } }
        public float? NightHours22 { get { return NightHours.ElementAt(21); } }
        public float? NightHours23 { get { return NightHours.ElementAt(22); } }
        public float? NightHours24 { get { return NightHours.ElementAt(23); } }
        public float? NightHours25 { get { return NightHours.ElementAt(24); } }
        public float? NightHours26 { get { return NightHours.ElementAt(25); } }
        public float? NightHours27 { get { return NightHours.ElementAt(26); } }
        public float? NightHours28 { get { return NightHours.ElementAt(27); } }
        public float? NightHours29 { get { return NightHours.Count() > 28 ? NightHours.ElementAt(28) : null; } }
        public float? NightHours30 { get { return NightHours.Count() > 29 ? NightHours.ElementAt(29) : null; } }
        public float? NightHours31 { get { return NightHours.Count() > 30 ? NightHours.ElementAt(30) : null; } }
        #endregion

        #region Сверхурочные часы
        public float? OvertimeHours1 { get { return OvertimeHours.ElementAt(0); } }
        public float? OvertimeHours2 { get { return OvertimeHours.ElementAt(1); } }
        public float? OvertimeHours3 { get { return OvertimeHours.ElementAt(2); } }
        public float? OvertimeHours4 { get { return OvertimeHours.ElementAt(3); } }
        public float? OvertimeHours5 { get { return OvertimeHours.ElementAt(4); } }
        public float? OvertimeHours6 { get { return OvertimeHours.ElementAt(5); } }
        public float? OvertimeHours7 { get { return OvertimeHours.ElementAt(6); } }
        public float? OvertimeHours8 { get { return OvertimeHours.ElementAt(7); } }
        public float? OvertimeHours9 { get { return OvertimeHours.ElementAt(8); } }
        public float? OvertimeHours10 { get { return OvertimeHours.ElementAt(9); } }
        public float? OvertimeHours11 { get { return OvertimeHours.ElementAt(10); } }
        public float? OvertimeHours12 { get { return OvertimeHours.ElementAt(11); } }
        public float? OvertimeHours13 { get { return OvertimeHours.ElementAt(12); } }
        public float? OvertimeHours14 { get { return OvertimeHours.ElementAt(13); } }
        public float? OvertimeHours15 { get { return OvertimeHours.ElementAt(14); } }
        public float? OvertimeHours16 { get { return OvertimeHours.ElementAt(15); } }
        public float? OvertimeHours17 { get { return OvertimeHours.ElementAt(16); } }
        public float? OvertimeHours18 { get { return OvertimeHours.ElementAt(17); } }
        public float? OvertimeHours19 { get { return OvertimeHours.ElementAt(18); } }
        public float? OvertimeHours20 { get { return OvertimeHours.ElementAt(19); } }
        public float? OvertimeHours21 { get { return OvertimeHours.ElementAt(20); } }
        public float? OvertimeHours22 { get { return OvertimeHours.ElementAt(21); } }
        public float? OvertimeHours23 { get { return OvertimeHours.ElementAt(22); } }
        public float? OvertimeHours24 { get { return OvertimeHours.ElementAt(23); } }
        public float? OvertimeHours25 { get { return OvertimeHours.ElementAt(24); } }
        public float? OvertimeHours26 { get { return OvertimeHours.ElementAt(25); } }
        public float? OvertimeHours27 { get { return OvertimeHours.ElementAt(26); } }
        public float? OvertimeHours28 { get { return OvertimeHours.ElementAt(27); } }
        public float? OvertimeHours29 { get { return OvertimeHours.Count() > 28 ? OvertimeHours.ElementAt(28) : null; } }
        public float? OvertimeHours30 { get { return OvertimeHours.Count() > 29 ? OvertimeHours.ElementAt(29) : null; } }
        public float? OvertimeHours31 { get { return OvertimeHours.Count() > 30 ? OvertimeHours.ElementAt(30) : null; } }
        #endregion

        #region Дроби для разделения всех часов и сверхурочных (ставить или не ставить дроби в зависимости от наличия сверхурочных)
        public string OvertimeSlash1 { get { return OvertimeHours1.HasValue ? "/" : null; } }
        public string OvertimeSlash2 { get { return OvertimeHours2.HasValue ? "/" : null; } }
        public string OvertimeSlash3 { get { return OvertimeHours3.HasValue ? "/" : null; } }
        public string OvertimeSlash4 { get { return OvertimeHours4.HasValue ? "/" : null; } }
        public string OvertimeSlash5 { get { return OvertimeHours5.HasValue ? "/" : null; } }
        public string OvertimeSlash6 { get { return OvertimeHours6.HasValue ? "/" : null; } }
        public string OvertimeSlash7 { get { return OvertimeHours7.HasValue ? "/" : null; } }
        public string OvertimeSlash8 { get { return OvertimeHours8.HasValue ? "/" : null; } }
        public string OvertimeSlash9 { get { return OvertimeHours9.HasValue ? "/" : null; } }
        public string OvertimeSlash10 { get { return OvertimeHours10.HasValue ? "/" : null; } }
        public string OvertimeSlash11 { get { return OvertimeHours11.HasValue ? "/" : null; } }
        public string OvertimeSlash12 { get { return OvertimeHours12.HasValue ? "/" : null; } }
        public string OvertimeSlash13 { get { return OvertimeHours13.HasValue ? "/" : null; } }
        public string OvertimeSlash14 { get { return OvertimeHours14.HasValue ? "/" : null; } }
        public string OvertimeSlash15 { get { return OvertimeHours15.HasValue ? "/" : null; } }
        public string OvertimeSlash16 { get { return OvertimeHours16.HasValue ? "/" : null; } }
        public string OvertimeSlash17 { get { return OvertimeHours17.HasValue ? "/" : null; } }
        public string OvertimeSlash18 { get { return OvertimeHours18.HasValue ? "/" : null; } }
        public string OvertimeSlash19 { get { return OvertimeHours19.HasValue ? "/" : null; } }
        public string OvertimeSlash20 { get { return OvertimeHours20.HasValue ? "/" : null; } }
        public string OvertimeSlash21 { get { return OvertimeHours21.HasValue ? "/" : null; } }
        public string OvertimeSlash22 { get { return OvertimeHours22.HasValue ? "/" : null; } }
        public string OvertimeSlash23 { get { return OvertimeHours23.HasValue ? "/" : null; } }
        public string OvertimeSlash24 { get { return OvertimeHours24.HasValue ? "/" : null; } }
        public string OvertimeSlash25 { get { return OvertimeHours25.HasValue ? "/" : null; } }
        public string OvertimeSlash26 { get { return OvertimeHours26.HasValue ? "/" : null; } }
        public string OvertimeSlash27 { get { return OvertimeHours27.HasValue ? "/" : null; } }
        public string OvertimeSlash28 { get { return OvertimeHours28.HasValue ? "/" : null; } }
        public string OvertimeSlash29 { get { return OvertimeHours29.HasValue ? "/" : null; } }
        public string OvertimeSlash30 { get { return OvertimeHours30.HasValue ? "/" : null; } }
        public string OvertimeSlash31 { get { return OvertimeHours31.HasValue ? "/" : null; } }
        #endregion

        #region Сумма дней за половины месяца
        public int DaysHalfMonthNumber1 { get; set; }
        public int DaysHalfMonthNumber2 { get; set; }
        #endregion

        #region Сумма часов за половины месяца
        public float HoursHalfMonthNumber1 { get; set; }
        public float HoursHalfMonthNumber2 { get; set; }
        #endregion

        #region Сумма ночных часов за половины месяца
        public float NightHoursHalfMonthNumber1 { get; set; }
        public float NightHoursHalfMonthNumber2 { get; set; }
        #endregion

        #region Данные по кодам оплаты начислений заработной платы
        public string PaymentViewCodes1 { get { return PaymentViewCodes.Count() > 0 ? PaymentViewCodes.ElementAt(0) : null;  } }
        public string PaymentViewCodes2 { get { return PaymentViewCodes.Count() > 1 ? PaymentViewCodes.ElementAt(1) : null; } }
        public string PaymentViewCodes3 { get { return PaymentViewCodes.Count() > 2 ? PaymentViewCodes.ElementAt(2) : null; } }
        public string PaymentViewCodes4 { get { return PaymentViewCodes.Count() > 3 ? PaymentViewCodes.ElementAt(3) : null; } }
        public string PaymentViewCodes5 { get { return PaymentViewCodes.Count() > 4 ? PaymentViewCodes.ElementAt(4) : null; } }
        public string PaymentViewCodes6 { get { return PaymentViewCodes.Count() > 5 ? PaymentViewCodes.ElementAt(5) : null; } }
        public string PaymentViewCodes7 { get { return PaymentViewCodes.Count() > 6 ? PaymentViewCodes.ElementAt(6) : null; } }
        public string PaymentViewCodes8 { get { return PaymentViewCodes.Count() > 7 ? PaymentViewCodes.ElementAt(7) : null; } }
        public string PaymentViewCodes9 { get { return PaymentViewCodes.Count() > 8 ? PaymentViewCodes.ElementAt(8) : null; } }
        public string PaymentViewCodes10 { get { return PaymentViewCodes.Count() > 9 ? PaymentViewCodes.ElementAt(9) : null; } }
        public string PaymentViewCodes11 { get { return PaymentViewCodes.Count() > 10 ? PaymentViewCodes.ElementAt(10) : null; } }
        public string PaymentViewCodes12 { get { return PaymentViewCodes.Count() > 11 ? PaymentViewCodes.ElementAt(11) : null; } }
        #endregion

        #region Данные по часам начислений заработно платы
        public float? PaymentViewHoursNumber1 { get { return PaymentViewHoursNumber.Count() > 0 ? PaymentViewHoursNumber.ElementAt(0) : (float?)null; } }
        public float? PaymentViewHoursNumber2 { get { return PaymentViewHoursNumber.Count() > 1 ? PaymentViewHoursNumber.ElementAt(1) : (float?)null; } }
        public float? PaymentViewHoursNumber3 { get { return PaymentViewHoursNumber.Count() > 2 ? PaymentViewHoursNumber.ElementAt(2) : (float?)null; } }
        public float? PaymentViewHoursNumber4 { get { return PaymentViewHoursNumber.Count() > 3 ? PaymentViewHoursNumber.ElementAt(3) : (float?)null; } }
        public float? PaymentViewHoursNumber5 { get { return PaymentViewHoursNumber.Count() > 4 ? PaymentViewHoursNumber.ElementAt(4) : (float?)null; } }
        public float? PaymentViewHoursNumber6 { get { return PaymentViewHoursNumber.Count() > 5 ? PaymentViewHoursNumber.ElementAt(5) : (float?)null; } }
        public float? PaymentViewHoursNumber7 { get { return PaymentViewHoursNumber.Count() > 6 ? PaymentViewHoursNumber.ElementAt(6) : (float?)null; } }
        public float? PaymentViewHoursNumber8 { get { return PaymentViewHoursNumber.Count() > 7 ? PaymentViewHoursNumber.ElementAt(7) : (float?)null; } }
        public float? PaymentViewHoursNumber9 { get { return PaymentViewHoursNumber.Count() > 8 ? PaymentViewHoursNumber.ElementAt(8) : (float?)null; } }
        public float? PaymentViewHoursNumber10 { get { return PaymentViewHoursNumber.Count() > 9 ? PaymentViewHoursNumber.ElementAt(9) : (float?)null; } }
        public float? PaymentViewHoursNumber11 { get { return PaymentViewHoursNumber.Count() > 10 ? PaymentViewHoursNumber.ElementAt(10) : (float?)null; } }
        public float? PaymentViewHoursNumber12 { get { return PaymentViewHoursNumber.Count() > 11 ? PaymentViewHoursNumber.ElementAt(11) : (float?)null; } }
        #endregion

        #region Данные по часам начислений заработно платы
        public float? PaymentViewDaysNumber1 { get { return PaymentViewDaysNumber.Count() > 0 ? PaymentViewDaysNumber.ElementAt(0) : (float?)null; } }
        public float? PaymentViewDaysNumber2 { get { return PaymentViewDaysNumber.Count() > 1 ? PaymentViewDaysNumber.ElementAt(1) : (float?)null; } }
        public float? PaymentViewDaysNumber3 { get { return PaymentViewDaysNumber.Count() > 2 ? PaymentViewDaysNumber.ElementAt(2) : (float?)null; } }
        public float? PaymentViewDaysNumber4 { get { return PaymentViewDaysNumber.Count() > 3 ? PaymentViewDaysNumber.ElementAt(3) : (float?)null; } }
        public float? PaymentViewDaysNumber5 { get { return PaymentViewDaysNumber.Count() > 4 ? PaymentViewDaysNumber.ElementAt(4) : (float?)null; } }
        public float? PaymentViewDaysNumber6 { get { return PaymentViewDaysNumber.Count() > 5 ? PaymentViewDaysNumber.ElementAt(5) : (float?)null; } }
        public float? PaymentViewDaysNumber7 { get { return PaymentViewDaysNumber.Count() > 6 ? PaymentViewDaysNumber.ElementAt(6) : (float?)null; } }
        public float? PaymentViewDaysNumber8 { get { return PaymentViewDaysNumber.Count() > 7 ? PaymentViewDaysNumber.ElementAt(7) : (float?)null; } }
        public float? PaymentViewDaysNumber9 { get { return PaymentViewDaysNumber.Count() > 8 ? PaymentViewDaysNumber.ElementAt(8) : (float?)null; } }
        public float? PaymentViewDaysNumber10 { get { return PaymentViewDaysNumber.Count() > 9 ? PaymentViewDaysNumber.ElementAt(9) : (float?)null; } }
        public float? PaymentViewDaysNumber11 { get { return PaymentViewDaysNumber.Count() > 10 ? PaymentViewDaysNumber.ElementAt(10) : (float?)null; } }
        public float? PaymentViewDaysNumber12 { get { return PaymentViewDaysNumber.Count() > 11 ? PaymentViewDaysNumber.ElementAt(11) : (float?)null; } }
        #endregion

        #region Передние Скобки для явок (ставить или не ставить скобки для явок в зависимости от их наличия)
        public string PaymentFrontBrackets1 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes1) ? "(" : null; } }
        public string PaymentFrontBrackets2 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes2) ? "(" : null; } }
        public string PaymentFrontBrackets3 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes3) ? "(" : null; } }
        public string PaymentFrontBrackets4 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes4) ? "(" : null; } }
        public string PaymentFrontBrackets5 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes5) ? "(" : null; } }
        public string PaymentFrontBrackets6 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes6) ? "(" : null; } }
        public string PaymentFrontBrackets7 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes7) ? "(" : null; } }
        public string PaymentFrontBrackets8 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes8) ? "(" : null; } }
        public string PaymentFrontBrackets9 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes9) ? "(" : null; } }
        public string PaymentFrontBrackets10 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes10) ? "(" : null; } }
        public string PaymentFrontBrackets11 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes11) ? "(" : null; } }
        public string PaymentFrontBrackets12 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes12) ? "(" : null; } } 
        #endregion

        #region Задние Скобки для явок (ставить или не ставить скобки для явок в зависимости от их наличия)
        public string PaymentRearBrackets1 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes1) ? ")" : null; } }
        public string PaymentRearBrackets2 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes2) ? ")" : null; } }
        public string PaymentRearBrackets3 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes3) ? ")" : null; } }
        public string PaymentRearBrackets4 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes4) ? ")" : null; } }
        public string PaymentRearBrackets5 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes5) ? ")" : null; } }
        public string PaymentRearBrackets6 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes6) ? ")" : null; } }
        public string PaymentRearBrackets7 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes7) ? ")" : null; } }
        public string PaymentRearBrackets8 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes8) ? ")" : null; } }
        public string PaymentRearBrackets9 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes9) ? ")" : null; } }
        public string PaymentRearBrackets10 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes10) ? ")" : null; } }
        public string PaymentRearBrackets11 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes11) ? ")" : null; } }
        public string PaymentRearBrackets12 { get { return !string.IsNullOrWhiteSpace(PaymentViewCodes12) ? ")" : null; } }
        #endregion

        #region Данные по кодам неявкок
        public string HookyViewCodes1 { get { return HookyViewCodes.Count() > 0 ? HookyViewCodes.ElementAt(0) : null; } }
        public string HookyViewCodes2 { get { return HookyViewCodes.Count() > 1 ? HookyViewCodes.ElementAt(1) : null; } }
        public string HookyViewCodes3 { get { return HookyViewCodes.Count() > 2 ? HookyViewCodes.ElementAt(2) : null; } }
        public string HookyViewCodes4 { get { return HookyViewCodes.Count() > 3 ? HookyViewCodes.ElementAt(3) : null; } }
        public string HookyViewCodes5 { get { return HookyViewCodes.Count() > 4 ? HookyViewCodes.ElementAt(4) : null; } }
        public string HookyViewCodes6 { get { return HookyViewCodes.Count() > 5 ? HookyViewCodes.ElementAt(5) : null; } }
        public string HookyViewCodes7 { get { return HookyViewCodes.Count() > 6 ? HookyViewCodes.ElementAt(6) : null; } }
        public string HookyViewCodes8 { get { return HookyViewCodes.Count() > 7 ? HookyViewCodes.ElementAt(7) : null; } }
        public string HookyViewCodes9 { get { return HookyViewCodes.Count() > 8 ? HookyViewCodes.ElementAt(8) : null; } }
        public string HookyViewCodes10 { get { return HookyViewCodes.Count() > 9 ? HookyViewCodes.ElementAt(9) : null; } }
        public string HookyViewCodes11 { get { return HookyViewCodes.Count() > 10 ? HookyViewCodes.ElementAt(10) : null; } }
        public string HookyViewCodes12 { get { return HookyViewCodes.Count() > 11 ? HookyViewCodes.ElementAt(11) : null; } }
        #endregion

        #region Данные по часам неявок
        public float? HookyViewHoursNumber1 { get { return HookyViewHoursNumber.Count() > 0 ? HookyViewHoursNumber.ElementAt(0) : (float?)null; } }
        public float? HookyViewHoursNumber2 { get { return HookyViewHoursNumber.Count() > 1 ? HookyViewHoursNumber.ElementAt(1) : (float?)null; } }
        public float? HookyViewHoursNumber3 { get { return HookyViewHoursNumber.Count() > 2 ? HookyViewHoursNumber.ElementAt(2) : (float?)null; } }
        public float? HookyViewHoursNumber4 { get { return HookyViewHoursNumber.Count() > 3 ? HookyViewHoursNumber.ElementAt(3) : (float?)null; } }
        public float? HookyViewHoursNumber5 { get { return HookyViewHoursNumber.Count() > 4 ? HookyViewHoursNumber.ElementAt(4) : (float?)null; } }
        public float? HookyViewHoursNumber6 { get { return HookyViewHoursNumber.Count() > 5 ? HookyViewHoursNumber.ElementAt(5) : (float?)null; } }
        public float? HookyViewHoursNumber7 { get { return HookyViewHoursNumber.Count() > 6 ? HookyViewHoursNumber.ElementAt(6) : (float?)null; } }
        public float? HookyViewHoursNumber8 { get { return HookyViewHoursNumber.Count() > 7 ? HookyViewHoursNumber.ElementAt(7) : (float?)null; } }
        public float? HookyViewHoursNumber9 { get { return HookyViewHoursNumber.Count() > 8 ? HookyViewHoursNumber.ElementAt(8) : (float?)null; } }
        public float? HookyViewHoursNumber10 { get { return HookyViewHoursNumber.Count() > 9 ? HookyViewHoursNumber.ElementAt(9) : (float?)null; } }
        public float? HookyViewHoursNumber11 { get { return HookyViewHoursNumber.Count() > 10 ? HookyViewHoursNumber.ElementAt(10) : (float?)null; } }
        public float? HookyViewHoursNumber12 { get { return HookyViewHoursNumber.Count() > 11 ? HookyViewHoursNumber.ElementAt(11) : (float?)null; } }
        #endregion

        #region Данные по часам неявок
        public float? HookyViewDaysNumber1 { get { return HookyViewDaysNumber.Count() > 0 ? HookyViewDaysNumber.ElementAt(0) : (float?)null; } }
        public float? HookyViewDaysNumber2 { get { return HookyViewDaysNumber.Count() > 1 ? HookyViewDaysNumber.ElementAt(1) : (float?)null; } }
        public float? HookyViewDaysNumber3 { get { return HookyViewDaysNumber.Count() > 2 ? HookyViewDaysNumber.ElementAt(2) : (float?)null; } }
        public float? HookyViewDaysNumber4 { get { return HookyViewDaysNumber.Count() > 3 ? HookyViewDaysNumber.ElementAt(3) : (float?)null; } }
        public float? HookyViewDaysNumber5 { get { return HookyViewDaysNumber.Count() > 4 ? HookyViewDaysNumber.ElementAt(4) : (float?)null; } }
        public float? HookyViewDaysNumber6 { get { return HookyViewDaysNumber.Count() > 5 ? HookyViewDaysNumber.ElementAt(5) : (float?)null; } }
        public float? HookyViewDaysNumber7 { get { return HookyViewDaysNumber.Count() > 6 ? HookyViewDaysNumber.ElementAt(6) : (float?)null; } }
        public float? HookyViewDaysNumber8 { get { return HookyViewDaysNumber.Count() > 7 ? HookyViewDaysNumber.ElementAt(7) : (float?)null; } }
        public float? HookyViewDaysNumber9 { get { return HookyViewDaysNumber.Count() > 8 ? HookyViewDaysNumber.ElementAt(8) : (float?)null; } }
        public float? HookyViewDaysNumber10 { get { return HookyViewDaysNumber.Count() > 9 ? HookyViewDaysNumber.ElementAt(9) : (float?)null; } }
        public float? HookyViewDaysNumber11 { get { return HookyViewDaysNumber.Count() > 10 ? HookyViewDaysNumber.ElementAt(10) : (float?)null; } }
        public float? HookyViewDaysNumber12 { get { return HookyViewDaysNumber.Count() > 11 ? HookyViewDaysNumber.ElementAt(11) : (float?)null; } }
        #endregion

        #region Передние Скобки для неявок (ставить или не ставить скобки для явок в зависимости от их наличия)
        public string HookyFrontBrackets1 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes1) ? "(" : null; } }
        public string HookyFrontBrackets2 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes2) ? "(" : null; } }
        public string HookyFrontBrackets3 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes3) ? "(" : null; } }
        public string HookyFrontBrackets4 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes4) ? "(" : null; } }
        public string HookyFrontBrackets5 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes5) ? "(" : null; } }
        public string HookyFrontBrackets6 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes6) ? "(" : null; } }
        public string HookyFrontBrackets7 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes7) ? "(" : null; } }
        public string HookyFrontBrackets8 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes8) ? "(" : null; } }
        public string HookyFrontBrackets9 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes9) ? "(" : null; } }
        public string HookyFrontBrackets10 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes10) ? "(" : null; } }
        public string HookyFrontBrackets11 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes11) ? "(" : null; } }
        public string HookyFrontBrackets12 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes12) ? "(" : null; } }
        #endregion

        #region Задние Скобки для явок (ставить или не ставить скобки для явок в зависимости от их наличия)
        public string HookyRearBrackets1 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes1) ? ")" : null; } }
        public string HookyRearBrackets2 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes2) ? ")" : null; } }
        public string HookyRearBrackets3 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes3) ? ")" : null; } }
        public string HookyRearBrackets4 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes4) ? ")" : null; } }
        public string HookyRearBrackets5 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes5) ? ")" : null; } }
        public string HookyRearBrackets6 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes6) ? ")" : null; } }
        public string HookyRearBrackets7 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes7) ? ")" : null; } }
        public string HookyRearBrackets8 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes8) ? ")" : null; } }
        public string HookyRearBrackets9 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes9) ? ")" : null; } }
        public string HookyRearBrackets10 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes10) ? ")" : null; } }
        public string HookyRearBrackets11 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes11) ? ")" : null; } }
        public string HookyRearBrackets12 { get { return !string.IsNullOrWhiteSpace(HookyViewCodes12) ? ")" : null; } }
        #endregion
    }
}
