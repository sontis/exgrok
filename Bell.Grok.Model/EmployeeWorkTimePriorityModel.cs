﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель временного интервала приоритета станции
    /// </summary>
    public class EmployeeWorkTimePriorityModel
    {
        /// <summary>
        /// Номер дня недели
        /// </summary>
        public int WeekDayNumber { get; set; }

        /// <summary>
        /// Нижняя граница интервала
        /// </summary>
        public string BeginTime { get; set; }

        /// <summary>
        /// Верхняя граница интервала
        /// </summary>
        public string EndTime { get; set; }
    }
}
