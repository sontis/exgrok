﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель представления создания пользователя на основе информации о сотруднике ЦППК
    /// </summary>
    public class UserCreateFromDictionaryModel
    {        
        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public int EmployeeId { get; set; }

        /// <summary>
        /// Табельный номер сотрудника
        /// </summary>
        [Display(Name = "Табельный номер")]
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Поле {0} является обязательным для заполнения")]
        public string EmployeeFirstName { get; set; }

        /// <summary>
        /// Отчество сотрудника
        /// </summary>
        [Display(Name = "Отчество")]        
        public string EmployeeMiddleName { get; set; }

        /// <summary>
        /// Фамилия сотрудника
        /// </summary>
        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Поле {0} является обязательным для заполнения")]
        public string EmployeeLastName { get; set; }
        
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public virtual string Login { get; set; }
        
        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public virtual string Password { get; set; }
                
        /// <summary>
        /// Подтверждение пароля пользователя
        /// </summary>
        public virtual string PasswordConfirmation { get; set; }

        /// <summary>
        /// Примечание пользователя
        /// </summary>
        [Display(Name = "Примечание")]
        public string Description { get; set; }

        /// <summary>
        /// Список моделей роли пользователя (назначенные на пользователя роли)
        /// </summary>
        public virtual UserRoleModel UserRole { get; set; }        
    }    
}