﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы, содержащей информацию о шаблонах графиков кассиров
    /// </summary>
    public class ScheduleTemplateGridModel
    {
        /// <summary>
        /// Идентификатор шаблона графика кассиров
        /// </summary>
        public int ScheduleTemplateId { get; set; }

        /// <summary>
        /// Краткое наименование шаблона графика кассиров
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Полное наименование шаблона графика кассиров
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Дата начала действия шаблона графика кассиров
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Дата окончания действия шаблона графика кассиров
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Автор шаблона графика кассиров
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Автор последних изменений шаблона графика кассиров
        /// </summary>
        public string LastAuthor { get; set; }

        /// <summary>
        /// Время создания шаблона графика кассиров
        /// </summary>
        public string CreationDateTime { get; set; }

        /// <summary>
        /// Время последних изменений шаблона графика кассиров
        /// </summary>
        public string ChangeDateTime { get; set; }

        /// <summary>
        /// Список названий направлений, на которых действует шаблон
        /// </summary>
        public string Directions { get; set; }

        /// <summary>
        /// Список названий участков, на которых действует шаблон
        /// </summary>
        public string Sectors { get; set; }

        /// <summary>
        /// Признак активности шаблона графика кассиров
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Url для просмотра соответствующего шаблона графика кассиров
        /// </summary>
        public string ReviewUrl { get; set; }
    }
}
