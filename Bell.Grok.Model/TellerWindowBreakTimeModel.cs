﻿namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель перерывов в работе кассового окна
    /// </summary>
    public class TellerWindowBreakTimeModel
    {
        /// <summary>
        /// Идентификатор перерыва
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Начало перерыва
        /// </summary>
        public string BeginTime { get; set; }

        /// <summary>
        /// Конец перерыва
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// Идентификатор дня недели 
        /// </summary>
        public int WeekDayId { get; set; }

        /// <summary>
        /// Идентификатор типа перерыва
        /// </summary>
        public int BreakId{ get; set; }
    }
}
