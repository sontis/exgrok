﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель для экспорта месячного табеля кассиров участка в excel-документ
    /// </summary>
    public class TimesheetExportModel
    {
        /// <summary>
        /// ФИО начальника направления
        /// </summary>
        public string DirectionChief { get; set; }

        /// <summary>
        /// ФИО начальника участка
        /// </summary>
        public string SectorChief { get; set; }

        /// <summary>
        /// ФИО автора документа, может совпадать с начальником участка
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public string CreationDate { get; set; }

        /// <summary>
        /// Начало периода табеля (первое число месяца)
        /// </summary>
        public string PeriodBeginDate { get; set; }

        /// <summary>
        /// Конец периода табеля (последнее число месяца)
        /// </summary>
        public string PeriodEndDate { get; set; }

        /// <summary>
        /// Список индивидуальных графиков сотрудников
        /// </summary>
        public List<TimesheetExportRowModel> Rows { get; set; }

        /// <summary>
        /// Список порядковых номеров индивидуальных графиков сотрудников
        /// </summary>
        public List<int> OrderNumbers { get { return Rows.Select((r, i) => i + 1).ToList(); } }
    }
}
