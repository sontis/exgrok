﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель среза расписаний кассовых окон
    /// </summary>
    public class TellerWindowScheduleModel
    {
        /// <summary>
        /// Идентификатор среза расписаний работы кассовых окон в хранилище ГРОК
        /// </summary>
        public int TellerWindowScheduleModelId { get; set; }                   

        /// <summary>
        /// Год, для сезона которого составлен срез расписаний
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Идентификатор станции, для которой предназначен срез расписаний работы кассовых окон
        /// </summary>
        public int SectorStationId { get; set; }

        /// <summary>
        /// Сезон, для которого составлен срез расписаний
        /// </summary>
        public SeasonModel Season { get; set; }
    }
}
