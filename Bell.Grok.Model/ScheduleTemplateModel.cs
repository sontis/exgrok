﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель редактирования шаблона графика кассиров
    /// </summary>
    public abstract class ScheduleTemplateModel : IValidatableObject
    {
        /// <summary>
        /// Идентификатор шаблона графика кассиров
        /// </summary>
        public int ScheduleTemplateId { get; set; }

        [Display(Name = "Полное наименование")]
        [StringLength(300, ErrorMessage = "Полное наименование должно содержать не более 300 символов")]
        [Required(ErrorMessage = "Поле {0} является обязательным для заполнения")]
        public string FullName { get; set; }

        /// <summary>
        /// Дата начала действия шаблона
        /// </summary>
        public virtual string BeginDate { get; set; }

        /// <summary>
        /// Дата окончания действия шаблона
        /// </summary>
        public virtual string EndDate { get; set; }

        /// <summary>
        /// Список идентификаторов направлений, на которых доступен шаблон
        /// </summary>
        [Display(Name = "Направления")]
        public List<int> Directions { get; set; }

        /// <summary>
        /// Список идентификаторов участков, на которых доступен шаблон
        /// </summary>
        [Display(Name = "Участки")]
        public List<int> Sectors { get; set; }        

        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract IEnumerable<ValidationResult> Validate(ValidationContext context);
    }
}
