﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель представления формы направлений и участков производства
    /// </summary>
    public class DirectionSectorViewModel
    {
        /// <summary>
        /// Идентификатор выбранного объекта
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Код выбранного объекта
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Фильтр по БПА
        /// </summary>
        public int? FilterBPAStandart { get; set; }

        /// <summary>
        /// Фильтр по МКТК
        /// </summary>
        public int? FilterMKTKStandart { get; set; }

        /// <summary>
        /// Название направления, на котором находится станция
        /// </summary>
        [Display(Name = "Направление/Рег. центр")]
        public string DirectionName { get; set; }

        /// <summary>
        /// Название участка, на котором расположена станция
        /// </summary>
        [Display(Name = "Участок производства")]
        public string SectorName { get; set; }

        /// <summary>
        /// Разрешение на просмотр нормативных стандартов
        /// </summary>
        public bool CanViewStandart { get; set; }

        /// <summary>
        /// Идентификатор участка, если идёт просмотр участка
        /// </summary>
        public int? SectorId { get; set; }

        /// <summary>
        /// Разрешение на просмотр планов выручки
        /// </summary>
        public bool CanViewProceedsPlan { get; set; }

        /// <summary>
        /// Разрешение на просмотр табелей
        /// </summary>
        public bool CanViewTimesheets { get; set; }
    }
}
