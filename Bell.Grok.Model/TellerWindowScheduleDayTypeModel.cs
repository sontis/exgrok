﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель типа дня для срезов расписаний работы кассовых окон
    /// </summary>
    public class TellerWindowScheduleDayTypeModel
    {
        /// <summary>
        /// Идентификатор типа дня для срезов расписаний работы кассовых окон в БД
        /// </summary>
        public int TellerWindowScheduleDayTypeId { get; set; }

        /// <summary>
        /// Название типа дня для срезов расписаний работы кассовых окон
        /// </summary>
        public string TellerWindowScheduleDayTypeName { get; set; }

        /// <summary>
        /// Флаг наличия данных в базе для расписания работы окон на данный тип дня.
        /// Необходим для индикации составления расписаний (по данным из ЭСАПР или по умолчанию)
        /// в случаях, когда содержимое расписания работы окон в базе ещё не заполнено
        /// </summary>
        public bool AreDataInBase { get; set; }
    }
}
