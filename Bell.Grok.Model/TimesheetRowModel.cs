﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы табелей
    /// </summary>
    public class TimesheetRowModel
    {
        /// <summary>
        /// Идентификатор строки - табель для сотрудника
        /// </summary>
        public int TimesheetDetailId { get; set; }

        /// <summary>
        /// Флаг, составлен ли график на определённый месяц 
        /// для определённого участка
        /// </summary>
        public bool IsExistSchedule { get; set; }

        /// <summary>
        /// Идентификатор списка табелей всех сотрудников на определённый месяц
        /// для определённого участка
        /// </summary>
        public int TimesheetId { get; set; }

        /// <summary>
        /// Полное имя сотрудника
        /// </summary>
        public string EmployeeFullName { get; set; }

        /// <summary>
        /// Должность сотрудника
        /// </summary>
        public string EmployeePosition { get; set; }

        /// <summary>
        /// Табельный номер сотрудника
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Количество отработанных дней за первую половину месяца
        /// </summary>
        public int FirstHalfMonthDays { get; set; }

        /// <summary>
        /// Количество отработанных часов за первую половину месяца
        /// </summary>
        public float FirstHalfMonthHours { get; set; }

        /// <summary>
        /// Количество отработанных ночных часов за первую половину месяца
        /// </summary>
        public float FirstHalfMonthNightHours { get; set; }

        /// <summary>
        /// Количество отработанных дней за вторую половину месяца
        /// </summary>
        public int SecondHalfMonthDays { get; set; }

        /// <summary>
        /// Количество отработанных часов за вторую половину месяца
        /// </summary>
        public float SecondHalfMonthHours { get; set; }

        /// <summary>
        ///  Количество отработанных ночных часов за вторую половину месяца
        /// </summary>
        public float SecondHalfMonthNightHours { get; set; }

        /// <summary>
        /// Количество отработанных дней за месяц
        /// </summary>
        public int TotalMonthDays { get; set; }

        /// <summary>
        /// Количество отработанных часов за месяц
        /// </summary>
        public float TotalMonthHours { get; set; }

        /// <summary>
        /// Количество отработанных ночных часов за месяц
        /// </summary>
        public float TotalMonthNightHours { get; set; }
    }
}
