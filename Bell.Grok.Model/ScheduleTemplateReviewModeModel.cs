﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель частичного представления для показа режима шаблона графика кассиров
    /// </summary>
    public class ScheduleTemplateReviewModeModel
    {
        /// <summary>
        /// Идентификатор режима шаблона графика кассиров
        /// </summary>
        public int ScheduleTemplateModeId { get; set; }

        /// <summary>
        /// Название режима шаблона графика кассиров
        /// </summary>
        public string Name { get; set; }        
    }
}
