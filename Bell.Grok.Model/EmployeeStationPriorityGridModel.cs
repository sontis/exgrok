﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы, содержащей информацию о приоритетах станций для кассира
    /// </summary>
    public class EmployeeStationPriorityGridModel
    {
        /// <summary>
        /// Идентификатор приоритета станции
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public int EmployeeId { get; set; }        

        /// <summary>
        /// Название станции
        /// </summary>
        public string StationName { get; set; }

        /// <summary>
        /// Приоритет
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Время кассира в пути из дома на работу, единица измерения неизвестна
        /// </summary>
        public byte TransferTime { get; set; }        

        /// <summary>
        /// Имеет ли данный приоритет установленные
        /// временные интервалы
        /// </summary>
        public bool HasIntervals { get; set; }
    }
}
