﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки kendo-grid для отображения режима шаблона графика кассиров
    /// </summary>
    public class ScheduleTemplateReviewDetailModel
    {
        /// <summary>
        /// Номер месяца
        /// </summary>
        public int MonthNumber { get; set; }

        /// <summary>
        /// Название месяца
        /// </summary>
        public string MonthName { get; set; }

        /// <summary>
        /// Количество рабочих дней месяца
        /// </summary>
        public int WorkDaysNum { get; set; }

        /// <summary>
        /// Список информации о количестве дневных рабочих часов 
        /// или о том, что день выходной
        /// </summary>
        public List<string> Days { get; set; }

        /// <summary>
        /// Список информации о количестве ночных рабочих часов 
        /// </summary>
        public List<string> Nights { get; set; }

        /// <summary>
        /// Список информации о праздничных днях
        /// </summary>
        public List<bool> Holidays { get; set; }

        /// <summary>
        /// Типы дней
        /// </summary>
        public List<string> DayTypes { get; set; }

        /// <summary>
        /// Флаги
        /// </summary>
        public List<string> AreSplitted { get; set; }

        /// <summary>
        /// Количество дневных рабочих часов
        /// </summary>
        public float HoursNum { get; set; }

        /// <summary>
        /// Количество ночных рабочих часов
        /// </summary>
        public float NightHoursNum { get; set; }

        /// <summary>
        /// Количество праздничных рабочих часов
        /// </summary>
        public float HolidaysHoursNum { get; set; } 
    }
}
