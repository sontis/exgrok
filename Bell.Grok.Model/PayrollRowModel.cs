﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы начислений
    /// </summary>
    public class PayrollRowModel
    {
        /// <summary>
        /// Идентификатор в базе
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Код оплаты
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Количество часов
        /// </summary>
        public float HoursNumber { get; set; }

        /// <summary>
        /// Количество дней
        /// </summary>
        public int DaysNumber { get; set; }
    }
}
