﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель узла-участка в дереве навигации
    /// </summary>
    public class SectorTreeModel
    {
        /// <summary>
        /// Признак, содержит ли узел вложенные элементы
        /// </summary>
        public bool HasChildren { get; set; }    
        /// <summary>
        /// Идентификатор участка
        /// </summary>
        public int SectorId { get; set; }
        /// <summary>
        /// Название участка
        /// </summary>
        public string SectorName { get; set; }
        /// <summary>
        /// Единый идентификатор узла в дереве
        /// </summary>
        public string UnifiedTreeId { get; set; }
        /// <summary>
        /// Url на страницу редактирования нормативной нагрузки
        /// </summary>
        public string ObjectUrl { get; set; }
    }
}
