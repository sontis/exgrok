﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель редактирования кассового окна
    /// </summary>
    public abstract class TellerWindowEditModel : IValidatableObject
    {
        /// <summary>
        /// Идентификатор кассового окна
        /// </summary>
        public int TellerWindowId { get; set; }

        /// <summary>
        /// Номер кассового окна
        /// </summary>
        [Display(Name = "№ окна")]
        public virtual int Number { get; set; }

        /// <summary>
        /// Список специализаций
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Специализация")]
        public IEnumerable<int> Specializations { get; set; }

        /// <summary>
        /// Место расположения
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Название")]
        public int PlacementName { get; set; }

        /// <summary>
        /// Номер места расположения
        /// </summary>        
        [Display(Name = "Номер")]
        public int? PlacementNumericValue { get; set; }

        /// <summary>
        /// Описание места расположения
        /// </summary>
        [Display(Name = "Описание")]
        public string PlacementComment { get; set; }

        /// <summary>
        /// Профиль
        /// </summary>
        [Display(Name = "Профиль")]
        public int? Profile { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Статус")]
        public int Status { get; set; }

        /// <summary>
        /// Способ оплаты
        /// </summary>
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Display(Name = "Способ оплаты")]
        public int PaymentMethod { get; set; }

        /// <summary>
        /// Дата установки статуса/специализации/способа оплаты
        /// </summary>
        public virtual string BeginTime { get; set; }

        /// <summary>
        /// Дата отмены статуса/специализации/способа оплаты
        /// </summary>
        public virtual string EndTime { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        [Display(Name = "Примечание")]
        public string Comment { get; set; }       

        /// <summary>
        /// Идентификатор участка
        /// </summary>
        public int SectorId { get; set; }

        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int StationId { get; set; }

        /// <summary>
        /// Дата для фильтрации кассиров по времени их контракта
        /// </summary>
        public string CurrentDate { get; set; }        

        /// <summary>
        /// Модели режимов работы
        /// </summary>
        public List<TellerWindowOperationPeriodModel> OperationPeriods { get; set; }

        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract IEnumerable<ValidationResult> Validate(ValidationContext context);
    }
}
