﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель специализации
    /// </summary>
    public class SpecializationModel
    {
        /// <summary>
        /// Id специализации
        /// </summary>
        public int SpecializationId { get; set; }
        
        /// <summary>
        /// Имя специализации
        /// </summary>
        public string SpecializationName { get; set; }
    }
}
