﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель добавления индивидуального графика кассира билетного
    /// </summary>
    public abstract class AddSchedulesModel : IValidatableObject
    {
        /// <summary>
        /// Идентификатор участка
        /// </summary>
        public int SectorId { get; set; }

        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int StationId { get; set; }        

        /// <summary>
        /// Все доступные Employee
        /// </summary>
        [Display(Name = "Сотрудник")]
        public int EmployeeId { get; set; }

        /// <summary>
        /// Все доступные шаблоны рассписаний
        /// </summary>
        [Display(Name = "Шаблон")]
        public int ScheduleTemlateId { get; set; }

        /// <summary>
        /// Кассовое окно
        /// </summary>
        [Display(Name = "№ окна")]
        public int TellerWindow { get; set; }

        /// <summary>
        /// Месяц назначения
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// Год назначения
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract IEnumerable<ValidationResult> Validate(ValidationContext context);
    }
}
