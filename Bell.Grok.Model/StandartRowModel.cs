﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель нормативных стандартов одной станции
    /// </summary>
    public class StandartRowModel
    {
        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int SectorStationId { get; set; }

        /// <summary>
        /// Название станции
        /// </summary>
        public string StationName { get; set; }

        /// <summary>
        /// Нормативный стандарт БПА
        /// </summary>
        public int BPA { get; set; }

        /// <summary>
        /// Нормативный стандарт МКТК
        /// </summary>
        public int MKTK { get; set; }
    }
}
