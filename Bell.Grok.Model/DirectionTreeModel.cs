﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель узла-направления в дереве навигации
    /// </summary>
    public class DirectionTreeModel
    {
        /// <summary>
        /// Признак, содержит ли узел вложенные элементы
        /// </summary>
        public bool HasChildren { get; set; }
        /// <summary>
        /// Название направления
        /// </summary>
        public string DirectionName { get; set; }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int DirectionId { get; set; }
    }
}
