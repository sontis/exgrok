﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель структуры узла-участка в дереве навигации
    /// </summary>
    public class SectorStructureItemModel
    {
        /// <summary>
        /// Идентификатор вложенного элемента в узел-участок
        /// </summary>
        public string SectorStructureItemId { get; set; }
        /// <summary>
        /// Название вложенного элемента в узел-участок
        /// </summary>
        public string SectorStructureItemName { get; set; }
        /// <summary>
        /// Признак, содержит ли узел вложенные элементы
        /// </summary>
        public bool HasChildren { get; set; }   
        /// <summary>
        /// Идентификатор родительского узла-участка
        /// </summary>
        public int SectorId { get; set; }
        /// <summary>
        /// Единый идентификатор узла в дереве
        /// </summary>
        public string UnifiedTreeId { get; set; }
    }
}
