﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель строки таблицы, содержащей информацию о пользователях
    /// </summary>
    public class UserGridModel
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Активен ли пользователь (или заблокирован)
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Url на страницу редактирования атрибутов пользователя
        /// </summary>
        public string EditUrl { get; set; }
        /// <summary>
        /// Url на страницу смены пароля пользователя
        /// </summary>
        public string ChangePasswordUrl { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Примечание
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Отчество пользователя
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Роль пользователя
        /// </summary>
        public string Role { get; set; }
    }
}