﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель файла, прикрепляемого к табелю
    /// </summary>
    public class TimesheetFileModel
    {
        /// <summary>
        /// Содержимое файла
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Название файла
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Расширение файла
        /// </summary>
        public string Extension { get; set; }
    }
}
