﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель представления для показа шаблона графика кассиров
    /// </summary>
    public class ScheduleTemplateReviewModel
    {
        /// <summary>
        /// Список моделей режимов
        /// </summary>
        public List<ScheduleTemplateReviewModeModel> Modes { get; set; }

        /// <summary>
        /// Название шаблона графика кассиров
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Год, для которого отображается шаблон графика кассиров
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Url для возврата на форму со списком шаблонов
        /// </summary>
        public string ReturnUrl { get; set; }

        public ScheduleTemplateReviewModel()
        {
            Modes = new List<ScheduleTemplateReviewModeModel>();
        }

        
    }
}
