﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель таблицы RoleObject. Создана для того, чтобы можно было использовать данные из этой таблицы
    /// в Web-слое, так как он не может напрямую обращаться к DAL.
    /// Модель предназначена для описания прав на определённые действия над различными объектами 
    /// для разных ролей пользователей
    /// </summary>
    public class RoleObjectModel
    {
        /// <summary>
        /// Идентификатор записи в таблице RoleObject
        /// </summary>
        public int RoleObjectId { get; set; }

        /// <summary>
        /// Идентификатор роли пользователя
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// Идентификатор объекта, над которым проводится действие
        /// </summary>
        public DataObjects DataObject { get; set; }

        /// <summary>
        /// Идентификатор действия, которое проводится над объектом
        /// </summary>
        public UserActions Action { get; set; }
    }
}
