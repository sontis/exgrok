﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    public class ScheduleRowModel
    {
        /// <summary>
        /// Идентификатор объединения графиков всех кассиров
        /// </summary>
        public int ScheduleId { get; set; }
        /// <summary>
        /// Порядковый номер распиния кассира на определённый месяц на определённой станции.
        /// Необходим для экспорта графиков в excel-документ        
        /// </summary>
        public int N { get; set; }
        /// <summary>
        /// Идентификатор графика отдельного кассира
        /// </summary>
        public int ScheduleDetailId { get; set; }
        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string EmployeeName { get; set; }
        /// <summary>
        /// Должность сотрудника
        /// </summary>
        public string EmployeePosition { get; set; }
        /// <summary>
        /// Табельный номер сотрудника
        /// </summary>
        public string EmployeeNumber { get; set; }
        /// <summary>
        /// Номер кассового окна
        /// </summary>
        public int? TellerWindowNumber { get; set; }
        /// <summary>
        /// Всего часов
        /// </summary>
        public double TotalHours { get; set; }
        /// <summary>
        /// Ночных часов
        /// </summary>
        public double NightHours { get; set; }
        /// <summary>
        /// Праздничных часов
        /// </summary>
        public double HolidayHours { get; set; }
        /// <summary>
        /// Дата согласования
        /// </summary>
        public string ApprovalDate { get; set; }
        /// <summary>
        /// Статус согласования
        /// </summary>
        public string ApprovalStatus { get; set; }
        /// <summary>
        /// Код статуса согласования
        /// </summary>
        public int ApprovalStatusId { get; set; }

        #region Часы по дням
        public float? Day1 { get; set; }
        public float? Day2 { get; set; }
        public float? Day3 { get; set; }
        public float? Day4 { get; set; }
        public float? Day5 { get; set; }
        public float? Day6 { get; set; }
        public float? Day7 { get; set; }
        public float? Day8 { get; set; }
        public float? Day9 { get; set; }
        public float? Day10 { get; set; }
        public float? Day11 { get; set; }
        public float? Day12 { get; set; }
        public float? Day13 { get; set; }
        public float? Day14 { get; set; }
        public float? Day15 { get; set; }
        public float? Day16 { get; set; }
        public float? Day17 { get; set; }
        public float? Day18 { get; set; }
        public float? Day19 { get; set; }
        public float? Day20 { get; set; }
        public float? Day21 { get; set; }
        public float? Day22 { get; set; }
        public float? Day23 { get; set; }
        public float? Day24 { get; set; }
        public float? Day25 { get; set; }
        public float? Day26 { get; set; }
        public float? Day27 { get; set; }
        public float? Day28 { get; set; }
        public float? Day29 { get; set; }
        public float? Day30 { get; set; }
        public float? Day31 { get; set; }
        #endregion 

        #region Ночные часы по дням
        public float? NightDay1 { get; set; }
        public float? NightDay2 { get; set; }
        public float? NightDay3 { get; set; }
        public float? NightDay4 { get; set; }
        public float? NightDay5 { get; set; }
        public float? NightDay6 { get; set; }
        public float? NightDay7 { get; set; }
        public float? NightDay8 { get; set; }
        public float? NightDay9 { get; set; }
        public float? NightDay10 { get; set; }
        public float? NightDay11 { get; set; }
        public float? NightDay12 { get; set; }
        public float? NightDay13 { get; set; }
        public float? NightDay14 { get; set; }
        public float? NightDay15 { get; set; }
        public float? NightDay16 { get; set; }
        public float? NightDay17 { get; set; }
        public float? NightDay18 { get; set; }
        public float? NightDay19 { get; set; }
        public float? NightDay20 { get; set; }
        public float? NightDay21 { get; set; }
        public float? NightDay22 { get; set; }
        public float? NightDay23 { get; set; }
        public float? NightDay24 { get; set; }
        public float? NightDay25 { get; set; }
        public float? NightDay26 { get; set; }
        public float? NightDay27 { get; set; }
        public float? NightDay28 { get; set; }
        public float? NightDay29 { get; set; }
        public float? NightDay30 { get; set; }
        public float? NightDay31 { get; set; }
        #endregion

        #region Типы дней
        public string DayTypes1 { get; set; }
        public string DayTypes2 { get; set; }
        public string DayTypes3 { get; set; }
        public string DayTypes4 { get; set; }
        public string DayTypes5 { get; set; }
        public string DayTypes6 { get; set; }
        public string DayTypes7 { get; set; }
        public string DayTypes8 { get; set; }
        public string DayTypes9 { get; set; }
        public string DayTypes10 { get; set; }
        public string DayTypes11 { get; set; }
        public string DayTypes12 { get; set; }
        public string DayTypes13 { get; set; }
        public string DayTypes14 { get; set; }
        public string DayTypes15 { get; set; }
        public string DayTypes16 { get; set; }
        public string DayTypes17 { get; set; }
        public string DayTypes18 { get; set; }
        public string DayTypes19 { get; set; }
        public string DayTypes20 { get; set; }
        public string DayTypes21 { get; set; }
        public string DayTypes22 { get; set; }
        public string DayTypes23 { get; set; }
        public string DayTypes24 { get; set; }
        public string DayTypes25 { get; set; }
        public string DayTypes26 { get; set; }
        public string DayTypes27 { get; set; }
        public string DayTypes28 { get; set; }
        public string DayTypes29 { get; set; }
        public string DayTypes30 { get; set; }
        public string DayTypes31 { get; set; }
        #endregion

        #region Флаги разделения дней
        public string IsSplitted1 { get; set; }
        public string IsSplitted2 { get; set; }
        public string IsSplitted3 { get; set; }
        public string IsSplitted4 { get; set; }
        public string IsSplitted5 { get; set; }
        public string IsSplitted6 { get; set; }
        public string IsSplitted7 { get; set; }
        public string IsSplitted8 { get; set; }
        public string IsSplitted9 { get; set; }
        public string IsSplitted10 { get; set; }
        public string IsSplitted11 { get; set; }
        public string IsSplitted12 { get; set; }
        public string IsSplitted13 { get; set; }
        public string IsSplitted14 { get; set; }
        public string IsSplitted15 { get; set; }
        public string IsSplitted16 { get; set; }
        public string IsSplitted17 { get; set; }
        public string IsSplitted18 { get; set; }
        public string IsSplitted19 { get; set; }
        public string IsSplitted20 { get; set; }
        public string IsSplitted21 { get; set; }
        public string IsSplitted22 { get; set; }
        public string IsSplitted23 { get; set; }
        public string IsSplitted24 { get; set; }
        public string IsSplitted25 { get; set; }
        public string IsSplitted26 { get; set; }
        public string IsSplitted27 { get; set; }
        public string IsSplitted28 { get; set; }
        public string IsSplitted29 { get; set; }
        public string IsSplitted30 { get; set; }
        public string IsSplitted31 { get; set; }
        #endregion
    }
}
