﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель станции
    /// </summary>
    public class StationModel
    {
        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int StationId { get; set; }
        /// <summary>
        /// Номер станции
        /// </summary>
        public int StationNumber { get; set; }
        /// <summary>
        /// Имя станции
        /// </summary>
        public string StationName { get; set; }
        /// <summary>
        /// К какому сектору относится станция
        /// </summary>
        public int? SectorId { get; set; }
    }
}
