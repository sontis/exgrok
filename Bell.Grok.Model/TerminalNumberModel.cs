﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель для фильтрации терминалов по их номеру
    /// </summary>
    public class TerminalNumberModel
    {
        /// <summary>
        /// Идентификатор терминала
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Номер терминала
        /// </summary>
        public string Number { get; set; }
    }
}
