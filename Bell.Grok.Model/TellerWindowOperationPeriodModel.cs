﻿using System.Collections.Generic;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель режима работы кассового окна
    /// </summary>
    public class TellerWindowOperationPeriodModel
    {
        /// <summary>
        /// Идентификатор режима
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Начало периода режима работы кассового окна
        /// </summary>
        public string BeginDate { get; set; }

        /// <summary>
        /// Конец периода режима работы кассового окна
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Идентификатор режима работы кассового окна
        /// </summary>
        public string ShiftnessId { get; set; }

        /// <summary>
        /// Перерывы в работе кассового окна
        /// </summary>
        public List<List<TellerWindowBreakTimeModel>> BreakTimes { get; set; }

        /// <summary>
        /// Время работы кассового окна
        /// </summary>
        public List<List<TellerWindowWorkTimeModel>> WorkTimes { get; set; }
    }
}
