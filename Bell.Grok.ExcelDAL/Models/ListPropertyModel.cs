﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Модель для описания списочных свойств экспортной модели
    /// </summary>
    public class ListPropertyModel
    {
        /// <summary>
        /// Тип объектов списочного свойства
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// Название списочного свойства
        /// </summary>
        public String PropertyName { get; set; }

        /// <summary>
        /// Свойства типа объектов списочного свойства
        /// </summary>
        public PropertyInfo[] RowsProperties 
        { 
            get
            {
                return Type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
            } 
        }

        /// <summary>
        /// Содержимое списочного свойства
        /// </summary>
        public Object Rows { get; set; }
    }
}
