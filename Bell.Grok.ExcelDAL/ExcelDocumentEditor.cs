﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Bell.Grok.Model;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Класс для редактирования excel-документов, в частности специальных шаблонов для экспорта графиков кассиров в формате excel
    /// </summary>
    public class ExcelDocumentEditor : ExcelDocumentParser
    {
        //Список букв для обозначения колонок
        private readonly List<char> Letters =
            new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ' };

        /// <summary>
        /// Функция, выдирающая из ссылки на ячейку номер строки
        /// </summary>
        /// <param name="cellReference">Ссылка на ячейку</param>
        /// <returns>Номер строки, в которой находится ячейка</returns>
        private uint GetRowIndex(string cellReference)
        {
            var regex = new Regex(@"\d+");
            var match = regex.Match(cellReference);

            return uint.Parse(match.Value);
        }

        /// <summary>
        /// Функция, выдирающая из ссылки на ячейку номер колонки
        /// </summary>
        /// <param name="cellReference">Ссылка на ячейку</param>
        /// <returns>Номер колонки, в которой находится ячейка</returns>
        private string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        /// <summary>
        /// Функция, возвращающая текущий рабочий лист документа
        /// </summary>
        /// <returns></returns>
        protected WorksheetPart GetWorksheetPart()
        {
            var workbookPart = SpreadSheetDocument.WorkbookPart;
            var sheets = workbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
            var relationshipId = sheets.First().Id.Value;
            var worksheetPart = (WorksheetPart)SpreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
            return worksheetPart;
        }

        /// <summary>
        /// Функция, фиксирующая изменения в excel-документе
        /// </summary>
        /// <param name="rowBreaks">Разрывы страниц</param>
        /// <param name="rowCount">Кол-во строк</param>
        protected void SaveChanges(RowBreaks rowBreaks, int rowCount)
        {
            var worksheetPart = GetWorksheetPart();

            CutForPrint(rowBreaks, rowCount, worksheetPart);

            worksheetPart.Worksheet.Save();
        }

        /// <summary>
        /// Функция, разделяющая документ на страницы для вывода на печать
        /// </summary>
        /// <param name="rowBreaks">Разрывы страниц</param>
        /// <param name="rowCount">Кол-во строк</param>
        /// <param name="worksheetPart">Документ</param>
        protected virtual void CutForPrint(RowBreaks rowBreaks, int rowCount, WorksheetPart worksheetPart)
        {            
        }

        /// <summary>
        /// Функция, возвращающая набор данных текущего рабочего листа
        /// </summary>
        /// <returns></returns>
        protected SheetData GetSheetData()
        {
            var worksheetPart = GetWorksheetPart();
            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

            return sheetData;
        }

        /// <summary>
        /// Функция, изменяющая текст в ячейке excel-документа
        /// </summary>
        /// <param name="cell">Ячейчка excel-документа</param>
        /// <param name="text">Новый текст</param>
        protected void AddValueToCell(Cell cell, string text)
        {
            cell.DataType = CellValues.InlineString;
            cell.RemoveAllChildren();
            var inlineString = new InlineString();
            var t = new Text();
            t.Text = text;
            inlineString.AppendChild(t);
            cell.AppendChild(inlineString);
        }

        /// <summary>
        /// Функция, сдвигающая вверх все строки, которые выше, чем строка, после которой предстоит вставить новую
        /// </summary>        
        /// <param name="rowIndex">Номер строки, после которой предстоит вставить новую</param>
        protected virtual void UpdateRowIndexes(uint rowIndex)
        {
            var worksheetPart = GetWorksheetPart();

            //Получаем все строки, которые выше, чем строка, после которой предстоит вставить новую
            var rows = worksheetPart.Worksheet.Descendants<Row>().Where(r => r.RowIndex.Value > rowIndex);

            foreach (var row in rows)
            {
                var newIndex = row.RowIndex + 1;
                var curRowIndex = row.RowIndex.ToString();
                var newRowIndex = newIndex.ToString();

                //Перебиваем ссылки на ячейки
                foreach (Cell cell in row.Elements<Cell>())
                {
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(curRowIndex, newRowIndex));
                }

                row.RowIndex = newIndex;
            }
        }

        /// <summary>
        /// Что называется workhorse-функция для изменения ссылок на ячейки excel-документа
        /// </summary>
        /// <param name="reference">Текущая ссылка на ячейку</param>
        /// <param name="cellRefPart">Вид изменения ссылки (смотри комментарии к типу CellReferencePartEnum)</param>
        /// <returns>Новая ссылка</returns>
        private string IncrementCellReference(string reference, CellReferencePartEnum cellRefPart)
        {
            var newReference = reference;

            if (cellRefPart != CellReferencePartEnum.None && !String.IsNullOrEmpty(reference))
            {
                var parts = Regex.Split(reference, "([A-Z]+)");

                if (cellRefPart == CellReferencePartEnum.Column || cellRefPart == CellReferencePartEnum.Both)
                {
                    var col = parts[1].ToCharArray().ToList();
                    var needsIncrement = true;
                    var index = col.Count - 1;

                    do
                    {
                        // Увеличиваем последнюю букву
                        col[index] = Letters[Letters.IndexOf(col[index]) + 1];

                        // Если буквы алфавита кончились, то переходим на две буквы, первая из которых 'A'
                        if (col[index] == Letters[Letters.Count - 1])
                        {
                            col[index] = Letters[0];
                        }
                        else
                        {
                            needsIncrement = false;
                        }

                    } while (needsIncrement && --index >= 0);

                    // Добавление второй буквы для колонок с номером больше 26
                    if (needsIncrement)
                    {
                        col.Add(Letters[0]);
                    }

                    parts[1] = new String(col.ToArray());
                }

                if (cellRefPart == CellReferencePartEnum.Row || cellRefPart == CellReferencePartEnum.Both)
                {
                    // Увеличиваем номер строки
                    parts[2] = (int.Parse(parts[2]) + 1).ToString();
                }

                newReference = parts[1] + parts[2];
            }

            return newReference;
        }

        /// <summary>
        /// Функция, увеличивающая номера строк у объединных ячеек, находящихся в строках, которые были сдвинуты вверх
        /// </summary>        
        /// <param name="rowIndex">Номер строки, после которой предстоит вставить новую</param>
        protected void UpdateMergedCellReferences(uint rowIndex)
        {
            var worksheetPart = GetWorksheetPart();

            if (worksheetPart.Worksheet.Elements<MergeCells>().Any())
            {
                var mergeCells = worksheetPart.Worksheet.Elements<MergeCells>().FirstOrDefault();

                if (mergeCells != null)
                {
                    // Находим все объединённые ячейки в строчках, которые были перемещены вверх на предыдущем шаге алгоритма
                    var mergeCellsList = mergeCells.Elements<MergeCell>().Where(r => r.Reference.HasValue)
                                                                                     .Where(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) >= rowIndex ||
                                                                                                 GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) >= rowIndex).ToList();

                    // Увеличиваем номер строки всех найденных объединённых ячеек
                    foreach (var mergeCell in mergeCellsList)
                    {
                        var cellReference = mergeCell.Reference.Value.Split(':');

                        if (GetRowIndex(cellReference.ElementAt(0)) >= rowIndex)
                        {
                            cellReference[0] = IncrementCellReference(cellReference.ElementAt(0), CellReferencePartEnum.Row);
                        }

                        if (GetRowIndex(cellReference.ElementAt(1)) >= rowIndex)
                        {
                            cellReference[1] = IncrementCellReference(cellReference.ElementAt(1), CellReferencePartEnum.Row);
                        }

                        mergeCell.Reference = new StringValue(cellReference[0] + ":" + cellReference[1]);
                    }
                }
            }
        }

        /// <summary>
        /// Метод, возвращающий диапазоны колонок(то есть буквенных частей индексов ячеек) объединённых ячеек в строке
        /// документа с указанным номером
        /// </summary>
        /// <param name="rowIndex">Номер ячейки</param>
        /// <returns>Список диапазонов колонок объединённых ячеек в строчном виде</returns>
        protected List<string> FindMergedCellsColumnRanges(uint rowIndex)
        {
            var worksheetPart = GetWorksheetPart();
            var resultList = new List<string>();

            if (worksheetPart.Worksheet.Elements<MergeCells>().Any())
            {
                var mergeCells = worksheetPart.Worksheet.Elements<MergeCells>().FirstOrDefault();

                if (mergeCells != null)
                {
                    // Находим все объединённые ячейки в строчках, которые были перемещены вверх на предыдущем шаге алгоритма
                    var mergeCellsList = mergeCells.Elements<MergeCell>().Where(r => r.Reference.HasValue)
                                                                                     .Where(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) == rowIndex ||
                                                                                                 GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) == rowIndex).ToList();

                    foreach (var mergeCell in mergeCellsList)
                    {
                        var cellReferences = mergeCell.Reference.Value.Split(':');
                        var newCellReferences = new List<string>();

                        foreach (var cellReference in cellReferences)
                        {
                            var parts = Regex.Split(cellReference, "([A-Z]+)");
                            if (parts.Count() > 2)
                            {
                                newCellReferences.Add(parts[1]);
                            }
                        }

                        if (newCellReferences.Count() == 2)
                        {
                            resultList.Add(newCellReferences[0] + ":" + newCellReferences[1]);
                        }
                    }
                }
            }

            return resultList;
        }

        /// <summary>
        /// Метод, проверяющий, имеются ли в строке документа с указанным номером
        /// объединные ячейки
        /// </summary>
        /// <param name="rowIndex">Номер строки</param>
        /// <returns>Истина, если в указанной строке есть объединённые ячейки,
        ///          и ложь в противном случае</returns>
        private bool AreMergedCellsInRow(uint rowIndex)
        {
            var worksheetPart = GetWorksheetPart();

            if (worksheetPart.Worksheet.Elements<MergeCells>().Any())
            {
                var mergeCells = worksheetPart.Worksheet.Elements<MergeCells>().FirstOrDefault();

                if (mergeCells != null)
                {
                    return mergeCells.Elements<MergeCell>().Where(r => r.Reference.HasValue)
                                                   .Any(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) == rowIndex ||
                                                             GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) == rowIndex);
                }
            }

            return false;
        }

        /// <summary>
        /// Метод, добавляющий к элементам списка колоночных индексов диапазонов ячеек
        /// строчный элемент
        /// </summary>
        /// <param name="rowIndex">Строчный элемент индекса ячеек - номер строки</param>
        /// <param name="columnRangesList">Список колоночных индексов диапазонов ячеек</param>
        /// <returns>Полноценный список индексов диапазонов ячеек</returns>
        private List<string> CalculateRangesForMergeInRow(uint rowIndex, List<string> columnRangesList)
        {
            var resultList = new List<string>();

            foreach (var columnRange in columnRangesList)
            {
                var parts = columnRange.Split(':');
                if (parts.Count() == 2)
                {
                    resultList.Add(parts[0] + rowIndex + ":" + parts[1] + rowIndex);
                }
            }

            return resultList;
        }

        /// <summary>
        /// Метод, возвращающий набор ссылок на объединённые ячейки документа
        /// </summary>
        /// <returns>Объект-набор ссылок на объединённые ячейки документа</returns>
        protected MergeCells GetMergeCells()
        {
            var worksheetPart = GetWorksheetPart();

            if (worksheetPart.Worksheet.Elements<MergeCells>().Any())
            {
                return worksheetPart.Worksheet.Elements<MergeCells>().FirstOrDefault();
            }

            return null;
        }

        /// <summary>
        /// Функция, вставляющая новую строчку для графика кассира
        /// </summary>
        /// <param name="row">Строка с метками для данных графика кассира, которую нужно клонировать и вставить после</param>        
        protected virtual void InsertRow(Row row)
        {
            var sheetData = GetSheetData();
            if (sheetData != null)
            {
                var rowIndex = row.RowIndex;

                UpdateRowIndexes(rowIndex);
                UpdateMergedCellReferences(rowIndex);

                var newRow = (Row)row.Clone();
                newRow.RowIndex = newRow.RowIndex.Value + 1;

                //Проиндексируем ячейки склонированной строки
                foreach (Cell cellItem in newRow.Elements<Cell>())
                {
                    var newRowIndex = Convert.ToUInt32(row.RowIndex.Value + 1);
                    var cellReference = cellItem.CellReference.Value;
                    cellItem.CellReference =
                        new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }

                sheetData.InsertAfter(newRow, row);
            }
        }

        /// <summary>
        /// Функция-движок класса. Заменяет метки в файле-шаблоне на данные объекта модели
        /// </summary>
        /// <param name="rows">Набор строк файла-шаблона</param>
        /// <param name="propertyInfos">Список свойств класса модели</param>
        /// <param name="model">Объект экспортной модели</param>
        /// <param name="listPropertiesMetaDataObjectsList">Список объектов-моделей для описания списочных свойств экспортной модели</param>
        protected virtual void ReplaceLabels(IEnumerable<Row> rows, PropertyInfo[] propertyInfos, object model, List<ListPropertyModel> listPropertiesMetaDataObjectsList)
        {
            ListPropertyModel scheduleRows = null;
            ListPropertyModel twsRows = null;

            foreach (var listPropertiesMetaDataObject in listPropertiesMetaDataObjectsList)
            {
                //Находим объект-модель для описания списка графиков кассиров
                if (listPropertiesMetaDataObject.Rows is List<ScheduleRowModel>)
                {
                    scheduleRows = listPropertiesMetaDataObject;
                }

                //Находим объект-модель для описания списка режимов работы касс
                if (listPropertiesMetaDataObject.Rows is List<TellerWindowScheduleRowModel>)
                {
                    twsRows = listPropertiesMetaDataObject;
                }
            }


            //Счётчик строк с информацией о графиках кассиров
            var scheduleRowsCount = 0;
            //Счётчик строк с информацией о режимах работы касс
            var twsRowsCount = 0;

            //Список колоночных диапазонов ячеек, которые в строках, содержащих данные о режиме работы касс, нужно объединить
            //для улучшения читабельности
            var columnRangesForMerge = new List<string>();

            //Список объединённых ячеек документа
            var mergeCells = GetMergeCells();
            //Список ячеек, которые предстоит объединить в строках, содержащих данные о режиме работы касс
            var mergeCellList = new List<MergeCell>();

            foreach (var row in rows)
            {
                //Инициализация флага, содержит ли строка данные о режиме работы касс
                var isTellerWindowModeRow = false;

                int columnCount = row.Descendants<Cell>().Count();

                if (columnCount == 0)
                {
                    continue;
                }

                var zeroCell = GetCellValue(SpreadSheetDocument,
                                            row.Descendants<Cell>().ElementAt(0));


                if (!string.IsNullOrWhiteSpace(zeroCell))
                {
                    //Если строка является заголовочной для подтаблицы с графиками работы касс
                    var isTellerWindowModesHeader = zeroCell.ToLower().Contains("режим");

                    if (isTellerWindowModesHeader)
                    {
                        //Получаем список колочных частей индекса диапазонов для объединения ячеек
                        //в строках, содержащих данные о режиме работы касс - загловочная строка в данном случае
                        //является образцом
                        columnRangesForMerge = FindMergedCellsColumnRanges(row.RowIndex);
                    }
                }

                //Парсим ячейки файла-шаблона
                for (int i = 0; i < columnCount; i++)
                {
                    var cell = row.Descendants<Cell>().ElementAt(i);
                    var cellText = GetCellValue(SpreadSheetDocument,
                                                        row.Descendants<Cell>().ElementAt(i));

                    //Если ячейка не пустая, то анализируем её содержимое
                    if (!string.IsNullOrWhiteSpace(cellText))
                    {
                        //Если ячейка содержит метку свойства списка графиков кассиров, то увеличиваем соответствующий счётчик
                        //и вставляем ещё одну строку с такими метками, если счётчик не привысил количества графиков кассиров
                        if (i == 0 && scheduleRows != null &&
                            cellText.Contains(scheduleRows.PropertyName) && scheduleRows.Rows is List<ScheduleRowModel>)
                        {
                            if (++scheduleRowsCount < (scheduleRows.Rows as List<ScheduleRowModel>).Count())
                            {
                                InsertRow(row);
                            }
                        }

                        //Если ячейка содержит метку свойства списка режимов работы кассовых окон, то увеличиваем соответствующий счётчик
                        if (i == 0 && twsRows != null &&
                            cellText.Contains(twsRows.PropertyName) &&
                            twsRows.Rows is List<TellerWindowScheduleRowModel>)
                        {
                            if (++twsRowsCount < (twsRows.Rows as List<TellerWindowScheduleRowModel>).Count())
                            {
                                InsertRow(row);
                            }
                        }

                        //Парсим содержимое ячейки
                        const string modelpropertyRegExpGroupName = "ModelField";
                        //Захват распарсенного значения будем делать через имя группы
                        var regex = new Regex(@"\[\[(?<" + modelpropertyRegExpGroupName + @">.*?)\]\]");
                        var match = regex.Match(cellText);

                        var wasMatch = match.Success;
                        var isMatched = true;

                        //В некоторых ячейках встречает несколько меток, поэтому парсинг делается в цикле
                        do
                        {
                            isMatched = match.Success;

                            if (isMatched)
                            {
                                //Получаем распарсенное значение по имени группы
                                var value = match.Groups[modelpropertyRegExpGroupName].Value;
                                match = match.NextMatch();

                                //Если это название одного из свойств модели
                                if (propertyInfos.Select(p => p.Name).ToArray().Contains(value))
                                {
                                    //Получаем значение из объекта модели и подставляем вместо метки
                                    var propertyInfo = propertyInfos.FirstOrDefault(p => p.Name == value);

                                    if (propertyInfo != null)
                                    {
                                        var propertyValue = propertyInfo.GetValue(model).ToString();
                                        cellText = cellText.Replace("[[" + value + "]]", propertyValue);
                                    }
                                }

                                //Если это свойство из модели графиков кассиров
                                if (scheduleRows != null && scheduleRows.RowsProperties != null &&
                                    scheduleRows.RowsProperties.Select(l => scheduleRows.PropertyName + "." + l.Name)
                                                .ToArray()
                                                .Contains(value))
                                {
                                    //Получаем значение из текущего элемента списка графиков кассиров и подставляем в соответствующей строчке вместо метки
                                    var scheduleRowsPropertyInfo = scheduleRows.RowsProperties.FirstOrDefault(
                                                                        l => (scheduleRows.PropertyName + "." + l.Name) == value);

                                    if (scheduleRowsPropertyInfo != null)
                                    {
                                        if (scheduleRows.Rows is List<ScheduleRowModel>)
                                        {
                                            var linesPropertyValue =
                                                scheduleRowsPropertyInfo.GetValue(
                                                    (scheduleRows.Rows as List<ScheduleRowModel>)[scheduleRowsCount - 1]);
                                            var linesPropertyValueStr = linesPropertyValue != null
                                                                            ? linesPropertyValue.ToString()
                                                                            : string.Empty;
                                            cellText = cellText.Replace("[[" + value + "]]", linesPropertyValueStr);
                                        }
                                    }
                                }

                                //Если это свойство из модели расписания работы окон
                                if (twsRows != null && twsRows.RowsProperties != null &&
                                    twsRows.RowsProperties.Select(t => twsRows.PropertyName + "." + t.Name)
                                           .ToArray()
                                           .Contains(value))
                                {
                                    //Строка содержит информацию о работе касс
                                    isTellerWindowModeRow = true;

                                    //Получаем значение из текущего элемента списка расписаний работы окон и подставляем в соответствующей строчке вместо метки
                                    var twsRowsPropertyInfo =
                                        twsRows.RowsProperties.FirstOrDefault(
                                            t => (twsRows.PropertyName + "." + t.Name) == value);

                                    if (twsRowsPropertyInfo != null)
                                    {
                                        if (twsRows.Rows is List<TellerWindowScheduleRowModel>)
                                        {
                                            var twsPropertyValue = twsRowsPropertyInfo.GetValue(
                                                                        (twsRows.Rows as List<TellerWindowScheduleRowModel>)[twsRowsCount - 1]);
                                            var twsPropertyValueStr = twsPropertyValue != null
                                                                          ? twsPropertyValue.ToString()
                                                                          : string.Empty;
                                            cellText = cellText.Replace("[[" + value + "]]", twsPropertyValueStr);
                                        }
                                    }
                                }
                            }

                        } while (isMatched);

                        //Если в ячейке были найдены метки, то меняем текст
                        if (wasMatch)
                        {
                            AddValueToCell(cell, cellText);
                        }
                    }
                }

                if (isTellerWindowModeRow)
                {
                    //Список диапазонов ячеек (полных индексов), которые в строках, содержащих данные о режиме работы касс, нужно объединить
                    //для улучшения читабельности
                    var rangesForMerge = CalculateRangesForMergeInRow(row.RowIndex, columnRangesForMerge);
                    //Есть ли в данной строке объекдинённые ячейки
                    var areMergerCellsInRow = AreMergedCellsInRow(row.RowIndex);

                    //Если известен диапазон ячеек, которые надо объединить, и они ещё не объединены                    
                    if (rangesForMerge.Any() && !areMergerCellsInRow)
                    {
                        //Формируем список диапазново ячеек, которые нужно будет объединить
                        foreach (var rangeForMerge in rangesForMerge)
                        {
                            var mergeCell = new MergeCell()
                                {
                                    Reference = rangeForMerge
                                };

                            mergeCellList.Add(mergeCell);
                        }
                    }
                }
            }

            if (mergeCells != null)
            {
                //Объединяем ячейки в строках, содержащих данные о режиме работы касс, нужно объединить
                mergeCells.Append(mergeCellList);
            }
        }

        /// <summary>
        /// Метод, копирующий файл шаблона в участок памяти для дальнейших манипуляций с ним
        /// </summary>
        /// <param name="fileName">Имя файла-шаблона</param>
        /// <returns>Объект класса MemoryStream</returns>
        private MemoryStream GetMemoryStreamFormTemplateFile(string fileName)
        {
            var memoryStream = new MemoryStream();
            var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            var fileLength = fileStream.Length;
            var bytes = new byte[fileLength];

            fileStream.Read(bytes, 0, (int)fileLength);
            memoryStream.Write(bytes, 0, (int)fileLength);
            fileStream.Close();

            return memoryStream;
        }

        /// <summary>
        /// Собственно фасад класса.
        /// </summary>
        /// <param name="fileName">Имя файла-шаблона</param>
        /// <param name="model">Модель с данными</param>
        /// <returns>Файловый поток для дальнейшего вывода его клиенту</returns>
        public Stream EditDocumentFromModel(string fileName, object model)
        {
            var memoryStream = GetMemoryStreamFormTemplateFile(fileName);

            using (SpreadSheetDocument = SpreadsheetDocument.Open(memoryStream, true))
            {
                RowBreaks rowBreaks;
                IEnumerable<Row> rows;
                Init(out rowBreaks, out rows);

                //Получаем свойства класса модели
                var propertyInfos = model.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);

                //Список объектов с данными о списочных свойствах модели
                var listPropertiesMetaDataObjectsList = new List<ListPropertyModel>();
                //Списочные свойства модели
                var modelGenericProperties = propertyInfos.Where(
                    p =>
                    p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(List<>));

                foreach (var modelGenericProperty in modelGenericProperties)
                {
                    var listPropertiesMetaDataObject = new ListPropertyModel();
                    //Определяем тип объектов списочного свойства
                    listPropertiesMetaDataObject.Type = modelGenericProperty.PropertyType.GetGenericArguments()[0];
                    //Определяем имя списочного свойства
                    listPropertiesMetaDataObject.PropertyName = modelGenericProperty.Name;

                    //Получаем содержимое списочного свойства
                    var property = propertyInfos.FirstOrDefault(p => p.Name == listPropertiesMetaDataObject.PropertyName);
                    if (property != null)
                    {
                        listPropertiesMetaDataObject.Rows = property.GetValue(model);
                    }

                    listPropertiesMetaDataObjectsList.Add(listPropertiesMetaDataObject);
                }

                //Ищем метки с именами свойств класса модели и меняем их на данные их модели
                ReplaceLabels(rows, propertyInfos, model, listPropertiesMetaDataObjectsList);

                SaveChanges(rowBreaks, rows.Count());

            }

            //Устанавливаем указатель на начало потока, иначе при сохранении потока в файл вернётся 0 байт
            memoryStream.Position = 0;
            return memoryStream;
        }
    }
}
