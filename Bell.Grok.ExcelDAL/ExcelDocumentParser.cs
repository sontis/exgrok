﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Bell.Grok.Model;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Класс для парсинга excel-документов, содержащих режимы графиков работы
    /// билетных кассиров
    /// </summary>
    public class ExcelDocumentParser
    {
        /// <summary>
        /// Список режимов графиков - таблиц с календарями в excel-документе
        /// </summary>
        private List<ScheduleTemplateModeModel> ScheduleTemplateModes { get; set; }

        /// <summary>
        /// Представление excel-документа библиотекой openxml 
        /// </summary>
        protected SpreadsheetDocument SpreadSheetDocument { get; set; }

        /// <summary>
        /// Год, на который составлен график
        /// </summary>
        private short Year { get; set; }

        /// <summary>
        /// Стиль - для конвертации строчек в числа с плавающей запятой
        /// </summary>
        private NumberStyles Style { get; set; }

        /// <summary>
        /// Культура - для конвертации строчек в числа с плавающей запятой
        /// </summary>
        private CultureInfo Culture { get; set; }

        public ExcelDocumentParser()
        {
            ScheduleTemplateModes = new List<ScheduleTemplateModeModel>();
            Year = 0;
            Style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;
            Culture = CultureInfo.CreateSpecificCulture("en-US");
        }        

        /// <summary>
        /// Метод, непосредственно парсящий конкретную ячейку документа
        /// </summary>
        /// <param name="document">openxml-представление документа</param>
        /// <param name="cell">Целевая ячейка для парсинга</param>
        /// <returns>Содержимое ячейки</returns>
        protected string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue != null ? cell.CellValue.InnerXml : null;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Функция, определяющая тип содержимого документа
        /// </summary>
        /// <param name="rows">Набор строк документа</param>
        /// <returns>Тип содержимого документа</returns>
        private MonthStringType DefineStringType(IEnumerable<Row> rows)
        {
            int monthStringCount = 0;
            bool isAreDayNames = false;
            bool isWithoutJanuaryAndFebruary = false;

            foreach (Row row in rows)
            {
                int columnCount = row.Descendants<Cell>().Count();

                if (columnCount == 0)
                {
                    continue;
                }

                var zeroCell = GetCellValue(SpreadSheetDocument,
                                                            row.Descendants<Cell>().ElementAt(0));

                //Ищем строку месяца январь в таблице режима графика
                if (!string.IsNullOrWhiteSpace(zeroCell) && zeroCell.ToLower().Contains("январь"))
                {
                    isWithoutJanuaryAndFebruary = true;

                    for (int i = 0; i < columnCount; i++)
                    {
                        var cellText = GetCellValue(SpreadSheetDocument,
                                                            row.Descendants<Cell>().ElementAt(i));

                        if (!string.IsNullOrWhiteSpace(cellText) &&
                            WeekDaysDictionary.WeekDays.ContainsKey(cellText))
                        {
                            isAreDayNames = true;
                            break;
                        }
                    }

                    continue;
                }

                //Подсчитываем количество строк, описывающих график работы
                if (isWithoutJanuaryAndFebruary)
                {
                    monthStringCount++;
                }

                //Ищем строку месяца февраль, чтобы закончить подсчёт строк, описывающих график работы
                if (!string.IsNullOrWhiteSpace(zeroCell) && zeroCell.ToLower().Contains("февраль"))
                {
                    isWithoutJanuaryAndFebruary = false;
                    break;
                }
            }

            //Определяем тип
            switch (monthStringCount)
            {
                case 1:
                    //Одна строчка - с количеством дневных рабочих часов
                    return MonthStringType.SimplyStructure;
                case 2:
                    return isAreDayNames
                               //Две строки: название дня недели и количество дневных часов
                               ? MonthStringType.WithoutNightHours
                               //Две строки: количество дневных часов и количество ночных часов
                               : MonthStringType.WithoutDayNames;
                case 3:
                    //Три строки: название дня недели, количество дневных часов и количество ночных часов
                    return MonthStringType.FullStructure;
                case 5:
                    //Пять строк: название дня недели, тип дня (ололо, нет привязки к таблице "календарь"!),
                    //количество дневных часов, количество ночных часов и флаг разделения дня
                    return MonthStringType.UnifiedStructure;
                default:
                    //Если документ левый, то есть excel-документ, но структура не соответствует правилам
                    throw new Exception("Некорректное содержимое документа");
            }            
        }       
        
        /// <summary>
        /// Функция, собирающая информацию о режиме графика (таблица в документе excel)
        /// </summary>
        /// <param name="rows">Набор строк документа</param>
        private void FindMode(IEnumerable<Row> rows)
        {            
            foreach (Row row in rows)
            {
                int columnCount = row.Descendants<Cell>().Count();

                if (columnCount == 0)
                {
                    continue;
                }

                for (int i = 0; i < columnCount; i++)
                {
                    var cellText = GetCellValue(SpreadSheetDocument,
                                                        row.Descendants<Cell>().ElementAt(i));

                    //Обнаружили название режима
                    if (!string.IsNullOrWhiteSpace(cellText) &&
                        cellText.ToLower().StartsWith("режим"))
                    {
                        //Создаём объект соответсвующей модели
                        var scheduleTemplateMode = new ScheduleTemplateModeModel()
                        {
                            Name = cellText,
                            ScheduleTemplateDetails = new List<ScheduleTemplateDetailModel>()
                        };

                        ScheduleTemplateModes.Add(scheduleTemplateMode);
                    }
                }

                for (int i = 0; i < columnCount; i++)
                {
                    var cellText = GetCellValue(SpreadSheetDocument,
                                                                row.Descendants<Cell>().ElementAt(i));

                    //Ищем год в документе, на который рассчитан график
                    if (!string.IsNullOrWhiteSpace(cellText) &&
                        cellText.ToLower().Contains("график работы"))
                    {
                        Year = Int16.Parse(Regex.Match(cellText, @"\b\d{4}\b").Value);
                    }
                }
                
            }

            //Если режим только один, то названия у него нет
            if (ScheduleTemplateModes.Count == 0)
            {
                var scheduleTemplateMode = new ScheduleTemplateModeModel()
                {
                    Name = null,
                    ScheduleTemplateDetails = new List<ScheduleTemplateDetailModel>()
                };

                ScheduleTemplateModes.Add(scheduleTemplateMode);
            }

            //По новым требованиям тз в шаблоне графиков не может быть более одного режима
            if (ScheduleTemplateModes.Count > 1)
            {
                throw new Exception("Каждому шаблону работы кассира билетного может соответствовать единственный режим работы кассира билетного!");
            }
        }        

        /// <summary>
        /// Основная функция парсера, анализирующая содержимое ячеек с количеством рабочих часов
        /// </summary>
        /// <param name="rows">Набор строк документа</param>
        /// <param name="monthStringType">Тип содержимого документа</param>
        /// <param name="calendar">Производственный календарь, содержащий информацию о праздничных днях</param>
        private void ParseModeData(IEnumerable<Row> rows, MonthStringType monthStringType, List<CalendarModel> calendar)
        {            
            int firstMonthDayCellNumber = 0;
            bool isInMonthString = false;
            int currentMonthStringCounter = 0;            
            byte currentMonthNumber = 0;
            int tableCounter = 0;            

            foreach (Row row in rows) 
            {
                int columnCount = row.Descendants<Cell>().Count();

                if (columnCount == 0)
                {
                    continue;
                }

                var zeroCell = GetCellValue(SpreadSheetDocument,
                                            row.Descendants<Cell>().ElementAt(0));

                //Ищем таблицу с информацией о режиме
                if (!string.IsNullOrWhiteSpace(zeroCell))
                {
                    var isHeader = zeroCell.ToLower().Contains("месяц");

                    if (isHeader)
                    {
                        //Увеличиваем счётчик режимов, чтобы записать информацию в нужный объект модели ScheduleTemplateModeModel
                        tableCounter++;                        
                        for (int i = 0; i < columnCount; i++)
                        {
                            var cellText = GetCellValue(SpreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                            //Определение номера колонки с информацией о рабочих часах первого числа месяца
                            if (!string.IsNullOrWhiteSpace(cellText) &&
                                (cellText.Contains("1") || cellText.ToLower().Contains("число")))
                            {
                                firstMonthDayCellNumber = i;
                                break;
                            }
                        }
                    }
                }

                if (columnCount < 33)
                {
                    continue;
                }
                
                var month = GetCellValue(SpreadSheetDocument, row.Descendants<Cell>().ElementAt(0));

                #region MonthIsnotNull

                //Если верхняя колонка с информацией о рабочих часах кассира
                if (month != null && MonthsDictionary.Months.ContainsKey(month.ToLower()))
                {
                    isInMonthString = true;
                    currentMonthStringCounter = 0;

                    currentMonthNumber = MonthsDictionary.Months[month.ToLower()];                    

                    var j = 0;
                    for (int i = firstMonthDayCellNumber; i <= firstMonthDayCellNumber + 30; i++)
                    {                        
                        j++;

                        //Создание объекта колонки таблицы excel-документа, содержащего информацию
                        //о режиме графика кассиров
                        var scheduleTemplateDetail = new ScheduleTemplateDetailModel();

                        scheduleTemplateDetail.Month = currentMonthNumber;
                        scheduleTemplateDetail.Year = Year;
                        scheduleTemplateDetail.DayType = (int) DayType.VD;

                        var isValidDate = true;
                        DateTime date = DateTime.MaxValue;
                        var dateStr = j + "." + currentMonthNumber + "." + Year;
                        if (DateTime.TryParse(dateStr, out date))
                        {
                            scheduleTemplateDetail.Date = date;                                
                        }
                        else
                        {
                            isValidDate = false;
                        }

                        //Если количество рабочих часов указано в верхней строке
                        if (monthStringType == MonthStringType.WithoutDayNames ||
                            monthStringType == MonthStringType.SimplyStructure)
                        {
                            var workDayHours = GetCellValue(SpreadSheetDocument,
                                                            row.Descendants<Cell>().ElementAt(i));

                            scheduleTemplateDetail.UpdateHoursAndDayType(workDayHours, calendar, Style, Culture, date);                                
                        }

                        //Добавляем информацию из колонки в объект, описывающий режим графика
                        if (isValidDate && ScheduleTemplateModes.Any())
                        {
                            if (tableCounter > ScheduleTemplateModes.Count)
                            {
                                throw new Exception("Произошла ошибка разбора шаблона графиков. Ключевое слово «Режим» в названии режимов графика работы не найдено");
                            }

                            ScheduleTemplateModes[tableCounter - 1].ScheduleTemplateDetails.Add(scheduleTemplateDetail);
                        }                        
                    }
                }

                #endregion

                #region MonthIsNull

                //Для средних и нижних строк с информацией о рабочих часах кассира
                if (month == null)
                {
                    if (isInMonthString)
                    {
                        currentMonthStringCounter++;
                    }

                    var j = 0;
                    for (int i = firstMonthDayCellNumber; i <= firstMonthDayCellNumber + 30; i++)
                    {
                        j++;

                        DateTime date = DateTime.MinValue;
                        var dateStr = j + "." + currentMonthNumber + "." + Year;
                        if (DateTime.TryParse(dateStr, out date))
                        {
                            date = new DateTime(Year, currentMonthNumber, j);
                        }

                        if (ScheduleTemplateModes.Any() && tableCounter > 0 && ScheduleTemplateModes.Count == tableCounter)
                        {
                            //Находим объект колонки таблицы excel-документа, содержащего информацию
                            //о режиме графика кассиров и продолжаем его заполнять
                            var scheduleTemplateDetail =
                                ScheduleTemplateModes[tableCounter - 1].ScheduleTemplateDetails.FirstOrDefault(s => s.Date == date);

                            if (scheduleTemplateDetail != null)
                            {
                                //Нижняя строка в двустрочной структуре без названий дней недели
                                if (monthStringType == MonthStringType.WithoutDayNames)
                                {
                                    var workNightDayHours = GetCellValue(SpreadSheetDocument,
                                                                             row.Descendants<Cell>().ElementAt(i));

                                    scheduleTemplateDetail.UpdateNightHours(workNightDayHours, Style, Culture);

                                    isInMonthString = false;
                                }

                                //Нижняя строка в двустрочной структуре без количества ночных часов
                                if (monthStringType == MonthStringType.WithoutNightHours)
                                {
                                    var workDayHours = GetCellValue(SpreadSheetDocument,
                                                                    row.Descendants<Cell>().ElementAt(i));

                                    scheduleTemplateDetail.UpdateHoursAndDayType(workDayHours, calendar, Style, Culture, date); 

                                    isInMonthString = false;
                                }
                                
                                if (monthStringType == MonthStringType.FullStructure)
                                {
                                    //Средняя строка в "полной" структуре
                                    if (currentMonthStringCounter == 1)
                                    {
                                        var workDayHours = GetCellValue(SpreadSheetDocument,
                                                                    row.Descendants<Cell>().ElementAt(i));

                                        scheduleTemplateDetail.UpdateHoursAndDayType(workDayHours, calendar, Style, Culture, date); 
                                    }

                                    //Нижняя строка в "полной" структуре
                                    if (currentMonthStringCounter == 2)
                                    {
                                        var workNightDayHours = GetCellValue(SpreadSheetDocument,
                                                                             row.Descendants<Cell>().ElementAt(i));

                                        scheduleTemplateDetail.UpdateNightHours(workNightDayHours, Style,Culture);

                                        isInMonthString = false;
                                    }
                                }

                                if (monthStringType == MonthStringType.UnifiedStructure)
                                {
                                    //Строка с типом дня в унифицированной структуре
                                    if (currentMonthStringCounter == 1)
                                    {
                                        var dayType = GetCellValue(SpreadSheetDocument,
                                                                    row.Descendants<Cell>().ElementAt(i));

                                        scheduleTemplateDetail.UpdateDayType(dayType, Style, Culture);
                                    }

                                    //Строка с количеством часов в унифицированной структуре
                                    if (currentMonthStringCounter == 2)
                                    {
                                        var workDayHours = GetCellValue(SpreadSheetDocument,
                                                                    row.Descendants<Cell>().ElementAt(i));

                                        scheduleTemplateDetail.UpdateHours(workDayHours, Style, Culture);
                                    }

                                    //Строка с количеством ночных часов в унифицированной структуре
                                    if (currentMonthStringCounter == 3)
                                    {
                                        var workNightDayHours = GetCellValue(SpreadSheetDocument,
                                                                    row.Descendants<Cell>().ElementAt(i));

                                        scheduleTemplateDetail.UpdateNightHours(workNightDayHours, Style, Culture);
                                    }

                                    //Строка с флагом разделения дня в унифицированной структуре
                                    if (currentMonthStringCounter == 4)
                                    {
                                        var isSplitted = GetCellValue(SpreadSheetDocument,
                                                                    row.Descendants<Cell>().ElementAt(i));

                                        scheduleTemplateDetail.UpdateIsSplitted(isSplitted, Style, Culture);

                                        isInMonthString = false;
                                    }
                                }
                            }
                        }

                    }

                }

                #endregion
            }
        }

        /// <summary>
        /// Функция, инициализирующая excel-документ для парсинга
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<Row> Init()
        {
            WorkbookPart workbookPart = SpreadSheetDocument.WorkbookPart;
            IEnumerable<Sheet> sheets = workbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)SpreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
            Worksheet workSheet = worksheetPart.Worksheet;
            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
            return sheetData.Descendants<Row>();
        }

        /// <summary>
        /// Функция, инициализирующая excel-документ для парсинга
        /// </summary>
        /// <returns></returns>
        protected void Init(out RowBreaks rowBreaks,out IEnumerable<Row> rows)
        {
            WorkbookPart workbookPart = SpreadSheetDocument.WorkbookPart;
            IEnumerable<Sheet> sheets = workbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
            string relationshipId = sheets.First().Id.Value;
            WorksheetPart worksheetPart = (WorksheetPart)SpreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
            Worksheet workSheet = worksheetPart.Worksheet;
            SheetData sheetData = workSheet.GetFirstChild<SheetData>();

            rowBreaks = workSheet.GetFirstChild<RowBreaks>();            
            rows=sheetData.Descendants<Row>();
        }

        /// <summary>
        /// Основная функция, вызываемая из других проектов
        /// </summary>
        /// <param name="stream">Содержимое файла</param>
        /// <param name="calendar">Производственный календарь</param>
        /// <returns>Список объектов модели ScheduleTemplateModeModel</returns>
        public List<ScheduleTemplateModeModel> Parse(Stream stream, List<CalendarModel> calendar)
        {            
            using (SpreadSheetDocument = SpreadsheetDocument.Open(stream, false))
            {
                IEnumerable<Row> rows = Init();
                MonthStringType monthStringType = DefineStringType(rows);                
                FindMode(rows);
                ParseModeData(rows, monthStringType, calendar);                               
            }

            return ScheduleTemplateModes;
        }        
    }

    public static class ScheduleTemplateDetailModelExtensions
    {
        /// <summary>
        /// Функция для обновления рабочих часов и типа дня объекта модели ScheduleTemplateDetailModel
        /// </summary>
        /// <param name="scheduleTemplateDetail">Объект модели ScheduleTemplateDetailModel</param>
        /// <param name="workDayHours">Количество рабочих часов в строчном представлении</param>
        /// <param name="calendar">Календарь - для определения праздничных дней</param>
        /// <param name="style">Стиль - для парсинга строки в double</param>
        /// <param name="culture">Культура - для парсинга строки в double</param>
        /// <param name="date">Дата в календаре</param>
        public static void UpdateHoursAndDayType(this ScheduleTemplateDetailModel scheduleTemplateDetail, string workDayHours, List<CalendarModel> calendar, 
                                                 NumberStyles style, CultureInfo culture, DateTime date)
        {
            if (workDayHours == null || workDayHours.ToLower().Contains("в"))
            {
                if (workDayHours != null && workDayHours.ToLower().Contains("в*"))
                {
                    scheduleTemplateDetail.DayType = (int) DayType.VVD;
                }
                else
                {
                    scheduleTemplateDetail.DayType = (int) DayType.VD;
                }
            }
            else
            {
                float workDaysHoursNum;
                if (float.TryParse(workDayHours, style, culture, out workDaysHoursNum))
                {
                    scheduleTemplateDetail.DayWorkingHours = workDaysHoursNum;
                    scheduleTemplateDetail.DayType = (int)DayType.WD;
                }
            }                                    

            if (calendar.Any(c => c.Date == date && c.IsHoliday))
            {
                scheduleTemplateDetail.DayType = (int)DayType.HD;
            }
        }      

        /// <summary>
        /// Функция для обновления типа дня объекта модели ScheduleTemplateDetailModel
        /// </summary>
        /// <param name="scheduleTemplateDetail">Объект модели ScheduleTemplateDetailModel</param>
        /// <param name="dayType">Тип дня</param>
        /// <param name="style">Стиль - для парсинга строки в double</param>
        /// <param name="culture">Культура - для парсинга строки в double</param>
        public static void UpdateDayType(this ScheduleTemplateDetailModel scheduleTemplateDetail, string dayType,
                                         NumberStyles style, CultureInfo culture)
        {
            if (dayType == null)
            {
                scheduleTemplateDetail.DayType = (int)DayType.WD;
            }
            else 
            {
                if (dayType.ToLower().Contains("в"))
                {                    
                    scheduleTemplateDetail.DayType = dayType.ToLower().Contains("в*")
                                                        ? (int) DayType.VVD
                                                        : (int) DayType.VD;                   
                } else if (dayType.ToLower().Contains("п"))
                {
                    scheduleTemplateDetail.DayType = (int)DayType.HD;
                }
                else
                {
                    scheduleTemplateDetail.DayType = (int)DayType.WD;
                }
            } 
        }

        /// <summary>
        /// Функция для обновления флага разделения рабочего дня объекта модели ScheduleTemplateDetailModel
        /// </summary>
        /// <param name="scheduleTemplateDetail">Объект модели ScheduleTemplateDetailModel</param>
        /// <param name="isSplitted">Флаг разделения рабочего дня</param>
        /// <param name="style">Стиль - для парсинга строки в double</param>
        /// <param name="culture">Культура - для парсинга строки в double</param>
        public static void UpdateIsSplitted(this ScheduleTemplateDetailModel scheduleTemplateDetail, string isSplitted,
                                            NumberStyles style, CultureInfo culture)
        {
            scheduleTemplateDetail.IsSplitted = isSplitted != null && 
                                                isSplitted.ToLower().Contains("ррд");
        }

        /// <summary>
        /// Функция для обновления рабочих часов объекта модели ScheduleTemplateDetailModel
        /// </summary>
        /// <param name="scheduleTemplateDetail">Объект модели ScheduleTemplateDetailModel</param>
        /// <param name="workDayHours">Количество рабочих часов в строчном представлении</param>
        /// <param name="style">Стиль - для парсинга строки в double</param>
        /// <param name="culture">Культура - для парсинга строки в double</param>
        public static void UpdateHours(this ScheduleTemplateDetailModel scheduleTemplateDetail, string workDayHours,
                                       NumberStyles style, CultureInfo culture)
        {
            float workDaysHoursNum;
            if (float.TryParse(workDayHours, style, culture,
                               out workDaysHoursNum))
            {
                scheduleTemplateDetail.DayWorkingHours = workDaysHoursNum;
            }
        }

        /// <summary>
        /// Функция для обновления ночных рабочих часов объекта модели ScheduleTemplateDetailModel
        /// </summary>
        /// <param name="scheduleTemplateDetail">Объект модели ScheduleTemplateDetailModel</param>
        /// <param name="workNightDayHours">Количество ночных рабочих часов в строчном представлении</param>
        /// <param name="style">Стиль - для парсинга строки в double</param>
        /// <param name="culture">Культура - для парсинга строки в double</param>
        public static void UpdateNightHours(this ScheduleTemplateDetailModel scheduleTemplateDetail, string workNightDayHours,
                                            NumberStyles style, CultureInfo culture)
        {
            float workNightDaysHoursNum;
            if (float.TryParse(workNightDayHours, style, culture,
                               out workNightDaysHoursNum))
            {
                scheduleTemplateDetail.NightWorkingHours = workNightDaysHoursNum;
            }
        }
    }
}
