﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Статический класс, содержащий словарь название дня недели-номер дня недели.
    /// В excel документах названия дней недели содержатся в таком виде
    /// </summary>
    public static class WeekDaysDictionary
    {
        public static readonly Dictionary<string, int> WeekDays = new Dictionary<string, int>
        {
            { "пн", 1 },
            { "вт", 2 },
            { "ср", 3 },
            { "чт", 4 },
            { "пт", 5 },
            { "сб", 6 },
            { "вс", 7 },            
        };
    }
}
