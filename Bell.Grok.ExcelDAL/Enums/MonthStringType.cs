﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Перечисление для различных структур документов excel
    /// в зависимости от содержимого таблиц с режимами графиков
    /// </summary>
    public enum MonthStringType
    {        
        FullStructure = 1,
        WithoutNightHours = 2,
        WithoutDayNames = 3,
        SimplyStructure = 4,
        UnifiedStructure = 5
    }
}
