﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Статический класс, содержащий словарь название месяца-номер месяца
    /// </summary>
    public static class MonthsDictionary
    {
        public static readonly Dictionary<string, byte> Months = new Dictionary<string, byte>
        {
            { "январь", 1 },
            { "февраль", 2 },
            { "март", 3 },
            { "апрель", 4 },
            { "май", 5 },
            { "июнь", 6 },
            { "июль", 7 },
            { "август", 8 },
            { "сентябрь", 9 },
            { "октябрь", 10 },
            { "ноябрь", 11 },
            { "декабрь", 12 },
        };
    }  
}
