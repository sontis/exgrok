﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Перечисление для изменения ссылок на ячейки excel-документа
    /// </summary>
    public enum CellReferencePartEnum
    {
        //Ссылка на ячейку не изменилась
        None,
        //Меняется колонка (то есть буквенная часть ссылки)
        Column,
        //Меняется строка (то есть численная часть ссылки)
        Row,
        //Меняются обе части
        Both
    }
}
