﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Перечисление дней недели
    /// </summary>
    public enum WeekDays
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7
    }
    
    public static class WeekDaysExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления дней недели
        /// </summary>
        public static string ToString(this WeekDays dayNumber)
        {
            switch (dayNumber)
            {
                case WeekDays.Monday:
                    return "Понедельник";
                case WeekDays.Tuesday:
                    return "Вторник";
                case WeekDays.Wednesday:
                    return "Среда";
                case WeekDays.Thursday:
                    return "Четверг";
                case WeekDays.Friday:
                    return "Пятница";
                case WeekDays.Saturday:
                    return "Суббота";
                case WeekDays.Sunday:
                    return "Воскресенье";
                default:
                    return string.Empty;
            }
        }

    }
}
