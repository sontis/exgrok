﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Bell.Grok.Model;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Bell.Grok.ExcelDAL
{
    /// <summary>
    /// Класс для редактирования excel-документов, содержащих табели сотрудников
    /// билетных кассиров
    /// </summary>
    public class TimesheetExcelDocumentEditor : ExcelDocumentEditor
    {
        /// <summary>
        /// Имена столбцов excel-документа, в которых необходимо объединять ячейки (вертикально по 6 строк)         
        /// </summary>
        private readonly List<string> LettersForMerge =
            new List<string>() { "B", "C", "D" };

        /// <summary>
        /// Имена столбцов excel-документа, в которых необходимо объединять ячейки (вертикально по 3 строки)         
        /// </summary>
        private readonly List<string> LettersForHalfMerge =
            new List<string>() { "V" };

        /// <summary>
        /// Перекрытый метод базового класса, вставляет новую строчку в excel-документ
        /// через 6 строк после указанной
        /// </summary>
        /// <param name="row">Строка с метками для данных графика кассира, которую нужно клонировать и вставить через 6 строк</param>
        protected override void InsertRow(Row row)
        {
            var sheetData = GetSheetData();
            if (sheetData != null)
            {
                var rowIndex = row.RowIndex;

                UpdateRowIndexes(rowIndex + 5);
                UpdateMergedCellReferences(rowIndex);

                var newRow = (Row) row.Clone();
                newRow.RowIndex = newRow.RowIndex.Value + 6;

                //Проиндексируем ячейки склонированной строки
                foreach (Cell cellItem in newRow.Elements<Cell>())
                {
                    var newRowIndex = Convert.ToUInt32(row.RowIndex.Value + 6);
                    var cellReference = cellItem.CellReference.Value;
                    cellItem.CellReference =
                        new StringValue(cellReference.Replace(row.RowIndex.Value.ToString(), newRowIndex.ToString()));
                }

                var rowBefore = Init().FirstOrDefault(f => f.RowIndex == row.RowIndex + 5);

                if (rowBefore != null)
                {
                    sheetData.InsertAfter(newRow, rowBefore);
                }
            }
            
        }

        /// <summary>
        /// Перекрытый метод базового класса. Делает тоже самое, но заточен под списочные свойства TimesheetExportModel
        /// </summary>
        /// <param name="rows">Набор строк файла-шаблона</param>
        /// <param name="propertyInfos">Список свойств класса модели</param>
        /// <param name="model">Объект экспортной модели</param>
        /// <param name="listPropertiesMetaDataObjectsList">Список объектов-моделей для описания списочных свойств экспортной модели</param>
        protected override void ReplaceLabels(IEnumerable<Row> rows, PropertyInfo[] propertyInfos, object model, List<ListPropertyModel> listPropertiesMetaDataObjectsList)
        {
            ListPropertyModel timesheetRows = null;
            ListPropertyModel orderNumbers = null;
            //Список пар-значений номеров строк, которые описывают интервал номеров строк, 
            //для ячеек которых необходимо делать вертикальное слияние (по 6 строк)
            var mergeRowIntervals = new Dictionary<uint, uint>();
            //Список пар-значений номеров строк, которые описывают интервал номеров строк, 
            //для ячеек которых необходимо делать вертикальное слияние (по 3 строки)
            var mergeRowHalfIntervals = new Dictionary<uint, uint>();

            foreach (var listPropertiesMetaDataObject in listPropertiesMetaDataObjectsList)
            {
                //Находим объект-модель для описания табелей сотрудников
                if (listPropertiesMetaDataObject.Rows is List<TimesheetExportRowModel>)
                {
                    timesheetRows = listPropertiesMetaDataObject;
                }

                //Находим объект-модель для описания порядковых номеров табелей сотрудников
                if (listPropertiesMetaDataObject.Rows is List<int>)
                {
                    orderNumbers = listPropertiesMetaDataObject;
                }
            }

            //Счётчик строк с информацией о табелях сотрудников
            var timesheetRowsCount = 0;    
            //Счётчик строк в пределах одного табеля (6 строк)
            //1 - отметки о явках для дней первой половины месяца
            //2 - рабочие часы для дней первой половины месяца
            //3 - ночные часы для дней первой половины месяца
            //4 - отметки о явках для дней второй половины месяца
            //5 - рабочие часы для дней второй половины месяца
            //6 - ночные часы для дней второй половины месяца
            var innerRowsCount = 0;            
            //Индекс первой строки для вертикального слияния ячеек в пределах одного табеля (6 строк)
            uint mergeRowIntervalBeginIndex = 0;
            //Индекс первой строки для вертикального слияния ячеек в пределах половины месяца одного табеля (3 строки)
            uint mergeRowHalfIntervalBeginIndex = 0;

            //Список объединённых ячеек документа
            var mergeCells = GetMergeCells();
            //Список ячеек, которые предстоит объединить в строках, содержащих данные о режиме работы касс
            var mergeCellList = new List<MergeCell>();

            foreach (var row in rows)
            {
                int columnCount = row.Descendants<Cell>().Count();

                if (columnCount == 0)
                {
                    continue;
                }
                else
                {                   
                    if (columnCount > 3)
                    {
                        var cellText = GetCellValue(SpreadSheetDocument,
                                                        row.Descendants<Cell>().ElementAt(3));
                       
                        if (!string.IsNullOrWhiteSpace(cellText) && timesheetRows != null)
                        {
                            //Если строка содержит информацию о месячном графике конкретного сотрудника
                            if (cellText.Contains(timesheetRows.PropertyName) &&
                                timesheetRows.Rows is List<TimesheetExportRowModel> &&
                                innerRowsCount < 6)
                            {                                
                                if (innerRowsCount == 0)
                                {
                                    //Если первая подстрока для индивидуального графика,
                                    //увеличиваем соответствующий счётчик
                                    timesheetRowsCount++;                      
                                    //Запоминаем номер строки для последующего вертикального слияния ячеек по 6 строк
                                    mergeRowIntervalBeginIndex = row.RowIndex.Value;
                                    //Запоминаем номер строки для последующего вертикального слияния ячеек по 3 строки
                                    mergeRowHalfIntervalBeginIndex = row.RowIndex.Value;
                                }

                                if (innerRowsCount == 2)
                                {
                                    //Фиксируем интервал номеров строк, 
                                    //для ячеек которых необходимо делать вертикальное слияние по 3 строки
                                    mergeRowHalfIntervals.Add(mergeRowHalfIntervalBeginIndex, row.RowIndex.Value);                                    
                                }

                                if (innerRowsCount == 3)
                                {
                                    //Запоминаем номер строки для последующего вертикального слияния ячеек по 3 строки
                                    mergeRowHalfIntervalBeginIndex = row.RowIndex.Value;
                                }

                                if (timesheetRowsCount < (timesheetRows.Rows as List<TimesheetExportRowModel>).Count())
                                {                                    
                                    InsertRow(row);
                                }

                                //Если последняя подстрока для индивидуального графика
                                if (++innerRowsCount == 6)
                                {
                                    innerRowsCount = 0;

                                    //Фиксируем интервал номеров строк, 
                                    //для ячеек которых необходимо делать вертикальное слияние по 6 строк
                                    mergeRowIntervals.Add(mergeRowIntervalBeginIndex, row.RowIndex.Value);
                                    //Фиксируем интервал номеров строк, 
                                    //для ячеек которых необходимо делать вертикальное слияние по 3 строки
                                    mergeRowHalfIntervals.Add(mergeRowHalfIntervalBeginIndex, row.RowIndex.Value);
                                }
                            }
                        }
                    }
                }

                 //Парсим ячейки файла-шаблона
                for (int i = 0; i < columnCount; i++)
                {
                    var cell = row.Descendants<Cell>().ElementAt(i);
                    var cellText = GetCellValue(SpreadSheetDocument,
                                                        row.Descendants<Cell>().ElementAt(i));

                    //Если ячейка не пустая, то анализируем её содержимое
                    if (!string.IsNullOrWhiteSpace(cellText))
                    {                        
                        //Парсим содержимое ячейки
                        const string modelpropertyRegExpGroupName = "ModelField";
                        //Захват распарсенного значения будем делать через имя группы
                        var regex = new Regex(@"\[\[(?<" + modelpropertyRegExpGroupName + @">.*?)\]\]");
                        var match = regex.Match(cellText);

                        var wasMatch = match.Success;
                        var isMatched = true;
                        
                        //В некоторых ячейках встречает несколько меток, поэтому парсинг делается в цикле
                        do
                        {
                            isMatched = match.Success;

                            if (isMatched)
                            {
                                //Получаем распарсенное значение по имени группы
                                var value = match.Groups[modelpropertyRegExpGroupName].Value;
                                match = match.NextMatch();

                                //Если это название одного из свойств модели
                                if (propertyInfos.Select(p => p.Name).ToArray().Contains(value))
                                {
                                    //Получаем значение из объекта модели и подставляем вместо метки
                                    var propertyInfo = propertyInfos.FirstOrDefault(p => p.Name == value);

                                    if (propertyInfo != null && !propertyInfo.PropertyType.IsGenericType)
                                    {
                                        var propertyValue = propertyInfo.GetValue(model).ToString();
                                        cellText = cellText.Replace("[[" + value + "]]", propertyValue);
                                    }                                    
                                }

                                //Если это свойство из модели табелей
                                if (timesheetRows != null && timesheetRows.RowsProperties != null &&
                                    timesheetRows.RowsProperties.Select(t => timesheetRows.PropertyName + "." + t.Name)
                                                 .ToArray()
                                                 .Contains(value))
                                {
                                    //Получаем значение из текущего элемента списка табелей и подставляем в соответствующей строчке вместо метки
                                    var timesheetRowsPropertyInfo = timesheetRows.RowsProperties.FirstOrDefault(
                                        t => (timesheetRows.PropertyName + "." + t.Name) == value);

                                    if (timesheetRowsPropertyInfo != null)
                                    {
                                        if (timesheetRows.Rows is List<TimesheetExportRowModel>)
                                        {
                                            var linesPropertyValue =
                                                timesheetRowsPropertyInfo.GetValue(
                                                    (timesheetRows.Rows as List<TimesheetExportRowModel>)[
                                                        timesheetRowsCount - 1]);
                                            var linesPropertyValueStr = linesPropertyValue != null
                                                                            ? linesPropertyValue.ToString()
                                                                            : string.Empty;
                                            cellText = cellText.Replace("[[" + value + "]]", linesPropertyValueStr);
                                        }
                                    }
                                }

                                //Если это свойство из списка порядковых номеров
                                if (orderNumbers != null && orderNumbers.PropertyName.Contains(value))
                                {
                                    //Получаем значение из текущего элемента списка порядковых номеров и подставляем в соответствующей строчке вместо метки
                                    if (orderNumbers.Rows is List<int>)
                                    {
                                        var orderNumberPropertyValue =
                                            (orderNumbers.Rows as List<int>)[
                                                    timesheetRowsCount - 1];
                                        var orderNumberPropertyValueStr = orderNumberPropertyValue.ToString();                                                                          
                                        cellText = cellText.Replace("[[" + value + "]]", orderNumberPropertyValueStr);
                                    }                                    
                                }
                            }
                        } while (isMatched);

                        //Если в ячейке были найдены метки, то меняем текст
                        if (wasMatch)
                        {
                            AddValueToCell(cell, cellText);
                        }
                    }
                }
            }

            //Определяем интервал номеров строк, в котором ячейки уже вертикально слиты (так как после вставки строк индексы смёрженных ячеек пересчитывались,
            //поэтому ячейки которые были смёржены ещё в файле-шаблоне уехали вниз и имеют самые большие номера строк)
            var maxBeginRowInterval = mergeRowIntervals.Select(m => m.Key).Max();

            //Проходим по всем интервалов номеров строк, в которых необходимо вертикально слить ячейки
            foreach (var mergeRowInterval in mergeRowIntervals)
            {
                //Если в этом интервале строк нет уже слитых ячеек 
                if (mergeRowInterval.Key != maxBeginRowInterval)
                {
                    //Проходим по по всем колонкам, в которых необходимо вертиально слить ячейки по 6 строк
                    foreach (var letterForMerge in LettersForMerge )
                    {
                        var mergeCell = new MergeCell()
                        {
                            Reference = letterForMerge + mergeRowInterval.Key + ":" + letterForMerge + mergeRowInterval.Value
                        };                    

                         mergeCellList.Add(mergeCell);
                    }                    
                }
            }

            //То же самое для слияния ячеек по три строки
            var maxBeginHalfRowInterval = mergeRowHalfIntervals.Select(m => m.Key).Max();

            foreach (var mergeRowHalfInterval in mergeRowHalfIntervals)
            {
                //Если в этом интервале строк нет уже слитых ячеек 
                if (mergeRowHalfInterval.Key != maxBeginHalfRowInterval && 
                    mergeRowHalfInterval.Key != maxBeginHalfRowInterval - 3)
                {
                    //Проходим по по всем колонкам, в которых необходимо вертиально слить ячейки по 6 строк
                    foreach (var letterForHalfMerge in LettersForHalfMerge)
                    {
                        var mergeCell = new MergeCell()
                        {
                            Reference = letterForHalfMerge + mergeRowHalfInterval.Key + ":" + letterForHalfMerge + mergeRowHalfInterval.Value
                        };

                        mergeCellList.Add(mergeCell);
                    }
                }
            }

            if (mergeCells != null)
            {
                //Объединяем ячейки в строках, для которых требуется вертикальное слияние
                mergeCells.Append(mergeCellList);
            }
        }

        /// <summary>
        /// Функция, разделяющая документ на страницы для вывода на печать
        /// </summary>
        /// <param name="rowBreaks">Разрывы страниц</param>
        /// <param name="rowCount">Кол-во строк</param>
        /// <param name="worksheetPart">Документ</param>
        protected override void CutForPrint(RowBreaks rowBreaks, int rowCount, WorksheetPart worksheetPart)
        {
            Break rowBreak;

            rowBreaks.RemoveAllChildren();
            if (rowCount > 64)
            {
                for (int i = 0; i < (((rowCount - 64) / 60) + 1); i++)
                {
                    rowBreak = new Break() { Id = (UInt32Value)(48u + 60 * i), Max = (UInt32Value)1024U, ManualPageBreak = true };
                    rowBreaks.Append(rowBreak);
                    rowBreaks.ManualBreakCount++;
                    rowBreaks.Count++;
                }
                if (rowCount - (48 + (((rowCount - 64) / 60) * 60)) > 58)
                {
                    rowBreak = new Break() { Id = (UInt32Value)(rowCount - 16u), Max = (UInt32Value)1024U, ManualPageBreak = true };
                    rowBreaks.Append(rowBreak);
                    rowBreaks.ManualBreakCount++;
                    rowBreaks.Count++;
                }
            }
            else
            {
                if (rowCount > 50)
                {
                    rowBreak = new Break() { Id = (UInt32Value)(rowCount - 16u), Max = (UInt32Value)1024U, ManualPageBreak = true };
                    rowBreaks.Append(rowBreak);
                    rowBreaks.ManualBreakCount++;
                    rowBreaks.Count++;
                }
            }
        }

    }
}
