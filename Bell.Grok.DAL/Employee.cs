//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        public Employee()
        {
            this.EmployeeShift = new HashSet<EmployeeShift>();
            this.EmployeeSpecialization = new HashSet<EmployeeSpecialization>();
            this.EmployeeStationPriority = new HashSet<EmployeeStationPriority>();
            this.VacationPeriod = new HashSet<VacationPeriod>();
            this.EmployeeProfessionalLevel = new HashSet<EmployeeProfessionalLevel>();
            this.ScheduleDetail = new HashSet<ScheduleDetail>();
            this.User = new HashSet<User>();
            this.TimeSheetDetail = new HashSet<TimeSheetDetail>();
        }
    
        public int EmployeeID { get; set; }
        public string EmployeeNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Nullable<decimal> RateQuantity { get; set; }
        public Nullable<int> PositionID { get; set; }
        public Nullable<System.DateTime> ContractEffectiveFrom { get; set; }
        public Nullable<System.DateTime> ContractEffectiveTo { get; set; }
        public Nullable<int> MaximumHoursPerWeek { get; set; }
        public Nullable<int> MaximumShiftLength { get; set; }
        public Nullable<bool> IsNighttimeAccepted { get; set; }
        public Nullable<bool> IsOvertimeAccepted { get; set; }
        public Nullable<bool> IsHolidayAccepted { get; set; }
        public Nullable<int> SectorID { get; set; }
        public Nullable<int> BaseStationID { get; set; }
    
        public virtual Position Position { get; set; }
        public virtual ICollection<EmployeeShift> EmployeeShift { get; set; }
        public virtual ICollection<EmployeeSpecialization> EmployeeSpecialization { get; set; }
        public virtual ICollection<EmployeeStationPriority> EmployeeStationPriority { get; set; }
        public virtual ICollection<VacationPeriod> VacationPeriod { get; set; }
        public virtual Sector Sector { get; set; }
        public virtual ICollection<EmployeeProfessionalLevel> EmployeeProfessionalLevel { get; set; }
        public virtual ICollection<ScheduleDetail> ScheduleDetail { get; set; }
        public virtual ICollection<User> User { get; set; }
        public virtual ICollection<TimeSheetDetail> TimeSheetDetail { get; set; }
        public virtual Station Station { get; set; }
    }
}
