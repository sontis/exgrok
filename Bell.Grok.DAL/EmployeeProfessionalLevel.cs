//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeProfessionalLevel
    {
        public int EmployeeProfessionalLevelID { get; set; }
        public int EmployeeID { get; set; }
        public int ProfessionalLevelID { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual ProfessionalLevel ProfessionalLevel { get; set; }
    }
}
