//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmployeeStationPriority
    {
        public EmployeeStationPriority()
        {
            this.EmployeeWorkTimePriority = new HashSet<EmployeeWorkTimePriority>();
        }
    
        public int EmployeeStationPriorityID { get; set; }
        public int EmployeeID { get; set; }
        public Nullable<int> PriorityLevel { get; set; }
        public int StationID { get; set; }
        public byte TransferTime { get; set; }
    
        public virtual Employee Employee { get; set; }
        public virtual Station Station { get; set; }
        public virtual ICollection<EmployeeWorkTimePriority> EmployeeWorkTimePriority { get; set; }
    }
}
