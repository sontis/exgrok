﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Model;
using Bell.Grok.Model.Enums;

namespace Bell.Grok.DAL.Dao
{
    /// <summary>
    /// Имитация сущности сезона срезов работы кассовых окон
    /// </summary>
    public class TellerWindowScheduleSeason
    {
        public int SeasonId { get; set; }
        public string SeasonName { get; set; }
    }

    public class HourType
    {
        public int HourTypeId { get; set; }
        public string HourTypeName { get; set; }
        public string HourTypeCode { get; set; }
    }

    /// <summary>
    /// Имитация сущности типа дня в сезоне среза работы кассовых окон
    /// </summary>
    public class TellerWindowScheduleDayType
    {
        public int TellerWindowScheduleDayTypeId { get; set; }
        public string TellerWindowScheduleDayTypeName { get; set; }
    }

    public class TellerWindowScheduleDao
    {
        /// <summary>
        /// Справочники для срезов работы кассовых окон: сезоны, типы часов и типы дней
        /// </summary>
        public static List<TellerWindowScheduleSeason> Seasons 
        { 
            get
            {
                return new List<TellerWindowScheduleSeason>()
                {
                    new TellerWindowScheduleSeason()
                        {
                            SeasonId = (int)TellerWindowScheduleSeasons.Summer,
                            SeasonName = TellerWindowScheduleSeasons.Summer.ToText()
                        },
                    new TellerWindowScheduleSeason()
                        {
                            SeasonId = (int)TellerWindowScheduleSeasons.Winter,
                            SeasonName = TellerWindowScheduleSeasons.Winter.ToText()
                        }
                };
            } 
        }

        public static List<HourType> HourTypes
        {
            get
            {
                return new List<HourType>()
                {
                    new HourType()
                        {
                            HourTypeId = (int)TellerWindowScheduleHourTypes.Rest,
                            HourTypeName = TellerWindowScheduleHourTypes.Rest.ToText(),
                            HourTypeCode = TellerWindowScheduleHourTypes.Rest.ToTextCode()
                        },
                    new HourType()
                        {
                            HourTypeId = (int)TellerWindowScheduleHourTypes.Work,
                            HourTypeName = TellerWindowScheduleHourTypes.Work.ToText(),
                            HourTypeCode = TellerWindowScheduleHourTypes.Work.ToTextCode()
                        },
                    new HourType()
                        {
                            HourTypeId = (int)TellerWindowScheduleHourTypes.TechnologicalBreak,
                            HourTypeName = TellerWindowScheduleHourTypes.TechnologicalBreak.ToText(),
                            HourTypeCode = TellerWindowScheduleHourTypes.TechnologicalBreak.ToTextCode()
                        },
                    new HourType()
                        {
                            HourTypeId = (int)TellerWindowScheduleHourTypes.DinnerBreak,
                            HourTypeName = TellerWindowScheduleHourTypes.DinnerBreak.ToText(),
                            HourTypeCode = TellerWindowScheduleHourTypes.DinnerBreak.ToTextCode()
                        }
                };
            }
        }

        public static List<TellerWindowScheduleDayType> TellerWindowScheduleDayTypes
        {
            get
            {
                return new List<TellerWindowScheduleDayType>()
                {
                    new TellerWindowScheduleDayType()
                        {
                            TellerWindowScheduleDayTypeId = (int)Model.Enums.TellerWindowScheduleDayTypes.Weekdays,
                            TellerWindowScheduleDayTypeName = Model.Enums.TellerWindowScheduleDayTypes.Weekdays.ToText()
                        },
                    new TellerWindowScheduleDayType()
                        {
                            TellerWindowScheduleDayTypeId = (int)Model.Enums.TellerWindowScheduleDayTypes.Holidays,
                            TellerWindowScheduleDayTypeName = Model.Enums.TellerWindowScheduleDayTypes.Holidays.ToText()
                        }
                };
            }
        }               

        /// <summary>
        /// Метод, возвращающий определённый срез работы кассовых окон станции
        /// </summary>
        /// <param name="isWinter">Сезон - зимний или летний</param>
        /// <param name="isHoliday">Тип дней - праздники (и выходные) или будни</param>
        /// <param name="year">Год</param>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Срез работы кассовых окон станции</returns>
        public StationLoadSlice GetStationLoadSlice(bool isWinter, bool isHoliday, int year, int sectorStationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                    context.StationLoadSlice
                           .Include(s => s.StationLoadSliceDetail)
                           .FirstOrDefault(s => s.IsWinter == isWinter &&
                                                s.IsHoliday == isHoliday &&
                                                s.Year == year &&
                                                s.SectorStationID == sectorStationId)
                );
        }

        /// <summary>
        /// Метод, проверяющий наличие данных из ЭСАПР для определённого среза
        /// работы кассовых окон
        /// </summary>
        /// <param name="isWinter">Сезон - зимний или летний</param>
        /// <param name="isHoliday">Тип дней - праздники (и выходные) или будни</param>
        /// <param name="year">Год</param>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Флаг наличия данных из ЭСАПР для указанного среза работы кассовых окон</returns>
        public bool AreRecommendedData(bool isWinter, bool isHoliday, int year, int sectorStationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                    context.StationLoadSlice
                           .Any(s => s.IsWinter == isWinter &&
                                                s.IsHoliday == isHoliday &&
                                                s.Year == year &&
                                                s.SectorStationID == sectorStationId)
                );
        }

        /// <summary>
        /// Метод возвращающий все объекты расписаний работы кассовых окон станции
        /// </summary>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Список объектов расписаний работы кассовых окон станции</returns>
        public List<StationSchedule> GetAllStationSchedules(int sectorStationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                    context.StationSchedule
                           .Where(s => s.SectorStationID == sectorStationId)
                           .ToList()
                );
        }

        /// <summary>
        /// Метод, проверяющий наличие существования расписания работы кассовых окон
        /// </summary>
        /// <param name="isWinter">Флаг сезона - зимний или летний</param>
        /// <param name="year">Год</param>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Результат проверки существования расписания работы кассовых окон</returns>
        public bool DoesStationScheduleExist(bool isWinter, int year, int sectorStationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                    context.StationSchedule
                           .Any(s => s.IsWinter == isWinter &&
                                     s.Year == year &&
                                     s.SectorStationID == sectorStationId)
                );
        }
        
        /// <summary>
        /// Метод, добавляющий данные объекта расписания работы кассовых окон станции
        /// в базу данных
        /// </summary>
        /// <param name="stationSchedule">Объект расписания работы кассовых окон станции</param>
        public void AddStationSchedule(StationSchedule stationSchedule)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    context.StationSchedule.Add(stationSchedule);
                    context.SaveChanges();
                });
        }
        
        /// <summary>
        /// Метод, возвращающий идентификатор расписания работы кассовых окон станции
        /// </summary>
        /// <param name="isWinter">Сезон - зимний или летний</param>
        /// <param name="year">Год</param>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Идентификатор расписания работы кассовых окон станции</returns>
        public int GetStationScheduleId(bool isWinter, int year, int sectorStationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                    context.StationSchedule
                           .First(s => s.IsWinter == isWinter &&
                                       s.Year == year &&
                                       s.SectorStationID == sectorStationId)
                           .StationScheduleID
                );
        }

        /// <summary>
        /// Метод, проверяющий существование в базе содержания расписания работы кассовых окон
        /// </summary>
        /// <param name="stationScheduleId">Идентификаторв расписания работы кассовых окон</param>
        /// <param name="isHoliday">Тип дней - будни или выходные (праздники)</param>
        /// <returns>Результат проверки</returns>
        public bool AreStationScheduleDetails(int stationScheduleId, bool isHoliday)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                    context.StationScheduleDetail
                           .Any(s => s.StationScheduleID == stationScheduleId &&
                                     s.IsHoliday == isHoliday)
                );
        }

        /// <summary>
        /// Метод, добавляющий содержимое расписаний работы кассовых окон в базу
        /// </summary>
        /// <param name="stationScheduleId">Идентификатор расписания работы кассовых окон</param>
        /// <param name="details">Содержимое расписаний работы кассовых окон</param>
        public void AddDetailsForStationSchedule(int stationScheduleId, List<StationScheduleDetail> details)
        {
             GrokExceptionManager.ProcessGrokDb(context =>
                 {
                     var stationSchedule = context.StationSchedule
                                                  .FirstOrDefault(s => s.StationScheduleID == stationScheduleId);

                     if (stationSchedule != null)
                     {
                         details.ForEach(d => stationSchedule.StationScheduleDetail.Add(d));
                         context.SaveChanges();
                     }
                 });
        }       

        /// <summary>
        /// Метод, возвращающий содержимое расписания работы кассовых окон на указанный тип дней
        /// </summary>
        /// <param name="stationScheduleId">Идентификатор расписания работы кассовых окон</param>
        /// <param name="isHoliday">Тип дней - будни или выходные (праздники)</param>
        /// <returns>Содержимое расписания работы кассовых окон на указанный тип дней</returns>
        public List<StationScheduleDetail> GetStationScheduleDetails(int stationScheduleId, bool isHoliday)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.StationScheduleDetail
                                                             .Include(s => s.StationScheduleDetailHour)
                                                             .Include(s => s.TellerWindow)
                                                             .Where(
                                                                 s =>
                                                                 s.StationScheduleID == stationScheduleId &&
                                                                 s.IsHoliday == isHoliday)
                                                             .ToList()
                );
        }

        /// <summary>
        /// Метод, возвращающий содержимое среза работы кассовых окон
        /// </summary>
        /// <param name="isWinter">Сезон - зимний или летний</param>
        /// <param name="isHoliday">Тип дня - будни или праздники (выходные)</param>
        /// <param name="sliceDayTypeId">Тип дней</param>
        /// <param name="year">Год</param>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <param name="stationTellerWindows">Список кассовых окон станции</param>
        /// <returns>Содержимое среза работы кассовых окон</returns>
        public List<TellerWindowScheduleContentModel> GetTellerWindowScheduleModels(bool isWinter,
                                                                             bool isHoliday,
                                                                             int sliceDayTypeId,
                                                                             int year,
                                                                             int sectorStationId,
                                                                             Dictionary<int, string> stationTellerWindows)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {                    
                    var loadSlice = context.StationLoadSlice
                                           .Include(s => s.StationLoadSliceDetail)
                                           .FirstOrDefault(s => s.IsWinter == isWinter &&
                                                                s.IsHoliday == isHoliday &&
                                                                s.Year == year &&
                                                                s.SectorStationID == sectorStationId);

                    var stationSchedule = context.StationSchedule
                                                 .First(s => s.IsWinter == isWinter &&
                                                             s.Year == year &&
                                                             s.SectorStationID == sectorStationId);

                    if (!AreStationScheduleDetails(stationSchedule.StationScheduleID, isHoliday))
                    {
                        if (loadSlice != null)
                        {
                            FormStationScheduleDetails(loadSlice, stationSchedule, isHoliday, context);
                        }
                        else
                        {
                            FormEmptyStationScheduleDetails(isHoliday, stationSchedule, stationTellerWindows, context);
                        }                        
                    }                    

                    return stationSchedule.StationScheduleDetail
                                          .Where(s => s.IsHoliday == isHoliday)
                                          .Select(s => new TellerWindowScheduleContentModel()
                                          {
                                              TellerWindowScheduleContentModelId = s.StationScheduleDetailID,
                                              TellerWindowScheduleId = s.StationScheduleID,
                                              TellerWindowScheduleDayTypeId = sliceDayTypeId,
                                              TellerWindowId = s.TellerWindowID.HasValue
                                                                    ? s.TellerWindowID.Value
                                                                    : 0,
                                              TellerWindowNumber = s.TellerWindow != null &&
                                                                   s.TellerWindow.TellerWindowNumber != null
                                                                        ? s.TellerWindow.TellerWindowNumber.ToString()
                                                                        : string.Empty,
                                              HourTypes = s.StationScheduleDetailHour
                                                           .OrderBy(ss => ss.Hour)
                                                           .Select(ss => new HourTypeModel()
                                                           {
                                                               HourTypeId = ss.TellerWindowHourTypeID,
                                                               HourTypeCode = ((TellerWindowScheduleHourTypes)ss.TellerWindowHourTypeID).ToTextCode()
                                                           }).ToList(),
                                              DoesWorkByDefault = s.StationScheduleDetailHour
                                                                   .OrderBy(ss => ss.Hour)
                                                                   .Select(ss => (TellerWindowScheduleHourTypes)ss.TellerWindowHourTypeID == TellerWindowScheduleHourTypes.Work)
                                                                   .ToList()
                                          })
                                          .ToList();
                });
        }

        /// <summary>
        /// Метод, формирующий содержимое срезов работы кассовых окон
        /// по данным из ЭСАПРа
        /// </summary>
        /// <param name="stationLoadSlice">Срез работы кассовых окон станции</param>
        /// <param name="stationSchedule">Расписание работы кассовых окон станции</param>
        /// <param name="isHoliday">Тип дня - будни или праздники (выходные)</param>
        /// <param name="context">Entity Framework контекст</param>
        /// <returns>Содержимое срезов работы кассовых окон</returns>
        private void FormStationScheduleDetails(StationLoadSlice stationLoadSlice, StationSchedule stationSchedule, bool isHoliday, GrokDb context)
        {
            var maxTellerWindowCount =
                    stationLoadSlice.StationLoadSliceDetail.Select(t => t.TellerWindowCount).Max();
            
            for (var i = 0; i < maxTellerWindowCount; i++)
            {
                var stationScheduleDetail = new StationScheduleDetail()
                {
                    IsHoliday = isHoliday,
                    StationScheduleDetailHour = stationLoadSlice.StationLoadSliceDetail.Select((s, index) => new StationScheduleDetailHour()
                    {
                        Hour = (byte)index,
                        TellerWindowHourTypeID = i < s.TellerWindowCount
                                                         ? (byte)TellerWindowScheduleHourTypes.Work
                                                         : (byte)TellerWindowScheduleHourTypes.Rest
                    }).ToList()
                };

                stationSchedule.StationScheduleDetail.Add(stationScheduleDetail);
            }

            context.SaveChanges();
        }

        /// <summary>
        /// Метод, формирующий содержимое пустых срезов работы кассовых окон 
        /// при отсутствии данных из ЭСАПРа
        /// </summary>
        /// <param name="isHoliday">Тип дня - будни или праздники (выходные)</param>
        /// <param name="stationSchedule">Расписание работы кассовых окон станции</param>
        /// <param name="stationTellerWindows">Список кассовых окон станции</param>
        /// <param name="context">Entity Framework контекст</param>
        /// <returns>Содержимое срезов работы кассовых окон</returns>
        private void FormEmptyStationScheduleDetails(bool isHoliday, StationSchedule stationSchedule, Dictionary<int, string> stationTellerWindows, GrokDb context)
        {
            for (var i = 0; i < stationTellerWindows.Count; i++)
            {
                var stationScheduleDetail = new StationScheduleDetail()
                {
                    IsHoliday = isHoliday,
                    StationScheduleDetailHour = GenerateEmptyStationScheduleDetailHourList()
                };

                stationSchedule.StationScheduleDetail.Add(stationScheduleDetail);
            }

            context.SaveChanges();
        }

        /// <summary>
        /// Метод, формирующий список типов часов при отсутствии данных из ЭСАПР
        /// </summary>
        /// <returns>Список типов часов</returns>
        private List<StationScheduleDetailHour> GenerateEmptyStationScheduleDetailHourList()
        {
            var result = new List<StationScheduleDetailHour>();
            for (var i = 0; i < 24; i++)
            {
                result.Add(new StationScheduleDetailHour()
                {
                    Hour = (byte)i,
                    TellerWindowHourTypeID = (byte)TellerWindowScheduleHourTypes.Rest
                });
            }

            return result;
        }     

        /// <summary>
        /// Метод, обновляющий объект с содержимым расписания работы кассовых окон
        /// </summary>
        /// <param name="models">>Список моделей с содержимым срезов работы кассовых окон</param>
        public void UpdateStationScheduleDetails(List<TellerWindowScheduleContentModel> models)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    models.ForEach(m =>
                        {
                            var stationScheduleDetail = context.StationScheduleDetail
                                                               .Include(s => s.StationScheduleDetailHour)
                                                               .FirstOrDefault(
                                                                   s =>
                                                                   s.StationScheduleDetailID ==
                                                                   m.TellerWindowScheduleContentModelId);

                            if (stationScheduleDetail != null)
                            {
                                if (m.TellerWindowId > 0)
                                {
                                    stationScheduleDetail.TellerWindowID = m.TellerWindowId;
                                }
                                
                                stationScheduleDetail.StationScheduleDetailHour
                                                     .OrderBy(s => s.Hour)
                                                     .ToList()
                                                     .ForEach(s =>
                                                              s.TellerWindowHourTypeID =
                                                              (byte) m.HourTypes[s.Hour].HourTypeId);
                            }
                        });

                    context.SaveChanges();
                });
        }

    }
}
