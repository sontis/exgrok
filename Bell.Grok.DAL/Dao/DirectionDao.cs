﻿using System.Collections.Generic;
using System.Linq;
using Bell.Grok.Model;

namespace Bell.Grok.DAL.Dao
{
    /// <summary>
    /// Дао направления
    /// </summary>
    public class DirectionDao : BaseDao
    {
        /// <summary>
        /// Получить все направления
        /// </summary>
        /// <returns>Список направлений</returns>
        public List<DirectionModel> GetAllDirections()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var query = context.Direction.Select(d => new DirectionModel
                {
                    DirectionId = d.DirectionID,
                    DirectionNumber = d.DirectionNumber,
                    DirectionName = d.DirectionName
                });

                return query.ToList();
            });
        }

        /// <summary>
        /// Получить все направления в виде списка Value\Text
        /// </summary>
        /// <returns>Список Value\Text</returns>
        public Dictionary<int, string> GetAllDirectionSelectedListItems()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                context.Direction
                       .ToDictionary(d => d.DirectionID, d => d.DirectionName)                       
            );
        }

        /// <summary>
        /// Получить инфо по направлениям пользователя
        /// </summary>
        /// <param name="isCompanyAccessLevel">True - пользователь имеет доступ ко всем направлениям</param>
        /// <param name="directionIds">Список идентификаторов направлений пользователя</param>
        /// <returns>Список с инфо по направлениям пользователя</returns>
        public List<DirectionTreeModel> GetUserDirections(bool isCompanyAccessLevel, List<int?> directionIds)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var query = context.Direction.Where(d => d.DirectionName.ToUpper() != "НЕ ОПРЕДЕЛЕНО");
                if (!isCompanyAccessLevel)
                {
                    query = query.Where(d => directionIds.Contains(d.DirectionID));
                }

                var directions = query.Select(d => new DirectionTreeModel
                {
                    DirectionId = d.DirectionID,
                    DirectionName = d.DirectionName,
                    HasChildren = d.Sector.Any(s => s.DirectionID == d.DirectionID)
                }).ToList();

                return directions;
            });
        }

        /// <summary>
        /// Получить название направления
        /// </summary>
        /// <param name="directionId">Идентификатор направления</param>
        /// <returns>Название направления</returns>
        public string GetDirectionNameById(int directionId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var direction = context.Direction.SingleOrDefault(d => d.DirectionID == directionId);

                return direction == null
                           ? null
                           : direction.DirectionName;
            });
        }
    }
}
