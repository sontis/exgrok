﻿using System.Collections.Generic;
using System.Linq;
using Bell.Grok.Model;
using System;

namespace Bell.Grok.DAL.Dao
{
    /// <summary>
    /// Dao календаря
    /// </summary>
    public class CalendarDao
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CalendarModel> GetAll()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Calendar.Select(c => new CalendarModel()
            {
                Date = c.Date,
                IsHoliday = c.IsHoliday,
                WeekDayNumber = c.WeekdayNumber.HasValue
                                       ? c.WeekdayNumber.Value
                                       : 0,
                WeekDayName = c.WeekdayName,
                IsWeekend = c.IsWeekend
            }).ToList());
        }

        /// <summary>
        /// Присутствуют ли данные для указанного года в базе данных
        /// </summary>
        /// <param name="year">Год</param>
        /// <returns>True - данные на этот год имеются</returns>
        public bool IsYearInCalendar(int year)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var date = new DateTime(year, 1, 1);

                return context.Calendar.Select(c => c.Date).Contains(date);
            });
        }
    }
}
