﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.DAL.Dao
{
    public class ScheduleStatusHistoryDao : BaseDao
    {
        /// <summary>
        /// Получить сгруппированные идентификаторы направлений в графиках кассиров с данными статусами
        /// </summary>
        /// <param name="scheduleStatuses">Список фильтра по статусам графиков кассиров</param>
        /// <returns>Список идентификаторов направлений</returns>
        public List<int> GetDirectionsIds(List<int> scheduleStatuses)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                if ((scheduleStatuses != null) && (scheduleStatuses.Count > 0))
                {
                    var sshDirs = from q in
                                      (from ssh in context.ScheduleStatusHistory//.AsEnumerable()
                                       group ssh by ssh.ScheduleID into grp
                                       join sshR in context.ScheduleStatusHistory on grp.Max(t => t.ScheduleStatusHistoryID) equals sshR.ScheduleStatusHistoryID
                                       where scheduleStatuses.Contains(sshR.ScheduleStatusID)
                                       select new { DirId = sshR.Schedule.SectorStation.Sector.DirectionID }
                                      )
                                  group q by q.DirId into grp2
                                  select grp2.Key;
                    var sshDirections = sshDirs.ToList();
                    return sshDirections;
                }
                else
                {
                    var sshDirs = from q in context.Direction
                                  select q.DirectionID;

                    var sshDirections = sshDirs.ToList();
                    return sshDirections;
                }
            });
        }

    }
}
