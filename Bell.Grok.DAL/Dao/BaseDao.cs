﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.DAL.Dao
{
    /// <summary>
    /// Родитель всех DAO-объектов.
    /// </summary>
    public abstract class BaseDao
    {
        /// <summary>
        /// контекст edmx-модели
        /// </summary>
        protected GrokDb _context = new GrokDb();

    }
}
