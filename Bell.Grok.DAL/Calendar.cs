//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Calendar
    {
        public int CalendarID { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<int> WeekdayNumber { get; set; }
        public string WeekdayName { get; set; }
        public bool IsHoliday { get; set; }
        public bool IsWeekend { get; set; }
    }
}
