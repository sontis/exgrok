﻿using System;
using Bell.Grok.Services.Exceptions;

namespace Bell.Grok.DAL
{
    /// <summary>
    /// Обработчик исключений контекста DB.
    /// </summary>
    public static class GrokExceptionManager
    {
        /// <summary>
        /// Выполнение функции. Если возникло исключение, то обработка его в соответствии с политикой
        /// и генерация нового исключения RepositoryException
        /// </summary>
        /// <param name="action">выполняемая функция</param>
        private static void ProcessInner(Action action)
        {
            try
            {
                action();
            }
            catch (Exception exception)
            {
                if (AppExceptionManager.HandleException(exception, "RepositoryException"))
                {
                    throw new RepositoryException("Handled Exception", exception);
                }
            }
        }

        public delegate T ContextFunction<T>(GrokDb context);
        public delegate void ContextAction(GrokDb context);
        public delegate T Function<T>();

        public static T ProcessGrokDb<T>(ContextFunction<T> function)
        {

            T processResult = default(T);
            ProcessInner(() =>
            {
                using (var context = new GrokDb())
                {
                    processResult = function(context);
                }
            });
            return processResult;
        }

        public static void ProcessGrokDb(ContextAction action)
        {

            ProcessInner(() =>
            {
                using (var context = new GrokDb())
                {
                    action(context);
                }
            });
        }

        public static T Process<T>(Function<T> function)
        {

            T processResult = default(T);
            ProcessInner(() =>
            {
                processResult = function();
            });
            return processResult;
        }

        public static void Process(Action action)
        {
            ProcessInner(action);
        }
    }
}

