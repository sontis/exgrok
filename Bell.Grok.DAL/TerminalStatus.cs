//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class TerminalStatus
    {
        public TerminalStatus()
        {
            this.Terminal = new HashSet<Terminal>();
            this.TerminalHistory = new HashSet<TerminalHistory>();
        }
    
        public int TerminalStatusID { get; set; }
        public string TerminalStatusName { get; set; }
    
        public virtual ICollection<Terminal> Terminal { get; set; }
        public virtual ICollection<TerminalHistory> TerminalHistory { get; set; }
    }
}
