//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class RoleObject
    {
        public int RoleObjectID { get; set; }
        public int RoleID { get; set; }
        public int DataObjectID { get; set; }
        public int RoleObjectRightID { get; set; }
    
        public virtual DataObject DataObject { get; set; }
        public virtual Role Role { get; set; }
        public virtual RoleObjectRight RoleObjectRight { get; set; }
    }
}
