//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScheduleDetailDay
    {
        public int ScheduleDetailDayID { get; set; }
        public int ScheduleDetailID { get; set; }
        public System.DateTime Date { get; set; }
        public int DayTypeID { get; set; }
        public Nullable<float> TotalHours { get; set; }
        public Nullable<float> NightHours { get; set; }
        public bool IsSplitted { get; set; }
    
        public virtual DayType DayType { get; set; }
        public virtual ScheduleDetail ScheduleDetail { get; set; }
    }
}
