//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SectorStationTerminal
    {
        public SectorStationTerminal()
        {
            this.TellerWindowTerminal = new HashSet<TellerWindowTerminal>();
            this.TerminalHistory = new HashSet<TerminalHistory>();
        }
    
        public int SectorStationTerminalID { get; set; }
        public int SectorStationID { get; set; }
        public int TerminalID { get; set; }
        public System.DateTime BeginDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
    
        public virtual SectorStation SectorStation { get; set; }
        public virtual Terminal Terminal { get; set; }
        public virtual ICollection<TellerWindowTerminal> TellerWindowTerminal { get; set; }
        public virtual ICollection<TerminalHistory> TerminalHistory { get; set; }
    }
}
