﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bell.Grok.DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class GrokDb : DbContext
    {
        public GrokDb()
            : base("name=GrokDb")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<C1cEmployee> C1cEmployee { get; set; }
        public DbSet<AccessLevel> AccessLevel { get; set; }
        public DbSet<Agent> Agent { get; set; }
        public DbSet<DataObject> DataObject { get; set; }
        public DbSet<Direction> Direction { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<EmployeeShift> EmployeeShift { get; set; }
        public DbSet<EmployeeSpecialization> EmployeeSpecialization { get; set; }
        public DbSet<EmployeeStationPriority> EmployeeStationPriority { get; set; }
        public DbSet<Position> Position { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleObject> RoleObject { get; set; }
        public DbSet<RoleObjectRight> RoleObjectRight { get; set; }
        public DbSet<Sector> Sector { get; set; }
        public DbSet<SectorStation> SectorStation { get; set; }
        public DbSet<ShiftDutyRole> ShiftDutyRole { get; set; }
        public DbSet<Specialization> Specialization { get; set; }
        public DbSet<Station> Station { get; set; }
        public DbSet<TellerWindow> TellerWindow { get; set; }
        public DbSet<TellerWindowBreak> TellerWindowBreak { get; set; }
        public DbSet<TellerWindowBreakTime> TellerWindowBreakTime { get; set; }
        public DbSet<TellerWindowDetailHistory> TellerWindowDetailHistory { get; set; }
        public DbSet<TellerWindowOperationPeriod> TellerWindowOperationPeriod { get; set; }
        public DbSet<TellerWindowPlacement> TellerWindowPlacement { get; set; }
        public DbSet<TellerWindowShiftness> TellerWindowShiftness { get; set; }
        public DbSet<TellerWindowSpecialization> TellerWindowSpecialization { get; set; }
        public DbSet<TellerWindowStatus> TellerWindowStatus { get; set; }
        public DbSet<TellerWindowTerminal> TellerWindowTerminal { get; set; }
        public DbSet<TellerWindowWorkTime> TellerWindowWorkTime { get; set; }
        public DbSet<Terminal> Terminal { get; set; }
        public DbSet<TerminalBreak> TerminalBreak { get; set; }
        public DbSet<TerminalBreakTime> TerminalBreakTime { get; set; }
        public DbSet<TerminalModel> TerminalModel { get; set; }
        public DbSet<TerminalOperationPeriod> TerminalOperationPeriod { get; set; }
        public DbSet<TerminalPlacement> TerminalPlacement { get; set; }
        public DbSet<TerminalPurpose> TerminalPurpose { get; set; }
        public DbSet<TerminalPurposeHistory> TerminalPurposeHistory { get; set; }
        public DbSet<TerminalStatus> TerminalStatus { get; set; }
        public DbSet<TerminalType> TerminalType { get; set; }
        public DbSet<TerminalWorkRegime> TerminalWorkRegime { get; set; }
        public DbSet<TerminalWorkTime> TerminalWorkTime { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<VacationPeriod> VacationPeriod { get; set; }
        public DbSet<WeekDay> WeekDay { get; set; }
        public DbSet<TellerWindowPayment> TellerWindowPayment { get; set; }
        public DbSet<TerminalProfile> TerminalProfile { get; set; }
        public DbSet<EmployeeWorkTimePriority> EmployeeWorkTimePriority { get; set; }
        public DbSet<C1cDepartment> C1cDepartment { get; set; }
        public DbSet<SectorStationTerminal> SectorStationTerminal { get; set; }
        public DbSet<ProfessionalLevel> ProfessionalLevel { get; set; }
        public DbSet<UserRoleDirection> UserRoleDirection { get; set; }
        public DbSet<EmployeeProfessionalLevel> EmployeeProfessionalLevel { get; set; }
        public DbSet<TellerWindowSpecializationHistory> TellerWindowSpecializationHistory { get; set; }
        public DbSet<Calendar> Calendar { get; set; }
        public DbSet<DayType> DayType { get; set; }
        public DbSet<DirectionScheduleTemplate> DirectionScheduleTemplate { get; set; }
        public DbSet<ScheduleTemplate> ScheduleTemplate { get; set; }
        public DbSet<ScheduleTemplateDetail> ScheduleTemplateDetail { get; set; }
        public DbSet<ScheduleTemplateMode> ScheduleTemplateMode { get; set; }
        public DbSet<SectorScheduleTemplate> SectorScheduleTemplate { get; set; }
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<ScheduleStatus> ScheduleStatus { get; set; }
        public DbSet<ScheduleDetail> ScheduleDetail { get; set; }
        public DbSet<ScheduleDetailDay> ScheduleDetailDay { get; set; }
        public DbSet<ScheduleStatusHistory> ScheduleStatusHistory { get; set; }
        public DbSet<StationStandardLoadTW> StationStandardLoadTW { get; set; }
        public DbSet<StationPlannedRevenue> StationPlannedRevenue { get; set; }
        public DbSet<TerminalHistory> TerminalHistory { get; set; }
        public DbSet<StationEmployeeShift> StationEmployeeShift { get; set; }
        public DbSet<StationEmployeeShiftSchedule> StationEmployeeShiftSchedule { get; set; }
        public DbSet<TimeSheetMark> TimeSheetMark { get; set; }
        public DbSet<TimeSheet> TimeSheet { get; set; }
        public DbSet<TimeSheetDetail> TimeSheetDetail { get; set; }
        public DbSet<TimeSheetDetailDay> TimeSheetDetailDay { get; set; }
        public DbSet<TimeSheetFile> TimeSheetFile { get; set; }
        public DbSet<TotalByTimeSheetMark> TotalByTimeSheetMark { get; set; }
        public DbSet<TimeSheetMarkCategory> TimeSheetMarkCategory { get; set; }
        public DbSet<TimeSheetHistory> TimeSheetHistory { get; set; }
        public DbSet<ObjectVersion> ObjectVersion { get; set; }
        public DbSet<StationLoadSlice> StationLoadSlice { get; set; }
        public DbSet<StationLoadSliceDetail> StationLoadSliceDetail { get; set; }
        public DbSet<StationSchedule> StationSchedule { get; set; }
        public DbSet<StationScheduleDetail> StationScheduleDetail { get; set; }
        public DbSet<StationScheduleDetailHour> StationScheduleDetailHour { get; set; }
        public DbSet<StationScheduleStatus> StationScheduleStatus { get; set; }
        public DbSet<StationScheduleStatusHistory> StationScheduleStatusHistory { get; set; }
        public DbSet<TellerWindowHourType> TellerWindowHourType { get; set; }
        public DbSet<TellerWindowProfile> TellerWindowProfile { get; set; }
    }
}
