﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление результатов проверки сущствования терминалов
    /// </summary>
    public enum TerminalNumberValidationStatus
    {      
        Busy = 1,
        Exists = 2,
        Absent = 3,
        Invalid = 4
    }

    public static class TerminalNumberValidationStatusExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов 
        /// </summary>
        public static string ToText(this TerminalNumberValidationStatus terminalNumberValidationStatus)
        {
            switch (terminalNumberValidationStatus)
            {               
                case TerminalNumberValidationStatus.Busy:
                    return "Терминал с таким номером используется в указанный промежуток времени";
                case TerminalNumberValidationStatus.Exists:
                    return "Терминал существует, но не используется в указанный промежуток времени";
                case TerminalNumberValidationStatus.Absent:
                    return "Терминал отсутствует";
                case TerminalNumberValidationStatus.Invalid:
                    return "Некорректные данные";
                default:
                    return terminalNumberValidationStatus.ToString();
            }
        }
    }
}
