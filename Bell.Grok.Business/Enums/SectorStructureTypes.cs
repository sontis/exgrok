﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление типов структуры участка
    /// </summary>
    public enum SectorStructureTypes
    {
        Stations = 0,
        Cashiers = 1
    }

    
    public static class SectorStructureTypesExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления типов структуры участка
        /// </summary>
        public static string ToText(this SectorStructureTypes sectorStructureType)
        {
            switch (sectorStructureType)
            {
                case SectorStructureTypes.Stations:
                    return "Станции";
                case SectorStructureTypes.Cashiers:
                    return "Кассиры";
                default:
                    return sectorStructureType.ToString();
            }
        }
    }
}
