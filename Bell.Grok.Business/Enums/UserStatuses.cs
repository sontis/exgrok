﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление статусов пользователя
    /// </summary>
    public enum UserStatuses : byte
    {
        Blocked = 0,
        Active = 1        
    }
    
    public static class UserStatusesExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления статусов пользователя
        /// </summary>
        public static string ToText(this UserStatuses userStatus)
        {
            switch (userStatus)
            {
                case UserStatuses.Blocked:
                    return "Заблокирован";
                case UserStatuses.Active:
                    return "Активен";                
                default:
                    return userStatus.ToString();
            }
        }

        /// <summary>
        /// Метод, возвращающий элемент перечисления статусов пользователя по их строковому представлению
        /// </summary>
        /// <param name="userStatus">Строковое представление элемента перечисления статусов пользователя</param>
        /// <returns>Элемент перечисления статусов пользователя</returns>
        public static UserStatuses FromText(string userStatus)
        {
            if (string.Equals(userStatus, "Заблокирован", StringComparison.CurrentCultureIgnoreCase)) return UserStatuses.Blocked;
            else if (string.Equals(userStatus, "Активен", StringComparison.CurrentCultureIgnoreCase)) return UserStatuses.Active;            
            else
            {
                throw new InvalidCastException("Неизвестный статус пользователя: " + userStatus);
            }
        }
    }
}
