﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление профилей кассового окна
    /// </summary>
    public enum TellerWindowProfiles
    {
        Output = 1,
        Input = 2
    }
    
    public static class TellerWindowProfilesExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления профилей кассового окна
        /// </summary>        
        public static string ToText(this TellerWindowProfiles tellerWindowProfile)
        {
            switch (tellerWindowProfile)
            {
                case TellerWindowProfiles.Output:
                    return "На выход";
                case TellerWindowProfiles.Input:
                    return "На вход";
                default:
                    return tellerWindowProfile.ToString();
            }
        }
    }
}
