﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business.Enums
{
    /// <summary>
    /// Перечисление статусов графиков кассиров
    /// </summary>
    public enum ScheduleStatuses 
    {
        Undefined = 0,
        NotReconcile = 1,
        ReconciledBySectionChief = 2,
        ReconciledByProfessionalGroupOrganizer = 3,
        Reconciled = 4
    }

    public static class ScheduleStatusesExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления статусов графиков кассиров
        /// </summary>
        public static string ToText(this ScheduleStatuses scheduleStatuses)
        {
            switch (scheduleStatuses)
            {
                case ScheduleStatuses.NotReconcile:
                    return "Не согласован";
                case ScheduleStatuses.ReconciledBySectionChief:
                    return "На согласовании Начальником участка";
                case ScheduleStatuses.ReconciledByProfessionalGroupOrganizer:
                    return "На согласовании Профгрупоргом";
                case ScheduleStatuses.Reconciled:
                    return "Согласован";
                default:
                    return scheduleStatuses.ToString();
            }
        }
    }
}
