﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление месяцев года
    /// </summary>
    public enum Months
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        Jule = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }

    public static class MonthsExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление месяцев года
        /// </summary>
        public static string ToText(this Months month)
        {
            switch (month)
            {
                case Months.January:
                    return "Январь";
                case Months.February:
                    return "Февраль";
                case Months.March:
                    return "Март";
                case Months.April:
                    return "Апрель";
                case Months.May:
                    return "Май";
                case Months.June:
                    return "Июнь";
                case Months.Jule:
                    return "Июль";
                case Months.August:
                    return "Август";
                case Months.September:
                    return "Сентябрь";
                case Months.October:
                    return "Октябрь";
                case Months.November:
                    return "Ноябрь";
                case Months.December:
                    return "Декабрь";
                default:
                    return month.ToString();
            }
        }        
    }
}
