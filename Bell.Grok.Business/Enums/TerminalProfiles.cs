﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление профилей терминала
    /// </summary>
    public enum TerminalProfiles
    {
        Output = 1,
        Input = 2,
        Transit = 3
    }

    public static class TerminalProfilesExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления профилей терминала
        /// </summary>        
        public static string ToText(this TerminalProfiles terminalProfile)
        {
            switch (terminalProfile)
            {
                case TerminalProfiles.Output:
                    return "На выход";
                case TerminalProfiles.Input:
                    return "На вход";
                case TerminalProfiles.Transit:
                    return "Транзит";
                default:
                    return terminalProfile.ToString();
            }
        }
    }
}
