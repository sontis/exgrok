﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление уровней доступа
    /// </summary>
    public enum AccessLevels
    {
        Company = 1,
        Direction = 2,
        Sector = 3 
    }
    
    public static class AccessLevelsExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление элементов перечисления уровней доступа
        /// </summary>
        public static string ToText(this AccessLevels accessLevels)
        {
            switch (accessLevels)
            {
                case AccessLevels.Company:
                    return "Компания";
                case AccessLevels.Direction:
                    return "Направление";
                case AccessLevels.Sector:
                    return "Сектор";
                default:
                    return accessLevels.ToString();
            }
        }
    }
}
