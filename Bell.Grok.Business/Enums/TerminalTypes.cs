﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление для типов терминалов
    /// </summary>
    public enum TerminalTypes
    {
        Mktk = 1,
        Pktk = 2,
        Bpa = 3,
        AsuExpress = 4,
        Ipt = 5
    }

    public static class TerminalTypesExtensions
    {
        /// <summary>
        /// Метод, возвращающий текстовое представление типа терминала
        /// </summary>       
        public static string ToText(this TerminalTypes terminalType)
        {
            switch (terminalType)
            {
                case TerminalTypes.Mktk:
                    return "МКТК";
                case TerminalTypes.Pktk:
                    return "ПКТК";
                case TerminalTypes.Bpa:
                    return "БПА";
                case TerminalTypes.AsuExpress:
                    return "АСУ Экспресс";
                case TerminalTypes.Ipt:
                    return "ИПТ";
                default:
                    return terminalType.ToString();
            }
        }
    }
}
