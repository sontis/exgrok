﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Перечисление дней недели
    /// </summary>
    public enum WeekDays
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7,
        Holidays = 8
    }

    public static class WeekDaysExtensions
    {
        /// <summary>
        /// Метод, возвращающий атрибут name для input-ов нижней временной границы
        /// в окне редактирования временных интервалов приоритетов станций
        /// в зависимости от номера дня недели
        /// </summary>
        /// <param name="dayNumber">Номер дня недели</param>
        /// <returns>Значение атрибута name соответствующего input-а</returns>
        public static string BeginDateToString(this WeekDays dayNumber)
        {
            switch (dayNumber)
            {
                case WeekDays.Monday:
                    return "MondayBegin";
                case WeekDays.Tuesday:
                    return "TuesdayBegin";
                case WeekDays.Wednesday:
                    return "WednesdayBegin";
                case WeekDays.Thursday:
                    return "ThursdayBegin";
                case WeekDays.Friday:
                    return "FridayBegin";
                case WeekDays.Saturday:
                    return "SaturdayBegin";
                case WeekDays.Sunday:
                    return "SundayBegin";
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Метод, возвращающий атрибут name для input-ов верхней временной границы
        /// в окне редактирования временных интервалов приоритетов станций
        /// в зависимости от номера дня недели
        /// </summary>
        /// <param name="dayNumber">Номер дня недели</param>
        /// <returns>Значение атрибута name соответствующего input-а</returns>
        public static string EndDateToString(this WeekDays dayNumber)
        {
            switch (dayNumber)
            {
                case WeekDays.Monday:
                    return "MondayEnd";
                case WeekDays.Tuesday:
                    return "TuesdayEnd";
                case WeekDays.Wednesday:
                    return "WednesdayEnd";
                case WeekDays.Thursday:
                    return "ThursdayEnd";
                case WeekDays.Friday:
                    return "FridayEnd";
                case WeekDays.Saturday:
                    return "SaturdayEnd";
                case WeekDays.Sunday:
                    return "SundayEnd";
                default:
                    return string.Empty;
            }
        }


        /// <summary>
        /// Метод, возвращающий текстовое представление перечисления дней недели
        /// </summary>
        /// <param name="dayNumber">Номер дня недели</param>
        /// <returns>Текстовое представление перечисления дня недели</returns>
        public static string ToText(this WeekDays dayNumber)
        {
            switch (dayNumber)
            {
                case WeekDays.Monday:
                    return "понедельник";
                case WeekDays.Tuesday:
                    return "вторник";
                case WeekDays.Wednesday:
                    return "среда";
                case WeekDays.Thursday:
                    return "четверг";
                case WeekDays.Friday:
                    return "пятница";
                case WeekDays.Saturday:
                    return "суббота";
                case WeekDays.Sunday:
                    return "воскресенье";
                case WeekDays.Holidays:
                    return "праздничный день";
                default:
                    return string.Empty;
            }
        }
    }
}
