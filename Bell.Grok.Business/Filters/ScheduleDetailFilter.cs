﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Extensions;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public static class ScheduleDetailFilter
    {
        /// <summary>
        /// Метод, фильтрующий плановые графики кассиров по их полному имени
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<ScheduleDetail> FilterByEmployeeFullName(IQueryable<ScheduleDetail> query,
                                                                          string filterValue)
        {
            Expression<Func<ScheduleDetail, bool>> employeeFullNameFilter = e => (e.Employee.LastName + " " +
                                                                                  e.Employee.FirstName.Substring(0, 1) +
                                                                                  "." +
                                                                                  e.Employee.MiddleName.Substring(0, 1) +
                                                                                  ".")
                                                                                     .ToUpper()
                                                                                     .Contains(filterValue.ToUpper());

            return query.Where(employeeFullNameFilter);
        }

        /// <summary>
        /// Метод, фильтрующий плановые графики кассиров по названию их должности
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<ScheduleDetail> FilterByEmployeePosition(IQueryable<ScheduleDetail> query,
                                                                          string filterValue)
        {
            Expression<Func<ScheduleDetail, bool>> employeePositionFilter = e => e.Employee.Position.PositionName.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(employeePositionFilter);
        }

        /// <summary>
        /// Метод, фильтрующий плановые графики кассиров по их табельному номеру
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<ScheduleDetail> FilterByEmployeeNumber(IQueryable<ScheduleDetail> query,
                                                                        string filterValue)
        {
            Expression<Func<ScheduleDetail, bool>> employeeNumberFilter = e => e.Employee.EmployeeNumber.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(employeeNumberFilter);
        }
    }
}
