﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class ScheduleTemplateFilter
    {
        /// <summary>
        /// Метод, фильтрующий шаблоны графиков кассиров по времени действия
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="startPeriod">Начальная дата действия шаблона графика</param>
        /// /// <param name="endPeriod">Конечная дата действия шаблона графика</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<ScheduleTemplate> FilterByActionTime(IQueryable<ScheduleTemplate> query, DateTime startPeriod, DateTime endPeriod)
        {

            Expression<Func<ScheduleTemplate, bool>> actionTimePeriod = s => s.BeginDate < endPeriod &&
                                                                                  startPeriod < s.EndDate;

            return query.Where(actionTimePeriod);
        }       

        /// <summary>
        /// Метод, фильтрующий шаблоны графиков кассиров по их активности
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="showActivityMode">Режим просмотра (только активные, только неактивные, все)</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<ScheduleTemplate> FilterByActivity(IQueryable<ScheduleTemplate> query,
                                                                    string showActivityMode)
        {
            Expression<Func<ScheduleTemplate, bool>> activityMode;

            switch (showActivityMode)
            {
                case "Active":
                    activityMode = a => a.IsActive;
                    break;
                case "NonActive":
                    activityMode = a => !a.IsActive;
                    break;
                default:
                    activityMode = a => true;
                    break;
            }

            return query.Where(activityMode);
        }
    }
}
