﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class TellerWindowFilter
    {
        /// <summary>
        /// Метод, фильтрующий кассовые окна по их номерам
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<TellerWindow> FilterByTellerWindowNumbers(IQueryable<TellerWindow> query, string filterValue)
        {
            var tellerWindowNumbersStrings = filterValue.Split(',');
            var tellerWindowNumbers = new List<int>();
           
            foreach (var tellerWindowNumbersString in tellerWindowNumbersStrings)
            {
                tellerWindowNumbers.Add(int.Parse(tellerWindowNumbersString));
            }

            Expression<Func<TellerWindow, bool>> tellerWindowNumbersFilter = t =>  tellerWindowNumbers.Contains(t.TellerWindowID);

            return query.Where(tellerWindowNumbersFilter);
        }

        /// <summary>
        /// Метод, фильтрующий кассовые окна по начальной границе периода эксплуатации
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="minFilterValue">Начальная граница временного фильтра</param>
        /// <param name="maxFilterValue">Начальная граница временного фильтра</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<TellerWindow> FilterByBeginExploitationPeriod(IQueryable<TellerWindow> query,
                                                                                DateTime minFilterValue, DateTime maxFilterValue)
        {          
            Expression<Func<TellerWindow, bool>> beginExploitationPeriod =
                t => t.TellerWindowDetailHistory.FirstOrDefault().BeginDate <= maxFilterValue;

            return query.Where(beginExploitationPeriod);
        }

        /// <summary>
        /// Метод, фильтрующий кассовые окна по конечной границе периода эксплуатации
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="minFilterValue">Начальная граница временного фильтра</param>
        /// <param name="maxFilterValue">Начальная граница временного фильтра</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<TellerWindow> FilterByEndExploitationPeriod(IQueryable<TellerWindow> query,
                                                                                DateTime minFilterValue, DateTime maxFilterValue)
        {
            Expression<Func<TellerWindow, bool>> endExploitationPeriod =
                t => t.TellerWindowDetailHistory.FirstOrDefault().EndDate == null ||
                     t.TellerWindowDetailHistory.FirstOrDefault().EndDate.Value >= minFilterValue;

            return query.Where(endExploitationPeriod);
        }
    }
}
