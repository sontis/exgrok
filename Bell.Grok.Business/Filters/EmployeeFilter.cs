﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class EmployeeFilter
    {
        /// <summary>
        /// Метод, фильтрующий сотрудников по имени
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<Employee> FilterByFirstName(IQueryable<Employee> query, string filterValue)
        {
            Expression<Func<Employee, bool>> firstNameFilter = e => e.FirstName.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(firstNameFilter);
        }

        /// <summary>
        /// Метод, фильтрующий сотрудников по отчеству
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<Employee> FilterByMiddleName(IQueryable<Employee> query, string filterValue)
        {
            Expression<Func<Employee, bool>> middleNameFilter = e => e.MiddleName.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(middleNameFilter);
        }

        /// <summary>
        /// Метод, фильтрующий сотрудников по фамилии
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<Employee> FilterByLastName(IQueryable<Employee> query, string filterValue)
        {
            Expression<Func<Employee, bool>> lastNameFilter = e => e.LastName.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(lastNameFilter);
        }

        /// <summary>
        /// Метод, отбраковывающий сотрудников, входящих в определённый месячный табель
        /// </summary>
        /// <param name="query"></param>
        /// <param name="timesheetId"></param>
        /// <returns></returns>
        public static IQueryable<Employee> ExcludeTimesheetEmployees(IQueryable<Employee> query, int timesheetId)
        {
            Expression<Func<Employee, bool>> excludeTimesheetiFilter =
                e => e.TimeSheetDetail.All(t => t.TimeSheetID != timesheetId);                                                          

            return query.Where(excludeTimesheetiFilter);
        }
    }
}
