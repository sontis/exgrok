﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class TerminalFilter
    {
        /// <summary>
        /// Метод, фильтрующий терминалы по идентификатору
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<SectorStationTerminal> FilterByTerminalNumber(IQueryable<SectorStationTerminal> query, string filterValue)
        {
            var terminalNumbers = filterValue.Split(',')
                                             .Select(int.Parse)
                                             .ToList();            

            Expression<Func<SectorStationTerminal, bool>> terminalNumberFilter = s => terminalNumbers.Contains(s.TerminalID);

            return query.Where(terminalNumberFilter);
        }

        /// <summary>
        /// Метод, фильтрующий терминалы по типу
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<SectorStationTerminal> FilterByTerminalType(IQueryable<SectorStationTerminal> query, string filterValue)
        {
            var terminalTypes= filterValue.Split(',')
                                          .Select(int.Parse)
                                          .ToList();             

            Expression<Func<SectorStationTerminal, bool>> terminalTypesFilter = s => terminalTypes.Contains(s.Terminal.TerminalTypeID);

            return query.Where(terminalTypesFilter);
        }
        
        /// <summary>
        /// Метод, фильтрующий терминалы по текущей привязке к окнам
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр - список окон</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<SectorStationTerminal> FilterByCurrentTellerWindows(IQueryable<SectorStationTerminal> query, string filterValue)
        {
            var tellerWindows = filterValue.Split(',')
                                           .Select(int.Parse)
                                           .ToList();
            
            var now = DateTime.Now;
            var nowDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            Expression<Func<SectorStationTerminal, bool>> currentTellerWindows = s => tellerWindows.Contains(
                s.TellerWindowTerminal.Any(twt => twt.BeginDate <= nowDate && 
                                                                  (!twt.EndDate.HasValue || 
                                                                   twt.EndDate.Value >= nowDate)) 
                                                    ? s.TellerWindowTerminal
                                                       .FirstOrDefault(twt => twt.BeginDate <= nowDate && 
                                                                              (!twt.EndDate.HasValue || 
                                                                               twt.EndDate.Value >= nowDate)
                                                                       ).TellerWindowID 
                                                    : -1
            );

            return query.Where(currentTellerWindows);
        }

        /// <summary>
        /// Метод, фильтрующий терминалы по привязке к окнам (за любое время)
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр - список окон</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<SectorStationTerminal> FilterByTellerWindows(IQueryable<SectorStationTerminal> query, string filterValue)
        {            
            var tellerWindows = filterValue.Split(',')
                                           .Select(int.Parse)
                                           .ToList();

            Expression<Func<SectorStationTerminal, bool>> tellerWindowsFilter =
                s => s.TellerWindowTerminal.Any(twt => tellerWindows.Contains(twt.TellerWindowID));

            return query.Where(tellerWindowsFilter);
        }

        /// <summary>
        /// Метод, фильтрующий терминалы по начальной границе периода эксплуатации
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="minFilterValue">Начальная граница временного фильтра</param>
        /// <param name="maxFilterValue">Начальная граница временного фильтра</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<SectorStationTerminal> FilterByBeginExploitationPeriod(IQueryable<SectorStationTerminal> query,
                                                                           DateTime minFilterValue, DateTime maxFilterValue)
        {
            Expression<Func<SectorStationTerminal, bool>> beginExploitationPeriod =
                s =>
                minFilterValue <= s.BeginDate &&
                s.BeginDate <= maxFilterValue;

            return query.Where(beginExploitationPeriod);
        }

        /// <summary>
        /// Метод, фильтрующий терминалы по конечной границе периода эксплуатации
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="minFilterValue">Начальная граница временного фильтра</param>
        /// <param name="maxFilterValue">Начальная граница временного фильтра</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<SectorStationTerminal> FilterByEndExploitationPeriod(IQueryable<SectorStationTerminal> query,
                                                                         DateTime minFilterValue, DateTime maxFilterValue)
        {
            Expression<Func<SectorStationTerminal, bool>> endExploitationPeriod =
                s => s.EndDate.HasValue
                         ? minFilterValue <= s.EndDate.Value &&
                           s.EndDate.Value <= maxFilterValue
                         : minFilterValue <= (DateTime) SqlDateTime.MaxValue &&
                           (DateTime) SqlDateTime.MaxValue <= maxFilterValue;

            return query.Where(endExploitationPeriod);
        }

        /// <summary>
        /// Метод, фильтрующий только те терминалы, которые активны в данный момент
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<SectorStationTerminal> FilterOnlyCurrent(IQueryable<SectorStationTerminal> query)
        {
            var now = DateTime.Now;
            var nowDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            Expression<Func<SectorStationTerminal, bool>> currentPeriod =
                s => s.EndDate.HasValue
                         ? nowDate <= s.EndDate.Value &&
                           s.BeginDate <= nowDate
                         : nowDate <= (DateTime)SqlDateTime.MaxValue &&
                           s.BeginDate <= nowDate;

            return query.Where(currentPeriod);
        }
    }
}
