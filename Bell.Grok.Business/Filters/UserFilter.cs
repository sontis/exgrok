﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public static class UserFilter
    {
        /// <summary>
        /// Метод, фильтрующий пользователей по имени
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<User> FilterByFirstName(IQueryable<User> query, string filterValue)
        {
            Expression<Func<User, bool>> firstNameFilter = u => u.FirstName.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(firstNameFilter);
        }

        /// <summary>
        /// Метод, фильтрующий пользователей по отчеству
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<User> FilterByMiddleName(IQueryable<User> query, string filterValue)
        {
            Expression<Func<User, bool>> middleNameFilter = u => u.MiddleName.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(middleNameFilter);
        }

        /// <summary>
        /// Метод, фильтрующий пользователей по фамилии
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<User> FilterByLastName(IQueryable<User> query, string filterValue)
        {
            Expression<Func<User, bool>> lastNameFilter = u => u.LastName.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(lastNameFilter);
        }
    }
}
