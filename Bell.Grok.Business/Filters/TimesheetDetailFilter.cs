﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Extensions;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public static class TimesheetDetailFilter
    {
        /// <summary>
        /// Метод, фильтрующий месячные табели сотрудников по их ФИО
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<TimeSheetDetail> FilterByEmployeeFullName(IQueryable<TimeSheetDetail> query, string filterValue)
        {
            Expression<Func<TimeSheetDetail, bool>> employeeFullNameFilter = e => (e.Employee.LastName + " " +
                                                                                  e.Employee.FirstName.Substring(0, 1) +
                                                                                  "." +
                                                                                  e.Employee.MiddleName.Substring(0, 1) +
                                                                                  ".")
                                                                                    .ToUpper().Contains(filterValue.ToUpper());

            return query.Where(employeeFullNameFilter);
        }

        /// <summary>
        /// Метод, фильтрующий месячные табели сотрудников по их табельнмоу номеру (фильтруется как строка)
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="filterValue">Значение-фильтр</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр фильтрации</returns>
        public static IQueryable<TimeSheetDetail> FilterByEmployeeNumber(IQueryable<TimeSheetDetail> query,
                                                                         string filterValue)
        {
            Expression<Func<TimeSheetDetail, bool>> employeeNumberFilter = t => t.Employee.EmployeeNumber.ToUpper().Contains(filterValue.ToUpper());

            return query.Where(employeeNumberFilter);
        }
    }
}
