﻿using Bell.Grok.Business.Enums;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using Bell.Grok.Business.Extensions;
using Bell.Grok.Services.Logging;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий по работе с графиками
    /// </summary>
    public class ScheduleRepository
    {
        /// <summary>
        /// Формат времени для конвертации значения DateTime в строку 
        /// </summary>
        private const string TimeFormat = "dd.MM.yyyy";

        private const string StationObjectName = "Station";

        /// <summary>
        /// Получение всех графиков
        /// </summary>
        /// <returns>Список всех графиков</returns>
        public List<ScheduleRowModel> GetAllSchedules(int month, int year, int sectorStationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var resultList = new List<ScheduleRowModel>();

                    var schedule = context.Schedule
                                          .FirstOrDefault(
                                              sch =>
                                              sch.Month == month && sch.Year == year &&
                                              sch.SectorStationID == sectorStationId);

                    if (schedule != null)
                    {                                                
                        var approveDate = GetApproveDate(schedule);

                        schedule.ScheduleDetail
                                .ToList()
                                .FillScheduleRowModelList(resultList, approveDate, TimeFormat);
                    }

                    return resultList;
                });
        }

        /// <summary>
        /// Перегрузка выборки плановых графиков с учётом постраничного просмотра
        /// </summary>
        /// <param name="month">Месяц</param>
        /// <param name="year">Год</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="page">Колчество графиков на странице</param>        
        /// <param name="pageSize">Размер страницы таблицы с графиками</param>
        /// <param name="sort">Список (из 1 элемента) с параметрами сортировки</param>
        /// <param name="filter">Коллекция параметров фильтрации</param>
        /// <param name="itemsCount">Общее количество графиков</param>
        /// <returns>Список графиков для указанной страницы</returns>
        public List<ScheduleRowModel> GetAllSchedules(int month, int year, int sectorId, int stationId,
                                                      int page, int pageSize, List<SortDescription> sort, FilterContainer filter, 
                                                      out int itemsCount)
        {
            var itemsCountInner = 0;

            var result = GrokExceptionManager.ProcessGrokDb(context =>
            {
                var resultList = new List<ScheduleRowModel>();

                var filterFunctions = filter != null
                                               ? GetFilterFunctions(filter)
                                               : new List<Func<IQueryable<ScheduleDetail>, IQueryable<ScheduleDetail>>>();
                var orderFunction = GetOrderFunction(sort);

                var scheduleQuery = context.ScheduleDetail
                                        .Include(s => s.Schedule)
                                        .Include(s => s.Schedule.SectorStation)
                                        .Include(s => s.ScheduleDetailDay)
                                        .Include(s => s.Employee)
                                        .Include(s => s.Employee.Position)
                                        .Include(s => s.TellerWindow)                                        
                                        .Include(s => s.Schedule.ScheduleStatusHistory)
                                        .Include(s => s.Schedule.ScheduleStatusHistory.Select(ss => ss.ScheduleStatus))
                                        .Include(s => s.Employee)
                                        .Where(
                                          sch =>
                                          sch.Schedule.Month == month &&
                                          sch.Schedule.Year == year &&
                                          sch.Schedule.SectorStation.SectorID == sectorId &&
                                          sch.Schedule.SectorStation.StationID == stationId)                                        
                                        .AsQueryable();

                foreach (var filterFunction in filterFunctions)
                {
                    scheduleQuery = filterFunction(scheduleQuery);
                }
                scheduleQuery = orderFunction(scheduleQuery).AsQueryable();         

                var schedules = new PagedList<ScheduleDetail>(scheduleQuery, page, pageSize);
                itemsCountInner = schedules.ItemCount;

                if (schedules.Any())
                {                    
                    var approveDate = GetApproveDate(schedules.First().Schedule);
                    schedules.FillScheduleRowModelList(resultList, approveDate, TimeFormat);
                }

                return resultList;
            });

            itemsCount = itemsCountInner;
            return result;
        }        

        /// <summary>
        /// Получение всех статусов расписаний
        /// </summary>
        /// <returns>Список всех статусов расписаний</returns>
        public Dictionary<byte, string> GetAllScheduleStatus()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.ScheduleStatus
                                                                        .ToDictionary(s => s.ScheduleStatusId,
                                                                                      s => s.Name));
        }

        /// <summary>
        /// Метод возвращает расписание работы кассовых окон
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="month">Текущий месяц</param>
        /// <param name="year">Текущий год</param>
        /// <returns>Расписание работы кассовых окон станции</returns>
        public List<TellerWindowScheduleRowModel> GetAllTellerWindowSchedules(int sectorId, int stationId, int month, int year)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var result = new List<TellerWindowScheduleRowModel>();

                    var schedule = context.Schedule
                                          .Include(s => s.SectorStation)
                                          .Include(s => s.SectorStation.Station)
                                          .Include(s => s.SectorStation.Station.StationEmployeeShift)
                                          .Include(
                                              s =>
                                              s.SectorStation.Station.StationEmployeeShift.Select(
                                                  ss => ss.StationEmployeeShiftSchedule))
                                          .Include(s =>                                              
                                              s.SectorStation.Station.StationEmployeeShift.Select(
                                                  ss => ss.StationEmployeeShiftSchedule.Select(
                                                      e => e.WeekDay)))
                                          .Include(s => s.ScheduleDetail)
                                          .Include(s => s.ScheduleDetail.Select(t => t.TellerWindow))
                                          .FirstOrDefault(s => s.SectorStation.SectorID == sectorId &&
                                                               s.SectorStation.StationID == stationId &&
                                                               s.Month == month &&
                                                               s.Year == year);

                    if (schedule != null)
                    {
                        var windows = schedule.ScheduleDetail                                              
                                              .Select(s => s.TellerWindow)   
                                              .Distinct()
                                              .OrderBy(s => s.TellerWindowNumber)                                              
                                              .ToList();

                        var tellerWindowScheduleRowModel = new TellerWindowScheduleRowModel();
                        if (windows.Any())
                        {
                            var resultNumber = new StringBuilder(windows[0].TellerWindowNumber.ToString());
                            for (var i = 1; i < windows.Count; i++)
                            {
                                resultNumber.Append(", ");
                                resultNumber.Append(windows[i].TellerWindowNumber.ToString());
                            }

                            tellerWindowScheduleRowModel.Numbers = resultNumber.ToString();
                        }

                        var shifts = schedule.SectorStation.Station.StationEmployeeShift.ToList();

                        for (int i = 0; i < 8; i++)
                        {
                            var shiftSchedules = shifts.SelectMany(s => s.StationEmployeeShiftSchedule)
                                                       .Where(e => e.WeekDay.WeekDayNumber == i + 1)
                                                       .OrderBy(e => e.StationEmployeeShift.Name)
                                                       .ToList();                                                      

                            var resultString = new StringBuilder();

                            foreach (var shiftSchedule in shiftSchedules)
                            {
                                if (shiftSchedule.BeginTime != shiftSchedule.EndTime)
                                {
                                    resultString.Append(shiftSchedule.StationEmployeeShift.Name);
                                    resultString.Append("см ");
                                    resultString.Append(shiftSchedule.BeginTime.ToString("hh\\:mm"));
                                    resultString.Append("-");
                                    resultString.Append(shiftSchedule.EndTime.ToString("hh\\:mm"));

                                    if (shiftSchedule.BreakBeginTime != shiftSchedule.BreakEndTime)
                                    {
                                        resultString.Append(" обед ");
                                        resultString.Append(shiftSchedule.BreakBeginTime.ToString("hh\\:mm"));
                                        resultString.Append("-");
                                        resultString.Append(shiftSchedule.BreakEndTime.ToString("hh\\:mm"));
                                        resultString.Append(" ");
                                    }
                                    resultString.Append(";");
                                }
                            }

                            tellerWindowScheduleRowModel.DaySchedules[i] = resultString.ToString();
                        }

                        result.Add(tellerWindowScheduleRowModel);
                    }
                    
                    return result;
                });
        }


        /// <summary>
        /// Удаляет график работы кассира
        /// </summary>
        /// <param name="scheduleDetailId">Идентификатор графика работы кассира</param>
        /// <param name="userName">Имя пользователя</param>
        public void DeleteSchedule(int scheduleDetailId, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var schedule =
                        context.ScheduleDetail.Include(e => e.Schedule)
                               .FirstOrDefault(sch => sch.ScheduleDetailID == scheduleDetailId);
                    if (schedule != null)
                    {
                        var employeeRepository = new EmployeeRepository();
                        var changes = new Dictionary<string, string>();
                        changes.Add("Employee",
                                    string.Format("{0}({1})", employeeRepository.GetFullNameById(schedule.EmployeeID),
                                                  schedule.EmployeeID));
                        changes.Add("Date", string.Format("{0} {1}", schedule.Schedule.Month, schedule.Schedule.Year));

                        context.ScheduleDetail.Remove(schedule);
                        context.SaveChanges();

                        #region Logging

                        GrokLogger.LogScheduleDelete(scheduleDetailId.ToString(), changes, userName);

                        #endregion
                    }
                });
        }

        /// <summary>
        /// Добавляет плановый график кассира билетного
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона граффика работы</param>
        /// <param name="month">Месяц назначения</param>
        /// <param name="year">Год назначения</param>
        /// <param name="sectorId">Идентификатор Направления</param>
        /// <param name="stationId">Идентификатор Станции</param>
        /// <param name="tellerWindowId">Идентификатор Кассового окна</param>
        /// <param name="employeeId">Идентификатор работника</param>
        /// <param name="userId"></param>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>ФИО работника</returns>
        public void AddSchedule(int scheduleTemplateId, byte month, short year, int sectorId, int stationId, int tellerWindowId, int employeeId, int userId, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var scheduleTemlate =
                    context.ScheduleTemplate.FirstOrDefault(e => e.ScheduleTemplateID == scheduleTemplateId);

                if (scheduleTemlate != null)
                {

                    var sectorStation =
                        context.SectorStation.First(e => ((e.SectorID == sectorId) && (e.StationID == stationId)));
                    var isSheduleExists =
                        context.Schedule.Any(s => s.SectorStationID == sectorStation.SectorStationID &&
                                                  s.Month == month &&
                                                  s.Year == year);

                    Schedule schedule;

                    if (isSheduleExists)
                    {
                        schedule =
                            context.Schedule.SingleOrDefault(s => s.SectorStationID == sectorStation.SectorStationID &&
                                                                  s.Month == month &&
                                                                  s.Year == year);
                    }
                    else
                    {
                        var scheduleStatusHistory = new ScheduleStatusHistory()
                            {
                                BeginDate = DateTime.Now,
                                UserID = userId,
                                ScheduleStatusID = (byte)ScheduleStatuses.NotReconcile
                            };
                        ChangeStationVersion(context);

                        schedule = new Schedule();
                        schedule.SectorStationID = sectorStation.SectorStationID;
                        schedule.Month = month;
                        schedule.Year = year;
                        schedule.ScheduleStatusHistory.Add(scheduleStatusHistory);

                        context.Schedule.Add(schedule);
                    }

                    var scheduleDetail = new ScheduleDetail();
                    scheduleDetail.EmployeeID = employeeId;
                    scheduleDetail.TellerWindowID = tellerWindowId;
                    scheduleDetail.ScheduleTemplateDetailID = scheduleTemlate
                                                                    .ScheduleTemplateMode
                                                                    .First()
                                                                    .ScheduleTemplateDetail
                                                                    .First()
                                                                    .ScheduleTemplateDetailID;

                    var scheduleTemplateDetails =
                        scheduleTemlate.ScheduleTemplateMode.First()
                                       .ScheduleTemplateDetail.Where(e => (e.Month == month) && (e.Year == year));


                    foreach (var templateDetail in scheduleTemplateDetails)
                    {
                        var scheduleDetailDay = new ScheduleDetailDay();

                        scheduleDetailDay.Date = new DateTime(templateDetail.Year, templateDetail.Month, templateDetail.Date.Day);
                        scheduleDetailDay.TotalHours = (templateDetail.DayWorkingHours.HasValue ? templateDetail.DayWorkingHours.Value : 0);
                        scheduleDetailDay.DayType = templateDetail.DayType;
                        scheduleDetailDay.NightHours = templateDetail.NightWorkingHours.HasValue
                                                           ? templateDetail.NightWorkingHours.Value
                                                           : 0;
                        scheduleDetailDay.IsSplitted = templateDetail.IsSplitted;

                        scheduleDetail.TotalHours += scheduleDetailDay.TotalHours.Value;
                        scheduleDetail.NightHours += templateDetail.NightWorkingHours.HasValue
                                          ? templateDetail.NightWorkingHours.Value
                                          : 0;

                        if (templateDetail.DayType.DayTypeID == (int)Bell.Grok.Model.DayType.HD)
                        {
                            scheduleDetail.HolidayHours += templateDetail.DayWorkingHours.HasValue
                                                                ? templateDetail.DayWorkingHours.Value
                                                                : 0;
                        }

                        scheduleDetail.ScheduleDetailDay.Add(scheduleDetailDay);
                    }

                    if (schedule != null)
                    {
                        schedule.ScheduleDetail.Add(scheduleDetail);

                        context.SaveChanges();

                        #region Logging
                        var stationRepository = new StationRepository();
                        var sectorRepository = new SectorRepository();
                        var scheduleTemplateRepository = new ScheduleTemplateRepository();
                        var employeeRepository = new EmployeeRepository();
                        var tellerWindowRepository = new TellerWindowRepository();
                        var changes = new Dictionary<string, string>();
                        var employee = employeeRepository.GetFullNameById(employeeId);
                        changes.Add("Employee", string.Format("{0}({1})", employee, employeeId));
                        changes.Add("Date", string.Format("{0} {1}", month, year));
                        changes.Add("SectorId", string.Format("{0}({1})", sectorRepository.GetSectorModelById(sectorId).SectorName, sectorId));
                        changes.Add("StationId", string.Format("{0}({1})", stationRepository.GetStationNameById(stationId), stationId));
                        changes.Add("TellerWindow", string.Format("{0}({1})", tellerWindowRepository.GetTellerWindowById(tellerWindowId).TellerWindowNumber, tellerWindowId));
                        changes.Add("ScheduleTemlateId", string.Format("{0}({1})", scheduleTemplateRepository.GetScheduleTemplateById(scheduleTemplateId).FullName, scheduleTemplateId));

                        GrokLogger.LogScheduleCreate("new", changes, userName);
                        #endregion
                    }
                }
            });
        }

        /// <summary>
        /// Метод, переводящий график кассиров из статуса "Не согласовано" в статус "На согласовании начальником участка"
        /// </summary>
        /// <param name="scheduleId">Идентификатор группы графиков станции на определённый месяц</param>
        /// <param name="userId">Идентификатор пользователя, отправившего график на согласование</param>
        /// <param name="userName">Имя пользователя</param>
        public void SendScheduleToReconcile(int scheduleId, int userId, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var schedule =
                    context.Schedule.SingleOrDefault(s => s.ScheduleID == scheduleId);

                if (schedule != null)
                {
                    var scheduleStatusHistory = new ScheduleStatusHistory()
                        {
                            BeginDate = DateTime.Now,
                            ScheduleStatusID = (byte)ScheduleStatuses.ReconciledBySectionChief,
                            UserID = userId
                        };
                    ChangeStationVersion(context);

                    schedule.ScheduleStatusHistory.Add(scheduleStatusHistory);

                    context.SaveChanges();

                    #region Logging
                    GrokLogger.LogScheduleEdit(scheduleId.ToString(), new Dictionary<string, string> { { "ScheduleStatus", "На согласовании начальником участка" }, { "Date", string.Format("{0} {1}", schedule.Month, schedule.Year) } }, userName);
                    #endregion
                }
            });

        }


        /// <summary>
        /// Метод, переводящий график кассиров из статуса "На согласовании начальником участка" в статус "На согласовании профгруппором"
        /// </summary>
        /// <param name="scheduleId">Идентификатор группы графиков станции на определённый месяц</param>
        /// <param name="userId">Идентификатор пользователя, согласовавшего график</param>
        /// <param name="userName">Имя пользователя</param>
        public void ConfirmReconciledScheduleBySectionChief(int scheduleId, int userId, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var schedule =
                    context.Schedule.SingleOrDefault(s => s.ScheduleID == scheduleId);

                if (schedule != null)
                {
                    var scheduleStatusHistory = new ScheduleStatusHistory()
                    {
                        BeginDate = DateTime.Now,
                        ScheduleStatusID = (byte)ScheduleStatuses.ReconciledByProfessionalGroupOrganizer,
                        UserID = userId
                    };
                    ChangeStationVersion(context);

                    var changes = GetStatusChanges(schedule, scheduleStatusHistory);

                    schedule.ScheduleStatusHistory.Add(scheduleStatusHistory);

                    context.SaveChanges();

                    #region Logging
                    GrokLogger.LogScheduleEdit(scheduleId.ToString(), new Dictionary<string, string> { { "ScheduleStatus", "На согласовании профгруппоргом" }, { "Date", string.Format("{0} {1}", schedule.Month, schedule.Year) } }, userName);
                    #endregion
                }
            });
        }

        /// <summary>
        /// Метод, переводящий график кассиров из статуса "На согласовании профгруппором" в статус "Согласован"
        /// </summary>
        /// <param name="scheduleId">Идентификатор группы графиков станции на определённый месяц</param>
        /// <param name="userId">Идентификатор пользователя, согласовавшего график</param>
        /// <param name="userName">Имя пользователя</param>
        public void ConfirmReconcileByProfessionalGroupOrginizer(int scheduleId, int userId, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var schedule =
                    context.Schedule.SingleOrDefault(s => s.ScheduleID == scheduleId);

                if (schedule != null)
                {
                    var scheduleStatusHistory = new ScheduleStatusHistory()
                    {
                        BeginDate = DateTime.Now,
                        ScheduleStatusID = (byte)ScheduleStatuses.Reconciled,
                        UserID = userId
                    };
                    ChangeStationVersion(context);

                    var changes = GetStatusChanges(schedule, scheduleStatusHistory);

                    schedule.ScheduleStatusHistory.Add(scheduleStatusHistory);

                    context.SaveChanges();

                    #region Logging
                    GrokLogger.LogScheduleEdit(scheduleId.ToString(), new Dictionary<string, string> { { "ScheduleStatus", "Согласован" }, { "Date", string.Format("{0} {1}", schedule.Month, schedule.Year) } }, userName);
                    #endregion
                }
            });
        }

        /// <summary>
        /// Метод, переводящий график кассиров в статус Не согласовано
        /// </summary>
        /// <param name="scheduleId">Идентификатор группы графиков станции на определённый месяц</param>
        /// <param name="userId">Идентификатор пользователя, отменивший согласование графика</param>
        /// <param name="userName">Имя пользователя</param>
        public void CancelReconcileSchedule(int scheduleId, int userId, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var schedule =
                    context.Schedule.SingleOrDefault(s => s.ScheduleID == scheduleId);

                if (schedule != null)
                {
                    var scheduleStatusHistory = new ScheduleStatusHistory()
                    {
                        BeginDate = DateTime.Now,
                        ScheduleStatusID = (byte)ScheduleStatuses.NotReconcile,
                        UserID = userId
                    };
                    ChangeStationVersion(context);

                    var changes = GetStatusChanges(schedule, scheduleStatusHistory);

                    schedule.ScheduleStatusHistory.Add(scheduleStatusHistory);

                    context.SaveChanges();

                    #region Logging
                    GrokLogger.LogScheduleEdit(scheduleId.ToString(), new Dictionary<string, string> { { "ScheduleStatus", "Отмена согласования" }, { "Date", string.Format("{0} {1}", schedule.Month, schedule.Year) } }, userName);
                    #endregion
                }
            });
        }

        private static Dictionary<string, string> GetStatusChanges(Schedule schedule, ScheduleStatusHistory scheduleStatusHistory)
        {
            ScheduleStatusHistory lastStatus = null;
            foreach (var statusHistory in schedule.ScheduleStatusHistory)
            {
                if ((lastStatus == null) || (statusHistory.BeginDate > lastStatus.BeginDate)) lastStatus = statusHistory;
            }

            var changes = new Dictionary<string, string>
                {
                    {"Month",schedule.Month.ToString()},
                    {"Year",schedule.Year.ToString()},
                    {
                        "scheduleStatusHistory",
                        string.Format("Changed from <{0}> to <{1}>",
                                      lastStatus == null ? -1 : lastStatus.ScheduleStatusID,
                                      scheduleStatusHistory.ScheduleStatusID)
                    }
                };
            return changes;
        }

        /// <summary>
        /// Метод, проверяющий наличие расписаний с неактивными шаблонами
        /// </summary>
        /// <param name="scheduleId">Идентификатор группы расписаний (для определённой станции на определённый месяц)</param>
        /// <returns>Статус в виде строки, если на станции есть расписания на основе неактивных шаблонов,
        ///          и null, если все расписания составлены на основе активных шаблонов</returns>
        public string CheckScheduleTemplatesActivity(int scheduleId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var schedule =
                    context.Schedule
                                .Include(s => s.ScheduleDetail)
                                .Include(s => s.ScheduleDetail.Select(sd => sd.Employee))
                                .Include(s => s.ScheduleDetail.Select(sd => sd.ScheduleTemplateDetail.ScheduleTemplateMode.ScheduleTemplate))
                                .FirstOrDefault(s => s.ScheduleID == scheduleId);


                if (schedule != null)
                {
                    if (
                        schedule.ScheduleDetail.Any(
                            sd => !sd.ScheduleTemplateDetail.ScheduleTemplateMode.ScheduleTemplate.IsActive))
                    {
                        var scheduleDetailWithNonActiveTemplate = schedule.ScheduleDetail.First(
                            sd => !sd.ScheduleTemplateDetail.ScheduleTemplateMode.ScheduleTemplate.IsActive);

                        return "Кассир " +
                               scheduleDetailWithNonActiveTemplate.Employee.CombineFullName() +
                               " имеет расписание на основе неактивного шаблона \"" +
                               scheduleDetailWithNonActiveTemplate.ScheduleTemplateDetail.ScheduleTemplateMode
                                                                  .ScheduleTemplate.ShortName + "\"";
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return "Не найдена информация про группу расписаний данной станции на текущий месяц";
                }
            });
        }

        /// <summary>
        /// Метод, проверяющий существование единственного графика кассира для определённого кассира
        /// на период указанного месяца
        /// </summary>        
        /// <param name="employeeId">Идентификатор сотрудника-кассира</param>
        /// <param name="year">Год</param>
        /// <param name="month">Месяц</param>
        /// <returns>Признак, существует ли единственный график кассира для определённого кассира
        ///          на период указанного месяца</returns>
        public bool IsScheduleTemplateBoundToEmployee(int employeeId, int year, int month)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.ScheduleDetail.Count(s => s.EmployeeID == employeeId &&
                                                                                             s.Schedule.Year == year &&
                                                                                             s.Schedule.Month == month) == 1);
        }

        /// <summary>
        /// Метод, возвращающий название станции для расписания определённого сотрудника на определённый месяц
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <param name="year">Год</param>
        /// <param name="month">Месяц</param>
        /// <returns>Название станции</returns>
        public string GetStationNameByDetailId(int employeeId, int year, int month)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var scheduleDetail = context.ScheduleDetail.FirstOrDefault(s => s.EmployeeID == employeeId &&
                                                                                    s.Schedule.Year == year &&
                                                                                    s.Schedule.Month == month);
                    if (scheduleDetail != null)
                    {
                        return scheduleDetail.Schedule.SectorStation.Station.StationName;
                    }
                    else
                    {
                        return string.Empty;
                    }
                });
        }

        /// <summary>
        /// Метод, возвращающий текущий статус графиков определённого месяца определённой станции
        /// </summary>
        /// <param name="scheduleId">Идентификатор группы графиков станции на определённый месяц</param>
        /// <returns>Текущий статус графиков определённого месяца определённой станции</returns>
        public ScheduleStatuses GetScheduleStatus(int scheduleId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var schedule = context.Schedule.FirstOrDefault(s => s.ScheduleID == scheduleId);

                if (schedule != null && context.ScheduleStatusHistory.Any(s => s.ScheduleID == scheduleId))
                {
                    var maxBeginDateForScheduleId =
                        context.ScheduleStatusHistory.Where(ss => ss.ScheduleID == scheduleId).Max(ss => ss.BeginDate);

                    var scheduleStatusHistory = schedule.ScheduleStatusHistory.FirstOrDefault(
                        ss => ss.ScheduleID == scheduleId && ss.BeginDate == maxBeginDateForScheduleId);

                    return scheduleStatusHistory != null
                               ? (ScheduleStatuses)scheduleStatusHistory.ScheduleStatusID
                               : ScheduleStatuses.Undefined;
                }
                else
                {
                    return ScheduleStatuses.Undefined;
                }
            });
        }

        /// <summary>
        /// Метод, возвращающий текущий статус графиков определённого месяца определённой станции (перегрузка)
        /// </summary>
        /// <param name="schedule">Группа графиков станции на определённый месяц</param>
        /// <returns>Текущий статус графиков определённого месяца определённой станции</returns>
        internal ScheduleStatuses GetScheduleStatus(Schedule schedule)
        {
            if (schedule != null && schedule.ScheduleStatusHistory.Any())
            {
                var maxBeginDateForScheduleId =
                    schedule.ScheduleStatusHistory.Max(ss => ss.BeginDate);

                var scheduleStatusHistory = schedule.ScheduleStatusHistory
                                                    .FirstOrDefault(ss => ss.BeginDate == maxBeginDateForScheduleId);

                return scheduleStatusHistory != null
                           ? (ScheduleStatuses)scheduleStatusHistory.ScheduleStatusID
                           : ScheduleStatuses.Undefined;
            }
            else
            {
                return ScheduleStatuses.Undefined;
            }
        }

        /// <summary>
        /// Метод, формирующий объект модели данных для экспорта графиков кассиров станции на определённый месяц
        /// </summary>
        /// <param name="scheduleId">Идентификатор группы графиков станции на определённый месяц</param>
        /// <param name="userId">Идентификатор пользователя, совершающего экспорт данных в файл</param>
        /// <returns>Объект модели данных для экспорта графиков кассиров станции на определённый месяц</returns>
        public ScheduleExportModel GetExportData(int scheduleId, int userId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var schedule = context.Schedule
                                        .Include(s => s.ScheduleDetail)
                                        .Include(s => s.SectorStation.Sector)
                                        .Include(s => s.SectorStation.Station)
                                        .FirstOrDefault(s => s.ScheduleID == scheduleId);

                var user = context.User.FirstOrDefault(u => u.UserID == userId);

                if (schedule != null)
                {
                    var sectorChiefRoleName = Roles.SectorHead.ToText();
                    var groupOrganizerName = Roles.ProfessionalGroupOrganizer.ToText();
                    const string undefinedName = "ФИО";

                    //Если график согласован, то определяем начальника участка и профгруппорга по истории согласований
                    var sectorChief = GetSectorChiefOrGroupOrginizerFromStatusHistory(scheduleId, ScheduleStatuses.ReconciledByProfessionalGroupOrganizer);
                    var groupOrganizer = GetSectorChiefOrGroupOrginizerFromStatusHistory(scheduleId, ScheduleStatuses.Reconciled);

                    if (sectorChief == null)
                    {
                        //Предполагаем, что у участка есть только один начальник
                        var sectorChiefUserRole = context.UserRole
                                                         .Include(ur => ur.User)
                                                         .FirstOrDefault(
                                                             u => u.SectorID == schedule.SectorStation.SectorID &&
                                                                  u.Role.RoleName == sectorChiefRoleName);

                        if (sectorChiefUserRole != null)
                        {
                            sectorChief = sectorChiefUserRole.User;
                        }
                    }

                    if (groupOrganizer == null)
                    {
                        //Предполагаем, что у участка есть только один профгруппорг
                        var groupOrganizerUserRole = context.UserRole
                                                            .Include(ur => ur.User)
                                                            .FirstOrDefault(
                                                                u => u.SectorID == schedule.SectorStation.SectorID &&
                                                                     u.Role.RoleName == groupOrganizerName);

                        if (groupOrganizerUserRole != null)
                        {
                            groupOrganizer = groupOrganizerUserRole.User;
                        }
                    }



                    var model = new ScheduleExportModel();
                    model.SectorName = schedule.SectorStation.Sector.SectorName;
                    model.StationName = schedule.SectorStation.Station.StationName;
                    model.ChiefName = sectorChief != null
                                          ? sectorChief.CombineUserName()
                                          : undefinedName;
                    model.GroupOrganizerName = groupOrganizer != null
                                                   ? groupOrganizer.CombineUserName()
                                                   : undefinedName;
                    model.Month = ((Months)schedule.Month).ToText();
                    model.Year = schedule.Year.ToString();
                    model.Rows = GetAllSchedules(schedule.Month, schedule.Year, schedule.SectorStationID);
                    var reconcileDate = model.Rows.FirstOrDefault(e => !string.IsNullOrWhiteSpace(e.ApprovalDate));
                    model.ReconcileDate = reconcileDate == null ? "" : reconcileDate.ApprovalDate;
                    model.TwsRows = GetAllTellerWindowSchedules(schedule.SectorStation.SectorID,
                                                                schedule.SectorStation.StationID, schedule.Month,
                                                                schedule.Year);
                    model.Implementer = user != null
                                            ? user.CombineUserName()
                                            : string.Empty;

                    return model;
                }
                else
                {
                   return null;
                }
            });
        }

        /// <summary>
        /// Возвращает пользователя-начальника участка, который делал согласование
        /// </summary>
        /// <param name="scheduleId">Идентификатор группы графиков станции на определённый месяц</param>
        /// <param name="status">Статус согласования, по которому можно определить кого искать: начальника участка или профгруппорга</param>
        /// <returns>Объект сущности User</returns>
        private User GetSectorChiefOrGroupOrginizerFromStatusHistory(int scheduleId, ScheduleStatuses status)
        {
            var currentStatus = GetScheduleStatus(scheduleId);

            if (currentStatus == ScheduleStatuses.Undefined ||
                currentStatus == ScheduleStatuses.NotReconcile)
            {
                return null;
            }
            else
            {
                return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var schedule = context.Schedule.FirstOrDefault(s => s.ScheduleID == scheduleId);

                    if (schedule != null && context.ScheduleStatusHistory.Any(s => s.ScheduleID == scheduleId &&
                                                                              s.ScheduleStatusID == (int)status))
                    {
                        var maxBeginDateForScheduleReconciledBySectorChief =
                            context.ScheduleStatusHistory.Where(ss => ss.ScheduleID == scheduleId &&
                                                                      ss.ScheduleStatusID == (int)status).Max(ss => ss.BeginDate);

                        var scheduleStatusHistory = schedule.ScheduleStatusHistory.FirstOrDefault(
                            ss => ss.ScheduleID == scheduleId &&
                                  ss.ScheduleStatusID == (int)status &&
                                  ss.BeginDate == maxBeginDateForScheduleReconciledBySectorChief);

                        return scheduleStatusHistory != null
                                   ? scheduleStatusHistory.User
                                   : null;
                    }
                    else
                    {
                        return null;
                    }
                });
            }
        }

        /// <summary>
        /// Метод, возвращающий дату согласования графика
        /// </summary>
        /// <param name="schedule">Группа графиков станции на определённый месяц</param>
        /// <returns>Дата согласования графика</returns>
        private DateTime? GetApproveDate(Schedule schedule)
        {
            var currentStatus = GetScheduleStatus(schedule);

            if (currentStatus != ScheduleStatuses.Reconciled)
            {
                return null;
            }
            else
            {
                if (schedule != null && schedule.ScheduleStatusHistory.Any())
                {
                    var maxBeginDateForScheduleReconcile = schedule.ScheduleStatusHistory
                                                                   .Where(ss => ss.ScheduleStatusID == (int)ScheduleStatuses.Reconciled)
                                                                   .Max(ss => ss.BeginDate);

                    return maxBeginDateForScheduleReconcile;
                }
                else
                {
                    return null;
                }                
            }
        }        

        /// <summary>
        /// Метод, возвращающий расписание по идентификатору
        /// </summary>
        /// <param name="scheduleId">Идентификатор расписания</param>
        /// <returns>Плановое расписание</returns>
        public Schedule GetScheduleById(int scheduleId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var schedule = context.Schedule.FirstOrDefault(sch => sch.ScheduleID == scheduleId);
                    return schedule;
                });
        }

        /// <summary>
        /// Метод, возвращающий планы графиков кассиров за определённый месяц
        /// для указанного участка
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="month">Месяц</param>
        /// <returns>Список объектов entity-класса ScheduleDetail</returns>
        internal List<ScheduleDetail> GetSchedulesBySectorAndMonth(int sectorId, SimpleMonthModel month)
        {
            using (var context = new GrokDb())
            {
                var sectorStationIds = context.SectorStation.Where(ss => ss.SectorID == sectorId).Select(ss => ss.SectorStationID);

                var scheduleDetails = context
                                    .ScheduleDetail
                                    .Include(sd => sd.Schedule)
                                    .Include(sd => sd.ScheduleDetailDay)
                                    .Where(s => sectorStationIds.Contains(s.Schedule.SectorStationID) &&
                                                            s.Schedule.Year == month.Year &&
                                                            s.Schedule.Month == month.Month);

                return scheduleDetails.ToList();
            }
        }

        /// <summary>
        /// Метод, возвращающий функцию сортировки
        /// </summary>
        /// <param name="sort">Параметры сортировки</param>
        /// <returns>Функция сортировки</returns>
        private Func<IQueryable<ScheduleDetail>, IOrderedQueryable<ScheduleDetail>> GetOrderFunction(List<SortDescription> sort)
        {
            var sortDescription = sort[0];

            switch (sortDescription.Field)
            {
                case "EmployeeName":
                    return query => ScheduleDetailSorter.OrderByEmployeeFullName(query, sortDescription.Dir);
                case "EmployeePosition":
                    return query => ScheduleDetailSorter.OrderByPosition(query, sortDescription.Dir);
                case "EmployeeNumber":
                    return query => ScheduleDetailSorter.OrderByEmployeeNumber(query, sortDescription.Dir);
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field +
                                                    "\' не поддерживается");
            }
        }
        /// <summary>
        /// Метод, возвращающий список функций фильтрации
        /// </summary>
        /// <param name="filter">Объект, содержащий параметры фильтрации</param>
        /// <returns>Список функций фильтрации</returns>
        private List<Func<IQueryable<ScheduleDetail>, IQueryable<ScheduleDetail>>> GetFilterFunctions(FilterContainer filter)
        {
            var queries = new List<Func<IQueryable<ScheduleDetail>, IQueryable<ScheduleDetail>>>();

            if (filter.filters != null)
            {
                foreach (var filterItem in filter.filters)
                {
                    var searchValue = filterItem.value;

                    if (filterItem.field == "EmployeeName")
                    {
                        queries.Add(query => ScheduleDetailFilter.FilterByEmployeeFullName(query, searchValue));
                    }

                    if (filterItem.field == "EmployeePosition")
                    {
                        queries.Add(query => ScheduleDetailFilter.FilterByEmployeePosition(query, searchValue));
                    }

                    if (filterItem.field == "EmployeeNumber")
                    {
                        queries.Add(query => ScheduleDetailFilter.FilterByEmployeeNumber(query, searchValue));
                    }
                }
            }

            return queries;
        }

        /// <summary>
        /// Метод, формирующий новый идентификатор состояния запроса списка станций
        /// при изменении статуса согласования плановых графиков работы кассовых окон
        /// </summary>
        /// <param name="context">Объект соединения с базой данных</param>
        private void ChangeStationVersion(GrokDb context)
        {
            var stationObjectVersion = context.ObjectVersion
                                              .FirstOrDefault(o => o.ObjectName.Equals(StationObjectName, StringComparison.OrdinalIgnoreCase));

            if (stationObjectVersion == null)
            {
                context.ObjectVersion.Add(new ObjectVersion()
                {
                    ObjectName = StationObjectName,
                    Version = Guid.NewGuid()
                });
            }
            else
            {
                stationObjectVersion.Version = Guid.NewGuid();
            }
        }
    }
}
