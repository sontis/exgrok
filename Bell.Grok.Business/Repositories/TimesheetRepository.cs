﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Data.Entity;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using Bell.Grok.Business.Extensions;
using Bell.Grok.Services.Logging;

namespace Bell.Grok.Business
{
    public class TimesheetRepository
    {
        /// <summary>
        /// Формат времени для конвертации значения DateTime в строку 
        /// </summary>
        private const string TimeFormat = "dd.MM.yyyy";

        /// <summary>
        /// Метод, проверяющий, составлен ли график на определённый метод
        /// для указанного участка
        /// </summary>
        /// <param name="month">Месяц</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Истина, если график составлен, и ложь в противном случае</returns>
        public bool IsTimesheetForMonth(SimpleMonthModel month, int sectorId)
        {
            using (var context = new GrokDb())
            {
                return context.TimeSheet.Any(t => t.SectorID == sectorId &&
                                                  t.Year == month.Year &&
                                                  t.Month == month.Month);
            }
        }

        /// <summary>
        /// Метод, составляющий ли график на определённый метод
        /// для указанного участка
        /// </summary>
        /// <param name="month">Месяц</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="userName">Имя пользователя, который инициировал эту операцию</param>
        public void FormTimesheet(SimpleMonthModel month, int sectorId, string userName)
        {
            using (var context = new GrokDb())
            {
                var timesheet = new TimeSheet();
                timesheet.SectorID = sectorId;
                timesheet.Year = (short)month.Year;
                timesheet.Month = (byte)month.Month;

                var date = new DateTime(month.Year, month.Month, 1);
                var employees = context.Employee.Where(e => e.SectorID == sectorId &&
                                                       e.ContractEffectiveFrom <= date &&
                                                       (!e.ContractEffectiveTo.HasValue || date <= e.ContractEffectiveTo)).ToList();

                foreach (var employee in employees)
                {
                    var timesheetDetail = new TimeSheetDetail();
                    timesheetDetail.Init(timesheet, employee.EmployeeID);
                }

                context.TimeSheet.Add(timesheet);
                context.SaveChanges();

                #region Logging
                var employeeRepository = new EmployeeRepository();
                GrokLogger.LogTimesheetCreate("new", new Dictionary<string, string>
                    {
                        {"Period month",timesheet.Month.ToString(CultureInfo.InvariantCulture)},
                        {"Period year",timesheet.Year.ToString(CultureInfo.InvariantCulture)},
                    }, userName);
                #endregion
            }
        }

        /// <summary>
        /// Метод, добавляющий сотрудника в месячный табель участка
        /// </summary>
        /// <param name="timesheetId">Идентификатор месячного табеля участка</param>
        /// <param name="employeeId">Идентификатор сотрудника, которого надо добавить в табель</param>
        /// <param name="userName">Имя пользователя, который инициировал эту операцию</param>
        public void AddEmployeeToTimesheet(int timesheetId, int employeeId, string userName)
        {
            using (var context = new GrokDb())
            {
                var timesheet = context.TimeSheet
                                       .FirstOrDefault(t => t.TimeSheetID == timesheetId);

                if (timesheet != null)
                {
                    var timesheetDetail = new TimeSheetDetail();
                    timesheetDetail.Init(timesheet, employeeId);

                    context.SaveChanges();

                    #region Logging
                    var employeeRepository = new EmployeeRepository();
                    GrokLogger.LogTimesheetEmployeeAdd(timesheetDetail.TimeSheetID.ToString(CultureInfo.InvariantCulture), new Dictionary<string, string>
                    {
                        {"TimeSheetDetailID",timesheetDetail.TimeSheetDetailID.ToString(CultureInfo.InvariantCulture)},
                        {"Employee",employeeRepository.GetById(employeeId).CombineFullName()},
                        {"EmployeeId",employeeId.ToString(CultureInfo.InvariantCulture)},
                        {"Period month",timesheet.Month.ToString(CultureInfo.InvariantCulture)},
                        {"Period year",timesheet.Year.ToString(CultureInfo.InvariantCulture)},
                    }, userName);
                    #endregion
                }
            }
        }

        /// <summary>
        /// Метод, удаляющий сотрудника из месячного табеля участка
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор месячного табеля участка</param>
        /// <param name="userName">Имя пользователя, который инициировал эту операцию</param>
        public void DeleteEmployeeFromTimesheet(int timesheetDetailId, string userName)
        {
            using (var context = new GrokDb())
            {
                var timesheetDetail = context.TimeSheetDetail.Include(e => e.Employee).Include(e => e.TimeSheet)
                                             .FirstOrDefault(t => t.TimeSheetDetailID == timesheetDetailId);

                if (timesheetDetail != null)
                {
                    #region Logging
                    var toLog = new Dictionary<string, string>
                        {
                            {"Employee", timesheetDetail.Employee.CombineFullName()},
                            {"EmployeeId", timesheetDetail.Employee.EmployeeID.ToString(CultureInfo.InvariantCulture)},
                            {"Period month", timesheetDetail.TimeSheet.Month.ToString(CultureInfo.InvariantCulture)},
                            {"Period year", timesheetDetail.TimeSheet.Year.ToString(CultureInfo.InvariantCulture)},
                        };
                    #endregion

                    while (timesheetDetail.TimeSheetDetailDay.Count > 0)
                    {
                        var timesheetDetailDay = timesheetDetail.TimeSheetDetailDay.ToList()[0];

                        timesheetDetail.TimeSheetDetailDay.Remove(timesheetDetailDay);

                        context.TimeSheetDetailDay.Remove(timesheetDetailDay);
                    }

                    while (timesheetDetail.TotalByTimeSheetMark.Count > 0)
                    {
                        var totalByTimeSheetMark = timesheetDetail.TotalByTimeSheetMark.ToList()[0];

                        timesheetDetail.TotalByTimeSheetMark.Remove(totalByTimeSheetMark);

                        context.TotalByTimeSheetMark.Remove(totalByTimeSheetMark);
                    }

                    context.TimeSheetDetail.Remove(timesheetDetail);

                    context.SaveChanges();

                    #region Logging
                    GrokLogger.LogTimesheetEmployeeDelete(timesheetDetail.TimeSheetID.ToString(CultureInfo.InvariantCulture), toLog, userName);
                    #endregion
                }
            }
        }

        /// <summary>
        /// Привязка файла к табелю
        /// </summary>
        /// <param name="sectorId">Идентификатор сектора табеля</param>
        /// <param name="month">Месяц табеля</param>
        /// <param name="year">Год табеля</param>
        /// <param name="name">Название файла</param>
        /// <param name="fileName">Имя файла</param>
        /// <param name="fileExtention">расширение файла</param>
        /// <param name="file">Поток файла</param>
        public void UploadFile(int sectorId, int month, int year, string name, string fileName, string fileExtention, Stream file)
        {
            using (var context = new GrokDb())
            {
                var timeSheet = context.TimeSheet.First(e => (e.SectorID == sectorId) && (e.Month == month) && (e.Year == year));

                var timeSheetFile = context.TimeSheetFile.Create();

                timeSheetFile.TimeSheet = timeSheet;
                timeSheetFile.Name = name;
                timeSheetFile.FileName = fileName;
                timeSheetFile.FileExt = fileExtention;
                timeSheetFile.FileStreamID = Guid.NewGuid();//todo ??? not used

                var buffer = new byte[16 * 1024];
                using (var ms = new MemoryStream())
                {
                    int read;
                    while ((read = file.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    timeSheetFile.FileData = ms.ToArray();
                }

                context.TimeSheetFile.Add(timeSheetFile);
                context.SaveChanges();

            }
        }

        /// <summary>
        /// Удаление файла привязанного к табелю
        /// </summary>
        /// <param name="fileId">Идентификатор файла</param>
        public void RemoveFile(int fileId)
        {
            using (var context = new GrokDb())
            {
                var file = context.TimeSheetFile.First(e => e.TimeSheetFileID == fileId);
                context.TimeSheetFile.Remove(file);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Загрузка привязанного к табелю файла
        /// </summary>
        /// <param name="fileId">Идентификатор файла</param>
        /// <returns>Файл</returns>
        public TimesheetFileModel LoadFile(int fileId)
        {
            using (var context = new GrokDb())
            {
                var timesheetFile = context.TimeSheetFile
                                           .First(e => e.TimeSheetFileID == fileId);

                return new TimesheetFileModel()
                    {
                        Data = timesheetFile.FileData,
                        Name = timesheetFile.FileName,
                        Extension = timesheetFile.FileExt
                    };
            }
        }

        /// <summary>
        /// Загрузка списка привязанных к табелю файлов
        /// </summary>
        /// <param name="sectorId">Идентификатор сектора табеля</param>
        /// <param name="month">Месяц табеля</param>
        /// <param name="year">Год табеля</param>
        /// <param name="page">Страница</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <param name="count">Общее количество файлов</param>
        /// <returns>Список файлов</returns>
        public List<TimesheetFileInformationModel> GetFiles(int sectorId, int month, int year, int page, int pageSize, out int count)
        {
            using (var context = new GrokDb())
            {
                var timeSheet = context.TimeSheet.FirstOrDefault(e => (e.SectorID == sectorId) && (e.Month == month) && (e.Year == year));

                if (timeSheet != null)
                {
                    count = context.TimeSheetFile
                                   .Count(e => e.TimeSheetID == timeSheet.TimeSheetID);

                    return context.TimeSheetFile
                                  .Where(e => e.TimeSheetID == timeSheet.TimeSheetID)
                                  .OrderBy(e => e.TimeSheetFileID)
                                  .Skip((page - 1) * pageSize)
                                  .Take(pageSize)
                                  .Select(e => new TimesheetFileInformationModel()
                                      {
                                          fileId = e.TimeSheetFileID, 
                                          name = e.Name
                                      })
                                  .ToList();
                }
                else
                {
                    count = 0;

                    return new List<TimesheetFileInformationModel>();
                }

            }
        }

        /// <summary>
        /// Метод, возвращающий отсортированный и отфильтрованный список табелей участка на определённый месяц для таблицы
        /// </summary>
        /// <param name="page">Страница грида</param>
        /// <param name="pageSize">Количество элементов на странице грида</param>
        /// <param name="sort">Параметры сортировки</param>
        /// <param name="filter">Параметры фильтра</param>
        /// <param name="month">Месяц</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="itemsCount">Общее количество табелей в гриде (то есть на определённом участке за указанный месяц)</param>
        /// <returns>Список объектов TimesheetRowModel</returns>
        public List<TimesheetRowModel> GetTimesheets(int page, int pageSize, List<SortDescription> sort, FilterContainer filter, SimpleMonthModel month, int sectorId, out int itemsCount)
        {
            var filterFunctions = filter != null
                                               ? GetFilterFunctions(filter)
                                               : new List<Func<IQueryable<TimeSheetDetail>, IQueryable<TimeSheetDetail>>>();
            var orderFunction = GetTimesheetDetailOrderFunction(sort);

            using (var context = new GrokDb())
            {
                var query = context.TimeSheetDetail
                                        .Include(t => t.TimeSheet)
                                        .Include(t => t.Employee)
                                        .Include(t => t.Employee.Position)
                                        .Include(t => t.Employee.ScheduleDetail)
                                        .Include(t => t.Employee.ScheduleDetail.Select(sd => sd.Schedule))
                                        .Where(t => t.TimeSheet.SectorID == sectorId &&
                                                    t.TimeSheet.Year == month.Year &&
                                                    t.TimeSheet.Month == month.Month)                                        
                                        .AsQueryable();
                foreach (var filterFunction in filterFunctions)
                {
                    query = filterFunction(query);
                }
                query = orderFunction(query).AsQueryable();               

                var pagedList = new PagedList<TimeSheetDetail>(query, page, pageSize);
                itemsCount = pagedList.ItemCount;

                var data = pagedList.Select(p => new TimesheetRowModel()
                    {
                        TimesheetDetailId = p.TimeSheetDetailID,
                        TimesheetId = p.TimeSheetID,
                        EmployeeFullName = p.Employee.CombineFullName(),
                        EmployeeNumber = p.Employee.EmployeeNumber,
                        EmployeePosition = p.Employee.Position.PositionName,
                        FirstHalfMonthDays = p.FirstHalfMonthDays,
                        FirstHalfMonthHours = p.FirstHalfMonthHours,
                        FirstHalfMonthNightHours = p.FirstHalfMonthNightHours,
                        SecondHalfMonthDays = p.SecondHalfMonthDays,
                        SecondHalfMonthHours = p.SecondHalfMonthHours,
                        SecondHalfMonthNightHours = p.SecondHalfMonthNightHours,
                        TotalMonthDays = p.TotalMonthDays,
                        TotalMonthHours = p.TotalMonthHours,
                        TotalMonthNightHours = p.TotalMonthNightHours,
                        IsExistSchedule = p.Employee.ScheduleDetail.Any(s => s.Schedule.Year == month.Year &&
                                                                             s.Schedule.Month == month.Month)                        
                    });

                return data.ToList();
            }
        }

        /// <summary>
        /// Метод, возвращающий идентификатор месячного табеля по станции
        /// </summary>
        /// <param name="month">Месяц</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Идентификатор месячного табеля по станции</returns>
        public int GetTimesheetId(SimpleMonthModel month, int sectorId)
        {
            using (var context = new GrokDb())
            {
                var timesheet = context.TimeSheet.FirstOrDefault(t => t.SectorID == sectorId &&
                                                                      t.Year == month.Year &&
                                                                      t.Month == month.Month);
                return timesheet == null
                           ? -1
                           : timesheet.TimeSheetID;
            }
        }

        /// <summary>
        /// Метод, генерирующий табели для кассиров, у которых есть плановые графики за указанный месяц
        /// </summary>        
        /// <param name="month">Месяц</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="isPrepayment">Флаг, показывающий половину месяца (первая, то есть аванс, или вторая, то есть полный табель)</param>
        public void GenerateHalfTimesheet(SimpleMonthModel month, int sectorId, bool isPrepayment)
        {
            var scheduleRepository = new ScheduleRepository();

            using (var context = new GrokDb())
            {
                var timesheet = context
                                    .TimeSheet
                                    .Include(t => t.TimeSheetDetail)
                                    .Include(t => t.TimeSheetDetail.Select(td => td.TimeSheetDetailDay))
                                    .FirstOrDefault(t => t.SectorID == sectorId &&
                                                    t.Month == month.Month &&
                                                    t.Year == month.Year);

                if (timesheet != null)
                {
                    var scheduleDetails = scheduleRepository.GetSchedulesBySectorAndMonth(sectorId, month);

                    foreach (var timeSheetDetail in timesheet.TimeSheetDetail)
                    {
                        var scheduleDetail =
                            scheduleDetails.FirstOrDefault(s => s.EmployeeID == timeSheetDetail.EmployeeID);

                        if (scheduleDetail != null)
                        {
                            var payMarks = GetAllPayMarks();
                            timeSheetDetail.FillFromScheduleDetail(scheduleDetail, payMarks, isPrepayment, context);
                        }
                    }

                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Метод, очищающий вторые половины табелей для кассиров, у которых есть плановые графики за указанный месяц
        /// </summary>        
        /// <param name="month">Месяц</param>
        /// <param name="sectorId">Идентификатор участка</param>
        public void ClearSecondTimesheetPart(SimpleMonthModel month, int sectorId)
        {
            using (var context = new GrokDb())
            {
                var timesheet = context
                                    .TimeSheet
                                    .Include(t => t.TimeSheetDetail)
                                    .Include(t => t.TimeSheetDetail.Select(td => td.TimeSheetDetailDay))
                                    .FirstOrDefault(t => t.SectorID == sectorId &&
                                                    t.Month == month.Month &&
                                                    t.Year == month.Year);

                if (timesheet != null)
                {
                    foreach (var timeSheetDetail in timesheet.TimeSheetDetail)
                    {                       
                        var timesheetDetailDays = timeSheetDetail.TimeSheetDetailDay.Where(t => t.Date.Day > 15);

                        foreach (var timesheetDetailDay in timesheetDetailDays)
                        {
                            timesheetDetailDay.TimeSheetMarkID = null;
                            timesheetDetailDay.TotalHours = null;
                            timesheetDetailDay.NightHours = null;
                        }

                        var payMarks = GetAllPayMarks();
                        timeSheetDetail.CalculatePayrollsAndHookies(payMarks, context);
                        timeSheetDetail.CalculateAggregateFileds(payMarks);
                    }                    

                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Метод, генерирующий табель для конкретного сотрудника из его планового графика за этот месяц
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор месячного табеля сотрудника</param>
        /// <param name="isPrepayment">Флаг, показывающий половину месяца (первая, то есть аванс, или вторая, то есть полный табель)</param>
        public void GenerateIndividualHalfTimesheet(int timesheetDetailId, bool isPrepayment)
        {
            using (var context = new GrokDb())
            {
                var timeSheetDetail = context.TimeSheetDetail
                                             .Include(t => t.TimeSheet)
                                             .Include(t => t.TimeSheetDetailDay)
                                             .FirstOrDefault(t => t.TimeSheetDetailID == timesheetDetailId);

                if (timeSheetDetail != null)
                {
                    var scheduleDetail = context.ScheduleDetail
                                                .Include(s => s.Schedule)
                                                .Include(s => s.ScheduleDetailDay)
                                                .FirstOrDefault(s => s.Schedule.Year == timeSheetDetail.TimeSheet.Year &&
                                                                     s.Schedule.Month == timeSheetDetail.TimeSheet.Month &&
                                                                     s.EmployeeID == timeSheetDetail.EmployeeID);
                    if (scheduleDetail != null)
                    {
                        var payMarks = GetAllPayMarks();
                        timeSheetDetail.FillFromScheduleDetail(scheduleDetail, payMarks, isPrepayment, context);

                        context.SaveChanges();
                    }
                }
            }
        }

        /// <summary>
        /// Метод, очищающий вторую половину табеля для конкретного сотрудника
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор месячного табеля сотрудника</param>
        public void ClearIndividualSecondTimesheetPart(int timesheetDetailId)
        {
            using (var context = new GrokDb())
            {
                var timeSheetDetail = context.TimeSheetDetail
                                             .Include(t => t.TimeSheet)
                                             .Include(t => t.TimeSheetDetailDay)
                                             .FirstOrDefault(t => t.TimeSheetDetailID == timesheetDetailId);
                if (timeSheetDetail != null)
                {

                    var timesheetDetailDays = timeSheetDetail.TimeSheetDetailDay.Where(t => t.Date.Day > 15);

                    foreach (var timesheetDetailDay in timesheetDetailDays)
                    {
                        timesheetDetailDay.TimeSheetMarkID = null;
                        timesheetDetailDay.TotalHours = null;
                        timesheetDetailDay.NightHours = null;
                        timesheetDetailDay.OvertimeHours = null;
                    }

                    timeSheetDetail.SecondHalfMonthDays = 0;
                    timeSheetDetail.SecondHalfMonthHours = 0;
                    timeSheetDetail.SecondHalfMonthNightHours = 0;

                    timeSheetDetail.TotalMonthDays = timeSheetDetail.FirstHalfMonthDays;
                    timeSheetDetail.TotalMonthHours = timeSheetDetail.FirstHalfMonthHours;
                    timeSheetDetail.TotalMonthNightHours = timeSheetDetail.FirstHalfMonthNightHours;

                    var payMarks = GetAllPayMarks();
                    timeSheetDetail.CalculatePayrollsAndHookies(payMarks, context);

                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Метод, возвращающий функцию сортировки табелей
        /// </summary>
        /// <param name="sort">Параметры сортировки</param>
        /// <returns>Функция сортировки</returns>
        private Func<IQueryable<TimeSheetDetail>, IOrderedQueryable<TimeSheetDetail>> GetTimesheetDetailOrderFunction(List<SortDescription> sort)
        {
            var sortDescription = sort[0];

            switch (sortDescription.Field)
            {
                case "EmployeeFullName":
                    return query => TimesheetDetailSorter.OrderByEmployeeFullName(query, sortDescription.Dir);
                case "EmployeeNumber":
                    return query => TimesheetDetailSorter.OrderByEmployeeNumber(query, sortDescription.Dir);
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field + "\' не поддерживается");
            }
        }        

        /// <summary>
        /// Метод, возвращающий список функций фильтрации
        /// </summary>
        /// <param name="filter">Коллекция параметров фильтрации</param>
        /// <returns>Список функций фильтрации</returns>
        private List<Func<IQueryable<TimeSheetDetail>, IQueryable<TimeSheetDetail>>> GetFilterFunctions(FilterContainer filter)
        {
            var queries = new List<Func<IQueryable<TimeSheetDetail>, IQueryable<TimeSheetDetail>>>();

            if (filter.filters != null)
            {
                foreach (var filterItem in filter.filters)
                {
                    var searchValue = filterItem.value;

                    if (filterItem.field == "EmployeeFullName")
                    {
                        queries.Add(query => TimesheetDetailFilter.FilterByEmployeeFullName(query, searchValue));
                    }

                    if (filterItem.field == "EmployeeNumber")
                    {
                        queries.Add(query => TimesheetDetailFilter.FilterByEmployeeNumber(query, searchValue));
                    }
                }
            }

            return queries;
        }               

        /// <summary>
        /// Сохранение истории изменения табелей
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор табеля работника</param>
        /// <param name="user">Изменяющий</param>
        /// <param name="description">Описание</param>
        /// <returns>Список файлов</returns>
        public void SaveTimesheetHistory(int? timesheetDetailId, string user, string description)
        {
            using (var context = new GrokDb())
            {
                var history = context.TimeSheetHistory.Create();

                var userEntity=context.User.FirstOrDefault(e => e.Login.Equals(user));
                if (userEntity != null)
                {
                    history.UserID = userEntity.UserID;
                }
                history.CreatedOn = DateTime.Now;
                history.Description = description;
                history.TimeSheetDetailID = timesheetDetailId;

                context.TimeSheetHistory.Add(history);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Метод, возвращающий информацию о явках (коды и часы)
        /// сотрудника за месяц
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор табеля сотрудника</param>
        /// <returns>Список объектов TimesheetRowDetailModel</returns>
        public List<TimesheetRowDetailModel> GetTimesheetDays(int timesheetDetailId)
        {
            using (var context = new GrokDb())
            {
                var result = new List<TimesheetRowDetailModel>();

                var timesheetDetail = context.TimeSheetDetail
                                             .Include(t => t.TimeSheet)
                                             .Include(t => t.TimeSheetDetailDay)
                                             .Include(t => t.TimeSheetDetailDay.Select(tt => tt.TimeSheetMark))
                                             .FirstOrDefault(t => t.TimeSheetDetailID == timesheetDetailId);                

                if (timesheetDetail != null)
                {
                    int index = 0;

                    result.Add(new TimesheetRowDetailModel()
                        {
                            Days = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day < 16)
                                                  .Select(t => t.TimeSheetMarkID.HasValue
                                                                   ? (float) t.TimeSheetMarkID.Value
                                                                   : 0).ToList(),
                            Overtimes = new List<float>(),
                            DoesHoursInputHaveForDay = new List<bool>(),
                            DoesOvertimeContain = new List<bool>(),
                            Codes = GetAllPayMarks(),
                            Index = index++,
                            IsPrepayment = true,
                            IsTotalHours = null,
                            DaysCount = 15
                        });
                    result.Add(new TimesheetRowDetailModel()
                        {
                            Days = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day < 16)
                                                  .Select(t => t.TotalHours.HasValue
                                                                ? t.TotalHours.Value
                                                                : 0).ToList(),
                            Overtimes = timesheetDetail.TimeSheetDetailDay
                                                       .Where(t => t.Date.Day < 16)
                                                       .Select(t => t.OvertimeHours.HasValue
                                                                ? t.OvertimeHours.Value
                                                                : 0).ToList(),
                            DoesHoursInputHaveForDay = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day < 16)
                                                  .Select(t => t.TimeSheetMarkID.HasValue && 
                                                               t.TimeSheetMark.IsAccount)
                                                  .ToList(),
                            DoesOvertimeContain = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day < 16)
                                                  .Select(t => t.TimeSheetMark != null &&
                                                               t.TimeSheetMark.HasOvertime)
                                                  .ToList(),
                            Codes = new List<PayMarkModel>(),
                            Index = index++,
                            IsPrepayment = true,
                            IsTotalHours = true,
                            DaysCount = 15
                        });
                    result.Add(new TimesheetRowDetailModel()
                        {
                            Days = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day < 16)
                                                  .Select(t => t.NightHours.HasValue
                                                                ? t.NightHours.Value
                                                                : 0).ToList(),
                            Overtimes = new List<float>(),
                            DoesHoursInputHaveForDay = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day < 16)
                                                  .Select(t => t.TimeSheetMarkID.HasValue &&
                                                               t.TimeSheetMark.IsAccount)
                                                  .ToList(),
                            DoesOvertimeContain = new List<bool>(),
                            Codes = new List<PayMarkModel>(),
                            Index = index++,
                            IsPrepayment = true,
                            IsTotalHours = false,
                            DaysCount = 15
                        });
                    result.Add(new TimesheetRowDetailModel()
                    {
                        Days = timesheetDetail.TimeSheetDetailDay
                                              .Where(t => t.Date.Day > 15)
                                              .Select(t => t.TimeSheetMarkID.HasValue
                                                               ? (float)t.TimeSheetMarkID.Value
                                                               : 0).ToList(),
                        Overtimes = new List<float>(),
                        DoesHoursInputHaveForDay = new List<bool>(),
                        DoesOvertimeContain = new List<bool>(),
                        Codes = GetAllPayMarks(),
                        Index = index++,
                        IsPrepayment = false,
                        IsTotalHours = null,
                        DaysCount = DateTime.DaysInMonth(timesheetDetail.TimeSheet.Year, timesheetDetail.TimeSheet.Month) - 15
                    });
                    result.Add(new TimesheetRowDetailModel()
                    {
                        Days = timesheetDetail.TimeSheetDetailDay
                                              .Where(t => t.Date.Day > 15)
                                              .Select(t => t.TotalHours.HasValue
                                                            ? t.TotalHours.Value
                                                            : 0).ToList(),
                        Overtimes = timesheetDetail.TimeSheetDetailDay
                                                       .Where(t => t.Date.Day > 15)
                                                       .Select(t => t.OvertimeHours.HasValue
                                                                ? t.OvertimeHours.Value
                                                                : 0).ToList(),
                        DoesHoursInputHaveForDay = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day > 15)
                                                  .Select(t => t.TimeSheetMarkID.HasValue &&
                                                               t.TimeSheetMark.IsAccount)
                                                  .ToList(),
                        DoesOvertimeContain = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day > 15)
                                                  .Select(t => t.TimeSheetMark != null && 
                                                               t.TimeSheetMark.HasOvertime)
                                                  .ToList(),
                        Codes = new List<PayMarkModel>(),
                        Index = index++,
                        IsPrepayment = false,
                        IsTotalHours = true,
                        DaysCount = DateTime.DaysInMonth(timesheetDetail.TimeSheet.Year, timesheetDetail.TimeSheet.Month) - 15
                    });
                    result.Add(new TimesheetRowDetailModel()
                    {
                        Days = timesheetDetail.TimeSheetDetailDay
                                              .Where(t => t.Date.Day > 15)
                                              .Select(t => t.NightHours.HasValue
                                                            ? t.NightHours.Value
                                                            : 0).ToList(),
                        Overtimes = new List<float>(),
                        DoesHoursInputHaveForDay = timesheetDetail.TimeSheetDetailDay
                                                  .Where(t => t.Date.Day > 15)
                                                  .Select(t => t.TimeSheetMarkID.HasValue &&
                                                               t.TimeSheetMark.IsAccount)
                                                  .ToList(),
                        DoesOvertimeContain = new List<bool>(),
                        Codes = new List<PayMarkModel>(),
                        Index = index++,
                        IsPrepayment = false,
                        IsTotalHours = false,
                        DaysCount = DateTime.DaysInMonth(timesheetDetail.TimeSheet.Year, timesheetDetail.TimeSheet.Month) - 15
                    });

                    result.ForEach(t => t.TimesheetDetailId = timesheetDetailId);
                }

                return result;
            }
        }

        /// <summary>
        /// Метод, обновляющий информацию по дням табеля сотрудника,
        /// то есть количество часов и коды явок и оплат
        /// </summary>
        /// <param name="details">Список объектов с часами и кодами явок</param>
        public void UpdateTimesheetDetails(List<TimesheetRowDetailModel> details)
        {
            using (var context = new GrokDb())
            {
                if (details.Any())
                {
                    var timesheetDetailId = details.First().TimesheetDetailId;

                    var timesheetDetail = context.TimeSheetDetail
                                                .Include(t => t.TimeSheetDetailDay)                                                
                                                .FirstOrDefault(t => t.TimeSheetDetailID == timesheetDetailId);

                    if (timesheetDetail != null)
                    {
                        var firstHalfMonthDays = timesheetDetail.TimeSheetDetailDay
                                                                .Where(t => t.Date.Day < 16)
                                                                .ToArray();
                        var secondhalfMonthDays = timesheetDetail.TimeSheetDetailDay
                                                                 .Where(t => t.Date.Day > 15)
                                                                 .ToArray();

                        foreach (var detail in details)
                        {
                            TimeSheetDetailDay[] halfMonthDays = detail.IsPrepayment
                                                                     ? firstHalfMonthDays
                                                                     : secondhalfMonthDays;                            
                            var daysCount = detail.Days.Count;

                            if (detail.Codes != null)
                            {                                    
                                for (int i = 0; i < daysCount; i++)
                                {
                                    halfMonthDays[i].TimeSheetMarkID = (int)detail.Days[i] == 0
                                                                                ? null
                                                                                : (int?) detail.Days[i];
                                }
                            }
                            else
                            {
                                if (detail.IsTotalHours.HasValue)
                                {
                                    if (detail.IsTotalHours.Value)
                                    {
                                        for (int i = 0; i < daysCount; i++)
                                        {
                                            halfMonthDays[i].TotalHours = detail.DoesHoursInputHaveForDay[i] 
                                                                                        ? detail.Days[i]
                                                                                        : (float?)null;
                                            halfMonthDays[i].OvertimeHours = detail.DoesOvertimeContain[i]
                                                                                        ? detail.Overtimes[i]
                                                                                        : (float?) null;
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < daysCount; i++)
                                        {
                                            halfMonthDays[i].NightHours = detail.DoesHoursInputHaveForDay[i]
                                                                                        ? detail.Days[i]
                                                                                        : (float?)null;
                                        }
                                    }
                                }
                            }
                        }

                        var payMarks = GetAllPayMarks();
                        timesheetDetail.CalculatePayrollsAndHookies(payMarks, context);
                        timesheetDetail.CalculateAggregateFileds(payMarks);

                        context.SaveChanges();
                    }
                }
            }
        }        

        /// <summary>
        /// Метод, возвращающий все коды начислений с часами
        /// или все коды неявок с часами для табеля сотрудника
        /// </summary>
        /// <param name="timesheetDetailId">Идентикификатор табеля сотрудника</param>
        /// <param name="isPresence">Флаг, показывающий, что именно нужно вернуть: явки или неявки</param>
        /// <returns>Список объектов PayrollRowModel</returns>
        public List<PayrollRowModel> GetAllPayrollsOrHookies(int timesheetDetailId, bool isPresence)
        {
            using (var context = new GrokDb())
            {                                
                return context.TotalByTimeSheetMark
                                .Include(p => p.TimeSheetMark)
                                .Include(p => p.TimeSheetMarkCategory)
                                .Where(p => p.TimeSheetDetailID == timesheetDetailId &&
                                            p.TimeSheetMark.IsPresence == isPresence)
                                .OrderBy(p => p.TimeSheetMark.NumberCode)
                                .ThenBy(p => p.TimeSheetMarkCategoryID)
                                .Select(p => new PayrollRowModel()
                                    {
                                        Id = p.TotalByTimeSheetMarklID,
                                        Code = p.TimeSheetMark.HasOvertime
                                                    ? p.TimeSheetMarkCategory.Name
                                                    : SqlFunctions.StringConvert((decimal)p.TimeSheetMark.NumberCode),
                                        HoursNumber = p.TotalHours,
                                        DaysNumber = p.TotalDays
                                    }).ToList();                
            }
        }

        /// <summary>
        /// Метод, формирующий список всех возможных кодов оплат и неявок       
        /// </summary>        
        /// <returns>Список объектов модели PayMarkModel</returns>
        private List<PayMarkModel> GetAllPayMarks()
        {
            using (var context = new GrokDb())
            {
                var paymarks = context.TimeSheetMark
                    .OrderBy(t => t.TimeSheetMarkID)
                    .Where(t => t.TimeSheetMarkID != (int) TimesheetMark.NightsHours &&
                                             t.TimeSheetMarkID != (int) TimesheetMark.EveningHours &&
                                             t.TimeSheetMarkID != (int) TimesheetMark.Overtime)
                    .Select(t => new PayMarkModel()
                    {
                        PayCode = t.TimeSheetMarkID,
                        PayMark = t.LetterCode,
                        IsPresence = t.IsPresence,
                        IsAccount = t.IsAccount,
                        HasOvertime = t.HasOvertime
                    }).ToList();

                paymarks.Insert(0, new PayMarkModel()
                    {
                        PayCode = 0,
                        PayMark = "-",
                        IsPresence = false,
                        IsAccount = false,
                        HasOvertime = false
                    });

                return paymarks;
            }            
        }

        /// <summary>
        /// Метод, формирующий объект модели данных для экспорта табелей сотрудников участка на определённый месяц
        /// </summary>
        /// <param name="timesheetId">Идентификатор месячного табеля всех сотрудников участка</param>
        /// <param name="userId">Имя пользователя, который инициировал эту операцию</param>
        /// <returns>Объект модели данных для экспорта табелей сотрудников участка на определённый месяц</returns>
        public TimesheetExportModel GetExportData(int timesheetId, int userId)
        {
            using (var context = new GrokDb())
            {
                var timesheet = context.TimeSheet
                                       .FirstOrDefault(t => t.TimeSheetID == timesheetId);

                if (timesheet != null)
                {
                    var timesheetExportModel = new TimesheetExportModel();
                    const string undefinedName = "ФИО";

                    //Предполагаем, что у направления есть только один начальник
                    var directionChiefUserRole = context.UserRole
                                                        .Include(ur => ur.UserRoleDirection)
                                                        .FirstOrDefault(
                                                            u =>
                                                            u.UserRoleDirection.Any(
                                                                urd =>
                                                                urd.Direction.Sector.Any(
                                                                    s => s.SectorID == timesheet.SectorID)
                                                            ) &&
                                                            u.Role.RoleName.Equals(Roles.DirectionHead.ToText(), StringComparison.OrdinalIgnoreCase)
                                                        );

                    if (directionChiefUserRole != null)
                    {
                        timesheetExportModel.DirectionChief = directionChiefUserRole.User != null
                                                                  ? directionChiefUserRole.User.CombineUserName()
                                                                  : undefinedName;
                    }
                    else
                    {
                        timesheetExportModel.DirectionChief = undefinedName;
                    }

                    //Предполагаем, что у участка есть только один начальник
                    var sectorChiefUserRole = context.UserRole
                                                     .Include(ur => ur.User)
                                                     .FirstOrDefault(u => u.SectorID == timesheet.SectorID &&
                                                                          u.Role.RoleName.Equals(
                                                                              Roles.SectorHead.ToText(),
                                                                              StringComparison.OrdinalIgnoreCase)
                                                     );

                    if (sectorChiefUserRole != null)
                    {
                        timesheetExportModel.SectorChief = sectorChiefUserRole.User != null
                                                                  ? sectorChiefUserRole.User.CombineUserName()
                                                                  : undefinedName;
                    }
                    else
                    {
                        timesheetExportModel.SectorChief = undefinedName;
                    }

                    var userImplementer = context.User.FirstOrDefault(u => u.UserID == userId);

                    if (userImplementer != null)
                    {
                        timesheetExportModel.Author = userImplementer.CombineUserName();
                    }
                    else
                    {
                        timesheetExportModel.Author = undefinedName;
                    }

                    timesheetExportModel.CreationDate = DateTime.Now.ToString(TimeFormat);
                    timesheetExportModel.PeriodBeginDate =
                        new DateTime(timesheet.Year, timesheet.Month, 1).ToString(TimeFormat);
                    timesheetExportModel.PeriodEndDate =
                        new DateTime(timesheet.Year, timesheet.Month, DateTime.DaysInMonth(timesheet.Year, timesheet.Month)).ToString(TimeFormat);

                    timesheetExportModel.Rows = GetAllTimesheetDetails(timesheetId);

                    return timesheetExportModel;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Метод, возвращающий все индивидуальные месячные табели сотрудников участка
        /// </summary>
        /// <param name="timesheetId">Идентификатор месячного табеля всех сотрудников участка</param>
        /// <returns>Список объектов модели TimesheetRowModel</returns>
        private List<TimesheetExportRowModel> GetAllTimesheetDetails(int timesheetId)
        {
            using (var context = new GrokDb())
            {
                var timesheetDetailsQuery = context.TimeSheetDetail
                                              .Include(t => t.TimeSheetDetailDay)
                                              .Include(t => t.TotalByTimeSheetMark)
                                              .Include(t => t.TotalByTimeSheetMark.Select(tt => tt.TimeSheetMark))
                                              .Include(t => t.TotalByTimeSheetMark.Select(tt => tt.TimeSheetMarkCategory))
                                              .Where(t => t.TimeSheetID == timesheetId).AsQueryable();

                var timesheetDetails = TimesheetDetailSorter.OrderByEmployeeFullName(timesheetDetailsQuery, "asc");

                return timesheetDetails.Select(p => new TimesheetExportRowModel()
                {
                    EmployeeFullName = p.Employee.LastName + " " +
                                       p.Employee.FirstName.Substring(0, 1) + " " +
                                       p.Employee.MiddleName.Substring(0, 1),
                    EmployeeNumber = p.Employee.EmployeeNumber,
                    EmployeePosition = p.Employee.Position.PositionName,
                    DaysHalfMonthNumber1 = p.FirstHalfMonthDays,
                    DaysHalfMonthNumber2 = p.SecondHalfMonthDays,   
                    HoursHalfMonthNumber1 = p.FirstHalfMonthHours,
                    HoursHalfMonthNumber2 = p.SecondHalfMonthHours,     
                    NightHoursHalfMonthNumber1 = p.FirstHalfMonthNightHours,
                    NightHoursHalfMonthNumber2 = p.SecondHalfMonthNightHours,                    
                    TotalMonthDays = p.TotalMonthDays,
                    TotalMonthHours = p.TotalMonthHours,
                    TotalMonthNightHours = p.TotalMonthNightHours,
                    Marks = p.TimeSheetDetailDay.OrderBy(t => t.TimeSheetDetailDayID).Select(t => t.TimeSheetMarkID),
                    Hours = p.TimeSheetDetailDay.OrderBy(t => t.TimeSheetDetailDayID).Select(t => t.TotalHours),
                    NightHours = p.TimeSheetDetailDay.OrderBy(t => t.TimeSheetDetailDayID).Select(t => t.NightHours),
                    OvertimeHours = p.TimeSheetDetailDay.OrderBy(t => t.TimeSheetDetailDayID).Select(t => t.OvertimeHours),
                    PaymentViewCodes = p.TotalByTimeSheetMark.Where(t => t.TimeSheetMark.IsPresence)
                                                             .OrderBy(t => t.TimeSheetMarkID).Select(t => t.TimeSheetMark.TimeSheetMarkID < 10
                                                                                                            ? "0" + SqlFunctions.StringConvert((decimal)t.TimeSheetMark.TimeSheetMarkID).Trim()
                                                                                                            : t.TimeSheetMark.HasOvertime
                                                                                                                    ? t.TimeSheetMarkCategory.Name
                                                                                                                    : SqlFunctions.StringConvert((decimal)t.TimeSheetMark.TimeSheetMarkID).Trim()),
                    PaymentViewHoursNumber = p.TotalByTimeSheetMark.Where(t => t.TimeSheetMark.IsPresence)
                                                                   .OrderBy(t => t.TimeSheetMarkID).Select(t => t.TotalHours),
                    PaymentViewDaysNumber = p.TotalByTimeSheetMark.Where(t => t.TimeSheetMark.IsPresence)
                                                                   .OrderBy(t => t.TimeSheetMarkID).Select(t => t.TotalDays),
                    HookyViewCodes = p.TotalByTimeSheetMark.Where(t => !t.TimeSheetMark.IsPresence)
                                                             .OrderBy(t => t.TimeSheetMarkID).Select(t => t.TimeSheetMark.TimeSheetMarkID < 10
                                                                                                            ? "0" + SqlFunctions.StringConvert((decimal)t.TimeSheetMark.TimeSheetMarkID).Trim()
                                                                                                            : SqlFunctions.StringConvert((decimal)t.TimeSheetMark.TimeSheetMarkID).Trim()),
                    HookyViewHoursNumber = p.TotalByTimeSheetMark.Where(t => !t.TimeSheetMark.IsPresence)
                                                                   .OrderBy(t => t.TimeSheetMarkID).Select(t => t.TotalHours),
                    HookyViewDaysNumber = p.TotalByTimeSheetMark.Where(t => !t.TimeSheetMark.IsPresence)
                                                                   .OrderBy(t => t.TimeSheetMarkID).Select(t => t.TotalDays)
                }).ToList();
            }
        }
    }
}
