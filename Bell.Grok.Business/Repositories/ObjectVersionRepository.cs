﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class ObjectVersionRepository
    {        
        /// <summary>
        /// Метод, возвращающий текущий идентификатор состояния привязок станций к участкам или 
        /// текущий идентификатор состояния нахождения кассиров на участках.
        /// Данный идентификатор необходим на случай "миграции" станции или кассира на другой участок для сброса кэша
        /// запросов списка станций, первых букв фамилий кассиров и списка кассиров, чьи фамилии начинаются на определённую букву, 
        /// в узлах дерева навигации в узлах дерева навигации. Данный идентификатор может изменяться по результатам
        /// синхронизации 1С заказчика с БД ГРОК или при изменении участка у кассира на форме "карточка кассира"
        /// (для идентификатора состояния нахождения кассиров на участках)
        /// </summary>
        /// <returns>Текущий идентификатор состояния привязок станций к участкам или 
        ///          текущий идентификатор состояния нахождения кассиров на участках</returns>
        public string GetObjectVersion(string objectName)
        {
            using (var context = new GrokDb())
            {
                var objectVersion = context.ObjectVersion
                                            .FirstOrDefault(s => s.ObjectName.Equals(objectName, StringComparison.OrdinalIgnoreCase));

                if (objectVersion != null)
                {                    
                    return objectVersion.Version.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }        
    }
}
