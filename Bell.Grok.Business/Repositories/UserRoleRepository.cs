﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class UserRoleRepository
    {
        public UserRoleRepository()
        {            
        }

        /// <summary>
        /// Метод, возвращающий список ролей для пользователя
        /// </summary>
        /// <param name="username">Имя пользователя</param>
        /// <returns>Список ролей (названий ролей)</returns>
        public string[] GetRolesForUser(string username)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    User user = context.User.SingleOrDefault(u => u.Login == username);

                    var roles =
                        context.UserRole.Where(ur => ur.UserID == user.UserID).Select(ur => ur.Role.RoleName).ToArray();

                    return roles;
                });
        }

        /// <summary>
        /// Метод, проверяющий обладает ли пользователь указанной ролью
        /// </summary>
        /// <param name="username">Имя пользователя</param>
        /// <param name="roleName">Имя роли</param>
        /// <returns>Истина, если пользователь обладает ролью, и ложь в противном случае</returns>
        public bool IsUserInRole(string username, string roleName)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.Login == username);

                var roles = context.UserRole.Where(ur => ur.UserID == user.UserID).Select(ur => ur.Role);

                if (user != null)
                {
                    return roles.Any(r => r.RoleName == roleName);
                }
                else
                {
                    return false;
                }
            });
        }

        /// <summary>
        /// Метод, проверяющий, имеет ли пользователь хотя бы одну роль
        /// </summary>
        /// <param name="username">Имя пользователя</param>
        /// <returns>Истина, если у пользователя есть одна и более роль, и ложь в противном случае</returns>
        public bool IsUserHasRoles(string username)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.Login == username);

                return context.UserRole.Any(ur => ur.UserID == user.UserID);
            });
        }

        /// <summary>
        /// Метод, возвращающий все роли пользователя для отображения их в таблице
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns> Список объектов UserRoleModel</returns>
        public List<UserRoleModel> GetAllUserRoles(int userId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var query = context.UserRole.
                                    Include(ur => ur.Role).
                                    Include(ur => ur.Sector).
                                    Include(ur => ur.UserRoleDirection).Where(ur => ur.UserID == userId).
                                    ToArray();

                var models = query.Select(ur => new UserRoleModel
                    {
                        UserId = ur.UserID,
                        RoleId = ur.RoleID,
                        AccessLevelId = ur.Role.AccessLevelID,      
                        DirectionIds =  ur.SectorID != null ? new List<int>() { ur.Sector.DirectionID }: ur.UserRoleDirection.Select(r => r.DirectionID),
                        SectorId = ur.SectorID,                        
                        RoleName = ur.Role.RoleName                        
                    }).ToList();                

                return models;
            });
        }

        public UserRoleModel GetUserRole(int userId) 
        {
            return GetAllUserRoles(userId).FirstOrDefault();
        }

        /// <summary>
        /// Удаляет все роли пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        public void DeleteAllUserRoles(int userId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var removeUserRoles = context.UserRole.
                                        Include(u => u.UserRoleDirection).
                                        Where(u => u.UserID == userId).ToArray();

                foreach (var removeUserRole in removeUserRoles)
                {
                    var directions = context.UserRoleDirection.Where(urd => urd.UserRoleID == removeUserRole.UserRoleID);
                    foreach (var userRoleDirection in directions)
                    {
                        context.UserRoleDirection.Remove(userRoleDirection);
                    }

                    context.UserRole.Remove(removeUserRole);
                }

                context.SaveChanges();
            });
        }        

        public void UpdateUserRole(int userId, UserRoleModel userRoleModel)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var userRole = context.UserRole.FirstOrDefault(ur => ur.UserID == userId);
                bool newRole = userRole == null;

                if (newRole)
                {
                    userRole = new UserRole();
                    userRole.UserID = userId;
                    userRole.RoleID = userRoleModel.RoleId;
                    userRole.SectorID = userRoleModel.SectorId;                    

                    if (userRoleModel.DirectionIds != null && userRole.SectorID == null)
                    {
                        foreach (var directionId in userRoleModel.DirectionIds)
                        {
                            var userRoleDirection = new UserRoleDirection();
                            userRoleDirection.DirectionID = directionId;
                            userRole.UserRoleDirection.Add(userRoleDirection);
                        }
                    }

                    context.UserRole.Add(userRole);
                }                                               

                context.SaveChanges();
            });
        }

        /// <summary>
        /// Метод, возвращающий идентификатор уровня доступа пользователя
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <returns>Идентификатор уровня доступа пользователя</returns>
        public int? GetUserAccessLevel(string userName)
        {
            return GrokExceptionManager.ProcessGrokDb<int?>(context =>
            {
                var user = context.User.
                                   Include(u => u.UserRole).
                                   Include(u => u.UserRole.Select(ur => ur.Role)).
                                   SingleOrDefault(u => u.Login == userName);
                
                if (user != null)
                {
                    var userRole = user.UserRole.FirstOrDefault();
                    if (userRole != null)
                    {
                        return userRole.Role.AccessLevelID;
                    }
                }

                return null;
            });
        }

        /// <summary>
        /// Метод, возвращающий идентификатор направления для начальников направлений
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <returns>Идентификатор направления</returns>
        public List<int> GetUserDirectionId(string userName)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var user = context.User.
                                   Include(u => u.UserRole).
                                   Include(u => u.UserRole.Select(ur => ur.Role)).
                                   Include(u => u.UserRole.Select(ur => ur.UserRoleDirection)).
                                   SingleOrDefault(u => u.Login == userName);

                if (user != null)
                {
                    var userRole = user.UserRole.FirstOrDefault();
                    if (userRole != null)
                    {
                        return userRole.UserRoleDirection.Select(ur => ur.DirectionID).ToList();
                    }
                }

                return null;
            });
        }

        /// <summary>
        /// Метод, возвращающий идентификатор участка для пользователей
        /// с уровнем доступа Участок
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <returns>Идентификатор участка</returns>
        public int? GetUserSectorId(string userName)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var user = context.User.
                                   Include(u => u.UserRole).
                                   Include(u => u.UserRole.Select(ur => ur.Role)).
                                   SingleOrDefault(u => u.Login == userName);

                if (user != null)
                {
                    var userRole = user.UserRole.FirstOrDefault();
                    if (userRole != null)
                    {
                        return userRole.SectorID;
                    }
                }

                return null;
            });
        }                     

        /// <summary>
        /// Метод, возвращающий идентификатор роли пользователя
        /// по его логину
        /// </summary>
        /// <param name="userName">Логин пользователя</param>
        /// <returns>Идентификатор роли пользователя</returns>
        public int? GetRoleByLogin(string userName)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var user = context.User
                                  .Include(u => u.UserRole)
                                  .FirstOrDefault(u => u.Login == userName);

                if (user != null)
                {
                    var userRole = user.UserRole.FirstOrDefault();

                    return userRole == null
                                ? (int?)null
                                : userRole.RoleID;
                }
                else
                {
                    return (int?) null;
                }                
            });
        }
    }
}
