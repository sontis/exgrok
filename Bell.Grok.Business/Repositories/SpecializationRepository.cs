﻿using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий по работе со специализациями
    /// </summary>
    public class SpecializationRepository
    {
        /// <summary>
        /// Получает полный список специализаций
        /// </summary>
        /// <returns>Список специализаций</returns>
        public List<SpecializationModel> GetAllSpecializations()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.Specialization.Select(s => new SpecializationModel
                        {
                            SpecializationId = s.SpecializationID,
                            SpecializationName = s.SpecializationName
                        });

                    return query.ToList();
                });

        }
    }
}
