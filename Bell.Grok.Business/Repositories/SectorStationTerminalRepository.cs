﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class SectorStationTerminalRepository
    {
        /// <summary>
        /// Метод, возвращающий объект сущности SectorStationTerminal
        /// </summary>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Объект сущности SectorStationTerminal</returns>
        public SectorStationTerminal GetSectorStationTerminalById(int sectorStationTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.SectorStationTerminal.
                                                                   Include(s => s.Terminal).
                                                                   Include(s => s.Terminal.TerminalPurposeHistory).
                                                                   Include(s => s.Terminal.TerminalProfile).
                                                                   Include(s => s.TellerWindowTerminal).
                                                                   SingleOrDefault(s => s.SectorStationTerminalID == sectorStationTerminalId));
        }            
    }
}
