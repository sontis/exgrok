﻿using System.Collections.Generic;
using System.Linq;
using Bell.Grok.Model;
using Bell.Grok.DAL.Dao;

namespace Bell.Grok.Business
{
    public sealed class DirectionRepository
    {
        /// <summary>
        /// Dao для доступа к направлениям
        /// </summary>
        private DirectionDao dao = new DirectionDao();

        public DirectionRepository()
        {
        }

        /// <summary>
        /// Метод, возвращающий список всех направлений
        /// </summary>
        /// <returns>Список объектов DirectionModel</returns>
        internal List<DirectionModel> GetAllDirections()
        {
            return dao.GetAllDirections();
        }

        /// <summary>
        /// Метод, возвращающий все направления для выпадающего списка
        /// </summary>
        /// <returns>Список объектов модели DropDownListItemModel</returns>
        public Dictionary<int, string> GetAllDirectionSelectedListItems()
        {
            return dao.GetAllDirectionSelectedListItems();
        }

        /// <summary>
        /// Получить направления для данного пользователя с фильтрацией по статусам графиков кассиров
        /// </summary>
        /// <param name="login">Имя пользователя</param>
        /// <param name="scheduleStatuses">Список фильтра статусов графиков кассиров</param>
        /// <returns>Список направлений</returns>
        public List<DirectionTreeModel> GetDirectionNodes(string login, List<int> scheduleStatuses = null)
        {
            var userRepository = new UserRepository();
            var userRoleRepository = new UserRoleRepository();
            var sectorRepository = new SectorRepository();

            var userId = userRepository.GetIdByLogin(login);
            var userRoles = userRoleRepository.GetAllUserRoles(userId);

            bool isCompanyAccessLevel = false;
            List<int?> directionIds = new List<int?>();
            List<int?> sectorIds = new List<int?>();

            foreach (var userRole in userRoles)
            {
                if (userRole.AccessLevelId == (int)AccessLevels.Company)
                {
                    isCompanyAccessLevel = true;
                    break;
                }

                if (userRole.AccessLevelId == (int)AccessLevels.Direction)
                {
                    directionIds.AddRange(userRole.DirectionIds.Select(d => (int?)d));
                }

                if (userRole.AccessLevelId == (int)AccessLevels.Sector)
                {
                    directionIds.Add(sectorRepository.GetDirectionId(userRole.SectorId));
                }
            }

            var directions = dao.GetUserDirections(isCompanyAccessLevel, directionIds);

            if ((scheduleStatuses != null) && (scheduleStatuses.Count > 0))
            {
                var scheduleStatusHistoryRepository = new ScheduleStatusHistoryRepository();

                var sshDirections = scheduleStatusHistoryRepository.GetDirectionsIds(scheduleStatuses);

                directions = directions.Where(e => sshDirections.Contains(e.DirectionId)).ToList();
            }

            return directions;
        }

        /// <summary>
        /// Получить название направления
        /// </summary>
        /// <param name="directionId">Идентификатор направления</param>
        /// <returns>Название направления</returns>
        public string GetDirectionNameById(int directionId)
        {
            return dao.GetDirectionNameById(directionId);
        }

    }
}