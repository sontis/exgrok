﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class RoleRepository
    {
        public RoleRepository()
        {

        }

        /// <summary>
        /// Метод, возвращающий все роли
        /// </summary>
        /// <returns>Список объектов RoleModel</returns>
        public List<RoleModel> GetAllRoles()
        {

            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var query = context.Role.Select(r => new RoleModel
                    {
                        RoleId = r.RoleID,
                        AccessLevelId = r.AccessLevelID,
                        RoleName = r.RoleName
                    });

                return query.ToList();
            });
        }

        /// <summary>
        /// Метод, возвращающий все роли для выпадающего списка
        /// </summary>
        /// <returns>Список объектов модели DropDownListItemModel</returns>
        public Dictionary<int, string> GetAllRoleSelectedListItems()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Role
                                                                        .OrderBy(r => r.RoleID)
                                                                        .ToDictionary(r => r.RoleID, r => r.RoleName));
        }
    }
}
