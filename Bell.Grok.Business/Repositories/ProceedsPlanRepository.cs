﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class ProceedsPlanRepository
    {
        /// <summary>
        /// Метод, возвращающий планы выручки по станции для определённого набора месяцев
        /// </summary>
        /// <param name="months">Набор месяцев</param>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <param name="orderby">Строка, содержащая параметры сортировки</param>
        /// <returns>Список объектов модели ProceedsPlanGridModel</returns>
        public List<ProceedsPlanGridModel> GetProceedPlansForPeriod(List<SimpleMonthModel> months, int sectorStationId, string orderby)
        {
            var orderFunction = GetOrderFunction(orderby);

            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var resultList = new List<ProceedsPlanGridModel>();

                    var minYear = months.First().Year;
                    var minMonth = months.First().Month;
                    var maxYear = months.Last().Year;
                    var maxMonth = months.Last().Month;
                    var existingProceedsPlans =
                        context.StationPlannedRevenue.Where(s => s.SectorStationID == sectorStationId &&
                                                                 ((s.Year == minYear &&
                                                                   s.Month >= minMonth) ||
                                                                   (s.Year > minYear &&
                                                                    s.Year < maxYear) ||
                                                                   (s.Year == maxYear &&
                                                                    s.Month <= maxMonth))).ToList();

                    foreach (var month in months)
                    {
                        var model = new ProceedsPlanGridModel()
                            {
                                StationMonthProceedsId = -1,
                                SectorStationId = sectorStationId,
                                Date = ((Months) month.Month).ToText() + ", " + month.Year,
                                Year = month.Year,
                                Month = month.Month,
                                CommonPlan = 0,
                                PaidPlan = 0
                            };

                        if (existingProceedsPlans.Count(s => s.SectorStationID == sectorStationId &&
                                                                     s.Year == month.Year &&
                                                                     s.Month == month.Month) == 1)
                        {
                            var stationPlannedRevenue =
                                existingProceedsPlans.First(s => s.SectorStationID == sectorStationId &&
                                                                         s.Year == month.Year &&
                                                                         s.Month == month.Month);

                            model.StationMonthProceedsId = stationPlannedRevenue.StationPlannedRevenueID;
                            model.CommonPlan = stationPlannedRevenue.TotalPlanRevenue;
                            model.PaidPlan = stationPlannedRevenue.IncludedPlanRevenue;
                        }

                        resultList.Add(model);
                    }

                    return orderFunction(resultList).ToList();
                });
        }    

        /// <summary>
        /// Метод, возвращающий планы выручки по участку для определённого набора месяцев
        /// </summary>
        /// <param name="months">Набор месяцев</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="orderby">Строка, содержащая параметры сортировки</param>
        /// <returns>Список объектов модели ProceedsPlanGridModel</returns>
        public List<ProceedsPlanGridModel> GetSectorProceedPlansForPeriod(List<SimpleMonthModel> months,
                                                                          int sectorId, string orderby)
        {
            var orderFunction = GetOrderFunction(orderby);

            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var resultList = new List<ProceedsPlanGridModel>();

                    var minYear = months.First().Year;
                    var minMonth = months.First().Month;
                    var maxYear = months.Last().Year;
                    var maxMonth = months.Last().Month;
                    var existingProceedsPlans =
                        context.StationPlannedRevenue
                        .Include(s => s.SectorStation)
                        .Where(s => s.SectorStation.SectorID == sectorId &&
                                                                 ((s.Year == minYear &&
                                                                   s.Month >= minMonth) ||
                                                                   (s.Year > minYear &&
                                                                    s.Year < maxYear) ||
                                                                   (s.Year == maxYear &&
                                                                    s.Month <= maxMonth))).ToList();

                    foreach (var month in months)
                    {
                        var model = new ProceedsPlanGridModel()
                            {
                                StationMonthProceedsId = -1,
                                SectorStationId = -1,
                                Date = ((Months) month.Month).ToText() + ", " + month.Year,
                                Year = month.Year,
                                Month = month.Month,
                                CommonPlan = 0,
                                PaidPlan = 0
                            };

                        if (existingProceedsPlans.Any(s => s.SectorStation.SectorID == sectorId &&
                                                                   s.Year == month.Year &&
                                                                   s.Month == month.Month))
                        {


                            model.CommonPlan = existingProceedsPlans.Where(s => s.SectorStation.SectorID == sectorId &&
                                                                  s.Year == month.Year &&
                                                                  s.Month == month.Month)
                                                      .Sum(s => s.TotalPlanRevenue);
                            model.PaidPlan = existingProceedsPlans.Where(s => s.SectorStation.SectorID == sectorId &&
                                                                s.Year == month.Year &&
                                                                s.Month == month.Month)
                                                    .Sum(s => s.IncludedPlanRevenue);
                        }

                        resultList.Add(model);
                    }

                    return orderFunction(resultList).ToList();
                });
        }
    
        /// <summary>
        /// Метод, добавляющий в базу данных новый план выручки
        /// </summary>
        /// <param name="model">Объект модели плана выручки</param>
        /// <returns>Идентификатор новой записи в базе данных</returns>
        public int AddProceedsPlan(ProceedsPlanGridModel model)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var stationPlannedRevenue = new StationPlannedRevenue()
                        {
                            SectorStationID = model.SectorStationId,
                            Year = (short) model.Year,
                            Month = (byte) model.Month,
                            TotalPlanRevenue = model.CommonPlan,
                            IncludedPlanRevenue = model.PaidPlan
                        };

                    context.StationPlannedRevenue.Add(stationPlannedRevenue);
                    context.SaveChanges();

                    return stationPlannedRevenue.StationPlannedRevenueID;
                });
        }

        /// <summary>
        /// Метод, обновляющий план выручки в базе данных
        /// </summary>
        /// <param name="model">Объект модели плана выручки</param>
        /// <returns>Идентификатор отредактированной записи в базе данных</returns>
        public int UpdateProceedsPlan(ProceedsPlanGridModel model)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var stationPlannedRevenue = context.StationPlannedRevenue
                                                       .FirstOrDefault(
                                                           s =>
                                                           s.StationPlannedRevenueID == model.StationMonthProceedsId);
                    if (stationPlannedRevenue != null)
                    {
                        stationPlannedRevenue.TotalPlanRevenue = model.CommonPlan;
                        stationPlannedRevenue.IncludedPlanRevenue = model.PaidPlan;
                        context.SaveChanges();
                        return stationPlannedRevenue.StationPlannedRevenueID;
                    }
                    else
                    {
                        return -1;
                    }
                });
        }

        private Func<IEnumerable<ProceedsPlanGridModel>, IOrderedEnumerable<ProceedsPlanGridModel>> GetOrderFunction(string orderBy)
        {
            var sortDescription = SortDescriptionCreateHelper.FactoryMethod(orderBy);

            switch (sortDescription.Field)
            {
                case "Date":
                    return query => ProceedsPlanSorter.OrderByDate(query, sortDescription.IsAscending);
                case "CommonPlan":
                    return query => ProceedsPlanSorter.OrderByCommonPlan(query, sortDescription.IsAscending);
                case "PaidPlan":
                    return query => ProceedsPlanSorter.OrderByPaidPlan(query, sortDescription.IsAscending);               
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field +
                                                    "\' не поддерживается");
            }
        }
    }
}
