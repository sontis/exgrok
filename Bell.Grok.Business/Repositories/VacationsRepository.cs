﻿using System.Data.Entity.SqlServer;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using System.Data.SqlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий по работе с отпусками
    /// </summary>
    public class VacationsRepository
    {
        /// <summary>
        /// Получает список отпусков работника
        /// </summary>
        /// <param name="employeeId">Id работника</param>
        /// <returns>Список отпусков</returns>
        public List<VacationModel> GetEmployeeVacations(int employeeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.VacationPeriod.Where(v => v.EmployeeID == employeeId).Select(v => new VacationModel()
                {
                    VacationPeriodId = v.VacationPeriodID,
                    EmployeeId = employeeId,
                    BeginDate = SqlFunctions.DateName("day", v.BeginDate).Trim() + "." +
                                SqlFunctions.StringConvert((double)v.BeginDate.Month)
                                            .TrimStart()
                                            .Insert(0, v.BeginDate.Month < 10 ? "0" : "") + "." +
                                SqlFunctions.DateName("year", v.BeginDate),
                    EndDate = v.EndDate.HasValue
                                  ? SqlFunctions.DateName("day", v.EndDate.Value).Trim() + "." +
                                    SqlFunctions.StringConvert((double)v.EndDate.Value.Month)
                                                .TrimStart()
                                                .Insert(0, v.BeginDate.Month < 10 ? "0" : "") + "." +
                                    SqlFunctions.DateName("year", v.EndDate.Value)
                                  : string.Empty,
                    Comment = v.Comment,
                }).ToList());
        }

        /// <summary>
        /// Создает новую запись об отпуске у работника
        /// </summary>
        /// <param name="model">Модель отпуска</param>
        /// <returns>Обновленную модель отпуска</returns>
        public VacationModel CreateVacation(VacationModel model)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var vacation = new VacationPeriod()
                        {
                            BeginDate = DateTime.Parse(model.BeginDate),
                            EndDate = DateTime.Parse(model.EndDate),
                            Comment = model.Comment,
                            EmployeeID = model.EmployeeId
                        };
                    context.VacationPeriod.Add(vacation);
                    context.SaveChanges();

                    model.VacationPeriodId = vacation.VacationPeriodID;

                    return model;
                });
        }

        /// <summary>
        /// Удаляет отпуск у работника
        /// </summary>
        /// <param name="vacationId">Id отпуска работника</param>
        public void DeleteVacation(int vacationId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var vacation = context.VacationPeriod.FirstOrDefault(v => v.VacationPeriodID == vacationId);
                    if (vacation != null)
                    {
                        context.VacationPeriod.Remove(vacation);
                        context.SaveChanges();
                    }
                });
        }

        /// <summary>
        /// Метод, проверяющий, существуют ли у сотрудника отпуски, 
        /// границы которых пересекаются с указанным интервалом
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <param name="beginDate">Начальная граница интервала</param>
        /// <param name="endDate">Конечная граница интервала</param>
        /// <returns>Объект сущности VacationPeriod, чьи границы пересекаются с указанным интервалом</returns>
        public VacationPeriod IsVacationInPeriod(int employeeId, DateTime beginDate, DateTime endDate)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.VacationPeriod.FirstOrDefault(v => v.EmployeeID == employeeId &&
                                                                                                      v.BeginDate <= endDate &&
                                                                                                      (v.EndDate.HasValue
                                                                                                           ? beginDate <= v.EndDate.Value
                                                                                                           : beginDate <= DateTime.MaxValue)));
        }

        /// <summary>
        /// Метод, проверяющий, существует ли у сотрудника с указанным идентификатором
        /// отпуск с датой начала меньше указанной
        /// </summary>
        /// <param name="beginDate">Контрольная дата начала</param>
        /// <param name="employeelId">Идентификатор сотрудника</param>
        /// <returns>Минимальная дата начала отпуска указанного сотрудника в случае, 
        ///         если она меньше контрольной даты начала, и null в противном случае</returns>
        public DateTime? AreVacationsWithLessBeginDate(DateTime beginDate, int employeelId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    if (context.VacationPeriod.
                                Any(v => v.EmployeeID == employeelId))
                    {
                        var minBeginDate = context.VacationPeriod.
                                                   Where(v => v.EmployeeID == employeelId).
                                                   Min(v => v.BeginDate);

                        return minBeginDate < beginDate
                                   ? minBeginDate
                                   : (DateTime?) null;
                    }
                    else
                    {
                        return (DateTime?) null;
                    }
                });
        }

        /// <summary>
        /// Метод, проверяющий, существует ли у сотрудника с указанным идентификатором
        /// отпуск с датой окончания больше указанной
        /// </summary>
        /// <param name="endDate">Контрольная дата окончания</param>
        /// <param name="employeelId">Идентификатор сотрудника</param>
        /// <returns>Максимальная дата окончания отпуска указанного сотрудника в случае, 
        ///         если она больше контрольной даты окончания, и null в противном случае</returns>
        public DateTime? AreVacationsWithBiggerEndDate(DateTime endDate, int employeelId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    if (context.VacationPeriod.
                                Any(v => v.EmployeeID == employeelId))
                    {
                        var maxEndDate = context.VacationPeriod.
                                                 Where(v => v.EmployeeID == employeelId).
                                                 Max(v => v.EndDate);

                        return maxEndDate > endDate
                                   ? maxEndDate
                                   : (DateTime?) null;
                    }
                    else
                    {
                        return (DateTime?) null;
                    }
                });
        }
    }
}
