﻿using Bell.Grok.DAL.Dao;
using System.Collections.Generic;

namespace Bell.Grok.Business
{
    public sealed class ScheduleStatusHistoryRepository
    {
        /// <summary>
        /// Dao для доступа к истории статуса графика кассиров
        /// </summary>
        private ScheduleStatusHistoryDao dao = new ScheduleStatusHistoryDao();

        /// <summary>
        /// Получить сгруппированный идентификаторы направлений в графиках кассиров с указанными статусами (для фильтрации дерева)
        /// </summary>
        /// <param name="scheduleStatuses">Список статусов графиков кассиров</param>
        /// <returns></returns>
        internal List<int> GetDirectionsIds(List<int> scheduleStatuses)
        {
            return dao.GetDirectionsIds(scheduleStatuses);
        }
    }
}
