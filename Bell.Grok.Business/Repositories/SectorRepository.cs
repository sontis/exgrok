﻿using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Bell.Grok.Business
{
    public class SectorRepository
    {
        /// <summary>
        /// Метод, возвращающий все секторы
        /// </summary>
        /// <returns>Список объектов SectorModel</returns>
        public List<SectorModel> GetAllSectors()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.Sector.Select(s => new SectorModel()
                        {
                            SectorId = s.SectorID,
                            DirectionId = s.DirectionID,
                            SectorNumber = s.SectorNumber,
                            SectorName = s.SectorName
                        }).ToList();
                    return query;
                });
        }

        /// <summary>
        /// Метод, возвращающий секторы направления
        /// </summary>
        /// <param name="directionId">Идентификатор направления</param>
        /// <returns>Список объектов SectorModel</returns>
        public List<SectorModel> GetAllSectorsForDirectory(int directionId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.Sector.Where(s => s.DirectionID == directionId).Select(s => new SectorModel()
                        {
                            SectorId = s.SectorID,
                            DirectionId = s.DirectionID,
                            SectorNumber = s.SectorNumber,
                            SectorName = s.SectorName
                        }).ToList();
                    return query;
                });
        }

        /// <summary>
        /// Метод возвращающий узлы-участки для дерева навигации, начальником которых является пользователь
        /// или все узлы-участки для заданного направления, если пользователь является начальником этого направления
        /// или имеет глобальный уровень доступа
        /// </summary>
        /// <param name="isAllSectorsForDirection">Возвращать ли все участки направления</param>
        /// <param name="sectorIds">Список идентификаторов участков, где пользователь является начальником 
        /// (актуально, если он не начальник направления или у него нет глобального уровня доступа)</param>
        /// <param name="directionId">Идентификатор направления, в пределах которого происходит поиск участков</param>
        /// <param name="scheduleStatuses">Список статусов расписаний для фильтрации</param>
        /// <returns>Список объектов SectorTreeModel</returns>
        public List<SectorTreeModel> GetSectorNodes(bool isAllSectorsForDirection, List<int?> sectorIds, int directionId, List<int> scheduleStatuses = null)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var query = context.Sector.Where(s => s.DirectionID == directionId);

                if (!isAllSectorsForDirection)
                {
                    query = query.Where(s => sectorIds.Contains(s.SectorID));
                }

                var sectors = query.ToList();

                if ((scheduleStatuses != null) && (scheduleStatuses.Count > 0))
                {
                    var sectorList = new HashSet<int>();

                    var stationHistories = new Dictionary<int, Tuple<int, ScheduleStatusHistory>>();

                    foreach (var scheduleStatusHistory in context.ScheduleStatusHistory.Where(e => (e.Schedule.SectorStation.Sector.DirectionID == directionId) && (e.Schedule.ScheduleDetail.Count > 0)))
                    {
                        if (stationHistories.ContainsKey(scheduleStatusHistory.ScheduleID))
                        {
                            if (stationHistories[scheduleStatusHistory.ScheduleID].Item2.BeginDate < scheduleStatusHistory.BeginDate)
                            {
                                stationHistories[scheduleStatusHistory.ScheduleID] = new Tuple<int, ScheduleStatusHistory>(scheduleStatusHistory.ScheduleStatusID, scheduleStatusHistory);
                            }
                        }
                        else
                        {
                            stationHistories.Add(scheduleStatusHistory.ScheduleID, new Tuple<int, ScheduleStatusHistory>(scheduleStatusHistory.ScheduleStatusID, scheduleStatusHistory));
                        }
                    }

                    foreach (var stationHistory in stationHistories.Where(e => scheduleStatuses.Contains(e.Value.Item1)))
                    {
                        if (!sectorList.Contains(stationHistory.Value.Item2.Schedule.SectorStation.Sector.SectorID))
                        {
                            sectorList.Add(stationHistory.Value.Item2.Schedule.SectorStation.Sector.SectorID);
                        }
                    }

                    sectors = sectors.Where(e => sectorList.Contains(e.SectorID)).ToList();
                }

                var result = sectors.Select(s => new SectorTreeModel
                    {
                        SectorId = s.SectorID,
                        SectorName = s.SectorName,
                        HasChildren = true
                    });

                return result.ToList();
            });
        }

        /// <summary>
        /// Метод возвращающий идентификатор направления, на котором находится участок
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Идентификатор направления</returns>
        public int GetDirectionId(int? sectorId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Sector.SingleOrDefault(s => s.SectorID == sectorId).DirectionID);
        }

        public List<SectorModel> GetStationSectors(int stationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.SectorStation.Where(s => s.StationID == stationId).ToArray().Select(s => ToSectorModel(s.Sector)).ToList());
        }

        /// <summary>
        /// Метод, преобразующий entity Sector в модель SectorModel
        /// </summary>
        /// <param name="sector">Объект сущности Sector</param>
        /// <returns>Объект модели SectorModel</returns>
        private SectorModel ToSectorModel(Sector sector)
        {
            var model = new SectorModel();
            model.SectorId = sector.SectorID;
            model.DirectionId = sector.DirectionID;
            model.SectorNumber = sector.SectorNumber;
            model.SectorName = sector.SectorName;

            return model;
        }

        internal Sector GetSectorById(int sectorId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Sector
                                                                        .Include(s => s.Direction)
                                                                        .SingleOrDefault(s => s.SectorID == sectorId));
        }

        /// <summary>
        /// Метод, возвращающий объект сущности Sector
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Объект сущности Sector</returns>
        public SectorModel GetSectorModelById(int sectorId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var sector = context.Sector
                                        .Include(s => s.Direction)
                                        .SingleOrDefault(s => s.SectorID == sectorId);

                    return sector == null
                               ? null
                               : new SectorModel()
                                   {
                                       SectorId = sector.SectorID,
                                       SectorName = sector.SectorName,
                                       SectorNumber = sector.SectorNumber,
                                       DirectionId = sector.DirectionID,
                                       DirectionName = sector.Direction.DirectionName
                                   };
                });
        }        

        /// <summary>
        /// Метод, возвращающий все участки для выпадающего списка
        /// </summary>
        /// <returns>Список объектов модели DropDownListItemModel</returns>
        public Dictionary<int, string> GetAllSectorSelectedListItems()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Sector
                                                                        .Include(s => s.Direction)
                                                                        .OrderBy(s => s.DirectionID)
                                                                        .ToDictionary(s => s.SectorID,
                                                                                      s => s.SectorName + 
                                                                                           " (" + 
                                                                                           s.Direction.DirectionName + 
                                                                                           " напр.)"));
        }

        /// <summary>
        /// Метод, проверяющий наличие станций на участке
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Истина, если станции на участке есть, и ложь в противном случае</returns>
        public bool AreStationsOnSector(int sectorId)
        {
            return
                GrokExceptionManager.ProcessGrokDb(context => context.SectorStation.Any(ss => ss.SectorID == sectorId));                
        }

        /// <summary>
        /// Метод, возвращающий название участка 
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Название участка </returns>
        public string GetSectorNameById(int sectorId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var sector = context.Sector.SingleOrDefault(s => s.SectorID == sectorId);

                    return sector == null
                               ? null
                               : sector.SectorName;
                });
        }
    }
}
