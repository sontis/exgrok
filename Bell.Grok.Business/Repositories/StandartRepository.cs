﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Business.Extensions;
using System.Collections.Specialized;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class StandartRepository
    {
        public StandartRepository()
        {

        }        

        /// <summary>
        /// Метод, строящий выражение для выборки нормативов значений нормальной нагрузки
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="directionId">Идентификатор направления</param>
        /// <param name="context">Объект для доступа к БД</param>
        /// <returns>Выражение для выборки нормативов значений нормальной нагрузки</returns>
        private IQueryable<SectorStation> GetSectorStationSelection(int? sectorId, int? directionId, GrokDb context)
        {
            var standarts = context.SectorStation
                                           .Include(s => s.Station)
                                           .Include(s => s.StationStandardLoadTW)
                                           .Include(s => s.StationStandardLoadTW.Select(ss => ss.TerminalType))
                                           .AsQueryable();

            if (directionId.HasValue)
            {
                standarts = standarts.Include(s => s.Sector)
                                     .Where(s => s.Sector.DirectionID == directionId)
                                     .AsQueryable();
            }

            if (sectorId.HasValue)
            {
                standarts = standarts.Where(s => s.SectorID == sectorId)
                                     .AsQueryable();
            }

            return standarts;
        }

        /// <summary>
        /// Метод, переконвертирующий выражение для выборки нормативов значений нормальной нагрузки
        /// в список объектов модели StandartRowModel
        /// </summary>
        /// <param name="standarts">Выражение для выборки нормативов значений нормальной нагрузки</param>
        /// <returns>список объектов модели StandartRowModel</returns>
        private List<StandartRowModel> SelectStandartRowModels(IQueryable<SectorStation> standarts)
        {            
            return standarts.Select(p => new StandartRowModel()
            {
                SectorStationId = p.SectorStationID,
                StationName = p.Station.StationName,
                BPA = p.StationStandardLoadTW.Any(s => s.TerminalTypeID == (int)TerminalTypes.Bpa)
                          ? p.StationStandardLoadTW.FirstOrDefault(s => s.TerminalTypeID == (int)TerminalTypes.Bpa)
                             .TicketsPerHour
                          : 0,
                MKTK = p.StationStandardLoadTW.Any(s => s.TerminalTypeID == (int)TerminalTypes.Mktk)
                           ? p.StationStandardLoadTW.FirstOrDefault(s => s.TerminalTypeID == (int)TerminalTypes.Mktk)
                              .TicketsPerHour
                           : 0
            }).ToList();
        }

        /// <summary>
        /// Метод, возвращающий нормативные стандарты всех станций на участке
        /// </summary>        
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во строк на страницу</param>
        /// <param name="orderby">Поле по которому идет сортировка(null если не нужна)</param>
        /// <param name="orderto">Порядок(desc- по ублыванию, любое другое значение - по возрастанию)</param>
        /// <param name="filterBpa">Фильтр по БПА(-1 если невыбрано)</param>
        /// <param name="filterMktk">Фильтр по МКТК(-1 если невыбрано)</param>
        /// <param name="sectorId">Разрешеный участок</param>
        /// <param name="directionId">Разрешённое направление</param>
        /// <param name="count">Кол-во станций</param>
        /// <returns>Список нормативных стандартов</returns>
        public List<StandartRowModel> GetStandartRowModels(int page, int pageSize, string @orderby, string orderto, 
                                                           int filterBpa, int filterMktk, int? sectorId, int? directionId, 
                                                           out int count)
        {
            var countInner = 0;
            var result = GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var standarts = GetSectorStationSelection(sectorId, directionId, context);
                    var rowModels = SelectStandartRowModels(standarts);

                    rowModels = OrderStandarts(rowModels, orderby, orderto).ToList();
                    rowModels = FilterStandarts(rowModels, filterBpa, filterMktk).ToList();
                    countInner = rowModels.Count;

                    return new PagedList<StandartRowModel>(rowModels, page, pageSize, countInner);                    
                });

            count = countInner;
            return result;
        }

        /// <summary>
        /// Метод, сортирующий нормативы значений нормальной нагрузки
        /// </summary>
        /// <param name="standarts">Список нормативов</param>
        /// <param name="orderby">Поле сортировки</param>
        /// <param name="orderto">Порядок сортировки</param>
        /// <returns></returns>
        private IEnumerable<StandartRowModel> OrderStandarts(IEnumerable<StandartRowModel> standarts, string orderby, string orderto)
        {
            if (!string.IsNullOrWhiteSpace(orderby))
            {
                if (orderby.Equals("StationName"))
                {
                    return orderto.ToLower().Equals("desc")
                               ? standarts.OrderByDescending(r => r.StationName)
                               : standarts.OrderBy(r => r.StationName);
                }
                
                if (orderby.Equals("BPA"))
                {
                    return orderto.ToLower().Equals("desc")
                               ? standarts.OrderByDescending(r => r.BPA)
                               : standarts.OrderBy(r => r.BPA);
                }
                
                if (orderby.Equals("MKTK"))
                {
                    return orderto.ToLower().Equals("desc")
                               ? standarts.OrderByDescending(r => r.MKTK)
                               : standarts.OrderBy(r => r.MKTK);
                }
            }            

            return standarts;                      
        }

        /// <summary>
        /// Метод, фильтрующий нормативы значений нормальной нагрузки
        /// </summary>
        /// <param name="standarts">Список нормативов</param>
        /// <param name="filterBpa">Значения фильтра по бпа-терминалам</param>
        /// <param name="filterMktk">Значения фильтра по мктк-терминалам</param>
        /// <returns></returns>
        private IEnumerable<StandartRowModel> FilterStandarts(IEnumerable<StandartRowModel> standarts, int filterBpa, int filterMktk)
        {
            if (filterBpa != -1)
            {
                standarts = standarts.Where(s => s.BPA == filterBpa);               
            }
            if (filterMktk != -1)
            {
                standarts = standarts.Where(e => e.MKTK == filterMktk);                
            }

            return standarts;
        }                

        /// <summary>
        /// Метод, сохраняющий нормативные стандарты
        /// </summary>
        public void SaveStandarts(List<int> ids, short value, int column)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var externalNumber = column == 1 ? 3 : 8;
                foreach (var sectorStationId in ids)
                {
                    var entity = context.StationStandardLoadTW.FirstOrDefault(e => (e.SectorStationID == sectorStationId) && (e.TerminalType.ExternalNumber == externalNumber));
                    if (entity == null)
                    {
                        entity = context.StationStandardLoadTW.Create();
                        entity.SectorStationID = sectorStationId;
                        entity.TerminalType = context.TerminalType.FirstOrDefault(e => e.ExternalNumber == externalNumber);
                        context.StationStandardLoadTW.Add(entity);
                    }

                    entity.TicketsPerHour = value;

                    context.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Метод, возвращающий количество нормативов, удовлетворяющих условию фильтрации
        /// </summary>
        /// <param name="filterBpa">Фильтр по БПА(-1 если невыбрано)</param>
        /// <param name="filterMktk">Фильтр по МКТК(-1 если невыбрано)</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="directionId">Идентификатор направления</param>
        /// <returns>Количество значений нормативов, удовлетворяющих условию фильтрации</returns>
        public int GetFilteredValuesCount(int filterBpa, int filterMktk, int? sectorId, int? directionId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var standarts = GetSectorStationSelection(sectorId, directionId, context);
                    var rowModels = SelectStandartRowModels(standarts);

                    rowModels = FilterStandarts(rowModels, filterBpa, filterMktk).ToList();

                    return rowModels.Count;
                });
        }

        /// <summary>
        /// Метод, получающий все значения нормативов на участке
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="directionId">Идентификатор направления</param>
        /// <param name="terminalTypeId">Идентификатор типа терминала</param>
        /// <returns>Список значений нормативов</returns>
        public List<short> GetAllSectorStandartsByTerminalType(int? sectorId, int? directionId, TerminalTypes terminalTypeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var standarts = context.StationStandardLoadTW
                                           .Include(s => s.SectorStation)
                                           .Include(s => s.TerminalType)
                                           .AsQueryable();

                    if (directionId.HasValue)
                    {
                        standarts = standarts.Include(s => s.SectorStation.Sector)
                                             .Where(s => s.SectorStation.Sector.DirectionID == directionId && 
                                                         s.TerminalTypeID == (int) terminalTypeId);
                    }

                    if (sectorId.HasValue)
                    {
                        standarts = standarts.Where(s => s.SectorStation.SectorID == sectorId &&
                                                         s.TerminalTypeID == (int) terminalTypeId);
                    }

                    var ticketsPerHourValues = standarts.Select(s => s.TicketsPerHour)
                                                        .Distinct()
                                                        .OrderBy(s => s)
                                                        .ToList();

                    if (!ticketsPerHourValues.Contains(0))
                    {
                        ticketsPerHourValues.Insert(0, 0);
                    }

                    return ticketsPerHourValues;
                });
        }
    }
}
