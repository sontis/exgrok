﻿using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий по работе с должностями
    /// </summary>
    public class PositionRepository
    {
        /// <summary>
        /// Получает полный список должностей
        /// </summary>
        /// <returns>Список должностей</returns>
        public List<PositionModel> GetAllPositions()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.Position.Where(s => s.IsCashierRole).Select(p => new PositionModel()
                        {
                            PositionId = p.PositionID,
                            PositionName = p.PositionName

                        }).ToList();
                    return query;
                });
        }

        /// <summary>
        /// Возвращает позицию по Id
        /// </summary>
        /// <param name="id">Id позиции</param>
        /// <returns>Модель поизиции</returns>
        public PositionModel GetPositionById(int id)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var result = context.Position.FirstOrDefault(p => p.PositionID == id);
                    if (result != null)
                    {
                        var position = new PositionModel()
                            {
                                PositionId = result.PositionID,
                                PositionName = result.PositionName

                            };
                        return position;
                    }
                    return null;
                });

        }

        /// <summary>
        /// Возвращает позицию по имени
        /// </summary>
        /// <param name="positionName">Имя позиции</param>
        /// <returns>Модель поизиции</returns>
        public PositionModel GetPositionByName(string positionName)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var result = context.Position.FirstOrDefault(p => p.PositionName == positionName);
                    if (result != null)
                    {
                        var position = new PositionModel()
                            {
                                PositionId = result.PositionID,
                                PositionName = result.PositionName

                            };
                        return position;
                    }
                    return null;
                });
            
        }
    }
}
