﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Data.Entity;
using Bell.Grok.Business.Extensions;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using Bell.Grok.Services.Logging;

namespace Bell.Grok.Business
{
    public class ScheduleTemplateRepository
    {
        /// <summary>
        /// Формат времени для конвертации значения DateTime в строку 
        /// </summary>
        private const string TimeFormat = "dd.MM.yyyy";

        /// <summary>
        /// Метод, обновляющий сущность ScheduleTemplate (вместе с навигационными свойствами) данными из модели ScheduleTemplateCreateModel
        /// </summary>
        /// <param name="createModel">Объект модели ScheduleTemplateCreateModel</param>
        /// <param name="authorUserId">Идентификатор пользователя, автора шаблона</param>
        /// <param name="name">Краткое наименование - имя загружаемого файла без расширения</param>
        /// <param name="modes">Список объектов, хранящих информацию, полученную из excel-документа, о режимах графиков кассиров</param>
        /// <param name="userName">Имя пользователя</param>
        public void UpdateFromModel(ScheduleTemplateModel createModel, int authorUserId, string name, List<ScheduleTemplateModeModel> modes, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var scheduleTemplate = new ScheduleTemplate();

                scheduleTemplate.AuthorID = authorUserId;
                scheduleTemplate.BeginDate = DateTime.Parse(createModel.BeginDate);
                scheduleTemplate.EndDate = DateTime.Parse(createModel.EndDate);
                scheduleTemplate.CreationDateTime = DateTime.Now;
                scheduleTemplate.FullName = createModel.FullName;
                scheduleTemplate.ShortName = name;

                if (createModel.Directions != null)
                {
                    foreach (var direction in createModel.Directions)
                    {
                        var directionScheduleTemplate = new DirectionScheduleTemplate();
                        directionScheduleTemplate.DirectionID = direction;
                        scheduleTemplate.DirectionScheduleTemplate.Add(directionScheduleTemplate);
                    }
                }

                if (createModel.Sectors != null)
                {
                    foreach (var sector in createModel.Sectors)
                    {
                        var sectorScheduleTemplate = new SectorScheduleTemplate();
                        sectorScheduleTemplate.SectorID = sector;
                        scheduleTemplate.SectorScheduleTemplate.Add(sectorScheduleTemplate);
                    }
                }

                if (modes != null && modes.Any())
                {
                    foreach (var mode in modes)
                    {
                        var scheduleTemplateMode = new ScheduleTemplateMode();
                        scheduleTemplateMode.Name = mode.Name;

                        if (mode.ScheduleTemplateDetails != null)
                        {
                            foreach (var detail in mode.ScheduleTemplateDetails)
                            {
                                var scheduleTemplateDetail = new ScheduleTemplateDetail();
                                scheduleTemplateDetail.Date = detail.Date;
                                scheduleTemplateDetail.Year = detail.Year;
                                scheduleTemplateDetail.Month = detail.Month;
                                scheduleTemplateDetail.DayWorkingHours = detail.DayWorkingHours;
                                scheduleTemplateDetail.NightWorkingHours = detail.NightWorkingHours;
                                scheduleTemplateDetail.DayTypeID = detail.DayType;
                                scheduleTemplateDetail.IsSplitted = detail.IsSplitted;

                                scheduleTemplateMode.ScheduleTemplateDetail.Add(scheduleTemplateDetail);
                            }
                        }

                        scheduleTemplate.ScheduleTemplateMode.Add(scheduleTemplateMode);
                    }
                }

                context.ScheduleTemplate.Add(scheduleTemplate);

                context.SaveChanges();

                #region Logging
                var allDirections = new DirectionRepository().GetAllDirections();
                var allSectors = new SectorRepository().GetAllSectors();
                var changes = new Dictionary<string, string>();
                changes.Add("FullName", createModel.FullName);
                changes.Add("BeginDate", createModel.BeginDate);
                changes.Add("EndDate", createModel.EndDate);
                changes.Add("Directions", ((createModel.Directions == null) || (!createModel.Directions.Any())) ? "" : createModel.Directions.Select(e =>
                {
                    var firstOrDefault = allDirections.FirstOrDefault(d => d.DirectionId == e);
                    return firstOrDefault != null ? new Tuple<string, string>(e.ToString(CultureInfo.InvariantCulture), firstOrDefault.DirectionName) : null;
                }).Select(e => string.Format("{0}({1})", e.Item2, e.Item1)).Aggregate((current, next) => current + ", " + next));
                changes.Add("Sectors", ((createModel.Sectors == null) || (!createModel.Sectors.Any())) ? "" : createModel.Sectors.Select(e =>
                {
                    var firstOrDefault = allSectors.FirstOrDefault(d => d.SectorId == e);
                    return firstOrDefault != null ? new Tuple<string, string>(e.ToString(CultureInfo.InvariantCulture), firstOrDefault.SectorName) : null;
                }).Select(e => string.Format("{0}({1})", e.Item2, e.Item1)).Aggregate((current, next) => current + ", " + next));
                GrokLogger.LogScheduleTemplateCreate("new", changes, userName);
                #endregion
            });

        }

        /// <summary>
        /// Собирается инфо об изменении описания шаблона графика кассиров
        /// </summary>
        /// <param name="model">Модель данных с изменениями</param>
        /// <param name="scheduleTemplate">Модель данных в источнике</param>
        /// <returns>Словарь с результатами анализа измененных данных</returns>
        private static Dictionary<string, string> CollectChangeInfo(ScheduleTemplateModel model, ScheduleTemplate scheduleTemplate)
        {
            var changes = new Dictionary<string, string>();

            if (scheduleTemplate.BeginDate != DateTime.Parse(model.BeginDate))
            {
                changes.Add("BeginDate", model.BeginDate);
            }
            if (scheduleTemplate.EndDate != DateTime.Parse(model.EndDate))
            {
                changes.Add("EndDate", model.EndDate);
            }
            if (scheduleTemplate.FullName != model.FullName)
            {
                changes.Add("FullName", model.FullName);
            }

            // анализ изменения набора направлений
            bool isDirectionsChange = false;
            if (model.Directions != null)
            {
                var originalDirections = scheduleTemplate.DirectionScheduleTemplate.Select(e => e.DirectionID).ToList();
                if (model.Directions.Count() != originalDirections.Count)
                {
                    isDirectionsChange = true;
                }
                else
                {
                    foreach (var direction in model.Directions)
                    {
                        if (!originalDirections.Contains(direction))
                        {
                            isDirectionsChange = true;
                        }
                    }
                }
            }
            else if (scheduleTemplate.DirectionScheduleTemplate.Count > 0)
            {
                isDirectionsChange = true;
            }

            // анализ изменения набора участков
            bool isSectorsChange = false;
            if (model.Sectors != null)
            {
                var originalSectors = scheduleTemplate.SectorScheduleTemplate.Select(e => e.SectorID).ToList();
                if (model.Sectors.Count() != originalSectors.Count)
                {
                    isSectorsChange = true;
                }
                else
                {
                    foreach (var sector in model.Sectors)
                    {
                        if (!originalSectors.Contains(sector))
                        {
                            isSectorsChange = true;
                        }
                    }
                }
            }
            else if (scheduleTemplate.SectorScheduleTemplate.Count > 0)
            {
                isSectorsChange = true;
            }

            if (isDirectionsChange)
            {
                var allDirections = new DirectionRepository().GetAllDirections();
                changes.Add("Directions", ((model.Directions == null) || (!model.Directions.Any())) ? "" : model.Directions.Select(e =>
                {
                    var firstOrDefault = allDirections.FirstOrDefault(d => d.DirectionId == e);
                    return firstOrDefault != null ? new Tuple<string, string>(e.ToString(CultureInfo.InvariantCulture), firstOrDefault.DirectionName) : null;
                }).Select(e => string.Format("{0}({1})", e.Item2, e.Item1)).Aggregate((current, next) => current + ", " + next));
            }

            if (isSectorsChange)
            {
                var allSectors = new SectorRepository().GetAllSectors();
                changes.Add("Sectors", ((model.Sectors == null) || (!model.Sectors.Any())) ? "" : model.Sectors.Select(e =>
                {
                    var firstOrDefault = allSectors.FirstOrDefault(d => d.SectorId == e);
                    return firstOrDefault != null ? new Tuple<string, string>(e.ToString(CultureInfo.InvariantCulture), firstOrDefault.SectorName) : null;
                }).Select(e => string.Format("{0}({1})", e.Item2, e.Item1)).Aggregate((current, next) => current + ", " + next));
            }

            return changes;
        }

        /// <summary>
        /// Обновление инфо о шаблоне графика кассиров
        /// </summary>
        /// <param name="model">Объект модели ScheduleTemplateEditModel</param>
        /// <param name="lastAuthorUserId">Идентификатор пользователя, автора последних изменений в шаблоне графика кассиров</param>
        /// <param name="userName">Имя пользователя</param>
        public void UpdateFromModel(ScheduleTemplateModel model, int lastAuthorUserId, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var scheduleTemplate = context.ScheduleTemplate
                                              .Include(s => s.DirectionScheduleTemplate)
                                              .Include(s => s.SectorScheduleTemplate)
                                              .SingleOrDefault(s => s.ScheduleTemplateID == model.ScheduleTemplateId);

                if (scheduleTemplate == null)
                {
                    return;
                }

                var changes = CollectChangeInfo(model, scheduleTemplate);

                if (changes.Count > 0)
                {
                    // если есть изменения в направлениях, то сохранить их
                    if (changes.ContainsKey("Directions"))
                    {
                        while (scheduleTemplate.DirectionScheduleTemplate.Count > 0)
                        {
                            var directionScheduleTemplate = scheduleTemplate.DirectionScheduleTemplate.ToList()[0];
                            scheduleTemplate.DirectionScheduleTemplate.Remove(directionScheduleTemplate);
                            context.DirectionScheduleTemplate.Remove(directionScheduleTemplate);
                        }

                        if (model.Directions != null)
                        {
                            foreach (var direction in model.Directions)
                            {
                                var directionScheduleTemplate = new DirectionScheduleTemplate();
                                directionScheduleTemplate.DirectionID = direction;
                                scheduleTemplate.DirectionScheduleTemplate.Add(directionScheduleTemplate);
                            }
                        }
                    }

                    // если есть изменения в участках, то сохранить их
                    if (changes.ContainsKey("Sectors"))
                    {
                        while (scheduleTemplate.SectorScheduleTemplate.Count > 0)
                        {
                            var sectorScheduleTemplate = scheduleTemplate.SectorScheduleTemplate.ToList()[0];
                            scheduleTemplate.SectorScheduleTemplate.Remove(sectorScheduleTemplate);
                            context.SectorScheduleTemplate.Remove(sectorScheduleTemplate);
                        }

                        if (model.Sectors != null)
                        {
                            foreach (var sector in model.Sectors)
                            {
                                var sectorScheduleTemplate = new SectorScheduleTemplate();
                                sectorScheduleTemplate.SectorID = sector;
                                scheduleTemplate.SectorScheduleTemplate.Add(sectorScheduleTemplate);
                            }
                        }
                    }

                    scheduleTemplate.BeginDate = DateTime.Parse(model.BeginDate);
                    scheduleTemplate.EndDate = DateTime.Parse(model.EndDate);
                    scheduleTemplate.FullName = model.FullName;
                    scheduleTemplate.ChangeDateTime = DateTime.Now;
                    scheduleTemplate.LastAuthorID = lastAuthorUserId;

                    context.SaveChanges();

                    #region Logging
                    GrokLogger.LogScheduleTemplateEdit(model.ScheduleTemplateId.ToString(), changes, userName);
                    #endregion
                }
            });
        }

        /// <summary>
        /// Метод, возвращающий отсортированный и отфильтрованный список шаблонов графиков кассиров для таблицы
        /// </summary>
        /// <param name="page">Номер страницы таблицы</param>
        /// <param name="pageSize">Количество элементов на странице таблицы</param>
        /// <param name="orderby">Строка с параметрами сортировки</param>
        /// <param name="filterDictionary">Коллекция параметров фильтрации</param>
        /// <param name="itemsCount">Общее количество шаблонов графиков кассиров (необходимо для правильного постраничного просмотра)</param>
        /// <returns>Список объектов ScheduleTemplateGridModel</returns>
        public List<ScheduleTemplateGridModel> Search(int page, int pageSize, string orderby, NameValueCollection filterDictionary, out int itemsCount)
        {

            var itemsCountInner = 0;

            var result = GrokExceptionManager.ProcessGrokDb(context =>
            {
                var orderFunction = GetTerminalOrderFunction(orderby);
                var filterFunctions = GetFilterFunctions(filterDictionary);

                var query = context.ScheduleTemplate
                                   .Include(s => s.SectorScheduleTemplate)
                                   .Include(s => s.SectorScheduleTemplate.Select(ss => ss.Sector))
                                   .Include(s => s.DirectionScheduleTemplate)
                                   .Include(s => s.DirectionScheduleTemplate.Select(d => d.Direction))
                                   .Include(s => s.LastAuthor)
                                   .AsQueryable<ScheduleTemplate>();

                foreach (var filterFunction in filterFunctions)
                {
                    query = filterFunction(query);
                }

                query = orderFunction(query).AsQueryable();
                itemsCountInner = query.Count();

                var pagedList = new PagedList<ScheduleTemplate>(query, page, pageSize);

                var data = pagedList.Select(s => new ScheduleTemplateGridModel()
                    {
                        ScheduleTemplateId = s.ScheduleTemplateID,
                        ShortName = s.ShortName,
                        FullName = s.FullName,
                        Author = (!string.IsNullOrEmpty(s.Author.LastName) ? s.Author.LastName + " " : string.Empty) +
                                 (!string.IsNullOrEmpty(s.Author.FirstName) ? s.Author.FirstName.Substring(0, 1) + "." : string.Empty) +
                                 (!string.IsNullOrEmpty(s.Author.MiddleName) ? s.Author.MiddleName.Substring(0, 1) + "." : string.Empty),
                        LastAuthor = s.LastAuthor != null
                                        ? (!string.IsNullOrEmpty(s.LastAuthor.LastName) ? s.LastAuthor.LastName + " " : string.Empty) +
                                          (!string.IsNullOrEmpty(s.LastAuthor.FirstName) ? s.LastAuthor.FirstName.Substring(0, 1) + "." : string.Empty) +
                                          (!string.IsNullOrEmpty(s.LastAuthor.MiddleName) ? s.LastAuthor.MiddleName.Substring(0, 1) + "." : string.Empty)
                                        : string.Empty,
                        BeginDate = s.BeginDate.ToString(TimeFormat),
                        EndDate = s.EndDate.ToString(TimeFormat),
                        ChangeDateTime = s.ChangeDateTime.HasValue
                                            ? s.ChangeDateTime.Value.ToString(TimeFormat)
                                            : string.Empty,
                        CreationDateTime = s.CreationDateTime.ToString(TimeFormat),
                        Directions = s.DirectionScheduleTemplate.Count > 0
                                        ? s.DirectionScheduleTemplate.Select(d => d.Direction.DirectionName + " напр.").Aggregate((i, j) => i + ", " + j)
                                        : string.Empty,
                        Sectors = s.SectorScheduleTemplate.Count > 0
                                        ? s.SectorScheduleTemplate.Select(ss => ss.Sector.SectorName + "/" + ss.Sector.Direction.DirectionName + " напр.").Aggregate((i, j) => i + ", " + j)
                                        : string.Empty,
                        IsActive = s.IsActive
                    });

                return data.ToList();
            });
            itemsCount = itemsCountInner;
            return result;
        }

        /// <summary>
        /// Метод, возвращающий объект сущности ScheduleTemplate
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <returns>Объект сущности ScheduleTemplate</returns>
        public ScheduleTemplate GetScheduleTemplateById(int scheduleTemplateId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.ScheduleTemplate
                                                                  .Include(s => s.DirectionScheduleTemplate)
                                                                  .Include(s => s.SectorScheduleTemplate)
                                                                  .Include(s => s.ScheduleTemplateMode)
                                                                  .Include(s => s.ScheduleTemplateMode.Select(ss => ss.ScheduleTemplateDetail))
                                                                  .SingleOrDefault(s => s.ScheduleTemplateID == scheduleTemplateId));
        }

        /// <summary>
        ///  Метод, копирующий данные из объекта сущности ScheduleTemplate (вместе с навигационными свойствами) в объект модели ScheduleTemplateEditModel
        /// </summary>
        /// <param name="model">Объект модели ScheduleTemplateEditModel</param>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <returns>Истина в случае успеха завершения и ложь, если в базе нет объекта сущности ScheduleTemplate с указанным идентификатором</returns>
        public bool EditModelFromScheduleTemplate(ScheduleTemplateModel model, int scheduleTemplateId)
        {
            return GrokExceptionManager.Process(() =>
                {
                    var scheduleTemplate = this.GetScheduleTemplateById(scheduleTemplateId);
                    if (scheduleTemplate == null)
                    {
                        return false;
                    }

                    model.ScheduleTemplateId = scheduleTemplateId;
                    model.FullName = scheduleTemplate.FullName;
                    model.BeginDate = scheduleTemplate.BeginDate.ToString(TimeFormat);
                    model.EndDate = scheduleTemplate.EndDate.ToString(TimeFormat);
                    model.Directions = scheduleTemplate.DirectionScheduleTemplate.Select(d => d.DirectionID).ToList();
                    model.Sectors = scheduleTemplate.SectorScheduleTemplate.Select(s => s.SectorID).ToList();

                    return true;
                });
        }

        /// <summary>
        /// Метод, копирующий данные из объекта сущности ScheduleTemplate (вместе с навигационными свойствами) в объект модели ScheduleTemplateReviewModel
        /// </summary>
        /// <param name="model">Объект модели ScheduleTemplateReviewModel</param>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <returns>Истина в случае успеха завершения и ложь, если в базе нет объекта сущности ScheduleTemplate с указанным идентификатором</returns>
        public bool EditModelFromScheduleTemplate(ScheduleTemplateReviewModel model, int scheduleTemplateId)
        {
            return GrokExceptionManager.Process(() =>
            {
                var scheduleTemplate = this.GetScheduleTemplateById(scheduleTemplateId);
                if (scheduleTemplate == null)
                {
                    return false;
                }

                model.Name = scheduleTemplate.ShortName;
                model.Year = scheduleTemplate.ScheduleTemplateMode.Any()
                                 ? scheduleTemplate.ScheduleTemplateMode.First().ScheduleTemplateDetail.Any()
                                       ? scheduleTemplate.ScheduleTemplateMode.First().ScheduleTemplateDetail.First().Year
                                       : DateTime.MinValue.Year
                                 : DateTime.MinValue.Year;
                foreach (var mode in scheduleTemplate.ScheduleTemplateMode)
                {
                    model.Modes.Add(new ScheduleTemplateReviewModeModel()
                        {
                            ScheduleTemplateModeId = mode.ScheduleTemplateModeID,
                            Name = mode.Name
                        });
                }

                return true;
            });
        }

        /// <summary>
        /// Метод, устанавливающий активность (неактивность) шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <param name="isActive">Флаг активности шаблона</param>
        /// <param name="userName"></param>
        public void SetScheduleTemplateActivity(int scheduleTemplateId, bool isActive, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
               {
                   var scheduleTemplate =
                       context.ScheduleTemplate.SingleOrDefault(s => s.ScheduleTemplateID == scheduleTemplateId);

                   if (scheduleTemplate != null)
                   {
                       scheduleTemplate.IsActive = isActive;
                       context.SaveChanges();
                   }
               });

            #region Logging
            GrokLogger.LogScheduleTemplateEdit(scheduleTemplateId.ToString(), new Dictionary<string, string> { { "IsActive", isActive.ToString() } }, userName);
            #endregion
        }

        /// <summary>
        /// Метод, проверяющий существование шаблона графика кассиров с определённым идентификатором
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <returns>Истина, если шаблон графика кассиров с указанным идентификатором существует, и ложь в противном случае</returns>
        public bool ScheduleTemplateWithIdExists(int scheduleTemplateId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.ScheduleTemplate.Count(s => s.ScheduleTemplateID == scheduleTemplateId) == 1);
        }

        /// <summary>
        /// Метод, формирующий информацию о режиме шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateModeId">Идентифкатор режима шаблона графика кассиров</param>
        /// <returns>Список объектов модели ScheduleTemplateReviewDetailModel</returns>
        public List<ScheduleTemplateReviewDetailModel> GetScheduleTemplateDetailsForMode(int scheduleTemplateModeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                //Считываем все записи ScheduleTemplateDetail из базы, которые связаны с указанным scheduleTemplateModeId
                var details =
                    context.ScheduleTemplateDetail
                            .Include(s => s.DayType)
                            .Where(s => s.ScheduleTemplateModeID == scheduleTemplateModeId)
                            .ToList();

                var gridDetails = new List<ScheduleTemplateReviewDetailModel>();

                foreach (var detail in details)
                {
                    if (gridDetails.Any(g => g.MonthNumber == detail.Month))
                    {
                        //Если запись ScheduleTemplateDetail не для первого дня месяца, и
                        //объект модели ScheduleTemplateReviewDetailModel для строки таблицы
                        //режима шаблона графика кассиров уже существует, то находим его
                        //и добавляем информацию о текущем дне (переменная detail)
                        var gridDetail = gridDetails.Single(g => g.MonthNumber == detail.Month);

                        gridDetail.AddDayDataToReviewDetailModel(detail);
                    }
                    else
                    {
                        //Если запись ScheduleTemplateDetail для первого дня месяца, то
                        //создаём новый объект модели ScheduleTemplateReviewDetailModel для
                        //строки таблицы режима шаблона графика кассиров
                        var scheduleTemplateReviewDetailModel =
                            ScheduleTemplateReviewExtensions.CreateReviewDetailModel(detail);
                        gridDetails.Add(scheduleTemplateReviewDetailModel);
                    }
                }

                return gridDetails;
            });
        }

        /// <summary>
        /// Метод, формирующий информацию о конкретном месяце из первого режима шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateId">Идентифкатор шаблона графика кассиров</param>
        /// <param name="month">Номер месяца</param>
        /// <returns>Список объектов модели ScheduleTemplateReviewDetailModel</returns>
        public List<ScheduleTemplateReviewDetailModel> GetMonthScheduleTemplateDetails(int scheduleTemplateId, int month)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var mode = context.ScheduleTemplateMode
                                      .FirstOrDefault(s => s.ScheduleTemplateID == scheduleTemplateId);

                    if (mode != null)
                    {
                        var details = context.ScheduleTemplateDetail
                                             .Include(s => s.ScheduleTemplateMode)
                                             .Where(
                                                 s => s.ScheduleTemplateMode.ScheduleTemplateID == scheduleTemplateId &&
                                                      s.ScheduleTemplateModeID == mode.ScheduleTemplateModeID &&
                                                      s.Month == month)
                                             .ToList();

                        if (details.Any())
                        {
                            var gridDetails = new List<ScheduleTemplateReviewDetailModel>();

                            foreach (var detail in details)
                            {
                                if (gridDetails.Any())
                                {
                                    //Прочие дни месяца
                                    var gridDetail = gridDetails.First();

                                    gridDetail.AddDayDataToReviewDetailModel(detail);
                                }
                                else
                                {
                                    //Первый день месяца
                                    var scheduleTemplateReviewDetailModel =
                                        ScheduleTemplateReviewExtensions.CreateReviewDetailModel(detail);

                                    gridDetails.Add(scheduleTemplateReviewDetailModel);
                                }                                
                            }

                            return gridDetails;
                        }
                    }

                    return new List<ScheduleTemplateReviewDetailModel>();                   
                });
        }

        /// <summary>
        /// Метод, возвращающий функцию сортировки
        /// </summary>
        /// <param name="orderBy">Параметры сортировки</param>
        /// <returns>Функция сортировки</returns>
        private Func<IQueryable<ScheduleTemplate>, IOrderedQueryable<ScheduleTemplate>> GetTerminalOrderFunction(string orderBy)
        {
            //ToDo LogExc
            var sortDescription = SortDescriptionCreateHelper.FactoryMethod(orderBy);

            switch (sortDescription.Field)
            {
                case "ShortName":
                    return query => ScheduleTemplateSorter.OrderByShortName(query, sortDescription.IsAscending);
                case "FullName":
                    return query => ScheduleTemplateSorter.OrderByFullName(query, sortDescription.IsAscending);
                case "BeginDate":
                    return query => ScheduleTemplateSorter.OrderByBeginDate(query, sortDescription.IsAscending);
                case "EndDate":
                    return query => ScheduleTemplateSorter.OrderByEndDate(query, sortDescription.IsAscending);
                case "Author":
                    return query => ScheduleTemplateSorter.OrderByAuthor(query, sortDescription.IsAscending);
                case "LastAuthor":
                    return query => ScheduleTemplateSorter.OrderByLastAuthor(query, sortDescription.IsAscending);
                case "CreationDateTime":
                    return query => ScheduleTemplateSorter.OrderByCreationDate(query, sortDescription.IsAscending);
                case "ChangeDateTime":
                    return query => ScheduleTemplateSorter.OrderByChangeDateTime(query, sortDescription.IsAscending);
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field + "\' не поддерживается");
            }
        }

        /// <summary>
        /// Метод, возвращающий список функций фильтрации
        /// </summary>
        /// <param name="filterDictionary">Коллекция параметров фильтрации</param>
        /// <returns>Список функций фильтрации</returns>
        private List<Func<IQueryable<ScheduleTemplate>, IQueryable<ScheduleTemplate>>> GetFilterFunctions(NameValueCollection filterDictionary)
        {
            //ToDo LogExc
            List<Func<IQueryable<ScheduleTemplate>, IQueryable<ScheduleTemplate>>> queries = new List<Func<IQueryable<ScheduleTemplate>, IQueryable<ScheduleTemplate>>>();

            var actionTime = filterDictionary["ActionTime"];
            if (!string.IsNullOrWhiteSpace(actionTime))
            {
                DateTime actionTimeDateTime;

                if (!DateTime.TryParse(actionTime, out actionTimeDateTime))
                {
                    actionTimeDateTime = DateTime.Now;
                }

                queries.Add(query => ScheduleTemplateFilter.FilterByActionTime(query, RoundDateToMonth(actionTimeDateTime, false), RoundDateToMonth(actionTimeDateTime, true)));
            }

            var activityMode = filterDictionary["ShowByActivity"];
            if (!string.IsNullOrWhiteSpace(activityMode))
            {
                queries.Add(query => ScheduleTemplateFilter.FilterByActivity(query, activityMode));
            }

            return queries;
        }

        /// <summary>
        /// Метод, возвращающий объекты сущности ScheduleTemplate
        /// </summary>
        /// <returns>Объекты сущности ScheduleTemplate</returns>
        public Dictionary<int, string> GetScheduleTemplates(List<int> currentDirectionIds, List<int> currentSectorIds)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.ScheduleTemplate
                                                                        .Include(s => s.DirectionScheduleTemplate)
                                                                        .Include(s => s.SectorScheduleTemplate)
                                                                        .Include(s => s.ScheduleTemplateMode)
                                                                        .Where(
                                                                            e =>
                                                                            ((e.DirectionScheduleTemplate.Count == 0) &&
                                                                             (e.SectorScheduleTemplate.Count == 0)) ||
                                                                             ((e.DirectionScheduleTemplate.Any(
                                                                                d =>
                                                                                currentDirectionIds.Any(
                                                                                    c => d.DirectionID == c))) ||
                                                                              (e.SectorScheduleTemplate.Any(
                                                                                 s =>
                                                                                 currentSectorIds.Any(
                                                                                     c => s.SectorID == c)))))
                                                                        .OrderBy(e => e.ShortName)
                                                                        .ToDictionary(e => e.ScheduleTemplateID,
                                                                                      e => e.ShortName));
        }

        /// <summary>
        /// Метод, проверяющий, активен ли шаблон графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <returns>Флаг активности шаблона</returns>
        public bool IsScheduleTemplateActive(int scheduleTemplateId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var scheduleTemplate =
                        context.ScheduleTemplate.SingleOrDefault(s => s.ScheduleTemplateID == scheduleTemplateId);

                    return scheduleTemplate != null && scheduleTemplate.IsActive;
                });
        }

        /// <summary>
        /// Метод, возвращающий имя краткое шаблона по идентификатору
        /// </summary>
        /// <returns></returns>
        public string GetTemplateNamebyId(int scheduleTemplateId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var scheduleTemplate =
                        context.ScheduleTemplate.FirstOrDefault(s => s.ScheduleTemplateID == scheduleTemplateId);

                    return scheduleTemplate != null
                               ? scheduleTemplate.ShortName
                               : string.Empty;
                });
        }

        /// <summary>
        /// Метод, формирующий строку "Итого" в таблице шаблона графиков
        /// </summary>
        /// <param name="scheduleTemplateReviewModeMonths">Список моделей, описывающих месяцы</param>
        /// <returns>Модель, описывающая строку "Итого" в таблице шаблона графиков</returns>
        public ScheduleTemplateReviewDetailModel GetFinalString(
            List<ScheduleTemplateReviewDetailModel> scheduleTemplateReviewModeMonths)
        {
            //ToDo LogExc
            return new ScheduleTemplateReviewDetailModel()
             {
                 MonthName = "Итого",
                 WorkDaysNum = scheduleTemplateReviewModeMonths.Select(st => st.WorkDaysNum).Sum(),
                 HoursNum = scheduleTemplateReviewModeMonths.Select(st => st.HoursNum).Sum(),
                 NightHoursNum = scheduleTemplateReviewModeMonths.Select(st => st.NightHoursNum).Sum(),
                 HolidaysHoursNum = scheduleTemplateReviewModeMonths.Select(st => st.HolidaysHoursNum).Sum(),
                 Days = new List<string>(),
                 Holidays = new List<bool>(),
                 Nights = new List<string>(),                 
                 DayTypes = new List<string>(),
                 AreSplitted = new List<string>(),
                 MonthNumber = -1
             };
        }

        /// <summary>
        /// Метод, проверяющий наличие шаблона графиков с определённым кратким наименованием 
        /// (то есть именем файла-шаблона)
        /// </summary>
        /// <param name="shortName">Краткое наименование шаблона графиков</param>
        /// <returns>Истина, если шаблон у сказанным кратким наименованием уже существует,
        ///          и ложь в противном случае</returns>
        public bool ScheduleTemplateWithShortNameExists(string shortName)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                if (String.IsNullOrWhiteSpace(shortName))
                {
                    return false;
                }

                return context.ScheduleTemplate.Count(s => s.ShortName.Equals(shortName, StringComparison.InvariantCultureIgnoreCase)) == 1;
            });
        }

        /// <summary>
        /// Округляет дату до месяца
        /// </summary>
        /// <param name="date">Дата для округления</param>
        /// <param name="toNextMonth">Округлять ли до следующего месяца</param>
        /// <returns>Округленную дату</returns>
        private DateTime RoundDateToMonth(DateTime date, bool toNextMonth)
        {
            //ToDo LogExc
            if (toNextMonth)
            {
                var year = date.Month == 12
                               ? date.Year + 1
                               : date.Year;
                return new DateTime(year, (date.Month + 12) % 12 + 1, 1);
            }
            else
            {
                return new DateTime(date.Year, date.Month, 1);
            }
        }
    }
}
