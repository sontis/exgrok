﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Business.Extensions;
using System.Collections.Specialized;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class StationRepository
    {
        public StationRepository()
        {

        }

        /// <summary>
        /// Метод, возвращающий все станции
        /// </summary>
        /// <returns>Список объектов StationModel</returns>
        public List<StationModel> GetAllStations()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.Station.Select(s => new StationModel
                        {
                            StationId = s.StationID,
                            StationNumber = s.StationNumber,
                            StationName = s.StationName
                        });

                    return query.ToList();
                });
        }

        /// <summary>
        /// Получение списка станций с секторами
        /// </summary>
        /// <returns>Список станций с секторами</returns>
        public List<StationModel> GetAllStationsWithSectors()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var result = new List<StationModel>();
                    var sectorStations = context.SectorStation.ToArray();
                    var stations = context.Station.ToArray();
                    foreach (var sectorStation in sectorStations)
                    {
                        var station = stations.SingleOrDefault(s => s.StationID == sectorStation.StationID);
                        result.Add(new StationModel()
                            {
                                StationId = station.StationID,
                                StationNumber = station.StationNumber,
                                StationName = station.StationName,
                                SectorId = sectorStation.SectorID
                            });
                    }

                    return result;
                });
        }

        /// <summary>
        /// Метод, сообщающий, содержит ли участок станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Истина, если на участке есть станции, и ложь в противном случае</returns>
        public bool IsAnyStationsOnSector(int sectorId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.SectorStation.Any(s => s.SectorID == sectorId));
        }

        /// <summary>
        /// Метод, возвращающий листья-станции дерева для узла-участка дерева
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="scheduleStatuses">Список статусов расписаний для фильтрации</param>
        /// <returns>Список объектов TreeLeafModel</returns>
        public List<TreeLeafModel> GetStationsBySector(int sectorId, List<int> scheduleStatuses = null)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var stations = context.SectorStation
                                      .Include(s => s.Station)
                                      .Where(s => s.SectorID == sectorId)
                                      .ToList();

                if ((scheduleStatuses != null) && (scheduleStatuses.Count > 0))
                {
                    var stationsList = new HashSet<int>();

                    var stationHistories = new Dictionary<int, Tuple<int, ScheduleStatusHistory>>();

                    foreach (var scheduleStatusHistory in context.ScheduleStatusHistory.Where(e => (e.Schedule.SectorStation.SectorID == sectorId) && (e.Schedule.ScheduleDetail.Count > 0)))
                    {
                        if (stationHistories.ContainsKey(scheduleStatusHistory.ScheduleID))
                        {
                            if (stationHistories[scheduleStatusHistory.ScheduleID].Item2.BeginDate < scheduleStatusHistory.BeginDate)
                            {
                                stationHistories[scheduleStatusHistory.ScheduleID] = new Tuple<int, ScheduleStatusHistory>(scheduleStatusHistory.ScheduleStatusID, scheduleStatusHistory);
                            }
                        }
                        else
                        {
                            stationHistories.Add(scheduleStatusHistory.ScheduleID, new Tuple<int, ScheduleStatusHistory>(scheduleStatusHistory.ScheduleStatusID, scheduleStatusHistory));
                        }
                    }

                    foreach (var stationHistory in stationHistories.Where(e => scheduleStatuses.Contains(e.Value.Item1)))
                    {
                        if (!stationsList.Contains(stationHistory.Value.Item2.Schedule.SectorStation.StationID))
                        {
                            stationsList.Add(stationHistory.Value.Item2.Schedule.SectorStation.StationID);
                        }
                    }

                    stations = stations.Where(e => stationsList.Contains(e.StationID)).ToList();
                }

                var result = stations.OrderBy(s => s.OrderNumber).Select(s => new TreeLeafModel
                    {
                        ObjectId = s.Station.StationID,
                        ObjectName = s.Station.StationName
                    });


                return result.ToList();
            });
        }

        /// <summary>
        /// Метод, возвращающий объект сущности Station
        /// </summary>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Объект сущности Station</returns>
        private Station GetStationById(int stationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Station.Include(s => s.SectorStation).SingleOrDefault(s => s.StationID == stationId));
        }

        /// <summary>
        /// Метод, возвращающий название станции
        /// </summary>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Название станции</returns>
        public string GetStationNameById(int stationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var station = context.Station.SingleOrDefault(s => s.StationID == stationId);

                    return station == null
                               ? null
                               : station.StationName;
                });
        }

        /// <summary>
        /// Метод, возвращающий список идентификаторов участков, на которых находится станция
        /// </summary>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Список идентификаторо участков, на которых находится станция</returns>
        public List<int> GetCurrentSectorIds(int stationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.SectorStation
                                                                        .Where(s => s.StationID == stationId)
                                                                        .Select(s => s.SectorID)
                                                                        .ToList());
        }

        /// <summary>
        /// Метод, возвращающий список идентификаторов направлений, на которых находится станция
        /// </summary>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Список идентификаторов направлений, на которых находится станция</returns>
        public List<int> GetCurrentDirectionIds(int stationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.SectorStation            
                                                                .Include(s => s.Sector)
                                                                .Where(s => s.StationID == stationId)                           
                                                                .Select(s => s.Sector.DirectionID)
                                                                .ToList());
        }

        /// <summary>
        /// Метод возвращает Id объекта SectorStaion, соответствующий id участка и станции
        /// </summary>
        /// <param name="stationId">Id станции</param>
        /// <param name="sectorId">Id участка</param>
        /// <returns>Id объекта SectorStation</returns>
        public int GetSectorStationId(int stationId, int sectorId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.SectorStation.SingleOrDefault(s => s.StationID == stationId && s.SectorID == sectorId).SectorStationID);
        }

        /// <summary>
        /// Возвращает полное наименование станции
        /// </summary>
        /// <param name="stationId">Id станции</param>
        /// <param name="sectorId">Id участка</param>
        /// <returns>Полное наименование станции</returns>
        public string GetStationFullName(int stationId, int sectorId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var sectorStation = context.SectorStation.SingleOrDefault(s => s.StationID == stationId && s.SectorID == sectorId);
                var stationName = sectorStation.Station.StationName;
                var sectorName = sectorStation.Sector.SectorName;
                var directionName = sectorStation.Sector.Direction.DirectionName;

                return string.Format("{0} - {1} - {2}", directionName, sectorName, stationName);
            });
        }

        /// <summary>
        /// Метод, копирующий данные из объектов сущностей Station и Sector в объект модели StationPassportModel
        /// </summary>
        /// <param name="model">Объект модели StationPassportModel</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="sectorRepository">Объект sectorRepository - для получения объекта сущности Sector</param>
        /// <returns>Объект модели StationPassportModel</returns>
        public bool EditModelFromStationAndSector(StationPassportModel model, int sectorId, int stationId, SectorRepository sectorRepository)
        {
            //ToDo LogExc
            var station = this.GetStationById(stationId);
            var sector = sectorRepository.GetSectorById(sectorId);

            if (station == null || sector == null)
            {
                return false;
            }

            model.Edit(station, sector);

            return true;
        }

        /// <summary>
        /// Метод, возвращающий все смены станции
        /// </summary>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Список объектов StationEmployeeShift</returns>
        public List<EmployeeShiftModel> GetAllShifts(int stationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.StationEmployeeShift.Where(e => e.StationID == stationId)
                                       .Select(e => new EmployeeShiftModel
                                        {
                                            Id = e.StationEmployeeShiftID,
                                            Name = e.Name
                                        }).ToList());
        }

        /// <summary>
        /// Метод, возвращающий расписание смены
        /// </summary>
        /// <param name="shiftId">Идентификатор смены</param>
        /// <returns>Список объектов StationEmployeeShiftSchedule</returns>
        public List<EmployeeShiftScheduleModel> GetAllShiftSchedule(int shiftId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.StationEmployeeShiftSchedule
                                                                        .Where(e => e.StationEmployeeShiftID == shiftId)
                                                                        .Include(e => e.WeekDay)
                                                                        .ToList()
                                                                        .Select(e =>
                                                                        {
                                                                            var result = new EmployeeShiftScheduleModel()
                                                                            {
                                                                                Id = e.StationEmployeeShiftScheduleID,
                                                                                WeekDay = e.WeekDay.WeekDayNumber,
                                                                                WeekDayName = e.WeekDay.WeekDayName,
                                                                            };
                                                                            if (e.BeginTime != e.EndTime)
                                                                            {
                                                                                result.BeginWorkTime = e.BeginTime.ToString(@"hh\:mm");
                                                                                result.EndWorkTime = e.EndTime.ToString(@"hh\:mm");
                                                                                result.WorkTime = string.Format("{0} - {1}", result.BeginWorkTime, result.EndWorkTime);
                                                                                if (e.BreakBeginTime != e.BreakEndTime)
                                                                                {
                                                                                    result.BeginBreakTime = e.BreakBeginTime.ToString(@"hh\:mm");
                                                                                    result.EndBreakTime = e.BreakEndTime.ToString(@"hh\:mm");
                                                                                    result.BreakTime = string.Format("{0} - {1}", result.BeginBreakTime, result.EndBreakTime);
                                                                                }
                                                                            }
                                                                            return result;
                                                                        }).ToList());
        }

        /// <summary>
        /// Метод, сохраняющий расписания на станции
        /// </summary>
        /// <param name="endBreakTime"></param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="shiftScheduleId"></param>
        /// <param name="beginWorkTime"></param>
        /// <param name="endWorkTime"></param>
        /// <param name="beginBreakTime"></param>
        public void SaveEmployeeShiftSchedule(int shiftScheduleId, string beginWorkTime, string endWorkTime, string beginBreakTime, string endBreakTime, int stationId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var shiftSchedule = context.StationEmployeeShiftSchedule.First(e => e.StationEmployeeShiftScheduleID == shiftScheduleId);

                    TimeSpan time;
                    shiftSchedule.BeginTime = TimeSpan.TryParse(beginWorkTime, out time) ? time : new TimeSpan();
                    shiftSchedule.EndTime = TimeSpan.TryParse(endWorkTime, out time) ? time : new TimeSpan();
                    shiftSchedule.BreakBeginTime = TimeSpan.TryParse(beginBreakTime, out time) ? time : new TimeSpan();
                    shiftSchedule.BreakEndTime = TimeSpan.TryParse(endBreakTime, out time) ? time : new TimeSpan();

                    context.SaveChanges();
                });
        }

        /// <summary>
        /// Метод, создающий новую смену на станции
        /// </summary>
        /// <param name="stationId">Идентификатор станции</param>
        public void CreateEmployeeShift(int stationId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var schedules = context.StationEmployeeShift.Where(e => e.StationID == stationId);
                    var resultName = 1;
                    foreach (var stationEmployeeShift in schedules)
                    {
                        int index;
                        if (int.TryParse(stationEmployeeShift.Name, out index))
                        {
                            if (index >= resultName) resultName = index + 1;
                        }
                    }

                    var shift = context.StationEmployeeShift.Create();
                    shift.Name = resultName.ToString(CultureInfo.InvariantCulture);
                    shift.StationID = stationId;
                    context.StationEmployeeShift.Add(shift);

                    foreach (var weekday in context.WeekDay)
                    {
                        var shiftSchedule = context.StationEmployeeShiftSchedule.Create();
                        context.StationEmployeeShiftSchedule.Add(shiftSchedule);

                        shiftSchedule.WeekDay = weekday;
                        shiftSchedule.StationEmployeeShift = shift;
                    }

                    context.SaveChanges();
                });
        }

        /// <summary>
        /// Метод, удаляющий смену на станции
        /// </summary>
        /// <param name="shiftId">Идентификатор смены</param>
        public void DeleteEmployeeShift(int shiftId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var shift = context.StationEmployeeShift.First(e => e.StationEmployeeShiftID == shiftId);

                    var shiftSchedules = context.StationEmployeeShiftSchedule.Where(e => e.StationEmployeeShiftID == shiftId);

                    foreach (var stationEmployeeShiftSchedule in shiftSchedules)
                    {
                        context.StationEmployeeShiftSchedule.Remove(stationEmployeeShiftSchedule);
                    }
                    context.StationEmployeeShift.Remove(shift);

                    context.SaveChanges();
                });
        }

        /// <summary>
        /// Метод, изменяющий номер смены на станции
        /// </summary>
        /// <param name="shiftId">Идентификатор смены</param>
        /// <param name="number"></param>
        public void ChangeEmployeeShiftNumber(int shiftId, int number)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var shift = context.StationEmployeeShift.First(e => e.StationEmployeeShiftID == shiftId);
                    shift.Name = number.ToString(CultureInfo.InvariantCulture);
                    context.SaveChanges();
                });
        }

        /// <summary>
        /// Метод, возвращающий список станций участка
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Список станций участка</returns>
        public Dictionary<int, string> GetAllSectorStations(int sectorId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                    context.SectorStation
                           .Include(s => s.Station)
                           .Where(s => s.SectorID == sectorId)
                           .OrderBy(s => s.OrderNumber)
                           .ToDictionary(s => s.Station.StationID,
                                         s => s.Station.StationName));
        }

        /// <summary>
        /// Метод, возвращающий список станций, для которых ещё не назначены приоритеты
        /// у указанного работника
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="employeeId">Идентификатор кассира</param>
        /// <returns>Список объектов DropDownListItemModel</returns>
        public Dictionary<int, string> GetStationsForPriority(int sectorId, int employeeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
               context.SectorStation
                           .Include(s => s.Station)
                           .Include(s => s.Station.EmployeeStationPriority)
                           .Where(s => s.SectorID == sectorId)
                           .Where(s => s.Station.EmployeeStationPriority.Count(e => e.EmployeeID == employeeId) == 0)
                           .OrderBy(s => s.OrderNumber)
                           .ToDictionary(s => s.StationID,
                                         s => s.Station.StationName));
        }
    }
}
