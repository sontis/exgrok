﻿using System.Collections.Generic;
using Bell.Grok.Model;
using Bell.Grok.DAL.Dao;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий для таблицы calendar
    /// </summary>
    public class CalendarRepository
    {
        /// <summary>
        /// Dao для доступа к календарю
        /// </summary>
        private CalendarDao dao = new CalendarDao();

        /// <summary>
        /// Метод, по сути возвращающий проивзодственный календарь
        /// </summary>
        /// <returns>Список объектов модели CalendarModel</returns>
        public List<CalendarModel> GetAll()
        {
            return dao.GetAll();
        }

        /// <summary>
        /// Присутствуют ли данные для указанного года в базе данных
        /// </summary>
        /// <param name="year">Год</param>
        /// <returns>True - данные на этот год имеются</returns>
        public bool IsYearInCalendar(int year)
        {
            return dao.IsYearInCalendar(year);
        }
    }
}
