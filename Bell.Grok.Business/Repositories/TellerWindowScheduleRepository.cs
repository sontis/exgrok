﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Extensions;
using Bell.Grok.DAL;
using Bell.Grok.DAL.Dao;
using Bell.Grok.Model;
using Bell.Grok.Model.Enums;

namespace Bell.Grok.Business
{    
    /// <summary>
    /// Репозиторий для работы со срезами расписаний работы кассовых окон
    /// </summary>
    public class TellerWindowScheduleRepository
    {
        /// <summary>
        /// Объект доступа к данным
        /// </summary>        
        private readonly TellerWindowScheduleDao dao = new TellerWindowScheduleDao();

        /// <summary>
        /// Метод, возвращающий сезоны расписаний работы кассовых окон
        /// </summary>
        /// <returns>Список сезонов расписаний работы кассовых окон</returns>
        public List<SeasonModel> GetSeasons()
        {
            return TellerWindowScheduleDao.Seasons.Select(s => new SeasonModel()
                {
                    SeasonId = s.SeasonId,
                    SeasonName = s.SeasonName
                }).ToList();
        }

        /// <summary>
        /// Метод, возвращающий сезон по его идентификатору
        /// </summary>
        /// <param name="id">Идентификатор сезоны</param>
        /// <returns>Объект сезона</returns>
        public SeasonModel GetSeasonById(int id)
        {
            var season = TellerWindowScheduleDao.Seasons.FirstOrDefault(s => s.SeasonId == id);

            return season == null
                       ? null
                       : new SeasonModel()
                           {
                               SeasonId = season.SeasonId,
                               SeasonName = season.SeasonName
                           };
        }

        /// <summary>
        /// Метод, возвращающий типы часов в расписаниях работы кассовых окон
        /// </summary>
        /// <returns>Список типов часов в расписаниях работы кассовых окон</returns>
        public List<HourTypeModel> GetHourTypes()
        {
            return TellerWindowScheduleDao.HourTypes.Select(h => new HourTypeModel()
                {
                    HourTypeId = h.HourTypeId,                    
                    HourTypeCode = h.HourTypeCode
                }).ToList();
        }

        /// <summary>
        /// Метод, возвращающий типы дней в расписаниях работы кассовых окон
        /// </summary>
        /// <returns>Список типов дней в расписаниях работы кассовых окон</returns>
        public List<TellerWindowScheduleDayTypeModel> GetTellerWindowScheduleDayTypes(int year, int seasonId, int sectorStationId)
        {               
            var scheduleStationId = dao.GetStationScheduleId(seasonId == (int)TellerWindowScheduleSeasons.Winter, 
                                                             year, sectorStationId);

            return TellerWindowScheduleDao.TellerWindowScheduleDayTypes.Select(s => new TellerWindowScheduleDayTypeModel()
                {
                    TellerWindowScheduleDayTypeId = s.TellerWindowScheduleDayTypeId,
                    TellerWindowScheduleDayTypeName = s.TellerWindowScheduleDayTypeName,
                    AreDataInBase = dao.AreStationScheduleDetails(scheduleStationId, s.TellerWindowScheduleDayTypeId == (int) TellerWindowScheduleDayTypes.Holidays)
                }).ToList();
        }

        /// <summary>
        /// Метод, возвращающий все расписания работы кассовых окон по станции
        /// </summary>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Все расписания работы кассовых окон по станции</returns>
        public List<TellerWindowScheduleModel> GetTellerWindowScheduleModels(int sectorStationId)
        {
            return dao.GetAllStationSchedules(sectorStationId)
                                      .Select(s => new TellerWindowScheduleModel()
                                          {
                                              TellerWindowScheduleModelId = s.StationScheduleID,
                                              SectorStationId = s.StationScheduleID,
                                              Year = s.Year,
                                              Season = new SeasonModel()
                                                  {
                                                      SeasonId = s.IsWinter
                                                                    ? (int)TellerWindowScheduleSeasons.Winter
                                                                    : (int)TellerWindowScheduleSeasons.Summer,
                                                      SeasonName = s.IsWinter
                                                                    ? TellerWindowScheduleSeasons.Winter.ToText()
                                                                    : TellerWindowScheduleSeasons.Summer.ToText()
                                                  }
                                          }).ToList();
        }

        /// <summary>
        /// Метод, добавляющий новый объект расписания работы кассовых окон
        /// </summary>
        /// <param name="model">Объект модели расписания работы кассовых окон</param>
        public void CreateTellerWindowScheduleModel(TellerWindowScheduleModel model)
        {            
            var stationSchedule = new StationSchedule()
                {
                    IsWinter = model.Season.SeasonId == (int)TellerWindowScheduleSeasons.Winter,
                    Year = (short) model.Year,
                    SectorStationID = model.SectorStationId
                };

            dao.AddStationSchedule(stationSchedule);
            model.TellerWindowScheduleModelId = stationSchedule.StationScheduleID;
        }

        /// <summary>
        /// Метод, валидирующий расписания работы кассовых окон на идентичность (нет ли дубликатов)
        /// </summary>
        /// <param name="model">Объект расписания работы кассовых окон</param>
        /// <returns>Результат проверки на дубликаты</returns>
        public bool DoesTellerWindowScheduleModelExist(TellerWindowScheduleModel model)
        {
            return dao.DoesStationScheduleExist(model.Season.SeasonId == (int)TellerWindowScheduleSeasons.Winter,
                                                                model.Year,
                                                                model.SectorStationId);
        }

        /// <summary>
        /// Метод, возвращающий содержимое расписания работы кассовых окон
        /// </summary>
        /// <param name="seasonId">Сезон расписания</param>
        /// <param name="tellerWindowScheduleDayTypeId">Тип дней</param>
        /// <param name="year">Год</param>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Содержимое расписания работы кассовых окон</returns>
        public List<TellerWindowScheduleContentModel> GetTellerWindowScheduleContentModels(int seasonId, int tellerWindowScheduleDayTypeId, int year, int sectorStationId)
        {
            var tellerWindowRepository = new TellerWindowRepository();
            var stationTellerWindows = tellerWindowRepository.GetAllNumbersForStation(sectorStationId);

            var result = dao.GetTellerWindowScheduleModels(seasonId == (int)TellerWindowScheduleSeasons.Winter, 
                                                           tellerWindowScheduleDayTypeId == (int)TellerWindowScheduleDayTypes.Holidays, 
                                                           tellerWindowScheduleDayTypeId, year, sectorStationId, stationTellerWindows);

            result.ForEach(r =>
                {
                    r.CalculateWorkPeriodsString();
                    r.CalculateHaveBreakFlags();
                });

            return result;
        }       

        /// <summary>
        /// Метод, обновляющий объект с содержимым расписания работы кассовых окон
        /// </summary>
        /// <param name="models">>Список моделей с содержимым расписания работы кассовых окон</param>
        public void UpdateStationScheduleDetails(List<TellerWindowScheduleContentModel> models)
        {
            dao.UpdateStationScheduleDetails(models);

            models.ForEach(m =>
                {                    
                    m.CalculateWorkPeriodsString();
                    m.CalculateHaveBreakFlags();                             
                });
        }        

        /// <summary>
        /// Метод, проверяющий наличие данных из ЭСАПР для определённого расписания
        /// работы кассовых окон
        /// </summary>
        /// <param name="seasonId">Сезон</param>
        /// <param name="tellerWindowScheduleDayTypeId">Тип дней</param>
        /// <param name="year">Год</param>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Результат проверки наличия данных из ЭСАПР для определённого расписания</returns>
        public bool AreRecommendedData(int seasonId, int tellerWindowScheduleDayTypeId, int year, int sectorStationId)
        {
            return dao.AreRecommendedData(seasonId == (int)TellerWindowScheduleSeasons.Winter, 
                                          tellerWindowScheduleDayTypeId == (int)TellerWindowScheduleDayTypes.Holidays, 
                                          year, sectorStationId);           
        }
    }
}
