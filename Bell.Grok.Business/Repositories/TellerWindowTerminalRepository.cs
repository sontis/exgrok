﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Business.Extensions;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class TellerWindowTerminalRepository
    {
        /// <summary>
        /// Формат времени для конвертации значения DateTime в строку
        /// </summary>
        private const string TimeFormat = "dd.MM.yyyy";

        public TellerWindowTerminalRepository()
        {
            
        }        

        /// <summary>
        /// Метод, возвращающий привязку терминала к кассовому окну по идентификатору
        /// </summary>
        /// <param name="tellerWindowTerminalId">Идентификатор привязки терминала к кассовому окну</param>
        /// <returns>Привязка терминала к кассовому окну</returns>
        public TellerWindowTerminal GetTellerWindowTerminalById(int tellerWindowTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindowTerminal.
                                                                   Include(t => t.TellerWindow).
                                                                   Include(t => t.SectorStationTerminal).
                                                                   Include(t => t.SectorStationTerminal.Terminal).
                                                                   SingleOrDefault(t => t.TellerWindowTerminalID == tellerWindowTerminalId));
        }

        /// <summary>
        /// Метод, проверяющий существование привязки терминала к кассовому окну с определённым идентификатором
        /// </summary>
        /// <param name="tellerWindowTerminalId">Идентификатор привязки терминала к кассовому окну</param>
        /// <returns>Истина, если привязка терминала к кассовому окну с указанным идентификатором существует, и ложь в противном случае</returns>
        public bool TellerWindowTerminalWithIdExists(int tellerWindowTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindowTerminal.Any(t => t.TellerWindowTerminalID == tellerWindowTerminalId));
        }

        /// <summary>
        /// Метод, возвращающий все привязки к кассовым окнам для определённой привязки терминала к станции
        /// </summary>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Список привязок к кассовым окнам указанной привязки терминала к станции</returns>
        internal List<TellerWindowTerminal> GetAllTellerWindowTerminalsBySectorStationTerminalId(int sectorStationTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindowTerminal.
                                                                   Include(t => t.SectorStationTerminal).
                                                                   Include(t => t.SectorStationTerminal.Terminal).
                                                                   Include(t => t.TellerWindow).
                                                                   Where(t => t.SectorStationTerminalID == sectorStationTerminalId &&
                                                                              t.SectorStationTerminal.SectorStationID == t.TellerWindow.SectorStationID).
                                                                   ToList());
        }        

        /// <summary>
        /// Метод, проверяющий наличие привязки ккт к ко 
        /// с уникальным набором параметров
        /// </summary>
        /// <param name="beginDate">Дата начала привязки</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns></returns>
        internal TellerWindowTerminal IsSuchTellerWindowTerminal(DateTime beginDate, int sectorStationTerminalId, int tellerWindowId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindowTerminal
                                                                        .Include(t => t.SectorStationTerminal)
                                                                        .Include(t => t.SectorStationTerminal.Terminal)
                                                                        .Include(t => t.TellerWindow)
                                                                        .SingleOrDefault(t => t.BeginDate == beginDate &&
                                                                                              t.SectorStationTerminalID == sectorStationTerminalId &&
                                                                                              t.TellerWindowID == tellerWindowId));
        }

        /// <summary>
        /// Метод, возвращающий все привязки определённого кассового окна к терминалам
        /// </summary>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>Список привязок указанного кассового окна к терминалам</returns>
        public List<TellerWindowTerminal> GetAllTellerWindowTerminalsByTellerWindowId(int tellerWindowId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindowTerminal.
                                                                   Include(t => t.SectorStationTerminal).
                                                                   Include(t => t.SectorStationTerminal.Terminal).
                                                                   Include(t => t.TellerWindow).
                                                                   Where(t => t.TellerWindowID == tellerWindowId &&
                                                                              t.SectorStationTerminal.SectorStationID == t.TellerWindow.SectorStationID)
                                                                  .ToList());
        }

        /// <summary>
        /// Метод, возвращающий строку со списком номеров терминалов, находящихся
        /// в данное время или в будущем в данном кассовом окне
        /// </summary>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>
        ///   Строка со списком номеров терминалов, находящихся
        ///   в данное время или в будущем в данном кассовом окне
        /// </returns>
        public string GetSetTerminalNumbers(int tellerWindowId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var now = DateTime.Now;
                    var currentDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

                    var numberAndProfilesList = context.TellerWindowTerminal
                                                       .Include(t => t.SectorStationTerminal)
                                                       .Include(t => t.SectorStationTerminal.Terminal)
                                                       .Include(t => t.SectorStationTerminal.Terminal.TerminalProfile)
                                                       .Include(t => t.TellerWindow)
                                                       .Where(t => t.TellerWindowID == tellerWindowId &&
                                                                   t.SectorStationTerminal.SectorStationID ==
                                                                   t.TellerWindow.SectorStationID &&
                                                                   ((t.EndDate.HasValue &&
                                                                     t.EndDate.Value >= currentDate) ||
                                                                    !t.EndDate.HasValue))
                                                       .ToDictionary(
                                                           t => t.SectorStationTerminal.Terminal.TerminalNumber,
                                                           t =>
                                                           t.SectorStationTerminal.Terminal.TerminalProfile
                                                            .TerminalProfileName);

                    return numberAndProfilesList.Any()
                               ? numberAndProfilesList.Aggregate(new StringBuilder(),
                                                                 (sb, nap) => sb.AppendFormat("\n{0}({1})", nap.Key, nap.Value),
                                                                 sb => sb.ToString())
                               : string.Empty;
                });
        }

        /// <summary>
        /// Метод, проверяющий, имеется ли в списке привязок терминала к кассовому окну - привязка с неопределённой датой окончания
        /// </summary>
        /// <param name="terminalId">Идентификатор терминала</param>
        /// <returns>Истина, если у терминала с указанным идентификатором существует привязка неопределённой датой окончания, 
        ///          и ложь в противном случае</returns>
        public bool IsSetCurrentInterval(int terminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindowTerminal.
                                                               //Include(t => t.Terminal).
                                                                   Include(t => t.TellerWindow).
                                                               /*Where(t => t.TerminalID == terminalId &&
                               t.Terminal.SectorStationID == t.TellerWindow.SectorStationID).*/

                                                                   Count(t => t.EndDate == null) == 1);
        }

        /// <summary>
        /// Метод, обновляющий сущность TellerWindowTerminal данными из модели AddTerminalToTellerWindowModel
        /// </summary>
        /// <param name="model">Объект модели AddTerminalToTellerWindowModel</param>
        public void CreateFromModel(TerminalToTellerWindowModel model)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var tellerWindowTerminal = new TellerWindowTerminal();

                    tellerWindowTerminal.SectorStationTerminalID = model.SectorStationTerminalId;
                    tellerWindowTerminal.TellerWindowID = model.TellerWindow;

                    tellerWindowTerminal.BeginDate = DateTime.Parse(model.BeginDate);

                    if (!string.IsNullOrWhiteSpace(model.EndDate))
                    {
                        tellerWindowTerminal.EndDate = DateTime.Parse(model.EndDate);
                    }

                    context.TellerWindowTerminal.Add(tellerWindowTerminal);
                    context.SaveChanges();
                });
        }

        /// <summary>
        /// Метод, обновляющий сущность TellerWindowTerminal данными из модели EditTerminalToTellerWindowModel
        /// </summary>
        /// <param name="model">Объект модели EditTerminalToTellerWindowModel</param>
        public void UpdateFromModel(TerminalToTellerWindowModel model)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var tellerWindowTerminal = context.TellerWindowTerminal.
                                                   SingleOrDefault(t => t.TellerWindowTerminalID == model.TellerWindowTerminalId);

                if (tellerWindowTerminal != null)
                {
                    tellerWindowTerminal.SectorStationTerminalID = model.SectorStationTerminalId;
                    tellerWindowTerminal.TellerWindowID = model.TellerWindow;

                    tellerWindowTerminal.BeginDate = DateTime.Parse(model.BeginDate);

                    tellerWindowTerminal.EndDate = !string.IsNullOrWhiteSpace(model.EndDate)
                                                       ? DateTime.Parse(model.EndDate)
                                                       : (DateTime?)null;

                    context.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Метод, копирующий данные из объекта сущности TellerWindowTerminal в объект модели EditTerminalToTellerWindowModel
        /// </summary>
        /// <param name="model">Объект модели EditTerminalToTellerWindowModel</param>
        /// <param name="tellerWindowTerminalId">Идентификатор привязки терминала к кассовому окну</param>
        /// <returns>Истина в случае успеха завершения и ложь, если в базе нет объекта сущности TellerWindowTerminal с указанным идентификатором</returns>
        public bool EditModelFromTellerWindowTerminal(TerminalToTellerWindowModel model, int tellerWindowTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var tellerWindowTerminal = GetTellerWindowTerminalById(tellerWindowTerminalId);
                    if (tellerWindowTerminal == null)
                    {
                        return false;
                    }

                    model.TellerWindowTerminalId = tellerWindowTerminalId;
                    model.Edit(tellerWindowTerminal, TimeFormat);

                    return true;
                });
        }

        /// <summary>
        /// Метод, возвращающий отсортированный  и отфильтрованный список привязок к кассовым окнам указанной привязки терминала к станции для таблицы
        /// </summary>
        /// <param name="page">Номер страницы таблицы</param>
        /// <param name="pageSize">Количество элементов на странице таблицы</param>
        /// <param name="orderby">Строка с параметрами сортировки</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <param name="itemsCount">Общее количество привязок терминала к кассовым окнам (необходимо для правильного постраничного просмотра)</param>
        /// <returns></returns>
        public List<TellerWindowTerminalGridModel> Search(int page, int pageSize, string orderby, int sectorStationTerminalId, out int itemsCount)
        {            
            var itemsCountInner = 0;
            var result = GrokExceptionManager.ProcessGrokDb(context =>
            {
                var orderFunction = GetTellerindowTerminalOrderFunction(orderby);

                var query = context.TellerWindowTerminal.
                                        Include(t => t.TellerWindow).
                                        Include(t => t.SectorStationTerminal).
                                        Include(t => t.SectorStationTerminal.Terminal).
                                        Where(t => t.SectorStationTerminalID == sectorStationTerminalId).
                                        AsQueryable<TellerWindowTerminal>();
                query = orderFunction(query).AsQueryable();                

                var pagedList = new PagedList<TellerWindowTerminal>(query, page, pageSize);
                itemsCountInner = pagedList.ItemCount;

                var data = pagedList.Select(t => new TellerWindowTerminalGridModel()
                    {
                        Id = t.TellerWindowTerminalID,
                        TellerWindowNumber = t.TellerWindow.TellerWindowNumber.HasValue ?
                                                t.TellerWindow.TellerWindowNumber.Value.ToString() :
                                                string.Empty,
                        TerminalNumber = t.SectorStationTerminal.Terminal.TerminalNumber,
                        BeginDate = t.BeginDate.ToString(TimeFormat),
                        EndDate = t.EndDate.HasValue ?
                                    t.EndDate.Value.ToString(TimeFormat) :
                                    string.Empty
                    });

                return data.ToList();
            });

            itemsCount = itemsCountInner;
            return result;
        }

        /// <summary>
        /// Метод, удаляющий привязку терминала к кассовому окну из базы данных
        /// </summary>
        /// <param name="tellerWindowTerminalId">Идентификатор привязки терминала к кассовому окну</param>
        public void DeleteTellerWindowTerminal(int tellerWindowTerminalId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var tellerWindowTerminal = context.TellerWindowTerminal.
                            SingleOrDefault(t => t.TellerWindowTerminalID == tellerWindowTerminalId);

                if (tellerWindowTerminal != null)
                {
                    context.TellerWindowTerminal.Remove(tellerWindowTerminal);
                    context.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Метод, проверяющий, существуют ли в рамках привязки терминала к станции с указанным идентификатором
        /// привязки терминала к окнам с датой установки меньше указанной
        /// </summary>
        /// <param name="beginDate">Контрольная дата установки</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Минимальная дата установки привязки терминала к кассовому окну в рамках привязки терминала к станции
        ///          в случае, если она меньше контрольной даты установки, и null в противном случае</returns>
        public DateTime? AreTellerWindowTerminalsWithLessBeginDateForTerminal(DateTime beginDate, int sectorStationTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                if (context.TellerWindowTerminal.
                            Any(t => t.SectorStationTerminalID == sectorStationTerminalId))
                {
                    var minBeginDate = context.TellerWindowTerminal.
                                               Where(t => t.SectorStationTerminalID == sectorStationTerminalId).
                                               Min(t => t.BeginDate);

                    return minBeginDate < beginDate
                               ? minBeginDate
                               : (DateTime?)null;
                }
                else
                {
                    return (DateTime?)null;
                }
            });
        }

        /// <summary>
        /// Метод, проверяющий, существуют ли у кассового окна с указанным идентификатором
        /// привязки к терминалам кс датой установки меньше указанной
        /// </summary>
        /// <param name="beginDate">Контрольная дата установки</param>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>Минимальная дата установки привязки терминала к кассовому окну 
        ///          в случае, если она меньше контрольной даты установки, и null в противном случае</returns>
        public DateTime? AreTellerWindowTerminalsWithLessBeginDateForTellerWindow(DateTime beginDate, int tellerWindowId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                if (context.TellerWindowTerminal
                           .Any(t => t.TellerWindowID == tellerWindowId))
                {
                    var minBeginDate = context.TellerWindowTerminal
                                              .Where(t => t.TellerWindowID == tellerWindowId)
                                              .Min(t => t.BeginDate);

                    return minBeginDate < beginDate
                               ? minBeginDate
                               : (DateTime?)null;
                }
                else
                {
                    return (DateTime?)null;
                }
            });
        }

        /// <summary>
        /// Метод, проверяющий, существует ли в рамках привязки терминала к станции с указанным идентификатором
        /// привязки терминала к окнам с датой снятия больше указанной
        /// </summary>
        /// <param name="endDate">Контрольная дата снятия</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки к станции</param>
        /// <returns>Максимальная дата снятия привязки терминала к кассовому окну в рамках привязки терминала к станции
        ///          в случае, если она больше контрольной даты снятия, и null в противном случае</returns>
        public DateTime? AreTellerWindowTerminalWithBiggerEndDateForTerminal(DateTime endDate, int sectorStationTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                if (context.TellerWindowTerminal.
                            Any(t => t.SectorStationTerminalID == sectorStationTerminalId))
                {
                    var maxEndDate = context.TellerWindowTerminal.
                                             Where(t => t.SectorStationTerminalID == sectorStationTerminalId).
                                             Max(t => t.EndDate);

                    return maxEndDate > endDate
                               ? maxEndDate
                               : (DateTime?)null;
                }
                else
                {
                    return (DateTime?)null;
                }
            });
        }

        /// <summary>
        /// Метод, проверяющий, существуют ли у кассового окна с указанным идентификатором
        /// привязки к терминалам кс датой установки больше указанной
        /// </summary>
        /// <param name="endDate">Контрольная дата установки</param>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>Минимальная дата установки привязки терминала к кассовому окну 
        ///          в случае, если она больше контрольной даты установки, и null в противном случае</returns>
        public DateTime? AreTellerWindowTerminalsWithBiggerBeginDateForTellerWindow(DateTime endDate, int tellerWindowId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                if (context.TellerWindowTerminal
                           .Any(t => t.TellerWindowID == tellerWindowId))
                {
                    var maxEndDate = context.TellerWindowTerminal
                                              .Where(t => t.TellerWindowID == tellerWindowId)
                                              .Max(t => t.EndDate);

                    return maxEndDate > endDate
                               ? maxEndDate
                               : (DateTime?)null;
                }
                else
                {
                    return (DateTime?)null;
                }
            });
        }

        /// <summary>
        /// Метод, закрывающий действующие или будущие привязки ккт к ко (с согласия пользователя) 
        /// в случае смены профиля ко
        /// </summary>
        /// <param name="tellerWindowId">Идентификатор кассовокого окна</param>
        /// <param name="context">Объект для связи с БД</param>
        /// <param name="newProfile">Новый профиль кассового окна</param>
        internal void CloseBindings(int tellerWindowId, int? newProfile, GrokDb context)
        {            
            var now = DateTime.Now;
            var currentDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);            

            int? invalidProfile = null;
            if (newProfile.HasValue)
            {
                //Если новый профиль ко - "На вход", закрываем привязки к ккт
                //с профилем "на выход"
                if (newProfile.Value == (int) TellerWindowProfiles.Input)
                {
                    invalidProfile = (int) TellerWindowProfiles.Output;
                }

                //Если новый профиль ко - "На выход", закрываем привязки к ккт
                //с профилем "на вход"
                if (newProfile.Value == (int) TellerWindowProfiles.Output)
                {
                    invalidProfile = (int) TellerWindowProfiles.Input;
                }
            }

            //Привязки, которые необходимо закрыть
            var tellerWindowTerminals = context.TellerWindowTerminal
                                                .Include(t => t.TellerWindow)
                                                .Include(t => t.SectorStationTerminal)
                                                .Include(t => t.SectorStationTerminal.Terminal)
                                                .Where(t => t.TellerWindowID == tellerWindowId &&
                                                            t.SectorStationTerminal.SectorStationID == t.TellerWindow.SectorStationID &&
                                                            ((t.EndDate.HasValue && t.EndDate.Value >= currentDate) || !t.EndDate.HasValue) &&
                                                             (invalidProfile.HasValue && t.SectorStationTerminal.Terminal.TerminalProfileID == invalidProfile))
                                                .ToList();
           
            tellerWindowTerminals.ForEach(t =>
                {
                    //Если привязка ко к ккт в настоящем и создана не позже текущей даты - то закрываем её
                    if (t.BeginDate <= currentDate)
                    {
                        t.EndDate = currentDate;
                    }
                    else
                    {
                        //Если в будущем, то удаляем
                        context.TellerWindowTerminal
                                .Remove(t);
                    }
                });                            
        }

        /// <summary>
        /// Метод, возвращающий профиль терминала
        /// </summary>
        /// <param name="tellerWindowTerminalId">Идентификатор привязки ккт к ко</param>
        /// <returns>Профиль терминала</returns>
        public int GetTerminalProfileByTellerWindowTerminalId(int tellerWindowTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                   var tellerWindowTerminal = context.TellerWindowTerminal
                                                      .Include(t => t.SectorStationTerminal)
                                                      .Include(t => t.SectorStationTerminal.Terminal)
                                                      .FirstOrDefault(t => t.TellerWindowTerminalID == tellerWindowTerminalId);

                    return tellerWindowTerminal != null
                               ? tellerWindowTerminal.SectorStationTerminal.Terminal.TerminalProfileID
                               : 0;
                });
        }

        /// <summary>
        /// Метод, возвращающий функцию сортировки
        /// </summary>
        /// <param name="orderBy">Параметры сортировки</param>
        /// <returns>Функция сортировки</returns>
        private Func<IQueryable<TellerWindowTerminal>, IOrderedQueryable<TellerWindowTerminal>> GetTellerindowTerminalOrderFunction(string orderBy)
        {
            //ToDo Exc
            var sortDescription = SortDescriptionCreateHelper.FactoryMethod(orderBy);

            switch (sortDescription.Field)
            {                
                case "BeginDate":
                    return query => TellerWindowTerminalSorter.OrderByBeginDate(query, sortDescription.IsAscending);
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field + "\' не поддерживается");
            }
        }       
    }
}
