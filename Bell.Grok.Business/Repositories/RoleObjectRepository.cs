﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class RoleObjectRepository
    {
        /// <summary>
        /// Метод, проверяющий наличие прав пользователя на выполнение определённой операции
        /// </summary>
        /// <param name="actionCode">Код операции</param>
        /// <param name="userName">Логин пользователя</param>
        /// <param name="objectCode">Код объекта, над которым совершаетсяя операция</param>
        /// <returns></returns>
        public bool CanUserMakeAction(string actionCode, string userName, string objectCode)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var userRole = context.User.First(u => u.Login == userName).UserRole.FirstOrDefault();
                    var dataObject = context.DataObject.FirstOrDefault(d => d.Code.ToLower() == objectCode.ToLower());
                    var roleObjetRight =
                        context.RoleObjectRight.FirstOrDefault(r => r.Code.ToLower() == actionCode.ToLower());

                    if (userRole != null && dataObject != null && roleObjetRight != null)
                    {
                        var roleId = userRole.RoleID;
                        var dataObjectId = dataObject.DataObjectID;
                        var roleObjectRightId = roleObjetRight.RoleObjectRightID;

                        return context.RoleObject.Any(r => r.RoleID == roleId && r.DataObjectID == dataObjectId &&
                                                           r.RoleObjectRightID == roleObjectRightId);
                    }
                    else
                    {
                        return false;
                    }
                });
        }
        
        /// <summary>
        /// Метод, возвращающий содержимое таблицы RoleObject БД
        /// </summary>
        /// <returns>Список объектов модели RoleObjectModel</returns>
        public List<RoleObjectModel> GetAllRoleObjects()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.RoleObject.Select(r => new RoleObjectModel
                {
                    Action = (UserActions) r.RoleObjectRightID,
                    DataObject = (DataObjects) r.DataObjectID,
                    RoleId = r.RoleID,
                    RoleObjectId = r.RoleObjectID
                }).ToList());
        }
    }
}
