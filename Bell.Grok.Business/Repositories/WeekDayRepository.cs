﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    /// <summary>
    /// репозиторий для работы со справочником дней недели
    /// </summary>
    public class WeekDayRepository
    {
        public WeekDayRepository()
        {

        }

        /// <summary>
        /// Метод, возвращающий идентификаторы всех дней недели
        /// </summary>
        /// <returns>Список идентификаторов всех дней недели</returns>
        public List<int> GetAllIds()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.WeekDay.Select(w => w.WeekDayID).ToList());
        }

        /// <summary>
        /// Метод, возвращающий идентификаторы рабочих дней недели
        /// </summary>
        /// <returns>Список идентификаторов рабочих дней недели</returns>
        public List<int> GetAllWorkDaysIds()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.WeekDay.Where(w => w.WeekDayNumber >= 1 && w.WeekDayNumber <= 5)
                                                                  .Select(w => w.WeekDayID)
                                                                  .ToList());
        }

        /// <summary>
        /// Метод, возвращающий идентификатор выходных днеей недели
        /// </summary>
        /// <returns>Список идентификаторов выходных днеей недели</returns>
        public List<int> GetAllWeekendIds()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.WeekDay.Where(w => w.WeekDayNumber >= 6 && w.WeekDayNumber <= 7)
                                                                  .Select(w => w.WeekDayID)
                                                                  .ToList());
        }

        /// <summary>
        /// Метод, возвращающий идентификатор дня недели по его номеру.
        /// С практической точки зрения бессмыслен, потому что скорее всего номер и идентификатор
        /// в соответствующей таблице БД совпадут, но с технологической точки зрения необходим
        /// </summary>
        /// <param name="number">Номер дня недели</param>
        /// <returns>Идентификатор дня недели</returns>
        public int GetIdByNumber(int number)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.WeekDay.FirstOrDefault(w => w.WeekDayNumber == number).WeekDayID);
        }
    }
}
