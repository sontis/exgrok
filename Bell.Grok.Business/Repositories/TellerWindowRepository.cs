﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Data.Entity;
using Bell.Grok.Business.Extensions;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;

namespace Bell.Grok.Business
{
    public class TellerWindowRepository
    {
        /// <summary>
        /// Формат времени для конвертации значения DateTime в строку 
        /// </summary>
        private const string TimeFormat = "dd.MM.yyyy";

        public TellerWindowRepository()
        {

        }

        /// <summary>
        /// Метод, проверяющий существование кассового окна с определённым идентификатором
        /// </summary>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>Истина, если кассовое окно с указанным идентификатором существует, и ложь в противном случае</returns>
        public bool TellerWindowWithIdExists(int tellerWindowId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindow.Any(t => t.TellerWindowID == tellerWindowId));
        }

        /// <summary>
        /// Метод, возвращающий объект сущности TellerWindow
        /// </summary>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>Объект сущности TellerWindow</returns>
        public TellerWindow GetTellerWindowById(int tellerWindowId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindow.
                                                                   Include(t => t.TellerWindowDetailHistory).
                                                                   Include(t => t.TellerWindowDetailHistory.Select(tt => tt.TellerWindowSpecializationHistory)).
                                                                   Include(t => t.TellerWindowPlacement).
                                                                   Include(t => t.TellerWindowOperationPeriod).
                                                                   Include(t => t.TellerWindowTerminal).
                                                                   Include("TellerWindowOperationPeriod.TellerWindowBreakTime").
                                                                   Include("TellerWindowOperationPeriod.TellerWindowBreakTime.WeekDay").
                                                                   Include("TellerWindowOperationPeriod.TellerWindowWorkTime").
                                                                   Include("TellerWindowOperationPeriod.TellerWindowWorkTime.WeekDay").
                                                                   SingleOrDefault(t => t.TellerWindowID == tellerWindowId));
        }

        /// <summary>
        /// Метод, возвращающий все специализации кассовых окон
        /// </summary>
        /// <returns>Список специализаций кассовых окон</returns>
        public Dictionary<int, string> GetAllSpecializations()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TellerWindowSpecialization
                                                             .ToDictionary(tws => tws.TellerWindowSpecializationID, 
                                                                           tws => tws.TellerWindowSpecializationName));
        }

        /// <summary>
        /// Метод, возвращающий все названия расположений кассовых окон
        /// </summary>
        /// <returns>Список названий расположений кассовых окон</returns>
        public Dictionary<int, string> GetAllPlacementNames()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TellerWindowPlacement
                                                             .ToDictionary(twp => twp.TellerWindowPlacementID, 
                                                                           twp => twp.TellerWindowPlacementName));
        }

        /// <summary>
        /// Метод, возвращающий все профили кассовых окон
        /// </summary>
        /// <returns>Список всех профилей кассовых окон</returns>
        public Dictionary<int, string> GetAllProfiles()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TellerWindowProfile
                                                             .ToDictionary(twp => twp.TellerWindowProfileID,
                                                                           twp => twp.Name));
        }

        /// <summary>
        /// Метод, возвращающий все статусы кассовых окон
        /// </summary>
        /// <returns>Список статусов кассовых окон</returns>
        public Dictionary<int, string> GetAllStatuses()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TellerWindowStatus
                                                             .ToDictionary(tws => tws.TellerWindowStatusID, 
                                                                           tws => tws.TellerWindowStatusName));
        }

        /// <summary>
        /// Метод, возвращающий все способы оплаты (для классификации кассовых окон)
        /// </summary>
        /// <returns>Список способов оплаты</returns>
        public Dictionary<int, string> GetAllPaymentMethods()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TellerWindowPayment
                                                             .ToDictionary(twp => twp.TellerWindowPaymentID,
                                                                           twp => twp.TellerWindowPaymentName));
        }

        /// <summary>
        /// Метод, возвращающий все типы перерывов кассовых окон
        /// </summary>
        /// <returns>Список типов перерывов</returns>
        public Dictionary<int, string> GetAllTellerWindowBreakes()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TellerWindowBreak
                                                             .ToDictionary(twp => twp.TellerWindowBreakID,
                                                                           twp => twp.TellerWindowBreakName));
        }

        /// <summary>
        /// Метод, возвращающий все режимы работы кассовых окон
        /// </summary>
        /// <returns>Список режимов</returns>
        public Dictionary<int, string> GetAllTellerWindowShiftness()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TellerWindowShiftness
                                                             .ToDictionary(twp => twp.TellerWindowShiftnessID,
                                                                           twp => twp.TellerWindowShiftnessName));
        }

        /// <summary>
        /// Метод, возвращающий все дни недели
        /// </summary>
        /// <returns>Список дней недели</returns>
        public Dictionary<int, string> GetAllWeekDays()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.WeekDay
                                                             .ToDictionary(twp => twp.WeekDayNumber,
                                                                           twp => twp.WeekDayName));
        }

        /// <summary>
        /// Метод, возвращающий список кассовых окон станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="terminalProfile">Профиль терминала, который влияет на выборку кассовых окон</param>
        /// <returns>Список кассовых окон станции</returns>
        public Dictionary<int, string> GetAllNumbersForStation(int sectorId, int stationId, int terminalProfile = 0)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.TellerWindow
                                       .Where(tw => tw.SectorStation.SectorID == sectorId &&
                                                    tw.SectorStation.StationID == stationId);
                    
                    if (terminalProfile == (int)TerminalProfiles.Input)
                    {
                        query = query.Where(q => q.TellerWindowProfileID == (int) TellerWindowProfiles.Input ||
                                                 !q.TellerWindowProfileID.HasValue);
                    }

                    if (terminalProfile == (int)TerminalProfiles.Output)
                    {
                        query = query.Where(q => q.TellerWindowProfileID == (int)TellerWindowProfiles.Output ||
                                                 !q.TellerWindowProfileID.HasValue);
                    }

                    return query.OrderBy(tw => tw.TellerWindowNumber)
                                .ToDictionary(tw => tw.TellerWindowID,
                                              tw => tw.TellerWindowNumber.HasValue
                                                        ? tw.TellerWindowNumber.Value
                                                            .ToString(CultureInfo.InvariantCulture)
                                                        : string.Empty);
                });
        }

        /// <summary>
        /// Перегрузка предыдущего метода
        /// </summary>
        /// <param name="sectorStationId">Идентификатор станции с учётом участка</param>
        /// <returns>Список кассовых окон станции</returns>
        public Dictionary<int, string> GetAllNumbersForStation(int sectorStationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TellerWindow
                                                             .Where(tw => tw.SectorStationID == sectorStationId)
                                                             .OrderBy(tw => tw.TellerWindowNumber)
                                                             .ToDictionary(tw => tw.TellerWindowID,
                                                                             tw => tw.TellerWindowNumber.HasValue 
                                                                                        ? tw.TellerWindowNumber.Value.ToString(CultureInfo.InvariantCulture) 
                                                                                        : string.Empty));
        }

        /// <summary>
        /// Метод, проверяющий существование кассового окна с определённым номером на станции
        /// </summary>
        /// <param name="number">Номер кассового окна</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Истина, если кассовое окно с указанным идентификатором существует на указанной станции, и ложь в противном случае</returns>
        public bool TellerWindowWithNumberExists(int number, int sectorId, int stationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.TellerWindow.Count(t => t.TellerWindowNumber == number &&
                                                                                           t.SectorStation.SectorID == sectorId &&
                                                                                           t.SectorStation.StationID == stationId) == 1);
        }

        /// <summary>
        /// Метод, обновляющий сущность TellerWindow (вместе с навигационными свойствами) данными из модели TellerWindowEditModel
        /// </summary>
        /// <param name="model">Объект модели TellerWindowEditModel</param>
        /// <param name="needCheckBindingsForClose">Флаг, показывающий, необходимо ли проверять активные привязки к ккт на необходимость закрытия</param>
        public void UpdateFromModel(TellerWindowEditModel model, bool needCheckBindingsForClose)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                TellerWindow tellerWindow;
                if (model.TellerWindowId == 0)
                {
                    tellerWindow = new TellerWindow();

                    tellerWindow.SectorStation =
                            context.SectorStation.SingleOrDefault(
                                ss => ss.SectorID == model.SectorId && ss.StationID == model.StationId);
                    tellerWindow.TellerWindowNumber = model.Number;
                    tellerWindow.TellerWindowDetailHistory.Add(new TellerWindowDetailHistory());

                    context.TellerWindow.Add(tellerWindow);
                }
                else
                {
                    tellerWindow = context.TellerWindow.
                                               Include(t => t.TellerWindowDetailHistory).
                                               SingleOrDefault(t => t.TellerWindowID == model.TellerWindowId);
                }

                if (tellerWindow != null)
                {
                    tellerWindow.TellerWindowPlacementID = model.PlacementName;
                    tellerWindow.PlacementNumericalValue = model.PlacementNumericValue;
                    tellerWindow.PlacementComment = model.PlacementComment;

                    var wasProfiledChanged = model.Profile != tellerWindow.TellerWindowProfileID;
                    tellerWindow.TellerWindowProfileID = model.Profile;

                    var tellerWindowDetailHistory = tellerWindow.TellerWindowDetailHistory.FirstOrDefault();

                    if (tellerWindowDetailHistory != null)
                    {
                        tellerWindowDetailHistory.TellerWindowStatusID = model.Status;
                        tellerWindowDetailHistory.TellerWindowPaymentID = model.PaymentMethod;
                        tellerWindowDetailHistory.Comment = model.Comment;

                        tellerWindowDetailHistory.BeginDate = DateTime.Parse(model.BeginTime);
                        if (model.EndTime != null)
                        {
                            tellerWindowDetailHistory.EndDate = DateTime.Parse(model.EndTime);

                            tellerWindow.TellerWindowTerminal
                                        .Where(t => !t.EndDate.HasValue)
                                        .ForEach(t => t.EndDate = tellerWindowDetailHistory.EndDate);
                        }
                        else
                        {
                            tellerWindowDetailHistory.EndDate = null;
                        }

                        UpdateTellerWindowSpecializations(tellerWindowDetailHistory, model.Specializations, context);
                    }


                    var format = "dd.MM.yyyy";
                    var provider = new CultureInfo("fr-FR");
                    var originalOperationPeriods = tellerWindow.TellerWindowOperationPeriod;
                    tellerWindow.TellerWindowOperationPeriod = new List<TellerWindowOperationPeriod>();
                    foreach (var operationPeriodModel in model.OperationPeriods)
                    {
                        var operationPeriod =
                            originalOperationPeriods.FirstOrDefault(
                                e => e.TellerWindowOperationPeriodID == operationPeriodModel.Id);
                        if (operationPeriod == null)
                        {
                            operationPeriod = new TellerWindowOperationPeriod();
                            context.TellerWindowOperationPeriod.Add(operationPeriod);
                        }
                        operationPeriod.BeginDate = DateTime.ParseExact(operationPeriodModel.BeginDate, format, provider);
                        operationPeriod.EndDate = string.IsNullOrWhiteSpace(operationPeriodModel.EndDate) ? null : (DateTime?)DateTime.ParseExact(operationPeriodModel.EndDate, format, provider);

                        int shiftnessId;
                        if (int.TryParse(operationPeriodModel.ShiftnessId, out shiftnessId))
                        {
                            operationPeriod.TellerWindowShiftnessID = shiftnessId;
                        }
                        else
                        {
                            operationPeriod.TellerWindowShiftnessID = null;
                        }


                        var originalWorkTimes = operationPeriod.TellerWindowWorkTime;
                        var originalBreakTimes = operationPeriod.TellerWindowBreakTime;
                        operationPeriod.TellerWindowWorkTime = new List<TellerWindowWorkTime>();
                        operationPeriod.TellerWindowBreakTime = new List<TellerWindowBreakTime>();

                        for (int day = 0; day < 8; day++)
                        {
                            foreach (var workTimeModel in operationPeriodModel.WorkTimes[day])
                            {
                                var workTime =
                                    originalWorkTimes.FirstOrDefault(e => e.TellerWindowWorkTimeID == workTimeModel.Id);
                                if (workTime == null)
                                {
                                    workTime = new TellerWindowWorkTime();
                                    context.TellerWindowWorkTime.Add(workTime);
                                }

                                workTime.BeginTime = TimeSpan.Parse(workTimeModel.BeginTime);
                                workTime.EndTime = TimeSpan.Parse(workTimeModel.EndTime.Equals("24:00") ? "00:00" : workTimeModel.EndTime);

                                workTime.WeekDayID = context.WeekDay.First(e => e.WeekDayNumber == day + 1).WeekDayID;

                                operationPeriod.TellerWindowWorkTime.Add(workTime);
                            }
                            foreach (var breakTimeModel in operationPeriodModel.BreakTimes[day])
                            {
                                var breakTime =
                                    originalBreakTimes.FirstOrDefault(e => e.TellerWindowBreakTimeID == breakTimeModel.Id);
                                if (breakTime == null)
                                {
                                    breakTime = new TellerWindowBreakTime();
                                    context.TellerWindowBreakTime.Add(breakTime);
                                }

                                breakTime.BeginTime = TimeSpan.Parse(breakTimeModel.BeginTime);
                                breakTime.EndTime = TimeSpan.Parse(breakTimeModel.EndTime.Equals("24:00") ? "00:00" : breakTimeModel.EndTime);
                                breakTime.TellerWindowBreakID = breakTimeModel.BreakId;
                                breakTime.WeekDayID = context.WeekDay.First(e => e.WeekDayNumber == day + 1).WeekDayID;

                                operationPeriod.TellerWindowBreakTime.Add(breakTime);
                            }
                        }
                        foreach (var workTime in originalWorkTimes.Where(oe => operationPeriod.TellerWindowWorkTime.All(e => e.TellerWindowWorkTimeID != oe.TellerWindowWorkTimeID)))
                        {
                            context.TellerWindowWorkTime.Remove(workTime);
                        }
                        foreach (var breakTime in originalBreakTimes.Where(oe => operationPeriod.TellerWindowBreakTime.All(e => e.TellerWindowBreakTimeID != oe.TellerWindowBreakTimeID)))
                        {
                            context.TellerWindowBreakTime.Remove(breakTime);
                        }

                        tellerWindow.TellerWindowOperationPeriod.Add(operationPeriod);
                    }
                    foreach (var tellerWindowOperationPeriod in originalOperationPeriods.Where(oe => tellerWindow.TellerWindowOperationPeriod.All(e => e.TellerWindowOperationPeriodID != oe.TellerWindowOperationPeriodID)))
                    {
                        context.TellerWindowOperationPeriod.Remove(tellerWindowOperationPeriod);
                    }

                    if (needCheckBindingsForClose &&
                        wasProfiledChanged)
                    {
                        var tellerWIndowTerminalRepository = new TellerWindowTerminalRepository();
                        tellerWIndowTerminalRepository.CloseBindings(model.TellerWindowId, model.Profile, context);
                    }

                    context.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Метод, возвращающий отсортированный  и отфильтрованный список кассовых окон станции для таблицы
        /// </summary>
        /// <param name="page">Номер страницы таблицы</param>
        /// <param name="pageSize">Количество элементов на странице таблицы</param>
        /// <param name="orderby">Строка с параметрами сортировки</param>
        /// <param name="filterDictionary">Коллекция параметров фильтрации</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="itemsCount">Общее количество кассовых окон на станции (необходимо для правильного постраничного просмотра)</param>
        /// <returns>Список объектов TellerWindowGridModel</returns>
        public List<TellerWindowGridModel> Search(int page, int pageSize, string orderby, NameValueCollection filterDictionary, int sectorId, int stationId, out int itemsCount)
        {
            var itemsCountInner = 0;
            var result = GrokExceptionManager.ProcessGrokDb(context =>
            {
                var filterFunctions = GetFilterFunctions(filterDictionary);
                var orderFunction = GetTellerWindowOrderFunction(orderby);

                var query = context.TellerWindow.
                                    Include(t => t.TellerWindowDetailHistory).
                                    Include(t => t.TellerWindowDetailHistory.Select(td => td.TellerWindowSpecializationHistory)).
                                    Include(t => t.TellerWindowDetailHistory.Select(td => td.TellerWindowSpecializationHistory.Select(tdd => tdd.TellerWindowSpecialization))).
                                    Include(t => t.TellerWindowDetailHistory.Select(td => td.TellerWindowStatus)).
                                    Include(t => t.TellerWindowDetailHistory.Select(td => td.TellerWindowPayment)).
                                    Include(t => t.TellerWindowPlacement).
                                    Include(t => t.TellerWindowProfile).
                                    Include(t => t.TellerWindowTerminal).
                                    Include(t => t.SectorStation).
                                    Include(t => t.TellerWindowTerminal).
                                    Include(t => t.TellerWindowTerminal.Select(twt => twt.SectorStationTerminal)).
                                    Include(t => t.TellerWindowTerminal.Select(twt => twt.SectorStationTerminal.Terminal)).
                                    Include(t => t.TellerWindowTerminal.Select(twt => twt.SectorStationTerminal.Terminal.TerminalType)).
                                    AsQueryable();
                query = query.Where(t => t.SectorStation.SectorID == sectorId &&
                                         t.SectorStation.StationID == stationId);

                foreach (var filterFunction in filterFunctions)
                {
                    query = filterFunction(query);
                }
                query = orderFunction(query).AsQueryable();                

                var pagedList = new PagedList<TellerWindow>(query, page, pageSize);
                itemsCountInner = pagedList.ItemCount;

                var now = DateTime.Now;
                var nowDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

                var tellerWindowTerminalEqualityComparer = new TellerWindowTerminalEqualityComparer();

                var data = pagedList.Select(t => new TellerWindowGridModel
                    {
                        Id = t.TellerWindowID,
                        Number = t.TellerWindowNumber,
                        Placement = t.TellerWindowPlacement.TellerWindowPlacementName + " " + t.PlacementNumericalValue + " " + t.PlacementComment,
                        ProfileName = t.TellerWindowProfileID.HasValue
                                            ? t.TellerWindowProfile.Name
                                            : string.Empty,
                        Specialization = t.TellerWindowDetailHistory.Any() 
                                                ? t.TellerWindowDetailHistory.First()
                                                                             .TellerWindowSpecializationHistory
                                                                             .Any()
                                                        ? t.TellerWindowDetailHistory.First()
                                                                                     .TellerWindowSpecializationHistory
                                                                                     .Select(tw => tw.TellerWindowSpecialization.TellerWindowSpecializationName)
                                                                                     .Aggregate((i, j) => i + "," + j) 
                                                        : string.Empty 
                                                : string.Empty,
                        Status = t.TellerWindowDetailHistory.Any() ?
                                            t.TellerWindowDetailHistory.First().TellerWindowStatus.TellerWindowStatusName :
                                            string.Empty,
                        Payment = t.TellerWindowDetailHistory.Any() ?
                                            t.TellerWindowDetailHistory.First().TellerWindowPayment.TellerWindowPaymentName :
                                            string.Empty,
                        BeginDate = t.TellerWindowDetailHistory.Any() ?
                                            t.TellerWindowDetailHistory.First().BeginDate.ToString(TimeFormat) :
                                            string.Empty,
                        EndDate = t.TellerWindowDetailHistory.Any() ?
                                            (t.TellerWindowDetailHistory.First().EndDate.HasValue ?
                                                t.TellerWindowDetailHistory.First().EndDate.Value.ToString(TimeFormat) : string.Empty) :
                                            string.Empty,
                        Description = t.TellerWindowDetailHistory.Any() ?
                                            (t.TellerWindowDetailHistory.First().Comment ?? string.Empty) :
                                            string.Empty,
                        TerminalNumber = t.TellerWindowTerminal.Any(twt => twt.BeginDate <= nowDate && (twt.EndDate == null || twt.EndDate.Value >= nowDate)) ?
                                                t.TellerWindowTerminal.Where(twt => twt.BeginDate <= nowDate && (twt.EndDate == null || twt.EndDate.Value >= nowDate)).Distinct(tellerWindowTerminalEqualityComparer).Select(tn => tn.SectorStationTerminal.Terminal.TerminalNumber).Aggregate((i, j) => i + "," + j) :
                                                string.Empty,
                        TerminalType = t.TellerWindowTerminal.Any(twt => twt.BeginDate <= nowDate && (twt.EndDate == null || twt.EndDate.Value >= nowDate)) ?
                                                t.TellerWindowTerminal.Where(twt => twt.BeginDate <= nowDate && (twt.EndDate == null || twt.EndDate.Value >= nowDate)).Distinct(tellerWindowTerminalEqualityComparer).Select(tt => tt.SectorStationTerminal.Terminal.TerminalType.TerminalTypeName).Aggregate((i, j) => i + "," + j) :
                                                string.Empty
                    });

                return data.ToList();
            });

            itemsCount = itemsCountInner;
            return result;
        }

        /// <summary>
        /// Метод, возвращающий профиль кассового окна
        /// </summary>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>Профиль кассового окна</returns>
        public int GetTellerWindowProfile(int tellerWindowId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var tellerWindow = context.TellerWindow
                                              .FirstOrDefault(t => t.TellerWindowID == tellerWindowId);

                    if (tellerWindow != null)
                    {
                        return tellerWindow.TellerWindowProfileID.HasValue
                                   ? tellerWindow.TellerWindowProfileID.Value
                                   : 0;
                    }

                    return 0;                    
                });
        }

        /// <summary>
        /// Метод, копирующий данные из объекта сущности TellerWindow (вместе с навигационными свойствами) в объект модели TellerWindowEditModel
        /// </summary>
        /// <param name="model">Объект модели TellerWindowEditModel</param>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>Истина в случае успеха завершения и ложь, если в базе нет объекта сущности TellerWindow с указанным идентификатором</returns>
        public bool EditModelFromTellerWindow(TellerWindowEditModel model, int tellerWindowId)
        {
            //ToDo Exc
            var tellerWindow = GetTellerWindowById(tellerWindowId);
            if (tellerWindow == null)
            {
                return false;
            }

            model.TellerWindowId = tellerWindowId;
            model.Edit(tellerWindow, TimeFormat);

            return true;
        }

        /// <summary>
        /// Метод, удаляющий кассовое окно (со всеми связанными данными) из базы данных
        /// </summary>
        /// <param name="tellerWindowId">Идентификатор удаляемого кассового окна</param>
        public void DeleteTellerWindow(int tellerWindowId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var tellerWindow = context.TellerWindow.
                                    Include(t => t.TellerWindowDetailHistory).
                                    Include(t => t.TellerWindowTerminal).
                                    SingleOrDefault(t => t.TellerWindowID == tellerWindowId);

                if (tellerWindow != null)
                {
                    while (tellerWindow.TellerWindowDetailHistory.Count > 0)
                    {
                        var tellerWindowhistory = tellerWindow.TellerWindowDetailHistory.ToList()[0];

                        while (tellerWindowhistory.TellerWindowSpecializationHistory.Count > 0)
                        {
                            var tellerWindowSpecializationHistory =
                                tellerWindowhistory.TellerWindowSpecializationHistory.ToList()[0];
                            tellerWindowhistory.TellerWindowSpecializationHistory.Remove(
                                tellerWindowSpecializationHistory);
                            context.TellerWindowSpecializationHistory.Remove(tellerWindowSpecializationHistory);
                        }

                        tellerWindow.TellerWindowDetailHistory.Remove(tellerWindowhistory);
                        context.TellerWindowDetailHistory.Remove(tellerWindowhistory);
                    }

                    while (tellerWindow.TellerWindowTerminal.Count > 0)
                    {
                        var tellerWindowTerminal = tellerWindow.TellerWindowTerminal.ToList()[0];

                        tellerWindow.TellerWindowTerminal.Remove(tellerWindowTerminal);
                        context.TellerWindowTerminal.Remove(tellerWindowTerminal);
                    }

                    while (tellerWindow.TellerWindowOperationPeriod.Count > 0)
                    {
                        var tellerWindowOperationPeriod = tellerWindow.TellerWindowOperationPeriod.ToList()[0];

                        while (tellerWindowOperationPeriod.TellerWindowWorkTime.Count > 0)
                        {
                            var tellerWindowWorkTime = tellerWindowOperationPeriod.TellerWindowWorkTime.ToList()[0];

                            tellerWindowOperationPeriod.TellerWindowWorkTime.Remove(tellerWindowWorkTime);
                            context.TellerWindowWorkTime.Remove(tellerWindowWorkTime);
                        }

                        while (tellerWindowOperationPeriod.TellerWindowBreakTime.Count > 0)
                        {
                            var tellerWindowBreakTime = tellerWindowOperationPeriod.TellerWindowBreakTime.ToList()[0];

                            tellerWindowOperationPeriod.TellerWindowBreakTime.Remove(tellerWindowBreakTime);
                            context.TellerWindowBreakTime.Remove(tellerWindowBreakTime);
                        }


                        tellerWindow.TellerWindowOperationPeriod.Remove(tellerWindowOperationPeriod);
                        context.TellerWindowOperationPeriod.Remove(tellerWindowOperationPeriod);
                    }

                    context.TellerWindow.Remove(tellerWindow);
                    context.SaveChanges();
                }
            });
        }        

        /// <summary>
        /// Метод, возвращающий функцию сортировки
        /// </summary>
        /// <param name="orderBy">Параметры сортировки</param>
        /// <returns>Функция сортировки</returns>
        private Func<IQueryable<TellerWindow>, IOrderedQueryable<TellerWindow>> GetTellerWindowOrderFunction(string orderBy)
        {
            //ToDo Exc
            var sortDescription = SortDescriptionCreateHelper.FactoryMethod(orderBy);

            switch (sortDescription.Field)
            {
                case "Number":
                    return query => TellerWindowSorter.OrderByNumber(query, sortDescription.IsAscending);
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field + "\' не поддерживается");
            }
        }

        /// <summary>
        /// Метод, возвращающий список функций фильтрации
        /// </summary>
        /// <param name="filterDictionary">Коллекция параметров фильтрации</param>
        /// <returns>Список функций фильтрации</returns>
        private List<Func<IQueryable<TellerWindow>, IQueryable<TellerWindow>>> GetFilterFunctions(NameValueCollection filterDictionary)
        {
            //ToDo Exc
            List<Func<IQueryable<TellerWindow>, IQueryable<TellerWindow>>> queries = new List<Func<IQueryable<TellerWindow>, IQueryable<TellerWindow>>>();

            var selectedWindowTellerNumbers = filterDictionary["SelectedWindowTellerNumbers"];
            if (!string.IsNullOrEmpty(selectedWindowTellerNumbers))
            {
                queries.Add(query => TellerWindowFilter.FilterByTellerWindowNumbers(query, selectedWindowTellerNumbers));
            }

            var beginExploitationPeriod = filterDictionary["TellerWindowBeginExploitationPeriod"];
            var endExploitationPeriod = filterDictionary["TellerWindowEndExploitationPeriod"];
            DateTime beginExploitationPeriodDateTime;
            DateTime endExploitationPeriodDateTime;
            if (!DateTime.TryParse(beginExploitationPeriod, out beginExploitationPeriodDateTime))
            {
                beginExploitationPeriodDateTime = (DateTime)SqlDateTime.MinValue;
            }
            if (!DateTime.TryParse(endExploitationPeriod, out endExploitationPeriodDateTime))
            {
                endExploitationPeriodDateTime = (DateTime)SqlDateTime.MaxValue;
            }

            queries.Add(query => TellerWindowFilter.FilterByBeginExploitationPeriod(query, beginExploitationPeriodDateTime, endExploitationPeriodDateTime));
            queries.Add(query => TellerWindowFilter.FilterByEndExploitationPeriod(query, beginExploitationPeriodDateTime, endExploitationPeriodDateTime));

            return queries;
        }

        /// <summary>
        /// Обновляет профессиональные навыки работника
        /// </summary>
        /// <param name="tellerWindowDetailHistory">Объект сущности TellerWindowDetailHistory</param>
        /// <param name="newSpecializationIds">Список идентификаторов новых специализаций</param>
        /// <param name="context">Entity Framework контекст</param>
        private void UpdateTellerWindowSpecializations(TellerWindowDetailHistory tellerWindowDetailHistory, IEnumerable<int> newSpecializationIds,
                                                       GrokDb context)
        {
            //ToDo Exc
            var specializations = tellerWindowDetailHistory.TellerWindowSpecializationHistory;

            var specializationsToBeDeleted = new List<TellerWindowSpecializationHistory>(tellerWindowDetailHistory.TellerWindowSpecializationHistory);

            if (newSpecializationIds != null)
            {
                List<int> ids = newSpecializationIds.ToList();
                //Сопоставляем старые и новые списки специализаций
                foreach (var specialization in specializations)
                {
                    int currentId = specialization.TellerWindowSpecializationID;
                    if (ids.Contains(currentId))
                    {
                        ids.Remove(currentId);
                        specializationsToBeDeleted.Remove(specialization);
                    }
                }
                //Добавляем элементы, которых не было в списке
                foreach (var id in ids)
                {
                    specializations.Add(new TellerWindowSpecializationHistory
                    {
                        TellerWindowDetailHistoryID = tellerWindowDetailHistory.TellerWindowDetailHistoryID,
                        TellerWindowSpecializationID = id
                    });
                }
            }

            //Удаляем элементы, которых нет в новом списке Ids
            foreach (var specialization in specializationsToBeDeleted)
            {
                context.TellerWindowSpecializationHistory.Remove(specialization);
            }
        }        
    }


}
