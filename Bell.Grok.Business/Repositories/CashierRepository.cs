﻿using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий по работе с кассирами
    /// </summary>
    public class CashierRepository
    {
        private const int DefaulMaximumHoursPerWeek = 40;
        private const int DefaultMaximumShiftLength = 12;
        private const string CashierObjectName = "Employee";        

        /// <summary>
        /// Возвращает кассира по его Id
        /// </summary>
        /// <param name="id">Id кассира</param>
        /// <param name="model">Модель кассира</param>
        /// <param name="currentDate">Текущая дата из фильтра для списка кассиров</param>
        /// <returns>Статус успеха</returns>
        public bool GetCashierById(CashierModel model, int id, string currentDate)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var employee = context.Employee.SingleOrDefault(c => c.EmployeeID == id);
                if (employee != null)
                {
                    return ToCashierModel(model, employee, context, currentDate);
                }
                return false;
            });

        }

        /// <summary>
        /// Метод, возвращающий идентификатор участка кассира
        /// </summary>
        /// <param name="id">Идентификатор кассира</param>
        /// <returns>Идентификатор участка кассира</returns>
        public int? GetCashierSectorById(int id)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var cashier = context.Employee.SingleOrDefault(c => c.EmployeeID == id);
                if (cashier != null)
                {
                    return cashier.SectorID;
                }
                return null;
            });

        }

        /// <summary>
        /// Метод, возвращающий листья-кассиры дерева для узла-участка дерева
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="date">Дата, которая должна укладываться в интервал конрактов кассиров</param>
        /// <returns>Список объектов TreeLeafModel</returns>
        public List<TreeLeafModel> GetCashersBySector(int sectorId, string date)
        {
            DateTime currentDate;
            if (!DateTime.TryParse(date, out currentDate))
            {
                currentDate = DateTime.Now;
            }

            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var query = context.Employee.
                                    Where(e => e.SectorID == sectorId &&
                                          e.ContractEffectiveFrom <= currentDate &&
                                          (!e.ContractEffectiveTo.HasValue || currentDate <= e.ContractEffectiveTo) && (e.Position.IsCashierRole))
                                          .OrderBy(e => e.LastName)
                                          .ToArray()
                                    .Select(e => new TreeLeafModel
                                        {
                                            ObjectName = e.LastName + " " + e.FirstName + " " + e.MiddleName,
                                            ObjectId = e.EmployeeID,
                                            ImgUrl = GetCashierStatusImageUrl(e, context.C1cEmployee.Include(x => x.C1cDepartment).SingleOrDefault(x => x.EmployeeNumber.Equals(e.EmployeeNumber,
                                                                                                                                                                                StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                                                                        x.FirstName.Equals(e.FirstName,
                                                                                                                                                                            StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                                                                        x.MiddleName.Equals(e.MiddleName,
                                                                                                                                                                            StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                                                                        x.LastName.Equals(e.LastName,
                                                                                                                                                                            StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                                                                        x.ContractEffectiveFrom <= currentDate &&
                                                                                                                                                        (x.ResignationDate.HasValue
                                                                                                                                                                ? currentDate <= x.ResignationDate.Value
                                                                                                                                                                : e.ContractEffectiveTo.HasValue
                                                                                                                                                                        ? currentDate <= e.ContractEffectiveTo.Value
                                                                                                                                                                        : currentDate <= DateTime.MaxValue)))
                                        });

                return query.ToList();
            });
        }

        /// <summary>
        /// Метод, проверяющий, есть ли на участке кассиры
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="date">Дата, которая должна укладываться в интервал конрактов кассиров</param>
        /// <returns>Истина, если на участке есть кассиры, и ложь в противном случае</returns>
        public bool IsAnyCashiersOnSector(int sectorId, string date)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                DateTime currentDate;
                if (!DateTime.TryParse(date, out currentDate))
                {
                    currentDate = DateTime.Now;
                }

                return context.Employee.Any(e => e.SectorID == sectorId && e.ContractEffectiveFrom <= currentDate &&
                                          (!e.ContractEffectiveTo.HasValue || currentDate <= e.ContractEffectiveTo) && (e.Position.IsCashierRole));
            });
        }

        public int? GetSectorId(int cashierId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var employee = context.Employee.SingleOrDefault(e => e.EmployeeID == cashierId);

                    return employee == null
                               ? null
                               : employee.SectorID;                    
                });
        }

        /// <summary>
        /// Обновляет кассира в базе данных
        /// </summary>
        /// <param name="cashier">Обновленная модель данных кассиров</param>
        /// <param name="currentDate">Дата, которая должна укладываться в интервал конрактов кассиров</param>
        /// <returns>Резльтат операции</returns>
        public bool UpdateCashier(CashierModel cashier, string currentDate)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var employee = context.Employee                                      
                                      .SingleOrDefault(c => (c.EmployeeID == cashier.EmployeeId));                

                if (employee != null)
                {
                    var oldSectorId = employee.SectorID.HasValue ? employee.SectorID.Value : -1;

                    if (oldSectorId != cashier.SectorId)
                    {
                        UpdateEmployeeStationPriority(employee.EmployeeID, cashier.SectorId, context);
                        ChangeCashierVersion(context);
                    }
                    else if (CheckEmployee1CAttributesChanges(employee, cashier))
                    {
                        ChangeCashierVersion(context);
                    }

                    UpdateEmployee(employee, cashier);
                    UpdateEmployeeSpecializations(employee, cashier.SpecializationIds, context);
                    UpdateEmployeeProfessionLevels(employee, cashier.ProfessionalLevelIds, context);
                    
                    context.SaveChanges();

                    return ToCashierModel(cashier, employee, context, currentDate);
                }

                return false;
            });
        }

        /// <summary>
        /// Функция, проверяющая изменение параметров кассира, которые взяты из 1С. Если данные изменились, то необходимо изменить цвет индикатора
        /// в дереве направлений. А для корректного выполнения этой операции необходиимо сбросить кэш запроса списка кассиров.
        /// </summary>
        /// <param name="employee">Данные касира в базе</param>
        /// <param name="model">Данные кассира с формы</param>
        /// <returns>Результат сравнения</returns>
        private bool CheckEmployee1CAttributesChanges(Employee employee, CashierModel model)
        {
            DateTime newEffectiveFromValue, newEffectiveToValue;
            DateTime? newEffectiveFrom = null, newEffectiveTo = null;
            if (DateTime.TryParse(model.ContractEffectiveFrom, out newEffectiveFromValue))
            {
                newEffectiveFrom = newEffectiveFromValue;
            }
            if (DateTime.TryParse(model.ContractEffectiveTo, out newEffectiveToValue))
            {
                newEffectiveTo = newEffectiveToValue;
            }

            return employee.FirstName != model.FirstName || 
                   employee.MiddleName != model.MiddleName ||
                   employee.LastName != model.LastName ||
                   employee.PositionID != model.PositionId ||
                   employee.ContractEffectiveFrom != newEffectiveFrom || 
                   employee.ContractEffectiveTo != newEffectiveTo ||
                   employee.RateQuantity != model.Rate;
        }

        /// <summary>
        /// Метод, увеличивающий приоритет станции для сотрудника
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>        
        /// <param name="priorityLevel">Уровень приоритета</param>
        /// <returns>Успех в случае отсутствия ошибок</returns>
        public bool PickUpStationPriority(int employeeId, int priorityLevel)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                //Вышестоящий приоритет
                var upStationPriority =
                    context.EmployeeStationPriority.FirstOrDefault(
                        e => e.EmployeeID == employeeId && e.PriorityLevel == priorityLevel - 1);

                //Искомый приоритет
                var actualStationPriority =
                    context.EmployeeStationPriority.FirstOrDefault(
                        e => e.EmployeeID == employeeId && e.PriorityLevel == priorityLevel);

                if (upStationPriority == null || actualStationPriority == null)
                {
                    return false;
                }

                //Меняем местами приоритеты
                upStationPriority.PriorityLevel = priorityLevel;
                actualStationPriority.PriorityLevel = priorityLevel - 1;

                context.SaveChanges();

                return true;
            });
        }

        /// <summary>
        /// Метод, уменьшающий приоритет станции для сотрудника
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <param name="priorityLevel">Уровень приоритета</param>
        /// <returns>Успех в случае отсутствия ошибок</returns>
        public bool PickDownStationPriority(int employeeId, int priorityLevel)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                //Нижестоящий приоритет
                var upStationPriority =
                    context.EmployeeStationPriority.FirstOrDefault(
                        e => e.EmployeeID == employeeId && e.PriorityLevel == priorityLevel + 1);

                //Искомый приоритет
                var actualStationPriority =
                    context.EmployeeStationPriority.FirstOrDefault(
                        e => e.EmployeeID == employeeId && e.PriorityLevel == priorityLevel);

                if (upStationPriority == null || actualStationPriority == null)
                {
                    return false;
                }

                //Меняем местами приоритеты
                upStationPriority.PriorityLevel = priorityLevel;
                actualStationPriority.PriorityLevel = priorityLevel + 1;

                context.SaveChanges();

                return true;
            });
        }

        /// <summary>
        /// Метод, возвращающий список кассиров участка, чьи фамилии начинаются с указанного символа и 
        /// чьи договоры действуют в указанную дату
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="date">Дата для фильтрации кассиров по интервалам контрактов</param>
        /// <param name="character">Первая буква фамилии</param>
        /// <returns>Список кассиров для ветки дерева навигации</returns>
        public List<TreeLeafModel> GetCashersOnCharacterBySector(int sectorId, string date, string character)
        {
            DateTime currentDate;
            if (!DateTime.TryParse(date, out currentDate))
            {
                currentDate = DateTime.Now;
            }

            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var c1Employees = context.C1cEmployee
                                             .Include(c => c.C1cDepartment)
                                             .Where(c => c.C1cDepartment.SectorID == sectorId &&
                                                         c.LastName.StartsWith(character) &&
                                                         (c.ResignationDate.HasValue
                                                              ? currentDate <= c.ResignationDate.Value
                                                              : c.ContractEffectiveTo.HasValue
                                                                    ? currentDate <= c.ContractEffectiveTo.Value
                                                                    : currentDate <= DateTime.MaxValue))
                                             .ToList();

                    var query = context.Employee
                                       .Include(e => e.Position)                                   
                                       .Where(e => e.SectorID == sectorId &&
                                              e.LastName.StartsWith(character) &&
                                              e.ContractEffectiveFrom <= currentDate &&
                                              (!e.ContractEffectiveTo.HasValue || currentDate <= e.ContractEffectiveTo) && 
                                              (e.Position.IsCashierRole))
                                              .OrderBy(e => e.LastName)
                                              .ToArray()
                                        .Select(e => new TreeLeafModel
                                        {
                                            ObjectName = e.LastName + " " + e.FirstName + " " + e.MiddleName,
                                            ObjectId = e.EmployeeID,
                                            ImgUrl = GetCashierStatusImageUrl(e, c1Employees.SingleOrDefault(x => x.EmployeeNumber.Equals(e.EmployeeNumber, 
                                                                                                                                          StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                                  x.FirstName.Equals(e.FirstName, 
                                                                                                                                     StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                                  x.MiddleName.Equals(e.MiddleName,
                                                                                                                                      StringComparison.InvariantCultureIgnoreCase) &&
                                                                                                                  x.LastName.Equals(e.LastName,
                                                                                                                                    StringComparison.InvariantCultureIgnoreCase)))
                                        });

                    return query.ToList();
            });
        }        

        /// <summary>
        /// Метод, возвращающий список первых букв фамилий кассиров участка,
        /// чей контракт действует в указанную дату
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="date">Дата для фильтрации кассиров по интервалам контрактов</param>
        /// <returns>Список первых букв фамилий кассиров участка</returns>
        public List<string> GetAllFirstCharactersOfSectorCashiersSurnames(int sectorId, string date)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                DateTime currentDate;
                if (!DateTime.TryParse(date, out currentDate))
                {
                    currentDate = DateTime.Now;                    
                }

                return context.Employee
                              .Where(e => e.SectorID == sectorId &&
                                          e.ContractEffectiveFrom <= currentDate &&
                                          (!e.ContractEffectiveTo.HasValue || currentDate <= e.ContractEffectiveTo) &&
                                          (e.Position.IsCashierRole))
                              .OrderBy(e => e.LastName)
                              .Select(e => e.LastName.Substring(0, 1))
                              .ToList();
            });
        }

        /// <summary>
        /// Метод, возвращающий первую букву фамилии кассира
        /// по идентификатору кассира
        /// </summary>
        /// <param name="employeeId">Идекнтификатор кассира</param>
        /// <returns>Первая буква фамилии указанного кассира</returns>
        public string GetFirstCharacterFromLastNameById(int employeeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var cashier = context.Employee.SingleOrDefault(s => s.EmployeeID == employeeId);

                if (cashier != null)
                {
                    return cashier.LastName.Substring(0, 1);
                }

                return string.Empty;
            });
        }

        /// <summary>
        /// Метод, возвращающий все профессиональные навыки кассиров
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, string > GetAllProfessionalLevels()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>context.ProfessionalLevel
                                                                       .ToDictionary(p => p.ProfessionalLevelID,
                                                                                     p => p.ProfessionalLevelName));
        }

        /// <summary>
        /// Конвертирует объект Entity Framework работника в модель кассира
        /// </summary>
        /// <param name="employee">Entity Framework объект кассира</param>
        /// <param name="model">Модель кассира</param>
        /// <param name="context">Объект для соединения с БД</param>
        /// <param name="currentDate">Текущая дата из фильтра для списка кассиров</param>
        /// <returns>Статус успеха</returns>
        private bool ToCashierModel(CashierModel model, Employee employee, GrokDb context, string currentDate)
        {
            if (employee != null)
            {
                DateTime filterDate;
                if (!DateTime.TryParse(currentDate, out filterDate))
                {
                    filterDate = DateTime.Now;
                }

                C1cEmployee employee1C = context.C1cEmployee.
                                              Include(e => e.C1cDepartment).
                                              Include(e => e.C1cDepartment.Sector1).
                                              SingleOrDefault(e => e.EmployeeNumber.Equals(employee.EmployeeNumber,
                                                                                           StringComparison.InvariantCultureIgnoreCase) &&
                                                                  e.FirstName.Equals(employee.FirstName,
                                                                                     StringComparison.InvariantCultureIgnoreCase) &&
                                                                  e.MiddleName.Equals(employee.MiddleName,
                                                                                      StringComparison.InvariantCultureIgnoreCase) &&
                                                                  e.LastName.Equals(employee.LastName,
                                                                                    StringComparison.InvariantCultureIgnoreCase) &&
                                                                  e.ContractEffectiveFrom <= filterDate &&
                                                                  (e.ResignationDate.HasValue
                                                                         ? filterDate <= e.ResignationDate.Value
                                                                         : e.ContractEffectiveTo.HasValue
                                                                                 ? filterDate <= e.ContractEffectiveTo.Value
                                                                                 : filterDate <= DateTime.MaxValue));                

                model.EmployeeId = employee.EmployeeID;
                model.FirstName = employee.FirstName;
                model.MiddleName = employee.MiddleName;
                model.LastName = employee.LastName;
                model.PersonalNumber = employee.EmployeeNumber;
                model.Rate = employee.RateQuantity;
                model.StationId = employee.BaseStationID;

                model.SpecializationIds = employee.EmployeeSpecialization.Select(s => s.SpecializationID).ToArray();
                model.ProfessionalLevelIds =
                    employee.EmployeeProfessionalLevel.Select(p => p.ProfessionalLevelID).ToArray();

                model.ContractEffectiveFrom = employee.ContractEffectiveFrom.HasValue ? employee.ContractEffectiveFrom.Value.ToShortDateString() : string.Empty;
                model.ContractEffectiveTo = employee.ContractEffectiveTo.HasValue ? employee.ContractEffectiveTo.Value.ToShortDateString() : string.Empty;
                model.MaximumHoursPerWeek = employee.MaximumHoursPerWeek.HasValue ? employee.MaximumHoursPerWeek.Value : DefaulMaximumHoursPerWeek;
                model.MaximumShiftLength = employee.MaximumShiftLength.HasValue ? employee.MaximumShiftLength : DefaultMaximumShiftLength;
                model.IsNighttimeAccepted = employee.IsNighttimeAccepted ?? false;
                model.IsOvertimeAccepted = employee.IsOvertimeAccepted ?? false;
                model.IsHolidayAccepted = employee.IsHolidayAccepted ?? false;

                ///Заполнение названия должности
                model.PositionId = employee.PositionID;
                if (model.PositionId.HasValue)
                {
                    var positionRepository = new PositionRepository();
                    var position = positionRepository.GetPositionById(model.PositionId.Value);
                    if (position != null)
                    {
                        model.Position = position.PositionName;
                    }
                }
                ///Заполнение названия сектора
                model.SectorId = employee.SectorID;
                if (model.SectorId.HasValue)
                {
                    var sectorRepository = new SectorRepository();
                    var sector = sectorRepository.GetSectorModelById(model.SectorId.Value);
                    if (sector != null)
                    {
                        model.Sector = sector.SectorName;
                    }
                }

                ///Заполнение данными из 1С
                if (employee1C != null)
                {
                    model.FirstName1C = employee1C.FirstName;
                    model.MiddleName1C = employee1C.MiddleName;
                    model.LastName1C = employee1C.LastName;
                    model.Position1С = employee1C.Position;
                    model.ContractEffectiveFrom1C = employee1C.ContractEffectiveFrom.HasValue ? employee1C.ContractEffectiveFrom.Value.ToShortDateString() : string.Empty;
                    model.ContractEffectiveTo1C = employee1C.ContractEffectiveTo.HasValue ? employee1C.ContractEffectiveTo.Value.ToShortDateString() : string.Empty;
                    model.Rate1C = employee1C.RateQuantity;
                    model.SectorId1C = employee1C.C1cDepartment == null ? null : employee1C.C1cDepartment.SectorID;
                    model.SectorName1C = employee1C.C1cDepartment == null ? null : employee1C.C1cDepartment.Sector1 == null ? null : employee1C.C1cDepartment.Sector1.SectorName;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Обновляет основные параметры Entity Framework объекта работника
        /// </summary>
        /// <param name="employee">Entity Framework объект работника</param>
        /// <param name="model">Модель кассира</param>
        private void UpdateEmployee(Employee employee, CashierModel model)
        {
            employee.FirstName = model.FirstName;
            employee.MiddleName = model.MiddleName;
            employee.LastName = model.LastName;
            employee.RateQuantity = model.Rate;
            employee.PositionID = model.PositionId;
            employee.SectorID = model.SectorId;
            employee.BaseStationID = model.StationId;
            DateTime effectiveFrom;
            if (DateTime.TryParse(model.ContractEffectiveFrom, out effectiveFrom))
            {
                employee.ContractEffectiveFrom = new DateTime?(effectiveFrom);
            }
            else
            {
                employee.ContractEffectiveFrom = null;
            }
            DateTime effectiveTo;
            if (DateTime.TryParse(model.ContractEffectiveTo, out effectiveTo))
            {
                employee.ContractEffectiveTo = new DateTime?(effectiveTo);
            }
            else
            {
                employee.ContractEffectiveTo = null;
            }
            employee.MaximumHoursPerWeek = model.MaximumHoursPerWeek;
            employee.MaximumShiftLength = model.MaximumShiftLength;
            employee.IsNighttimeAccepted = model.IsNighttimeAccepted;
            employee.IsOvertimeAccepted = model.IsOvertimeAccepted;
            employee.IsHolidayAccepted = model.IsHolidayAccepted;
        }

        /// <summary>
        /// Обновляет специализации работника
        /// </summary>
        /// <param name="employee">Entity Framework объект работника</param>
        /// <param name="newSpecializationsIds">Id новых специализаций</param>
        /// <param name="context">Entity Framework контекст</param>
        private void UpdateEmployeeSpecializations(Employee employee, int[] newSpecializationsIds, GrokDb context)
        {
            var employeeSpecializations = employee.EmployeeSpecialization;

            List<EmployeeSpecialization> specsToBeDeleted = new List<EmployeeSpecialization>(employee.EmployeeSpecialization);

            if (newSpecializationsIds != null)
            {
                List<int> ids = newSpecializationsIds.ToList();
                //Сопоставляем старые и новые списки специализаций.
                foreach (var spec in employeeSpecializations)
                {
                    int currentId = spec.SpecializationID;
                    if (ids.Contains(currentId))
                    {
                        ids.Remove(currentId);
                        specsToBeDeleted.Remove(spec);
                    }
                }
                //Добавляем элементы, которых не было в списке
                foreach (var id in ids)
                {
                    employeeSpecializations.Add(new EmployeeSpecialization() { EmployeeID = employee.EmployeeID, SpecializationID = id });
                }
            }

            //Удаляем элементы, которых нет в новом списке Ids
            foreach (var spec in specsToBeDeleted)
            {
                context.EmployeeSpecialization.Remove(spec);
            }

        }

        /// <summary>
        /// Обновляет профессиональные навыки работника
        /// </summary>
        /// <param name="employee">Entity Framework объект работника</param>
        /// <param name="newProfessionalLevelIds">Id новых профессиональных навыков</param>
        /// <param name="context">Entity Framework контекст</param>
        private void UpdateEmployeeProfessionLevels(Employee employee, int[] newProfessionalLevelIds, GrokDb context)
        {
            var employeeProfessionalLevels = employee.EmployeeProfessionalLevel;

            List<EmployeeProfessionalLevel> professionalLEvelsToBeDeleted = new List<EmployeeProfessionalLevel>(employee.EmployeeProfessionalLevel);

            if (newProfessionalLevelIds != null)
            {
                List<int> ids = newProfessionalLevelIds.ToList();
                //Сопоставляем старые и новые списки профессиональных навыков
                foreach (var employeeProfessionalLevel in employeeProfessionalLevels)
                {
                    int currentId = employeeProfessionalLevel.ProfessionalLevelID;
                    if (ids.Contains(currentId))
                    {
                        ids.Remove(currentId);
                        professionalLEvelsToBeDeleted.Remove(employeeProfessionalLevel);
                    }
                }
                //Добавляем элементы, которых не было в списке
                foreach (var id in ids)
                {
                    employeeProfessionalLevels.Add(new EmployeeProfessionalLevel() { EmployeeID = employee.EmployeeID, ProfessionalLevelID = id });
                }
            }

            //Удаляем элементы, которых нет в новом списке Ids
            foreach (var employeeProfessionalLevel in professionalLEvelsToBeDeleted)
            {
                context.EmployeeProfessionalLevel.Remove(employeeProfessionalLevel);
            }
        }

        /// <summary>
        /// Метод, обновляющий приоритеты станций работника (вызывается при назначении участка для работника)
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="context">DB-context</param>
        private void UpdateEmployeeStationPriority(int employeeId, int? sectorId, GrokDb context)
        {
            if (sectorId.HasValue)
            {
                var oldSectorPriorities = context.EmployeeStationPriority.
                                                    Where(e => e.EmployeeID == employeeId).
                                                    ToList();
                //Удаляем старые приоритеты, связанные с предыдущим участком сотрудника
                foreach (var oldSectorPriority in oldSectorPriorities)
                {
                    var stationPriority = oldSectorPriority.EmployeeStationPriorityID;
                    var intervalsOfSectorPriority = context.EmployeeWorkTimePriority.Where(e => e.EmployeeStationPriorityID == stationPriority);

                    //Удаляем интервалы для приоритета
                    foreach (var intervalOfSectorPriority in intervalsOfSectorPriority)
                    {
                        context.EmployeeWorkTimePriority.Remove(intervalOfSectorPriority);
                    }

                    context.EmployeeStationPriority.Remove(oldSectorPriority);
                }                                
            }
        }

        private string GetCashierStatusImageUrl(Employee employee, C1cEmployee employee1C)
        {
            if (employee1C == null) return "/Content/img/red.png";

            if ((employee.FirstName != employee1C.FirstName) || (employee.MiddleName != employee1C.MiddleName) || (employee.LastName != employee1C.LastName) ||
                (employee.Position.PositionName != employee1C.Position) || (employee.ContractEffectiveFrom != employee1C.ContractEffectiveFrom) ||
                (employee.ContractEffectiveTo != employee1C.ContractEffectiveTo) || (employee.RateQuantity != employee1C.RateQuantity) || ((employee1C.C1cDepartment != null) && (employee.SectorID != employee1C.C1cDepartment.SectorID)))
                return "/Content/img/yellow.png";

            return "/Content/img/green.png";
        }

        /// <summary>
        /// Метод, формирующий новый идентификатор состояния нахождения кассиров на участках,
        /// при изменении участка у кассира на форме "карточка кассира".
        /// </summary>
        /// <param name="context">объект соединения с базой данных</param>
        private void ChangeCashierVersion(GrokDb context)
        {
            var cashierObjectVersion = context.ObjectVersion
                                              .FirstOrDefault(o => o.ObjectName.Equals(CashierObjectName, StringComparison.OrdinalIgnoreCase));

            if (cashierObjectVersion == null)
            {
                context.ObjectVersion.Add(new ObjectVersion()
                    {
                        ObjectName = CashierObjectName,
                        Version = Guid.NewGuid()
                    });
            }
            else
            {
                cashierObjectVersion.Version = Guid.NewGuid();
            }
        }        

    }
}
