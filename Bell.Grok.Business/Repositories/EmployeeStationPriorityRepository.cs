﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class EmployeeStationPriorityRepository
    {
        /// <summary>
        /// Формат времени для конвертации значения TimeSpan в строку 
        /// </summary>
        private readonly string TimeFormat = @"hh\:mm";

        /// <summary>
        /// Метод, возвращающий список приоритетов станций кассира,
        /// отсортированный по приоритетам
        /// </summary>
        /// <param name="employeeId">Идентификатор кассира</param>
        /// <returns>Список объектов EmployeeStationPriorityGridModel</returns>
        public List<EmployeeStationPriorityGridModel> Search(int employeeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {                    
                    var query = context.EmployeeStationPriority.
                                        Include(e => e.EmployeeWorkTimePriority).
                                        Include(e => e.Station).
                                        Where(e => e.EmployeeID == employeeId).
                                        OrderBy(e => e.PriorityLevel).
                                        Select(e => new EmployeeStationPriorityGridModel()
                                            {
                                                Id = e.EmployeeStationPriorityID,
                                                StationName = e.Station.StationName,
                                                Priority = e.PriorityLevel.Value,
                                                EmployeeId = e.EmployeeID,
                                                TransferTime = e.TransferTime,
                                                HasIntervals = e.EmployeeWorkTimePriority.Any()
                                            });

                    return query.ToList();
                });
        }

        /// <summary>
        /// Метод, добавляющий новый приоритет станции для кассиру в базу
        /// </summary>
        /// <param name="model">Модель приоритета станции для кассира</param>
        public void CreateStationPriority(EmployeeStationPriorityCreateModel model)
        {
           GrokExceptionManager.ProcessGrokDb(context =>
               {
                   var employeeStationPriorityCount = context.EmployeeStationPriority
                                                             .Count(e => e.EmployeeID == model.EmployeeId);

                   var stationPriority = new EmployeeStationPriority()
                       {
                           EmployeeID = model.EmployeeId,
                           StationID = model.StationId,
                           TransferTime = model.TransferTime,
                           PriorityLevel = employeeStationPriorityCount + 1
                       };

                   context.EmployeeStationPriority.Add(stationPriority);
                   context.SaveChanges();
               });
        }

        /// <summary>
        /// Метод, обновляющий время в пути в приоритете станции кассира
        /// </summary>
        /// <param name="model">Модель приоритета станции кассира</param>
        public void UpdateStationIntervalTransferTime(EmployeeStationPriorityGridModel model)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
               {
                   var employeeStationPriority = context.EmployeeStationPriority
                                                        .FirstOrDefault(e => e.EmployeeStationPriorityID == model.Id);

                   if (employeeStationPriority != null)
                   {
                       employeeStationPriority.TransferTime = model.TransferTime;
                       context.SaveChanges();
                   }
               });
        }

        /// <summary>
        /// Метод, обновляющий интервалы времени для указанного в модели приоритета станции.        
        /// </summary>
        /// <param name="model">Объект модели EmployeeWeekWorkTimePriorityModel</param>
        public void UpdateStationPriorityIntervals(EmployeeWeekWorkTimePriorityModel model)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var oldIntervals =
                        context.EmployeeWorkTimePriority.Where(
                            e => e.EmployeeStationPriorityID == model.StationPriorityId);

                    //Удаляем уже существующие интервалы для приоритета
                    foreach (var oldInterval in oldIntervals)
                    {
                        context.EmployeeWorkTimePriority.Remove(oldInterval);
                    }

                    var newIntervals = model.EmployeeDayWorkTimePriorities;

                    //Добавляем новые взамен удалённых
                    foreach (var newInterval in newIntervals)
                    {
                        var employeeWorkTimePriority = new EmployeeWorkTimePriority()
                            {
                                EmployeeStationPriorityID = model.StationPriorityId,
                                WeekDayID = newInterval.WeekDayId,
                                BeginTime = newInterval.BeginTime,
                                EndTime = newInterval.EndTime
                            };

                        context.EmployeeWorkTimePriority.Add(employeeWorkTimePriority);
                    }

                    context.SaveChanges();
                });
        }

        /// <summary>
        /// Метод возвращающий список объектов EmployeeWorkTimePriorityGridModel для 
        /// таблицы временных интервалов указанного приоритета станции
        /// </summary>
        /// <param name="stationPriorityId">Идентификатор приоритета станции</param>
        /// <returns>Список объектов EmployeeWorkTimePriorityGridModel</returns>
        public List<EmployeeWorkTimePriorityGridModel> GetTimeIntervalsForStationPriority(int stationPriorityId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.EmployeeWorkTimePriority.
                                        Include(e => e.WeekDay).
                                        Where(e => e.EmployeeStationPriorityID == stationPriorityId).
                                        OrderBy(e => e.WeekDay.WeekDayNumber).
                                        AsEnumerable();

                    var data = query.Select(q => new EmployeeWorkTimePriorityGridModel()
                        {
                            Id = q.EmployeeWorkTimePriorityID,
                            DayName = q.WeekDay.WeekDayName,
                            WorkTime = q.BeginTime.HasValue && q.EndTime.HasValue
                                           ? q.BeginTime.Value.ToString(TimeFormat) + "-" +
                                             q.EndTime.Value.ToString(TimeFormat)
                                           : string.Empty
                        });

                    return data.ToList();
                });
        }

        /// <summary>
        /// Метод, возвращающий список объектов EmployeeWorkTimePriorityModel для отображения
        /// во всплывающем окне уже введённых временных интервалов указанного приоритета станции
        /// </summary>
        /// <param name="stationPriorityId">Идентификатор приоритета станции</param>
        /// <returns>Список объектов EmployeeWorkTimePriorityModel</returns>
        public List<EmployeeWorkTimePriorityModel> GetTimeIntervalsForWindow(int stationPriorityId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.EmployeeWorkTimePriority.
                                        Include(e => e.WeekDay).
                                        Where(e => e.EmployeeStationPriorityID == stationPriorityId).
                                        AsEnumerable();

                    var data = query.Select(q => new EmployeeWorkTimePriorityModel()
                        {
                            WeekDayNumber = q.WeekDay.WeekDayNumber,
                            BeginTime = q.BeginTime.HasValue
                                            ? q.BeginTime.Value.ToString(TimeFormat)
                                            : string.Empty,
                            EndTime = q.EndTime.HasValue
                                          ? q.EndTime.Value.ToString(TimeFormat)
                                          : string.Empty
                        });

                    return data.ToList();
                });
        }       

        /// <summary>
        /// Метод, удаляющий временные интервалы для указанного приоритета станции
        /// </summary>
        /// <param name="employeePriorityId">Идентификатор приоритета станции</param>
        public void DeletePriorityIntervals(int employeePriorityId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var employeeWorkTimePriorities =
                        context.EmployeeWorkTimePriority.Where(e => e.EmployeeStationPriorityID == employeePriorityId)
                               .ToList();

                    foreach (var employeeWorkTimePriority in employeeWorkTimePriorities)
                    {
                        context.EmployeeWorkTimePriority.Remove(employeeWorkTimePriority);
                    }

                    context.SaveChanges();
                });
        }

        /// <summary>
        /// Метод, удаляющий приоритет станции (естественно вместе со связанными интервалами)
        /// </summary>
        /// <param name="employeePriorityId">Идентификатор приоритета станции</param>
        public void DeletePriority(int employeePriorityId)
        {
             GrokExceptionManager.ProcessGrokDb(context =>
                 {
                     var stationPrioritet =
                         context.EmployeeStationPriority
                                .Include(e => e.EmployeeWorkTimePriority)
                                .FirstOrDefault( e => e.EmployeeStationPriorityID == employeePriorityId);

                     if (stationPrioritet != null)
                     {
                         //Сначала удаляем все временные интервалы
                         var workTimeIntervals = stationPrioritet.EmployeeWorkTimePriority.ToList();
                         workTimeIntervals.ForEach(w => context.EmployeeWorkTimePriority.Remove(w));

                         //Далее пересчитываем оставшиеся приоритеты
                         var moreLowPrioritets = context.EmployeeStationPriority
                                                        .Where(e => e.EmployeeID == stationPrioritet.EmployeeID &&
                                                                    e.PriorityLevel > stationPrioritet.PriorityLevel)
                                                        .ToList();
                         moreLowPrioritets.ForEach(m => m.PriorityLevel = m.PriorityLevel - 1);

                         //Собственно удаляем приоритет
                         context.EmployeeStationPriority.Remove(stationPrioritet);
                         context.SaveChanges();
                     }
                 });
        }
    }
}
