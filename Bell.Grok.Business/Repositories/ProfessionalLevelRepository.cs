﻿using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий по работе с профессиональными навыками
    /// </summary>
    public class ProfessionalLevelRepository
    {
        /// <summary>
        /// Получает полный список специализаций
        /// </summary>
        /// <returns>Список специализаций</returns>
        public List<ProfessionalLevelModel> GetAllProfessionalLevels()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var query = context.ProfessionalLevel.Select(p => new ProfessionalLevelModel()
                        {
                            ProfessionalLevelId = p.ProfessionalLevelID,
                            ProfessionalLevelName = p.ProfessionalLevelName
                        });

                    return query.ToList();
                });
        }
    }
}
