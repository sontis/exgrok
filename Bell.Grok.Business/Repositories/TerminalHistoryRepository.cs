﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий для работы с историзацей ККТ в базе данных
    /// </summary>
    public class TerminalHistoryRepository
    {
        /// <summary>
        /// Метод, сохраняющий информацию об изменении терминала или его привязки 
        /// к станции в базу
        /// </summary>
        /// <param name="sectorStationTerminal">Привязка терминала к станции, 
        ///                                     включает в себя информацию и о терминале</param>
        /// <param name="userId">Идентификатор пользователя, который произвёл изменения</param>
        public void SaveChanging(SectorStationTerminal sectorStationTerminal, int userId)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var terminalhistory = new TerminalHistory()
                        {
                            TerminalID = sectorStationTerminal.Terminal.TerminalID,
                            TerminalStatusID = sectorStationTerminal.Terminal.TerminalStatusID,
                            Comment = sectorStationTerminal.Terminal.Comment,
                            SectorStationTerminalID = sectorStationTerminal.SectorStationTerminalID,
                            StationBeginDate = sectorStationTerminal.BeginDate,
                            StationEndDate = sectorStationTerminal.EndDate,
                            UserID = userId,
                            ModifiedDate = DateTime.Now
                        };

                    context.TerminalHistory.Add(terminalhistory);
                    context.SaveChanges();
                });
        }
    }
}
