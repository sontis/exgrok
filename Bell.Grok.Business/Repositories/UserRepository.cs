﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Extensions;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public class UserRepository
    {        
        public UserRepository()
        {            
        }

        /// <summary>
        /// Метод, возвращающий объект сущности User
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="password">Пароль в хешированном виде</param>
        /// <returns>Объект сущности User</returns>
        public User GetUserByLogin(string userName, string password)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.Login == userName && u.Password == password);
                return user;
            });
        }

        /// <summary>
        /// Метод, возвращающий объект сущности User
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <returns>Объект сущности User</returns>
        public User GetUserByLogin(string userName)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.
                                SingleOrDefault(u => u.Login == userName);
                return user;
            });
        }        

        /// <summary>
        /// Метод, возвращающий всех пользователей
        /// </summary>
        /// <returns>Список объектов сущности User</returns>
        public IEnumerable<User> GetAllUsers()
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.User.AsEnumerable());
        }

        /// <summary>
        /// Метод, создающий нового пользователя
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        public void RegisterUser(string login, string password)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var user = new User()
                {
                    Login = login,
                    Password = password,
                    Status = (int)UserStatuses.Active
                };

                context.User.Add(user);
                context.SaveChanges();
            });
        }

        /// <summary>
        /// Метод, проверяющий существование пользователя с определённым логином
        /// </summary>
        /// <param name="login">Имя пользователя</param>
        /// <returns>Истина, если пользователь с указанным логином существует, и ложь в противном случае</returns>
        public bool UserWithLoginExists(string login)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                if (String.IsNullOrWhiteSpace(login))
                {
                    return false;
                }                

                return context.User.Count(u => u.Login == login) == 1;
            });
        }

        /// <summary>
        /// Метод, проверяющий существование пользователя с определёнными логином и паролем
        /// </summary>
        /// <param name="login">Имя пользователя</param>
        /// <param name="password">Пароль в хэшированном виде</param>
        /// <returns>Истина, если пользователь с указанными логином и паролем существует, и ложь в противном случае</returns>
        public bool UserWithLoginExists(string login, string password)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                if (String.IsNullOrWhiteSpace(login))
                {
                    return false;
                }

                return context.User.Count(u => u.Login == login && u.Password == password) == 1;
            });
        }        

        /// <summary>
        /// Метод, возвращающий отсортированный  и отфильтрованный список пользователей для таблицы
        /// </summary>
        /// <param name="page">Номер страницы таблицы</param>
        /// <param name="pageSize">Количество элементов на странице таблицы</param>
        /// <param name="sort">Список (из 1 элемента) с параметрами сортировки</param>
        /// <param name="filter">Коллекция параметров фильтрации</param>
        /// <param name="itemsCount">Общее количество пользователей (необходимо для правильного постраничного просмотра)</param>
        /// <returns>Массив объектов EmployeeGridModel</returns>
        public UserGridModel[] Search(int page, int pageSize, List<SortDescription> sort, FilterContainer filter, out int itemsCount)
        {
            int itemsCountResult = 0;

            var result= GrokExceptionManager.ProcessGrokDb(context =>
            {
                var filterFunctions = filter != null
                                                ? GetFilterFunctions(filter)
                                                : new List<Func<IQueryable<User>, IQueryable<User>>>();
                var orderFunction = GetUserOrderFunction(sort);

                var query = context.User.AsQueryable<User>();

                foreach (var filterFunction in filterFunctions)
                {
                    query = filterFunction(query);
                }
                query = orderFunction(query).AsQueryable();
                itemsCountResult = query.Count();

                var pagedList = new PagedList<User>(query, page, pageSize);                
                
                var data = pagedList.Select(e => new UserGridModel
                {
                    UserId = e.UserID,
                    IsActive = e.Status == (byte)UserStatuses.Active,                    
                    Login = e.Login,
                    Description = e.Comment,
                    FirstName = e.FirstName,
                    MiddleName = e.MiddleName,
                    LastName = e.LastName,
                    Role = GetUserRolesAsString(e.UserRole)
                });

                return data.ToArray();
            });

            itemsCount = itemsCountResult;
            return result;
        }

        /// <summary>
        /// Метод, возвращающий объект сущности User по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Объект сущност User</returns>
        public User GetUserById(int id)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.User.SingleOrDefault(u => u.UserID == id));
        }

        /// <summary>
        /// Метод, возвращающий логин пользователя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Логин пользователя</returns>
        public string GetLoginById(int id)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.UserID == id);
                return user != null ? user.Login : string.Empty;
            });
        }

        /// <summary>
        /// Метод, возвращающий идентификатор пользователя по логину
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Идентификатор пользователя</returns>
        public int GetIdByLogin(string login)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.Login == login);
                return user != null ? user.UserID : -1;
            });
        }

        /// <summary>
        /// Метод, проверяющий существование пользователя с определёнными идентификатором
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Истина, если пользователь с указанными логином и паролем существует, и ложь в противном случае</returns>
        public bool UserWithIdExists(int id)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.User.Any(u => u.UserID == id));
        }

        /// <summary>
        /// Метод, обновляющий сущность User данными из модели UserCreateFromDictionaryModel
        /// </summary>
        /// <param name="model">Объект модели UserCreateFromDictionaryModel</param>        
        public void CreateUserFromModel(UserCreateFromDictionaryModel model)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.Login == model.Login);
                user.UpdateFrom(model);

                UserRole userRole = new UserRole
                {
                    User = user,
                    RoleID = model.UserRole.RoleId,
                    SectorID = model.UserRole.SectorId                    
                };

                if (model.UserRole.DirectionIds != null && userRole.SectorID == null)
                {
                    foreach (var directionId in model.UserRole.DirectionIds)
                    {
                        var userRoleDirection = new UserRoleDirection();
                        userRoleDirection.DirectionID = directionId;
                        userRole.UserRoleDirection.Add(userRoleDirection);
                    }
                }

                context.UserRole.Add(userRole);

                context.SaveChanges();
            });
        }

        /// <summary>
        /// Метод, обновляющий сущность User данными из модели UserCreateModel
        /// </summary>
        /// <param name="model">Объект модели UserCreateModel</param>        
        public void CreateUserFromModel(UserModel model) 
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.Login == model.Login);
                user.UpdateFrom(model);
                
                var userRole = new UserRole
                {
                    User = user,
                    RoleID = model.UserRole.RoleId,
                    SectorID = model.UserRole.SectorId                    
                };

                if (model.UserRole.DirectionIds != null && userRole.SectorID == null)
                {
                    foreach (var directionId in model.UserRole.DirectionIds)
                    {
                        var userRoleDirection = new UserRoleDirection();
                        userRoleDirection.DirectionID = directionId;
                        userRole.UserRoleDirection.Add(userRoleDirection);
                    }
                }

                context.UserRole.Add(userRole);

                context.SaveChanges();
            });
        }

        /// <summary>
        /// Метод, обновляющий сущность User данными из модели UserEditModel
        /// </summary>
        /// <param name="model">Объект модели UserEditModel</param>
        public void UpdateUserFromModel(UserModel model)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.UserID == model.UserId);
                user.UpdateFrom(model);
                context.SaveChanges();
            });
        }

        /// <summary>
        /// Метод, обновляющий статус пользователя
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="status">Новый статус</param>
        public void UpdateUserStatus(int userId, UserStatuses status)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.UserID == userId);

                if (user != null)
                {
                    user.Status = (byte) status;
                    context.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Метод, меняющий пароль пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="password">Пароль в хэшированном виде</param>
        public void UpdateUserPassword(string login, string password)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                User user = context.User.SingleOrDefault(u => u.Login == login);
                user.Password = password;
                context.SaveChanges();
            });
        }

        /// <summary>
        /// Метод, проверяющий, блокирован ли пользователь
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Истина, если пользователь блокирован, и ложь в противном случае</returns>
        public bool IsUserBlocked(string login)
        {            
            User user = this.GetUserByLogin(login);
            if (user == null)
            {
                return false;
            }
            else
            {
                return user.Status == (byte)UserStatuses.Blocked;   
            }                            
        }

        /// <summary>
        /// Метод, копирующий данные из объекта сущности User в объект модели UserEditModel
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <param name="model">Объект для заполнения</param>
        /// <returns>Результат поиска пользователя по id</returns>
        public bool EditModelFromUser(int userId, UserModel model) 
        {
            var user = this.GetUserById(userId);
            if (user == null)
            {
                return false;
            }
            
            model.UserId = userId;
            model.Edit(user);

            return true;
        }

        /// <summary>
        /// Метод, возвращающий ФИО пользователя по его логину
        /// </summary>
        /// <param name="login">Логин</param>
        /// <returns>ФИО пользователя</returns>
        public string GetFullNameByLogin(string login)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var user = context.User.SingleOrDefault(u => u.Login.Equals(login, StringComparison.CurrentCultureIgnoreCase));

                return user == null
                           ? string.Empty
                           : user.LastName + " " + user.FirstName + " " + user.MiddleName;
            });
        }

        /// <summary>
        /// Метод, возвращающий функцию сортировки
        /// </summary>
        /// <param name="sort">Параметры сортировки</param>
        /// <returns>Функция сортировки</returns>
        private Func<IQueryable<User>, IOrderedQueryable<User>> GetUserOrderFunction(List<SortDescription> sort)
        {
            var sortDescription = sort[0];

            switch (sortDescription.Field)
            {
                case "Login":
                    return query => UserSorter.OrderByLogin(query, sortDescription.Dir); 
                case "FirstName":
                    return query => UserSorter.OrderByFirstName(query, sortDescription.Dir);
                case "MiddleName":
                    return query => UserSorter.OrderByMiddleName(query, sortDescription.Dir);
                case "LastName":
                    return query => UserSorter.OrderByLastName(query, sortDescription.Dir);
                case "Role":
                    return query => UserSorter.OrderByRole(query, sortDescription.Dir);
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field + "\' не поддерживается");
            }
        }


        private List<Func<IQueryable<User>, IQueryable<User>>> GetFilterFunctions(FilterContainer filter)
        {
            var queries = new List<Func<IQueryable<User>, IQueryable<User>>>();

            if (filter.filters != null)
            {
                foreach (var filterItem in filter.filters)
                {
                    var searchValue = filterItem.value;

                    if (filterItem.field == "FirstName")
                    {                        
                        queries.Add(query => UserFilter.FilterByFirstName(query, searchValue));
                    }

                    if (filterItem.field == "MiddleName")
                    {
                        queries.Add(query => UserFilter.FilterByMiddleName(query, searchValue));
                    }
                    
                    if (filterItem.field == "LastName")
                    {
                        queries.Add(query => UserFilter.FilterByLastName(query, searchValue));
                    }
                }
            }

            return queries;
        }
        
        /// <summary>
        /// Получает описание пользователя для списка пользователей вида "Направление, должность"
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns>Значение поля описание</returns>
        private string GetUserDescription(User user, GrokDb context)
        {
            string result = string.Empty;
            var employee = context.Employee.SingleOrDefault(e => e.EmployeeID == user.EmployeeID);

            if (employee != null)
            {
                if ((employee.Sector != null) && (employee.Position != null))
                {
                    result = string.Format("{0}, {1}", employee.Sector.SectorName, employee.Position.PositionName);
                }
            }

            return result;
        }

        private string GetUserRolesAsString(IEnumerable<UserRole> roles)
        {
            var result = new StringBuilder();

            foreach (var role in roles)
            {
                result.AppendFormat("{0} ", role.Role.RoleName);
            }
            return result.ToString();
        }
    }
}
