﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data.Entity;
using System.Collections.Specialized;
using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public sealed class EmployeeRepository
    {
        public EmployeeRepository()
        {
        }

        /// <summary>
        /// Метод, возвращающий объект сущности Employee по идентификатору
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника ЦППК</param>
        /// <returns>Объект сущности Employee по идентификатору</returns>
        public Employee GetById(int employeeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Employee.FirstOrDefault(e => e.EmployeeID == employeeId));
        }

        /// <summary>
        /// Метод, проверяющий наличие сотрудников на участке
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Истина, если сотрудники на участке есть, и ложь в противном случае</returns>
        public bool AreEmployeesOnSector(int sectorId)
        {
            using (var context = new GrokDb())
            {
                return context.Employee.Any(e => e.SectorID == sectorId);
            }
        }

        /// <summary>
        /// Метод, возвращающий отсортированный  и отфильтрованный список сотрудников для таблицы
        /// </summary>
        /// <param name="page">Номер страницы таблицы</param>
        /// <param name="pageSize">Количество элементов на странице таблицы</param>
        /// <param name="orderby">Строка с параметрами сортировки</param>
        /// <param name="filterDictionary">Коллекция параметров фильтрации</param>
        /// <param name="itemsCount">Общее количество сотрудников (необходимо для правильного постраничного просмотра)</param>
        /// <param name="timesheetId">Для табелей - для отбраковки сотрудников, которые уже есть в табеле</param>
        /// <returns>Массив объектов EmployeeGridModel</returns>
        public EmployeeGridModel[] Search(int page, int pageSize, string orderby, NameValueCollection filterDictionary, int? timesheetId, out int itemsCount)
        {
            var itemsCountInner = 0;

            var result = GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var filterFunctions = GetFilterFunctions(filterDictionary);
                    var orderFunction = GetOrderFunction(orderby);

                    var query = context.Employee.
                                        Include(e => e.Sector).
                                        Include(e => e.Position).AsQueryable<Employee>();
                    foreach (var filterFunction in filterFunctions)
                    {
                        query = filterFunction(query);
                    }

                    //Спецпроверка для списка сотрудников в табеле
                    if (timesheetId.HasValue)
                    {
                        query = EmployeeFilter.ExcludeTimesheetEmployees(query, timesheetId.Value);
                    }

                    query = orderFunction(query).AsQueryable();
                    itemsCountInner = query.Count();

                    var pagedList = new PagedList<Employee>(query, page, pageSize);

                    var data = pagedList.Select(e => new EmployeeGridModel
                        {
                            EmployeeId = e.EmployeeID,
                            EmployeeNumber = e.EmployeeNumber,
                            EmployeeFirstName = e.FirstName,
                            EmployeeMiddleName = e.MiddleName,
                            EmployeeLastName = e.LastName,
                            Sector = e.Sector == null ? string.Empty : e.Sector.SectorName,
                            Position = e.Position.PositionName,
                            Description =
                                e.Sector == null
                                    ? e.Position.PositionName
                                    : e.Sector.SectorName + ", " + e.Position.PositionName
                        });

                    return data.ToArray();
                });
            itemsCount = itemsCountInner;
            return result;
        }        

        /// <summary>
        /// Метод, возвращающий функцию сортировки
        /// </summary>
        /// <param name="orderBy">Параметры сортировки</param>
        /// <returns>Функция сортировки</returns>
        private Func<IQueryable<Employee>, IOrderedQueryable<Employee>> GetOrderFunction(string orderBy)
        {
            var sortDescription = SortDescriptionCreateHelper.FactoryMethod(orderBy);

            switch (sortDescription.Field)
            {
                case "EmployeeNumber":
                    return query => EmployeeSorter.OrderByEmployeeNumber(query, sortDescription.IsAscending);
                case "EmployeeFirstName":
                    return query => EmployeeSorter.OrderByEmployeeFirstName(query, sortDescription.IsAscending);
                case "EmployeeMiddleName":
                    return query => EmployeeSorter.OrderByEmployeeMiddleName(query, sortDescription.IsAscending);
                case "EmployeeLastName":
                    return query => EmployeeSorter.OrderByEmployeeLastName(query, sortDescription.IsAscending);
                case "Sector":
                    return query => EmployeeSorter.OrderBySector(query, sortDescription.IsAscending);
                case "Position":
                    return query => EmployeeSorter.OrderByPosition(query, sortDescription.IsAscending);
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field +
                                                    "\' не поддерживается");
            }
        }

        /// <summary>
        /// Метод, возвращающий список функций фильтрации
        /// </summary>
        /// <param name="filterDictionary">Коллекция параметров фильтрации</param>
        /// <returns>Список функций фильтрации</returns>
        private List<Func<IQueryable<Employee>, IQueryable<Employee>>> GetFilterFunctions(NameValueCollection filterDictionary)
        {
            List<Func<IQueryable<Employee>, IQueryable<Employee>>> queries = new List<Func<IQueryable<Employee>, IQueryable<Employee>>>();

            var firstName = filterDictionary["EmployeeFirstName"];
            if (!string.IsNullOrEmpty(firstName))
            {
                queries.Add(query => EmployeeFilter.FilterByFirstName(query, firstName));
            }

            var middleName = filterDictionary["EmployeeMiddleName"];
            if (!string.IsNullOrEmpty(middleName))
            {
                queries.Add(query => EmployeeFilter.FilterByMiddleName(query, middleName));
            }

            var lastName = filterDictionary["EmployeeLastName"];
            if (!string.IsNullOrEmpty(lastName))
            {
                queries.Add(query => EmployeeFilter.FilterByLastName(query, lastName));
            }

            return queries;
        }

        /// <summary>
        /// Метод, возвращающий объекты модели DropDownList по идентификатору участка
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="month">Месяц</param>
        /// <returns>Объект модели DropDownList по идентификатору</returns>
        public Dictionary<int, string> GetEmployees(int? sectorId, SimpleMonthModel month)
        {
            return GrokExceptionManager.ProcessGrokDb(context => 
                                                      context.Employee
                                                             .Where(e => e.SectorID == sectorId)
                                                             .ToList()
                                                             .Where(e => (!e.ContractEffectiveFrom.HasValue) || 
                                                                          ((e.ContractEffectiveFrom.Value.Year < month.Year) ||
                                                                           ((e.ContractEffectiveFrom.Value.Year == month.Year) && 
                                                                            (e.ContractEffectiveFrom.Value.Month <= month.Month))))
                                                             .Where(e => (!e.ContractEffectiveTo.HasValue) || 
                                                                          ((e.ContractEffectiveTo.Value.Year > month.Year) ||
                                                                           ((e.ContractEffectiveTo.Value.Year == month.Year) && 
                                                                            (e.ContractEffectiveTo.Value.Month >= month.Month))))
                                                             .OrderBy(e => e.LastName)
                                                             .ToDictionary(e => e.EmployeeID,
                                                                           e => string.Format("{0} {1} {2}", e.LastName, e.FirstName, e.MiddleName)));
        }

        /// <summary>
        /// Метод, возвращающий полное имя кассира по идентификатору
        /// </summary>
        /// <param name="employeeId">Идентификатор кассира</param>
        /// <returns>Полное имя кассира</returns>
        public string GetFullNameById(int employeeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var employee = context.Employee.FirstOrDefault(e => e.EmployeeID == employeeId);

                    return employee != null
                               ? employee.LastName + " " + employee.FirstName + " " +
                                 (!string.IsNullOrWhiteSpace(employee.MiddleName)
                                      ? employee.MiddleName
                                      : string.Empty)
                               : string.Empty;
                });
        }

        /// <summary>
        /// Метод, возвращающий название шаблона графика для кассира
        /// </summary>
        /// <param name="employeeId">Идентификатор кассира</param>
        /// <returns>Название наблона графика для кассира</returns>
        public string GetScheduleTemplateNameForEmployee(int employeeId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var employee = context.Employee.FirstOrDefault(e => e.EmployeeID == employeeId);

                    return employee != null
                               ? employee.ScheduleDetail.Any()
                                     ? employee.ScheduleDetail.FirstOrDefault()
                                               .ScheduleTemplateDetail.ScheduleTemplateMode.ScheduleTemplate.ShortName
                                     : string.Empty
                               : string.Empty;
                });
        }

    }
}
