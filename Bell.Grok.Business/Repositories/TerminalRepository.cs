﻿using Bell.Grok.Business.Logging;
using Bell.Grok.DAL;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Extensions;
using System.Data.Entity;
using Bell.Grok.Model;
using System.Data.SqlTypes;
using TerminalModel = Bell.Grok.DAL.TerminalModel;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Репозиторий по работе с терминалами
    /// </summary>
    public class TerminalRepository
    {
        /// <summary>
        /// Формат времени для конвертации значения DateTime в строку 
        /// </summary>
        private const string TimeFormat = "dd.MM.yyyy";

        /// <summary>
        /// Делегат для сохранения информации об изменении терминала и его привязок к станциям
        /// </summary>
        /// <param name="sectorStationTerminal">Привязка терминала к станции, 
        ///                                     включает в себя информацию и о терминале</param>
        /// <param name="userName"></param>
        public delegate void SaveTerminalChangingInHistory(SectorStationTerminal sectorStationTerminal,
                                                           string userName);      

        /// <summary>
        /// Событие изменения терминала и его привязок к станциям
        /// </summary>
        public static event SaveTerminalChangingInHistory TerminalChanged;        

        /// <summary>
        /// Получает полный список терминалов
        /// </summary>
        /// <returns>Список терминалов</returns>
        public List<TerminalModel> GetAll()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получает терминал по Id
        /// </summary>
        /// <param name="id">Id терминала</param>
        /// <returns>Терминал</returns>
        public TerminalModel GetByID(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Метод, проверяющий существование терминала с определённым номером в базе
        /// </summary>
        /// <param name="number">Номер терминала</param>        
        /// <returns>Истина, если терминал с указанным номером существует, и ложь в противном случае</returns>
        public bool TerminalWithNumberExists(string number)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Terminal.Count(t => t.TerminalNumber == number) == 1);
        }        

        /// <summary>
        /// Метод, проверяющий, используется ли терминал с указанным номером в текущий момент, то есть
        /// находится ли в эксплуатации и привязан ли к какому-либо окну
        /// </summary>
        /// <param name="number">Номер терминала</param>
        /// <returns>Истина, если терминал с указанным номером, используется в текущий момент и ложь в противном случае</returns>
        public bool IsTerminalUsingInCurrentDate(string number)
        {
            var currentDate = DateTime.Now;

            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var terminal = context.Terminal.
                                        Include(t => t.TerminalPurposeHistory).                                        
                                        FirstOrDefault(t => t.TerminalNumber == number);

                if (terminal == null)
                {
                    return false;
                }

                var isUsedNow = terminal.TerminalPurposeHistory.Any(tph => tph.BeginDate <= currentDate &&
                                                    (tph.DateEnd.HasValue
                                                        ? currentDate <= tph.DateEnd.Value
                                                        : currentDate <= DateTime.MaxValue));                

                return isUsedNow;
            });
        }

        /// <summary>
        /// Метод, ищущий привязку терминала к станции по номеру терминала и периоду привязки
        /// </summary>
        /// <param name="number">Номер терминала</param>
        /// <param name="beginDate">Дата начала привязки</param>
        /// <param name="endDate">Дата окончания привязки</param>
        /// <returns>Объект сущности SectorStationTerminal</returns>
        public SectorStationTerminal IsTerminalBusyInPeriod(string number, DateTime beginDate, DateTime endDate)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var terminal = context.Terminal.
                                        Include(t => t.TerminalPurposeHistory).  
                                        Include(t => t.SectorStationTerminal.Select(s => s.SectorStation)).
                                        Include(t => t.SectorStationTerminal.Select(s => s.SectorStation.Sector)).
                                        Include(t => t.SectorStationTerminal.Select(s => s.SectorStation.Station)).
                                        FirstOrDefault(t => t.TerminalNumber == number);

                if (terminal == null)
                {
                    return null;
                }                

                return
                    terminal.SectorStationTerminal.FirstOrDefault(
                        sst => sst.BeginDate <= endDate && (sst.EndDate.HasValue ? beginDate <= sst.EndDate.Value : beginDate <= DateTime.MaxValue) );
            });
        }

        /// <summary>
        /// Метод, ищущий привязку терминала к станции по номеру терминала и периоду привязки, 
        /// кроме привязки с указанным идентификатором
        /// </summary>
        /// <param name="number">Номер терминала</param>
        /// <param name="beginDate">Дата начала привязки</param>
        /// <param name="endDate">Дата окончания привязки</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Объект сущности SectorStationTerminal</returns>
        public SectorStationTerminal IsTerminalBusyInPeriod(string number, DateTime beginDate, DateTime endDate, int sectorStationTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var terminal = context.Terminal.
                                        Include(t => t.TerminalPurposeHistory).
                                        Include(t => t.SectorStationTerminal.Select(s => s.SectorStation)).
                                        Include(t => t.SectorStationTerminal.Select(s => s.SectorStation.Sector)).
                                        Include(t => t.SectorStationTerminal.Select(s => s.SectorStation.Station)).
                                        FirstOrDefault(t => t.TerminalNumber == number);

                if (terminal == null)
                {
                    return null;
                }

                return
                    terminal.SectorStationTerminal.FirstOrDefault(
                        sst => (    sst.BeginDate <= endDate && 
                                    (sst.EndDate.HasValue ? 
                                        beginDate <= sst.EndDate.Value : 
                                        beginDate <= DateTime.MaxValue)) &&
                               (    sst.SectorStationTerminalID != sectorStationTerminalId));
            });
        }

        /// <summary>
        /// Метод, возвращающий информацию о терминале (тип, модель, назначение, агент, профиль и статус)
        /// </summary>
        /// <param name="number">Номер терминала</param>
        /// <returns>Объект модели ExistingTerminalInformationModel</returns>
        public ExistingTerminalInformationModel GetInformationAboutExistionTerminal(string number)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var terminal = context.Terminal
                                      .Include(t => t.TerminalPurposeHistory)
                                      .FirstOrDefault(t => t.TerminalNumber == number);

                if (terminal == null)
                {
                    return null;
                }

                return new ExistingTerminalInformationModel()
                {
                    Type = terminal.TerminalTypeID,
                    Model = terminal.TerminalModelID ?? -1,
                    Profile = terminal.TerminalProfileID,
                    Agent = terminal.AgentID,
                    Purpose = terminal.TerminalPurposeHistory.Any() ? 
                                terminal.TerminalPurposeHistory.First().TerminalPurposeID :
                                -1,
                    Status = terminal.TerminalStatusID                    
                };
            });
        }


        /// <summary>
        /// Метод, проверяющий существование терминала с определённым идентификатором
        /// </summary>
        /// <param name="terminalId">Идентификатор терминала</param>
        /// <returns>Истина, если терминал с указанным идентификатором существует, и ложь в противном случае</returns>
        public bool TerminalWithIdExists(int terminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Terminal.Any(t => t.TerminalID == terminalId));
        }

        /// <summary>
        /// Метод, возвращающий объект сущности Terminal
        /// </summary>
        /// <param name="terminalId">Идентификатор терминала</param>
        /// <returns>Объект сущности Terminal</returns>
        public Terminal GetTerminalById(int terminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context => context.Terminal.
                                                                   Include(t => t.TerminalPurposeHistory).                               
                                                                   SingleOrDefault(t => t.TerminalID == terminalId));
        }

        /// <summary>
        /// Метод, возвращающий все типы терминалов
        /// </summary>
        /// <returns>Список типов терминалов</returns>
        public Dictionary<int, string> GetAllTypes()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>            
                                                      context.TerminalType
                                                             .ToDictionary(tt => tt.TerminalTypeID,
                                                                           tt => tt.TerminalTypeName));
        }

        /// <summary>
        /// Метод, возвращающий все модели терминалов
        /// </summary>
        /// <returns>Список моделей терминалов</returns>
        public Dictionary<int, string> GetAllModels()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TerminalModel
                                                             .ToDictionary(tm => tm.TerminalModelID,
                                                                           tm => tm.TerminalModelName));
        }

        /// <summary>
        /// Метод, возвращающий все профили терминалов
        /// </summary>
        /// <returns>Список профилей терминалов</returns>
        public Dictionary<int, string> GetAllProfiles()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TerminalProfile
                                                             .ToDictionary(tp => tp.TerminalProfileID,
                                                                           tp => tp.TerminalProfileName));
        }

        /// <summary>
        /// Метод, возвращающий все статусы терминалов
        /// </summary>
        /// <returns>Список статусов терминалов</returns>
        public Dictionary<int, string> GetAllStatuses()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TerminalStatus
                                                             .ToDictionary(ts => ts.TerminalStatusID,
                                                                           ts => ts.TerminalStatusName));
        }

        /// <summary>
        /// Метод, возвращающий все назначений терминалов
        /// </summary>
        /// <returns>Список назначений терминалов</returns>
        public Dictionary<int, string> GetAllPurposes()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TerminalPurpose
                                                             .ToDictionary(tp => tp.TerminalPurposeID,
                                                                           tp => tp.TerminalPurposeName));
        }

        /// <summary>
        /// Метод, возвращающий всех агентов терминалов
        /// </summary>
        /// <returns>Список агентов терминалов</returns>
        public Dictionary<int, string> GetAllAgetns()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>context.Agent
                                                                       .ToDictionary(a => a.AgentID,
                                                                                     a => a.AgentName));
        }

        /// <summary>
        /// Метод, возвращающий все названия расположений терминалов
        /// </summary>
        /// <returns>Список расположений терминалов</returns>
        public Dictionary<int, string> GetAllPlacements()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                                                      context.TerminalPlacement
                                                             .ToDictionary(tp => tp.TerminalPlacementID,
                                                                           tp => tp.TerminalPlacementName));
        }

        /// <summary>
        /// Метод, возвращающий модели терминалов в зависимости от типа.
        /// </summary>
        /// <param name="currentType">Идентификатор типа терминалов</param>
        /// <returns>Список моделей, соответствующий указанному типу</returns>
        public Dictionary<int, string> GetTypeMarks(int currentType)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>context.TerminalModel
                                                                        .Where(tm => tm.TerminalTypeID == currentType)
                                                                        .ToDictionary(tm => tm.TerminalModelID,
                                                                                      tm => tm.TerminalModelName));
        }

        /// <summary>
        /// Метод, обновляющий сущность Terminal (вместе с навигационными свойствами) данными из модели TerminalCreateModel
        /// </summary>
        /// <param name="model">Объект модели TerminalCreateModel</param>
        public void UpdateFromModel(Model.TerminalModel model, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                Terminal terminal;
                SectorStationTerminal sectorStationTerminal;
                if (this.TerminalWithNumberExists(model.Number))
                {
                    //Создание новой привязки терминала к станции
                    var sectorStation =
                        context.SectorStation.SingleOrDefault(
                            ss => ss.SectorID == model.SectorId && ss.StationID == model.StationId);

                    terminal = context.Terminal.
                                       Include(t => t.TerminalPurposeHistory).
                                       SingleOrDefault(t => t.TerminalNumber == model.Number);
                    sectorStationTerminal = new SectorStationTerminal();

                    terminal.UpdateFrom(model, sectorStationTerminal, sectorStation);
                }
                else
                {
                    //Создание нового терминала
                    terminal = new Terminal();
                    var terminalPurposeHistory = new TerminalPurposeHistory();
                    sectorStationTerminal = new SectorStationTerminal();
                    var sectorStation =
                        context.SectorStation.SingleOrDefault(
                            ss => ss.SectorID == model.SectorId && ss.StationID == model.StationId);

                    terminal.UpdateFrom(terminalPurposeHistory, sectorStationTerminal, sectorStation, model);
                    context.Terminal.Add(terminal);
                }                                
                
                context.SaveChanges();
                TerminalChanged(sectorStationTerminal, userName);
            });
        }

        /// <summary>
        /// Метод, обновляющий сущность SectorStationTerminal (вместе с навигационным свойством Terminal) данными из модели TerminalEditMode
        /// </summary>
        /// <param name="model">Объект модели TerminalEditMode</param>        
        public void EditFromModel(Model.TerminalModel model, string userName)
        {
            GrokExceptionManager.ProcessGrokDb(context =>
            {
                var sectorStationTerminal = context.SectorStationTerminal.
                                                    Include(s => s.TellerWindowTerminal).
                                                    Include(s => s.Terminal).
                                                    Include(s => s.Terminal.TerminalPurposeHistory).
                                                    SingleOrDefault(s => s.SectorStationTerminalID == model.SectorStationTerminalId);

                if (sectorStationTerminal != null)
                {
                    sectorStationTerminal.UpdateFrom(model);
                    sectorStationTerminal.CheckUnfinishedTellerWindowTerminalIntervals();

                    context.SaveChanges();
                    TerminalChanged(sectorStationTerminal, userName);
                }
            });
        }

        /// <summary>
        /// Метод, возвращающий отсортированный  и отфильтрованный список привязок терминалов к станции (и информацией об этом терминале) для таблицы
        /// </summary>
        /// <param name="page">Номер страницы таблицы</param>
        /// <param name="pageSize">Количество элементов на странице таблицы</param>
        /// <param name="orderby">Строка с параметрами сортировки</param>
        /// <param name="filterDictionary">Коллекция параметров фильтрации</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="itemsCount">Общее количество привязок терминалов к станции (необходимо для правильного постраничного просмотра)</param>
        /// <returns>Список объектов TerminalGridModel</returns>
        public List<TerminalGridModel> Search(int page, int pageSize, string orderby,
                                              NameValueCollection filterDictionary, int sectorId, int stationId,
                                              out int itemsCount)
        {
            var itemsCountInner = 0;
            var result= GrokExceptionManager.ProcessGrokDb(context =>
            {
                var filterFunctions = GetFilterFunctions(filterDictionary);
                var orderFunction = GetTerminalOrderFunction(orderby);

                var query = context.SectorStationTerminal.
                                        Include(s => s.Terminal).
                                        Include(s => s.Terminal.TerminalPurposeHistory).
                                        Include(s => s.Terminal.TerminalPurposeHistory.Select(tp => tp.TerminalPurpose)).
                                        Include(s => s.Terminal.TerminalModel).
                                        Include(s => s.Terminal.Agent).
                                        Include(s => s.Terminal.TerminalPlacement).                                        
                                        Include(s => s.Terminal.TerminalStatus).
                                        Include(s => s.Terminal.TerminalType).
                                        Include(s => s.Terminal.TerminalProfile).
                                        Include(s => s.Terminal.SectorStationTerminal).
                                        AsQueryable<SectorStationTerminal>();                    

                foreach (var filterFunction in filterFunctions)
                {
                    query = filterFunction(query);
                }
                query = query.Where(s => s.SectorStation.SectorID == sectorId &&
                                         s.SectorStation.StationID == stationId);

                query = orderFunction(query).AsQueryable();                

                var pagedList = new PagedList<SectorStationTerminal>(query, page, pageSize);
                itemsCountInner = pagedList.ItemCount;

                var data = pagedList.Select(s => new TerminalGridModel()
                    {
                        Id = s.Terminal.TerminalID,
                        Number = s.Terminal.TerminalNumber,
                        AgentName = s.Terminal.Agent.AgentName,
                        Comment = s.Terminal.Comment,
                        ModelName = s.Terminal.TerminalModelID != null ?
                                        s.Terminal.TerminalModel.TerminalModelName :
                                        string.Empty,
                        ProfileName = s.Terminal.TerminalProfile.TerminalProfileName,
                        Placement = s.Terminal.TerminalPlacement.TerminalPlacementName + " " + s.Terminal.PlacementNumericalValue + " " + s.Terminal.PlacementComment,
                        PurposeName = s.Terminal.TerminalPurposeHistory.FirstOrDefault() != null ?
                                      s.Terminal.TerminalPurposeHistory.FirstOrDefault().TerminalPurpose.TerminalPurposeName :
                                      string.Empty,
                        BeginDate = s.BeginDate.ToString(TimeFormat),
                        EndDate = s.EndDate != null ?
                                            s.EndDate.Value.ToString(TimeFormat) :
                                            string.Empty,
                        TypeName = s.Terminal.TerminalType.TerminalTypeName,
                        StatusName = s.Terminal.TerminalStatus.TerminalStatusName,
                        SectorStationTerminalId = s.SectorStationTerminalID
                    });

                return data.ToList();
            });

            itemsCount = itemsCountInner;
            return result;
        }

        /// <summary>
        /// Метод, копирующий данные из объекта сущности SectorStationTerminal (вместе с навигационным свойством Terminal) в объект модели TerminalEditModel
        /// </summary>
        /// <param name="model">Объект модели TerminalEditModel</param>        
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Истина в случае успеха завершения и ложь, если в базе нет объекта сущности Terminal с указанным идентификатором</returns>
        public bool EditModelFromTerminal(Model.TerminalModel model, int sectorStationTerminalId)
        {
            var sectorStationTerminalRepository = new SectorStationTerminalRepository();

            var sectorStationTerminal = sectorStationTerminalRepository.GetSectorStationTerminalById(sectorStationTerminalId);
            if (sectorStationTerminal == null)
            {
                return false;
            }
                        
            model.SectorStationTerminalId = sectorStationTerminalId;
            model.Edit(sectorStationTerminal, TimeFormat);

            return true;
        }        

        /// <summary>
        /// Метод, возвращающий номер терминала с указанным идентификатор
        /// </summary>
        /// <param name="terminalId">Идентификатор терминала</param>
        /// <returns>Номер терминала</returns>
        public string GetNumberById(int terminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var terminal = context.Terminal.SingleOrDefault(t => t.TerminalID == terminalId);

                return terminal != null ? terminal.TerminalNumber : string.Empty;
            });
        }

        /// <summary>
        /// Метод, возвращающий номер и профиль терминала по его идентификатору привязки к станции
        /// </summary>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Номер и профиль терминала</returns>
        public Tuple<string, int> GetNumberAndProfileBySectorStationTerminalId(int sectorStationTerminalId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
                {
                    var number = string.Empty;                    
                    var profile = 0;

                var sectorStationTerminal =
                    context.SectorStationTerminal.SingleOrDefault(
                        s => s.SectorStationTerminalID == sectorStationTerminalId);

                if (sectorStationTerminal != null)
                {
                    if (sectorStationTerminal.Terminal != null)
                    {
                        number = sectorStationTerminal.Terminal.TerminalNumber;
                        profile = sectorStationTerminal.Terminal.TerminalProfileID;
                    }
                }

                return new Tuple<string, int>(number, profile);
            });
        }        

        /// <summary>
        /// Метод, возвращающий список номеров окон для фильтрации, 
        /// к которым в данный момент привязаны терминалы (в пределах указанной станции)
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Список кассовых окон. к которым прикреплены терминалы</returns>
        public List<TerminalNumberModel> GetAllNumbers(int sectorId, int stationId)
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var query = context.SectorStationTerminal
                                   .Include(s => s.Terminal)
                                   .Where(t => t.SectorStation.SectorID == sectorId &&
                                               t.SectorStation.StationID == stationId)
                                   .OrderBy(t => t.Terminal.TerminalNumber)
                                   .GroupBy(t => t.Terminal.TerminalNumber)                                   
                                   .Select(g => g.FirstOrDefault())                                   
                                   .Select(t => new TerminalNumberModel()
                                                {
                                                    Id = t.TerminalID,
                                                    Number = t.Terminal.TerminalNumber
                                                })                                                                      
                                   .ToList();                                                         

                return query;
            });
        }

        /// <summary>
        /// Метод, возвращающий название всех типов терминалов для фильтрации
        /// </summary>
        /// <returns></returns>
        public List<TerminalTypeModel> GetTerminalTypes()
        {
            return GrokExceptionManager.ProcessGrokDb(context =>
            {
                var query = context.TerminalType.Select(tp => new TerminalTypeModel()
                    {
                        Id = tp.TerminalTypeID,
                        Name = tp.TerminalTypeName
                    });

                return query.ToList();
            });
        }

        /// <summary>
        /// Метод, возвращающий функцию сортировки
        /// </summary>
        /// <param name="orderBy">Параметры сортировки</param>
        /// <returns>Функция сортировки</returns>
        private Func<IQueryable<SectorStationTerminal>, IOrderedQueryable<SectorStationTerminal>> GetTerminalOrderFunction(string orderBy)
        {
            var sortDescription = SortDescriptionCreateHelper.FactoryMethod(orderBy);

            switch (sortDescription.Field)
            {
                case "Number":
                    return query => TerminalSorter.OrderByNumber(query, sortDescription.IsAscending);
                case "BeginDate":
                    return query => TerminalSorter.OrderByBeginDate(query, sortDescription.IsAscending);
                default:
                    throw new NotSupportedException("Сортировка по параметру \'" + sortDescription.Field + "\' не поддерживается");
            }
        }

        /// <summary>
        /// Метод, возвращающий список функций фильтрации
        /// </summary>
        /// <param name="filterDictionary">Коллекция параметров фильтрации</param>
        /// <returns>Список функций фильтрации</returns>
        private List<Func<IQueryable<SectorStationTerminal>, IQueryable<SectorStationTerminal>>> GetFilterFunctions(NameValueCollection filterDictionary)
        {
            var queries = new List<Func<IQueryable<SectorStationTerminal>, IQueryable<SectorStationTerminal>>>();

            //ToDo LogExc
            var terminalNumbers = filterDictionary["SelectedTerminalNumbers"];
            if (!string.IsNullOrWhiteSpace(terminalNumbers))
            {
                queries.Add(query => TerminalFilter.FilterByTerminalNumber(query, terminalNumbers));
            }

            var terminalTypeStrings = filterDictionary["SelectedTerminalTypes"];
            if (!string.IsNullOrWhiteSpace(terminalTypeStrings))
            {
                queries.Add(query => TerminalFilter.FilterByTerminalType(query, terminalTypeStrings));
            }

            var windowsStrings = filterDictionary["SelectedWindowTellerNumbersInTerminalTab"];

            //По умолчанию показывать только текущие
            var isShowCurrent = true;
            bool tryParseResult;
            if (Boolean.TryParse(filterDictionary["IsShowCurrent"], out tryParseResult))
            {
                isShowCurrent = Boolean.Parse(filterDictionary["IsShowCurrent"]);
            }

            if (isShowCurrent)
            {
                queries.Add(TerminalFilter.FilterOnlyCurrent);
                if (!string.IsNullOrWhiteSpace(windowsStrings))
                {
                    queries.Add(query => TerminalFilter.FilterByCurrentTellerWindows(query, windowsStrings));
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(windowsStrings))
                {
                    queries.Add(query => TerminalFilter.FilterByTellerWindows(query, windowsStrings));
                }

                var beginExploitationPeriod = filterDictionary["TerminalBeginExploitationPeriod"];
                var endExploitationPeriod = filterDictionary["TerminalEndExploitationPeriod"];
                DateTime beginExploitationPeriodDateTime;
                DateTime endExploitationPeriodDateTime;
                if (!DateTime.TryParse(beginExploitationPeriod, out beginExploitationPeriodDateTime))
                {
                    beginExploitationPeriodDateTime = (DateTime)SqlDateTime.MinValue;
                }
                if (!DateTime.TryParse(endExploitationPeriod, out endExploitationPeriodDateTime))
                {
                    endExploitationPeriodDateTime = (DateTime)SqlDateTime.MaxValue;
                }

                queries.Add(query => TerminalFilter.FilterByBeginExploitationPeriod(query, beginExploitationPeriodDateTime, endExploitationPeriodDateTime));
                queries.Add(query => TerminalFilter.FilterByEndExploitationPeriod(query, beginExploitationPeriodDateTime, endExploitationPeriodDateTime)); 
            }
                       

            return queries;
        }
    }
}
