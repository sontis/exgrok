﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    public class PagedList<T> : List<T>
    {
        /// <summary>
        /// Текущая страница
        /// </summary>
        public int Page { get; private set; }

        /// <summary>
        /// Количество элементов на странице
        /// </summary>
        public int PageSize { get; private set; }

        /// <summary>
        /// Общее количество элементов
        /// </summary>
        public int ItemCount { get; private set; }

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int PageCount
        {
            get { return (int)Math.Ceiling((double)ItemCount / PageSize); }
        }

        /// <summary>
        /// Конструктор принимающий набор элементов для постраничного показа
        /// </summary>
        /// <param name="data">Набор элементов</param>
        /// <param name="page">Текущая странциа</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="itemCount">Общее количество элементов</param>
        public PagedList(IEnumerable<T> data, int page, int pageSize, int itemCount)
        {
            Page = page;
            PageSize = pageSize;
            ItemCount = itemCount;

            var items = data.Skip((page - 1) * pageSize).Take(pageSize);
            AddRange(items);
        }

        /// <summary>
        /// Конструктор, формирующий выражение для выборки элементов из базы данных для отображения их на указанной странице
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        public PagedList(IQueryable<T> query, int page, int pageSize)
        {
            Page = page;
            PageSize = pageSize;
            ItemCount = query.Count();

            var items = query.Skip((page - 1) * pageSize).Take(pageSize);
            AddRange(items);
        }
    }
}
