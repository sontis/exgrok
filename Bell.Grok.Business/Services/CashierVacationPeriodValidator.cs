﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Класс для проверки историчности отпусков кассиров
    /// </summary>
    public class CashierVacationPeriodValidator
    {
        private const string TimeFormat = "dd.MM.yyyy";

        public CashierVacationPeriodValidator()
        {
            
        }

        /// <summary>
        /// Метод, проверяющий, пересекается ли новый отпуск с уже существующими
        /// </summary>
        /// <param name="beginDate">Начальная граница нового отпуска</param>
        /// <param name="endDate">Конечная граница нового отпуска</param>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <returns>Статус валидации</returns>
        public string ValidateVacationPeriod(DateTime beginDate, DateTime endDate, int employeeId)
        {
            var vacationRepository = new VacationsRepository();
            var vacation = vacationRepository.IsVacationInPeriod(employeeId, beginDate, endDate);

            if (vacation != null)
            {                
                var endDateInMessage = vacation.EndDate.HasValue
                                               ? " по " + vacation.EndDate.Value.ToString(TimeFormat)
                                               : " на неопределённый срок";
                return "Введённый интервал отпуска пересекаетя с уже существующим: с " + vacation.BeginDate.ToString(TimeFormat) +
                       " по " + endDateInMessage;
            }

            return null;
        }

        /// <summary>
        /// Метод, проверяющий, выходят ли границы нового отпуска с интервалом 
        /// контракта кассира
        /// </summary>
        /// <param name="beginDate">Начальная граница нового отпуска</param>
        /// <param name="endDate">Конечная граница нового отпуска</param>
        /// <param name="cashierContractBeginDate">Начальная граница времени действия договора кассира (из формы, может отличаться от значения в БД)</param>
        /// <param name="cashierContractEndDate">Конечная граница времени действия договора кассира (из формы, может отличаться от значения в БД)</param>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <returns>Статус валидации</returns>
        public string ValidateVacationIntervalWithEmployeeContractInterval(DateTime beginDate, DateTime endDate,
                                                                           DateTime cashierContractBeginDate, DateTime cashierContractEndDate,
                                                                           int employeeId)
        {                        
                if ((beginDate < cashierContractBeginDate) ||
                    (endDate > cashierContractEndDate))
                {
                    var endDateInMessage = cashierContractEndDate != DateTime.MaxValue
                                                ? " по " + cashierContractEndDate.ToString(TimeFormat)
                                                : " на неопределённый срок";
                    var beginDateInMessage = cashierContractBeginDate != DateTime.MaxValue
                                                    ? cashierContractBeginDate.ToString(TimeFormat)
                                                    : " неопределённого срока ";

                    return
                        "Введённый интервал отпуска не должен выходить за границы договора кассира: с " +
                        beginDateInMessage + 
                        endDateInMessage;
                }
               
                return string.Empty;                     
        }
    }
}
