﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    public class EmployeeValidator
    {
        private const string TimeFormat = "dd.MM.yyyy";

        public EmployeeValidator()
        {
            
        }

        /// <summary>
        /// Метод, проверяющий, не выходят ли новые границы конракта сотрудника
        /// за границы интервалов отпусков данного сотрудника
        /// </summary>
        /// <param name="beginDate">Новое значение начала конракта сотрудника</param>
        /// <param name="endDate">Новое значение окончания конракта сотрудника</param>
        /// <param name="employeeId">Идентификатор сотрудника</param>         
        /// <returns>Статус, что новые значения интервала конракта сотрудника
        ///          некорректны, если они выбиваются за границы связанных отпусков сотрудника,
        ///          и null в противном случае</returns>
        public string ValidateVacationIntervals(DateTime beginDate, DateTime endDate, int employeeId)
        {
            var vacationRepository = new VacationsRepository();

            var minVacationBeginDate = vacationRepository.AreVacationsWithLessBeginDate(beginDate, employeeId);

            if (minVacationBeginDate.HasValue)
            {
                return
                    "У сотрудника есть отпуск с датой начала " +
                    minVacationBeginDate.Value.ToString(TimeFormat);
            }

            var maxVacationEndDate = vacationRepository.AreVacationsWithBiggerEndDate(endDate, employeeId);

            if (maxVacationEndDate.HasValue)
            {
                return
                    "У сотрудника есть отпуск с датой окончания " +
                    maxVacationEndDate.Value.ToString(TimeFormat);
            }

            return null;
        } 
    }
}
