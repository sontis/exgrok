﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    public class ScheduleValidator
    {
        /// <summary>
        /// Метод, проверяющий, чтобы каждому кассиру может соответствовать в дискретный период времени (месяц) 
        /// единственный шаблон графика работы кассира билетного
        /// </summary>
        /// <param name="employeeId">Идентификатор кассира</param>        
        /// <param name="year">Год</param>
        /// <param name="month">Месяц</param>
        /// <returns>Статус, что в текущий период времени кассир уже работает по расписанию на основе определённого шаблона,
        ///          и null в противном случае</returns>
        public string CheckTemplateUniquenessForEmployee(int employeeId, int year, int month)
        {
            var scheduleRepository = new ScheduleRepository();            
            var employeeRepository = new EmployeeRepository();

            if (scheduleRepository.IsScheduleTemplateBoundToEmployee(employeeId, year, month))
            {
                var employeeName = employeeRepository.GetFullNameById(employeeId);
                var scheduleTemplateName = employeeRepository.GetScheduleTemplateNameForEmployee(employeeId);
                var stationName = scheduleRepository.GetStationNameByDetailId(employeeId, year, month);

                return "Кассир " + employeeName + " в " + month + "." + year +  " работает по расписанию на основе шаблона \"" +
                       scheduleTemplateName + "\" на станции " + stationName;
            }

            return null;
        }

    }
}
