﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Сервис валидации шаблонов графиков кассиров
    /// </summary>
    public class ScheduleTemplateValidator
    {
        /// <summary>
        /// Метод, проверяющий существование шаблонов графиков с определённым кратким наименованием
        /// (то есть с названием файла-шаблона)
        /// </summary>
        /// <param name="shortName">Краткое наименование шаблона графика</param>
        /// <returns>Статус, что шаблон с таким кратким наименованием уже есть, и null в противном случае</returns>
        public string ValidateShortName(string shortName)
        {
            var scheduleTemplateRepository = new ScheduleTemplateRepository();

            if (scheduleTemplateRepository.ScheduleTemplateWithShortNameExists(shortName))
            {
                return string.Format("Шаблон из файла с именем «{0}» уже загружен", shortName);
            }

            return null;
        }
    }
}
