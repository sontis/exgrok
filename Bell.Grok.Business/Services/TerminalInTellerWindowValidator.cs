﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Сервис валидации данных привязки терминала к кассовому окну по запросу с клиентской стороны
    /// </summary>
    public class TerminalInTellerWindowValidator
    {
        private const string TimeFormat = "dd.MM.yyyy";

        public TerminalInTellerWindowValidator()
        {
            
        }

        /// <summary>
        /// Метод, проверяющий: меньше ли дата начала периода даты окончания периода
        /// </summary>
        /// <param name="beginDate">Дата начала периода</param>
        /// <param name="endDate">Дата окончания периода</param>
        /// <returns>Результат сравнения дат начала и окончания периода</returns>
        public bool IsBeginDateLessEndDate(DateTime beginDate, DateTime endDate)
        {
            return beginDate <= endDate;
        }

        /// <summary>
        /// Метод, проверяющий, не выходит ли интервал привязки терминала к окну 
        /// за границы интервала привязки терминала к станции
        /// </summary>
        /// <param name="beginDate">Начальная граница интервала привязки терминала к окну</param>
        /// <param name="endDate">Конечная граница интервала привязки терминала к окну</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>        
        /// <returns></returns>
        public string ValidateTellerWindowTerminalBindIntervalWithStationBindInterval(DateTime beginDate, DateTime endDate, int sectorStationTerminalId)
        {
            var sectorStationTerminalRepository = new SectorStationTerminalRepository();
            var sectorStationTerminal =
                sectorStationTerminalRepository.GetSectorStationTerminalById(sectorStationTerminalId);                
            
            if (sectorStationTerminal != null)
            {
                if ((beginDate < sectorStationTerminal.BeginDate) ||
                    (endDate > sectorStationTerminal.EndDate))
                {
                    var endDateInMessage = sectorStationTerminal.EndDate.HasValue
                                               ? " по " + sectorStationTerminal.EndDate.Value.ToString(TimeFormat)
                                               : " на неопределённый срок";
                    return
                        "Дата привязки терминала к окну не должна выходить за пределы интервала привязки терминала к станции: с " +
                        sectorStationTerminal.BeginDate.ToString(TimeFormat) + endDateInMessage;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Метод, проверяющий, не выходит ли интервал привязки терминала к окну 
        /// за границы интервала действия окна
        /// </summary>
        /// <param name="beginDate">Начальная граница интервала привязки терминала к окну</param>
        /// <param name="endDate">Конечная граница интервала привязки терминала к окну</param>
        /// <param name="tellerWindowId">Идетификатор кассового окна</param>
        /// <returns></returns>
        public string ValidateTellerWindowTerminalBindIntervalWithTellerWindowPeriod(DateTime beginDate,
            DateTime endDate, int tellerWindowId)
        {
            var tellerWindowRepository = new TellerWindowRepository();
            var tellerWindow = tellerWindowRepository.GetTellerWindowById(tellerWindowId);

            if (tellerWindow != null &&
                tellerWindow.TellerWindowDetailHistory.Any())
            {
                var tellerWindowDetailHistory = tellerWindow.TellerWindowDetailHistory.First();
                if ((beginDate < tellerWindowDetailHistory.BeginDate) ||
                    (endDate > tellerWindowDetailHistory.EndDate))
                {
                    var endDateInMessage = tellerWindowDetailHistory.EndDate.HasValue
                                               ? " по " + tellerWindowDetailHistory.EndDate.Value.ToString(TimeFormat)
                                               : " на неопределённый срок";
                    return
                        "Дата привязки терминала к окну не должна выходить за пределы срока действия окна: с " +
                        tellerWindowDetailHistory.BeginDate.ToString(TimeFormat) + endDateInMessage;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Метод, проверяющий, существует ли у терминала привязка к кассовому окну кроме указанной
        /// в определённый период времени 
        /// </summary>
        /// <param name="beginDate">Дата начала периода привязки терминала к кассовому окну</param>
        /// <param name="endDate">Дата окончания периода привязки терминала к кассовому окну</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <param name="tellerWindowTerminalId">Идентификатор игнорируемой привязки терминала к касссовому окну</param>
        /// <returns>Статус, что в указанный период времени указанный терминал привязан к кассовому окну,
        ///          и null в противном случае</returns>
        public string ValidateDateIntersectionsByTerminalId(DateTime beginDate, DateTime endDate, int sectorStationTerminalId, int tellerWindowTerminalId)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();
            var terminalInTellerWindows =
                tellerWindowTerminalRepository.GetAllTellerWindowTerminalsBySectorStationTerminalId(
                    sectorStationTerminalId);

            foreach (var terminalInTellerWindow in terminalInTellerWindows)
            {                                
                if ((terminalInTellerWindow.BeginDate < endDate &&
                        (terminalInTellerWindow.EndDate.HasValue ?
                            beginDate < terminalInTellerWindow.EndDate.Value :
                            beginDate < DateTime.MaxValue)) &&                    
                    (terminalInTellerWindow.TellerWindowTerminalID != tellerWindowTerminalId))
                {
                    var endDateInMessage = terminalInTellerWindow.EndDate.HasValue
                                               ? terminalInTellerWindow.EndDate.Value.ToString(TimeFormat)
                                               : " неопределённый срок";

                    return string.Format("Введённый интервал пересекается с {0} для терминала {1} в окне {2}",
                                            terminalInTellerWindow.BeginDate.ToString(TimeFormat) + "-" +
                                            endDateInMessage,
                                            terminalInTellerWindow.SectorStationTerminal.Terminal.TerminalNumber,
                                            terminalInTellerWindow.TellerWindow.TellerWindowNumber);
                }
            }

            return null;
        }        

        /// <summary>
        /// Метод, соответствующий констраинту БД UK_TellerWindowTerminal, проверяющий уникальность
        /// набора параметров: кассовое окно, привязка терминала к станции, дата начала привязки
        /// </summary>
        /// <param name="beginDate">Дата начала привязки</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <param name="tellerWindowId">Идентификатор ко</param>
        /// <param name="tellerWindowTerminalId">Идентификатор привязки ккт к ко</param>
        /// <returns></returns>
        public string ValidateTellerWindowTerminalUniqueness(DateTime beginDate, int sectorStationTerminalId,
                                                             int tellerWindowId, int tellerWindowTerminalId)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();

            var tellerWindowTerminal = tellerWindowTerminalRepository.IsSuchTellerWindowTerminal(beginDate,
                                                                                                 sectorStationTerminalId,
                                                                                                 tellerWindowId);
            if (tellerWindowTerminal != null &&
                tellerWindowTerminalId != tellerWindowTerminal.TellerWindowTerminalID)
            {
                return string.Format("У ккт {0} уже есть привязка к окну {1}, начинающаяся {2}",
                                     tellerWindowTerminal.SectorStationTerminal.Terminal.TerminalNumber,
                                     tellerWindowTerminal.TellerWindow.TellerWindowNumber,
                                     tellerWindowTerminal.BeginDate.ToString(TimeFormat));
            }

            return null;
        }

        /// <summary>
        /// Метод, проверяющий, существует ли у кассового окна привязка к терминалу
        /// в указанный период времени
        /// </summary>
        /// <param name="beginDate">Дата начала периода привязки терминала к кассовому окну</param>
        /// <param name="endDate">Дата окончания периода привязки терминала к кассовому окну</param>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>Статус, что в указанный период времени указанное кассовое окно привязано к терминалу,
        ///          и null в противном случае</returns>
        public string ValidateDateIntersectionsByTellerWindowId(DateTime beginDate, DateTime endDate, int tellerWindowId)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();
            var terminalInTellerWindows =
                tellerWindowTerminalRepository.GetAllTellerWindowTerminalsByTellerWindowId(tellerWindowId);

            foreach (var terminalInTellerWindow in terminalInTellerWindows)
            {                
                if (terminalInTellerWindow.BeginDate <= endDate &&
                    (terminalInTellerWindow.EndDate.HasValue ?
                        beginDate <= terminalInTellerWindow.EndDate.Value :
                        beginDate <= DateTime.MaxValue))
                {
                    var endDateInMessage = terminalInTellerWindow.EndDate.HasValue
                                               ? terminalInTellerWindow.EndDate.Value.ToString(TimeFormat)
                                               : " неопределённый срок";

                    return string.Format("Введённый интервал пересекается с {0} для терминала {1} в окне {2}",
                                            terminalInTellerWindow.BeginDate.ToString(TimeFormat) + "-" +
                                            endDateInMessage,
                                            terminalInTellerWindow.SectorStationTerminal.Terminal.TerminalNumber,
                                            terminalInTellerWindow.TellerWindow.TellerWindowNumber);
                }
            }

            return null;            
        }

        /// <summary>
        /// Метод, проверяющий, существует ли у кассового окна привязка к 
        /// терминалу в рамках всех привязок терминалов к станциям кроме указанной
        /// </summary>
        /// <param name="beginDate">Дата начала периода привязки терминала к кассовому окну</param>
        /// <param name="endDate">Дата окончания периода привязки терминала к кассовому окну</param>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Статус, что в указанный период времени указанное кассовое окно привязано к терминалу,
        ///          и null в противном случае</returns>
        public string ValidateDateIntersectionsByTellerWindowId(DateTime beginDate, DateTime endDate, int tellerWindowId, int sectorStationTerminalId)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();
            var terminalInTellerWindows =
                tellerWindowTerminalRepository.GetAllTellerWindowTerminalsByTellerWindowId(tellerWindowId);

            foreach (var terminalInTellerWindow in terminalInTellerWindows)
            {
                if (terminalInTellerWindow.BeginDate <= endDate &&
                    ((terminalInTellerWindow.EndDate.HasValue ?
                        beginDate <= terminalInTellerWindow.EndDate.Value :
                        beginDate <= DateTime.MaxValue)) &&
                     (terminalInTellerWindow.SectorStationTerminalID != sectorStationTerminalId))
                {
                    var endDateInMessage = terminalInTellerWindow.EndDate.HasValue
                                               ? terminalInTellerWindow.EndDate.Value.ToString(TimeFormat)
                                               : " неопределённый срок";

                    return string.Format("Введённый интервал пересекается с {0} для терминала {1} в окне {2}",
                                            terminalInTellerWindow.BeginDate.ToString(TimeFormat) + "-" +
                                            endDateInMessage,
                                            terminalInTellerWindow.SectorStationTerminal.Terminal.TerminalNumber,
                                            terminalInTellerWindow.TellerWindow.TellerWindowNumber);
                }
            }

            return null;
        }        

        /// <summary>
        /// Метод, валидирующий типы ккт и выбранного пользователем ко
        /// </summary>
        /// <param name="tellerWindowTerminalId">Идентификатор привязки ккт к ко - отсюда получаем профиль ккт</param>
        /// <param name="tellerWindowId">Идентификатор нового кассового окна</param>
        /// <returns>Статус валидации</returns>
        public string ValidateProfiles(int tellerWindowTerminalId, int tellerWindowId)
        {
            //При создании привязки ккт к ко данная валидация не проводится
            if (tellerWindowTerminalId == 0)
            {
                return null;
            }

            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();
            var tellerWindowRepository = new TellerWindowRepository();
            
            var terminalProfile =
                tellerWindowTerminalRepository.GetTerminalProfileByTellerWindowTerminalId(tellerWindowTerminalId);
            var tellerWindowProfile = tellerWindowRepository.GetTellerWindowProfile(tellerWindowId);

            //Профили "На вход" и "на выход" должны быть с обеих сторон отношения.
            //Профиль ккт "Транзит" совместим с любым другим
            if ((terminalProfile == (int) TerminalProfiles.Input &&
                 tellerWindowProfile == (int) TellerWindowProfiles.Input) ||
                (terminalProfile == (int) TerminalProfiles.Output &&
                 tellerWindowProfile == (int) TellerWindowProfiles.Output) ||
                (terminalProfile == (int)TerminalProfiles.Transit) ||
                (tellerWindowProfile == 0))
            {
                return null;
            }

            return "В текущий момент профили терминала и кассового окна несовместимы";
        }
    }
}
