﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class CashierSectorValidator
    {
        /// <summary>
        /// Метод, проверяющий, может ли пользователь получать информацию о кассире       
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="cashierId">Идентификатор кассира</param>
        /// <returns>Результат проверки</returns>
        public bool CheckAccessToCashier(string userName, int cashierId)
        {
            var userRepository = new UserRepository();
            var userRoleRepository = new UserRoleRepository();
            var cashierRepository = new CashierRepository();
            var sectorRepository = new SectorRepository();

            var user = userRepository.GetUserByLogin(userName);
            var accessLevel = userRoleRepository.GetUserAccessLevel(userName);
            var cashierSectorId = cashierRepository.GetCashierSectorById(cashierId);

            if (accessLevel.HasValue)
            {
                if (accessLevel.Value == (int) AccessLevels.Company)
                {
                    return true;
                }

                if (accessLevel.Value == (int) AccessLevels.Direction)
                {
                    var directionOfCashierSectorId = sectorRepository.GetDirectionId(cashierSectorId);
                    var userDirectionIds = userRoleRepository.GetUserDirectionId(userName);

                    return userDirectionIds != null && userDirectionIds.Contains(directionOfCashierSectorId);
                }

                if (accessLevel.Value == (int) AccessLevels.Sector)
                {
                    var userSectorId = userRoleRepository.GetUserSectorId(userName);

                    return userSectorId.HasValue && cashierSectorId.HasValue &&
                           userSectorId.Value == cashierSectorId.Value;
                }

                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
