﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Сервис валидации терминала по запросу с клиентской стороны
    /// </summary>
    public class TerminalValidator
    {
        public TerminalValidator()
        {
            
        }

        /// <summary>
        /// Метод, проверяющий: меньше ли дата начала периода даты окончания периода
        /// </summary>
        /// <param name="beginDate">Дата начала периода</param>
        /// <param name="endDate">Дата окончания периода</param>
        /// <returns>Результат сравнения дат начала и окончания периода</returns>
        public bool IsBeginDateLessEndDate(DateTime beginDate, DateTime endDate)
        {
            return beginDate <= endDate;
        }

        /// <summary>
        /// Метод, проверяющий, существует ли у терминала привязка к станции
        /// в указанный период времени
        /// </summary>
        /// <param name="number">Номер терминала</param>
        /// <param name="beginDate">Начало периода</param>    
        /// <param name="endDate">Конец периода</param>    
        /// <param name="timeFormat">Формат отображения времени</param>
        /// <returns>Статус, что в указанный период времени указанный терминал привязан к станции,
        ///          и null в противном случае</returns>
        public string ValidateNumber(string number, DateTime beginDate, DateTime endDate, string timeFormat)
        {
            var terminalRepository = new TerminalRepository();
            var sectorStationTerminal = terminalRepository.IsTerminalBusyInPeriod(number, beginDate, endDate);

            if (sectorStationTerminal != null)
            {
                var sectorStation = sectorStationTerminal.SectorStation;

                if (sectorStation == null)
                {
                    return "Терминал с таким номером используется в данный момент на какой-то станции";
                }
                else
                {
                    var endDateInMessage = sectorStationTerminal.EndDate.HasValue
                                               ? " по " + sectorStationTerminal.EndDate.Value.ToString(timeFormat)
                                               : " на неопределённый срок";
                    return "Терминал с таким номером привязан к станции " + sectorStation.Station.StationName +
                            ", " + sectorStation.Sector.SectorName + " участок, с " + sectorStationTerminal.BeginDate.ToString(timeFormat) + endDateInMessage;
                }                
            }

            return null;
        }

        /// <summary>
        /// Метод, проверяющий, существует ли у терминала привязка к станции
        /// в указанный период времени кроме указанной
        /// </summary>
        /// <param name="number">Номер терминала</param>
        /// <param name="beginDate">Начало периода</param>    
        /// <param name="endDate">Конец периода</param>    
        /// <param name="timeFormat">Формат отображения времени</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Статус, что в указанный период времени указанный терминал привязан к станции, 
        ///          если идентификатор привязки не совпадает с переданным,
        ///          и null в противном случае</returns>
        public string ValidateNumber(string number, DateTime beginDate, DateTime endDate, string timeFormat,
                                     int sectorStationTerminalId)
        {
            var terminalRepository = new TerminalRepository();
            var sectorStationTerminal = terminalRepository.IsTerminalBusyInPeriod(number, beginDate, endDate, sectorStationTerminalId);

            if (sectorStationTerminal != null)
            {
                var sectorStation = sectorStationTerminal.SectorStation;

                if (sectorStation == null)
                {
                    return "Терминал с таким номером используется в данный момент на какой-то станции";
                }
                else
                {
                    var endDateInMessage = sectorStationTerminal.EndDate.HasValue
                                               ? " по " + sectorStationTerminal.EndDate.Value.ToString(timeFormat)
                                               : " на неопределённый срок";
                    return "Терминал с таким номером привязан к станции " + sectorStation.Station.StationName +
                            ", " + sectorStation.Sector.SectorName + " участок, с " + sectorStationTerminal.BeginDate.ToString(timeFormat) + endDateInMessage;
                }
            }

            return null;
        }

        /// <summary>
        /// Метод, проверяющий, не выходят границы интервалов привязок терминала к кассовым окнам, относящимся к данной привязке к станции
        /// за новые границы интервала привязки терминала к станции
        /// </summary>
        /// <param name="beginDate">Новое значение даты ввода терминала на станции</param>
        /// <param name="endDate">Новое значение даты вывода терминала на станции</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <param name="timeFormat">Формат отображения времени</param>        
        /// <returns>Статус, что новые значения интервала привязки терминала к станции
        ///          некорректны, если границы связанных привязок терминала к кассовым окнам выбиваются за них
        ///          и null в противном случае</returns>
        public string ValidateTellerWindowBindIntervals(DateTime beginDate, DateTime endDate, int sectorStationTerminalId, string timeFormat)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();

            var minTellerWindowTerminalBeginDate =
                tellerWindowTerminalRepository.AreTellerWindowTerminalsWithLessBeginDateForTerminal(beginDate,
                                                                                         sectorStationTerminalId);
            if (minTellerWindowTerminalBeginDate.HasValue)
            {
                return
                    "В рамках данной привязки терминала к станции существует привязка терминала к окну с датой установки " +
                    minTellerWindowTerminalBeginDate.Value.ToString(timeFormat);
            }

            var maxTellerWindowTerminalEndDate =
                tellerWindowTerminalRepository.AreTellerWindowTerminalWithBiggerEndDateForTerminal(endDate, 
                                                                                        sectorStationTerminalId);

            if (maxTellerWindowTerminalEndDate.HasValue)
            {
                return 
                    "В рамках данной привязки терминала к станции существует привязка терминала к окну с датой снятия " +
                    maxTellerWindowTerminalEndDate.Value.ToString(timeFormat);
            }

            return null;
        }

        /// <summary>
        /// Метод, проверяющий при закрытии привязки терминала к станции, нет ли в рамках этой привязки к станции
        /// незакрытых привязок к окну и, если они есть, не превышает ли дата установки незакрытой привязки терминала к окну
        /// новую дату вывода терминала со станции
        /// </summary>
        /// <param name="isBindToStationClosed">Выводится ли терминал со станции</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <param name="terminalEndDate">Новая дата закрытия привязки терминала к станции</param>
        /// <param name="timeFormat">Формат отображения времени</param>
        /// <returns>Статус, что новая дата вывода терминала со станции невалидна, и null в противном случае</returns>
        public string ValidateUnfinishedTellerWindowTerminalIntervals(bool isBindToStationClosed, int sectorStationTerminalId, DateTime terminalEndDate, string timeFormat)
        {
            if (isBindToStationClosed)
            {
                var sectorStationTerminalRepository = new SectorStationTerminalRepository();
                var sectorStationTerminal =
                    sectorStationTerminalRepository.GetSectorStationTerminalById(sectorStationTerminalId);

                var unfinishedTellerWindowTerminals =
                    sectorStationTerminal.TellerWindowTerminal.SingleOrDefault(t => !t.EndDate.HasValue);

                if (unfinishedTellerWindowTerminals != null)
                {
                    if (unfinishedTellerWindowTerminals.BeginDate > terminalEndDate)
                    {
                        return
                            "В рамках данной привязки терминала к станции существует привязка терминала к окну с датой установки " +
                            unfinishedTellerWindowTerminals.BeginDate.ToString(timeFormat);
                    }

                    return null;
                }

                return null;
            }

            return null;
        }
    }
}
