﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    public class TimesheetValidator
    {
        /// <summary>
        /// Метод, проверяющий наличие сотрудниковна участке.        
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Истина, если на участке есть сотрудники. 
        ///          И ложь, если нет сотрудников.</returns>
        public bool AreEmployeesOnSector(int sectorId)
        {            
            var employeeRepository = new EmployeeRepository();

            return employeeRepository.AreEmployeesOnSector(sectorId);
        }
    }
}
