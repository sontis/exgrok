﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Сервис валидации данных кассового окна по запросу с клиентской стороны
    /// </summary>
    public class TellerWindowValidator
    {
        public TellerWindowValidator()
        {
            
        }
        
        /// <summary>
        /// Метод, проверяющий, существует ли на станции кассовое окно с указанным номером
        /// </summary>
        /// <param name="number">Номер кассового окна</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Статус, что номер занят, если он уже зарезервирован каким-либо кассовым окном в пределах станции,
        ///          и null в противном случае</returns>
        public string ValidateNumber(int number, int sectorId, int stationId)
        {
            var tellerWindowRepository = new TellerWindowRepository();

            if (tellerWindowRepository.TellerWindowWithNumberExists(number, sectorId, stationId))
            {
                return "Кассовое окно с таким номером на данной станции уже существует";
            }

            return null;
        }

        /// <summary>
        /// Метод, проверяющий, не выходят ли новые границы периода работы кассового окна
        /// за границы интервалов привязок данного окна к терминалам
        /// </summary>
        /// <param name="beginDate">Новое значение даты начала периода эксплуатации окна</param>
        /// <param name="endDate">Новое значение даты окончания периода эксплуатации окна</param>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <param name="timeFormat">Формат отображения времени</param>
        /// <returns>Статус, что новые границы периода работы кассового окна
        ///          некорректны, если они выбиваются за границы интервалов привязок данного окна к терминалам,
        ///          и null в противном случае</returns>
        public string ValidateTerminalBindIntervals(DateTime beginDate, DateTime endDate, int tellerWindowId, string timeFormat)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();

            var minTellerWindowTerminalBeginDate =
                tellerWindowTerminalRepository.AreTellerWindowTerminalsWithLessBeginDateForTellerWindow(beginDate,
                    tellerWindowId);

            if (minTellerWindowTerminalBeginDate.HasValue)
            {
                return 
                    "У кассового окна существует привязка к терминалу с датой установки " +
                    minTellerWindowTerminalBeginDate.Value.ToString(timeFormat);
            }

            var maxTellerWindowTerminalEndDate =
                tellerWindowTerminalRepository.AreTellerWindowTerminalsWithBiggerBeginDateForTellerWindow(endDate,
                    tellerWindowId);

            if (maxTellerWindowTerminalEndDate.HasValue)
            {
                return "У кассового окна существует привязка терминала к окну с датой снятия " +
                       maxTellerWindowTerminalEndDate.Value.ToString(timeFormat);
            }

            return null;
        }

        /// <summary>
        /// Метод, проверяющий при закрытии кассового окна, нет ли у данного окна
        /// незакрытых привязок к терминалу и, если они есть, не превышает ли дата установки незакрытой привязки терминала к окну
        /// новую дату закрытия кассового окна
        /// </summary>
        /// <param name="isWindowClosed">Закрывается ли окно</param>
        /// <param name="tellerWindowlId">Идентификатор кассового окна</param>
        /// <param name="tellerWindowEndDate">Новая дата закрытия кассового окна</param>
        /// <param name="timeFormat">Формат отображения времени</param>
        /// <returns>Статус, что новая дата закрытия кассового окна невалидна, и null в противном случае</returns>
        public string ValidateUnfinishedTellerWindowTerminalIntervals(bool isWindowClosed,
            int tellerWindowlId, DateTime tellerWindowEndDate, string timeFormat)
        {
            if (isWindowClosed)
            {
                var tellerWindowRepository = new TellerWindowRepository();

                var tellerWindow = tellerWindowRepository.GetTellerWindowById(tellerWindowlId);

                var unfinishedTellerWindowTerminals =
                    tellerWindow.TellerWindowTerminal
                                .Where(t => !t.EndDate.HasValue)
                                .ToList();

                if (unfinishedTellerWindowTerminals.Any())
                {
                    var maxUnfinishedBeginDate = unfinishedTellerWindowTerminals.Max(t => t.BeginDate);
                    if (maxUnfinishedBeginDate > tellerWindowEndDate)
                    {
                        return
                            "У данного кассового окна существует привязка к терминалу с датой установки " +
                            maxUnfinishedBeginDate.ToString(timeFormat);
                    }

                    return null;
                }
            }

            return null;
        }
    }
}
