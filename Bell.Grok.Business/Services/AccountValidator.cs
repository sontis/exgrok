﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Сервис, проверяющий, свободен ли логин, по запросу с клиентской стороны
    /// </summary>
    public class AccountValidator
    {        
        public AccountValidator()
        {            
        }

        /// <summary>
        /// Метод, проверяющий, свободен ли логин
        /// </summary>
        /// <param name="login">Логин</param>
        /// <returns>Статус, что логин занят, если он уже зарезервирован каким-либо пользователем, и null в противном случае</returns>
        public string ValidateRegistrationLogin(string login)
        {
            var userRepository = new UserRepository();

            if (userRepository.UserWithLoginExists(login))
            {
                return "Логин занят";
            }

            return null;
        }
    }
}
