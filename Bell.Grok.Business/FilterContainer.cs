﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Класс для работы с данными о фильтрации коллекций (работа с фильтрацией Kendo)
    /// </summary>
    public class FilterContainer
    {
        /// <summary>
        /// Список фильтров
        /// </summary>
        public List<FilterDescription> filters { get; set; }
        public string logic { get; set; }
    }
}