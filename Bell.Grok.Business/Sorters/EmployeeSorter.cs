﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public static class EmployeeSorter
    {
        /// <summary>
        /// Метод, сортирующий сотрудников по табельному номеру
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<Employee> OrderByEmployeeNumber(IQueryable<Employee> query, bool isAscending)
        {
            Expression<Func<Employee, string>> employeeNumberSelector = e => e.EmployeeNumber;

            return isAscending
                       ? query.OrderBy(employeeNumberSelector)
                       : query.OrderByDescending(employeeNumberSelector);
        }

        /// <summary>
        /// Метод, сортирующий сотрудников по имени
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<Employee> OrderByEmployeeFirstName(IQueryable<Employee> query, bool isAscending)
        {
            Expression<Func<Employee, string>> employeeFirstNameSelector = e => e.FirstName;

            return isAscending
                       ? query.OrderBy(employeeFirstNameSelector)
                       : query.OrderByDescending(employeeFirstNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий сотрудников по отчеству
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<Employee> OrderByEmployeeMiddleName(IQueryable<Employee> query, bool isAscending)
        {
            Expression<Func<Employee, string>> employeeMiddleNameSelector = e => e.MiddleName;

            return isAscending
                       ? query.OrderBy(employeeMiddleNameSelector)
                       : query.OrderByDescending(employeeMiddleNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий сотрудников по фамилии
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<Employee> OrderByEmployeeLastName(IQueryable<Employee> query, bool isAscending)
        {
            Expression<Func<Employee, string>> employeeLastNameSelector = e => e.LastName;

            return isAscending
                       ? query.OrderBy(employeeLastNameSelector)
                       : query.OrderByDescending(employeeLastNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий сотрудников по названию сектора, к которому они принадлежат
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<Employee> OrderBySector(IQueryable<Employee> query, bool isAscending)
        {
            Expression<Func<Employee, string>> sectorSelector = e => e.Sector.SectorName;

            return isAscending
                       ? query.OrderBy(sectorSelector)
                       : query.OrderByDescending(sectorSelector);
        }

        /// <summary>
        /// Метод, сортирующий сотрудников по названию должности, которой они обладают
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<Employee> OrderByPosition(IQueryable<Employee> query, bool isAscending)
        {
            Expression<Func<Employee, string>> positionSelector = e => e.Position.PositionName;

            return isAscending
                       ? query.OrderBy(positionSelector)
                       : query.OrderByDescending(positionSelector);
        }        
    }
}
