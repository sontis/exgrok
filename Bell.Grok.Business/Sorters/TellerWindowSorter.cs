﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class TellerWindowSorter
    {
        /// <summary>
        /// Метод, сортирующий кассовые окна по номеру
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<TellerWindow> OrderByNumber(IQueryable<TellerWindow> query, bool isAscending)
        {
            Expression<Func<TellerWindow, int?>> numberSelector = t => t.TellerWindowNumber;

            return isAscending
                       ? query.OrderBy(numberSelector)
                       : query.OrderByDescending(numberSelector);
        }
    }
}
