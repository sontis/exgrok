﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public static class ScheduleDetailSorter
    {
        /// <summary>
        /// Метод, сортирующий плановые графики по ФИО кассира
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleDetail> OrderByEmployeeFullName(IQueryable<ScheduleDetail> query, string dir)
        {
            Expression<Func<ScheduleDetail, string>> employeeFullNameSelector = e => e.Employee.LastName + " " + 
                                                                                     e.Employee.FirstName.Substring(0, 1) + " " + 
                                                                                     e.Employee.MiddleName.Substring(0, 1);

            return dir == "asc"
                       ? query.OrderBy(employeeFullNameSelector)
                       : query.OrderByDescending(employeeFullNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий плановые графики по должности кассира
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleDetail> OrderByPosition(IQueryable<ScheduleDetail> query, string dir)
        {
            Expression<Func<ScheduleDetail, string>> positionSelector = e => e.Employee.Position.PositionName;

            return dir == "asc"
                       ? query.OrderBy(positionSelector)
                       : query.OrderByDescending(positionSelector);
        }   

        /// <summary>
        /// Метод, сортирующий плановые графики по табельному номеру кассира
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleDetail> OrderByEmployeeNumber(IQueryable<ScheduleDetail> query, string dir)
        {
            Expression<Func<ScheduleDetail, string>> employeeNumberSelector = e => e.Employee.EmployeeNumber;

            return dir == "asc"
                       ? query.OrderBy(employeeNumberSelector)
                       : query.OrderByDescending(employeeNumberSelector);
        }


    }
}
