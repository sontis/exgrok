﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Business.Extensions;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public static class TimesheetDetailSorter
    {
        /// <summary>
        /// Метод, сортирующий месячные табели сотрудников по их ФИО
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<TimeSheetDetail> OrderByEmployeeFullName(IQueryable<TimeSheetDetail> query, string dir)
        {
            Expression<Func<TimeSheetDetail, string>> employeeFullNameSelector = t => t.Employee.LastName + " " + t.Employee.FirstName.Substring(0, 1) + " " + t.Employee.MiddleName.Substring(0, 1);

            return dir == "asc"
                       ? query.OrderBy(employeeFullNameSelector)
                       : query.OrderByDescending(employeeFullNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий месячные табели сотрудников по их табельном номеру (сортируется как строка)
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<TimeSheetDetail> OrderByEmployeeNumber(IQueryable<TimeSheetDetail> query, string dir)
        {
            Expression<Func<TimeSheetDetail, string>> employeeNumberSelector = t => t.Employee.EmployeeNumber;

            return dir == "asc"
                       ? query.OrderBy(employeeNumberSelector)
                       : query.OrderByDescending(employeeNumberSelector);
        }


    }
}
