﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Model;

namespace Bell.Grok.Business
{
    public static class ProceedsPlanSorter
    {
        /// <summary>
        /// Метод, сортирующий планы выручки станции по их дате
        /// </summary>
        /// <param name="list">Список планов выручки</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Отсортированный список планов выручки</returns>
        public static IOrderedEnumerable<ProceedsPlanGridModel> OrderByDate(IEnumerable<ProceedsPlanGridModel> list, bool isAscending)
        {
            Func<ProceedsPlanGridModel, DateTime> proceedsPlanDateSelector = p => new DateTime(p.Year, p.Month, 1);

            return isAscending
                       ? list.OrderBy(proceedsPlanDateSelector)
                       : list.OrderByDescending(proceedsPlanDateSelector);
        }

        /// <summary>
        /// Метод, сортирующий планы выручки станции по общему плану выручки
        /// </summary>
        /// <param name="list">Список планов выручки</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Отсортированный список планов выручки</returns>
        public static IOrderedEnumerable<ProceedsPlanGridModel> OrderByCommonPlan(IEnumerable<ProceedsPlanGridModel> list, bool isAscending)
        {
            Func<ProceedsPlanGridModel, Decimal> proceedsPlanCommonPlanSelector = p => p.CommonPlan;

            return isAscending
                      ? list.OrderBy(proceedsPlanCommonPlanSelector)
                      : list.OrderByDescending(proceedsPlanCommonPlanSelector);
        }

        /// <summary>
        /// Метод, сортирующий планы выручки станции по платным планам выручки
        /// </summary>
        /// <param name="list">Список планов выручки</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Отсортированный список планов выручки</returns>
        public static IOrderedEnumerable<ProceedsPlanGridModel> OrderByPaidPlan(IEnumerable<ProceedsPlanGridModel> list, bool isAscending)
        {
            Func<ProceedsPlanGridModel, Decimal> proceedsPlanPaidPlanSelector = p => p.PaidPlan;

            return isAscending
                      ? list.OrderBy(proceedsPlanPaidPlanSelector)
                      : list.OrderByDescending(proceedsPlanPaidPlanSelector);
        }
    }
}
