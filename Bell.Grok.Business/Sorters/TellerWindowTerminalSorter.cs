﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using Bell.Grok.DAL;

namespace Bell.Grok.Business
{    
    public class TellerWindowTerminalSorter
    {
        /// <summary>
        /// Метод, сортирующий привязки терминала к кассовым окнам по дате установки
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<TellerWindowTerminal> OrderByBeginDate(IQueryable<TellerWindowTerminal> query,
                                                                               bool isAscending)
        {
            Expression<Func<TellerWindowTerminal, DateTime>> beginDateSelector = t => t.BeginDate;

            return isAscending
                       ? query.OrderBy(beginDateSelector)
                       : query.OrderByDescending(beginDateSelector);
        }
    }
}
