﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class ScheduleTemplateSorter
    {
        /// <summary>
        /// Метод, сортирующий шаблоны графиков кассиров по краткому наименованию
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleTemplate> OrderByShortName(IQueryable<ScheduleTemplate> query, bool isAscending)
        {
            Expression<Func<ScheduleTemplate, string>> scheduleShortNameSelector = s => s.ShortName;

            return isAscending
                       ? query.OrderBy(scheduleShortNameSelector)
                       : query.OrderByDescending(scheduleShortNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий шаблоны графиков кассиров по полному наименованию
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleTemplate> OrderByFullName(IQueryable<ScheduleTemplate> query,
                                                                          bool isAscending)
        {
            Expression<Func<ScheduleTemplate, string>> scheduleFullNameSelector = s => s.FullName;

            return isAscending
                       ? query.OrderBy(scheduleFullNameSelector)
                       : query.OrderByDescending(scheduleFullNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий шаблоны графиков кассиров по дате начала действия
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleTemplate> OrderByBeginDate(IQueryable<ScheduleTemplate> query,
                                                                          bool isAscending)
        {
            Expression<Func<ScheduleTemplate, DateTime>> scheduleBeginDateSelector = s => s.BeginDate;

            return isAscending
                       ? query.OrderBy(scheduleBeginDateSelector)
                       : query.OrderByDescending(scheduleBeginDateSelector);
        }

        /// <summary>
        /// Метод, сортирующий шаблоны графиков кассиров по дате окончания действия
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleTemplate> OrderByEndDate(IQueryable<ScheduleTemplate> query,
                                                                          bool isAscending)
        {
            Expression<Func<ScheduleTemplate, DateTime>> scheduleEndDateSelector = s => s.EndDate;

            return isAscending
                       ? query.OrderBy(scheduleEndDateSelector)
                       : query.OrderByDescending(scheduleEndDateSelector);
        }

        /// <summary>
        /// Метод, сортирующий шаблоны графиков кассиров по их автору
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleTemplate> OrderByAuthor(IQueryable<ScheduleTemplate> query,
                                                                        bool isAscending)
        {
            Expression<Func<ScheduleTemplate, string>> scheduleAuthorSelector = s => s.Author.LastName;

            return isAscending
                       ? query.OrderBy(scheduleAuthorSelector)
                       : query.OrderByDescending(scheduleAuthorSelector);
        }

        /// <summary>
        /// Метод, сортирующий шаблоны графиков кассиров по автору последнего изменения
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleTemplate> OrderByLastAuthor(IQueryable<ScheduleTemplate> query,
                                                                          bool isAscending)
        {
            Expression<Func<ScheduleTemplate, string>> scheduleLastAuthorSelector = s => s.LastAuthor.LastName;

            return isAscending
                       ? query.OrderBy(scheduleLastAuthorSelector)
                       : query.OrderByDescending(scheduleLastAuthorSelector);
        }

        /// <summary>
        /// Метод, сортирующий шаблоны графиков кассиров по дате их создания
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleTemplate> OrderByCreationDate(IQueryable<ScheduleTemplate> query,
                                                                              bool isAscending)
        {
            Expression<Func<ScheduleTemplate, DateTime>> scheduleCreationDateSelector = s => s.CreationDateTime;

            return isAscending
                       ? query.OrderBy(scheduleCreationDateSelector)
                       : query.OrderByDescending(scheduleCreationDateSelector);
        }

        /// <summary>
        /// Метод, сортирующий шаблоны графиков кассиров по дате последнего изменения
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<ScheduleTemplate> OrderByChangeDateTime(IQueryable<ScheduleTemplate> query,
                                                                          bool isAscending)
        {
            Expression<Func<ScheduleTemplate, DateTime?>> scheduleChangeDateTimeSelector = s => s.ChangeDateTime;

            return isAscending
                       ? query.OrderBy(scheduleChangeDateTimeSelector)
                       : query.OrderByDescending(scheduleChangeDateTimeSelector);
        }
    }
}
