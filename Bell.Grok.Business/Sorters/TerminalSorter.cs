﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class TerminalSorter
    {
        /// <summary>
        /// Метод, сортирующий терминалы по номеру
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<SectorStationTerminal> OrderByNumber(IQueryable<SectorStationTerminal> query, bool isAscending)
        {
            Expression<Func<SectorStationTerminal, string>> numberSelector = s => s.Terminal.TerminalNumber;

            return isAscending
                       ? query.OrderBy(numberSelector)
                       : query.OrderByDescending(numberSelector);
        }

        /// <summary>
        /// Метод, сортирующий терминалы по дате ввода
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="isAscending">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<SectorStationTerminal> OrderByBeginDate(IQueryable<SectorStationTerminal> query, bool isAscending)
        {
            Expression<Func<SectorStationTerminal, DateTime>> beginDateSelector =
                s => s.BeginDate;

            return isAscending
                       ? query.OrderBy(beginDateSelector)
                       : query.OrderByDescending(beginDateSelector);
        }
    }
}
