﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    public class UserSorter
    {
        /// <summary>
        /// Метод, сортирующий пользователей по логину
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<User> OrderByLogin(IQueryable<User> query, string dir)
        {
            Expression<Func<User, string>> loginSelector = e => e.Login;

            return dir == "asc"
                       ? query.OrderBy(loginSelector)
                       : query.OrderByDescending(loginSelector);
        }

        /// <summary>
        /// Метод, сортирующий пользователей по имени
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<User> OrderByFirstName(IQueryable<User> query, string dir)
        {
            Expression<Func<User, string>> firstNameSelector = e => e.FirstName;

            return dir == "asc"
                       ? query.OrderBy(firstNameSelector)
                       : query.OrderByDescending(firstNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий пользователей по отчеству
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<User> OrderByMiddleName(IQueryable<User> query, string dir)
        {
            Expression<Func<User, string>> middleNameSelector = e => e.MiddleName;

            return dir == "asc"
                       ? query.OrderBy(middleNameSelector)
                       : query.OrderByDescending(middleNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий пользователей по фамилии
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<User> OrderByLastName(IQueryable<User> query, string dir)
        {
            Expression<Func<User, string>> lastNameSelector = e => e.LastName;

            return dir == "asc"
                       ? query.OrderBy(lastNameSelector)
                       : query.OrderByDescending(lastNameSelector);
        }

        /// <summary>
        /// Метод, сортирующий пользователей по роли
        /// </summary>
        /// <param name="query">Объект для построения запроса к базе данных</param>
        /// <param name="dir">Порядок сортировки</param>
        /// <returns>Объект для построения запроса к базе данных, учитывающий параметр сортировки</returns>
        public static IOrderedQueryable<User> OrderByRole(IQueryable<User> query, string dir)
        {
            Expression<Func<User, string>> roleSelector = u => u.UserRole.FirstOrDefault().Role.RoleName;

            return dir == "asc"
                       ? query.OrderBy(roleSelector)
                       : query.OrderByDescending(roleSelector);
        }
    }
}
