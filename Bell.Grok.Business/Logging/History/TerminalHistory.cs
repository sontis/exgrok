﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Logging
{
    /// <summary>
    /// Класс для формирования историзации контрольно-кассовых терминалов
    /// </summary>
    public static class TerminalHistoryService
    {
        /// <summary>
        /// Метод, инициализации сервиса, подписывающийся на событие
        /// изменения терминала и его привязок к станциям
        /// </summary>
        public static void Init()
        {
            TerminalRepository.TerminalChanged += SaveChangingInfo;            
        }

        /// <summary>
        /// Обработчик события изменения терминала и его привязок к станциям
        /// </summary>
        /// <param name="sectorStationTerminal">Привязка терминала к станции, 
        ///                                     включает в себя информацию и о терминале</param>
        /// <param name="userName">Логин пользователя, который произвёл изменения</param>
        static void SaveChangingInfo(SectorStationTerminal sectorStationTerminal, string userName)
        {
            var userRepository = new UserRepository();
            var terminalHistoryRepository = new TerminalHistoryRepository();

            var userId = userRepository.GetIdByLogin(userName);
            terminalHistoryRepository.SaveChanging(sectorStationTerminal, userId);
        }        
    }
}
