﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;

namespace Bell.Grok.Business.Logging.Listeners
{
    [ConfigurationElementType(typeof(CustomTraceListenerData))]
    public class TimesheetHandler : CustomTraceListener
    {
        public override void Write(string message)
        {
            throw new NotImplementedException();
        }

        public override void WriteLine(string message)
        {
            throw new NotImplementedException();
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id,
                                       object data)
        {
            if (this.Filter == null || this.Filter.ShouldTrace(eventCache, source, eventType, id, null, null, data, null))
            {
                var timesheetRepository = new TimesheetRepository();

                var dictionary = (Dictionary<string, object>)data.GetType().GetProperty("ExtendedProperties").GetGetMethod().Invoke(data, null);

                var description = new StringBuilder();

                description.Append("Timestamp: ");
                description.Append(eventCache.DateTime.ToString("hh:mm:ss dd:MM:yyyy"));

                int? detailId = null;
                string user = null;
                foreach (var item in dictionary)
                {
                    description.Append(string.Format(", {0}: ", item.Key));
                    description.Append(item.Value);
                    if (item.Key.Equals("TimeSheetDetailID"))
                    {
                        int detailIdOut;
                        if (int.TryParse(item.Value.ToString(), out detailIdOut))
                        {
                            detailId = detailIdOut;
                        }
                    }
                    if (item.Key.Equals("User"))
                    {
                        user = item.Value.ToString();
                    }
                }

                timesheetRepository.SaveTimesheetHistory(detailId, user, description.ToString());

            }
        }
    }
}
