﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Статический класс, облегчающий обход дней месяца
    /// </summary>
    public static class MonthDaysIterator
    {
        /// <summary>
        /// Метод, возвращающий список дней месяца
        /// </summary>
        /// <param name="year">Год</param>
        /// <param name="month">Месяц</param>
        /// <returns>Список дней месяца</returns>
        public static IEnumerable<DateTime> AllDatesInMonth(int year, int month)
        {
            int days = DateTime.DaysInMonth(year, month);
            for (int day = 1; day <= days; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }

        /// <summary>
        /// Метод, возвращающий список дней первой половины месяца
        /// </summary>
        /// <param name="year">Год</param>
        /// <param name="month">Месяц</param>
        /// <returns>Список дней первой половины месяца</returns>
        public static IEnumerable<DateTime> FirstHalfOfMonth(int year, int month)
        {            
            for (int day = 1; day <= 15; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }

        /// <summary>
        /// Метод, возвращающий список дней второй половины месяца
        /// </summary>
        /// <param name="year">Год</param>
        /// <param name="month">Месяц</param>
        /// <returns>Список дней второй половины месяца</returns>
        public static IEnumerable<DateTime> SecondHalfOfMonth(int year, int month)
        {
            int days = DateTime.DaysInMonth(year, month);
            for (int day = 16; day <= days; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }
    }
}
