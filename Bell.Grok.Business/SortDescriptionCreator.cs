﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business
{
    public class SortDescription
    {
        /// <summary>
        /// Поле, по которому производится сортировка
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// Порядок сортировки
        /// </summary>
        public bool IsAscending { get; set; }

        /// <summary>
        /// Порядок сортировки для kendo
        /// </summary>
        public string Dir { get; set; }
    }

    public class SortDescriptionCreateHelper
    {
        /// <summary>
        /// Метод, создающий объект, содержащий параметр сортировки
        /// </summary>
        /// <param name="orderby">Исходная строка, полученная с клиентской стороны</param>
        /// <returns>Объект, содержащий параметр сортировки</returns>
        public static SortDescription FactoryMethod(string orderby)
        {
            var sortDesk = new SortDescription();
            string[] sortParam = orderby.Split(' ');

            sortDesk.Field = sortParam[0];
            sortDesk.IsAscending = sortParam[1].Equals("asc", StringComparison.OrdinalIgnoreCase);

            return sortDesk;
        }
    }
}
