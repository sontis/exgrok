﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class TerminalCreateModelExtensions
    {
        /// <summary>
        /// Метод, инициализирующий объект сущности Terminal и его навигационное свойство TerminalPurposeHistory данными из
        /// объекта модели TerminalCreateModel, а также объектом сущности SectorStation
        /// </summary>
        /// <param name="terminal">Объект сущности Terminal</param>
        /// <param name="terminalPurposeHistory">Объект сущности TerminalPurposeHistory</param>
        /// <param name="sectorStation">Объект сущности SectorStation</param>
        /// <param name="model">Объект модели TerminalCreateModel</param>
        public static void UpdateFrom(this Terminal terminal, TerminalPurposeHistory terminalPurposeHistory, SectorStationTerminal sectorStationTerminal, SectorStation sectorStation, Model.TerminalModel model)
        {
            terminal.TerminalNumber = model.Number;
            terminal.TerminalTypeID = model.Type;
            terminal.TerminalModelID = model.Mark;
            terminal.TerminalProfileID = model.Profile;
            terminal.TerminalStatusID = model.Status;
            terminal.AgentID = model.Agent;
            terminal.TerminalPlacementID = model.PlacementName;
            terminal.PlacementNumericalValue = model.PlacementNumericValue;
            terminal.PlacementComment = model.PlacementComment;
            terminal.Comment = model.Comment;

            terminalPurposeHistory.TerminalPurposeID = model.Purpose;
            terminalPurposeHistory.BeginDate = DateTime.Parse(model.BeginTime);
            if (model.EndTime != null)
            {
                terminalPurposeHistory.DateEnd = DateTime.Parse(model.EndTime);
            }

            sectorStationTerminal.BeginDate = DateTime.Parse(model.BeginTime);            
            if (model.EndTime != null)
            {
                sectorStationTerminal.EndDate = DateTime.Parse(model.EndTime);                
            }
            sectorStationTerminal.SectorStation = sectorStation;

            terminal.SectorStationTerminal.Add(sectorStationTerminal);

            terminal.TerminalPurposeHistory.Add(terminalPurposeHistory);            
        }

        /// <summary>
        /// Метод, копирующий данные из объекта модели TerminalCreateModel в объект сущности Terminal
        /// </summary>
        /// <param name="terminal">Объект сущности Terminal</param>
        /// <param name="model">Объект модели TerminalCreateModel</param>        
        /// <param name="sectorStation">Объект сущности SectorStation</param>
        public static void UpdateFrom(this Terminal terminal, Model.TerminalModel model, SectorStationTerminal sectorStationTerminal, SectorStation sectorStation)
        {
            terminal.TerminalTypeID = model.Type;
            terminal.TerminalModelID = model.Mark;
            terminal.TerminalProfileID = model.Profile;
            terminal.TerminalStatusID = model.Status;
            terminal.AgentID = model.Agent;
            terminal.TerminalPlacementID = model.PlacementName;
            terminal.PlacementNumericalValue = model.PlacementNumericValue;
            terminal.PlacementComment = model.PlacementComment;
            terminal.Comment = model.Comment;

            var terminalPurposeHistory = terminal.TerminalPurposeHistory.FirstOrDefault();

            if (terminalPurposeHistory != null)
            {
                terminalPurposeHistory.TerminalPurposeID = model.Purpose;
                terminalPurposeHistory.BeginDate = DateTime.Parse(model.BeginTime);
                if (model.EndTime != null)
                {
                    terminalPurposeHistory.DateEnd = DateTime.Parse(model.EndTime);
                }
            }

            sectorStationTerminal.BeginDate = DateTime.Parse(model.BeginTime);
            if (model.EndTime != null)
            {
                sectorStationTerminal.EndDate = DateTime.Parse(model.EndTime);
            }
            sectorStationTerminal.SectorStation = sectorStation;

            terminal.SectorStationTerminal.Add(sectorStationTerminal);            
        }
    }
}
