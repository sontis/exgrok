﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class TellerWindowTerminalModelExtensions
    {
        /// <summary>
        /// Метод, копирующий данные из объекта сущности TellerWindowTerminal в объект модели EditTerminalToTellerWindowModel
        /// </summary>
        /// <param name="model">Объект модели EditTerminalToTellerWindowModel</param>
        /// <param name="tellerWindowTerminal">объект сущности TellerWindowTerminal</param>
        /// <param name="timeFormat">Формат времени для преобразования значения dateTime в строку</param>
        public static void Edit(this TerminalToTellerWindowModel model, TellerWindowTerminal tellerWindowTerminal,
                                string timeFormat)
        {
            model.SectorStationTerminalId = tellerWindowTerminal.SectorStationTerminal.SectorStationTerminalID;
            model.TerminalNumber = tellerWindowTerminal.SectorStationTerminal.Terminal.TerminalNumber;
            model.TellerWindowNumber = tellerWindowTerminal.TellerWindow.TellerWindowNumber.ToString();
            model.TellerWindow = tellerWindowTerminal.TellerWindowID;            
            model.BeginDate = tellerWindowTerminal.BeginDate.ToString(timeFormat);

            if (tellerWindowTerminal.EndDate != null)
            {
                model.EndDate = tellerWindowTerminal.EndDate.Value.ToString(timeFormat);
            }
        }
    }
}
