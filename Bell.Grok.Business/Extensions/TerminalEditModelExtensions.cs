﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class TerminalEditModelExtensions
    {
        /// <summary>
        /// Метод, копирующий данные из объекта модели TerminalEditModel в объект сущности SectorStationTerminal и его навигационное свойство Terminal
        /// </summary>
        /// <param name="sectorStationTerminal">Объект сущности SectorStationTermina</param>
        /// <param name="model">Объект модели TerminalEditModel</param>
        public static void UpdateFrom(this SectorStationTerminal sectorStationTerminal, Model.TerminalModel model)
        {
            sectorStationTerminal.Terminal.TerminalTypeID = model.Type;
            sectorStationTerminal.Terminal.TerminalModelID = model.Mark;
            sectorStationTerminal.Terminal.TerminalProfileID = model.Profile;
            sectorStationTerminal.Terminal.TerminalStatusID = model.Status;
            sectorStationTerminal.Terminal.AgentID = model.Agent;
            sectorStationTerminal.Terminal.TerminalPlacementID = model.PlacementName;
            sectorStationTerminal.Terminal.PlacementNumericalValue = model.PlacementNumericValue;
            sectorStationTerminal.Terminal.PlacementComment = model.PlacementComment;
            sectorStationTerminal.Terminal.Comment = model.Comment;

            var terminalPurposeHistory = sectorStationTerminal.Terminal.TerminalPurposeHistory.FirstOrDefault();

            if (terminalPurposeHistory != null)
            {
                terminalPurposeHistory.TerminalPurposeID = model.Purpose;
                terminalPurposeHistory.BeginDate = DateTime.Parse(model.BeginTime);
                if (model.EndTime != null)
                {
                    terminalPurposeHistory.DateEnd = DateTime.Parse(model.EndTime);
                }
                else
                {
                    terminalPurposeHistory.DateEnd = null;
                }
            }

            sectorStationTerminal.BeginDate = DateTime.Parse(model.BeginTime);

            if (model.EndTime != null)
            {
                sectorStationTerminal.EndDate = DateTime.Parse(model.EndTime);
            }
            else
            {
                sectorStationTerminal.EndDate = null;
            }
        }

        /// <summary>
        /// Метод, проверяющий, закрывающий открытую привязку терминала к кассовому окну
        /// в случае закрытия указанной привязки терминала к станции
        /// </summary>
        /// <param name="sectorStationTerminal">Объект сущности SectorStationTerminal</param>        
        public static void CheckUnfinishedTellerWindowTerminalIntervals(this SectorStationTerminal sectorStationTerminal)
        {            
            if (sectorStationTerminal.EndDate.HasValue)
            {
                var unfinishedTellerWindowTerminal =
                    sectorStationTerminal.TellerWindowTerminal.SingleOrDefault(t => !t.EndDate.HasValue);

                if (unfinishedTellerWindowTerminal != null)
                {
                    unfinishedTellerWindowTerminal.EndDate = sectorStationTerminal.EndDate;                    
                }
            }                       
        }
    }
}
