﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class TellerWindowEditExtensions
    {
        /// <summary>
        /// Метод, копирующий данные из объекта сущности TellerWindow в объект модели TellerWindowEditModel
        /// </summary>
        /// <param name="model">Объект модели TellerWindowEditModel</param>
        /// <param name="tellerWindow">Объект сущности TellerWindow</param>
        /// <param name="timeFormat">Формат времени для преобразования значения dateTime в строку</param>
        public static void Edit(this TellerWindowEditModel model, TellerWindow tellerWindow, string timeFormat)
        {
            if (tellerWindow.TellerWindowNumber != null)
            {
                model.Number = tellerWindow.TellerWindowNumber.Value;
            }

            model.Profile = tellerWindow.TellerWindowProfileID;

            if (tellerWindow.PlacementNumericalValue != null)
            {
                model.PlacementNumericValue = tellerWindow.PlacementNumericalValue.Value;
            }

            if (tellerWindow.PlacementComment != null)
            {
                model.PlacementComment = tellerWindow.PlacementComment;
            }

            model.PlacementName = tellerWindow.TellerWindowPlacementID;

            var now = DateTime.Now;
            var nowdate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);            

            var tellerWindowDetailHistory = tellerWindow.TellerWindowDetailHistory.FirstOrDefault();
            if (tellerWindowDetailHistory != null)
            {
                model.Specializations = tellerWindowDetailHistory.TellerWindowSpecializationHistory.Select(t => t.TellerWindowSpecializationID).ToArray();

                if (tellerWindowDetailHistory.TellerWindowStatusID != null)
                {
                    model.Status =
                        tellerWindowDetailHistory.TellerWindowStatusID.Value;
                }

                if (tellerWindowDetailHistory.TellerWindowPaymentID != null)
                {
                    model.PaymentMethod =
                        tellerWindowDetailHistory.TellerWindowPaymentID.Value;
                }

                model.BeginTime = tellerWindowDetailHistory.BeginDate.ToString(timeFormat);

                if (tellerWindowDetailHistory.EndDate != null)
                {
                    model.EndTime =
                        tellerWindowDetailHistory.EndDate.Value.ToString(timeFormat);
                }

                if (tellerWindowDetailHistory.Comment != null)
                {
                    model.Comment = tellerWindowDetailHistory.Comment;
                }
            }

            model.OperationPeriods = new List<TellerWindowOperationPeriodModel>();
            var tellerWindowOperationPeriods = tellerWindow.TellerWindowOperationPeriod;
            foreach (var operationPeriod in tellerWindowOperationPeriods)
            {
                var operationPeriodModel = new TellerWindowOperationPeriodModel
                    {
                        Id = operationPeriod.TellerWindowOperationPeriodID,
                        BeginDate = operationPeriod.BeginDate.ToString("dd.MM.yyyy"),
                        EndDate =
                            operationPeriod.EndDate.HasValue ? operationPeriod.EndDate.Value.ToString("dd.MM.yyyy") : "",
                        ShiftnessId = operationPeriod.TellerWindowShiftnessID.HasValue ? operationPeriod.TellerWindowShiftnessID.ToString() : "",
                        BreakTimes = new List<List<TellerWindowBreakTimeModel>>(),
                        WorkTimes = new List<List<TellerWindowWorkTimeModel>>()
                    };
                var workTimes = operationPeriod.TellerWindowWorkTime;
                for (var day = 1; day < 9; day++)
                {
                    var workTimeModels = new List<TellerWindowWorkTimeModel>();
                    int currentDay = day;
                    foreach (var workTime in workTimes.Where(e => e.WeekDay.WeekDayNumber == currentDay))
                    {
                        workTimeModels.Add(new TellerWindowWorkTimeModel
                            {
                                Id = workTime.TellerWindowWorkTimeID,
                                BeginTime = workTime.BeginTime.ToString("hh':'mm"),
                                EndTime = workTime.EndTime.ToString("hh':'mm").Equals("00:00") ? "24:00" : workTime.EndTime.ToString("hh':'mm"),
                                WeekDayId = workTime.WeekDayID
                            });
                    }
                    operationPeriodModel.WorkTimes.Add(workTimeModels);
                }
                var breakTimes = operationPeriod.TellerWindowBreakTime;
                for (var day = 1; day < 9; day++)
                {
                    var breakTimeModels = new List<TellerWindowBreakTimeModel>();
                    int currentDay = day;
                    foreach (var breakTime in breakTimes.Where(e => e.WeekDay.WeekDayNumber == currentDay))
                    {
                        breakTimeModels.Add(new TellerWindowBreakTimeModel
                            {
                                Id = breakTime.TellerWindowBreakTimeID,
                                BeginTime = breakTime.BeginTime.ToString("hh':'mm"),
                                EndTime = breakTime.EndTime.ToString("hh':'mm").Equals("00:00") ? "24:00" : breakTime.EndTime.ToString("hh':'mm"),
                                WeekDayId = breakTime.WeekDayID,
                                BreakId = breakTime.TellerWindowBreakID
                            });
                    }
                    operationPeriodModel.BreakTimes.Add(breakTimeModels);
                }
                model.OperationPeriods.Add(operationPeriodModel);
            }
        }
    }
}
