﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class StationPassportModelExtensions
    {
        /// <summary>
        /// Метод, копирующий данные из объектов сущностей Station и Sector в объект модели StationPassportModel
        /// </summary>
        /// <param name="stationPassportModel">Объект модели StationPassportModel</param>
        /// <param name="station">Объект сущности Station</param>
        /// <param name="sector">Объект сущности Sector</param>
        public static void Edit(this StationPassportModel stationPassportModel, Station station, Sector sector)
        {
            stationPassportModel.StationNumber = station.StationNumber;
            stationPassportModel.StationName = station.StationName;
            stationPassportModel.DirectionName = sector.Direction.DirectionName;
            stationPassportModel.SectorName = sector.SectorName;
        }
    }
}
