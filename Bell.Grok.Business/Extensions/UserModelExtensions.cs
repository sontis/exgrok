﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class UserModelExtensions
    {
        /// <summary>
        /// Метод, копирующий данные из объекта модели UserModel в объект сущности User
        /// </summary>
        /// <param name="user">Объект сущности User</param>
        /// <param name="model">Объект модели UserModel</param>
        public static void UpdateFrom(this User user, UserModel model) 
        {
            user.FirstName = model.EmployeeFirstName;
            user.MiddleName = model.EmployeeMiddleName;
            user.LastName = model.EmployeeLastName;
            user.Comment = model.Description;
        }        

        /// <summary>
        /// Метод, копирующий данные из объекта сущности User в объект модели UserModel
        /// </summary>
        /// <param name="model">Объект модели UserModel</param>
        /// <param name="user">Объект сущности User</param>
        public static void Edit(this UserModel model, User user)
        {
            model.EmployeeFirstName = user.FirstName;
            model.EmployeeMiddleName = user.MiddleName;
            model.EmployeeLastName = user.LastName;
            model.Description = user.Comment;
            model.AreNameFieldsReadonly = user.EmployeeID.HasValue;
        }
    }
}
