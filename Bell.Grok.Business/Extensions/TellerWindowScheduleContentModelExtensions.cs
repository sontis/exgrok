﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    /// <summary>
    /// Класс с методами расширения для модели TellerWindowScheduleModel
    /// </summary>
    public static class TellerWindowScheduleContentModelExtensions
    {
        /// <summary>
        /// Метод расширения, вычисляющий строку с расписанием работы кассового окна
        /// </summary>
        /// <param name="model">Объект модели TellerWindowScheduleModel</param>
        public static void CalculateWorkPeriodsString(this TellerWindowScheduleContentModel model)
        {
            var result = new StringBuilder();
            var currentBeginHour = -1;
            var isInsideInterval = false;
            
            for (var i = 0; i < model.HourTypes.Count; i++)
            {
                if (model.HourTypes[i].HourTypeId == (int)TellerWindowScheduleHourTypes.Work &&
                    !isInsideInterval)
                {
                    currentBeginHour = i;
                    isInsideInterval = true;
                    continue;
                }

                if (model.HourTypes[i].HourTypeId == (int)TellerWindowScheduleHourTypes.Rest &&
                    isInsideInterval)
                {
                    result.Append(currentBeginHour)
                          .Append("-")
                          .Append(i)                          
                          .Append(Environment.NewLine);

                    isInsideInterval = false;
                }
            }

            if (isInsideInterval)
            {
                result.Append(currentBeginHour)
                      .Append("-")
                      .Append("24");
            }

            model.WorkPeriods = result.ToString();
        }

        /// <summary>
        /// Метод расширения, определяющий возможность наличия перерывов для часов среза работы кассовых окон
        /// </summary>
        /// <param name="model">Объект модели TellerWindowScheduleModel</param>
        public static void CalculateHaveBreakFlags(this TellerWindowScheduleContentModel model)
        {
            var result = new List<bool>();
            result.Add(false);

            for (var i = 1; i < 23; i++)
            {
                result.Add(model.HourTypes[i - 1].HourTypeId != (int) TellerWindowScheduleHourTypes.Rest &&
                           model.HourTypes[i].HourTypeId != (int)TellerWindowScheduleHourTypes.Rest &&
                           model.HourTypes[i + 1].HourTypeId != (int) TellerWindowScheduleHourTypes.Rest);                                                   
            }

            result.Add(false);

            model.CanBreakHave = result;
        }
    }
}
