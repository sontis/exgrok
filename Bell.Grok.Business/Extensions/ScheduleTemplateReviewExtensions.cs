﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class ScheduleTemplateReviewExtensions
    {
        /// <summary>
        /// Метод, анализирующий объект, хранящий данные шаблона графика на конкретный день,
        /// и вносящий эти данные в объект модели ScheduleTemplateReviewDetailModel
        /// </summary>
        /// <param name="model">Объект модели ScheduleTemplateReviewDetailModel</param>
        /// <param name="detail">Объект, хранящий данные шаблона графика на конкретный день</param>
        public static void AddDayDataToReviewDetailModel(this ScheduleTemplateReviewDetailModel model,
                                                         ScheduleTemplateDetail detail)
        {
            //Хитрая бизнес-логика
            //Если день выходной, то выводим букву "В",
            //иначе выводим количество рабочих часов
            model.Days
                      .Add(detail.DayWorkingHours.HasValue &&
                           detail.DayWorkingHours.Value > 0
                                ? detail.DayWorkingHours.ToString()
                                : string.Empty);

            model.Nights
                       .Add(detail.NightWorkingHours.HasValue &&
                            detail.NightWorkingHours.Value > 0
                                ? detail.NightWorkingHours.ToString()
                                : string.Empty);
            model.DayTypes
                       .Add(((Bell.Grok.Model.DayType)detail.DayTypeID).ToChar());

            model.AreSplitted
                       .Add(detail.IsSplitted.SplittedToText());

            //Помечаем праздничные дни
            model.Holidays.Add(detail.DayTypeID == (int)Bell.Grok.Model.DayType.HD);

            //Обновляем значения вычисляемых полей
            if (detail.DayWorkingHours.HasValue &&
                detail.DayWorkingHours.Value > 0)
            {
                model.WorkDaysNum++;
                model.HoursNum += detail.DayWorkingHours.Value;

                if (detail.DayTypeID == (int)Bell.Grok.Model.DayType.HD)
                {
                    model.HolidaysHoursNum += detail.DayWorkingHours.Value;
                }
            }

            if (detail.NightWorkingHours.HasValue &&
                detail.NightWorkingHours.Value > 0)
            {
                model.NightHoursNum += detail.NightWorkingHours.Value;
            }
        }

        /// <summary>
        /// Метод, создающий объект модели ScheduleTemplateReviewDetailModel и вносящий в него информацию
        /// данные первого дня месяца из шаблона графика кассиров
        /// </summary>
        /// <param name="detail">Объект, хранящий данные шаблона графика на первый день месяца</param>
        /// <returns>Объект модели ScheduleTemplateReviewDetailModel</returns>
        public static ScheduleTemplateReviewDetailModel CreateReviewDetailModel(ScheduleTemplateDetail detail)
        {
            return new ScheduleTemplateReviewDetailModel()
                {
                    //Хитрая бизнес-логика
                    MonthNumber = detail.Month,
                    MonthName = ((Months) detail.Month).ToText(),
                    Days = new List<string>()
                        {
                            detail.DayWorkingHours.HasValue &&
                            detail.DayWorkingHours.Value > 0
                                ? detail.DayWorkingHours.ToString()
                                : string.Empty

                        },
                    Nights = new List<string>()
                        {
                            detail.NightWorkingHours.HasValue &&
                            detail.NightWorkingHours.Value > 0
                                ? detail.NightWorkingHours.ToString()
                                : string.Empty
                        },
                    Holidays = new List<bool>()
                        {
                            //Помечаем праздничные дни
                            detail.DayTypeID == (int) Bell.Grok.Model.DayType.HD
                        },
                    DayTypes = new List<string>()
                        {
                            ((Bell.Grok.Model.DayType) detail.DayTypeID).ToChar()
                        },
                    AreSplitted = new List<string>()
                        {
                            detail.IsSplitted.SplittedToText()
                        },
                    //Инициализируем вычисляемые поля
                    WorkDaysNum = detail.DayWorkingHours.HasValue &&
                                  detail.DayWorkingHours.Value > 0
                                      ? 1
                                      : 0,
                    HoursNum = detail.DayWorkingHours.HasValue &&
                               detail.DayWorkingHours.Value > 0
                                   ? detail.DayWorkingHours.Value
                                   : 0,
                    NightHoursNum = detail.NightWorkingHours.HasValue &&
                                    detail.NightWorkingHours.Value > 0
                                        ? detail.NightWorkingHours.Value
                                        : 0,
                    HolidaysHoursNum = detail.DayWorkingHours.HasValue &&
                                       detail.DayWorkingHours.Value > 0 &&
                                       detail.DayTypeID == (int) Bell.Grok.Model.DayType.HD
                                           ? detail.DayWorkingHours.Value
                                           : 0
                };
        }
    }
}
