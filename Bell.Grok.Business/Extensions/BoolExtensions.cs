﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bell.Grok.Business.Extensions
{
    public static class BoolExtensions
    {
        /// <summary>
        /// Метод для перевода в текстовое представление флага разделения дня (шаблоны графиков кассиров)
        /// </summary>
        /// <param name="isSplitted">Значение флага разделения дня</param>
        /// <returns>Текстовое представление флага разделения дня</returns>
        public static string SplittedToText(this bool isSplitted)
        {
            return isSplitted
                       ? "ррд"
                       : string.Empty;
        }
    }
}
