﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business.Extensions
{
    public static class UserExtensions
    {
        /// <summary>
        /// Метод, генерирующий полное имя пользователя
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns>Полное имя пользователя</returns>
        public static string CombineUserName(this User user)
        {
            return (!string.IsNullOrEmpty(user.LastName) ? user.LastName + " " : string.Empty) +
                   (!string.IsNullOrEmpty(user.FirstName) ? user.FirstName.Substring(0, 1) + "." : string.Empty) +
                   (!string.IsNullOrEmpty(user.MiddleName) ? user.MiddleName.Substring(0, 1) + "." : string.Empty);
        }
    }
}
