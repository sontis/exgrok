﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class TerminalEditExtensions
    {
        /// <summary>
        /// Метод, копирующий данные из объекта сущности SectorStationTerminal и его навигационного свойства Terminal в объект модели TerminalEditModel
        /// </summary>
        /// <param name="model">Объект модели TerminalEditModel</param>
        /// <param name="sectorStationTerminal">Объект сущности SectorStationTerminal</param>
        /// <param name="timeFormat">Формат времени для преобразования значения dateTime в строку</param>
        public static void Edit(this Model.TerminalModel model, SectorStationTerminal sectorStationTerminal, string timeFormat)
        {
            model.Number = sectorStationTerminal.Terminal.TerminalNumber; 
           
            model.Type = sectorStationTerminal.Terminal.TerminalTypeID;            

            if (sectorStationTerminal.Terminal.TerminalModelID != null)
            {
                model.Mark = sectorStationTerminal.Terminal.TerminalModelID.Value;
            }
            
            model.Profile = sectorStationTerminal.Terminal.TerminalProfileID;
            model.ProfileName = sectorStationTerminal.Terminal.TerminalProfile.TerminalProfileName;
            
            model.Status = sectorStationTerminal.Terminal.TerminalStatusID;            
            
            model.Agent = sectorStationTerminal.Terminal.AgentID;
                        
            model.PlacementName = sectorStationTerminal.Terminal.TerminalPlacementID;            

            if (sectorStationTerminal.Terminal.PlacementNumericalValue != null)
            {
                model.PlacementNumericValue = sectorStationTerminal.Terminal.PlacementNumericalValue.Value;
            }

            if (sectorStationTerminal.Terminal.PlacementComment != null)
            {
                model.PlacementComment = sectorStationTerminal.Terminal.PlacementComment;
            }

            if (sectorStationTerminal.Terminal.Comment != null)
            {
                model.Comment = sectorStationTerminal.Terminal.Comment;
            }

            var terminalPurposeHistory = sectorStationTerminal.Terminal.TerminalPurposeHistory.FirstOrDefault();
            if (terminalPurposeHistory != null)
            {               
                model.Purpose = terminalPurposeHistory.TerminalPurposeID;                
            }

            model.BeginTime = sectorStationTerminal.BeginDate.ToString(timeFormat);

            if (sectorStationTerminal.EndDate.HasValue)
            {
                model.EndTime = sectorStationTerminal.EndDate.Value.ToString(timeFormat);
            }

            //Определение, существует ли у терминала привязка к кассовому окну в данное время или в будущем
            var now = DateTime.Now;
            var currentDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);            
            model.IsInTellerWindow = sectorStationTerminal.TellerWindowTerminal
                                                          .Any(t => (t.EndDate.HasValue &&
                                                                     t.EndDate.Value >= currentDate) ||
                                                                    !t.EndDate.HasValue);
        }
    }
}
