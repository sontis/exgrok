﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class ScheduleDetailExtensions
    {
        /// <summary>
        /// Метод расширения для списка объектов сущности ScheduleDetail, который
        /// заполняет из этих объектов список объектов модели ScheduleRowModel
        /// </summary>
        /// <param name="schedules">Список объектов сущности ScheduleDetail</param>
        /// <param name="resultList">Список объектов модели ScheduleRowModel</param>
        /// <param name="approveDate">Дата утверждения графика</param>
        /// <param name="timeFormat">Формат даты для конвертации DateTime в строку</param>
        public static void FillScheduleRowModelList(this List<ScheduleDetail> schedules,
                                                    List<ScheduleRowModel> resultList,
                                                    DateTime? approveDate,
                                                    string timeFormat)
        {
             //Счётчик для заполнения порядковых номеров
                var count = 0;                    

                foreach (var scheduleDetail in schedules)
                {
                    var employee = scheduleDetail.Employee;
                    var scheduleDays = scheduleDetail.ScheduleDetailDay.ToList();

                    ScheduleStatusHistory scheduleStatusHistory = null;

                    if (scheduleDetail.Schedule.ScheduleStatusHistory.Any())
                    {
                        var maxBeginDay = scheduleDetail.Schedule.ScheduleStatusHistory.Max(ss => ss.BeginDate);
                        scheduleStatusHistory = scheduleDetail.Schedule.ScheduleStatusHistory
                                                                  .FirstOrDefault(ss => ss.BeginDate == maxBeginDay);
                    }

                    var model = new ScheduleRowModel();
                    model.ScheduleId = scheduleDetail.ScheduleID;
                    model.ScheduleDetailId = scheduleDetail.ScheduleDetailID;
                    model.EmployeeName = employee.CombineFullName();
                    model.EmployeePosition = employee.Position.PositionName;
                    model.EmployeeNumber = employee.EmployeeNumber;
                    model.TellerWindowNumber = scheduleDetail.TellerWindow.TellerWindowNumber;
                    model.TotalHours = scheduleDetail.TotalHours;
                    model.NightHours = scheduleDetail.NightHours;
                    model.HolidayHours = scheduleDetail.HolidayHours;
                    model.N = ++count;
                    model.ApprovalDate = approveDate.HasValue
                                                ? approveDate.Value.ToString(timeFormat)
                                                : string.Empty;
                    model.ApprovalStatus = scheduleStatusHistory != null
                                                ? scheduleStatusHistory.ScheduleStatus.Name
                                                : string.Empty;
                    model.ApprovalStatusId = scheduleStatusHistory != null
                                                    ? scheduleStatusHistory.ScheduleStatusID
                                                    : 0;

                    var modelType = model.GetType();

                    foreach (var day in scheduleDays)
                    {
                        PropertyInfo prop = modelType.GetProperty("Day" + day.Date.Day,
                                                                    BindingFlags.Public | BindingFlags.Instance);
                        if (null != prop && prop.CanWrite &&
                            day.DayTypeID != (int)Bell.Grok.Model.DayType.VD &&
                            day.DayTypeID != (int)Bell.Grok.Model.DayType.VVD)
                        {
                            prop.SetValue(model, day.TotalHours, null);
                        }

                        prop = modelType.GetProperty("NightDay" + day.Date.Day,
                                                                    BindingFlags.Public | BindingFlags.Instance);

                        if (null != prop && prop.CanWrite)
                        {
                            prop.SetValue(model, day.NightHours, null);
                        }

                        prop = modelType.GetProperty("DayTypes" + day.Date.Day,
                                                                    BindingFlags.Public | BindingFlags.Instance);

                        if (null != prop && prop.CanWrite)
                        {
                            prop.SetValue(model, ((Bell.Grok.Model.DayType)day.DayTypeID).ToChar(), null);
                        }

                        prop = modelType.GetProperty("IsSplitted" + day.Date.Day,
                                                                    BindingFlags.Public | BindingFlags.Instance);

                        if (null != prop && prop.CanWrite)
                        {
                            prop.SetValue(model, day.IsSplitted.SplittedToText(), null);
                        }
                    }

                    resultList.Add(model);
                }                        
        }
    }
}
