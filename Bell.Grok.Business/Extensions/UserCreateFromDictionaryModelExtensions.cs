﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class UserCreateFromDictionaryModelExtensions
    {
        /// <summary>
        /// Метод, копирующий данные из объекта модели UserCreateFromDictionaryModel в объект сущности User
        /// </summary>
        /// <param name="user">Объект сущности User</param>
        /// <param name="model">Объект модели UserCreateFromDictionaryModel</param>
        public static void UpdateFrom(this User user, UserCreateFromDictionaryModel model)
        {
            user.EmployeeID = model.EmployeeId;
            user.FirstName = model.EmployeeFirstName;
            user.MiddleName = model.EmployeeMiddleName;
            user.LastName = model.EmployeeLastName;
            user.Comment = model.Description;
        }
    }
}
