﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business.Extensions
{
    public static class EmployeeExtensions
    {
        /// <summary>
        /// Метод, генерирующий полное имя сотрудника с инициалами
        /// </summary>
        /// <param name="employee">Сотрудник</param>
        /// <returns>Полное имя сотрудника с инициалами</returns>
        public static string CombineFullName(this Employee employee)
        {
            return string.Format("{0} {1}.{2}.", employee.LastName, employee.FirstName[0], employee.MiddleName[0]);            
        }
    }
}
