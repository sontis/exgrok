﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;
using Bell.Grok.Model;

namespace Bell.Grok.Business.Extensions
{
    public static class TimesheetDetailExtensions
    {
        /// <summary>
        /// Метод, заполняющий индифидуальный месячный табель данными
        /// из соответствующего планового графика (то есть того же кассира за тот же месяц)
        /// </summary>
        /// <param name="timeSheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="scheduleDetail">Объект индивидуального месячного планового графика кассира</param>
        /// <param name="payMarks">Все коды оплат</param>
        /// <param name="isPrepayment">Половина месяца (первая, то есть аванс, или вторая)</param>
        /// <param name="context">объект соединения с базой данных</param>
        public static void FillFromScheduleDetail(this TimeSheetDetail timeSheetDetail, ScheduleDetail scheduleDetail,
                                                  List<PayMarkModel> payMarks, bool isPrepayment, GrokDb context)
        {
            ResetTimesheetDetailProperties(timeSheetDetail, isPrepayment);

            var timeSheetDetailDays = isPrepayment
                                          ? timeSheetDetail.TimeSheetDetailDay.Where(
                                              t => t.Date.Day < 16)
                                          : timeSheetDetail.TimeSheetDetailDay.Where(
                                              t => t.Date.Day > 15);

            foreach (var timeSheetDetailDay in timeSheetDetailDays)
            {
                var scheduleDetailDay = scheduleDetail.ScheduleDetailDay
                                                      .FirstOrDefault(
                                                          s => s.Date == timeSheetDetailDay.Date);
                
                //В любом случае в плановых графиках нет сверхурочных часов
                timeSheetDetailDay.OvertimeHours = null;

                if (scheduleDetailDay != null)
                {
                    if (scheduleDetailDay.TotalHours.HasValue &&
                        scheduleDetailDay.TotalHours.Value > 0)
                    {
                        timeSheetDetailDay.TotalHours = scheduleDetailDay.TotalHours;
                        timeSheetDetailDay.NightHours = scheduleDetailDay.NightHours;                        
                        if (isPrepayment)
                        {
                            timeSheetDetail.FirstHalfMonthDays++;
                            timeSheetDetail.FirstHalfMonthHours += scheduleDetailDay.TotalHours.Value;
                        }
                        else
                        {
                            timeSheetDetail.SecondHalfMonthDays++;
                            timeSheetDetail.SecondHalfMonthHours += scheduleDetailDay.TotalHours.Value;
                        }
                        timeSheetDetail.TotalMonthDays++;
                        timeSheetDetail.TotalMonthHours += scheduleDetailDay.TotalHours.Value;                        

                        if (scheduleDetailDay.NightHours.HasValue)
                        {
                            timeSheetDetail.TotalMonthNightHours += scheduleDetailDay.NightHours.Value;

                            if (isPrepayment)
                            {
                                timeSheetDetail.FirstHalfMonthNightHours +=
                                    scheduleDetailDay.NightHours.Value;
                            }
                            else
                            {
                                timeSheetDetail.SecondHalfMonthNightHours +=
                                    scheduleDetailDay.NightHours.Value;
                            }
                        }

                        timeSheetDetailDay.TimeSheetMarkID = scheduleDetailDay.DayTypeID ==
                                                             (int) Bell.Grok.Model.DayType.WD
                                                                 ? (int)TimesheetMark.Presence
                                                                 : (int)TimesheetMark.Holidays;                                                                      
                    }
                    else
                    {
                        timeSheetDetailDay.TotalHours = null;
                        timeSheetDetailDay.NightHours = null;
                        timeSheetDetailDay.TimeSheetMarkID = null;

                        if (scheduleDetailDay.DayTypeID != (int) Bell.Grok.Model.DayType.WD)
                        {
                            timeSheetDetailDay.TimeSheetMarkID = (int) TimesheetMark.Weekend;
                        }                        
                    }
                }
            }
               
            CalculatePayrollsAndHookies(timeSheetDetail, payMarks, context);
        }

        /// <summary>
        /// Метод, инициализирующий значения индивидуального месячного табеля сотрудника
        /// </summary>
        /// <param name="timesheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="timesheet">Объект месячного табеля всех кассиров участка</param>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        public static void Init(this TimeSheetDetail timesheetDetail, TimeSheet timesheet, int employeeId)
        {
            timesheetDetail.EmployeeID = employeeId;
            timesheetDetail.FirstHalfMonthDays = 0;
            timesheetDetail.FirstHalfMonthHours = 0;
            timesheetDetail.FirstHalfMonthNightHours = 0;
            timesheetDetail.SecondHalfMonthDays = 0;
            timesheetDetail.SecondHalfMonthHours = 0;
            timesheetDetail.SecondHalfMonthNightHours = 0;
            timesheetDetail.TotalMonthDays = 0;
            timesheetDetail.TotalMonthHours = 0;
            timesheetDetail.TotalMonthNightHours = 0;

            timesheet.TimeSheetDetail.Add(timesheetDetail);

            foreach (var day in MonthDaysIterator.AllDatesInMonth(timesheet.Year, timesheet.Month))
            {
                var timesheetDetailDay = new TimeSheetDetailDay();
                timesheetDetailDay.Date = day;

                timesheetDetail.TimeSheetDetailDay.Add(timesheetDetailDay);
            }
        }

        /// <summary>
        /// Метод, сбрасывающий (обнуляющий) суммарные поля индифидуального месячного табеля
        /// </summary>
        /// <param name="timeSheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="isPrepayment">Половина месяца (первая, то есть аванс, или вторая)</param>
        private static void ResetTimesheetDetailProperties(TimeSheetDetail timeSheetDetail, bool isPrepayment)
        {
            if (isPrepayment)
            {
                timeSheetDetail.TotalMonthDays -= timeSheetDetail.FirstHalfMonthDays;
                timeSheetDetail.TotalMonthHours -= timeSheetDetail.FirstHalfMonthHours;
                timeSheetDetail.TotalMonthNightHours -= timeSheetDetail.FirstHalfMonthNightHours;

                timeSheetDetail.FirstHalfMonthDays = 0;
                timeSheetDetail.FirstHalfMonthHours = 0;
                timeSheetDetail.FirstHalfMonthNightHours = 0;
            }
            else
            {
                timeSheetDetail.TotalMonthDays -= timeSheetDetail.SecondHalfMonthDays;
                timeSheetDetail.TotalMonthHours -= timeSheetDetail.SecondHalfMonthHours;
                timeSheetDetail.TotalMonthNightHours -= timeSheetDetail.SecondHalfMonthNightHours;

                timeSheetDetail.SecondHalfMonthDays = 0;
                timeSheetDetail.SecondHalfMonthHours = 0;
                timeSheetDetail.SecondHalfMonthNightHours = 0;
            }
        }        

        /// <summary>
        /// Метод, рассчитывающий количество часов для кода оплаты "Ночные часы"
        /// </summary>
        /// <param name="timeSheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="payMarks">Все коды оплат</param>
        /// <param name="context">Объект соединения с базой данных</param>
        private static void CalculateNightHoursPayCode(this TimeSheetDetail timeSheetDetail, List<PayMarkModel> payMarks, GrokDb context)
        {
            var hoursCount = timeSheetDetail.TimeSheetDetailDay
                                            .Where(t => t.NightHours.HasValue && 
                                                        t.NightHours > 0 &&
                                                        payMarks.Any(p => p.PayCode == t.TimeSheetMarkID) &&
                                                        payMarks.First(p => p.PayCode == t.TimeSheetMarkID).IsPresence)
                                            .Select(t => t.NightHours)
                                            .Sum().Value;
            var daysCount =
                (byte)timeSheetDetail.TimeSheetDetailDay.Count(t => t.NightHours.HasValue && t.NightHours > 0 &&
                                                        payMarks.Any(p => p.PayCode == t.TimeSheetMarkID) &&
                                                        payMarks.First(p => p.PayCode == t.TimeSheetMarkID).IsPresence);

            if (hoursCount > 0)
            {
                if (timeSheetDetail.TotalByTimeSheetMark.Any(p => p.TimeSheetMarkID == (int) TimesheetMark.NightsHours))
                {
                    var totalByTimeSheetMark = timeSheetDetail.TotalByTimeSheetMark
                                                              .First(
                                                                  p =>
                                                                  p.TimeSheetMarkID == (int) TimesheetMark.NightsHours);
                    totalByTimeSheetMark.TotalHours = hoursCount;
                    totalByTimeSheetMark.TotalDays = daysCount;
                }
                else
                {
                    timeSheetDetail.TotalByTimeSheetMark.Add(new TotalByTimeSheetMark()
                        {
                            TimeSheetMarkID = (int) TimesheetMark.NightsHours,
                            TotalHours = hoursCount,
                            TotalDays = daysCount
                        });
                }
            }
            else
            {
                Func<TotalByTimeSheetMark, bool> condition = p => p.TimeSheetMarkID == (int) TimesheetMark.NightsHours;
                timeSheetDetail.RemoveTotalByTimeSheetMark(condition, context);
            }
        }

        /// <summary>
        /// Метод, рассчитывающий количество часов для кода оплаты "Явка со сверхурочными"
        /// категории оплаты по полуторному тарифу за час
        /// </summary>
        /// <param name="timeSheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="context">Объект соединения с базой данных</param>
        private static void CalculateOneAndhalfOvertimeHours(this TimeSheetDetail timeSheetDetail, GrokDb context)
        {
            var hoursCount = timeSheetDetail.TimeSheetDetailDay
                                                     .Where(t => t.OvertimeHours.HasValue &&
                                                                 t.OvertimeHours.Value > 0)
                                                     .Select(t => t.OvertimeHours > 2
                                                                      ? 2
                                                                      : t.OvertimeHours)
                                                     .Sum().Value;

            var daysCount = (byte)timeSheetDetail.TimeSheetDetailDay
                                                        .Count(t => t.OvertimeHours.HasValue &&
                                                                    t.OvertimeHours.Value > 0);

            if (hoursCount > 0)
            {
                timeSheetDetail.UpdateOvertimeTotalByTimeSheetMarkCategory(TimesheetMarkCategory.OneAndHalfOvertime,
                                                                           hoursCount, daysCount);
            }
            else
            {
                Func<TotalByTimeSheetMark, bool> condition = p => p.TimeSheetMarkID == (int) TimesheetMark.PresenceWithOvertime &&
                                                                  p.TimeSheetMarkCategoryID == (int) TimesheetMarkCategory.OneAndHalfOvertime;
                timeSheetDetail.RemoveTotalByTimeSheetMark(condition, context);
            }
        }

        /// <summary>
        /// Метод, рассчитывающий количество часов для кода оплаты "Явка со сверхурочными"
        /// категории оплаты по двойному тарифу за час
        /// </summary>
        /// <param name="timeSheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="context">Объект соединения с базой данных</param>
        private static void CalculateDoubleOvertimeHours(this TimeSheetDetail timeSheetDetail, GrokDb context)
        {
            var hoursCount = timeSheetDetail.TimeSheetDetailDay
                                                     .Where(t => t.OvertimeHours.HasValue &&
                                                                 t.OvertimeHours.Value > 0)
                                                     .Select(t => t.OvertimeHours > 2
                                                                      ? t.OvertimeHours - 2
                                                                      : 0)
                                                     .Sum().Value;

            var daysCount = (byte)timeSheetDetail.TimeSheetDetailDay
                                                        .Count(t => t.OvertimeHours.HasValue &&
                                                                    t.OvertimeHours.Value > 2);
            if (hoursCount > 0)
            {
                timeSheetDetail.UpdateOvertimeTotalByTimeSheetMarkCategory(TimesheetMarkCategory.DoubleOvertime,
                                                                           hoursCount, daysCount);
            }
            else
            {
                Func<TotalByTimeSheetMark, bool> condition = p => p.TimeSheetMarkID == (int)TimesheetMark.PresenceWithOvertime &&
                                                                  p.TimeSheetMarkCategoryID == (int)TimesheetMarkCategory.DoubleOvertime;
                timeSheetDetail.RemoveTotalByTimeSheetMark(condition, context);
            }
        }        

        /// <summary>
        /// Функция-рабочая лошадка для удаления объектов суммарных значений кодов оплаты "Ночные часы" и "Явка со сверхурочными"
        /// из базы данных
        /// </summary>
        /// <param name="timeSheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="condition">Функция для поиска объета, который необходимо удалить из базы данных</param>
        /// <param name="context">Объект соединения с базой данных</param>
        private static void RemoveTotalByTimeSheetMark(this TimeSheetDetail timeSheetDetail, Func<TotalByTimeSheetMark, bool> condition, GrokDb context)
        {
            var totalByTimeSheetMark = timeSheetDetail.TotalByTimeSheetMark
                                                                  .FirstOrDefault(condition);

            if (totalByTimeSheetMark != null)
            {
                timeSheetDetail.TotalByTimeSheetMark.Remove(totalByTimeSheetMark);
                context.TotalByTimeSheetMark.Remove(totalByTimeSheetMark);
            }
        }

        /// <summary>
        /// Функция-рабочая лошадка для создания/обновления объектов суммарных значений кода оплаты "Явка со сверхурочными"
        /// для разных категорий оплаты
        /// </summary>
        /// <param name="timeSheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="category">Категория оплаты</param>
        /// <param name="hoursCount">Суммарное количество часов для данной категории</param>
        /// <param name="daysCount">Суммарное колчество дней для данной категории</param>
        private static void UpdateOvertimeTotalByTimeSheetMarkCategory(this TimeSheetDetail timeSheetDetail,
                                                                       TimesheetMarkCategory category,
                                                                       float hoursCount, byte daysCount)
        {
            if (timeSheetDetail.TotalByTimeSheetMark.Any(p => p.TimeSheetMarkID == (int)TimesheetMark.PresenceWithOvertime &&
                                                              p.TimeSheetMarkCategoryID == (int)category))
            {
                var totalByTimeSheetMark = timeSheetDetail.TotalByTimeSheetMark
                                                          .First(p => p.TimeSheetMarkID == (int)TimesheetMark.PresenceWithOvertime &&
                                                                      p.TimeSheetMarkCategoryID == (int)category);
                totalByTimeSheetMark.TotalHours = hoursCount;
                totalByTimeSheetMark.TotalDays = daysCount;
            }
            else
            {
                timeSheetDetail.TotalByTimeSheetMark.Add(new TotalByTimeSheetMark()
                {
                    TimeSheetMarkID = (int)TimesheetMark.PresenceWithOvertime,
                    TimeSheetMarkCategoryID = (int)category,
                    TotalHours = hoursCount,
                    TotalDays = daysCount
                });
            }
        }        

        /// <summary>
        /// Метод, рассчитывающий количество часов по всем кодам оплат
        /// </summary>
        /// <param name="timeSheetDetail">Объект индивидуального месячного табеля кассира</param>
        /// <param name="payMarks">Все коды оплат</param>
        /// <param name="context">Объект соединения с базой данных</param>
        public static void CalculatePayrollsAndHookies(this TimeSheetDetail timeSheetDetail, List<PayMarkModel> payMarks, GrokDb context)
        {           
            foreach (var payMark in payMarks)
            {                
                var timeSheetDetailDays = timeSheetDetail.TimeSheetDetailDay
                                                         .Where(ts => ts.TimeSheetMarkID == payMark.PayCode &&
                                                                      payMark.IsAccount &&
                                                                      ts.TimeSheetMarkID != (int)TimesheetMark.PresenceWithOvertime)
                                                         .ToList();

                if (timeSheetDetailDays.Any())
                {
                    var hoursQuantity = timeSheetDetailDays
                                              .Select(ts => ts.TotalHours)
                                              .Sum();

                    var daysCount = timeSheetDetailDays.Count();
                    
                    if (timeSheetDetail.TotalByTimeSheetMark.Any(p => p.TimeSheetMarkID == payMark.PayCode))
                    {
                        var totalByTimeSheetMark = timeSheetDetail.TotalByTimeSheetMark
                                                                  .First(p => p.TimeSheetMarkID == payMark.PayCode);

                        totalByTimeSheetMark.TotalHours = hoursQuantity.Value;
                        totalByTimeSheetMark.TotalDays = (byte)daysCount;
                    }
                    else
                    {
                        timeSheetDetail.TotalByTimeSheetMark.Add(new TotalByTimeSheetMark()
                        {
                            TimeSheetMarkID = payMark.PayCode,
                            TotalHours = hoursQuantity.Value,
                            TotalDays = (byte)daysCount
                        });
                    }
                }
                else
                {
                    if (timeSheetDetail.TotalByTimeSheetMark.Any(p => p.TimeSheetMarkID == payMark.PayCode &&
                                                                      p.TimeSheetDetailID != (int)TimesheetMark.NightsHours &&
                                                                      p.TimeSheetDetailID != (int)TimesheetMark.PresenceWithOvertime))
                    {
                        var totalByTimeSheetMark = timeSheetDetail.TotalByTimeSheetMark
                                                                  .First(p => p.TimeSheetMarkID == payMark.PayCode);

                        timeSheetDetail.TotalByTimeSheetMark.Remove(totalByTimeSheetMark);
                        context.TotalByTimeSheetMark.Remove(totalByTimeSheetMark);
                    }
                }
            }
           
            //Код оплаты "Ночные часы" не проставляется вручную, а считается как сумма ночных часов
            timeSheetDetail.CalculateNightHoursPayCode(payMarks, context);
            //Расчёт сверхурочных по полуторному тарифу
            timeSheetDetail.CalculateOneAndhalfOvertimeHours(context);
            //Расчёт сверхурочных по двойному тарифу
            timeSheetDetail.CalculateDoubleOvertimeHours(context);
        }        

        /// <summary>
        /// Метод, пересчитывающий сумамарные поля сущности TimeSheetDetail
        /// </summary>
        /// <param name="timeSheetDetail">Объект сущности TimeSheetDetail</param>
        /// <param name="payMarks">Все коды оплат - передаются потому, что t.TimeSheetMark может иметь значение null</param>
        public static void CalculateAggregateFileds(this TimeSheetDetail timeSheetDetail, List<PayMarkModel> payMarks)
        {
            timeSheetDetail.FirstHalfMonthDays = (byte)timeSheetDetail.TimeSheetDetailDay
                                                                .Count(
                                                                    t => t.Date.Day < 16 && t.TimeSheetMarkID.HasValue &&
                                                                    payMarks.First(p => p.PayCode == t.TimeSheetMarkID.Value).IsPresence);
            timeSheetDetail.SecondHalfMonthDays = (byte)timeSheetDetail.TimeSheetDetailDay
                                                                .Count(
                                                                    t => t.Date.Day > 15 && t.TimeSheetMarkID.HasValue &&
                                                                    payMarks.First(p => p.PayCode == t.TimeSheetMarkID.Value).IsPresence);
            timeSheetDetail.TotalMonthDays = (byte)(timeSheetDetail.FirstHalfMonthDays + timeSheetDetail.SecondHalfMonthDays);

            timeSheetDetail.FirstHalfMonthHours = timeSheetDetail.TimeSheetDetailDay
                                                                .Where(
                                                                    t => t.Date.Day < 16 && t.TimeSheetMarkID.HasValue &&
                                                                    payMarks.First(p => p.PayCode == t.TimeSheetMarkID.Value).IsPresence)
                                                                .Select(t => t.TotalHours)
                                                                .Sum().Value;
            timeSheetDetail.SecondHalfMonthHours = timeSheetDetail.TimeSheetDetailDay
                                                                .Where(
                                                                    t => t.Date.Day > 15 && t.TimeSheetMarkID.HasValue &&
                                                                    payMarks.First(p => p.PayCode == t.TimeSheetMarkID.Value).IsPresence)
                                                                .Select(t => t.TotalHours)
                                                                .Sum().Value;
            timeSheetDetail.TotalMonthHours = timeSheetDetail.FirstHalfMonthHours + timeSheetDetail.SecondHalfMonthHours;

            timeSheetDetail.FirstHalfMonthNightHours = timeSheetDetail.TimeSheetDetailDay
                                                                .Where(
                                                                    t => t.Date.Day < 16 && t.TimeSheetMarkID.HasValue &&
                                                                    payMarks.First(p => p.PayCode == t.TimeSheetMarkID.Value).IsPresence)
                                                                .Select(t => t.NightHours)
                                                                .Sum().Value;

            timeSheetDetail.SecondHalfMonthNightHours = timeSheetDetail.TimeSheetDetailDay
                                                               .Where(
                                                                   t => t.Date.Day > 15 && t.TimeSheetMarkID.HasValue &&
                                                                    payMarks.First(p => p.PayCode == t.TimeSheetMarkID.Value).IsPresence)
                                                               .Select(t => t.NightHours)
                                                               .Sum().Value;
            timeSheetDetail.TotalMonthNightHours = timeSheetDetail.FirstHalfMonthNightHours + timeSheetDetail.SecondHalfMonthNightHours;
        }
    }
}
