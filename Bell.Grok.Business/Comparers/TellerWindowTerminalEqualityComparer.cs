﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bell.Grok.DAL;

namespace Bell.Grok.Business
{
    /// <summary>
    /// Реализация стандартного интерфейса для кастомного сравнения объектов
    /// </summary>
    internal class TellerWindowTerminalEqualityComparer : IEqualityComparer<TellerWindowTerminal>
    {
        public bool Equals(TellerWindowTerminal twt1, TellerWindowTerminal twt2)
        {
            return twt1.SectorStationTerminal.Terminal.TerminalNumber ==
                   twt2.SectorStationTerminal.Terminal.TerminalNumber;
        }

        public int GetHashCode(TellerWindowTerminal twt)
        {
            return twt.SectorStationTerminal.Terminal.TerminalNumber.GetHashCode();
        }
    }
}
