﻿using System;

namespace Bell.Grok.Services.Exceptions
{
    public class RepositoryException : Exception
    {
        public RepositoryException()
        {

        }
        public RepositoryException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}
