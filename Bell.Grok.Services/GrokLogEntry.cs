﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Bell.Grok.Services.Logging
{
    /// <summary>
    /// Класс формирования сообщений для лог-файла
    /// </summary>
    [XmlRoot("customLogEntry")]
    [Serializable]
    public class GrokLogEntry : LogEntry
    {
        /// <summary>
        /// Конструткор по умолчанию
        /// </summary>
        public GrokLogEntry() { }

        /// <summary>
        /// Конструктор с параметрами
        /// </summary>
        /// <param name="type"></param>
        /// <param name="code"></param>
        /// <param name="parameters">Коллекция параметров действия пользователя</param>
        /// <param name="category"></param>
        public GrokLogEntry(string category, TraceEventType type, int code, IDictionary<string, object> parameters = null)
            : base(string.Empty, category, -1, code, type, string.Empty, parameters)
        {
            this.TimeStamp = DateTime.Now;
        }
    }
}