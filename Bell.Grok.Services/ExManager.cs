﻿using Bell.Grok.Services.Logging;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Bell.Grok.Services.Exceptions
{
    /// <summary>
    /// Менеджер исключений
    /// </summary>
    public static class ExManager
    {
        /// <summary>
        /// Менеджер
        /// </summary>
        public static readonly ExceptionManager Manager;

        /// <summary>
        /// ctor
        /// </summary>
        static ExManager()
        {
            Logger.SetLogWriter(GrokLogger.Logger);

            IConfigurationSource config = ConfigurationSourceFactory.Create();
            ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);

            Manager = factory.CreateManager();
        }
    }

}
