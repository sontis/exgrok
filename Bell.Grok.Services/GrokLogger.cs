﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Bell.Grok.Services.Logging
{
    /// <summary>
    /// Логер. Использует Enterprise Library Logging Bloсk для логирования действий пользователей.
    /// </summary>
    public static class GrokLogger
    {
        internal static readonly LogWriter Logger = new LogWriterFactory().Create();

        static GrokLogger()
        {
        }

        /// <summary>
        /// Логирование каждого обращения к Action любого Controller (код: 100 108)
        /// </summary>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="succesful">Успешная Авторизация</param>
        public static void LogActivity(IDictionary<string, object> parameters, bool succesful = false)
        {


            var entry = new GrokLogEntry("Activity", succesful ? TraceEventType.Information : TraceEventType.Error, succesful ? 100 : 108, parameters);

            entry.ExtendedProperties.Add("Category", "Activity");
            entry.ExtendedProperties.Add("Action", succesful ? "Success" : "Fail");
            entry.ExtendedProperties.Add("Code", entry.EventId);

            Logger.Write(entry);
        }

        /// <summary>
        /// Логирование попытки аутентификации (код: 180 180)
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="message">Сообщение</param>
        /// <param name="succesful">Успех аутентификации</param>
        public static void LogAuthentication(string login, string message, bool succesful = false)
        {
            var entry = new GrokLogEntry("Authentication", succesful ? TraceEventType.Information : TraceEventType.Warning, succesful ? 180 : 188);

            entry.ExtendedProperties.Add("Category", "Authentication");
            entry.ExtendedProperties.Add("Action", succesful ? "Success Log in" : "Fail Log in");
            entry.ExtendedProperties.Add("Code", entry.EventId);
            entry.ExtendedProperties.Add("Login", login);
            entry.ExtendedProperties.Add("Message", message);

            Logger.Write(entry);
        }

        /// <summary>
        /// Логирование выхода из системы (код: 182)
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        public static void LogAuthenticationLogout(string userName)
        {
            var entry = new GrokLogEntry("Authentication",TraceEventType.Information, 182);

            entry.ExtendedProperties.Add("Category", "Authentication");
            entry.ExtendedProperties.Add("Action", "Log out");
            entry.ExtendedProperties.Add("Code", entry.EventId);
            entry.ExtendedProperties.Add("Login", userName);

            Logger.Write(entry);
        }

        /// <summary>
        /// Логирование редактирования шаблона расписания (код: 200)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogScheduleTemplateEdit(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 200, "ScheduleTemplate", "ScheduleTemplate Edit", userName);
        }

        /// <summary>
        /// Логирование создания шаблона расписания (код: 201)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogScheduleTemplateCreate(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 201, "ScheduleTemplate", "ScheduleTemplate Create", userName);
        }

        /// <summary>
        /// Логирование редактирования планового расписания (код: 220)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogScheduleEdit(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 220, "Schedule", "Schedule Edit", userName);
        }

        /// <summary>
        /// Логирование создания индивидуального расписания (код: 224)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogScheduleCreate(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 224, "Schedule", "ScheduleDetail Create", userName);
        }

        /// <summary>
        /// Логирование удаления индивидуального расписания (код: 225)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogScheduleDelete(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 225, "Schedule", "ScheduleDetail Delete", userName);
        }

        /// <summary>
        /// Логирование редактирования табеля(код: 230)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogTimesheetEdit(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 230, "Timesheet", "Timesheet Edit", userName);
        }

        /// <summary>
        /// Логирование создания табеляя (код: 231)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogTimesheetCreate(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 231, "Timesheet", "Timesheet Create", userName);
        }

        /// <summary>
        /// Логирование добавления сотрудника в табель (код: 234)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogTimesheetEmployeeAdd(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 234, "Timesheet", "Timesheet Employee Add", userName);
        }

        /// <summary>
        /// Логирование удаления сотрудника из табеля (код: 235)
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="userName">Имя пользователя</param>
        public static void LogTimesheetEmployeeDelete(string id, IDictionary<string, string> parameters, string userName)
        {
            LogChange(id, parameters, 235, "Timesheet", "Timesheet Employee Delete", userName);
        }

        /// <summary>
        /// Формирование записи лога
        /// </summary>
        /// <param name="id">Идентификатор редактируемой сущности</param>
        /// <param name="parameters">Логируемые значения</param>
        /// <param name="code">Код события</param>
        /// <param name="category">Категория события</param>
        /// <param name="action">Описание действия</param>
        /// <param name="userName">Имя пользователя</param>
        private static void LogChange(string id, IDictionary<string, string> parameters, int code, string category, string action, string userName)
        {
            var innerParameters = new Dictionary<string, object>
                {
                    {"Category", category},
                    {"Action", action},
                    {"Code", code},
                    {"User", userName},
                    {"Id", id},
                };
            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    innerParameters.Add(parameter.Key, parameter.Value);
                }
            }
            var entry = new GrokLogEntry(category, TraceEventType.Information, code, innerParameters);
            Logger.Write(entry);
        }
    }
}