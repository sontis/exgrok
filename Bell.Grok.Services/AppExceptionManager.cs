﻿using System;

namespace Bell.Grok.Services.Exceptions
{
    /// <summary>
    /// Обработчик исключений приложения.
    /// </summary>
    public static class AppExceptionManager
    {
        /// <summary>
        /// Обработчик исключений приложения
        /// </summary>
        /// <param name="exception">Обрабатываемое исключение</param>
        /// <returns>true - рекомендуется перевызвать исключение</returns>
        public static bool HandleException(Exception exception)
        {
            return ExManager.Manager.HandleException(exception, "ApplicationException");
        }

        /// <summary>
        /// Обработчик исключений в соответствии с политикой
        /// </summary>
        /// <param name="exception">Обрабатываемое исключение</param>
        /// <param name="policyName">Название политики</param>
        /// <returns>true - рекомендуется перевызвать исключение</returns>
        public static bool HandleException(Exception exception, string policyName)
        {
            return ExManager.Manager.HandleException(exception, policyName);
        }

    }

}
