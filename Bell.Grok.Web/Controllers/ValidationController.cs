﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Bell.Grok.Business;

namespace Bell.Grok.Web.Controllers
{
    public class ValidationController : Controller
    {
        public ValidationController()
        {
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-validation, проверяющего,
        /// существует ли пользователь указанным логином или нет
        /// </summary>
        /// <param name="login">Проверяемый логин</param>
        /// <returns>JSON с результатом проверки</returns>
        [HttpPost]
        public JsonResult RegistrationLogin(string login)
        {
            var accountValidator = new AccountValidator();

            return Validation(accountValidator.ValidateRegistrationLogin(login));
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-validation, проверяющего,
        /// существует ли кассовое окно на станции с указанным номером
        /// </summary>
        /// <param name="number">Номер кассового окна</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="isNew">Идентификатор нового кассового окна</param>
        /// <returns>JSON с результатом проверки</returns>
        [HttpPost]
        public JsonResult TellerWindowNumber(int number, int sectorId, int stationId, bool isNew)
        {
            var tellerWindowValidator = new TellerWindowValidator();

            return Validation(isNew ? tellerWindowValidator.ValidateNumber(number, sectorId, stationId) : null);
        }

        /// <summary>
        /// Метод, генерирующий JSON на основе статуса проверки существования логина
        /// </summary>
        /// <param name="error">Статус проверки существования логина</param>
        /// <returns>JSON с результатом проверки</returns>
        private JsonResult Validation(string error)
        {
            return (error == null) ? Json(true) : Json(error);
        }
    }
}
