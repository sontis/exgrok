﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class HomeController : Controller
    {
        private List<string> AllCharacters
        {
            get
            {
                return new List<string>
                    {
                        "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", 
                        "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Э", "Ю", "Я"
                    };
            }
        }

        public HomeController()
        {
        }

        /// <summary>
        ///  Метод, возвращающий стандартное представление домашней страницы
        /// </summary>
        /// <param name="scheduleStatuses">Идентификаторы статусов расписаний для фильтрации узлов дерева станций</param>
        /// <returns>Стандартное представление домашней страницы</returns>
        [AuthGrok]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Index(List<string> scheduleStatuses)
        {
            ViewBag.IsCollapseAll = true;
            Session["ScheduleStatuses"] = scheduleStatuses;
            return View();
        }

        /// <summary>
        /// Метод, возвращающий представление домашней страницы после 
        /// смены даты фильтрации кассира
        /// </summary>
        /// <param name="parentNode">Ветка дерева, которая будет открыта 
        /// (открытая ветка дерева сохраняет свою открытость, а вот выделение с листа снимается)</param>
        /// <param name="date">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Cтандартное представление домашней страницы с открытой веткой</returns>
        [AuthGrok]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult IndexAfterDateChange(string parentNode, string date)
        {
            ViewBag.IsCollapseAll = false;

            Session["ParentNode"] = parentNode;
            Session["SelectedNode"] = parentNode;
            Session["CurrentDate"] = date;

            return View("Index");
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-treeview 
        /// </summary>
        /// <returns>JSON со списком направлений</returns>
        /// <param name="scheduleStatuses">Список статусов согласования графиков</param>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]
        [HttpPost]
        [OutputCache(Duration = 3600, VaryByParam = "scheduleStatuses", VaryByCustom = "SessionID;StationVersion", Location = OutputCacheLocation.Server)]
        public ActionResult ReadDirections(string scheduleStatuses)
        {
            var login = User.Identity.Name;

            var directionRepository = new DirectionRepository();
            var directionNodes = directionRepository.GetDirectionNodes(login, String.IsNullOrWhiteSpace(scheduleStatuses) ? null : scheduleStatuses.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(e => int.Parse(e)).ToList());

            var directionViewNodes = AutoMapper.Mapper.Map<IEnumerable<DirectionTreeModel>, IEnumerable<DirectionTreeViewModel>>(directionNodes);

            foreach (var directionViewNode in directionViewNodes)
            {
                directionViewNode.UnifiedTreeId = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Direction"] + directionViewNode.DirectionId;
                directionViewNode.ObjectUrl = Url.Action("Index", "DirectionSector", new { id = directionViewNode.DirectionId, code = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Direction"] });
            }

            return Json(
                new { Items = directionViewNodes });
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-treeview 
        /// </summary>
        /// <param name="directionId">Идентификатор направления</param>
        /// <param name="scheduleStatuses">Список статусов согласования графиков</param>
        /// <returns>JSON со списком участков указанного направления</returns>        
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]
        [HttpPost]
        [OutputCache(VaryByParam = "directionId;scheduleStatuses", Duration = 3600, VaryByCustom = "SessionID;StationVersion", Location = OutputCacheLocation.Server)]
        public ActionResult ReadSectors(int directionId, string scheduleStatuses)
        {
            var sectorRepository = new SectorRepository();
            var userRepository = new UserRepository();
            var userRoleRepository = new UserRoleRepository();

            var login = User.Identity.Name;
            var userId = userRepository.GetIdByLogin(login);
            var userRoles = userRoleRepository.GetAllUserRoles(userId);

            bool isAllSectorsForDirection = false;
            List<int?> sectorIds = new List<int?>();

            foreach (var userRole in userRoles)
            {
                if (userRole.AccessLevelId == 1)
                {
                    isAllSectorsForDirection = true;
                    break;
                }

                if (userRole.AccessLevelId == 2 && userRole.DirectionIds.Contains(directionId))
                {
                    isAllSectorsForDirection = true;
                    break;
                }

                if (userRole.AccessLevelId == 3)
                {
                    sectorIds.Add(userRole.SectorId);
                }
            }

            var sectorNodes = sectorRepository.GetSectorNodes(isAllSectorsForDirection, sectorIds, directionId, String.IsNullOrWhiteSpace(scheduleStatuses) ? null : scheduleStatuses.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(e => int.Parse(e)).ToList());

            foreach (var sectorNode in sectorNodes)
            {
                sectorNode.UnifiedTreeId = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Sector"] + sectorNode.SectorId;
                sectorNode.ObjectUrl = Url.Action("Index", "DirectionSector", new { id = sectorNode.SectorId, code = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Sector"] });
            }

            return Json(
                new { Items = sectorNodes });
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-treeview
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="date">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Элементы структуры сектора ("станции" и "кассиры")</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]
        [HttpPost]
        [OutputCache(VaryByParam = "sectorId;date", Duration = 3600, VaryByCustom = "SessionID", Location = OutputCacheLocation.Server)]
        public ActionResult ReadSectorStructure(int sectorId, string date)
        {
            var cashierRepository = new CashierRepository();
            var stationRepository = new StationRepository();

            var sectorStructure = new List<SectorStructureItemModel>
                {
                    new SectorStructureItemModel
                        {
                            SectorStructureItemId = ((int) SectorStructureTypes.Stations).ToString()
                                                    + " " 
                                                    + sectorId.ToString(),
                            SectorStructureItemName = SectorStructureTypes.Stations.ToText(),
                            HasChildren = stationRepository.IsAnyStationsOnSector(sectorId)                            
                        },
                    new SectorStructureItemModel()
                        {
                            SectorStructureItemId = ((int) SectorStructureTypes.Cashiers).ToString() 
                                                    + " " 
                                                    + sectorId.ToString(),
                            SectorStructureItemName = SectorStructureTypes.Cashiers.ToText(),
                            HasChildren = cashierRepository.IsAnyCashiersOnSector(sectorId, date)                          
                        }
                };

            foreach (var sectorStructureItem in sectorStructure)
            {
                sectorStructureItem.UnifiedTreeId = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + sectorStructureItem.SectorStructureItemId;
            }

            return Json(
                new { Items = sectorStructure });
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-treeview
        /// </summary>
        /// <param name="type">Тип узловых элементов (станции или кассиры)</param>
        /// <param name="sectorId">Идентификатор участка</param>      
        /// <param name="date">Дата для фильтрации кассиров по времени их контракта</param>  
        /// <param name="scheduleStatuses">Список статусов согласования графиков</param>
        /// <returns>JSON со списком станций или первых букв фамилий кассиров для указанного участка</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]
        [HttpPost]
        [OutputCache(VaryByParam = "type;sectorId;date;scheduleStatuses", Duration = 3600, VaryByCustom = "SessionID;CashierVersion;StationVersion", Location = OutputCacheLocation.Server)]
        public ActionResult ReadStationsAndCashiersCharacters(int type, int sectorId, string date, string scheduleStatuses)
        {
            var cashierRepository = new CashierRepository();
            var stationRepository = new StationRepository();

            if (type == (int)SectorStructureTypes.Cashiers)
            {
                var characterWithCashiers = cashierRepository.GetAllFirstCharactersOfSectorCashiersSurnames(sectorId, date)
                                                             .Distinct()
                                                             .Select(ch => new CashierCharachterModel()
                                                                                {
                                                                                    ObjectId = sectorId + " " + ch,
                                                                                    ObjectName = ch,
                                                                                    UnifiedTreeId = UnifiedTreeIdPrefixDictionary.TreeIdCodes["CashierCharacter"] + "_" + ch,
                                                                                    HasChildren = true
                                                                                });


                return Json(new { Items = characterWithCashiers });
            }

            if (type == (int)SectorStructureTypes.Stations)
            {               
                var stations = stationRepository.GetStationsBySector(sectorId, String.IsNullOrWhiteSpace(scheduleStatuses) ? null : scheduleStatuses.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(e => int.Parse(e)).ToList());
                foreach (var station in stations)
                {
                    station.ObjectUrl = Url.Action("Index", "Station", new { SectorId = sectorId, StationId = station.ObjectId, currentDate = date });
                    station.UnifiedTreeId = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + station.ObjectId;
                }

                return Json(new { Items = stations });
            }

            return Json(new { });
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-treeview
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="date">Дата для фильтрации кассиров по времени их контракта</param>
        /// <param name="character">Первая буква фамилии кассиров</param>
        /// <returns>JSON со списком кассиров для указанного участка. фамилии которых начинаются с указанной буквы</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]
        [HttpPost]
        [OutputCache(VaryByParam = "sectorId;date;character", Duration = 3600, VaryByCustom = "SessionID;CashierVersion", Location = OutputCacheLocation.Server)]
        public ActionResult ReadCashiers(int sectorId, string date, string character)
        {
            var cachierRepository = new CashierRepository();

            var cachiers = cachierRepository.GetCashersOnCharacterBySector(sectorId, date, character);
            foreach (var cachier in cachiers)
            {
                cachier.ObjectUrl = Url.Action("Index", "Cashier", new { id = cachier.ObjectId, currentDate = date });
                cachier.UnifiedTreeId = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Cashier"] + cachier.ObjectId;
            }

            return Json(new { Items = cachiers });
        }

        /// <summary>
        /// Метод, вычисляющий идентификаторы узлов ветки, к который принадлежит выделенный листовой элемент
        /// </summary>
        /// <param name="node">Единый идентификатор узла-листового элемента</param>
        /// <param name="parentNode">Единый идентификатор родительского узла листового элемента 
        /// (необходимо для станций, так как станция может принадлежать нескольким участкам)</param>
        /// <returns>JSON со списком единых идентификаторов узлов, составляющих открытую ветку</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult UpdateExpandedNodes(string node, string parentNode)
        {
            var cashierRepository = new CashierRepository();           
            var sectorRepository = new SectorRepository();

            if (node.StartsWith(UnifiedTreeIdPrefixDictionary.TreeIdCodes["Cashier"]))
            {
                int cashierId = int.Parse(node.Substring(1));
                var sectorId = cashierRepository.GetSectorId(cashierId);
                var directionId = sectorRepository.GetDirectionId(sectorId);
                var firstCharacter = cashierRepository.GetFirstCharacterFromLastNameById(cashierId);

                var branch = new List<string>
                    {
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["Direction"] + directionId,
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["Sector"] + sectorId,
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Cashiers + " " + sectorId,
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["CashierCharacter"] + "_" + firstCharacter,
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["Cashier"] + cashierId
                    };

                return Json(new { Branch = branch });

            }

            if (node.StartsWith(UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"]))
            {
                int stationId = int.Parse(node.Substring(1));
                int sectorId = int.Parse(parentNode.Split(' ').ElementAt(1));
                int directionId = sectorRepository.GetDirectionId(sectorId);

                var branch = new List<string>
                    {
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["Direction"] + directionId,
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["Sector"] + sectorId,
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId,
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId
                    };

                return Json(new { Branch = branch });
            }

            if (node.StartsWith(UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"]))
            {
                int sectorId = int.Parse(node.Split(' ').ElementAt(1));
                int directionId = sectorRepository.GetDirectionId(sectorId);

                var branch = new List<string>
                    {
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["Direction"] + directionId,
                        UnifiedTreeIdPrefixDictionary.TreeIdCodes["Sector"] + sectorId,
                        node,
                    };

                return Json(new { Branch = branch });
            }

            if (node.StartsWith(UnifiedTreeIdPrefixDictionary.TreeIdCodes["Sector"]))
            {

                var branch = new List<string>
                    {
                        parentNode,
                        node,
                    };

                return Json(new { Branch = branch });
            }

            if (node.StartsWith(UnifiedTreeIdPrefixDictionary.TreeIdCodes["Direction"]))
            {

                var branch = new List<string>
                    {
                        node,
                    };

                return Json(new { Branch = branch });
            }


            return Json(new { });
        }

    }
}
