﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers;

namespace Bell.Grok.Web.Controllers
{
    /// <summary>
    /// Контроллер для меню администратора и сотрудника ОТиЗП
    /// </summary>
    [AjaxAuthGrok(Roles.Administrator, Roles.OTZPEmployee)]
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class MenuController : Controller
    {
        /// <summary>
        /// Метод, обрабатывающий ajax-звпрос от kendo-menu
        /// </summary>
        /// <returns>JSON с набором пунктов меню администратора и сотрудника ОТиЗП</returns>
        [HttpPost]
        public JsonResult GetActions()
        {
            if (User.IsInRole(Roles.Administrator.ToText()))
            {
                var adminMenuAttributes = GrokMenuProvider.GetItems(Url).Select(d => new
                    {
                        items = string.Empty,
                        text = d.ItemText,
                        url = d.ItemUrl
                    });

                return Json(
                    new
                        {
                            status = JsonStatuses.Ok,
                            items = adminMenuAttributes.ToArray(),
                            text = GrokMenuProvider.GetHeaderName()
                        });
            }

            if (User.IsInRole(Roles.OTZPEmployee.ToText()))
            {
                var adminMenuAttributes = GrokMenuProvider.GetWorkDepartmentItems(Url).Select(d => new
                {
                    items = string.Empty,
                    text = d.ItemText,
                    url = d.ItemUrl
                });

                return Json(
                    new
                    {
                        status = JsonStatuses.Ok,
                        items = adminMenuAttributes.ToArray(),
                        text = GrokMenuProvider.GetWorkDepartmentHeaderName()
                    });
            }

            return Json(new {});
        }

    }
}
