﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Services.Exceptions;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web.Controllers
{
    public class TellerWindowScheduleController : Controller
    {
        #region AJAX

        /// <summary>
        /// Обработчик запроса от kendo-grid на получение расписаний работы кассовых окон станции
        /// </summary>
        /// <param name="sectorStationId">Идентификатор станции</param>
        /// <returns>Список объектов срезов работы кассовых окон</returns>
        [HttpPost]
        public ActionResult ReadTellerWindowSchedules(int sectorStationId)
        {
            var tellerWindowScheduleRepository = new TellerWindowScheduleRepository();            

            try
            {
                var tellerWidnowSchedules = tellerWindowScheduleRepository.GetTellerWindowScheduleModels(sectorStationId);
                var tellerWidnowScheduleRows =
                    tellerWidnowSchedules.Select(
                        AutoMapper.Mapper.Map<TellerWindowScheduleModel, ViewModel.TellerWindowScheduleRowModel>)
                                     .ToList();

                return Json(new { Items = tellerWidnowScheduleRows });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик запроса от kendo-grid на добавление среза работы кассовых окон в хранлище
        /// </summary>
        /// <param name="rowModels">Объект модели строки таблицы срезов работы кассовых окон, обёрнутый в массив</param>
        /// <returns>Объект модели строки таблицы срезов работы кассовых окон с выставленным идентификатором</returns>
        [HttpPost]
        public ActionResult CreateTellerWindowSchedule(List<ViewModel.TellerWindowScheduleRowModel> rowModels)
        {
            var tellerWindowScheduleRepository = new TellerWindowScheduleRepository();
            var models =
                rowModels.Select(AutoMapper.Mapper.Map<ViewModel.TellerWindowScheduleRowModel, TellerWindowScheduleModel>)
                         .ToList();

            try
            {
                var model = models.First(m => m.TellerWindowScheduleModelId == 0);                
                if (tellerWindowScheduleRepository.DoesTellerWindowScheduleModelExist(model))
                {
                    return Json(new { status = JsonStatuses.Invalid, message = "Срез расписаний для данного сезона уже создан" });
                }                

                tellerWindowScheduleRepository.CreateTellerWindowScheduleModel(model);

                var response = new List<ViewModel.TellerWindowScheduleRowModel>();
                response.Add(AutoMapper.Mapper.Map<TellerWindowScheduleModel, ViewModel.TellerWindowScheduleRowModel>(model));

                return Json(response);
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик запроса от kendo-grid на получение срезов работы кассовых окон станции
        /// </summary>
        /// <param name="seasonId">Сезон</param>
        /// <param name="tellerWindowScheduleDayTypeId">Тип дней</param>
        /// <param name="year">Год</param>
        /// <param name="sectorStationId">Идентификатор участка</param>        
        /// <returns>Json со срезом работы кассовых окон станции</returns>
        [HttpPost]
        public ActionResult ReadIndividualTellerWindowSchedules(int seasonId, int tellerWindowScheduleDayTypeId, int year, int sectorStationId)
        {
            var tellerWindowScheduleRepository = new TellerWindowScheduleRepository();

            try
            {
                var tellerWindowSchedules = tellerWindowScheduleRepository.GetTellerWindowScheduleContentModels(seasonId, tellerWindowScheduleDayTypeId, year, sectorStationId);
                var tellerWindowRowSchedules =
                    tellerWindowSchedules.Select(AutoMapper.Mapper
                                                           .Map<TellerWindowScheduleContentModel, TellerWindowScheduleContentRowModel>)
                                         .ToList();
                tellerWindowRowSchedules.Reverse();

                var areRecommendedData = tellerWindowScheduleRepository.AreRecommendedData(seasonId, tellerWindowScheduleDayTypeId, year, sectorStationId);

                return Json(new { Items = tellerWindowRowSchedules, AreRecommendedData = areRecommendedData });
            }             
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик запроса от kendo-grid на обновление данных по срезам расписаний для кассовых окон станции
        /// </summary>
        /// <param name="tellerWindowRowSchedules">Список моделей содержимого расписаний для кассовых окон</param>
        /// <returns>Статус выполнения запроса</returns>
        [HttpPost]
        public ActionResult UpdateIndividualTellerWindowSchedules(List<TellerWindowScheduleContentRowModel> tellerWindowRowSchedules)
        {
            var tellerWindowScheduleRepository = new TellerWindowScheduleRepository();
            var tellerWindowSchedules =
                tellerWindowRowSchedules.Select(
                    AutoMapper.Mapper.Map<TellerWindowScheduleContentRowModel, TellerWindowScheduleContentModel>)
                                        .ToList();

            try
            {
                tellerWindowScheduleRepository.UpdateStationScheduleDetails(tellerWindowSchedules);
                var resultTellerWindowRowSchedules = tellerWindowSchedules.Select(AutoMapper.Mapper
                                                                                            .Map<TellerWindowScheduleContentModel, TellerWindowScheduleContentRowModel>)
                                                                          .ToList();

                return Json(resultTellerWindowRowSchedules);
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        #endregion
    }
}
