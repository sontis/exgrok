﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web.Routing;
using Bell.Grok.Business;
using System;
using System.Linq;
using System.Web.Mvc;
using Bell.Grok.Business.Enums;
using Bell.Grok.ExcelDAL;
using Bell.Grok.Model;
using Bell.Grok.Services.Exceptions;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web.Controllers
{
    public class ScheduleController : Controller
    {
        private const string ExcelMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private const string DefaultScheduleFileName = "Экспорт.xlsx";
        private const string RelativeTemplateFilePath = "ReportTemplates/Schedule.xlsx";
        private const string AppDateRelativePath = "~/App_Data";

        //
        // GET: /Schedule/

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Получение списка всех графиков
        /// </summary>
        /// <returns>Список графиков работы</returns>
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.StationSchedule)]
        public JsonResult GetAllSchedules(int page, int pageSize, List<SortDescription> sort, FilterContainer filter,
                                          int stationId, int month, int year, int sectorId)
        {
            var scheduleRepository = new ScheduleRepository();            
            
            int resultCount;
            
            var result = scheduleRepository.GetAllSchedules(month + 1, //Javascript begin index from 0
                                                 year,
                                                 sectorId,
                                                 stationId,
                                                 page,
                                                 pageSize,
                                                 sort,
                                                 filter,
                                                 out resultCount);            

            return Json(
                new
                {
                    Items = result,
                    Count = resultCount
                });
        }

        public JsonResult GetAllTellerWindowSchedules(int month, int year, int sectorId, int stationId)
        {
            var scheduleRepository = new ScheduleRepository();
            var schedules = scheduleRepository.GetAllTellerWindowSchedules(sectorId,
                                                                           stationId,
                                                                           month + 1, //Javascript begin index from 0
                                                                           year);

            return Json(
                new
                {
                    Schedules = schedules,
                });
        }

        /// <summary>
        /// Получение формы для добавления граффика кассира билетного
        /// </summary>
        /// <returns>Список графиков работы</returns>
        [RoleObjectAuthGrok(UserActions.Add, DataObjects.StationSchedule)]
        public ActionResult AddShedules(int sectorId, int stationId, int? year, int? month)
        {
            var model = new AddSchedulesViewModel();

            model.Year = year.HasValue ? year.Value : DateTime.Now.Year;
            model.Month = month.HasValue ? month.Value : DateTime.Now.Month;
            model.SectorId = sectorId;
            model.StationId = stationId;

            FillAddSchedulesViewModel(model);


            return View(model);
        }

        /// <summary>
        /// Добавление граффика кассира билетного
        /// </summary>
        [HttpPost]
        [RoleObjectAuthGrok(UserActions.Add, DataObjects.StationSchedule)]
        public ActionResult AddShedules(AddSchedulesViewModel model)
        {
            var scheduleRepository = new ScheduleRepository();
            var userRepotitory = new UserRepository();


            if (ModelState.IsValid)
            {
                var userId = userRepotitory.GetIdByLogin(User.Identity.Name);

                scheduleRepository.AddSchedule(model.ScheduleTemlateId, (byte)model.Month, (short)model.Year,
                                               model.SectorId, model.StationId, model.TellerWindow, model.EmployeeId, userId, User.Identity.Name);

                return RedirectToAction("AddSchedulesSuccess",
                                        new RouteValueDictionary(
                                            new
                                            {
                                                controller = "Schedule",
                                                action = "AddSchedulesSuccess",
                                                sectorId = model.SectorId,
                                                stationId = model.StationId,
                                                month = model.Month,
                                                year = model.Year
                                            }));

            }
            else
            {
                FillAddSchedulesViewModel(model);

                return View(model);
            }
        }

        private static void FillAddSchedulesViewModel(AddSchedulesViewModel model)
        {
            var employeeRepository = new EmployeeRepository();
            var tellerWindowRepository = new TellerWindowRepository();
            var scheduleTemplateRepository = new ScheduleTemplateRepository();
            var stationRepository = new StationRepository();

            var month = new SimpleMonthModel()
                {
                    Year = model.Year,
                    Month = model.Month
                };
            model.AllEmployees =
                employeeRepository.GetEmployees(model.SectorId, month)                                  
                                  .Select(e => e.ToSelectListItem())                                  
                                  .ToList();
            
            var currentSectorIds = stationRepository.GetCurrentSectorIds(model.StationId);
            var currentDirectionIds = stationRepository.GetCurrentDirectionIds(model.StationId);
            model.AllScheduleTemlates =
                scheduleTemplateRepository.GetScheduleTemplates(currentDirectionIds, currentSectorIds).Select(e => e.ToSelectListItem()).ToList();                                          

            model.AllTellerWindows =
                tellerWindowRepository.GetAllNumbersForStation(model.SectorId, model.StationId)
                                      .Select(e => e.ToSelectListItem())
                                      .ToList();
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешном добавлении планового граффика
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <returns>Представление, извещающее об успешном изменении планового граффика</returns>
        public ActionResult AddSchedulesSuccess(int sectorId, int stationId, string month, string year)
        {
            ViewBag.SectorId = sectorId;
            ViewBag.StationId = stationId;
            ViewBag.Month = month;
            ViewBag.Year = year;

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;

            return View();
        }

        /// <summary>
        /// Удаляет график работы кассира
        /// </summary>
        /// <param name="scheduleDetailId">Идентификатор графика работы кассира</param>
        /// <returns></returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Delete, DataObjects.StationSchedule)]
        public ActionResult DeleteSchedule(int scheduleDetailId)
        {
            try
            {
                var scheduleRepository = new ScheduleRepository();                
                scheduleRepository.DeleteSchedule(scheduleDetailId, User.Identity.Name);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Метод, изменяющий статус расписания с "Не согласовано" на "На согласовании начальником участка"
        /// </summary>
        /// <param name="scheduleId">Идентификатор расписания</param>
        /// <returns>Json-объект со статусом операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.ApprovalSubmitting, DataObjects.StationSchedule)]
        public ActionResult SendScheduleToReconcile(int scheduleId)
        {
            var scheduleRepository = new ScheduleRepository();
            var userRepository = new UserRepository();

            try
            {
                var templatesActivityStatus = scheduleRepository.CheckScheduleTemplatesActivity(scheduleId);

                if (!string.IsNullOrWhiteSpace(templatesActivityStatus))
                {
                    return Json(new { status = JsonStatuses.Error, message = templatesActivityStatus });
                }

                var currentScheduleStatus = scheduleRepository.GetScheduleStatus(scheduleId);
                if (currentScheduleStatus == ScheduleStatuses.Undefined)
                {
                    throw new Exception("Не найдена информация о текущем статусе графиков данной станции на данный месяц");
                }

                if (currentScheduleStatus == ScheduleStatuses.NotReconcile)
                {
                    var userId = userRepository.GetIdByLogin(User.Identity.Name);
                    scheduleRepository.SendScheduleToReconcile(scheduleId, userId, User.Identity.Name);

                    return Json(new { status = JsonStatuses.Ok });
                }
                else
                {
                    return Json(new { status = JsonStatuses.Error, message = "Изменения статуса графика станции на данный месяц не произошло, так как он не находится в состоянии \"Не согласовано\"" });
                }
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Метод, изменяющий статус расписания с "На согласовании начальником учатстка" на "На согласовании профгруппоргом"
        /// </summary>
        /// <param name="scheduleId">Идентификатор расписания</param>
        /// <returns>Json-объект со статусом операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.ApprovConfirmChief, DataObjects.StationSchedule)]
        public ActionResult ConfirmReconcileScheduleBySectionChief(int scheduleId)
        {
            var scheduleRepository = new ScheduleRepository();
            var userRepository = new UserRepository();

            try
            {
                var currentScheduleStatus = scheduleRepository.GetScheduleStatus(scheduleId);
                if (currentScheduleStatus == ScheduleStatuses.Undefined)
                {
                    throw new Exception("Не найдена информация о текущем статусе графиков данной станции на данный месяц");
                }

                if (currentScheduleStatus == ScheduleStatuses.ReconciledBySectionChief)
                {
                    var userId = userRepository.GetIdByLogin(User.Identity.Name);
                    scheduleRepository.ConfirmReconciledScheduleBySectionChief(scheduleId, userId, User.Identity.Name);

                    return Json(new { status = JsonStatuses.Ok });
                }
                else
                {
                    return Json(new { status = JsonStatuses.Error, message = "Изменения статуса графика станции на данный месяц не произошло, так как он не находится в состоянии \"На согласовании начальником участка\"" });
                }
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Метод, изменяющий статус расписания с "На согласовании профгруппоргом" на "Согласован"
        /// </summary>
        /// <param name="scheduleId">Идентификатор расписания</param>
        /// <returns>Json-объект со статусом операции</returns>
        [RoleObjectAjaxAuthGrok(UserActions.ApprovConfirmTU, DataObjects.StationSchedule)]
        public ActionResult ConfirmReconcileByProfessionalGroupOrginizer(int scheduleId)
        {
            var scheduleRepository = new ScheduleRepository();
            var userRepository = new UserRepository();

            try
            {
                var currentScheduleStatus = scheduleRepository.GetScheduleStatus(scheduleId);
                if (currentScheduleStatus == ScheduleStatuses.Undefined)
                {
                    throw new Exception("Не найдена информация о текущем статусе графиков данной станции на данный месяц");
                }

                if (currentScheduleStatus == ScheduleStatuses.ReconciledByProfessionalGroupOrganizer)
                {
                    var userId = userRepository.GetIdByLogin(User.Identity.Name);
                    scheduleRepository.ConfirmReconcileByProfessionalGroupOrginizer(scheduleId, userId, User.Identity.Name);

                    return Json(new { status = JsonStatuses.Ok });
                }
                else
                {
                    return Json(new { status = JsonStatuses.Error, message = "Изменения статуса графика станции на данный месяц не произошло, так как он не находится в состоянии \"На согласовании профгруппоргом\"" });
                }
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Метод, отменяющий согласование расписания
        /// </summary>
        /// <param name="scheduleId">Идентификатор расписания</param>
        /// <returns>Json-объект со статусом операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.ApprovalCancel, DataObjects.StationSchedule)]
        public ActionResult CancelReconcileSchedule(int scheduleId)
        {
            var scheduleRepository = new ScheduleRepository();
            var userRepository = new UserRepository();

            try
            {
                var currentScheduleStatus = scheduleRepository.GetScheduleStatus(scheduleId);

                if (currentScheduleStatus != ScheduleStatuses.NotReconcile)
                {
                    var userId = userRepository.GetIdByLogin(User.Identity.Name);

                    scheduleRepository.CancelReconcileSchedule(scheduleId, userId, User.Identity.Name);

                    return Json(new { status = JsonStatuses.Ok });
                }
                else
                {
                    return Json(new { status = JsonStatuses.Error, message = "Изменения статуса графика станции на данный месяц не произошло, так как он находится в состоянии \"Не согласовано\"" });
                }
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        #region FILERESULT
        /// <summary>
        /// Метод, производящий выгрузку графиков кассиров станции за определённый месяц в файл формата excel 
        /// </summary>
        /// <param name="id">Идентификатор расписания</param>
        /// <returns>Excel-файл с графиками кассиров станции за определённый месяц</returns>
        [RoleObjectAuthGrok(UserActions.Export, DataObjects.StationSchedule)]
        public ActionResult Export(int? id)
        {
            var scheduleRepository = new ScheduleRepository();
            var userRepository = new UserRepository();

            var userId = userRepository.GetIdByLogin(User.Identity.Name);
            var model = scheduleRepository.GetExportData(id ?? 0, userId);

            if (model == null)
            {
                return new HttpStatusCodeResult(404, "Schedule information wasnot found in the database");
            }
            else
            {
                var fileName = HttpContext.Server.MapPath(Path.Combine(AppDateRelativePath, RelativeTemplateFilePath));
                var editor = new ExcelDocumentEditor();
                var resultStream = editor.EditDocumentFromModel(fileName, model);

                return File(resultStream, ExcelMimeType, DefaultScheduleFileName);
            }
        }

        #endregion
    }
}
