﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Web.ViewModel;
using Bell.Grok.Web.Helpers;

namespace Bell.Grok.Web.Controllers
{
    /// <summary>
    /// Контроллер разделяемых элементов
    /// </summary>
    public class SharedController : Controller
    {
        /// <summary>
        /// Метод, возвращающий частичное представление, отображающее состояние авторизованности пользователя (залогинен или нет)
        /// </summary>
        /// <returns>Частичное представление, отображающее состояние авторизованности пользователя</returns>
        [ChildActionOnly]
        public ActionResult LoginStatus()
        {
            return PartialView();
        }

        /// <summary>
        /// Метод, возвращающий частичное представление, отображающее дерево навигации
        /// </summary>
        /// <returns>Частичное представление, отображающее дерево навигации</returns>
        [ChildActionOnly]
        public ActionResult NavigationTree()
        {
            var scheduleRepository = new ScheduleRepository();
            var statuses = scheduleRepository.GetAllScheduleStatus();
            var model = new NavigationTreeModel();
            model.ScheduleStatuses = statuses.Select(a => a.ToSelectListItem()).ToList();
            model.ScheduleStatus = Session["ScheduleStatuses"] as List<string>;
            model.FilterEnabled = User.IsInRole(Roles.ProfessionalGroupOrganizer.ToText()) || User.IsInRole(Roles.SectorHead.ToText()) || User.IsInRole(Roles.OTZPEmployee.ToText());

            foreach (var status in model.ScheduleStatuses)
            {
                if (model.ScheduleStatus != null && model.ScheduleStatus.Any(e => e.Equals(status.Value))) status.Selected = true;
            }
            return PartialView(model);
        }

    }
}
