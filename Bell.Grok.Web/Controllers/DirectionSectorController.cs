﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using Bell.Grok.Business;
using Bell.Grok.ExcelDAL;
using Bell.Grok.Model;
using Bell.Grok.Services.Exceptions;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.Services;

namespace Bell.Grok.Web.Controllers
{
    public class DirectionSectorController : Controller
    {
        private const string JsonFormat = "text/plain";
        private const string ExcelMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private const string DefaultScheduleFileName = "Экспорт.xlsx";
        private const string RelativeTemplateFilePath = "ReportTemplates/Timesheet.xlsx";
        private const string AppDateRelativePath = "~/App_Data";

        /// <summary>
        /// Метод, возвращающий представление, для редактирование нормативной нагрузки станций  и  табелей учета времени
        /// </summary>
        /// <param name="id">Идентификатор объекта</param>
        /// <param name="code">Код объекта</param>
        /// <param name="filterBpa">Фильтр по БПА(-1 если невыбрано)</param>
        /// <param name="filterMktk">Фильтр по МКТК(-1 если невыбрано)</param>
        /// <returns>Представление, для редактирования нормативной нагрузки станций и табелей учета времени</returns>
        public ActionResult Index(int id, string code, int? filterBpa, int? filterMktk)
        {
            var model = new DirectionSectorViewModel();
            model.Id = id;
            model.Code = code;
            model.FilterBPAStandart = filterBpa;
            model.FilterMKTKStandart = filterMktk;

            if (string.IsNullOrWhiteSpace(code))
            {
                model.SectorName = "";
                model.DirectionName = "";
                Session["ParentNode"] = "";
                Session["SelectedNode"] = "";
            }
            else if (UnifiedTreeIdPrefixDictionary.TreeIdCodes.First(e => e.Value.Equals(code)).Key.Equals("Sector"))
            {
                var sectorRepository = new SectorRepository();
                var directionRepository = new DirectionRepository();
                var directionId = sectorRepository.GetDirectionId(id);
                model.SectorName = sectorRepository.GetSectorNameById(id);
                model.SectorId = id;
                model.DirectionName = directionRepository.GetDirectionNameById(directionId);
                Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Direction"] + directionId;
                Session["SelectedNode"] = code + id;

            }
            else if (UnifiedTreeIdPrefixDictionary.TreeIdCodes.First(e => e.Value.Equals(code)).Key.Equals("Direction"))
            {
                var directionRepository = new DirectionRepository();
                model.SectorName = "";
                model.DirectionName = directionRepository.GetDirectionNameById(id);
                Session["ParentNode"] = "";
                Session["SelectedNode"] = code + id;
            }
            else
            {
                model.SectorName = "";
                model.DirectionName = "";
                Session["ParentNode"] = "";
                Session["SelectedNode"] = "";
            }

            //В данной версии тз привилегии на просмотр нормативов и планов выручки совпадают.
            //Но всё может измениться, посему заводим две переменные
            model.CanViewStandart = GrokRoleObjectsService.CanUserMakeAction(UserActions.Read,
                                                                             User.Identity.Name,
                                                                             DataObjects.StationStandardLoadTW);
            model.CanViewProceedsPlan = GrokRoleObjectsService.CanUserMakeAction(UserActions.Read,
                                                                             User.Identity.Name,
                                                                             DataObjects.StationPlannedRevenue);
            model.CanViewTimesheets = GrokRoleObjectsService.CanUserMakeAction(UserActions.Read,
                                                                               User.Identity.Name,
                                                                               DataObjects.Timesheet);

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий частичное представление для просмотра планов выручки участка
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Частичное представление для просмотра планов выручки участка</returns>
        [ChildActionOnly]
        [OutputCache(VaryByParam = "*", Duration = 11)]
        [RoleObjectAuthGrok(UserActions.Read, DataObjects.StationPlannedRevenue)]
        public ActionResult ProceedsPlansReview(int sectorId)
        {
            ViewBag.SectorId = sectorId;
            return PartialView();
        }

        /// <summary>
        /// Метод, возвращающий частичное представление для табелей учёта времени
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Частичное представление для табелей учёта времени</returns>
        [ChildActionOnly]
        [OutputCache(VaryByParam = "*", Duration = 11)]
        [RoleObjectAuthGrok(UserActions.Read, DataObjects.Timesheet)]
        public ActionResult Timesheet(int sectorId)
        {
            var httpRuntimeSection = ConfigurationManager.GetSection("system.web/httpRuntime") as HttpRuntimeSection;

            var sectorRepository = new SectorRepository();            
            var sectorModel = sectorRepository.GetSectorModelById(sectorId);

            ViewBag.SectorName = sectorModel.DirectionName + "-" + sectorModel.SectorName;
            ViewBag.SectorId = sectorId;

            ViewBag.MaxRequestLength = httpRuntimeSection != null
                                           ? httpRuntimeSection.MaxRequestLength
                                           : 4096;

            ViewBag.AreButtonsVisible = GrokRoleObjectsService.CanUserMakeAction(UserActions.Update,
                                                                               User.Identity.Name,
                                                                               DataObjects.Timesheet);

            ViewBag.IsUploadButtonVisible = GrokRoleObjectsService.CanUserMakeAction(UserActions.AttachDoc,
                                                                               User.Identity.Name,
                                                                               DataObjects.Timesheet);

            ViewBag.IsDeleteUploadButtonVisible = GrokRoleObjectsService.CanUserMakeAction(UserActions.DeleteAttachedDoc,
                                                                               User.Identity.Name,
                                                                               DataObjects.Timesheet);

            return PartialView();
        }

        /// <summary>
        /// Метод, возвращающий представление, для редактирование нормативной нагрузки станций
        /// </summary>
        /// <param name="id">Идентификатор объекта</param>
        /// <param name="code">Код объекта</param>
        /// <param name="filterBpa">Фильтр по БПА(-1 если невыбрано)</param>
        /// <param name="filterMktk">Фильтр по МКТК(-1 если невыбрано)</param>
        /// <returns>Представление, для редактирование нормативной нагрузки станций</returns>
        [RoleObjectAuthGrok(UserActions.Read, DataObjects.StationStandardLoadTW)]
        [OutputCache(VaryByParam = "*", Duration = 11)]
        public ActionResult EditStandart(int id, string code, int? filterBpa, int? filterMktk)
        {
            var model = new EditStandartViewModel();
            model.Id = id;
            model.Code = code;

            model.FilterBPAStandart = filterBpa ?? -1;
            model.FilterMKTKStandart = filterMktk ?? -1;

            UpdateStandarts(id, code, model);

            var needChangeFilter = false;
            if (!model.AllBPAStandartNames
                .Any(e => e.Value.Equals((model.FilterBPAStandart).ToString(CultureInfo.InvariantCulture))))
            {
                model.FilterBPAStandart = -1;
                needChangeFilter = true;
            }
            if (!model.AllMKTKStandartNames
                .Any(e => e.Value.Equals((model.FilterMKTKStandart).ToString(CultureInfo.InvariantCulture))))
            {
                model.FilterMKTKStandart = -1;
                needChangeFilter = true;
            }

            if (needChangeFilter)
            {
                UpdateStandarts(id, code, model);
            }

            model.CanEdit = GrokRoleObjectsService.CanUserMakeAction(UserActions.Update,
                                                                        User.Identity.Name,
                                                                        DataObjects.StationStandardLoadTW);

            return PartialView(model);
        }

        private void UpdateStandarts(int id, string code, EditStandartViewModel model)
        {
            var standartRepository = new StandartRepository();
            List<short> ticketsPerHourBpaList, ticketsPerHourMktkList;
                       
            var codeItem = UnifiedTreeIdPrefixDictionary.TreeIdCodes.FirstOrDefault(e => e.Value == code);
            if (codeItem.Key == "Sector")
            {
                ticketsPerHourBpaList = standartRepository.GetAllSectorStandartsByTerminalType(id, null, 
                                                                                               TerminalTypes.Bpa);
                ticketsPerHourMktkList = standartRepository.GetAllSectorStandartsByTerminalType(id, null,
                                                                                                TerminalTypes.Mktk);

            }
            else if (codeItem.Key == "Direction")
            {
                ticketsPerHourBpaList = standartRepository.GetAllSectorStandartsByTerminalType(null, id,
                                                                                               TerminalTypes.Bpa);
                ticketsPerHourMktkList = standartRepository.GetAllSectorStandartsByTerminalType(null, id,
                                                                                                TerminalTypes.Mktk);
            }
            else
            {
                ticketsPerHourBpaList = standartRepository.GetAllSectorStandartsByTerminalType(null, null,
                                                                                               TerminalTypes.Bpa);
                ticketsPerHourMktkList = standartRepository.GetAllSectorStandartsByTerminalType(null, null,
                                                                                                TerminalTypes.Mktk);
            }


            model.AllBPAStandartNames = ticketsPerHourBpaList.Select(t => new SelectListItem()
                                                                                {
                                                                                    Value = t.ToString(),
                                                                                    Text = t.ToString(CultureInfo.InvariantCulture)
                                                                                }).ToList();
            model.AllMKTKStandartNames = ticketsPerHourMktkList.Select(t => new SelectListItem()
                                                                                {
                                                                                    Value = t.ToString(),
                                                                                    Text = t.ToString(CultureInfo.InvariantCulture)
                                                                                }).ToList();            

            model.AllBPAStandartNames.Insert(0,
                                                new SelectListItem
                                                {
                                                    Value = (-1).ToString(CultureInfo.InvariantCulture),
                                                    Text = "Не выбрано"
                                                });
            model.AllMKTKStandartNames.Insert(0,
                                                 new SelectListItem
                                                 {
                                                     Value = (-1).ToString(CultureInfo.InvariantCulture),
                                                     Text = "Не выбрано"
                                                 });

        }

        #region AJAX

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с табелями
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="sort">Параметры сортировки</param>
        /// <param name="filter">Параметры фильтрации</param>
        /// <param name="monthDate">Дата, по которой определяется месяц для табелей</param>
        /// <param name="sectorId">Идентификатор станции</param>
        /// <returns>Список табелей и их количество в формате JSON</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.Timesheet)]
        public ActionResult GetTimesheets(int page, int pageSize, List<SortDescription> sort, FilterContainer filter,
                                          string monthDate, int sectorId)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                DateTime date;
                if (!DateTime.TryParse(monthDate, out date))
                {
                    throw new Exception("Некорректное значение месяца");
                }
                var month = DatesToMontsListConverter.ConvertDateToMonth(date);

                int itemsCount;

                var timesheets = timesheetRepository.GetTimesheets(page, pageSize, sort, filter, month, sectorId, out itemsCount);
                var timesheetId = timesheets.Any() 
                                        ? timesheets.First().TimesheetId
                                        : timesheetRepository.GetTimesheetId(month, sectorId);

                return Json(
                    new
                    {
                        Items = timesheets,
                        Count = itemsCount,
                        TimesheetId = timesheetId
                    });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage }, JsonFormat);
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса от внутреннего kendo-grid для табелей, 
        /// который содержит информаицю по дням
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор табеля</param>
        /// <returns>Строки для внутренней таблицы табелей в формате JSON</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.Timesheet)]
        public ActionResult GetTimesheetDetails(int timesheetDetailId)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                var timesheetRowDetails = timesheetRepository.GetTimesheetDays(timesheetDetailId);

                return Json(
                    new
                    {
                        Items = timesheetRowDetails,
                        Count = timesheetRowDetails.Count
                    });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage }, JsonFormat);
            }
        }

        /// <summary>
        /// Метод, обновляющий данные о явках и часах в табелях сотрудников
        /// </summary>
        /// <param name="details">Строки внутренней kendo-таблицы с часами и кодами явок</param>
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.Timesheet)]
        public ActionResult SaveTimesheetDetails(List<TimesheetRowDetailModel> details)
        {           
            var timesheetRepository = new TimesheetRepository();

            try
            {
                timesheetRepository.UpdateTimesheetDetails(details);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, exceptionMessage);
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid для кодов оплаты и кодов неявок
        /// табеля сотрудника
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор табеля сотрудника</param>
        /// <param name="isPresence">Флаг, показывающий, что надо вернуть: кода оплаты или неявок</param>
        /// <returns>Список кодов оплат или неявок и их количество в формате JSON</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.Timesheet)]
        public ActionResult GetTimesheetPayrollsOrHookies(int timesheetDetailId, bool isPresence)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                var codes = timesheetRepository.GetAllPayrollsOrHookies(timesheetDetailId, isPresence);

                return Json(
                    new
                    {
                        Items = codes,
                        Count = codes.Count
                    });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, exceptionMessage);
            }
        }       

        /// <summary>
        /// Обработчик ajax-запроса, проверяющего существование табеля для конкретного месяца
        /// для указанного участка
        /// </summary>
        /// <param name="monthDate">Месяц (дата с первым числом этого месяца)</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.Timesheet)]
        public ActionResult IsTimesheetForMonth(string monthDate, int sectorId)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                DateTime date;
                if (!DateTime.TryParse(monthDate, out date))
                {
                    throw new Exception("Некорректное значение месяца");
                }
                var month = DatesToMontsListConverter.ConvertDateToMonth(date);
                var isTimesheetExists = timesheetRepository.IsTimesheetForMonth(month, sectorId);

                return Json(new { status = JsonStatuses.Ok, result = new { isExists = isTimesheetExists } });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = e.Message });
            }
        }

        /// <summary>
        /// Метод, возвращающий файлы привязанные к табелю
        /// </summary>
        /// <param name="sectorId">Идентификатор сектора</param>
        /// <param name="month">Месяц табеля</param>
        /// <param name="year">Год табеля</param>
        /// <param name="page">Страница</param>
        /// <param name="pageSize">Размер страницы</param>
        /// <returns>Json-объект со статусом операции и список ошибок в случае невалидности модели</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.ViewAttachedDoc, DataObjects.Timesheet)]
        public ActionResult GetFiles(int sectorId, int month, int year, int page, int pageSize)
        {
            var timesheetRepository = new TimesheetRepository();

            int count;
            var files = timesheetRepository.GetFiles(sectorId, month, year, page, pageSize, out count);            

            return Json(
                new
                {
                    Items = files,
                    Count = count
                });
        }

        /// <summary>
        /// Метод, возвращающий результат добавления файла к табелю
        /// </summary>
        /// <param name="name">Название файла</param>
        /// <param name="sectorId">Идентификатор сектора</param>
        /// <param name="month">Месяц табеля</param>
        /// <param name="year">Год табеля</param>
        /// <param name="file">Бинарное представление файла</param>
        /// <returns>Json-объект со статусом операции и список ошибок в случае невалидности модели</returns>        
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.AttachDoc, DataObjects.Timesheet)]
        public ActionResult UploadFile(string name, int sectorId, int month, int year, HttpPostedFileBase file)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                var fileNameParts = file.FileName.Split('.');
                var fileName = fileNameParts[0];
                fileName = fileName.Substring(0, Math.Min(fileName.Length, 128));
                var fileExtention = (fileNameParts[fileNameParts.Count() - 1]);
                fileExtention = fileExtention.Substring(0, Math.Min(fileExtention.Length, 4));

                if (fileExtention.IsExtentionByExecutedFormat())
                {
                    var errorMessage = "Расширение файла \"." + fileExtention + "\" является недопустимым";
                    return Json(new { status = JsonStatuses.Error, message = errorMessage }, "text/plain");
                }

                timesheetRepository.UploadFile(sectorId, month, year, name, fileName, fileExtention, file.InputStream);

                return Json(new { status = JsonStatuses.Ok }, "text/plain");
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = e.Message }, "text/plain");
            }

        }

        /// <summary>
        /// Метод, возвращающий результат удаления файла из привязанных к табелю
        /// </summary>
        /// <param name="fileId">Идентификатор файла</param>
        /// <returns>Json-объект со статусом операции и список ошибок в случае невалидности модели</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.DeleteAttachedDoc, DataObjects.Timesheet)]
        public ActionResult RemoveFile(int fileId)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                timesheetRepository.RemoveFile(fileId);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = e.Message });
            }

        }

        /// <summary>
        /// Обработчик ajax-запроса составления табеля на конкретный месяц
        /// для указанного участка
        /// </summary>
        /// <param name="monthDate">Месяц (дата с первым числом этого месяца)</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="isAuto">Флаг, показывающий, необходимо ли автоматически
        ///                     генерировать табели для кассиров из графиков</param>        
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.FormTimesheet, DataObjects.Timesheet)]
        public ActionResult FormTimesheet(string monthDate, int sectorId, bool isAuto)
        {
            var timesheetRepository = new TimesheetRepository();            
            var timesheetValidator = new TimesheetValidator();

            try
            {                
                DateTime date;
                if (!DateTime.TryParse(monthDate, out date))
                {
                    throw new Exception("Некорректное значение месяца");
                }
                var month = DatesToMontsListConverter.ConvertDateToMonth(date);
                timesheetRepository.FormTimesheet(month, sectorId, User.Identity.Name);

                string warning = null;
                if (!timesheetValidator.AreEmployeesOnSector(sectorId))
                {
                    warning = "На участке нет сотрудников";
                }

                if (isAuto)
                {                    
                    timesheetRepository.GenerateHalfTimesheet(month, sectorId, true);
                }

                return Json(new { status = JsonStatuses.Ok, message = warning });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса добавления сотрудника в табель
        /// </summary>
        /// <param name="timesheetId">Идентификатор месячного табеля участка</param>
        /// <param name="employeeId">Идентификатор сотрудника, которого надо добавить в табель</param>
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.Timesheet)]
        public ActionResult AddEmployee(int timesheetId, int employeeId)
        {
            var timesheetRepository = new TimesheetRepository();
            var employeeRepository = new EmployeeRepository();

            try
            {
                timesheetRepository.AddEmployeeToTimesheet(timesheetId, employeeId, User.Identity.Name);
                var employeeFullName = employeeRepository.GetFullNameById(employeeId);

                return Json(new { status = JsonStatuses.Ok, result = new { employeeName = employeeFullName } });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса удаления сотрудника из табеля
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор месячного табеля участка</param>
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.Timesheet)]
        public ActionResult DeleteEmployee(int timesheetDetailId)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                timesheetRepository.DeleteEmployeeFromTimesheet(timesheetDetailId, User.Identity.Name);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage });
            }

        }

        /// <summary>
        /// Обработчик ajax-запроса генерации табеля для сотрудников в должности кассира, если у них имеются плановые графики
        /// за указанный месяц
        /// </summary>
        /// <param name="monthDate">Месяц (дата с первым числом этого месяца)</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="isPrepayment">Флаг, показывающий половину месяца (первая, то есть аванс, или вторая, то есть полный табель)</param>
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.Timesheet)]
        public ActionResult GeneratePrepayments(string monthDate, int sectorId, bool isPrepayment)
        {
            var timesheetRepository = new TimesheetRepository();            

            try
            {
                DateTime date;
                if (!DateTime.TryParse(monthDate, out date))
                {
                    throw new Exception("Некорректное значение месяца");
                }
                var month = DatesToMontsListConverter.ConvertDateToMonth(date);
                
                timesheetRepository.GenerateHalfTimesheet(month, sectorId, isPrepayment);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса очистки второй половины табелей сотрудников в должности кассира, 
        /// если у них имеются плановые графики
        /// </summary>
        /// <param name="monthDate">Месяц (дата с первым числом этого месяца)</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.Timesheet)]
        public ActionResult ClearSecondTimesheetparts(string monthDate, int sectorId)
        {
            var timesheetRepository = new TimesheetRepository();            

            try
            {
                DateTime date;
                if (!DateTime.TryParse(monthDate, out date))
                {
                    throw new Exception("Некорректное значение месяца");
                }
                var month = DatesToMontsListConverter.ConvertDateToMonth(date);
                
                timesheetRepository.ClearSecondTimesheetPart(month, sectorId);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса генерации месячного табеля для конкретного сотрудника из его планового графика за этот месяц
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор месячного табеля сотрудника</param>
        /// <param name="isPrepayment">Флаг, показывающий половину месяца (первая, то есть аванс, или вторая, то есть полный табель)</param>
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.Timesheet)]
        public ActionResult GenerateIndividualPrepayments(int timesheetDetailId, bool isPrepayment)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                timesheetRepository.GenerateIndividualHalfTimesheet(timesheetDetailId, isPrepayment);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса очистки второй половины месячного табеля для конкретного сотрудника
        /// </summary>
        /// <param name="timesheetDetailId">Идентификатор месячного табеля сотрудника</param>
        /// <returns>Json-объект со статусом выполения операции</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.Timesheet)]
        public ActionResult ClearIndividualSecondTimesheetpart(int timesheetDetailId)
        {
            var timesheetRepository = new TimesheetRepository();

            try
            {
                timesheetRepository.ClearIndividualSecondTimesheetPart(timesheetDetailId);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                    ? e.InnerException.Message
                                    : e.Message;
                return Json(new { status = JsonStatuses.Error, message = exceptionMessage });
            }
        }

        ///  <summary>
        /// Метод возвращающий нормативные стандарты для  выбранного объекта
        ///  </summary>
        /// <param name="id">Идентификатор объекта</param>
        ///  <param name="code">Код объекта</param>
        /// <param name="page">Номер страницы</param>
        /// <param name="pageSize">Кол-во строк на страницу</param>
        /// <param name="orderby">Поле по которому идет сортировка(null если не нужна)</param>
        /// <param name="orderto">Порядок(desc- по ублыванию, любое другое значение - по возрастанию)</param>
        /// <param name="filterBpa">Фильтр по БПА(-1 если невыбрано)</param>
        /// <param name="filterMktk">Фильтр по МКТК(-1 если невыбрано)</param>
        /// <returns>Нормативные стандарты для  выблранного объекта</returns>
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.StationStandardLoadTW)]
        [HttpPost]
        public ActionResult GetStandartRows(int page, int pageSize, string orderby, string orderto, int filterBpa, int filterMktk, int id, string code)
        {
            int count;
            var model = StandartRowModels(page, pageSize, @orderby, orderto, filterBpa, filterMktk, id, code, out count);


            return new JsonResult { Data = new { Items = model, Count = count } };
        }        

        private List<StandartRowModel> StandartRowModels(int page, int pageSize, string @orderby, string orderto, int filterBpa, int filterMktk,
                                       int id, string code, out int count)
        {
            var standartRepository = new StandartRepository();            

            List<StandartRowModel> model;            

            var codeItem = UnifiedTreeIdPrefixDictionary.TreeIdCodes.FirstOrDefault(e => e.Value == code);
            if (codeItem.Key == "Sector")
            {
                model = standartRepository.GetStandartRowModels(page, pageSize, @orderby, orderto, 
                                                                filterBpa, filterMktk, id, null,
                                                                out count);
            }
            else if (codeItem.Key == "Direction")
            {
                model = standartRepository.GetStandartRowModels(page, pageSize, @orderby, orderto, 
                                                                filterBpa, filterMktk, null, id,
                                                                out count);
            }
            else
            {
                model = standartRepository.GetStandartRowModels(page, pageSize, @orderby, orderto, 
                                                                filterBpa, filterMktk, null, null,
                                                                out count);
            }
            return model;
        }

        ///  <summary>
        ///  Сохраняет нормативные стандарты
        ///  </summary>
        /// <param name="ids">Идентификаторы стандартов</param>
        /// <param name="value">Зачение</param>
        /// <param name="column">Номер столбца(1-МКТК, 2-БПА)</param>
        /// <param name="filterBpa">Фильтр по БПА(-1 если невыбрано)</param>
        /// <param name="filterMktk">Фильтр по МКТК(-1 если невыбрано)</param>
        /// <param name="id">Идентификатор объекта</param>
        ///  <param name="code">Код объекта</param>
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.StationStandardLoadTW)]
        [HttpPost]
        public ActionResult SetStandartRows(List<int> ids, int value, int column, int filterBpa, int filterMktk, int id, string code)
        {
            var standartRepository = new StandartRepository();

            standartRepository.SaveStandarts(ids, checked((short)value), column);

            int newFilteredValuesCount;
            List<short> allSectorBpaStandarts, allSectorMktkStandarts;

            var codeItem = UnifiedTreeIdPrefixDictionary.TreeIdCodes.FirstOrDefault(e => e.Value == code);
            if (codeItem.Key == "Sector")
            {
                newFilteredValuesCount = standartRepository.GetFilteredValuesCount(filterBpa, filterMktk, id, null);
                allSectorBpaStandarts = standartRepository.GetAllSectorStandartsByTerminalType(id, null, TerminalTypes.Bpa);               
                allSectorMktkStandarts = standartRepository.GetAllSectorStandartsByTerminalType(id, null, TerminalTypes.Mktk);                                
            } 
            else if (codeItem.Key == "Direction")
            {
                newFilteredValuesCount = standartRepository.GetFilteredValuesCount(filterBpa, filterMktk, null, id);
                allSectorBpaStandarts = standartRepository.GetAllSectorStandartsByTerminalType(null, id, TerminalTypes.Bpa);               
                allSectorMktkStandarts = standartRepository.GetAllSectorStandartsByTerminalType(null, id, TerminalTypes.Mktk); 
            }
            else
            {
                newFilteredValuesCount = standartRepository.GetFilteredValuesCount(filterBpa, filterMktk, null, null);
                allSectorBpaStandarts = standartRepository.GetAllSectorStandartsByTerminalType(null, null, TerminalTypes.Bpa);               
                allSectorMktkStandarts = standartRepository.GetAllSectorStandartsByTerminalType(null, null, TerminalTypes.Mktk); 
            }

            return Json(new { Data = "Ok", 
                              newFilteredValuesCount = newFilteredValuesCount,
                              allSectorBpaStandarts = allSectorBpaStandarts,
                              allSectorMktkStandarts = allSectorMktkStandarts});            
        }

        #endregion

        #region FILERESULT

        /// <summary>
        /// Метод, производящий выгрузку табеля станции за определённый месяц в файл формата excel 
        /// </summary>
        /// <param name="fileId">Идентификатор файла</param>
        /// <returns>Json-объект со статусом операции и список ошибок в случае невалидности модели</returns>
        [RoleObjectAuthGrok(UserActions.ViewAttachedDoc, DataObjects.Timesheet)]
        public FileResult LoadFile(int fileId)
        {
            var timesheetRepository = new TimesheetRepository();
            var file = timesheetRepository.LoadFile(fileId);

            return File(file.Data, System.Net.Mime.MediaTypeNames.Text.Html, string.Format("{0}.{1}", file.Name, file.Extension));
        }

        /// <summary>
        /// Обработчик запроса печати табелей (экспорта в excel)
        /// </summary>
        /// <param name="id">Идентификатор месячного табеля всех сотрудника участка</param>
        /// <returns>excel-файл с данными табеля</returns>
        [RoleObjectAuthGrok(UserActions.Export, DataObjects.Timesheet)]
        public ActionResult Export(int? id)
        {            
            var timesheetRepository = new TimesheetRepository();
            var userRepository = new UserRepository();

            var userId = userRepository.GetIdByLogin(User.Identity.Name);
            var model = timesheetRepository.GetExportData(id ?? 0, userId);           
           
            if (model == null)
            {
                return new HttpStatusCodeResult(404, "Timesheet information wasnot found in the database");
            }
            else
            {                
                var fileName = HttpContext.Server.MapPath(Path.Combine(AppDateRelativePath, RelativeTemplateFilePath));
                var editor = new TimesheetExcelDocumentEditor();                
                var resultStream = editor.EditDocumentFromModel(fileName, model);               

                return File(resultStream, ExcelMimeType, DefaultScheduleFileName);                
            }
        }

        #endregion
    }
}
