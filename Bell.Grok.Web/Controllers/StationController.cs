﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Services.Exceptions;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.Services;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web.Controllers
{    
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class StationController : Controller
    {
        //
        // GET: /Station/

        /// <summary>
        /// Метод, возвращающий форму паспорта станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка (станция может принадлежать сразу нескольким участкам!!!)</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <param name="month">Месяц выбранного планового граффика</param>
        /// <param name="year">Год выбранного планового граффика</param>
        /// <returns>Строго типизированное представление с формой паспорта станции</returns>
        [AuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]
        public ActionResult Index(int sectorId, int stationId, string currentDate, string month = null, string year = null)
        {
            var stationRepository = new StationRepository();
            var sectorRepository = new SectorRepository();
            var tellerWindowRepository = new TellerWindowRepository();
            var terminalRepository = new TerminalRepository();
            var tellerWindowScheduleRepository = new TellerWindowScheduleRepository();

            var model = new StationPassportViewModel();
            if (!stationRepository.EditModelFromStationAndSector(model, sectorId, stationId, sectorRepository))
            {
                return HttpNotFound();
            }
            model.IsShowCurrent = true;
            model.WindowTellerNumbers = tellerWindowRepository.GetAllNumbersForStation(sectorId, stationId)
                                                              .Select(a => a.ToSelectListItem())
                                                              .ToList();
            model.SelectedWindowTellerNumbers = new List<TellerWindowNumberModel>();

            model.TerminalNumbers = terminalRepository.GetAllNumbers(sectorId, stationId);
            model.SelectedTerminalNumbers = new List<TerminalNumberModel>();

            model.TerminalTypes = terminalRepository.GetTerminalTypes();
            model.SelectedTerminalTypes = new List<TerminalTypeModel>();

            model.SelectedWindowTellerNumbersInTerminalTab = new List<TellerWindowNumberModel>();

            model.Month = string.IsNullOrWhiteSpace(month) ? (DateTime.Now.Month + 2).ToString(CultureInfo.InvariantCulture) : month;
            model.Year = string.IsNullOrWhiteSpace(year) ? DateTime.Now.Year.ToString(CultureInfo.InvariantCulture) : year;

            model.Seasons = tellerWindowScheduleRepository.GetSeasons();

            ViewBag.SectorId = sectorId;
            ViewBag.StationId = stationId;
            ViewBag.SectorStationId = stationRepository.GetSectorStationId(stationId, sectorId);
            ViewBag.CurrentDate = currentDate;
            ViewBag.CurrentUrl = Request.RawUrl;

            ViewBag.FullStationName = stationRepository.GetStationFullName(stationId, sectorId);

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            ViewBag.AreButtonsDisabled = User.IsInRole(Roles.OOPEmployee.ToText()) || User.IsInRole(Roles.COEmployee.ToText()) ||
                                         User.IsInRole(Roles.OTZPEmployee.ToText()) || User.IsInRole(Roles.DirectionHead.ToText()) ||
                                         User.IsInRole(Roles.ProfessionalGroupOrganizer.ToText());

            ViewBag.IsTellerWindowsTabVisible = GrokRoleObjectsService.CanUserMakeAction(UserActions.Read,
                                                                                         User.Identity.Name,
                                                                                         DataObjects.TellerWindow);
            ViewBag.IsTerminalsTabVisible = GrokRoleObjectsService.CanUserMakeAction(UserActions.Read,
                                                                                     User.Identity.Name,
                                                                                     DataObjects.Terminal);

            ViewBag.AreAddScheduleButtonsEnabled = GrokRoleObjectsService.CanUserMakeAction(UserActions.Add,
                                                                                            User.Identity.Name,
                                                                                            DataObjects.StationSchedule);
            ViewBag.AreDeleteScheduleButtonsEnabled = GrokRoleObjectsService.CanUserMakeAction(UserActions.Delete,
                                                                                                  User.Identity.Name,
                                                                                                  DataObjects.StationSchedule);
            ViewBag.AreSendReconcileScheduleButtonsEnabled = GrokRoleObjectsService.CanUserMakeAction(UserActions.ApprovalSubmitting,
                                                                                                      User.Identity.Name,
                                                                                                      DataObjects.StationSchedule);
            ViewBag.AreCancelReconcileScheduleButtonsEnabled = GrokRoleObjectsService.CanUserMakeAction(UserActions.ApprovalCancel,
                                                                                                        User.Identity.Name,
                                                                                                        DataObjects.StationSchedule);
            ViewBag.IsUserSectorChief = GrokRoleObjectsService.CanUserMakeAction(UserActions.ApprovConfirmChief,
                                                                                 User.Identity.Name,
                                                                                 DataObjects.StationSchedule);
            ViewBag.IsUserProfGroupOrganizator = GrokRoleObjectsService.CanUserMakeAction(UserActions.ApprovConfirmTU,
                                                                                          User.Identity.Name,
                                                                                          DataObjects.StationSchedule);
            ViewBag.IsProceedsPlansTabVisible = GrokRoleObjectsService.CanUserMakeAction(UserActions.Read,
                                                                                         User.Identity.Name,
                                                                                         DataObjects.StationPlannedRevenue);
            ViewBag.IsProceedsPlansGridEditable = GrokRoleObjectsService.CanUserMakeAction(UserActions.Update,
                                                                                           User.Identity.Name,
                                                                                           DataObjects.StationPlannedRevenue);
            ViewBag.IsEmployeeShiftScheduleTabVisible = GrokRoleObjectsService.CanUserMakeAction(UserActions.Read,
                                                                                                 User.Identity.Name,
                                                                                                 DataObjects.StationEmployeeShiftSchedule);
            ViewBag.IsEmployeeShiftScheduleTabEditable = GrokRoleObjectsService.CanUserMakeAction(UserActions.Update,
                                                                                                 User.Identity.Name,
                                                                                                 DataObjects.StationEmployeeShiftSchedule);
            ViewBag.IsScheduleTabVisible = GrokRoleObjectsService.CanUserMakeAction(UserActions.Read,
                                                                                                 User.Identity.Name,
                                                                                                 DataObjects.StationSchedule);

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление редактирования|создания кассового окна на станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="tellerWindowId">Идентификатор редактируемого кассового окна(если отсутствует то это созданеи нового окна)</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Строго типизированное представление с формой редактирования кассового окна на станции</returns>
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.TellerWindow)]
        public ActionResult EditTellerWindow(int sectorId, int stationId, int? tellerWindowId, string currentDate)
        {
            var tellerWindowRepository = new TellerWindowRepository();
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();

            var model = new TellerWindowEditViewModel();
            if (tellerWindowId.HasValue)
            {
                if (!tellerWindowRepository.TellerWindowWithIdExists(tellerWindowId.Value))
                {
                    return HttpNotFound();
                }

                if (!tellerWindowRepository.EditModelFromTellerWindow(model, tellerWindowId.Value))
                {
                    return HttpNotFound();
                }

                model.SetTerminalsListString = tellerWindowTerminalRepository.GetSetTerminalNumbers(tellerWindowId.Value);
                model.OldProfile = model.Profile;
            }
            else
            {
                model.IsNew = true;
            }

            model.SectorId = sectorId;
            model.StationId = stationId;
            model.CurrentDate = currentDate;

            model.AllPaymentMethods =
                tellerWindowRepository.GetAllPaymentMethods().Select(a => a.ToSelectListItem()).ToList();
            model.AllProfiles =
                tellerWindowRepository.GetAllProfiles().Select(a => a.ToSelectListItem()).ToList();
            model.AllPlacementNames =
                tellerWindowRepository.GetAllPlacementNames().Select(a => a.ToSelectListItem()).ToList();
            model.AllSpecializations =
                tellerWindowRepository.GetAllSpecializations().Select(a => a.ToSelectListItem()).ToList();
            model.AllStatuses =
                tellerWindowRepository.GetAllStatuses().Select(a => a.ToSelectListItem()).ToList();
            model.AllTellerWindowBreakes =
                tellerWindowRepository.GetAllTellerWindowBreakes().Select(a => a.ToSelectListItem()).ToList();
            model.AllTellerWindowShiftnesses =
                tellerWindowRepository.GetAllTellerWindowShiftness().Select(a => a.ToSelectListItem()).ToList();
            model.AllWeekDays =
                tellerWindowRepository.GetAllWeekDays().OrderBy(e => e.Value).Select(a => a.ToSelectListItem()).ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View(model);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы редактирования|создания кассового окна на станции
        /// </summary>
        /// <param name="model">Модель представления редактирования|создания кассового окна на станции</param>
        /// <returns>Перенаправление на страницу EditTellerWindowSuccess в случае валидности модели
        ///          и строго типизированное представление с формой редактирования кассового окна на станции
        ///          в противном случае</returns>
        [HttpPost]
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.TellerWindow)]
        public ActionResult EditTellerWindow(TellerWindowEditViewModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.OperationPeriodsData))
            {
                model.OperationPeriods =
                    new JavaScriptSerializer().Deserialize<List<TellerWindowOperationPeriodModel>>(
                        model.OperationPeriodsData);
            }

            var tellerWindowRepository = new TellerWindowRepository();
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();

            if (ModelState.IsValid)
            {
                var needCheckBindingsForClose = !model.IsNew &&
                                        !string.IsNullOrEmpty(model.SetTerminalsListString);

                tellerWindowRepository.UpdateFromModel(model, needCheckBindingsForClose);

                return RedirectToAction("EditTellerWindowSuccess",
                                        new RouteValueDictionary(
                                            new
                                            {
                                                controller = "Station",
                                                action = "EditTellerWindowSuccess",
                                                sectorId = model.SectorId,
                                                stationId = model.StationId,
                                                currentDate = model.CurrentDate,
                                                isNew = model.IsNew
                                            }));
            }

            if (!model.IsNew)
            {
                model.SetTerminalsListString = tellerWindowTerminalRepository.GetSetTerminalNumbers(model.TellerWindowId);
            }

            model.AllPaymentMethods =
                tellerWindowRepository.GetAllPaymentMethods().Select(a => a.ToSelectListItem()).ToList();
            model.AllProfiles =
                tellerWindowRepository.GetAllProfiles().Select(a => a.ToSelectListItem()).ToList();
            model.AllPlacementNames =
                tellerWindowRepository.GetAllPlacementNames().Select(a => a.ToSelectListItem()).ToList();
            model.AllSpecializations =
                tellerWindowRepository.GetAllSpecializations().Select(a => a.ToSelectListItem()).ToList();
            model.AllStatuses =
                tellerWindowRepository.GetAllStatuses().Select(a => a.ToSelectListItem()).ToList();
            model.AllTellerWindowBreakes =
                tellerWindowRepository.GetAllTellerWindowBreakes().Select(a => a.ToSelectListItem()).ToList();
            model.AllTellerWindowShiftnesses =
                tellerWindowRepository.GetAllTellerWindowShiftness().Select(a => a.ToSelectListItem()).ToList();
            model.AllWeekDays =
                tellerWindowRepository.GetAllWeekDays().OrderBy(e => e.Value).Select(a => a.ToSelectListItem()).ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + model.SectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + model.StationId;
            Session["CurrentDate"] = model.CurrentDate;

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешном изменении|создании кассового окна на станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <param name="isNew">Признак создания нового кассового окна</param>
        /// <returns>Представление, извещающее об успешном изменении кассового окна на станции</returns>
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.TellerWindow)]
        public ActionResult EditTellerWindowSuccess(int sectorId, int stationId, string currentDate, bool isNew)
        {
            ViewBag.SectorId = sectorId;
            ViewBag.StationId = stationId;
            ViewBag.CurrentDate = currentDate;
            ViewBag.IsNew = isNew;

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View();
        }

        /// <summary>
        /// Метод, возвращающий представление создания терминала (если его ещё нет) и привязки его к станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Строго типизированное представление с формой создания терминала и привязки его к станции</returns>
        [RoleObjectAuthGrok(UserActions.Add, DataObjects.Terminal)]
        public ActionResult CreateTerminal(int sectorId, int stationId, string currentDate)
        {
            var terminalRepository = new TerminalRepository();

            var terminalCreateModel = new TerminalCreateViewModel();
            terminalCreateModel.SectorId = sectorId;
            terminalCreateModel.StationId = stationId;
            terminalCreateModel.CurrentDate = currentDate;

            terminalCreateModel.AllAgents = terminalRepository.GetAllAgetns().Select(a => a.ToSelectListItem()).ToList();
            terminalCreateModel.AllMarks = new List<SelectListItem>();
            terminalCreateModel.AllPlacements = terminalRepository.GetAllPlacements().Select(a => a.ToSelectListItem()).ToList();
            terminalCreateModel.AllProfiles = terminalRepository.GetAllProfiles().Select(a => a.ToSelectListItem()).ToList();
            terminalCreateModel.AllPurposes = terminalRepository.GetAllPurposes().Select(a => a.ToSelectListItem()).ToList();
            terminalCreateModel.AllStatuses = terminalRepository.GetAllStatuses().Select(a => a.ToSelectListItem()).ToList();
            terminalCreateModel.AllTypes = terminalRepository.GetAllTypes().Select(a => a.ToSelectListItem()).ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            ViewBag.IsAfterError = false;

            return View(terminalCreateModel);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы создания терминала (если его ещё нет) и привязки его к станции
        /// </summary>
        /// <param name="model">Модель представления создания терминала на станции</param>
        /// <returns>Перенаправление на страницу CreateTerminalSuccess в случае валидности модели
        ///          и строго типизированное представление с формой создания терминала (если его ещё нет) и привязки его к станции</returns>
        [HttpPost]
        [RoleObjectAuthGrok(UserActions.Add, DataObjects.Terminal)]
        public ActionResult CreateTerminal(TerminalCreateViewModel model)
        {
            var terminalRepository = new TerminalRepository();

            if (ModelState.IsValid)
            {
                terminalRepository.UpdateFromModel(model, User.Identity.Name);

                return RedirectToAction("CreateTerminalSuccess",
                                        new RouteValueDictionary(
                                            new
                                            {
                                                controller = "Station",
                                                action = "CreateTerminalSuccess",
                                                sectorId = model.SectorId,
                                                stationId = model.StationId,
                                                currentDate = model.CurrentDate
                                            }));
            }

            model.AllAgents = terminalRepository.GetAllAgetns().Select(a => a.ToSelectListItem()).ToList();
            model.AllMarks = new List<SelectListItem>();
            model.AllPlacements = terminalRepository.GetAllPlacements().Select(a => a.ToSelectListItem()).ToList();
            model.AllProfiles = terminalRepository.GetAllProfiles().Select(a => a.ToSelectListItem()).ToList();
            model.AllPurposes = terminalRepository.GetAllPurposes().Select(a => a.ToSelectListItem()).ToList();
            model.AllStatuses = terminalRepository.GetAllStatuses().Select(a => a.ToSelectListItem()).ToList();
            model.AllTypes = terminalRepository.GetAllTypes().Select(a => a.ToSelectListItem()).ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + model.SectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + model.StationId;
            Session["CurrentDate"] = model.CurrentDate;

            ViewBag.IsAfterError = true;

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешном создании терминала (если его ещё нет) и привязки его к станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Представление, извещающее об успешном создании терминала (если его ещё нет) и привязки его к станции</returns>
        [RoleObjectAuthGrok(UserActions.Add, DataObjects.Terminal)]
        public ActionResult CreateTerminalSuccess(int sectorId, int stationId, string currentDate)
        {
            ViewBag.SectorId = sectorId;
            ViewBag.StationId = stationId;
            ViewBag.CurrentDate = currentDate;

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View();
        }

        /// <summary>
        /// Метод, возвращающий представление редактирования терминала и(или) привязки его к станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="terminalId">Идентификатор редактируемого терминала</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <returns>Строго типизированное представление с формой редактирования терминала и привязки его к станции</returns>
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.Terminal)]
        public ActionResult EditTerminal(int sectorId, int stationId, int terminalId, string currentDate, int sectorStationTerminalId)
        {
            var terminalRepository = new TerminalRepository();

            if (!terminalRepository.TerminalWithIdExists(terminalId))
            {
                return HttpNotFound();
            }

            var model = new TerminalEditViewModel();
            if (!terminalRepository.EditModelFromTerminal(model, sectorStationTerminalId))
            {
                return HttpNotFound();
            }

            model.SectorId = sectorId;
            model.StationId = stationId;
            model.CurrentDate = currentDate;

            model.AllAgents = terminalRepository.GetAllAgetns().Select(a => a.ToSelectListItem()).ToList();
            model.AllMarks = new List<SelectListItem>();
            model.AllPlacements = terminalRepository.GetAllPlacements().Select(a => a.ToSelectListItem()).ToList();
            model.AllProfiles = terminalRepository.GetAllProfiles().Select(a => a.ToSelectListItem()).ToList();
            model.AllPurposes = terminalRepository.GetAllPurposes().Select(a => a.ToSelectListItem()).ToList();
            model.AllStatuses = terminalRepository.GetAllStatuses().Select(a => a.ToSelectListItem()).ToList();
            model.AllTypes = terminalRepository.GetAllTypes().Select(a => a.ToSelectListItem()).ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View(model);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы редактирования терминала и(или) привязки его к станции
        /// </summary>
        /// <param name="model">Модель представления редактирования терминала на станции</param>
        /// <returns>Перенаправление на страницу EditTerminalSuccess в случае валидности модели
        ///          и строго типизированное представление с формой редактирования терминала и привязки его к станции</returns>
        [HttpPost]
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.Terminal)]
        public ActionResult EditTerminal(TerminalEditViewModel model)
        {
            var terminalRepository = new TerminalRepository();

            if (ModelState.IsValid)
            {
                terminalRepository.EditFromModel(model, User.Identity.Name);

                return RedirectToAction("EditTerminalSuccess",
                                        new RouteValueDictionary(
                                            new
                                            {
                                                controller = "Station",
                                                action = "EditTerminalSuccess",
                                                sectorId = model.SectorId,
                                                stationId = model.StationId,
                                                currentDate = model.CurrentDate
                                            }));
            }

            model.AllAgents = terminalRepository.GetAllAgetns().Select(a => a.ToSelectListItem()).ToList();
            model.AllMarks = new List<SelectListItem>();
            model.AllPlacements = terminalRepository.GetAllPlacements().Select(a => a.ToSelectListItem()).ToList();
            model.AllProfiles = terminalRepository.GetAllProfiles().Select(a => a.ToSelectListItem()).ToList();
            model.AllPurposes = terminalRepository.GetAllPurposes().Select(a => a.ToSelectListItem()).ToList();
            model.AllStatuses = terminalRepository.GetAllStatuses().Select(a => a.ToSelectListItem()).ToList();
            model.AllTypes = terminalRepository.GetAllTypes().Select(a => a.ToSelectListItem()).ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + model.SectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + model.StationId;
            Session["CurrentDate"] = model.CurrentDate;

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешном изменении терминала и(или) привязки его к станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Представление, извещающее об успешном изменении терминала и привязки его к станции</returns>
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.Terminal)]
        public ActionResult EditTerminalSuccess(int sectorId, int stationId, string currentDate)
        {
            ViewBag.SectorId = sectorId;
            ViewBag.StationId = stationId;
            ViewBag.CurrentDate = currentDate;

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View();
        }

        /// <summary>
        /// Метод, возвращающий представление создания привязки терминала к кассовому окну на станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Строго типизированное представление с формой создания привязки терминала к кассовому окну на станции</returns>
        [RoleObjectAuthGrok(UserActions.Add, DataObjects.Terminal)]
        public ActionResult AddTerminalToTellerWindow(int sectorId, int stationId, int sectorStationTerminalId, string currentDate)
        {
            var terminalRepository = new TerminalRepository();
            var tellerWindowRepository = new TellerWindowRepository();

            var model = new TerminalToTellerWindowViewModel();

            var terminalNumvarAndProfilePair = terminalRepository.GetNumberAndProfileBySectorStationTerminalId(sectorStationTerminalId);
            model.TerminalNumber = terminalNumvarAndProfilePair.Item1;
            model.TerminalProfile = terminalNumvarAndProfilePair.Item2;
            model.SectorStationTerminalId = sectorStationTerminalId;

            model.SectorId = sectorId;
            model.StationId = stationId;
            model.CurrentDate = currentDate;

            model.AllTellerWindowsNumbers = tellerWindowRepository.GetAllNumbersForStation(sectorId, stationId, terminalNumvarAndProfilePair.Item2)
                                                                  .Select(a => a.ToSelectListItem())
                                                                  .ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View(model);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы создания привязки терминала к кассовому окну на станции
        /// </summary>
        /// <param name="model">Модель представления создания привязки терминала к кассовому окну на станции</param>
        /// <returns>Перенаправление на страницу AddTerminalToTellerWindowSuccess в случае валидности модели
        ///          и строго типизированное представление с формой создания привязки терминала к кассовому окну на станции</returns>
        [HttpPost]
        [RoleObjectAuthGrok(UserActions.Add, DataObjects.Terminal)]
        public ActionResult AddTerminalToTellerWindow(TerminalToTellerWindowViewModel model)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();
            var tellerWindowRepository = new TellerWindowRepository();

            if (ModelState.IsValid)
            {
                tellerWindowTerminalRepository.CreateFromModel(model);

                return RedirectToAction("AddTerminalToTellerWindowSuccess",
                                        new RouteValueDictionary(
                                            new
                                            {
                                                controller = "Station",
                                                action = "AddTerminalToTellerWindowSuccess",
                                                sectorId = model.SectorId,
                                                stationId = model.StationId,
                                                currentDate = model.CurrentDate
                                            }));
            }

            model.AllTellerWindowsNumbers = tellerWindowRepository.GetAllNumbersForStation(model.SectorId, model.StationId, model.TerminalProfile)
                                                                  .Select(a => a.ToSelectListItem())
                                                                  .ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + model.SectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + model.StationId;
            Session["CurrentDate"] = model.CurrentDate;

            CheckEqualErrors();

            return View(model);
        }

        /// <summary>
        /// Метод, удаляющий одинаковые ошибки модели
        /// </summary>
        private void CheckEqualErrors()
        {
            var equalErrors = ModelState[""].Errors.GroupBy(e => e.ErrorMessage).SelectMany(group => group.Skip(1)).ToList();
            if (equalErrors.Count > 0)
            {
                var oneEqualError =
                    ModelState[""].Errors.FirstOrDefault(e => e.ErrorMessage == equalErrors[0].ErrorMessage);

                if (oneEqualError != null)
                {
                    ModelState[""].Errors.Remove(oneEqualError);
                }
            }
        }

        /// <summary>
        /// Метод, возвращающий представление редактирования привязки терминала к кассовому окну на станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="tellerWindowTerminalId">Идентификатор редактируемой привязки терминала к кассовому окну</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Строго типизированное представление с формой редактирования привязки терминала к кассовому окну на станции</returns>
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.Terminal)]
        public ActionResult EditTerminalToTellerWindow(int sectorId, int stationId, int tellerWindowTerminalId, string currentDate)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();
            var tellerWindowRepository = new TellerWindowRepository();

            if (!tellerWindowTerminalRepository.TellerWindowTerminalWithIdExists(tellerWindowTerminalId))
            {
                return HttpNotFound();
            }

            var model = new TerminalToTellerWindowViewModel();
            if (!tellerWindowTerminalRepository.EditModelFromTellerWindowTerminal(model, tellerWindowTerminalId))
            {
                return HttpNotFound();
            }

            model.SectorId = sectorId;
            model.StationId = stationId;
            model.CurrentDate = currentDate;            

            model.AllTellerWindowsNumbers = tellerWindowRepository.GetAllNumbersForStation(sectorId, stationId, model.TerminalProfile)
                                                                  .Select(a => a.ToSelectListItem())
                                                                  .ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View(model);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы редактирования привязки терминала к кассовому окну на станции
        /// </summary>
        /// <param name="model">Модель представления редактирования привязки терминала к кассовому окну на станции</param>
        /// <returns>Перенаправление на страницу EditTerminalToTellerWindowSuccess в случае валидности модели
        ///          и строго типизированное представление с формой редактирования привязки терминала к кассовому окну на станции</returns>
        [HttpPost]
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.Terminal)]
        public ActionResult EditTerminalToTellerWindow(TerminalToTellerWindowViewModel model)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();
            var tellerWindowRepository = new TellerWindowRepository();

            if (ModelState.IsValid)
            {
                tellerWindowTerminalRepository.UpdateFromModel(model);

                return RedirectToAction("EditTerminalToTellerWindowSuccess",
                                        new RouteValueDictionary(
                                            new
                                            {
                                                controller = "Station",
                                                action = "EditTerminalToTellerWindowSuccess",
                                                sectorId = model.SectorId,
                                                stationId = model.StationId,
                                                currentDate = model.CurrentDate
                                            }));
            }

            model.AllTellerWindowsNumbers = tellerWindowRepository.GetAllNumbersForStation(model.SectorId, model.StationId, model.TerminalProfile)
                                                                  .Select(a => a.ToSelectListItem())
                                                                  .ToList();

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + model.SectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + model.StationId;
            Session["CurrentDate"] = model.CurrentDate;

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешном создании привязки терминала к кассовому окну на станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Представление, извещающее об успешном создании привязки терминала к кассовому окну на станции</returns>
        [RoleObjectAuthGrok(UserActions.Add, DataObjects.Terminal)]
        public ActionResult AddTerminalToTellerWindowSuccess(int sectorId, int stationId, string currentDate)
        {
            ViewBag.SectorId = sectorId;
            ViewBag.StationId = stationId;
            ViewBag.CurrentDate = currentDate;

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View();
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешном изменении привязки терминала к кассовому окну на станции
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Представление, извещающее об успешном изменении привязки терминала к кассовому окну на станции</returns>
        [RoleObjectAuthGrok(UserActions.Update, DataObjects.Terminal)]
        public ActionResult EditTerminalToTellerWindowSuccess(int sectorId, int stationId, string currentDate)
        {
            ViewBag.SectorId = sectorId;
            ViewBag.StationId = stationId;
            ViewBag.CurrentDate = currentDate;

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View();
        }

        /// <summary>
        /// Метод. возвращающий представление редактирования срезов работы кассовых окон
        /// </summary>
        /// <param name="year">Год</param>
        /// <param name="seasonId">Идентификатор сезона среза</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <param name="returnUrl">Предыдущий URL</param>
        /// <returns>Представление редактирования срезов работы кассовых окон</returns>
        public ActionResult TellerWindowSchedulesEdit(int year, int seasonId, int stationId, int sectorId, string currentDate, string returnUrl)
        {
            var tellerWindowScheduleRepository = new TellerWindowScheduleRepository();
            var tellerWindowRepository = new TellerWindowRepository();
            var stationRepository = new StationRepository();

            var model = new TellerWindowSchedulesViewModel();
            model.Year = year;
            model.Season = tellerWindowScheduleRepository.GetSeasonById(seasonId);
            model.SectorStationId = stationRepository.GetSectorStationId(stationId, sectorId);
            model.ReturnUrl = returnUrl;

            model.HourTypes = tellerWindowScheduleRepository.GetHourTypes();
            model.TellerWindowScheduleDayTypes = tellerWindowScheduleRepository.GetTellerWindowScheduleDayTypes(year, seasonId, model.SectorStationId);
            model.TellerWindowNumbers = tellerWindowRepository.GetAllNumbersForStation(sectorId, stationId)
                                                              .Select(a => a.ToSelectListItem())
                                                              .ToList();

            model.TellerWindowNumbers.Insert(0, new SelectListItem()
                                                {
                                                    Value = "0",
                                                    Text = string.Empty
                                                });

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Stations + " " + sectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Station"] + stationId;
            Session["CurrentDate"] = currentDate;

            return View(model);
        }

        #region AJAX

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с кассовыми окнами станции
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="orderby">Параметры сортировки</param>
        /// <param name="filter">Параметры фильтрации</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Список кассовых окон станции и их количество в формате JSON</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.TellerWindow)]
        public ActionResult ReadCashBoxes(int page, int pageSize, string orderby, string filter, int sectorId, int stationId, string currentDate)
        {
            var tellerWindowRepository = new TellerWindowRepository();
            var filterDictionary = HttpUtility.ParseQueryString(filter);
            int itemsCount;

            var tellersWindows = tellerWindowRepository.Search(page, pageSize, orderby, filterDictionary, sectorId, stationId, out itemsCount);

            foreach (var tellersWindow in tellersWindows)
            {
                tellersWindow.EditUrl = Url.Action("EditTellerWindow", "Station",
                                                   new
                                                       {
                                                           sectorId = sectorId,
                                                           stationId = stationId,
                                                           currentDate = currentDate,
                                                           tellerWindowId = tellersWindow.Id
                                                       });
            }

            return Json(
                new
                {
                    Items = tellersWindows.ToArray(),
                    Count = itemsCount
                });
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с терминалами станции
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="orderby">Параметры сортировки</param>
        /// <param name="filter">Параметры фильтрации</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>        
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Список терминалов станции и их количество в формате JSON</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.Terminal)]
        public ActionResult ReadTerminals(int page, int pageSize, string orderby, string filter, int sectorId, int stationId, string currentDate)
        {
            var terminalRepository = new TerminalRepository();
            var filterDictionary = HttpUtility.ParseQueryString(filter);
            int itemsCount;

            var terminals = terminalRepository.Search(page, pageSize, orderby, filterDictionary, sectorId, stationId, out itemsCount);

            foreach (var terminal in terminals)
            {
                terminal.EditUrl = Url.Action("EditTerminal", "Station",
                                              new
                                                  {
                                                      sectorId = sectorId,
                                                      stationId = stationId,
                                                      currentDate = currentDate,
                                                      terminalId = terminal.Id,
                                                      sectorStationTerminalId = terminal.SectorStationTerminalId
                                                  });
            }

            return Json(new
                {
                    Items = terminals.ToArray(),
                    Count = itemsCount
                });
        }

        /// <summary>
        /// Обработчик ajax-запроса удаления кассового окна
        /// </summary>
        /// <param name="tellerWindowId">Идентификатор кассового окна</param>
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Delete, DataObjects.TellerWindow)]
        public ActionResult DeleteTellerWindow(int tellerWindowId)
        {
            var tellerWindowRepository = new TellerWindowRepository();

            try
            {
                if (!tellerWindowRepository.TellerWindowWithIdExists(tellerWindowId))
                {
                    return Json(new { status = JsonStatuses.Error, message = "Кассовое окно не найдено" });
                }

                tellerWindowRepository.DeleteTellerWindow(tellerWindowId);

                return Json(new { status = JsonStatuses.Ok, isActive = false });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса моделей терминалов, относящихся к указанному типу
        /// </summary>
        /// <param name="currentType">Тип терминалов</param>
        /// <returns>Список моделей терминалов, относящихся к указанному типу и статус успешного выполенения запроса
        ///          в случае успеха, статус ошибки и сообщение об ошибке в случае неудачи</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.Terminal)]
        public ActionResult GetTerminalTypeMarks(int currentType)
        {
            var terminalRepository = new TerminalRepository();

            try
            {
                var typeMarks = terminalRepository.GetTypeMarks(currentType);

                return Json(new { status = JsonStatuses.Ok, items = typeMarks.Select(tm => tm.ToSelectListItem()).ToList() });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }        

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с привязками терминалов к кассовым окнам станции
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="orderby">Параметры сортировки</param>
        /// <param name="sectorStationTerminalId">Идентификатор привязки терминала к станции, для которой ищутся привязки этого терминала к кассовым окнам станции</param>  
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции</param>      
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Список привязок терминалов к кассовым окнам станции и их количество в формате JSON</returns>
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.Terminal)]
        public ActionResult ReadTerminalInTellerWindow(int page, int pageSize, string orderby, int sectorStationTerminalId, int sectorId, int stationId, string currentDate)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();
            int itemsCount;

            var tellerWindowTerminals = tellerWindowTerminalRepository.Search(page, pageSize, orderby, sectorStationTerminalId,
                                                                              out itemsCount);

            foreach (var tellerWindowTerminal in tellerWindowTerminals)
            {
                tellerWindowTerminal.EditUrl = Url.Action("EditTerminalToTellerWindow", "Station",
                                              new
                                              {
                                                  sectorId = sectorId,
                                                  stationId = stationId,
                                                  currentDate = currentDate,
                                                  tellerWindowTerminalId = tellerWindowTerminal.Id
                                              });
            }

            return Json(new
            {
                Items = tellerWindowTerminals.ToArray(),
                Count = itemsCount
            });
        }

        /// <summary>
        /// Обработчик ajax-запроса удаления привязки терминала к кассовому окну
        /// </summary>
        /// <param name="tellerWindowTerminalId">Идентификатор привязки терминала к кассовому окну</param>
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [HttpPost]
        [RoleObjectAjaxAuthGrok(UserActions.Delete, DataObjects.Terminal)]
        public ActionResult DeleteTellerWindowTerminal(int tellerWindowTerminalId)
        {
            var tellerWindowTerminalRepository = new TellerWindowTerminalRepository();

            try
            {
                if (!tellerWindowTerminalRepository.TellerWindowTerminalWithIdExists(tellerWindowTerminalId))
                {
                    return Json(new { status = JsonStatuses.Error, message = "Привязка терминала к кассовому окну не найдена" });
                }

                tellerWindowTerminalRepository.DeleteTellerWindowTerminal(tellerWindowTerminalId);

                return Json(new { status = JsonStatuses.Ok, isActive = false });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }        

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid со сменами на станции
        /// </summary>
        /// <param name="stationId">Идентификатор станции</param>        
        /// <returns>Список смен на станции и их количество в формате JSON</returns>
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.StationEmployeeShiftSchedule)]
        [HttpPost]
        public ActionResult ReadEmployeeShifts(int stationId)
        {
            var stationRepository = new StationRepository();

            var shifts = stationRepository.GetAllShifts(stationId);
            
            return new JsonResult { Data = shifts };
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с расписанием смен 
        /// </summary>
        /// <param name="shiftId">Идентификатор смены</param>
        /// <returns>Список расписаний в смена и их количество в формате JSON</returns>
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.StationEmployeeShiftSchedule)]
        [HttpPost]
        public ActionResult ReadEmployeeShiftSchedule(int shiftId)
        {
            var stationRepository = new StationRepository();

            var schedules = stationRepository.GetAllShiftSchedule(shiftId);
            
            return new JsonResult { Data = schedules };
        }

        ///  <summary>
        ///  Сохраняет расписания смен на станции
        ///  </summary>
        /// <param name="endBreakTime"></param>
        /// <param name="stationId">Идентификатор станции</param>
        /// <param name="shiftScheduleId"></param>
        /// <param name="beginWorkTime"></param>
        /// <param name="endWorkTime"></param>
        /// <param name="beginBreakTime"></param>
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.StationEmployeeShiftSchedule)]
        [HttpPost]
        public ActionResult SetEmployeeShiftSchedule(int shiftScheduleId, string beginWorkTime, string endWorkTime, string beginBreakTime, string endBreakTime, int stationId)
        {
            var stationRepository = new StationRepository();

            stationRepository.SaveEmployeeShiftSchedule(shiftScheduleId, beginWorkTime, endWorkTime, beginBreakTime, endBreakTime, stationId);

            return new JsonResult { Data = "Ok" };
        }

        ///  <summary>
        ///  Создает смену на станции
        ///  </summary>
        /// <param name="stationId">Идентификатор станции</param>
        [RoleObjectAjaxAuthGrok(UserActions.Add, DataObjects.StationEmployeeShiftSchedule)]
        [HttpPost]
        public ActionResult CreateEmployeeShift(int stationId)
        {
            var stationRepository = new StationRepository();

            stationRepository.CreateEmployeeShift(stationId);

            return new JsonResult { Data = "Ok" };
        }

        ///  <summary>
        ///  Удаляет смену на станции
        ///  </summary>
        /// <param name="scheduleId">Идентификатор смены</param>
        [RoleObjectAjaxAuthGrok(UserActions.Delete, DataObjects.StationEmployeeShiftSchedule)]
        [HttpPost]
        public ActionResult DeleteEmployeeShift(int scheduleId)
        {
            var stationRepository = new StationRepository();

            stationRepository.DeleteEmployeeShift(scheduleId);

            return new JsonResult { Data = "Ok" };
        }

        ///  <summary>
        ///  Изменяет номер смены
        ///  </summary>
        /// <param name="scheduleId">Идентификатор смены</param>
        /// <param name="number">Номер смены</param>
        [RoleObjectAjaxAuthGrok(UserActions.Delete, DataObjects.StationEmployeeShiftSchedule)]
        [HttpPost]
        public ActionResult ChangeEmployeeShiftNumber(int scheduleId, int number)
        {
            var stationRepository = new StationRepository();

            stationRepository.ChangeEmployeeShiftNumber(scheduleId, number);

            return new JsonResult { Data = "Ok" };
        }
        #endregion

    }
}
