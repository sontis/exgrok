﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Bell.Grok.ExcelDAL;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.ViewModel;
using Bell.Grok.Business;
using Bell.Grok.Services.Exceptions;

namespace Bell.Grok.Web.Controllers
{    
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class ScheduleTemplateController : Controller
    {
        /// <summary>
        ///     Тип mime для json.
        /// </summary>
        private const string JsonFormat = "text/plain";

        /// <summary>
        /// Максимально допустимая длина имени загружаемого файла
        /// </summary>
        private const int MaxFileNameLength = 100;

        //
        // GET: /ScheduleTemplate/

        /// <summary>
        /// Метод, возвращающий форму ведения шаблонов графиков кассиров
        /// </summary>
        /// <returns>Представление с формой ведения шаблонов графиков кассиров</returns>
        [AuthGrok(Roles.OTZPEmployee)]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Метод, возвращающий частичное строготипизированное представление
        /// с формой создания шаблона графика кассиров
        /// </summary>
        /// <returns>Строго типизированное частичное представление с формой создания шаблона графика кассиров</returns>
        [AuthGrok(Roles.OTZPEmployee)]
        public ActionResult Create()
        {
            var directionRepository = new DirectionRepository();
            var sectorRepository = new SectorRepository();

            var model = new ScheduleTemplateViewModel();
            model.AllDirections =
                directionRepository.GetAllDirectionSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
            model.AllSectors =
                sectorRepository.GetAllSectorSelectedListItems().Select(a => a.ToSelectListItem()).ToList();

            return PartialView(model);
        }

        /// <summary>
        /// Метод, возвращающий частичное строготипизированное представление
        /// с формой редактирования шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <returns>Строго типизированное частичное представление с формой редактирования шаблона графика кассиров</returns>
        [AuthGrok(Roles.OTZPEmployee)]
        public ActionResult Edit(int scheduleTemplateId)
        {
            var directionRepository = new DirectionRepository();
            var sectorRepository = new SectorRepository();
            var scheduleTemplateRepository = new ScheduleTemplateRepository();

            var model = new ScheduleTemplateViewModel();
            if (!scheduleTemplateRepository.EditModelFromScheduleTemplate(model, scheduleTemplateId))
            {
                return HttpNotFound();
            }

            model.AllDirections = directionRepository.GetAllDirectionSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
            model.AllSectors = sectorRepository.GetAllSectorSelectedListItems().Select(a => a.ToSelectListItem()).ToList();

            return PartialView(model);
        }

        /// <summary>
        /// Метод, возвращающий строготипизированное представление
        /// с формой просмотра содержимого шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <returns>Строготипизированное представление с формой просмотра содержимого шаблона графика кассиров</returns>
        [AuthGrok(Roles.OTZPEmployee)]
        public ActionResult Review(int scheduleTemplateId)
        {
            var scheduleTemplateRepository = new ScheduleTemplateRepository();

            var model = new ScheduleTemplateReviewModel();
            model.ReturnUrl = Url.Action("Index", "ScheduleTemplate");
            if (!scheduleTemplateRepository.EditModelFromScheduleTemplate(model, scheduleTemplateId))
            {
                return HttpNotFound();
            }

            return View(model);
        }

        #region AJAX

        /// <summary>
        /// Метод, возвращающий результат создания нового шаблона графика кассиров и 
        /// запускающий парсинг excel-документа
        /// </summary>
        /// <param name="model">Модель представления создания шаблона графика кассиров</param>
        /// <param name="file">Таки бинарное представление excel-файла</param>
        /// <returns>Json-объект со статусом операции и список ошибок в случае невалидности модели</returns>
        [AjaxAuthGrok(Roles.OTZPEmployee)]
        [HttpPost]
        public ActionResult Create(ScheduleTemplateViewModel model, HttpPostedFileBase file)
        {
            var userRepository = new UserRepository();
            var scheduleTemplateRepository = new ScheduleTemplateRepository();
            var calendarRepository = new CalendarRepository();
            var excelDocumentParser = new ExcelDocumentParser();
            var validator = new ScheduleTemplateValidator();

            try
            {
                if (ModelState.IsValid)
                {
                    if (file.FileName.Length > MaxFileNameLength)
                    {
                        throw new Exception("Длина имени файла превышает 100 символов. Загрузка отменена!");
                    }

                    var authorUserId = userRepository.GetIdByLogin(User.Identity.Name);
                    var shortName = Path.GetFileNameWithoutExtension(file.FileName);

                    //Проверяем, загружены ли шаблоны из файла с таким именем
                    var shortNameErrorStatus = validator.ValidateShortName(shortName);
                    //Если да, то возвращаем ошибку
                    if (!string.IsNullOrWhiteSpace(shortNameErrorStatus))
                    {
                        ModelState.AddModelError(string.Empty, shortNameErrorStatus);
                        return Json(new { status = bool.FalseString, serverStatus = JsonStatuses.Invalid, errors = ModelState.ExtractValidationErrors() }, JsonFormat);
                    }

                    var calendars = calendarRepository.GetAll();
                    var schedulteTemplateModes = excelDocumentParser.Parse(file.InputStream, calendars);

                    if (schedulteTemplateModes != null && schedulteTemplateModes.Any())
                    {
                        scheduleTemplateRepository.UpdateFromModel(model, authorUserId, shortName, schedulteTemplateModes, User.Identity.Name);
                    }
                    else
                    {
                        throw new Exception("В документе отсутствуют режимы графиков кассиров!");
                    }

                    var isYearInCalendar = true;
                    if (schedulteTemplateModes.Any() &&
                        schedulteTemplateModes[0].ScheduleTemplateDetails.Any())
                    {
                        isYearInCalendar =
                            calendarRepository.IsYearInCalendar(
                                schedulteTemplateModes[0].ScheduleTemplateDetails[0].Year);
                    }

                    return Json(new { status = bool.TrueString, serverStatus = JsonStatuses.Ok, isValidYear = isYearInCalendar }, JsonFormat);
                }
                else
                {
                    return Json(new { status = bool.FalseString, serverStatus = JsonStatuses.Invalid, errors = ModelState.ExtractValidationErrors() }, JsonFormat);
                }
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);

                string exceptionMessage = e.InnerException != null
                                     ? e.InnerException.Message
                                     : e.Message;
                return Json(new { status = bool.FalseString, serverStatus = JsonStatuses.Error, message = exceptionMessage }, JsonFormat);
            }
        }

        /// <summary>
        /// Метод, возвращающий результат редактирования уже существующего шаблона графика кассиров
        /// </summary>
        /// <param name="model">Модель представления редактирования шаблона графика кассиров</param>
        /// <returns>Json-объект со статусом операции и список ошибок в случае невалидности модели</returns>
        [AjaxAuthGrok(Roles.OTZPEmployee)]
        [HttpPost]
        public ActionResult Edit(ScheduleTemplateViewModel model)
        {
            var scheduleTemplateRepository = new ScheduleTemplateRepository();
            var userRepository = new UserRepository();

            try
            {
                if (ModelState.IsValid)
                {
                    var lastAuthorUserId = userRepository.GetIdByLogin(User.Identity.Name);

                    scheduleTemplateRepository.UpdateFromModel(model, lastAuthorUserId, User.Identity.Name);

                    return Json(new { status = JsonStatuses.Ok });
                }
                else
                {
                    return Json(new { status = JsonStatuses.Invalid, errors = ModelState.ExtractValidationErrors() });
                }
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с шаблонами графиков кассиров
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="orderby">Параметры сортировки</param>
        /// <param name="filter">Параметры фильтрации</param>
        /// <returns>Список список шаблонов графиков кассиров и их количество в формате JSON</returns>
        [AjaxAuthGrok(Roles.OTZPEmployee)]
        [HttpPost]
        public ActionResult ReadScheduleTemplates(int page, int pageSize, string orderby, string filter)
        {
            var scheduleTemplateRepository = new ScheduleTemplateRepository();
            var filterDictionary = HttpUtility.ParseQueryString(filter);
            int itemsCount;

            var scheduleTemplates = scheduleTemplateRepository.Search(page, pageSize, orderby, filterDictionary, out itemsCount);
            scheduleTemplates.ForEach(
                s =>
                s.ReviewUrl = Url.Action("Review", "ScheduleTemplate", new { scheduleTemplateId = s.ScheduleTemplateId }));

            return Json(new
                {
                    Items = scheduleTemplates.ToArray(),
                    Count = itemsCount
                });
        }


        /// <summary>
        /// Обработчик ajax-запроса активизации шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [AjaxAuthGrok(Roles.OTZPEmployee)]
        [HttpPost]
        public ActionResult ActivateScheduleTemplate(int scheduleTemplateId)
        {
            var scheduleTemplateRepository = new ScheduleTemplateRepository();

            try
            {
                if (!scheduleTemplateRepository.ScheduleTemplateWithIdExists(scheduleTemplateId))
                {
                    return Json(new { status = JsonStatuses.Error, message = "Шаблон графика не найден" });
                }

                scheduleTemplateRepository.SetScheduleTemplateActivity(scheduleTemplateId, true, User.Identity.Name);

                return Json(new { status = JsonStatuses.Ok, isActive = true });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса деактивизации шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>        
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [AjaxAuthGrok(Roles.OTZPEmployee)]
        [HttpPost]
        public ActionResult DeactivateScheduleTemplate(int scheduleTemplateId)
        {
            var scheduleTemplateRepository = new ScheduleTemplateRepository();

            try
            {
                if (!scheduleTemplateRepository.ScheduleTemplateWithIdExists(scheduleTemplateId))
                {
                    return Json(new { status = JsonStatuses.Error, message = "Шаблон графика не найден" });
                }

                scheduleTemplateRepository.SetScheduleTemplateActivity(scheduleTemplateId, false, User.Identity.Name);

                return Json(new { status = JsonStatuses.Ok, isActive = false });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с режимом
        /// шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateModeId">Идентификатор режима шаблона графика кассиров</param>
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [AjaxAuthGrok(Roles.OTZPEmployee)]
        public ActionResult ReadScheduleTemplateDetails(int scheduleTemplateModeId)
        {
            var scheduleTemplateRepository = new ScheduleTemplateRepository();

            try
            {
                var scheduleTemplateReviewModeMonths =
                    scheduleTemplateRepository.GetScheduleTemplateDetailsForMode(scheduleTemplateModeId);

                //Формируем строку "Итого"
                var finalString = scheduleTemplateRepository.GetFinalString(scheduleTemplateReviewModeMonths);
                scheduleTemplateReviewModeMonths.Add(finalString);

                return Json(new { Items = scheduleTemplateReviewModeMonths });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с данными по указанному месяцу
        /// для первого режима шаблона графика кассиров
        /// </summary>
        /// <param name="scheduleTemplateId">Идентификатор шаблона графика кассиров</param>
        /// <param name="month">Номер месяца</param>
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [AjaxAuthGrok]
        public ActionResult ReadMonthScheduleTemplateDetails(int scheduleTemplateId, int month)
        {
            var scheduleTemplateRepository = new ScheduleTemplateRepository();

            try
            {
                var scheduleTemplateForMonth =
                    scheduleTemplateRepository.GetMonthScheduleTemplateDetails(scheduleTemplateId, month);

                return Json(new { Items = scheduleTemplateForMonth });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        #endregion

    }
}
