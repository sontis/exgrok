﻿using System.Configuration;
using System.Net;
using Bell.Grok.Business;
using Bell.Grok.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Bell.Grok.Services.Exceptions;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web.Controllers
{
    /// <summary>
    /// Контроллер для работы с кассирами
    /// </summary>    
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class CashierController : Controller
    {
        /// <summary>
        /// Имеет ли пользователь доступ только для чтения
        /// </summary>
        private bool ReadOnlyAccess
        {
            get
            {
                var currentUser = ControllerContext.HttpContext.User;
                return currentUser.IsInRole(Roles.OOPEmployee.ToText()) || currentUser.IsInRole(Roles.COEmployee.ToText()) ||
                       currentUser.IsInRole(Roles.OTZPEmployee.ToText()) || currentUser.IsInRole(Roles.ProfessionalGroupOrganizer.ToText());
            }
        }

        /// <summary>
        /// Получает форму кассира по его Id
        /// </summary>
        /// <param name="id">Id кассира</param>
        /// <param name="currentDate">Дата для фильтрации кассиров по времени их контракта</param>
        /// <returns>Карточку кассира</returns>                
        [AuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]   
        public ActionResult Index(int id, string currentDate)
        {
            var cashierSectorValidator = new CashierSectorValidator();
            if (!cashierSectorValidator.CheckAccessToCashier(User.Identity.Name, id))
            {
                return new HttpStatusCodeResult(403, "Access denied!");
            }

            var isForProduction = bool.Parse(ConfigurationManager.AppSettings["ForProduction"]);
            ViewData["ReadOnlyAccess"] = ReadOnlyAccess;
            if (isForProduction)
            {
                return new EmptyResult();
            }

            CashierRepository cashierRepository = new CashierRepository();
            PositionRepository positionRepository = new PositionRepository();
            var positions = positionRepository.GetAllPositions();
            ViewData["Positions"] = positions;

            SectorRepository sectorRepository = new SectorRepository();
            var sectors = sectorRepository.GetAllSectors();
            ViewData["Sectors"] = sectors;

            SpecializationRepository specializationRepository = new SpecializationRepository();
            var specializations = specializationRepository.GetAllSpecializations();
            ViewData["Specializations"] = specializations;

            ProfessionalLevelRepository professionalLevelRepository = new ProfessionalLevelRepository();
            var professionalLevels = professionalLevelRepository.GetAllProfessionalLevels();
            ViewData["ProfessionalLevels"] = professionalLevels;

            var boolDropDownDataSource = new SelectList(new[]
                                          {
                                              new{Value=true,Name="Да"},
                                              new {Value=false, Name="Нет"}                                                                                            
                                          },
                           "Value", "Name", false);

            ViewData["boolDropDownDataSource"] = boolDropDownDataSource;
            var result = new CashierViewModel();
            if (!cashierRepository.GetCashierById(result, id, currentDate))
            {
                TempData["Message"] = "Запрашиваемый каcсир не найден";
            }

            var stationRepository = new StationRepository();
            var stations = stationRepository.GetAllSectorStations(result.SectorId.HasValue 
                                                                        ? result.SectorId.Value
                                                                        : 0);

            ViewData["Stations"] = stations.Select(s => s.ToSelectListItem());

            result.CurrentDate = currentDate;            

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Cashiers + " " + result.SectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Cashier"] + result.EmployeeId;
            Session["CurrentDate"] = currentDate;
            
            return View(result);
        }

        /// <summary>
        /// Обновляет данные кассира
        /// </summary>
        /// <param name="model">Модель кассира</param>        
        /// <returns>Перенаправляет на представление по умолчанию</returns>
        [HttpPost]
        [AuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        public ActionResult Index(CashierViewModel model)
        {            
            ViewData["ReadOnlyAccess"] = ReadOnlyAccess;            
            PositionRepository positionRepository = new PositionRepository();
            var positions = positionRepository.GetAllPositions();
            ViewData["Positions"] = positions;

            SectorRepository sectorRepository = new SectorRepository();
            var sectors = sectorRepository.GetAllSectors();
            ViewData["Sectors"] = sectors;

            SpecializationRepository specializationRepository = new SpecializationRepository();
            var specializations = specializationRepository.GetAllSpecializations();
            ViewData["Specializations"] = specializations;

            ProfessionalLevelRepository professionalLevelRepository = new ProfessionalLevelRepository();
            var professionalLevels = professionalLevelRepository.GetAllProfessionalLevels();
            ViewData["ProfessionalLevels"] = professionalLevels;

            var boolDropDownDataSource = new SelectList(new[]
                                          {
                                              new{Value=true,Name="Да"},
                                              new {Value=false, Name="Нет"}                                                                                            
                                          },
                           "Value", "Name", false);

            ViewData["boolDropDownDataSource"] = boolDropDownDataSource;

            var stationRepository = new StationRepository();
            var stations = stationRepository.GetAllSectorStations(model.SectorId.HasValue
                                                                        ? model.SectorId.Value
                                                                        : 0);

            ViewData["Stations"] = stations.Select(s => s.ToSelectListItem());
            
            if (ModelState.IsValid)
            {
                CashierRepository repository = new CashierRepository();
                repository.UpdateCashier(model, model.CurrentFilterDate);

                TempData["Message"] = "Данные успешно сохранены";                
            }

            Session["ParentNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["SectorStructure"] + (int)SectorStructureTypes.Cashiers + " " + model.SectorId;
            Session["SelectedNode"] = UnifiedTreeIdPrefixDictionary.TreeIdCodes["Cashier"] + model.EmployeeId;
            Session["CurrentDate"] = model.CurrentDate;

            return View(model);
        }

        /// <summary>
        /// Получает все отпуска работника
        /// </summary>
        /// <param name="employeeId">Id работника</param>
        /// <returns>Список отпусков работника в виде JSON объекта</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]   
        public JsonResult GetEmployeeVacations(int employeeId)
        {
            VacationsRepository vacationRepository = new VacationsRepository();
            return Json(vacationRepository.GetEmployeeVacations(employeeId));
        }

        /// <summary>
        /// Создает новую запись об отпуске для работника
        /// </summary>
        /// <param name="model">Модель отпуска</param>
        /// <param name="employeeId">Id работника</param>
        /// <returns>Обновленную модель отпуска</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        public JsonResult CreateVacation(VacationModel model, int employeeId)
        {
            VacationsRepository vacationRepository = new VacationsRepository();
            var updatedModel = vacationRepository.CreateVacation(model);
            return Json(updatedModel);
        }

        /// <summary>
        /// Обработчик ajax-запроса исторической валидации отпусков кассира
        /// </summary>
        /// <param name="model">Объект модели VacationModel</param>
        /// <returns>Json-объект со статусом валидации и
        ///          сообщением в случае ошибки</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        public JsonResult ValidateVacation(VacationModel model)
        {
            var validator = new CashierVacationPeriodValidator();

            var beginDate = DateTime.Parse(model.BeginDate);
            var endDate =  !string.IsNullOrWhiteSpace(model.EndDate) 
                                ? DateTime.Parse(model.EndDate)
                                : DateTime.MaxValue;
            var cashierContractBeginDate = DateTime.Parse(model.CashierContractBeginDate);
            var cashierContractEndDate = !string.IsNullOrWhiteSpace(model.CashierContractEndDate)
                                             ? DateTime.Parse(model.CashierContractEndDate)
                                             : DateTime.MaxValue;

            var cashierContractIntervalStatus =
                validator.ValidateVacationIntervalWithEmployeeContractInterval(beginDate,
                                                                               endDate,
                                                                               cashierContractBeginDate,
                                                                               cashierContractEndDate,
                                                                               model.EmployeeId);

            if (!string.IsNullOrWhiteSpace(cashierContractIntervalStatus))
            {
                return Json(new {status = JsonStatuses.Invalid, message = cashierContractIntervalStatus});
            }

            var historyValidationStatus = validator.ValidateVacationPeriod(beginDate,
                                                                           endDate,
                                                                           model.EmployeeId);

            if (!string.IsNullOrWhiteSpace(historyValidationStatus))
            {
                return Json(new { status = JsonStatuses.Invalid, message = historyValidationStatus });
            }            

            return Json(new { status = JsonStatuses.Ok});
        }

        /// <summary>
        /// Удаляет запись об отпуске
        /// </summary>
        /// <param name="model">Модель отпуска</param>
        /// <returns></returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        public JsonResult DeleteVacation(VacationModel model)
        {
            VacationsRepository vacationRepository = new VacationsRepository();
            vacationRepository.DeleteVacation(model.VacationPeriodId);

            return null;
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с приоритетами станций сотрудника
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <returns>Json со списком приоритетов станций сотрудников</returns>
        [HttpPost]
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]   
        public JsonResult GetStationPriorities(int employeeId)
        {
            var stationPriorityRepository = new EmployeeStationPriorityRepository();

            var stationPriorities = stationPriorityRepository.Search(employeeId);

            return Json(new
                {
                    Items = stationPriorities,
                    Count = stationPriorities.Count()
                });         
        }

        /// <summary>
        /// Обработчик ajax-запроса повышения приоритета станции у сотрудника
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <param name="priorityLevel">Текущий уровень приоритета</param>
        /// <returns>Статус обработки ajax-запроса</returns>
        [HttpPost]
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        public JsonResult PickUpStationPriority(int employeeId, int priorityLevel)
        {
            var cashierRepository = new CashierRepository();

            try
            {
                if (!cashierRepository.PickUpStationPriority(employeeId, priorityLevel))
                {
                    return Json(new { status = JsonStatuses.Error, message = "Не найдены приоритеты для данного сотрудника" });
                }

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса понижения приоритета станции у сотрудника
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <param name="priorityLevel">Текущий уровень приоритета</param>
        /// <returns>Статус обработки ajax-запроса</returns>
        [HttpPost]
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        public JsonResult PickDownStationPriority(int employeeId, int priorityLevel)
        {
            var cashierRepository = new CashierRepository();

            try
            {
                if (!cashierRepository.PickDownStationPriority(employeeId, priorityLevel))
                {
                    return Json(new { status = JsonStatuses.Error, message = "Не найдены приоритеты для данного сотрудника" });
                }

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса изменения временных интервалов приоритетов
        /// </summary>
        /// <param name="model">Объект модели EmployeeWeekWorkTimePriorityModel, генерируется кастомным биндером</param>
        /// <returns>Статус обработки ajax-запроса</returns>
        [HttpPost]
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        public JsonResult UpdatePriorityIntervals(EmployeeWeekWorkTimePriorityModel model)
        {
            var employeeStationRepository = new EmployeeStationPriorityRepository();

            try
            {
                if (!ModelState.IsValid)
                {
                    var bindingErrors = ModelState[""].Errors;
                    var messages = "";
                    foreach (var bindingError in bindingErrors)
                    {
                        messages += "\n\t" + bindingError.ErrorMessage + "\n";
                    }

                    return Json(new { status = JsonStatuses.Invalid, message = messages });
                }

                employeeStationRepository.UpdateStationPriorityIntervals(model);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }            
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с временными интервалами для указанного приоритета станции
        /// </summary>
        /// <param name="stationPriorityId">Идентификатор приоритета станции</param>
        /// <returns>Json со списком временных интервалов для указанного приоритета станции</returns>
        [HttpPost]
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)]   
        public JsonResult GetTimeIntervalsForStationPriority(int stationPriorityId)
        {
            var stationPriorityRepository = new EmployeeStationPriorityRepository();

            var timeIntervalsForStationPriority =
                stationPriorityRepository.GetTimeIntervalsForStationPriority(stationPriorityId);

            return Json(new
            {                
                Items = timeIntervalsForStationPriority,
                Count = timeIntervalsForStationPriority.Count()
            });     
        }

        /// <summary>
        /// Обработчик ajax-запроса временных интервалов указанного приоритета станции
        /// для их отображения во всплывающем окне
        /// </summary>
        /// <param name="stationPriorityId">Идентификатор приоритета станции</param>
        /// <returns>Json со строкой, содержащей временный интервалы и предназначенной для десериализации</returns>
        [HttpPost]
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist, Roles.OOPEmployee, Roles.COEmployee, Roles.OTZPEmployee, Roles.ProfessionalGroupOrganizer)] 
        public JsonResult GetTimeIntervalsForWindow(int stationPriorityId)
        {
            var stationPriorityRepository = new EmployeeStationPriorityRepository();

            var timeIntervalsForStationPriorities =
                stationPriorityRepository.GetTimeIntervalsForWindow(stationPriorityId);

            if (timeIntervalsForStationPriorities.Any())
            {
                var stringForDeserialize = string.Empty;
                foreach (var timeIntervalsForStationPriority in timeIntervalsForStationPriorities)
                {
                    if (timeIntervalsForStationPriority.BeginTime.StartsWith("0"))
                    {
                        timeIntervalsForStationPriority.BeginTime = timeIntervalsForStationPriority.BeginTime.Remove(0, 1);                                                                                                                     
                    }

                    if (timeIntervalsForStationPriority.EndTime.StartsWith("0"))
                    {
                        timeIntervalsForStationPriority.EndTime = timeIntervalsForStationPriority.EndTime.Remove(0, 1);
                    }

                    stringForDeserialize +=
                        ((WeekDays) timeIntervalsForStationPriority.WeekDayNumber).BeginDateToString() +
                        "=" +
                        timeIntervalsForStationPriority.BeginTime +
                        "&" +
                        ((WeekDays) timeIntervalsForStationPriority.WeekDayNumber).EndDateToString() +
                        "=" +
                        timeIntervalsForStationPriority.EndTime +
                        "&";

                }

                stringForDeserialize = stringForDeserialize.Remove(stringForDeserialize.Length - 1);

                return Json(new {body = stringForDeserialize});
            }
            else
            {
                return Json(new {});
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса удаления всех временных интервалов приоритета станции
        /// для указанного приоритета станции
        /// </summary>
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [HttpPost]
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        public JsonResult DeleteIntervalsForPriority(int stationPriorityId)
        {
            var employeeStationPriorityRepository = new EmployeeStationPriorityRepository();

            try
            {
                employeeStationPriorityRepository.DeletePriorityIntervals(stationPriorityId);

                return Json(new { status = JsonStatuses.Ok, isActive = false });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }            
        }

        /// <summary>
        /// Обработчик запроса от выпадающего списка со станциями
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <returns>Список станций участка</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        [HttpPost]
        public JsonResult GetSectorStations(int sectorId)
        {
            var stationRepository = new StationRepository();

            try
            {
                var stations = stationRepository.GetAllSectorStations(sectorId)
                                                .Select(s => new SelectListItem()
                                                    {
                                                        Value = s.Key.ToString(),
                                                        Text = s.Value
                                                    });

                return Json(new { status = JsonStatuses.Ok, items = stations });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }  
        }

        /// <summary>
        /// Обработчик запроса от kendo-dropdownlist-а на получение списка станций, 
        /// для которых ещё не установлен приоритет у указанного работника
        /// </summary>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="employeeId">Идентификатор кассира</param>
        /// <returns>Json-объект со списком станций (идентификатор-название)</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        [HttpPost]
        public ActionResult GetStationsForPriority(int sectorId, int employeeId)
        {
            var stationRepository = new StationRepository();

            try
            {                
                var stations = stationRepository.GetStationsForPriority(sectorId, employeeId)
                                                .Select(s => new SelectListItem()
                                                    {
                                                        Value = s.Key.ToString(),
                                                        Text = s.Value
                                                    });                

                return Json(stations);
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса на создание приоритета станции для кассира
        /// </summary>
        /// <param name="model">Модель для создания приоритета станции для кассира</param>
        /// <returns>Json-объект со статусом завершения операции</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        [HttpPost]
        public JsonResult CreateStationPriority(EmployeeStationPriorityCreateModel model)
        {
            var employeeStationPriorityRepository = new EmployeeStationPriorityRepository();

            try
            {
                employeeStationPriorityRepository.CreateStationPriority(model);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик от kendo-grid-а с приоритетами станций кассира на редактирование
        /// </summary>
        /// <param name="model">Модель приоритета станции кассира</param>
        /// <returns>Http-статус выполнения запроса</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        [HttpPost]
        public HttpStatusCodeResult UpdateStationPriority(EmployeeStationPriorityGridModel model)
        {
            var employeeStationPriorityRepository = new EmployeeStationPriorityRepository();

            try
            {                
                employeeStationPriorityRepository.UpdateStationIntervalTransferTime(model);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Произошла ошибка при выполнении запроса");
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса на удаление приоритет станции (естественно вместе со связанными интервалами)
        /// </summary>
        /// <param name="employeePriorityId">Идентификатор приоритета станции</param>
        /// <returns>Json-объект со статусом завершения операции</returns>
        [AjaxAuthGrok(Roles.DirectionHead, Roles.SectorHead, Roles.SeniorTicketCashier, Roles.Technologist)]
        [HttpPost]
        public JsonResult DeleteStationPrioritet(int employeePriorityId)
        {
            var employeeStationPriorityRepository = new EmployeeStationPriorityRepository();

            try
            {
                employeeStationPriorityRepository.DeletePriority(employeePriorityId);

                return Json(new { status = JsonStatuses.Ok });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }
    }
}
