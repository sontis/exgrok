﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using Bell.Grok.Business;
using Bell.Grok.Web.Services;
using Bell.Grok.Web.ViewModel;
using Bell.Grok.Services.Logging;

namespace Bell.Grok.Web.Controllers
{
    /// <summary>
    /// Контроллер для авторизации
    /// </summary>
    public class AccountController : Controller
    {
        public AccountController()
        {
            
        }

        /// <summary>
        /// Метод, возвращающий представление с формой авторизации
        /// </summary>
        /// <returns>Строго типизированное представление с формой авторизации</returns>
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return GetDefaultRedirection(User.Identity.Name);
            }

            var model = new LoginViewModel { ReturnUrl = GetReturnUrl() };

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий стандартный путь для перенаправления в случае успешной авторизации
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        private ActionResult GetDefaultRedirection(string userName)
        {
            return RedirectToRoute("Home");
        }

        /// <summary>
        /// Метод, возвращающая путь для перенаправления из Url
        /// </summary>
        /// <returns></returns>
        private string GetReturnUrl()
        {
            return Request.QueryString["returnUrl"] ?? String.Empty;
        }

        //
        // POST: /Login

        /// <summary>
        /// Метод, обрабатывающий данные формы авторизации.
        /// В случае успеха создаёт билет проверки подлинности для указанного имени пользователя 
        /// и добавляет его к коллекции файлов Cookie ответа
        /// </summary>
        /// <param name="model">Модель представления страницы авторизации</param>
        /// <returns>Перенаправление по стандартному Url в случае валидности модели
        ///          и строго типизированное представление с формой авторизации
        ///          в противном случае</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {            
            if (User.Identity.IsAuthenticated)
            {
                return GetDefaultRedirection(User.Identity.Name);
            }

            var userRepository = new UserRepository();
            var userRoleRepository = new UserRoleRepository();

            if (ModelState.IsValid)
            {
                string errorMessage;

                if (!userRepository.UserWithLoginExists(model.Login))
                {
                    errorMessage = "Пользователь с данным именем не зарегистрирован";
                }
                else if (userRepository.IsUserBlocked(model.Login))
                {
                    errorMessage = "Пользователь удалён. Вход в систему невозможен";
                }                
                else if (!Membership.ValidateUser(model.Login, model.Password))
                {
                    errorMessage = "Неверный пароль";
                }
                else if (!userRoleRepository.IsUserHasRoles(model.Login))
                {
                    errorMessage = "На данного пользователя не назначена ни одна роль";
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(model.Login, model.RememberMe);                                

                    #region Logging
                    GrokLogger.LogAuthentication(model.Login, "Успешная аутентификация", true);
                    #endregion

                    if (!string.IsNullOrWhiteSpace(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }

                    return GetDefaultRedirection(model.Login);
                }

                #region Logging
                GrokLogger.LogAuthentication(model.Login, errorMessage);
                #endregion

                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    ModelState.AddModelError("", errorMessage);
                }
            }

            return View(model);
        }

        //
        // GET: /Logout
        /// <summary>
        /// Метод для разлогинирования пользователя.
        /// Удаляет из браузера билет проверки подлинности с помощью форм.
        /// </summary>
        /// <param name="returnUrl">Url для возврата</param>
        /// <returns>Перенаправление на стандартный Url</returns>
        public ActionResult Logout(string returnUrl)
        {
            FormsAuthentication.SignOut();            

            Session.Abandon();
            Session.Clear();             

            #region Logging
            GrokLogger.LogAuthenticationLogout(User.Identity.Name);
            #endregion

            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToRoute("Home");
        }

    }
}
