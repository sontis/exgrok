﻿using System.Collections;
using Bell.Grok.Business;
using Bell.Grok.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bell.Grok.Web.Controllers
{
    /// <summary>
    /// Контроллер для работы с терминалами
    /// </summary>
    public class TerminalController : Controller
    {
        private const string TimeFormat = "dd.MM.yyyy";

        /// <summary>
        /// Обработчик ajax-запроса информации о терминале с указанным номером
        /// </summary>
        /// <param name="number">Номер терминала</param>
        /// <returns>Json со статусом терминала и информацию о терминале
        ///          в случае, если терминал с указанным номером существует</returns>
        [HttpPost]
        public ActionResult GetTerminalInfo(string number, string beginDate, string endDate)
        {
            var _beginDate = DateTime.Parse(beginDate);
            var _endDate = string.IsNullOrWhiteSpace(endDate) ? DateTime.MaxValue : DateTime.Parse(endDate);

            var terminalValidator = new TerminalValidator();
            if (!terminalValidator.IsBeginDateLessEndDate(_beginDate, _endDate))
            {
                return Json(new { status = TerminalNumberValidationStatus.Invalid, message = "Дата ввода не может превышать дату вывода" });
            }

            var terminalRepository = new TerminalRepository();
            
            if (!terminalRepository.TerminalWithNumberExists(number))
            {
                return Json(new {status = TerminalNumberValidationStatus.Absent});
            }
            else
            {
                var historyValidationStatus = terminalValidator.ValidateNumber(number, _beginDate, _endDate, TimeFormat);

                if (string.IsNullOrWhiteSpace(historyValidationStatus))
                {
                    var terminalInforamation = terminalRepository.GetInformationAboutExistionTerminal(number);
                    return Json(new {status = TerminalNumberValidationStatus.Exists, body = terminalInforamation});
                }
                else
                {
                    return Json(new { status = TerminalNumberValidationStatus.Busy, message = historyValidationStatus });
                }
            }            
        }

    }
}
