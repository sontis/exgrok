﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Services.Exceptions;
using Bell.Grok.Web.Helpers;

namespace Bell.Grok.Web.Controllers
{
    public class ProceedsPlanController : Controller
    {
        private const string JsonFormat = "text/plain";

        #region AJAX

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с планами выручки на чтение 
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="orderby">Параметры сортировки</param>
        /// <param name="beginDate">Начальная дата (первое число начального месяца)</param>
        /// <param name="endDate">Конечная дата (первое число конечного месяца)</param>
        /// <param name="sectorId">Идентификатор участка</param>
        /// <param name="stationId">Идентификатор станции (глобальный без привязки к участку)</param>
        /// <returns>Список список планов выручки и их количество в формате JSON</returns>
        [RoleObjectAjaxAuthGrok(UserActions.Read, DataObjects.StationPlannedRevenue)]
        [HttpPost]
        public ActionResult ReadProceedsPlans(int page, int pageSize, string orderby, string beginDate, string endDate,
                                              int sectorId, int? stationId)
        {            
            var stationRepository = new StationRepository();
            var proceedsPlanRepository = new ProceedsPlanRepository();
                 
            try
            {
                DateTime firstTime, lastMonth;
                if (!DateTime.TryParse(beginDate, out firstTime))
                {
                    throw new Exception("Некорректное значение начального месяца интервала");
                }
                if (!DateTime.TryParse(endDate, out lastMonth))
                {
                    throw new Exception("Некорректное значение конечного месяца интервала");
                }
                var monthsList = DatesToMontsListConverter.ConvertDatesToMonthsList(firstTime, lastMonth);

                List<ProceedsPlanGridModel> plans;
                int itemsCount;

                //Если есть идентификатор станции
                if (stationId.HasValue)
                {
                    //Значит нужно сделать выборку планов выручки по станции
                    var sectorStationId = stationRepository.GetSectorStationId(stationId.Value, sectorId);                   
                    plans = proceedsPlanRepository.GetProceedPlansForPeriod(monthsList, sectorStationId, orderby);                    
                    itemsCount = plans.Count;                    
                }
                else
                {
                    //Иначе нужно сделать выборку планов выручки по участку
                    plans = proceedsPlanRepository.GetSectorProceedPlansForPeriod(monthsList, sectorId, orderby);
                    itemsCount = plans.Count;                   
                }                

                return Json(new
                {
                    Items = new PagedList<ProceedsPlanGridModel>(plans, page, pageSize, itemsCount),
                    Count = itemsCount
                });
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return Json(new { status = JsonStatuses.Error, message = e.Message }, JsonFormat);
            }
            
        }        

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с планами выручки на редактирование 
        /// </summary>
        /// <param name="model">Объект модели плана выручки</param>
        /// <returns>Http-статус выполнения запроса и идентификатор отредактированной записи
        ///          в случае успеха </returns>
        [RoleObjectAjaxAuthGrok(UserActions.Update, DataObjects.StationPlannedRevenue)]
        [HttpPost]
        public HttpStatusCodeResult UpdateProceedsPlan(ProceedsPlanGridModel model)
        {
            var proceedsPlanRepository = new ProceedsPlanRepository();

            try
            {
                var id = model.StationMonthProceedsId == -1
                                ? proceedsPlanRepository.AddProceedsPlan(model)
                                : proceedsPlanRepository.UpdateProceedsPlan(model);                

                return new HttpStatusCodeResult(HttpStatusCode.OK, id.ToString());
            }
            catch (Exception e)
            {
                AppExceptionManager.HandleException(e);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, e.Message);
            }                                        
        }        

        #endregion
    }
}
