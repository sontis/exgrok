﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace System.Web.Mvc
{
    /// <summary>
    /// Класс, содержащий обёртки для MapRoute, заточенный под admin area
    /// </summary>
    public static class AdminAreaRegistrationContextExtensions
    {
        public static void MapAdminRoute(this AreaRegistrationContext context, string name, string url, object defaults)
        {
            context.MapAdminRoute(name, url, defaults, null);
        }

        public static void MapAdminRoute(this AreaRegistrationContext context, string name, string url, object defaults, object constraints)
        {
            context.MapRoute(name, url, defaults, constraints, new string[] { "Bell.Grok.Web.Areas.Admin.Controllers" });
        }
    }
}