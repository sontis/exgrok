﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;
using Bell.Grok.Web.Resources;

namespace Bell.Grok.Web.Areas.Admin.ViewModel
{
    /// <summary>
    /// Класс-наследник модели представления создания пользователя на основе информации о сотруднике ЦППК.
    /// Необходим для использования атрибутов, находящихся в web-проекте
    /// </summary>
    public class UserCreateFromDictionaryViewModel : UserCreateFromDictionaryModel, IValidatableObject
    {
        public UserCreateFromDictionaryViewModel()
        {
            UserRole = new UserRoleViewModel();
        }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GROK))]
        [RegistrationLogin]
        [Remote("ValidateRegistrationLogin", HttpMethod = "POST")]      
        [RegularExpression(@"^[a-zA-Z\d]{1,20}$", ErrorMessage = "Логин должен включать не более 20 символов: цифр или латинских букв")]
        [Display(Name = "Логин")]
        public override string Login
        {
            get
            {
                return base.Login;
            }
            set
            {
                base.Login = value;
            }
        }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GROK))]
        [RegularExpression(@"^[a-zA-Z\d]{6,20}$", ErrorMessage = "Пароль должен включать не менее 6 и не более 20 символов: цифр или латинских букв")]
        [Display(Name = "Пароль")]
        public override string Password
        {
            get
            {
                return base.Password;
            }
            set
            {
                base.Password = value;
            }
        }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GROK))]
        [RegularExpression(@"^[a-zA-Z\d]{6,20}$", ErrorMessage = "Подтверждение пароля должно включать не менее 6 и не более 20 символов: цифр или латинских букв")]
        [Display(Name = "Подтверждение пароля")]
        public override string PasswordConfirmation
        {
            get
            {
                return base.PasswordConfirmation;
            }
            set
            {
                base.PasswordConfirmation = value;
            }
        }

        public new UserRoleViewModel UserRole { get; set; }

        /// <summary>
        /// Метод IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            if (!Password.Equals(PasswordConfirmation))
            {
                yield return new ValidationResult("Пароли не совпадают");
            }

            if (UserRole != null)
            {
                if ((UserRole.AccessLevelId >= 2) && (UserRole.DirectionIds == null))
                {
                    yield return new ValidationResult("Необходимо указать направление");
                }
                if ((UserRole.AccessLevelId >= 3) && (!UserRole.SectorId.HasValue))
                {
                    yield return new ValidationResult("Необходимо указать участок");
                }

            }
            else
            {
                yield return new ValidationResult("Необходимо добавить хотя бы одну роль");
            }
        }
    }
}