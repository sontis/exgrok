﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Web.Helpers.Validation;
using Bell.Grok.Web.Resources;

namespace Bell.Grok.Web.Areas.Admin.ViewModel
{
    public class UserCreateViewModel : UserViewModel
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GROK))]
        [RegistrationLogin]
        [Remote("ValidateRegistrationLogin", HttpMethod = "POST")]
        [RegularExpression(@"^[a-zA-Z\d]{1,20}$", ErrorMessage = "Логин должен включать не более 20 символов: цифр или латинских букв")]
        [Display(Name = "Логин")]
        public override string Login { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GROK))]
        [RegularExpression(@"^[a-zA-Z\d]{6,20}$", ErrorMessage = "Пароль должен включать не менее 6 и не более 20 символов: цифр или латинских букв")]
        [Display(Name = "Пароль")]
        public override string Password { get; set; }

        /// <summary>
        /// Подтверждение пароля пользователя
        /// </summary>
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(GROK))]
        [RegularExpression(@"^[a-zA-Z\d]{6,20}$", ErrorMessage = "Подтверждение пароля должно включать не менее 6 и не более 20 символов: цифр или латинских букв")]
        [Display(Name = "Подтверждение пароля")]
        public override string PasswordConfirmation { get; set; }
    }
}