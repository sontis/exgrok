﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;

namespace Bell.Grok.Web.Areas.Admin.ViewModel
{
    public class UserRoleViewModel : UserRoleModel
    {        
        /// <summary>
        /// Набор элементов для выпадающего списка ролей
        /// </summary>
        public List<SelectListItem> AllRoles { get; set; }

        /// <summary>
        /// Набор элементов для выпадающего списка направлений
        /// </summary>
        public List<SelectListItem> AllDirections { get; set; } 
    }
}