﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;
using Bell.Grok.Web.Resources;

namespace Bell.Grok.Web.Areas.Admin.ViewModel
{
    public class UserViewModel : UserModel, IValidatableObject
    {
        public UserViewModel()
        {
            UserRole = new UserRoleViewModel();
        }

        public new UserRoleViewModel UserRole { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Display(Name = "Имя")]
        [RegularExpression(@"^[a-zA-Zа-яА-Я\-\s\']{0,100}$", ErrorMessage = "Имя должно включать не более 100 строчных символов (букв, тире, пробелов и апострофов)")]
        [StringLength(100, ErrorMessage = "Имя должно содержать не более 100 символов")]
        [Required(ErrorMessage = "Поле {0} является обязательным для заполнения")]
        public override string EmployeeFirstName { get; set; }

        /// <summary>
        /// Отчество пользователя
        /// </summary>
        [Display(Name = "Отчество")]
        [RegularExpression(@"^[a-zA-Zа-яА-Я\-\s\']{0,100}$", ErrorMessage = "Отчество должно включать не более 100 строчных символов (букв, тире, пробелов и апострофов)")]
        [StringLength(100, ErrorMessage = "Отчество должно содержать не более 100 символов")]
        public override string EmployeeMiddleName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        [Display(Name = "Фамилия")]
        [RegularExpression(@"^[a-zA-Zа-яА-Я\-\s\']{0,100}$", ErrorMessage = "Фамилия должна включать не более 100 строчных символов (букв, тире, пробелов и апострофов)")]
        [StringLength(100, ErrorMessage = "Фамилия должна содержать не более 100 символов")]
        [Required(ErrorMessage = "Поле {0} является обязательным для заполнения")]
        public override string EmployeeLastName { get; set; }        

        /// <summary>
        /// Примечание пользователя
        /// </summary>
        [Display(Name = "Примечание")]
        public override string Description { get; set; }

        /// <summary>
        /// Метод IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            if (UserRole != null)
            {
                if ((UserRole.AccessLevelId >= 2) && (UserRole.DirectionIds == null))
                {
                    yield return new ValidationResult("Необходимо указать направление");
                }
                if ((UserRole.AccessLevelId >= 3) && (!UserRole.SectorId.HasValue))
                {
                    yield return new ValidationResult("Необходимо указать участок");
                }

            }
            else
            {
                yield return new ValidationResult("Необходимо добавить хотя бы одну роль");
            }
        }
        
    }
}