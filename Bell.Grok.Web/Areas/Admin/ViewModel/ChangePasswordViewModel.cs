﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;
using Bell.Grok.Web.Resources;

namespace Bell.Grok.Web.Areas.Admin.ViewModel
{
    /// <summary>
    /// Класс-наследник модели представления смены пароля
    /// Необходим для использования атрибутов, находящихся в web-проекте
    /// </summary>
    public class ChangePasswordViewModel : ChangePasswordModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.GROK))]
        [RegularExpression(@"^[a-zA-Z\d]{6,20}$", ErrorMessage = "Пароль должен включать не менее 6 и не более 20 символов: цифр или латинских букв")]
        [Display(Name = "Пароль")]
        public override string Password
        {
            get
            {
                return base.Password;
            }
            set
            {
                base.Password = value;
            }
        }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(Resources.GROK))]
        [RegularExpression(@"^[a-zA-Z\d]{6,20}$", ErrorMessage = "Подтверждение пароля должно включать не менее 6 и не более 20 символов: цифр или латинских букв")]
        [Display(Name = "Подтверждение пароля")]
        public override string PasswordConfirmation
        {
            get
            {
                return base.PasswordConfirmation;
            }
            set
            {
                base.PasswordConfirmation = value;
            }
        }
    }
}
