﻿using System.Web.Mvc;

namespace Bell.Grok.Web.Areas.Admin
{    
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Admin"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            //context.MapAdminRoute("Admin_Users", "admin/users", new { controller = "Users", action = "List" });            
            context.MapAdminRoute("AdminList", "admin/list", new { controller = "Users", action = "List"});            
            context.MapAdminRoute("AdminListRead", "admin/list/read", new { controller = "Users", action = "Read" });
            context.MapAdminRoute("AdminListEmployeesRead", "admin/employees-list/read", new { controller = "Users", action = "ReadEmployees" });
            context.MapAdminRoute("AdminCreateFromDictionary", "admin/createFromDictionary", new { controller = "Users", action = "CreateFromDictionary"});
            context.MapAdminRoute("AdminCreate", "admin/create", new { controller = "Users", action = "Create" });
            context.MapAdminRoute("AdminEdit", "admin/edit", new { controller = "Users", action = "Edit" });
            context.MapAdminRoute("AdminChangePassword", "admin/changePassword", new { controller = "Users", action = "ChangePassword" });
            context.MapAdminRoute("AdminUserCreateSuccess", "admin/userCreateSuccess", new { controller = "Users", action = "CreateSuccess"});
            context.MapAdminRoute("AdminUserEditSuccess", "admin/userEditSuccess", new { controller = "Users", action = "EditSuccess" });
            context.MapAdminRoute("AdminUserChangePasswordSuccess", "admin/userChangePasswordSuccess", new { controller = "Users", action = "ChangePasswordSuccess" });
            context.MapAdminRoute("AdminUserActive", "admin/userActive", new { controller = "Users", action = "Active"});
            context.MapAdminRoute("AdminUserBlock", "admin/userBlock", new { controller = "Users", action = "Block"});
            context.MapAdminRoute("AdminCommon", "admin/{action}", new { controller = "Users" });
        }
    }
}