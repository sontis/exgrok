﻿$(function () {
    var mod = Controller.instance;

    var KendoTable = mod.create(KendoTableModule);
    var UserBlock = mod.create(UserBlockModule);

    var kendoTable = new KendoTable({ el: $("#main") });
    var userBlock = new UserBlock({ el: $("#users-grid") });

    kendoTable.initTable();
});

//Модуль, реализующий блокировку пользователей
var UserBlockModule = {
    elements: {
        "input[name=user-block]": "userBlockButton",
        "input[name=user-active]": "userActiveButton",
        "#active-url": "activeUrl",
        "#block-url": "blockUrl"
    },

    events: {
        "click userBlockButton": "userBlockButtonHandler",
        "click userActiveButton": "userActiveButtonHandler"
    },

    //Функция блокировки пользователя
    userBlockButtonHandler: function (e) {
        var gridSelector = this.el.selector;
        var gridDataSource = $(gridSelector).data("kendoGrid");
        var dataItem = gridDataSource.dataItem($(e.currentTarget).closest("tr"));
        var blockUrl = $(this.blockUrl.selector).attr('data-url');

        if (confirm('Вы действительно хотите заблокировать пользователя ' + dataItem.Login + '?')) {
            $.post(blockUrl, { userId: dataItem.UserId }, function (result) {
                if (result.status === JsonStatus.ok) {
                    dataItem.IsActive = result.isActive;
                    gridDataSource.refresh();
                    Utils.Notification.show('Пользователь ' + dataItem.Login + ' заблокирован');
                }

                if (result.status === JsonStatus.error) {
                    alert(result.message);
                }
            })
                .error(function () {
                    alert("get actions error");
                });
        }
    },

    //Функция разблокировки пользователя
    userActiveButtonHandler: function (e) {
        var gridSelector = this.el.selector;
        var gridDataSource = $(gridSelector).data("kendoGrid");
        var dataItem = gridDataSource.dataItem($(e.currentTarget).closest("tr"));
        var activeUrl = $(this.activeUrl.selector).attr('data-url');

        if (confirm('Вы действительно хотите активировать пользователя ' + dataItem.Login + '?')) {
            $.post(activeUrl, { userId: dataItem.UserId }, function (result) {
                if (result.status === JsonStatus.ok) {
                    dataItem.IsActive = result.isActive;
                    gridDataSource.refresh();
                    Utils.Notification.show('Пользователь ' + dataItem.Login + ' активирован');
                }

                if (result.status === JsonStatus.error) {
                    alert(result.message);
                }
            })
                .error(function () {
                    alert("get actions error");
                });
        }
    }
};

//Модуль kendo-таблицы
var KendoTableModule = {
    elements: {
        "#users-grid": "usersGrid"
    },

    events: {

    },

    initActiveFilter: function (filter) {
        if (filter.filters) {
            for (var i = 0; i < filter.filters.length; i++) {
                this.initActiveFilter(filter.filters[i]);
            }
        } else {
            $('th[data-field=' + filter.field + '] a.k-grid-filter').addClass('k-state-active');
        }
    },

    //Функция, инициализирующая таблицу
    initTable: function () {
        var usersGridSelector = this.trimParentSelector(this.usersGrid.selector);

        var dataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            sort: { field: "Login", dir: "asc" },
            pageSize: 10,
            transport: {
                read: {
                    url: $("#users-grid").attr("data-url"),
                    dataType: "json",
                    type: "POST",
                    contentType: 'application/json; charset=utf-8'
                },
                parameterMap: function (options) {
                    return JSON.stringify(options);
                }
            },
            schema: {
                model: {
                    id: "EmployeeId",
                    fields: {
                        EditUrl: {},
                        ChangePasswordUrl: {},
                        FirstName: {},
                        MiddleName: {},
                        LastName: {},
                        Role: {},
                        Login: {},
                        Description: {},
                    }
                },
                data: "Items",
                total: "Count"
            },
            error: function () {
                if (!window.unloading) {
                    alert('При получении данных возникла ошибка');
                }
            }
        });

        $(usersGridSelector).kendoGrid({
            autoBind: false,
            dataSource: dataSource,
            filterable: {
                extra: false,
                operators: {
                    string: {
                        contains: "содержит"
                    }
                },
                messages: {
                    info: "Элементы со значением",
                    filter: "Применить",
                    clear: "Сбросить"
                }
            },
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            pageable: {
                messages: {
                    display: "{0} - {1} из {2}",
                    empty: "Нет элементов для отображения",
                    page: "Страница",
                    of: "из {0}",
                    itemsPerPage: "элементов на страницу",
                    first: "К первой странице",
                    previous: "К предыдущей странице",
                    next: "К следующей странице",
                    last: "К последней странице",
                    refresh: "Обновить"
                }
            },
            dataBound: function (e) {
                $('tr:has(input[name=user-active])').each(function () {
                    var colorParts = {};
                    colorParts['red'] = '255';
                    colorParts['green'] = '0';
                    colorParts['blue'] = '0';
                    colorParts['transparent'] = this.className === 'k-alt' ? '0.2' : '0.1';

                    var blockedColor = "rgba(" +
                        colorParts['red'] + ", " +
                        colorParts['green'] + ", " +
                        colorParts['blue'] + ", " +
                        colorParts['transparent'] + ")";
                    $(this).css('background-color', blockedColor);
                });

                $('tr:has(input[name=user-active])').attr('title', 'Пользователь заблокирован');
                $('tr:has(input[name=user-block])').attr('title', 'Пользователь активен');

                //Сохраняем в сессию текущее состояние грида
                var currentState = kendo.stringify({
                    page: dataSource.page(),
                    pageSize: dataSource.pageSize(),
                    sort: dataSource.sort(),
                    group: dataSource.group(),
                    filter: dataSource.filter()
                });

                $.session.set('CurrentGridState', currentState);
            },
            columns: [
                { template: kendo.template($('#editColumn').html()), filterable: false, sortable: false },
                { field: "FirstName", title: "Имя" },
                { field: "MiddleName", title: "Отчество" },
                { field: "LastName", title: "Фамилия" },
                { field: "Role", title: "Роль", filterable: false },
                { field: "Login", title: "Логин", filterable: false },
                { field: "Description", title: "Примечание", filterable: false, sortable: false },
                { title: "Статус", template: kendo.template($('#blockColumn').html()), filterable: false, sortable: false }]
        }).data("kendoGrid");

        //Восстанавливаем из сессии состояние грида.
        //Если в сессии ничего нет, то грузим грид по умолчанию
        var grid = $(usersGridSelector).data("kendoGrid");
        var gridStateFromSession = $.session.get('CurrentGridState');
        if (gridStateFromSession !== undefined) {
            var initialState = JSON.parse(gridStateFromSession);
            if (initialState) {
                if (initialState.filter) {
                    this.initActiveFilter(initialState.filter);
                }
                grid.dataSource.query(initialState);
            } else {
                grid.dataSource.read();
            }
        } else {
            grid.dataSource.read();
        }
    }
};