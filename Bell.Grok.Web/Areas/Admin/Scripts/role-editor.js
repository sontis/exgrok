﻿var sectorsDataSource = new kendo.data.DataSource({
    transport: {
        read: function (operation) {
            var directionIds = $("select[name='UserRole.DirectionIds']").val();

            if (directionIds) {
                var directionId = directionIds[0];

                $.ajax({
                    async: false,
                    url: $("#GetSectorsUrl").attr("data-url"),
                    dataType: "json",
                    data: {
                        directionId: directionId
                    },
                    success: function (response) {
                        operation.success(response);
                    },
                    error: function (e) {
                        if (!window.unloading) {
                            alert('get actions error');
                        }
                    }
                });
            } else {
                operation.success([]);
            }
        }
    }
});

$(function () {
    var roleDdModel = kendo.observable({            
        sectorsSource: sectorsDataSource,

        role: null, 
        directions: [], 
        sector: null, 

        //Вычисляемое свойство модели - уровень доступа
        accessLevelId: function (e) {
            var roleId = this.get('role');
            if (roleId) {
                var roleNumberId = parseInt(roleId);
                if (roleNumberId === 5 || roleNumberId === 6 || roleNumberId === 7 || roleNumberId === 8) {
                    return 1;
                }
                if (roleNumberId === 1) {
                    return 2;
                }
                if (roleNumberId === 2 || roleNumberId === 3 || roleNumberId === 4 || roleNumberId === 9) {
                    return 3;
                }
            }

            return 99;
        },

        //Доступен ли список с направлениями
        isDirectionsDisabled: function (e) {
            var accessLevelId = this.accessLevelId(); 
            var isDisabled = accessLevelId <= 1;
            if (isDisabled) this.set("directions", '');

            return isDisabled;
        },

        //Доступен ли список с участками
        isSectorsDisabled: function (e) {
            var accessLevelId = this.accessLevelId();
            var directions = this.get('directions');
            var isNoDirections = !directions || directions.length === 0;
            var isDisabled = (accessLevelId <= 2) || (isNoDirections);
            if (isDisabled) this.set("sector", '');
            return isDisabled;
        },

        //Обработчик изменения значения списка направлений
        directionsChanged: function (data) {
            var accessLevelId = this.accessLevelId(); 

            if (accessLevelId === 3) {                   
                var sectorsSource = this.get("sectorsSource");
                sectorsSource.read();
            }
        },
            
        //Обработчик прихода списка секторов
        sectorsDataBound: function(e) {
            var sectorId = $("#SectorIdValue").attr("data-sector-id-value");
            if (sectorId !== "") {
                var sectorNum = parseInt(sectorId);
                if (sectorNum !== NaN) {
                    this.set("sector", sectorNum);
                }
            }
        },
            
        //Проверка уровня доступа - если уровень доступа "Участок", то можно выбрать только одно направление
        roleLevelChanged: function(e) {
            var accessLevelId = this.accessLevelId();
            var selectedDirectionsOptions = $("select[name='UserRole.DirectionIds']").data("kendoMultiSelect").options;
                
            if (accessLevelId === 3) {
                selectedDirectionsOptions.maxSelectedItems = 1;
            } else {
                selectedDirectionsOptions.maxSelectedItems = null;
            }
        },

        //Функция очистки списк направлений
        clearDirections: function (e) {                
            this.set("directions", []);                
        }            

    });

    roleDdModel.role = $("select[name='UserRole.RoleId']").val();
    roleDdModel.directions = $("select[name='UserRole.DirectionIds']").val();
    roleDdModel.sector = $("select[name='UserRole.SectorId']").val();        

    kendo.bind($('#roleSelector'), roleDdModel);
        
    //Если уровень доступа роли - участок, то ограничиваем количество выбранных направлений (только одно)
    if (roleDdModel.accessLevelId() === 3) {
        $("select[name='UserRole.DirectionIds']").data("kendoMultiSelect").options.maxSelectedItems = 1;
    }
        
});                