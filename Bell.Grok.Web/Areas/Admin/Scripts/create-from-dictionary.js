﻿$(function () {
    var mod = Controller.instance;

    var AdminList = mod.create(AdminListPage);
    var KendoWindow = mod.create(KendoWindowModule);
    var KendoTable = mod.create(KendoTableModule);
    var ViewBagParams = mod.create(ViewBagParamsModule);

    var adminList = new AdminList({ el: $("#main") });
    var kendoWindow = new KendoWindow({ el: $("#employee-dictionary-popup") });
    var kendoTable = new KendoTable({ el: $("#search-params") });
    var viewBagParams = new ViewBagParams({ el: $("#viewbag-params") });

    $.subscribe("addFromDictionaryEvent", kendoWindow.showWindow, adminList);
    $.subscribe("windowOpenEvent", kendoTable.initTable, adminList);
    $.subscribe("chooseEmployeeEvent", adminList.showUserAttributes, adminList);
    $.subscribe("initPageEvent", viewBagParams.checkAfterErrorDiv, viewBagParams);
    $.subscribe("findButtonEvent", kendoTable.updateTable, adminList);

    adminList.initPage();
});

//Модуль проверки параметров ViewBag
var ViewBagParamsModule = {
    elements: {
        "#is-after-error": "isAfterErrorDiv"
    },

    events: {

    },

    //Функция, проверяющая, загружено ли представление с валидной моделью
    checkAfterErrorDiv: function (topic, data) {
        var isAfterErrorDivSelector = this.trimParentSelector(this.isAfterErrorDiv.selector);

        if ($(isAfterErrorDivSelector).attr("data-is-after-error").toLowerCase() === "true") {
            $(data.addFromDictionaryButtonSelector).attr('disabled', 'disabled');
            $(data.addFromDictionaryButtonSelector).addClass('k-state-disabled');

            $(data.submitButtonSelector).removeAttr('disabled');
            $(data.submitButtonSelector).removeClass('k-state-disabled');
        } else {
            data.addFromDictionaryHandler();
        }
    }
};

//Модель страницы
var AdminListPage = {
    elements: {
        "#add-from-dictionary": "addFromDictionaryButton",
        "#employee-dictionary-popup": "employeeDictionaryWindow",
        "#employee-dictionary-grid": "employeeDictionaryGrid",
        "#choose-employee": "chooseEmployeeButton",
        "#EmployeeId": "employeeIdId",
        "#EmployeeNumber": "employeeNumberId",
        "#EmployeeFirstName": "employeeFirstNameId",
        "#EmployeeMiddleName": "employeeMiddleNameId",
        "#EmployeeLastName": "employeeLastNameId",
        "#new-user": "newUserWindow",
        ":submit": "submitButton",
        "#search-params": "searchParamsDiv",
        "#Description": "userDescription"
    },

    events: {
        "click addFromDictionaryButton": "addFromDictionaryHandler",
        "input EmployeeNumberId": "changeEmployeeNumberIdHandler"
    },

    //Обработчик нажатия на кнопку вызова окна, испускает пользовательское событие для других модулей
    addFromDictionaryHandler: function () {
        $.publish("addFromDictionaryEvent");
    },

    //Функция, делающая видимыми контролы для редактирования атрибутов пользователя
    showUserAttributes: function () {
        var employeeDictionaryWindowSelector = this.trimParentSelector(this.employeeDictionaryWindow.selector),
            employeeIdSelector = this.trimParentSelector(this.employeeIdId.selector),
            employeeNumberSelector = this.trimParentSelector(this.employeeNumberId.selector),
            employeeFirstNameSelector = this.trimParentSelector(this.employeeFirstNameId.selector),
            employeeMiddleNameSelector = this.trimParentSelector(this.employeeMiddleNameId.selector),
            employeeLastNameSelector = this.trimParentSelector(this.employeeLastNameId.selector),
            employeeGridItemSelector = this.trimParentSelector(this.employeeDictionaryGrid.selector),
            userDescriptionSelector = this.trimParentSelector(this.userDescription.selector),
            submitSelector = this.trimParentSelector(this.submitButton.selector);

        var kendoGridSelectedItem = $(employeeGridItemSelector).data("kendoGrid").select();
        var currentDataItem = $("#employee-dictionary-grid").data("kendoGrid").dataItem(kendoGridSelectedItem);

        $(employeeDictionaryWindowSelector).data("kendoWindow").close();

        $(submitSelector).removeAttr('disabled');
        $(submitSelector).removeClass('k-state-disabled');

        $(employeeIdSelector).attr('value', currentDataItem.EmployeeId);
        $(employeeNumberSelector).attr('value', currentDataItem.EmployeeNumber);
        $(employeeFirstNameSelector).attr('value', currentDataItem.EmployeeFirstName);
        $(employeeMiddleNameSelector).attr('value', currentDataItem.EmployeeMiddleName);
        $(employeeLastNameSelector).attr('value', currentDataItem.EmployeeLastName);

        $(userDescriptionSelector).text(currentDataItem.Description);
    },

    //Функция, инициализирующая страницу
    initPage: function () {
        $.publish("initPageEvent", {
            submitButtonSelector: this.trimParentSelector(this.submitButton.selector),
            addFromDictionaryButtonSelector: this.trimParentSelector(this.addFromDictionaryButton.selector),
            addFromDictionaryHandler: this.addFromDictionaryHandler
        });
    }
};

//Модуль kendo-window
var KendoWindowModule = {
    elements: {
        "#choose-employee": "chooseEmployeeButton"
    },

    events: {
        "click chooseEmployeeButton": "chooseEmployeeHandler"
    },

    //Функция, запускающая kendo-window
    showWindow: function () {
        var chooseButtonSelector = this.trimParentSelector(this.chooseEmployeeButton.selector),
            windowSelector = this.trimParentSelector(this.employeeDictionaryWindow.selector);

        if (!$(windowSelector).data("kendoWindow")) {
            $(windowSelector).show();
            $(windowSelector).kendoWindow({
                width: "900px",
                height: "650px",
                title: "Сотрудники",
                modal: true
            });
        } else {
            $(windowSelector).data("kendoWindow").open();
        }

        $(chooseButtonSelector).attr('disabled', 'disabled');
        $.publish("windowOpenEvent");
    },

    //Обработчик кнопки фиксации выбора сотрудника, испускает пользовательское событие для уведомления других модулей
    chooseEmployeeHandler: function () {
        $.publish("chooseEmployeeEvent");
    }
};

//Модуль kendo-grid
var KendoTableModule = {
    elements: {
        "#find-button": "findButton"
    },

    events: {
        "click findButton": "findButtonHandler"
    },

    //Обработчик нажатия на кнопку фильтрации сотрудников, испускает пользовательское событие для уведомления других модулей
    findButtonHandler: function () {
        $.publish("findButtonEvent");
    },

    //Функция, обновляющая таблицу
    updateTable: function () {
        var employeeDictionaryGridSelector = this.trimParentSelector(this.employeeDictionaryGrid.selector),
            data = $(employeeDictionaryGridSelector).data("kendoGrid").dataSource;

        data.page(1);
    },

    //Функция, инициализирующая таблицу
    initTable: function () {
        var employeeDictionaryGridSelector = this.trimParentSelector(this.employeeDictionaryGrid.selector),
            chooseButtonSelector = this.trimParentSelector(this.chooseEmployeeButton.selector),
            searchParamSelector = this.trimParentSelector(this.searchParamsDiv.selector);

        var dataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            serverSorting: true,
            sort: { field: "EmployeeNumber", dir: "asc" },
            pageSize: 10,
            change: function () {
                $(chooseButtonSelector).addClass('k-state-disabled');
                $(chooseButtonSelector).attr('disabled', 'disabled');
            },
            transport: {
                read: {
                    url: $(employeeDictionaryGridSelector).attr("data-url"),
                    dataType: "json",
                    type: "POST"
                },
                parameterMap: function (options) {
                    return {
                        page: options.page,
                        pageSize: options.pageSize,
                        orderby: (options.sort.length > 0) ? options.sort[0].field + ' ' + options.sort[0].dir : "",
                        filter: $(searchParamSelector).serialize()
                    };
                }
            },
            schema: {
                model: {
                    id: "EmployeeId",
                    fields: {
                        EmployeeId: {},
                        EmployeeNumber: {},
                        EmployeeFirstName: {},
                        EmployeeMiddleName: {},
                        EmployeeLastName: {},
                        Sector: {},
                        Position: {},
                        Description: {}
                    }
                },
                data: "Items",
                total: "Count"
            },
            error: function () {
                if (!window.unloading) {
                    alert('При получении данных возникла ошибка');
                }
            }
        });

        $(employeeDictionaryGridSelector).kendoGrid({
            scrollable: false,
            dataSource: dataSource,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            selectable: "row",
            change: function () {
                $(chooseButtonSelector).removeClass('k-state-disabled');
                $(chooseButtonSelector).removeAttr('disabled');
            },
            pageable: {
                messages: {
                    display: "{0} - {1} из {2}",
                    empty: "Нет элементов для отображения",
                    page: "Страница",
                    of: "из {0}",
                    itemsPerPage: "элементов на страницу",
                    first: "К первой странице",
                    previous: "К предыдущей странице",
                    next: "К следующей странице",
                    last: "К последней странице",
                    refresh: "Обновить"
                }
            },
            columns: [
                { field: "EmployeeNumber", title: "Табельный номер" },
                { field: "EmployeeFirstName", title: "Имя" },
                { field: "EmployeeMiddleName", title: "Отчество" },
                { field: "EmployeeLastName", title: "Фамилия" },
                { field: "Sector", title: "Производственный участок" },
                { field: "Position", title: "Должность" }]
        }).data("kendoGrid");
    }
};