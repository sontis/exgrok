﻿$(function () {
    var areNameFiledsReadonly = $("#are-name-fileds-readonly").attr("data-readonly").toLowerCase() === "true";

    if (areNameFiledsReadonly) {
        $("#named-fields :text,textarea").attr("readonly", "readonly");
    }
});