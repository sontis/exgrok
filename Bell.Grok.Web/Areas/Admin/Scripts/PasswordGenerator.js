﻿function generatePassword() {
    var length = 6,
        charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    
    $('.passwordInput').val(retVal);    
}

function showPassword() {
    var inputs = $('.passwordInput');
    if (inputs.attr('type') == 'password') {
        inputs.attr('type', 'text');
    }
    else {
        inputs.attr('type', 'password');
    }
}