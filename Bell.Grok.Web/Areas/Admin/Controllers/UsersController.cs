﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

using Bell.Grok.Business;
using Bell.Grok.Business.Extensions;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.Areas.Admin.ViewModel;
using Roles = Bell.Grok.Model.Roles;

namespace Bell.Grok.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Контроллер для работы с пользователями
    /// </summary>    
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UsersController : Controller
    {
        /// <summary>
        /// Список ролей, получается только один раз
        /// </summary>
        private static List<RoleModel> _roles = new RoleRepository().GetAllRoles();
        
        public UsersController()
        {            
        }        

        /// <summary>
        /// Метод, возвращающий представление со списком пользователей
        /// </summary>
        /// <returns>Строго типизированное представление со списком пользователей</returns>
        [AuthGrok(Roles.Administrator)]
        public ActionResult List()
        {
            return View();
        }

        /// <summary>
        /// Метод, возвращающий представление создания пользователя на основе информации о сотруднике ЦППК
        /// </summary>
        /// <returns>Строго типизированное представление с формой создания пользователя на основе информации о сотруднике ЦППК</returns>
        [AuthGrok(Roles.Administrator)]
        public ActionResult CreateFromDictionary()
        {
            var directionRepository = new DirectionRepository();
            var roleRepository = new RoleRepository();

            var model = new UserCreateFromDictionaryViewModel();                        
            model.UserRole.RoleId = _roles.First().RoleId;

            model.UserRole.AllRoles = roleRepository.GetAllRoleSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
            model.UserRole.AllDirections = directionRepository.GetAllDirectionSelectedListItems().Select(a => a.ToSelectListItem()).ToList();                       

            ViewBag.IsAfterError = false;
            return View(model);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы создания пользователя на основе информации о сотруднике ЦППК
        /// </summary>
        /// <param name="model">Модель представления создания пользователя на основе информации о сотруднике ЦППК</param>
        /// <returns>Перенаправление на страницу CreateSuccess в случае валидности модели
        ///          и строго типизированное представление с формой создания пользователя на основе информации о сотруднике ЦППК
        ///          в противном случае</returns>
        [AuthGrok(Roles.Administrator)]
        [HttpPost]
        public ActionResult CreateFromDictionary(UserCreateFromDictionaryViewModel model)
        {
            var userRepository = new UserRepository();            
            var directionRepository = new DirectionRepository();
            var roleRepository = new RoleRepository();
            
            if (ModelState.IsValid)
            {
                MembershipCreateStatus status;
                Membership.CreateUser(model.Login, model.Password, null, null, null, true, out status);
                
                if (status == MembershipCreateStatus.Success)
                {
                    var userModel =
                        AutoMapper.Mapper.Map<UserCreateFromDictionaryViewModel, UserCreateFromDictionaryModel>(model);
                    userRepository.CreateUserFromModel(userModel);                    

                    return RedirectToAction("CreateSuccess");
                }
                else
                {
                    ModelState.AddModelError("", status.ToString());
                }
                
            }

            model.UserRole.AllRoles  = roleRepository.GetAllRoleSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
            model.UserRole.AllDirections = directionRepository.GetAllDirectionSelectedListItems().Select(a => a.ToSelectListItem()).ToList();            
            
            ViewBag.IsAfterError = true;
            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление создания пользователя
        /// </summary>
        /// <returns>Строго типизированное представление с формой создания пользователя</returns>
        [AuthGrok(Roles.Administrator)]
        public ActionResult Create()
        {            
            var directionRepository = new DirectionRepository();
            var roleRepository = new RoleRepository();

            var model = new UserCreateViewModel();            
            model.UserRole.RoleId = _roles.First().RoleId;

            model.UserRole.AllRoles = roleRepository.GetAllRoleSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
            model.UserRole.AllDirections = directionRepository.GetAllDirectionSelectedListItems().Select(a => a.ToSelectListItem()).ToList();            

            return View(model);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы создания пользователя
        /// </summary>
        /// <param name="model">Модель представления создания пользователя</param>
        /// <returns>Перенаправление на страницу CreateSuccess в случае валидности модели
        ///          и строго типизированное представление с формой создания пользователя
        ///          в противном случае</returns>
        [AuthGrok(Roles.Administrator)]
        [HttpPost]
        public ActionResult Create(UserCreateViewModel model)
        {
            var userRepository = new UserRepository();            
            var directionRepository = new DirectionRepository();
            var roleRepository = new RoleRepository();

            if (ModelState.IsValid)
            {
                MembershipCreateStatus status;
                Membership.CreateUser(model.Login, model.Password, null, null, null, true, out status);                

                if (status == MembershipCreateStatus.Success)
                {
                    var userModel = AutoMapper.Mapper.Map<UserViewModel, UserModel>(model);
                    userRepository.CreateUserFromModel(userModel);

                    return RedirectToAction("CreateSuccess");
                }
                else
                {
                    ModelState.AddModelError("", status.ToString());
                }
            }

            model.UserRole.AllRoles = roleRepository.GetAllRoleSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
            model.UserRole.AllDirections = directionRepository.GetAllDirectionSelectedListItems().Select(a => a.ToSelectListItem()).ToList();          

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление редактирования пользователя
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns>Строго типизированное представление с формой редактирования пользователя</returns>
        [AuthGrok(Roles.Administrator)]
        public ActionResult Edit(int userId)
        {
            var userRepository = new UserRepository();
            var userRoleRepository = new UserRoleRepository();
            var directionRepository = new DirectionRepository();
            var roleRepository = new RoleRepository();

            if (!userRepository.UserWithIdExists(userId))
            {
                return HttpNotFound();
            }

            var model = new UserViewModel();            
            if (!userRepository.EditModelFromUser(userId, model))
            {
                return HttpNotFound();
            }

            var userRoleModel = userRoleRepository.GetUserRole(userId);
            model.UserRole = AutoMapper.Mapper.Map<UserRoleModel, UserRoleViewModel>(userRoleModel);

            model.UserRole.AllRoles = roleRepository.GetAllRoleSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
            model.UserRole.AllDirections = directionRepository.GetAllDirectionSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
                        
            return View(model);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы редактирования пользователя
        /// </summary>
        /// <param name="model">Модель представления редактирования пользователя</param>
        /// <returns>Перенаправление на страницу EditSuccess в случае валидности модели
        ///          и строго типизированное представление с формой редактирования пользователя
        ///          в противном случае</returns>
        [AuthGrok(Roles.Administrator)]
        [HttpPost]
        public ActionResult Edit(UserViewModel model)
        {
            var userRepository = new UserRepository();
            var userRoleRepository = new UserRoleRepository();
            var directionRepository = new DirectionRepository();
            var roleRepository = new RoleRepository();

            if (!userRepository.UserWithIdExists(model.UserId))
            {
                return HttpNotFound();
            }            
            
            if (ModelState.IsValid)
            {
                userRoleRepository.DeleteAllUserRoles(model.UserId);
                userRoleRepository.UpdateUserRole(model.UserId, model.UserRole);

                userRepository.UpdateUserFromModel(model);

                return RedirectToAction("EditSuccess");
            }

            var userRoleModel = userRoleRepository.GetUserRole(model.UserId);
            model.UserRole = AutoMapper.Mapper.Map<UserRoleModel, UserRoleViewModel>(userRoleModel);

            model.UserRole.AllRoles = roleRepository.GetAllRoleSelectedListItems().Select(a => a.ToSelectListItem()).ToList();
            model.UserRole.AllDirections = directionRepository.GetAllDirectionSelectedListItems().Select(a => a.ToSelectListItem()).ToList();

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление с формой смены пароля
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns>Строго типизированное представление с формой смены пароля</returns>
        [AuthGrok(Roles.Administrator)]
        public ActionResult ChangePassword(int userId)
        {
            var userRepository = new UserRepository();

            if (!userRepository.UserWithIdExists(userId))
            {
                return HttpNotFound();
            }

            var model = new ChangePasswordViewModel();
            model.UserId = userId;

            return View(model);
        }

        /// <summary>
        /// Метод, обрабатывающий данные формы смены пароля
        /// </summary>
        /// <param name="model">Модель представления смены пароля</param>
        /// <returns>Перенаправление на страницу ChangePasswordSuccess в случае валидности модели
        ///          и строго типизированное представление с формой смены пароля
        ///          в противном случае</returns>
        [AuthGrok(Roles.Administrator)]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            var userRepository = new UserRepository();

            if (!userRepository.UserWithIdExists(model.UserId))
            {
                return HttpNotFound();
            }

            var login = userRepository.GetLoginById(model.UserId);

            if (ModelState.IsValid)
            {
                MembershipUser membershipUser = Membership.GetUser(login, true);                

                if (membershipUser == null)
                {                    
                    ModelState.AddModelError("", String.Format("Не найдено пользователя с логином {0}", login) );
                    return View("ChangePasswordError");
                }

                if (!membershipUser.ChangePassword("old", model.Password))
                {
                    ModelState.AddModelError("", "Ошибка при смене пароля");
                    return View("ChangePasswordError");
                }

                return RedirectToAction("ChangePasswordSuccess");
            }

            return View(model);
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешном создании пользователя
        /// </summary>
        /// <returns>Представление, извещающее об успешном создании пользователя</returns>
        [AuthGrok(Roles.Administrator)]
        public ActionResult CreateSuccess()
        {
            return View();
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешном изменении атрибутов пользователя
        /// </summary>
        /// <returns>Представление, извещающее об успешном изменении атрибутов пользователя</returns>
        [AuthGrok(Roles.Administrator)]
        public ActionResult EditSuccess()
        {
            return View();
        }

        /// <summary>
        /// Метод, возвращающий представление, извещающее об успешной смене пароля
        /// </summary>
        /// <returns>Представление, извещающее об успешной смене пароля</returns>
        [AuthGrok(Roles.Administrator)]
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }        

        #region AJAX

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с сотрудниками ЦППК
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="orderby">Параметры сортировки</param>
        /// <param name="filter">Параметры фильтрации</param>
        /// <param name="timesheetId">Для табелей - для отбраковки сотрудников, которые уже есть в табеле</param>
        /// <returns>Список сотрудников ЦППК и их количество в формате JSON</returns>
        [AjaxAuthGrok]
        [HttpPost]
        public ActionResult ReadEmployees(int page, int pageSize, string orderby, string filter, int? timesheetId)
        {
            var employeeRepository = new EmployeeRepository();
            var filterDictionary = HttpUtility.ParseQueryString(filter);
            int itemsCount;
            
            var employees = employeeRepository.Search(page, pageSize, orderby, filterDictionary, timesheetId, out itemsCount);            

            return Json(
                new
                {
                    Items = employees.ToArray(),
                    Count = itemsCount
                });
        }

        /// <summary>
        /// Обработчик ajax-запроса от kendo-grid с пользователями
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="pageSize">Количество элементов на странице</param>
        /// <param name="sort">Список (из 1 элемента) с параметрами сортировки</param>
        /// <param name="filter">Параметры фильтрации</param>
        /// <returns>Список пользователей и их количество в формате JSON</returns>
        [AjaxAuthGrok(Roles.Administrator)]
        [HttpPost]
        public ActionResult Read(int page, int pageSize, List<SortDescription> sort, FilterContainer filter)
        {
            var userRepository = new UserRepository();
            int itemsCount;
            
            var users = userRepository.Search(page, pageSize, sort, filter, out itemsCount);            

            foreach (var user in users)
            {
                user.EditUrl = Url.Action("Edit", new {userId = user.UserId});
                user.ChangePasswordUrl = Url.Action("ChangePassword", new {userId = user.UserId});
            }

            return Json(
                new
                    {
                        Items = users.ToArray(),
                        Count = itemsCount
                    });
        }

        /// <summary>
        /// Обработчик ajax-запроса блокировки пользователя
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [AjaxAuthGrok(Roles.Administrator)]
        [HttpPost]        
        public ActionResult Block(int userId)
        {
            var userRepository = new UserRepository();

            try
            {
                if (!userRepository.UserWithIdExists(userId))
                {
                    return Json(new { status = JsonStatuses.Error, message = "Пользователь не найден" });
                }

                userRepository.UpdateUserStatus(userId, UserStatuses.Blocked);                

                return Json(new { status = JsonStatuses.Ok, isActive = false});
            }
            catch (Exception e)
            {
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса активизации пользователя
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns>JSON, содержащий статус выполнения запроса</returns>
        [AjaxAuthGrok(Roles.Administrator)]
        [HttpPost]
        public ActionResult Active(int userId)
        {
            var userRepository = new UserRepository();

            try
            {
                if (!userRepository.UserWithIdExists(userId))
                {
                    return Json(new { status = JsonStatuses.Error, message = "Пользователь не найден" });
                }

                userRepository.UpdateUserStatus(userId, UserStatuses.Active);                

                return Json(new { status = JsonStatuses.Ok, isActive = true });
            }
            catch (Exception e)
            {
                return Json(new { status = JsonStatuses.Error, message = "Произошла ошибка при выполнении запроса" });
            }
        }        

        /// <summary>
        /// Возвращает список всех секций по AJAX запросу
        /// </summary>
        /// <returns>Список всех секций</returns>
        [AjaxAuthGrok(Roles.Administrator)]
        public JsonResult GetSectors(int directionId)
        {
            var secRepository = new SectorRepository();            
            var sectors = secRepository.GetAllSectorsForDirectory(directionId);            
            return Json(sectors, JsonRequestBehavior.AllowGet);
        }                

        #endregion                     
              

    }
}
