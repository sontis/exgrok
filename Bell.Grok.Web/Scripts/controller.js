var Controller = (function ($) {
    var mod = {};

    mod.create = function (includes) {
        var result = function () {
            this.initializer.apply(this, arguments);
            this.init.apply(this, arguments);
        };

        result.fn = result.prototype;
        result.fn.init = function () {
        };

        result.proxy = function (func) {
            return $.proxy(func, this);
        };
        result.fn.proxy = result.proxy;

        result.include = function (ob) {
            $.extend(this.fn, ob);
        };
        result.extend = function (ob) {
            $.extend(this, ob);
        };

        result.fn.getKey = function (ob, value) {
            for (var prop in ob) {
                if (ob[prop] === value) {
                    return prop;
                }
            }
        };        

        result.getKey = function (ob) {
            for (var prop in ob) {                
                if (ob[prop] === value)
                    return prop;                
            }
        };
        
        result.fn.trimParentSelector = function(selector) {
            var selectors = selector.split(' ');
            if (selectors.length > 1) {
                return selectors[1];
            } else {
                return '';
            }
        };

        result.include({
            initializer: function (options) {
                var key;
                this.options = options;

                for (key in this.options) {                    
                    this[key] = this.options[key];                    
                }

                if (this.events) {
                    this.delegateEvents();
                }
                if (this.elements) {
                    this.refreshElements();
                }
            },

            $: function (selector) {
                return $(selector, this.el);
            },

            refreshElements: function () {
                var key;
                for (key in this.elements) {                    
                    this[this.elements[key]] = this.$(key);                    
                }
            },

            eventSplitter: /^(\w+)\s*(.*)$/,

            delegateEvents: function () {
                var key,
                    methodName,
                    method,
                    match,
                    eventName,
                    selector;

                for (key in this.events) {                    
                    methodName = this.events[key];
                    method = this.proxy(this[methodName]);

                    match = key.match(this.eventSplitter);
                    eventName = match[1];
                    selector = this.getKey(this.elements, match[2]);

                    if (selector === '') {
                        this.el.bind(eventName, method);
                    } else {
                        this.el.delegate(selector, eventName, method);
                    }                    
                }
            }
        });

        if (includes) {
            result.include(includes);
        }

        return result;
    };

    return {
        instance: mod
    };
} (jQuery));