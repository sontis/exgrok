﻿/// <summary>
///     Обеспечивает работу с ошибками валидации.
/// </summary>
var ValidationHelper = function ($) {
    var validatableFormSelector,
        validatableFieldClass,
        validationErrorClass,
        validationSummarySelector;

    /// <summary>
    ///     Очищает ошибки, сгенерированные jquery validate.
    /// </summary>
    var clearErrors = function () {
        var container = $(validationSummarySelector),
            list = container.find("ul");

        if (list.length > 0) {
            list.empty();
            container.addClass("validation-summary-valid").removeClass("validation-summary-errors");
        }

        $("span.field-validation-error").each(function () {
            $(this).empty();
            $(this).removeClass("field-validation-error");
            $(this).addClass("field-validation-valid");
        });

        $(".input-validation-error").each(function () {
            if ($(this).is('span'))
                $(this).empty();
            $(this).removeClass("input-validation-error");
            $(this).addClass("input-validation-valid");
            
            if ($(this).data("kendoTooltip") !== undefined) {
                $(this).data("kendoTooltip").destroy();
            }
        });

        $('.grok-validation-error').each(function () {
            $(this).removeClass('grok-validation-error');
            $(this).removeAttr("original-title");
        });
    },

    /// <summary>
    ///     Вставляет ошибки в контейнер.
    /// </summary>
    insertErrorsToValidationSummary = function (messages) {
        var containerSelector = typeof validationSummarySelector === 'undefined' ? '[data-valmsg-summary=true]' : validationSummarySelector,
            container = $(containerSelector),
            list = container.find("ul");

        if (list.length > 0) {
            list.empty();
            container.addClass("validation-summary-errors").removeClass("validation-summary-valid");

            $.each(messages, function (i, message) {
                $("<li />").html(message).appendTo(list);
            });
        }
    },

    /// <summary>
    ///     Вставляет ошибку в контейнер.
    /// </summary>
    insertErrorToValidationSummary = function (message) {
        insertErrorsToValidationSummary([message]);
    },

    /// <summary>
    ///     Отображает ошибки валидации.
    /// </summary>
    /// <param name="errors">
    ///     Массив объектов: { ElementName: string; ErrorMessage: string }
    /// </param>
    /// <param name="formSelector">Селектор формы.</param>
    showValidationErrors = function (errors, formSelector) {
        var errorObject = {},
            i,
            name,
            error,
            validator;

        for (i = 0; i < errors.length; i += 1) {
            name = errors[i].ElementName;
            error = errors[i].ErrorMessage;

            if (!name) {
                insertErrorToValidationSummary(error);
            } else {
                errorObject[name] = error;
            }
        }

        validator = $(formSelector).validate({
            errorClass: 'input-validation-error',
            errorElement: 'span',
            errorPlacement: function(error, element) {                
                $("span[data-valmsg-for=" + element[0].name + "]")
                    .removeClass('field-validation-valid')
                    .addClass('field-validation-error')
                    .html(error);                
            }            
        });
        validator.showErrors(errorObject);

        $(formSelector + ' input:not([type=hidden]),' + formSelector + ' textarea,' + formSelector + ' select').each(function () {
            validateElement(this);
        });
    },

    /// <summary>
    ///     Проверяет заданное поле на наличие ошибок.
    /// </summary>
    validateElement = function (input) {
        var element = $(input);
        var comboboxInput;

        if (!isElementValid(element)) {
            var errorMessage = getValidationMessage(element);
            element.addClass(validationErrorClass);            
            
            element.kendoTooltip({
                content: errorMessage.toString(),
                width: 150,
                position: "top"
            });
            
        } else {
            element.removeClass(validationErrorClass);

            if (element.data("kendoTooltip") !== undefined) {
                element.data("kendoTooltip").destroy();
            }            
        }
    },

    /// <summary>
    ///     Проверяет, существует ли для заданного элемента ошибка валидации.
    /// </summary>
    isElementValid = function (element) {
        if (element.hasClass('input-validation-error')) {
            return false;
        } else {
            var validationMessageContainer = $('span[data-valmsg-for=' + element.attr('name') + ']');
            var childLength = validationMessageContainer.children().length;
            if (childLength > 0) {
                element.addClass(validationErrorClass);
                return false;
            }
        }

        return true;
    },

    /// <summary>
    ///     Получает сообщение об ошибке.
    /// </summary>
    getValidationMessage = function (element) {
        //
        // Сообщение об ошибке может находится как внутри span'а с классом field-validation-error,
        // так и во вложенном span'е.
        //
        
        var validationMessageContainer = $('span[data-valmsg-for=' + element.attr('name') + ']');
        var innerSpan = validationMessageContainer.find('span');

        if (innerSpan.length > 0) {
            return innerSpan.text();
        } else {
            return validationMessageContainer.text();
        }
    },

    /// <summary>
    ///     Проверяет все поля формы на допустимость значений.
    /// </summary>
    validateAllFields = function () {
        $(validatableFormSelector + ' input:not([type=hidden]),' + validatableFormSelector + ' textarea,' + validatableFormSelector + ' select').each(function () {
            validateElement(this);
        });
    },

    /// <summary>
    ///     Инициализирует валидацию формы.
    /// </summary>
    /// <param name="initInfo">
    ///     formSelector - селектор формы
    ///     validatableFieldClass - класс полей, для которых нужно выполнять валидацию
    ///     validationErrorClass - класс ошибок валидации
    ///     validationSummarySelector - селектор контейнера, содержащего ошибки валидации модели
    /// </param>
    init = function (initInfo) {
        if (typeof initInfo.formSelector === 'undefined') {
            initInfo.formSelector = 'form';
        }
        if (typeof initInfo.validatableFieldClass === 'undefined') {
            initInfo.validatableFieldClass = 'grok-validatable-field';
        }
        if (typeof initInfo.validationErrorClass === 'undefined') {
            initInfo.validationErrorClass = 'grok-validation-error';
        }
        if (typeof initInfo.validationSummarySelector === 'undefined') {
            initInfo.validationSummarySelector = '[data-valmsg-summary=true]';
        }

        validatableFormSelector = initInfo.formSelector;
        validatableFieldClass = initInfo.validatableFieldClass;
        validationErrorClass = initInfo.validationErrorClass;
        validationSummarySelector = initInfo.validationSummarySelector;

        // Ставим обработчик валидации элементов формы.        
        $(validatableFormSelector).addTriggersToJqueryValidate().triggerElementValidationsOnFormValidation();
        $(validatableFormSelector + ' input:not([type=hidden]):not(.ignore-validation),' + validatableFormSelector + ' textarea,' + validatableFormSelector + ' select').elementValidation(function (element) {
            validateElement(element);
        });        
    };

    return {
        clearErrors: clearErrors,
        insertErrorToValidationSummary: insertErrorToValidationSummary,
        insertErrorsToValidationSummary: insertErrorsToValidationSummary,
        showValidationErrors: showValidationErrors,
        validateAllFields: validateAllFields,
        init: init
    };
} ($);

