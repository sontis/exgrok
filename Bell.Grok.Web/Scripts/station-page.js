﻿var viewModel;

var resetTimeSpan = function (time) {
    if (time) {
        var newTime = new Date(0);
        newTime.setHours(time.getHours());
        newTime.setMinutes(time.getMinutes());
        return newTime;
    } else {
        return null;
    }
};

$(function() {
    kendo.culture("ru-RU");
    //Переменная, показывающая, находится ли мышка над иконкой ссылки "Редактировать"
    InterfaceModule.isMouseOverAnchor = false;

    //Достаём из кук данные фильтра кассовых окон и десериализуем их
    CookieModule.loadSearchParams("#cash-boxes-search-params");

    //Достаём из кук данные фильтра терминалов и десериализуем их
    CookieModule.loadSearchParams("#terminal-search-params");

    //Ранняя инициализация datepicker-ов для задания ограничений
    InterfaceModule.initDatePickersConstraints();

    //Определяем возможность редактирования грида планов выручки
    //в зависимости от роли пользователя
    InterfaceModule.setProceedsPlanGridEditableProperty();

    var dataBindingModule = DataBindingModule(CookieModule);
    var hubModule = HubModule(CookieModule, ViewModelsModule);

    viewModel = kendo.observable({
        isTellerWindowsTabVisible: $("#is-teller-windows-tab-visible").attr("data-is-visible").toLowerCase() === "true",
        isTerminalsTabVisible: $("#is-terminals-tab-visible").attr("data-is-visible").toLowerCase() === "true",
        isScheduleTabVisible: $("#is-schedule-tab-visible").attr("data-is-visible").toLowerCase() === "true",
        isProceedsPlansTabVisible: $("#is-proceeds-plans-tab-visible").attr("data-is-visible").toLowerCase() === "true",
        isEmployeeShiftScheduleTabVisible: $("#is-employee-shift-schedule-tab-visible").attr("data-is-visible").toLowerCase() === "true",
        //Субмодуль для отслеживания изменений вкладок
        tabStripViewModel: kendo.observable(ViewModelsModule.tabStripViewModel),
        //Субмодуль для отслеживания изменения значений формы фильтра кассовых окон
        tellerWindowFilterViewModel: kendo.observable(ViewModelsModule.tellerWindowFilterViewModel),
        //Субмодуль для отслеживания изменения значений формы фильтра терминалов
        terminalFilterViewModel: kendo.observable(ViewModelsModule.terminalFilterViewModel),
        //Субмодуль для отслеживания изменения значения даты графика работы
        scheduleViewModel: kendo.observable(ViewModelsModule.scheduleViewModel),
        //Субмодуль для отслеживания изменения значений формы фильтра планов выручки
        proceedsPlanViewModel: kendo.observable(ViewModelsModule.proceedsPlanViewModel),
        //Субмодуль для формы редактирования смен персонала
        shiftScheduleViewModel: kendo.observable(ViewModelsModule.shiftScheduleViewModel),
        //Источник данных таблицы кассовых окон
        tellerWindowGridSource: hubModule.tellerWindowGridSource,
        //Источник данных таблицы терминалов
        terminalGridSource: hubModule.terminalGridSource,
        //Источник данных таблицы привязок терминалов к кассовым окнам
        tellerWindowTerminalGridSource: hubModule.tellerWindowTerminalGridSource,
        //Источник данных таблицы графиков
        schedulesSource: hubModule.schedulesSource,
        //Источник данных таблицы режимов работы касс
        tellerWindowSchedulesSource: hubModule.tellerWindowSchedulesSource,
        //Источник данных для таблицы планов по выручке
        proceedsPlanSource: hubModule.proceedsPlanSource,
        //Функция, инициирующая фильтрацию кассовых окон
        tellerWindowFilter: FilterModule.tellerWindowFilter,
        //Функция сброса фильтра на вкладке кассовых окон
        tellerWindowResetFilter: FilterModule.tellerWindowResetFilter,
        //Функция, инициирующая фильтрацию терминалов
        terminalFilter: FilterModule.terminalFilter,
        //Функция сброса фильтра на вкладке ККТ
        resetTerminalFilter: FilterModule.resetTerminalFilter,
        //Функция, удаляющая кассовое окно
        tellerWindowDeleteListener: DeleteListenersModule.tellerWindowDeleteListener,        
        //Функция, удаляющая привязку терминала к окну
        tellerWindowTerminalDeleteListener: DeleteListenersModule.tellerWindowTerminalDeleteListener,
        //Функция, удаляющая расписание кассира
        scheduleDeleteListener: DeleteListenersModule.scheduleDeleteListener,
        //Функция для перехода на страницу добавления граффика
        AddSchedule: ScheduleModule.add,
        //Обработчик нажатия на ссылку для экспорта расписаний
        scheduleExportClick: ScheduleModule.exportClick,
        //Функция, согласующая график кассира
        scheduleSendReconcileListener: ScheduleModule.sendReconcileListener,
        //Функция, подтверждающая согласование графика кассиров
        confirmScheduleReconcileListener: ScheduleModule.confirmReconcileListener,
        //Функция, отменяющая согласование графика кассира
        scheduleCancelReconcileListener: ScheduleModule.cancelReconcileListener,        
        //Обработчик захода курсора на иконку "Редактировать"
        mouseOverEditTerminalListener: InterfaceModule.mouseOverEditTerminalListener,
        //Обработчик ухода курсора с иконки "Редактировать"
        mouseOutEditTerminalListener: InterfaceModule.mouseOutEditTerminalListener,
        //Обработчик выделения терминала в соответствующей таблице
        terminalSelected: InterfaceModule.terminalSelected,
        //Обработчик смены вкладки. Сохраняет номер текущей вкладки в куках
        tabStripActivateHandler: InterfaceModule.tabStripActivateHandler,
        //Функция, делающая неактивными контролы для дат в фильтре терминалов
        showOnlyCurrent: InterfaceModule.showOnlyCurrent,
        //Функция. делающая активными контролы для дат в фильтре терминалов
        showAll: InterfaceModule.showAll,
        //Обработчик прихода данных в таблицу кассовых окон
        tellerWindowsBound: dataBindingModule.tellerWindowsBound,
        //Обработчик прихода данных в таблицу терминалов
        terminalsBound: dataBindingModule.terminalsBound,
        //Обработчик прихода данных в таблицу расписаний
        scheduleBound: dataBindingModule.scheduleBound,
        //Обработчик прихода данных в таблицу планов графиков
        proceesdsPlanBound: dataBindingModule.proceesdsPlanBound,
        //Инициализация текущей привязки терминала к станции и таблицы привязок окон к терминалу
        terminalGridDataBinding: dataBindingModule.terminalGridDataBinding
    });   

    kendo.bind($("#station-passport-info"), viewModel);
    //Делаем дополнительное связывание между отдельными частями разметки и вложенными моделями, так как
    //при изменении значений (value-binding) для вложенных viewmodel kendo mvvm выдаёт ошибку    
    kendo.bind($("#scheduleDateInput"), viewModel.scheduleViewModel);
    kendo.bind($("#proceeds-plan-search-params"), viewModel.proceedsPlanViewModel);

    if (viewModel.isTellerWindowsTabVisible) {
        InitialTellerWindowModule.init();
       
    }
    if (viewModel.isTerminalsTabVisible) {
        InitialTerminalParamsModule.init();
    }

    viewModel.shiftScheduleViewModel.init();    

    if (viewModel.isEmployeeShiftScheduleTabVisible) {
        //Инициализируем вкладку с режимами работы работников
        EmployeeShiftScheduleModule.init();
    }
    
    //Инициализируем вкладку со срезами расписаний работы кассовых окон
    TellerWindowScheduleModule.init();    

    //Установка обработчиков собственных событий
    CustomEventsHandlersModule.init();

    //Восстанавливаем текущий номер вкладки из сессии
    CookieModule.loadTabIndex();

    //Загрузить данные для активной вкладки
    viewModel.tabStripViewModel.defineCurrentTab();

    //Если пользователь сотрудник ООП или сотрудник ЦО или начальник направления или сотрудник ОТиЗП
    //или профгруппорг, то делаем неактивными все кнопки на вкладках кассовых окон и ККТ
    InterfaceModule.checkButtonsAvailAbility();
});

//Модуль вкладки "Срезы работы кассовых окон"
var TellerWindowScheduleModule = (function () {
    var currentYear = new Date().getFullYear();
    var seasons = [];

    //Собственный редактор для выбора года
    var customYearEditor = function (container, options) {        
        var data = [
            { Value: currentYear, Text: currentYear.toString() },
            { Value: (currentYear + 1), Text: (currentYear + 1).toString() },
            { Value: (currentYear + 2), Text: (currentYear + 2).toString() }
        ];

        $('<input required data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                autoBind: false,
                dataSource: data,
                dataValueField: "Value",
                dataTextField: "Text"               
            });            
    };

    //Собственный редактор для выбора сезона
    var customSeasonEditor = function (container, options) {        
        $('<input required data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                autoBind: false,
                dataSource: seasons,
                dataValueField: "SeasonId",
                dataTextField: "SeasonName"                
            });
    };
    
    //Обработчик отмены добавления среза
    var cancelHandler = function () {
        var dataSource = $("#teller-window-schedules").data("kendoGrid")
                                                        .dataSource;
        var data = dataSource.data();

        var dataItems = $.grep(data, function (element) {
            return element.TellerWindowScheduleModelId === null ||
                element.TellerWindowScheduleModelId === 0;
        });

        if (dataItems.length === 1) {
            dataSource.remove(dataItems[0]);
        }
    };    

    //Функция, строящая датасорс
    var getDataSource = function () {
        var sectorStationId = $("#sector-station-id").attr("data-value");

        return new kendo.data.DataSource({            
            type: "json",
            serverSorting: false,
            transport: {
                read: {
                    url: $("#teller-window-schedules-read-url").attr("data-url"),
                    dataType: "json",
                    type: "POST"
                },
                create: {
                    url: $("#teller-window-schedule-create-url").attr("data-url"),
                    type: "POST",
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',                    
                    complete: function (e) {
                        var grid = $("#teller-window-schedules").data("kendoGrid");
                        var response = JSON.parse(e.responseText);
                        var obj = null;
                        if (response.length > 0) {
                            obj = response[0];
                        }                            

                        if (obj && obj.TellerWindowScheduleModelId) {
                            var allData = grid.dataSource.data();                                                        
                            
                            var dataItems = $.grep(allData, function (element) {
                                return element.TellerWindowScheduleModelId === null ||
                                    element.TellerWindowScheduleModelId === 0;
                            });
                            
                            if (dataItems.length === 1) {
                                dataItems[0].TellerWindowScheduleModelId = obj.TellerWindowScheduleModelId;
                            }
                        }
                        
                        var dsSort = [];
                        dsSort.push({ field: "Year", dir: "asc" });
                        dsSort.push({ field: "Season", dir: "asc" });
                        grid.dataSource.sort(dsSort);
                    }
                },
                parameterMap: function (options, operation) {
                    if (operation === "read") {
                        return {
                            sectorStationId: sectorStationId
                        };
                    } else if (operation === "create" && options.models) {
                        return JSON.stringify(options.models);                       
                    } else {
                        return null;
                    }
                }
            },
            schema: {
                errors: function(response) {
                    if (response.status &&
                        response.status === JsonStatus.invalid) {
                        return response.message;
                    }
                                        
                    return false;
                },
                model: {                    
                    fields: {
                        TellerWindowScheduleModelId: { editable: false, nullable: true },
                        Year: { defaultValue: { Value: currentYear, Text: currentYear.toString() } },
                        Season: { defaultValue: seasons[0] },
                        SectorStationId: { editable: false, defaultValue: sectorStationId }
                    },                    
                    customGetter: function (field) {
                        switch (field) {
                            case "Year":
                                return this.Year.Value;
                            case "Season":
                                return this.Season.SeasonId;
                            default:
                                return this[field];
                        }
                    }
                },
                data: "Items"
            },
            batch: true,            
            error: function (e) {                
                if (!window.unloading) {
                    if (e.errors) {
                        alert(e.errors);
                        cancelHandler();
                    } else {
                        alert('При получении данных возникла ошибка');
                    }
                }
            },            
            requestEnd: function (e) {
                if (e.response !== undefined &&
                    e.response.status !== undefined &&
                    e.response.status === JsonStatus.error) {
                    alert(e.response.message);
                }
            }            
        });
    };

    //Функция, производящая перенаправление на страницу редактирования срезов работы кассовых окон
    var detailsHandler = function(e) {
        var row = $(e.currentTarget).closest("tr");
        var rowData = $("#teller-window-schedules").data("kendoGrid")
                                                     .dataItem(row);        
        var stationId = $("#station-id").attr("data-value");
        var sectorId = $("#sector-id").attr("data-value");
        var currentDate = $("#current-date-str").attr("data-date");
        var returnUrl = $("#current-url").attr("data-url");

        window.location.href = $("#teller-window-schedule-edit-url").attr("data-url") + "?" +
            "year=" + rowData.Year.Value + "&" +
            "seasonId=" + rowData.Season.SeasonId + "&" +
            "stationId=" + stationId + "&" +
            "sectorId=" + sectorId + "&" +
            "currentDate=" + currentDate + "&" +
            "returnUrl=" + returnUrl;
    };
   
    //Функция. конструирующая таблицу
    var initGrid = function(dataSource) {
        $("#teller-window-schedules").kendoGrid({
            autoBind: false,
            dataSource: dataSource,
            toolbar: [ { name: "create", text: "Добавить" } ],
            columns: [
                { field: "Year", title: "Год", editor: customYearEditor, template: "#=Year.Value#" },
                { field: "Season", title: "Сезон", editor: customSeasonEditor, template: "#=Season.SeasonName#" },
                {
                    command: [{ name: "edit", text: { edit: "Редактировать", update: "Сохранить", cancel: "Отмена" } },
                              { name: "details", text: "Редактировать", click: detailsHandler }],
                    title: "&nbsp;"
                }],
            editable: {
                mode: "inline"                
            },
            dataBound: function (e) {                
                $("#teller-window-schedules .k-grid-edit").hide();                                                
            },
            cancel: function(e) {
                cancelHandler();
                e.preventDefault();
            }             
        });
    };       

    var init = function () {
        seasons = $("#seasons").data("seasons");        

        var dataSource = getDataSource();               
        initGrid(dataSource);               
    };        

    return {
        init: init        
    };
})($);

//Модуль вкладки "Режимы работы работника"
var EmployeeShiftScheduleModule = (function () {
    var detailInit = function (e) {
        var columns;
        if ($("#shiftScheduleButtonColumn").length > 0) {
            columns = [
                { "template": kendo.template($("#shiftScheduleButtonColumn").html()), width: "40px" },
                { field: "WeekDayName", title: "День недели", width: "170px" },
                { field: "WorkTime", title: "Время работы", width: "110px" },
                { field: "BreakTime", title: "Обед", width: "110px" },
            ];
        } else {
            columns = [
                { field: "WeekDayName", title: "День недели", width: "170px" },
                { field: "WorkTime", title: "Время работы", width: "110px" },
                { field: "BreakTime", title: "Обед", width: "110px" },
            ];
        }
        $("<div/>").appendTo(e.detailCell).kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: $("#read-employee-shift-schedule").attr("data-url"),
                        dataType: "json",
                        type: "POST",
                    },
                    parameterMap: function (options) {

                        return {
                            shiftId: e.data.Id,
                        };
                    },
                },
            },
            scrollable: false,
            columns: columns
        });
    };

    var init = function () {
        var columns;
        if ($("#shiftButtonColumn").length > 0) {
            columns = [
                { "template": kendo.template($("#shiftButtonColumn").html()), width: 105 },
                { field: "Name", title: "Смена" },
            ];
        } else {
            columns = [
                { field: "Name", title: "Смена" },
            ];
        }

        $("#employeeShiftScheduleGrid").kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: $("#read-employee-shifts").attr("data-url"),
                        dataType: "json",
                        type: "POST",
                    },
                    parameterMap: function (options) {

                        return {
                            stationId: $("#station-id").attr("data-value"),
                        };
                    },
                },
                batch: true,
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            Name: { editable: false },
                        }
                    }
                }
            },
            autoBind: false,
            detailInit: detailInit,
            dataBound: function () {
                var toExpand = [];
                var expandedShifts = $.session.get('ExpandedShifts');
                if (expandedShifts) {
                    var master = $("#employeeShiftScheduleGrid .k-master-row");
                    for (var i = 0; i < master.length; i++) {
                        if (expandedShifts.indexOf(i) > -1) {
                            toExpand.push(master[i]);
                        }
                    }

                    this.expandRow(toExpand);
                }
            },
            columns: columns,
        });
    };

    return {
        init: init
    };
})($);

//Всё делается по аналогии с InitialTerminalParamsModule, но для кассовых окон
var InitialTellerWindowModule = (function () {
    var initialTellerWindowsParams,
        init = function () {
            initialTellerWindowsParams = {
                tellerWindowNumbers: new Array(),
                beginDate: kendo.toString($("#TellerWindowBeginExploitationPeriod").data("kendoDatePicker").value(), "dd.MM.yyyy"),
                endDate: kendo.toString($("#TellerWindowEndExploitationPeriod").data("kendoDatePicker").value(), "dd.MM.yyyy")
            };

            $("#SelectedWindowTellerNumbers").data("kendoMultiSelect").value().forEach(function (entry) {
                initialTellerWindowsParams.tellerWindowNumbers.push(entry);
            });
        },
        getInitialTellerWindowParams = function () {
            return initialTellerWindowsParams;
        };

    return {
        init: init,
        getInitialTellerWindowParams: getInitialTellerWindowParams
    };
}($));

//Модуль для отслеживания изменений на форме фильтрации терминалов
var InitialTerminalParamsModule = (function () {
    var initialTerminalParams,
        //Функция получения значения контрола "Дата с"
        initBeginDate = function () {
            var isShowCurrent = $('input[name=IsShowCurrent]:checked').val().toLowerCase() === "true";
            if (!isShowCurrent) {
                return kendo.toString($("#TerminalBeginExploitationPeriod").data("kendoDatePicker").value(), "dd.MM.yyyy");
            } else {
                return "";
            }
        },
        //Функция получения значения контрола "Дата по"
        initEndDate = function () {
            var isShowCurrent = $('input[name=IsShowCurrent]:checked').val().toLowerCase() === "true";
            if (!isShowCurrent) {
                return kendo.toString($("#TerminalEndExploitationPeriod").data("kendoDatePicker").value(), "dd.MM.yyyy");
            } else {
                return "";
            }
        },
        //Функция инициализации объекта с параметрами фильтра терминалов
        init = function () {
            //Запоминаем первоначальные параметры фильтра терминалов
            initialTerminalParams = {
                terminalNumber: new Array(),
                terminalType: new Array(),
                selectedWindowTellerNumber: new Array(),
                isShowCurrent: $('input[name=IsShowCurrent]:checked').val(),
                beginDate: initBeginDate(),
                endDate: initEndDate()
            };

            //Заполняем исходный массив номеров терминалов
            $("#SelectedTerminalNumbers").data("kendoMultiSelect").value().forEach(function (entry) {
                initialTerminalParams.terminalNumber.push(entry);
            });

            //Заполняем исходный массив типов терминалов
            $("#SelectedTerminalTypes").data("kendoMultiSelect").value().forEach(function (entry) {
                initialTerminalParams.terminalType.push(entry);
            });

            //Заполняем исодный массив номеров окон, к которым привязан терминал
            $("#SelectedWindowTellerNumbersInTerminalTab").data("kendoMultiSelect").value().forEach(function (entry) {
                initialTerminalParams.selectedWindowTellerNumber.push(entry);
            });
        },
        getInitialTerminalParams = function () {
            return initialTerminalParams;
        };

    return {
        init: init,
        getInitialTerminalParams: getInitialTerminalParams
    };
}($));

//Модуль обработчиков удалений различных объектов
var DeleteListenersModule = (function () {    
    var tellerWindowDeleteListener = function (e) {
        e.preventDefault();
        
        var grid = $("#teller-windows-grid").data("kendoGrid");
        var dataSource = grid.dataSource;
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var deleteUrl = $("#delete-teller-window-url").attr('data-url');

        //Запрос на удаление кассового окна из базы данных
        if (confirm('Вы действительно хотите удалить кассовое окно №' + dataItem.Number + '?')) {
            $.post(deleteUrl, { tellerWindowId: dataItem.Id }, function (result) {
                if (result.status === JsonStatus.ok) {
                    var currentPage = $.calculateCurrentPage(dataSource);
                    //Обновление таблицы кассовых окон и красивый показ сообщения об успехе операции                   
                    dataSource.page(currentPage);                                                                
                    

                    $("#terminals-grid").data("kendoGrid").dataSource.read();

                    $('#current-sector-station-terminal-id').attr('data-current-sector-station-terminal-id', '-1');
                    $("#teller-window-teminal-grid").data("kendoGrid").dataSource.read();
                    $('#add-terminal-to-teller-window').addClass("k-state-disabled");

                    Utils.Notification.show('Кассовое окно №' + dataItem.Number + ' удалено');
                }

                if (result.status === JsonStatus.error) {
                    alert(result.message);
                }
            })
                .error(function () {
                    alert("get actions error");
                });
        }
    };    

    var tellerWindowTerminalDeleteListener = function (e) {
        e.preventDefault();

        var grid = $("#teller-window-teminal-grid").data("kendoGrid");
        var dataSource = grid.dataSource;
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var deleteUrl = $("#delete-teller-window-terminal-url").attr('data-url');

        //Запрос на удаление привязки терминала к окну из базы данных
        if (confirm('Вы действительно хотите удалить привязку терминала №' + dataItem.TerminalNumber +
            ' к окну №' + dataItem.TellerWindowNumber + '?')) {
            $.post(deleteUrl, { tellerWindowTerminalId: dataItem.Id }, function (result) {
                if (result.status === JsonStatus.ok) {
                    var currentPage = $.calculateCurrentPage(dataSource);
                    //Обновление таблицы привязок терминалов к окнам и красивый показ сообщения об успехе операции
                    dataSource.page(currentPage);

                    $("#teller-windows-grid").data("kendoGrid").dataSource.read();

                    Utils.Notification.show('Привязка терминала №' + dataItem.TerminalNumber +
                        ' к окну ' + dataItem.TellerWindowNumber + ' удалена');
                }

                if (result.status === JsonStatus.error) {
                    alert(result.message);
                }
            })
                .error(function () {
                    alert("get actions error");
                });
        }
    };

    var scheduleDeleteListener = function (e) {        
        var grid = $("#schedules-grid").data("kendoGrid");
        var dataSource = grid.dataSource;
        var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));
        var deleteUrl = $("#delete-schedule-url").attr('data-url');

        if (dataItem.ApprovalStatusId !== ScheduleStatus.notReconcile) {
            e.preventDefault();
            return;
        }

        //Запрос на удаление расписания кассира
        if (confirm('Вы действительно хотите удалить данное расписание?')) {
            $.post(deleteUrl, { scheduleDetailId: dataItem.ScheduleDetailId }, function (result) {
                if (result.status === JsonStatus.ok) {
                    var currentPage = $.calculateCurrentPage(dataSource);
                    //Обновление таблицы расписания кассиров
                    dataSource.page(currentPage);

                    Utils.Notification.show('Расписание успешно удалено');
                }

                if (result.status === JsonStatus.error ||
                    result.status === JsonStatus.invalid) {
                    alert(result.message);
                }
            })
                .error(function () {
                    alert("get actions error");
                });
        }
    };

    return {
        tellerWindowDeleteListener: tellerWindowDeleteListener,        
        tellerWindowTerminalDeleteListener: tellerWindowTerminalDeleteListener,
        scheduleDeleteListener: scheduleDeleteListener
    };
}());

//Модуль для управления хранением настроек в куки
var CookieModule = (function () {
    var loadSearchParams = function (selector) {
        var searchParams = $.session.get(selector + '-' + window.location.pathname);
        if (searchParams !== null && searchParams !== undefined) {
            $(selector).deserialize(searchParams);
        }
    };

    var loadGridState = function (selector) {
        var grid = $(selector).data("kendoGrid");

        var gridStateFromSession = $.session.get(selector + '-' + window.location.pathname);
        if (gridStateFromSession !== undefined) {
            var initialState = JSON.parse(gridStateFromSession);
            if (initialState) {
                grid.dataSource.query(initialState);
            } else {
                grid.dataSource.read();
            }
        } else {
            grid.dataSource.read();
        }
    };

    var loadTabIndex = function () {
        var currentQueryString = window.location.pathname;
        var tabStripCurrentIndex = $.session.get('TabStripCurrentIndex-' + currentQueryString);
        var fixedTabStripIndex = $.session.get('FixedTabStripIndex');
        var tabStrip = $("#teller-tabstrip").data("kendoTabStrip");

        //Если есть зафиксированная вкладка, то устанавливаем по умолчанию её,
        //иначе выставляем последнюю выбранную вкладку
        if (fixedTabStripIndex !== null && fixedTabStripIndex !== undefined &&
            fixedTabStripIndex !== '') {
            tabStrip.select(fixedTabStripIndex);
        } else if (tabStripCurrentIndex !== null && tabStripCurrentIndex !== undefined) {
            tabStrip.select(tabStripCurrentIndex);
        } else if (tabStrip.items().length > 0) {
            tabStrip.select(0);
        }

        //Если текущая вкладка нулевая, то нужно принудительно вызвать обработчик выбора вкладки
        //для показа кнопки фиксации вкладки
        var currentIndex = tabStrip.select().index();
        if (currentIndex === 0) {
            tabStrip.trigger("activate");
        }
    };

    var getScheduleDate = function () {
        return $.session.get('scheduleDate' + '-' + window.location.pathname);
    };

    var getProceedsPlanBeginDate = function () {
        return $.session.get("proceedsPlanBeginDate" + '-' + window.location.pathname);
    };

    var getProceedsPlanEndDate = function () {
        return $.session.get("proceedsPlanEndDate" + '-' + window.location.pathname);
    };

    var getSearchParams = function (selector) {
        return $.session.get(selector + '-' + window.location.pathname) || "";
    };

    var getFixedTabIndex = function () {
        return $.session.get('FixedTabStripIndex') || "";
    };

    var saveSearchParams = function (selector) {
        var searchParamsSerialiaze = $(selector).serialize();
        $.session.set(selector + '-' + window.location.pathname, searchParamsSerialiaze);
    };

    var saveScheduleDate = function () {
        var currentScheduleDate = $("#scheduleDateInput").data("kendoDatePicker").value();
        if ((currentScheduleDate !== null) && (!isNaN(currentScheduleDate))) {
            $.session.set('scheduleDate' + '-' + window.location.pathname, currentScheduleDate);
        }
    };

    var saveProceedsPlansDates = function () {
        var currentProceedsPlanBeginDate = $("#proceeds-plan-begin-date").data("kendoDatePicker").value();
        var currentProceedsPlanEndDate = $("#proceeds-plan-end-date").data("kendoDatePicker").value();
        if ((currentProceedsPlanBeginDate !== null) && (!isNaN(currentProceedsPlanBeginDate))) {
            $.session.set("proceedsPlanBeginDate" + '-' + window.location.pathname, currentProceedsPlanBeginDate);
        }
        if ((currentProceedsPlanEndDate !== null) && (!isNaN(currentProceedsPlanEndDate))) {
            $.session.set("proceedsPlanEndDate" + '-' + window.location.pathname, currentProceedsPlanEndDate);
        }
    };

    var saveGridState = function (selector) {
        var dataSource = $(selector).data("kendoGrid").dataSource;

        var currentGridState = kendo.stringify({
            page: dataSource.page(),
            pageSize: dataSource.pageSize(),
            sort: dataSource.sort(),
            group: dataSource.group()
        });

        $.session.set(selector + "-" + window.location.pathname, currentGridState);
    };

    var saveCurrentTabIndex = function () {
        var currentIndex = $("#teller-tabstrip").data("kendoTabStrip").select().index();
        $.session.set('TabStripCurrentIndex-' + window.location.pathname, currentIndex);
    };

    var saveFixedTabIndex = function () {
        var currentIndex = $("#teller-tabstrip").data("kendoTabStrip").select().index();
        $.session.set('FixedTabStripIndex', currentIndex);
    };

    var resetFixedTabIndex = function () {
        $.session.set('FixedTabStripIndex', '');
    };

    return {
        loadSearchParams: loadSearchParams,
        loadGridState: loadGridState,
        loadTabIndex: loadTabIndex,
        getScheduleDate: getScheduleDate,
        getProceedsPlanBeginDate: getProceedsPlanBeginDate,
        getProceedsPlanEndDate: getProceedsPlanEndDate,
        getSearchParams: getSearchParams,
        getFixedTabIndex: getFixedTabIndex,
        saveSearchParams: saveSearchParams,
        saveProceedsPlansDates: saveProceedsPlansDates,
        saveScheduleDate: saveScheduleDate,
        saveGridState: saveGridState,
        saveCurrentTabIndex: saveCurrentTabIndex,
        saveFixedTabIndex: saveFixedTabIndex,
        resetFixedTabIndex: resetFixedTabIndex
    };
}());

//Модуль моделей представления
var ViewModelsModule = (function (cookieModule) {
    var tabStripViewModel = {
        tabFixed: function () {
            $("li.k-state-active input")
                .removeClass("fix-button-tab")
                .addClass("unfix-button-tab")
                .attr("title", "Открепить вкладку")
                .show();
        },
        tabUnfixed: function () {
            $("li.k-state-active input")
                .removeClass("unfix-button-tab")
                .addClass("fix-button-tab")
                .attr("title", "Закрепить вкладку")
                .show();
        },

        fixTabHandler: function (e) {
            var selector = "li.k-state-active input";
            var fixButton = $(selector);
            if (fixButton.length > 0) {
                if ($(selector).hasClass("fix-button-tab")) {
                    tabStripViewModel.tabFixed();
                    cookieModule.saveFixedTabIndex();
                } else {
                    tabStripViewModel.tabUnfixed();
                    cookieModule.resetFixedTabIndex();
                }
            }
        },
        
        defineCurrentTab: function() {
            var selectedItem = $("#teller-tabstrip").data("kendoTabStrip").select();
            
            if (selectedItem.hasClass("teller-window-li")) {
                $(document).trigger("tellerWindowTabActivated");
            } else if (selectedItem.hasClass("terminal-li")) {
                $(document).trigger("terminalTabActivated");
            } else if (selectedItem.hasClass("schedule-li")) {
                $(document).trigger("scheduleTabActivated");
            } else if (selectedItem.hasClass("proceed-plans-li")) {
                $(document).trigger("proceedPlansTabActivated");
            } else if (selectedItem.hasClass("employee-shift-schedule-li")) {
                $(document).trigger("employeeShiftScheduleTabActivated");
            } else if (selectedItem.hasClass("teller-window-schedule-li")) {
                $(document).trigger("tellerWindowScheduleTabActivated");
            }
        }
    };

    var tellerWindowFilterViewModel = {
        tellerWindowNumbersChanged: function () {
            var currentValue = $("#SelectedWindowTellerNumbers").data("kendoMultiSelect").value();
            var initialTellerWindowParams = InitialTellerWindowModule.getInitialTellerWindowParams();

            if (!$.areArraysEqual(initialTellerWindowParams.tellerWindowNumbers, currentValue)) {
                $(".k-multiselect").has("#SelectedWindowTellerNumbers").find("input").addClass("filter-change");
                $("#teller-window-filter-change-div").html('<span class="error-message">' +
                    'Параметры фильтра не применены!</span>');
            } else {
                $(".k-multiselect").has("#SelectedWindowTellerNumbers").find("input").removeClass("filter-change");
                $("#teller-window-filter-change-div").html('');
            }
        },
        beginDateChanged: function () {
            var currentValue = kendo.toString($("#TellerWindowBeginExploitationPeriod").data("kendoDatePicker").value(), "dd.MM.yyyy");
            var initialTellerWindowParams = InitialTellerWindowModule.getInitialTellerWindowParams();

            if (initialTellerWindowParams.beginDate !== currentValue) {
                $("#TellerWindowBeginExploitationPeriod").addClass("filter-change-not-imp");
                $("#teller-window-filter-change-div").html('<span class="error-message">' +
                    'Параметры фильтра не применены!</span>');
            } else {
                $("#TellerWindowBeginExploitationPeriod").removeClass("filter-change-not-imp");
                $("#teller-window-filter-change-div").html('');
            }
        },
        endDateChanged: function () {
            var currentValue = kendo.toString($("#TellerWindowEndExploitationPeriod").data("kendoDatePicker").value(), "dd.MM.yyyy");
            var initialTellerWindowParams = InitialTellerWindowModule.getInitialTellerWindowParams();

            if (initialTellerWindowParams.endDate !== currentValue) {
                $("#TellerWindowEndExploitationPeriod").addClass("filter-change-not-imp");
                $("#teller-window-filter-change-div").html('<span class="error-message">' +
                    'Параметры фильтра не применены!</span>');
            } else {
                $("#TellerWindowEndExploitationPeriod").removeClass("filter-change-not-imp");
                $("#teller-window-filter-change-div").html('');
            }

        }
    };

    var terminalFilterViewModel = {
        //Обработчик изменения номеров терминала на форме фильтрации терминалов
        terminalNumbersChanged: function () {
            var currentValue = $("#SelectedTerminalNumbers").data("kendoMultiSelect").value();
            var initialTerminalParams = InitialTerminalParamsModule.getInitialTerminalParams();

            if (!$.areArraysEqual(initialTerminalParams.terminalNumber, currentValue)) {
                $(".k-multiselect").has("#SelectedTerminalNumbers").find("input").addClass("filter-change");
                $("#terminal-filter-change-div").html('<span class="error-message">' +
                    'Параметры фильтра не применены!</span>');
            } else {
                $(".k-multiselect").has("#SelectedTerminalNumbers").find("input").removeClass("filter-change");                
                $("#terminal-filter-change-div").html('');
            }
        },
        //Обработчик изменения типов терминалов на форме фильтрации терминалов
        terminalTypesChanged: function () {
            var currentValue = $("#SelectedTerminalTypes").data("kendoMultiSelect").value();
            var initialTerminalParams = InitialTerminalParamsModule.getInitialTerminalParams();

            if (!$.areArraysEqual(initialTerminalParams.terminalType, currentValue)) {
                $(".k-multiselect").has("#SelectedTerminalTypes").find("input").addClass("filter-change");
                $("#terminal-filter-change-div").html('<span class="error-message">' +
                    'Параметры фильтра не применены!</span>');
            } else {
                $(".k-multiselect").has("#SelectedTerminalTypes").find("input").removeClass("filter-change");
                $("#terminal-filter-change-div").html('');
            }
        },
        //Обработчик изменения номеров кассовых окон, к которым привязаны терминалы, на форме фильтрации терминалов
        selectedWindowTellerNumberChanged: function () {
            var currentValue = $("#SelectedWindowTellerNumbersInTerminalTab").data("kendoMultiSelect").value();
            var initialTerminalParams = InitialTerminalParamsModule.getInitialTerminalParams();

            if (!$.areArraysEqual(initialTerminalParams.selectedWindowTellerNumber, currentValue)) {
                $(".k-multiselect").has("#SelectedWindowTellerNumbersInTerminalTab").find("input").addClass("filter-change");
                $("#terminal-filter-change-div").html('<span class="error-message">' +
                    'Параметры фильтра не применены!</span>');
            } else {
                $(".k-multiselect").has("#SelectedWindowTellerNumbersInTerminalTab").find("input").removeClass("filter-change");
                $("#terminal-filter-change-div").html('');
            }
        },
        //Обработчик изменения радиобаттонов "Показывать текущие"/"Показывать все" на форме фильтрации терминалов
        isShowCurrentChanged: function () {
            var currentValue = $('input[name=IsShowCurrent]:checked').val();
            var initialTerminalParams = InitialTerminalParamsModule.getInitialTerminalParams();

            if (initialTerminalParams.isShowCurrent !== currentValue) {
                $("#terminal-show-current-radiogroup").addClass("filter-change");
                $("#terminal-filter-change-div").html('<span class="error-message">' +
                    'Параметры фильтра не применены!</span>');
            } else {
                $("#terminal-show-current-radiogroup").removeClass("filter-change");
                $("#terminal-filter-change-div").html('');
            }
        },
        //Обработчик изменения "Дата с" на форме фильтрации терминалов
        beginDateChanged: function () {
            var isShowCurrent = $('input[name=IsShowCurrent]:checked').val().toLowerCase() === "true";
            if (!isShowCurrent) {
                var currentValue = kendo.toString($("#TerminalBeginExploitationPeriod").data("kendoDatePicker").value(), "dd.MM.yyyy");
                var initialTerminalParams = InitialTerminalParamsModule.getInitialTerminalParams();
                var initialValue = initialTerminalParams.beginDate;

                if (initialValue !== currentValue) {
                    $("#TerminalBeginExploitationPeriod").addClass("filter-change-not-imp");
                    $("#terminal-filter-change-div").html('<span class="error-message">' +
                        'Параметры фильтра не применены!</span>');
                } else {
                    $("#TerminalBeginExploitationPeriod").removeClass("filter-change-not-imp");
                    $("#terminal-filter-change-div").html('');
                }
            }
        },
        //Обработчик изменения "Дата по" на форме фильтрации терминалов
        endDateChanged: function () {
            var isShowCurrent = $('input[name=IsShowCurrent]:checked').val().toLowerCase() === "true";
            if (!isShowCurrent) {
                var currentValue = kendo.toString($("#TerminalEndExploitationPeriod").data("kendoDatePicker").value(), "dd.MM.yyyy");
                var initialTerminalParams = InitialTerminalParamsModule.getInitialTerminalParams();
                var initialValue = initialTerminalParams.endDate;

                if (initialValue !== currentValue) {
                    $("#TerminalEndExploitationPeriod").addClass("filter-change-not-imp");
                    $("#terminal-filter-change-div").html('<span class="error-message">' +
                        'Параметры фильтра не применены!</span>');
                } else {
                    $("#TerminalEndExploitationPeriod").removeClass("filter-change-not-imp");
                    $("#terminal-filter-change-div").html('');
                }
            }
        }
    };

    var scheduleViewModel = {
        scheduleDate: function () {
            var savedScheduleDate = CookieModule.getScheduleDate();
            if (savedScheduleDate) {
                return new Date(savedScheduleDate);
            }

            var year = parseInt($("#model-year").attr("data-value"));
            var month = parseInt($("#model-month").attr("data-value"));
            var date = new Date(year, month - 1, 1);
            return date;
        },
        //Изменяем текущую дату расписания
        changeScheduleDate: function () {
            var scheduleDateInput = $("#scheduleDateInput");

            Utils.Validation.validateMonthDatePicker(scheduleDateInput, function () {
                var schedulesGridDataSource = $('#schedules-grid').data("kendoGrid").dataSource;
                var tellerWindowSchedulesGridDataSource = $('#tellerWindowSchedules-grid').data("kendoGrid").dataSource;
                schedulesGridDataSource.page(1);
                tellerWindowSchedulesGridDataSource.read();
            });
        },
    };

    var proceedsPlanViewModel = {
        proceedsPlanGridReadData: function () {
            var invalidDatePickersCount = $("#proceeds-plan-search-params .grok-validation-error");

            //Если нет ошибок валидации при вооде дат
            if (invalidDatePickersCount && invalidDatePickersCount.length === 0) {
                var beginPeriod = $("#proceeds-plan-begin-date").data("kendoDatePicker").value();
                var endPeriod = $("#proceeds-plan-end-date").data("kendoDatePicker").value();

                //Если дата начала периода не больше даты конца периода
                if ((beginPeriod != null) && (endPeriod != null)) {
                    if (beginPeriod > endPeriod) {
                        $("#proceeds-plan-filter-change-div").html('<span class="error-message">' +
                            'Дата начала периода превосходит его конец' +
                            '</span>');
                        return;
                    }
                }

                $("#proceeds-plan-filter-change-div").html('');
                var proceedsPlanGridDataSource = $("#proceeds-plan-grid").data("kendoGrid").dataSource;
                proceedsPlanGridDataSource.page(1);
            }
        },

        //Функция, вычисляющая начальную дату месяца по умолчанию
        proceedsPlanBeginDate: function () {
            var savedProceedsPlanBeginDate = CookieModule.getProceedsPlanBeginDate();
            if (savedProceedsPlanBeginDate) {
                return new Date(savedProceedsPlanBeginDate);
            }

            var date = new Date();
            date.setMonth(date.getMonth() - 6);

            return date;
        },
        //Функция, вычисляющая конечную дату месяца по умолчанию
        proceedsPlanEndDate: function () {
            var savedProceedsPlanEndDate = CookieModule.getProceedsPlanEndDate();
            if (savedProceedsPlanEndDate) {
                return new Date(savedProceedsPlanEndDate);
            }

            var date = new Date();
            date.setMonth(date.getMonth() + 6);

            return date;
        },
        //Функция, инициирубщая фильтрацию в таблице планов по выручке
        //при изменении начального месяца периода
        changeBeginMonth: function () {
            var proceedsPlanBeginDateInput = $("#proceeds-plan-begin-date");

            Utils.Validation.validateMonthDatePicker(proceedsPlanBeginDateInput, this.proceedsPlanGridReadData);
        },
        //Функция, инициирующая фильтрацию в таблице планов по выручке
        //при изменени конечного месяца периода
        changeEndMonth: function () {
            var proceedsPlanEndDateInput = $("#proceeds-plan-end-date");

            Utils.Validation.validateMonthDatePicker(proceedsPlanEndDateInput, this.proceedsPlanGridReadData);
        }
    };

    var shiftScheduleViewModel = {
        //Всплывающее окно
        popupWindow: {},
        //Всплывающее окно
        popupNumberWindow: {},
        //Добавление смены
        addShift: function () {
            $.ajax({
                url: $("#create-employee-shift").attr("data-url"),
                type: "POST",
                data: { stationId: $("#station-id").attr("data-value") },
                success: function () {
                    var dataSource = $('#employeeShiftScheduleGrid').data("kendoGrid").dataSource;

                    var expanded = [];
                    var master = $("#employeeShiftScheduleGrid .k-master-row");
                    for (var i = 0; i < master.length; i++) {
                        var next = $(master[i]).next();
                        if ((next.hasClass("k-detail-row")) && (next.css('display') != "none")) {
                            expanded.push(i);
                        }
                    }
                    $.session.set('ExpandedShifts', expanded);

                    dataSource.read();
                },
            });
        },
        //Удаление смены
        deleteShift: function (e) {
            if (confirm("Вы действительно хотите удалить смену?")) {
                var $tr = $(e).closest("tr");
                var grid = $('#employeeShiftScheduleGrid').data("kendoGrid");
                var dataItem = grid.dataItem($tr);
                var id = dataItem.Id;

                $.ajax({
                    url: $("#delete-employee-shift").attr("data-url"),
                    type: "POST",
                    data: { scheduleId: id },
                    success: function () {
                        var dataSource = $('#employeeShiftScheduleGrid').data("kendoGrid").dataSource;

                        var expanded = [];
                        var master = $("#employeeShiftScheduleGrid .k-master-row");
                        for (var i = 0; i < master.length; i++) {
                            var next = $(master[i]).next();
                            if ((next.hasClass("k-detail-row")) && (next.css('display') != "none")) {
                                expanded.push(i);
                            }
                        }
                        $.session.set('ExpandedShifts', expanded);

                        dataSource.read();
                    },
                });
            }
        },
        //Изменение номера смены
        changeShift: function (e) {
            var $tr = $(e).closest("tr");
            var grid = $('#employeeShiftScheduleGrid').data("kendoGrid");
            var dataItem = grid.dataItem($tr);
            var id = dataItem.Id;

            $("#changeShiftNumberId").html(id);
            $("#changeShiftNumberInput").data("kendoNumericTextBox").value(null);
            shiftScheduleViewModel.popupNumberWindow.open();
        },
        //Изменение номера смены
        changeShiftSubmit: function (e) {

            var id = $("#changeShiftNumberId")[0].innerHTML;
            var number = $("#changeShiftNumberInput")[0].value;

            for (var i = 0; i < $('#employeeShiftScheduleGrid').data("kendoGrid")._data.length;i++) {
                var item = $('#employeeShiftScheduleGrid').data("kendoGrid")._data[i];
                if ((item.Id != id) && (item.Name === number.toString())) {
                    alert("Смена с таким номером уже существует");
                    return;
                }
            }

            $.ajax({
                url: $("#change-employee-shift-number").attr("data-url"),
                type: "POST",
                data: { scheduleId: id, number: number },
                success: function () {
                    shiftScheduleViewModel.popupNumberWindow.close();

                    var dataSource = $('#employeeShiftScheduleGrid').data("kendoGrid").dataSource;

                    var expanded = [];
                    var master = $("#employeeShiftScheduleGrid .k-master-row");
                    for (var i = 0; i < master.length; i++) {
                        var next = $(master[i]).next();
                        if ((next.hasClass("k-detail-row")) && (next.css('display') != "none")) {
                            expanded.push(i);
                        }
                    }
                    $.session.set('ExpandedShifts', expanded);

                    dataSource.read();
                    Utils.Notification.show("Изменения сохранены");
                },
            });
        },
        //Изменение номера смены
        changeShiftReject: function (e) {
            shiftScheduleViewModel.popupNumberWindow.close();
        },
        //Изменение расписания смены
        changeShiftShedule: function (e) {
            var $tr = $(e).closest("tr");
            var grid = $($tr).parents("[data-role=grid]").data("kendoGrid");
            var dataItem = grid.dataItem($tr);

            shiftScheduleId = dataItem.Id;
            var time;
            if (dataItem.BeginWorkTime) {
                time = new Date(0);
                time.setHours(dataItem.BeginWorkTime.split(':')[0]);
                time.setMinutes(dataItem.BeginWorkTime.split(':')[1]);
                $("#shift-timepicker-beginWork").data("kendoTimePicker").value(time);
            } else
                $("#shift-timepicker-beginWork").data("kendoTimePicker").value(null);

            if (dataItem.EndWorkTime) {
                time = new Date(0);
                time.setHours(dataItem.EndWorkTime.split(':')[0]);
                time.setMinutes(dataItem.EndWorkTime.split(':')[1]);
                $("#shift-timepicker-endWork").data("kendoTimePicker").value(time);
            } else {
                $("#shift-timepicker-endWork").data("kendoTimePicker").value(null);
            }

            if (dataItem.BeginBreakTime) {
                time = new Date(0);
                time.setHours(dataItem.BeginBreakTime.split(':')[0]);
                time.setMinutes(dataItem.BeginBreakTime.split(':')[1]);
                $("#shift-timepicker-beginBreak").data("kendoTimePicker").value(time);
            } else {
                $("#shift-timepicker-beginBreak").data("kendoTimePicker").value(null);
            }

            if (dataItem.EndBreakTime) {
                time = new Date(0);
                time.setHours(dataItem.EndBreakTime.split(':')[0]);
                time.setMinutes(dataItem.EndBreakTime.split(':')[1]);
                $("#shift-timepicker-endBreak").data("kendoTimePicker").value(time);
            } else {
                $("#shift-timepicker-endBreak").data("kendoTimePicker").value(null);
            }

            shiftScheduleViewModel.popupWindow.open();
        },
        //Подтверждение изменения расписания смены
        changeShiftSheduleSubmit: function (e) {
            var beginWorkTime = resetTimeSpan($("#shift-timepicker-beginWork").data("kendoTimePicker").value());
            var endWorkTime = resetTimeSpan($("#shift-timepicker-endWork").data("kendoTimePicker").value());
            var beginBreakTime = resetTimeSpan($("#shift-timepicker-beginBreak").data("kendoTimePicker").value());
            var endBreakTime = resetTimeSpan($("#shift-timepicker-endBreak").data("kendoTimePicker").value());

            var beginWorkTimeS = "";
            var endWorkTimeS = "";
            var beginBreakTimeS = "";
            var endBreakTimeS = "";

            if (beginWorkTime || endWorkTime) {
                if (!(beginWorkTime && endWorkTime)) {
                    alert("Рабоче время не может быть открытым отрезком времени");
                    return;
                }
                beginWorkTimeS = beginWorkTime.getHours() + ":" + beginWorkTime.getMinutes();
                endWorkTimeS = endWorkTime.getHours() + ":" + endWorkTime.getMinutes();
            }

            if (beginBreakTime || endBreakTime) {
                if (!(beginBreakTime && endBreakTime)) {
                    alert("Обед не может быть открытым отрезком времени");
                    return;
                }

                if (!(beginWorkTime)) {
                    alert("Обед не может быть без рабочего времени");
                    return;
                }

                beginBreakTimeS = beginBreakTime.getHours() + ":" + beginBreakTime.getMinutes();
                endBreakTimeS = endBreakTime.getHours() + ":" + endBreakTime.getMinutes();

                if (beginWorkTime.getTime() >= endWorkTime.getTime()) {
                    endWorkTime.setDate(endWorkTime.getDate() + 1);
                    if (beginBreakTime.getTime() <= beginWorkTime.getTime()) {
                        beginBreakTime.setDate(beginBreakTime.getDate() + 1);
                        endBreakTime.setDate(endBreakTime.getDate() + 1);
                    }
                }
                if (endBreakTime.getTime() <= beginBreakTime.getTime()) {
                    endBreakTime.setDate(endBreakTime.getDate() + 1);
                }

                if (
                    (endBreakTime.getTime() > endWorkTime.getTime()) ||
                    (beginBreakTime.getTime() < beginWorkTime.getTime()) ||
                    (endBreakTime.getTime() <= beginBreakTime.getTime())
                ) {
                    alert("Обед не может быть вне рабочего времени");
                    return;
                }
            }

            $.ajax({
                url: $("#set-employee-shift-schedule").attr("data-url"),
                type: "POST",
                data: {
                    shiftScheduleId: shiftScheduleId,
                    beginWorkTime: beginWorkTimeS,
                    endWorkTime: endWorkTimeS,
                    beginBreakTime: beginBreakTimeS,
                    endBreakTime: endBreakTimeS,
                    stationId: $("#station-id").attr("data-value")
                },
                success: function () {
                    shiftScheduleViewModel.popupWindow.close();
                    var dataSource = $('#employeeShiftScheduleGrid').data("kendoGrid").dataSource;

                    var expanded = [];
                    var master = $("#employeeShiftScheduleGrid .k-master-row");
                    for (var i = 0; i < master.length; i++) {
                        var next = $(master[i]).next();
                        if ((next.hasClass("k-detail-row")) && (next.css('display') != "none")) {
                            expanded.push(i);
                        }
                    }
                    $.session.set('ExpandedShifts', expanded);

                    dataSource.read();
                    Utils.Notification.show("Изменения сохранены");
                },
            });


        },
        //Отмена изменения расписания смены
        changeShiftSheduleReject: function (e) {
            shiftScheduleViewModel.popupWindow.close();
        },
        //Редактируемое время  смены
        shiftScheduleId: 0,
        //Редактируемое время начала смены
        beginWorkTime: "",
        //Редактируемое время конца смены
        endWorkTime: "",
        //Редактируемое время начала перерыва смены
        beginBreakTime: "",
        //Редактируемое время конца перерыва смены
        endBreakTime: "",
        init: function() {
            //Окно для внесения изменений
            shiftScheduleViewModel.popupWindow = $("#change-shift-popup").kendoWindow({
                actions: {

                },
                minWidth:
                    "450px",
                minHeight:
                    "130px",
                maxWidth:
                    "450px",
                maxHeight:
                    "130px",
                modal:
                    true,
                visible:
                    false,
                close:
                    function () {
                    }
            }).data("kendoWindow");

            shiftScheduleViewModel.popupNumberWindow = $("#change-shift-number-popup").kendoWindow({
                actions: {

                },
                minWidth:
                    "200px",
                minHeight:
                    "130px",
                maxWidth:
                    "200px",
                maxHeight:
                    "130px",
                modal:
                    true,
                visible:
                    false,
                close:
                    function () {
                    }
            }).data("kendoWindow");

            $("#changeShiftNumberInput").kendoNumericTextBox({
                min: 1,
                format: "#",
                decimals: 0,
                step: 1,
                width: 40
            });

            if (shiftScheduleViewModel.popupWindow) {
                shiftScheduleViewModel.popupWindow.center();
            }
            if (shiftScheduleViewModel.popupNumberWindow) {
                shiftScheduleViewModel.popupNumberWindow.center();
            }
        }
    };               

    return {
        tabStripViewModel: tabStripViewModel,
        tellerWindowFilterViewModel: tellerWindowFilterViewModel,
        terminalFilterViewModel: terminalFilterViewModel,
        scheduleViewModel: scheduleViewModel,
        proceedsPlanViewModel: proceedsPlanViewModel,
        shiftScheduleViewModel: shiftScheduleViewModel        
    };
}(CookieModule));

//Модуль обработчиков собственных событий (преимущественно событий активации вкладок)
var CustomEventsHandlersModule = (function (cookieModule) {
    var tellerWindowTabActivatedHandler = function() {
        cookieModule.loadGridState("#teller-windows-grid");
        $(document).off("tellerWindowTabActivated");
    };

    var terminalTabActivatedHandler = function () {
        CookieModule.loadGridState("#terminals-grid");
        $(document).off("terminalTabActivated");
    };

    var scheduleTabActivatedHandler = function () {
        CookieModule.loadGridState("#schedules-grid");
        var grid = $("#tellerWindowSchedules-grid").data("kendoGrid"); 
        if (grid) {
            grid.dataSource.read();
        }
        $(document).off("scheduleTabActivated");
    };

    var proceedPlansTabActivatedHandler = function () {
        CookieModule.loadGridState("#proceeds-plan-grid");
        $(document).off("proceedPlansTabActivated");
    };

    var employeeShiftScheduleTabActivatedHandler = function () {
        var grid = $("#employeeShiftScheduleGrid").data("kendoGrid");
        if (grid) {
            grid.dataSource.read();
        }
        $(document).off("employeeShiftScheduleTabActivated");
    };

    var tellerWindowScheduleTabActivatedHandler = function () {
        var grid = $("#teller-window-schedules").data("kendoGrid");
        if (grid) {
            grid.dataSource.read();
        }        
        $(document).off("tellerWindowScheduleTabActivated");
    };

    var init = function() {
        $(document).on("tellerWindowTabActivated", tellerWindowTabActivatedHandler);
        $(document).on("terminalTabActivated", terminalTabActivatedHandler);
        $(document).on("scheduleTabActivated", scheduleTabActivatedHandler);
        $(document).on("proceedPlansTabActivated", proceedPlansTabActivatedHandler);
        $(document).on("employeeShiftScheduleTabActivated", employeeShiftScheduleTabActivatedHandler);
        $(document).on("tellerWindowScheduleTabActivated", tellerWindowScheduleTabActivatedHandler);
    };

    return {
        tellerWindowTabActivatedHandler: tellerWindowTabActivatedHandler,
        terminalTabActivatedHandler: terminalTabActivatedHandler,
        scheduleTabActivatedHandler: scheduleTabActivatedHandler,
        proceedPlansTabActivatedHandler: proceedPlansTabActivatedHandler,
        employeeShiftScheduleTabActivatedHandler: employeeShiftScheduleTabActivatedHandler,
        tellerWindowScheduleTabActivatedHandler: tellerWindowScheduleTabActivatedHandler,
        init: init
    };
}(CookieModule));

//Модуль для операций с графиками кассиров
var ScheduleModule = (function () {
    var add = function () {
        var baseUri = $("#addSchedule-url").attr("data-url");
        var date = $("#scheduleDateInput").data("kendoDatePicker").value();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        window.location.href = baseUri + "&year=" + year + "&month=" + month;
    };

    var exportClick = function (e) {
        var grid = $("#schedules-grid");
        var gridData = grid.data("kendoGrid").dataSource.data();
        var totalNumber = gridData.length;

        if (totalNumber === 0) {
            e.preventDefault();
            return;
        }

        $("#scheduleTab").mask("Формирование экспортного файла...");

        var exportUrl = $("#export-url").attr("data-url");
        var exportButton = $("#schedileExportLink");
        $.fileDownload(exportUrl + "/" + exportButton.attr("scheduleId"),
            {
                successCallback: function (url) {
                    $("#scheduleTab").unmask();
                },
                failCallback: function (html, url) {
                    $("#scheduleTab").unmask();
                }
            });

    };

    var sendReconcileListener = function (e) {
        var grid = $("#schedules-grid");
        var gridDataSource = grid.data("kendoGrid");
        var data = gridDataSource.dataSource;
        var dataItem = gridDataSource.dataItem(grid.find("tr:first"));
        var sendreconcileScheduleUrl = $("#send-to-reconcile-schedule-url").attr("data-url");

        //Запрос на отправку графиков на согласование
        if (confirm('Вы действительно хотите отправить на согласование данное расписание?')) {
            $.post(sendreconcileScheduleUrl, { scheduleId: dataItem.ScheduleId }, function (result) {
                if (result.status === JsonStatus.ok) {
                    //Обновление таблицы расписания кассиров
                    data.read();

                    Utils.Notification.show('Расписание отправлено на согласование');
                }

                if (result.status === JsonStatus.error ||
                    result.status === JsonStatus.invalid) {
                    alert(result.message);
                }
            })
                .error(function () {
                    alert("get actions error");
                });
        }
    };

    var confirmReconcileListener = function (e) {
        var grid = $("#schedules-grid");
        var gridDataSource = grid.data("kendoGrid");
        var data = gridDataSource.dataSource;
        var dataItem = gridDataSource.dataItem(grid.find("tr:first"));

        var url;
        var successMessage;

        if (dataItem.ApprovalStatusId === ScheduleStatus.reconciledBySectionChief) {
            url = $("#confirm-reconcile-schedule-by-section-chief").attr("data-url");
            successMessage = 'Расписание согласовано начальником участка';
        } else if (dataItem.ApprovalStatusId === ScheduleStatus.reconciledByProfessionalGroupOrganizer) {
            url = $("#confirm-schedule-reconcile-by-professional-group-orginizer").attr("data-url");
            successMessage = 'Расписание согласовано!';
        } else {
            alert("Не подходящий статус согласования");
            return;
        }

        //Запрос на согласование графика                        
        if (confirm('Вы действительно хотите подтвердить согласование данного расписания?')) {
            $.post(url, { scheduleId: dataItem.ScheduleId }, function (result) {
                if (result.status === JsonStatus.ok) {
                    //Обновление таблицы расписания кассиров
                    data.read();

                    Utils.Notification.show(successMessage);
                }

                if (result.status === JsonStatus.error ||
                    result.status === JsonStatus.invalid) {
                    alert(result.message);
                }
            })
                .error(function () {
                    alert("get actions error");
                });
        }
    };

    var cancelReconcileListener = function (e) {
        var grid = $("#schedules-grid");
        var gridDataSource = grid.data("kendoGrid");
        var data = gridDataSource.dataSource;
        var dataItem = gridDataSource.dataItem(grid.find("tr:first"));
        var cancelReconcileScheduleUrl = $("#cancel-reconcile-schedule-url").attr("data-url");

        //Запрос на рассогласование расписания кассира
        if (confirm('Вы действительно хотите отменить согласование данного расписания?')) {
            $.post(cancelReconcileScheduleUrl, { scheduleId: dataItem.ScheduleId }, function (result) {
                if (result.status === JsonStatus.ok) {
                    //Обновление таблицы расписания кассиров
                    data.read();

                    Utils.Notification.show('Статус расписания изменён');
                }

                if (result.status === JsonStatus.error ||
                    result.status === JsonStatus.invalid) {
                    alert(result.message);
                }
            })
                .error(function () {
                    alert("get actions error");
                });
        }
    };

    return {
        add: add,
        exportClick: exportClick,
        sendReconcileListener: sendReconcileListener,
        confirmReconcileListener: confirmReconcileListener,
        cancelReconcileListener: cancelReconcileListener
    };
}());

//Модуль фильтрующих функций
var FilterModule = (function (cookieModule, initialTellerWindowModule, initialTerminalParamsModule) {
    var tellerWindowFilter = function (e) {
        if ($('#cash-boxes-search-params .grok-validation-error').length === 0) {

            //Проверка правильности начала и конца периода запроса
            var beginPeriod = $("#TellerWindowBeginExploitationPeriod").data("kendoDatePicker").value();
            var endPeriod = $("#TellerWindowEndExploitationPeriod").data("kendoDatePicker").value();
            if ((beginPeriod != null) && (endPeriod != null))
                if ($("#TellerWindowBeginExploitationPeriod").data("kendoDatePicker").value() > $("#TellerWindowEndExploitationPeriod").data("kendoDatePicker").value()) {
                    $("#teller-window-filter-change-div").html('<span class="error-message">' +
                        'Дата начала периода превосходит его конец' +
                        '</span>');
                    return;
                }

            //Снимаем подсветку о неприменённости фильтра
            $("#cash-boxes-search-params .filter-change").removeClass("filter-change");
            $("#cash-boxes-search-params .filter-change-not-imp").removeClass("filter-change-not-imp");
            //Убираем сообщение о неприменённости фильтра
            $("#teller-window-filter-change-div").html('');
            //Фиксируем текущие значения формы фильтрации как начальные
            initialTellerWindowModule.init();

            //Кладём текущие значения формы фильтрации кассовых окон в куки
            cookieModule.saveSearchParams("#cash-boxes-search-params");

            var gridDataSource = $("#teller-windows-grid").data("kendoGrid");
            var data = gridDataSource.dataSource;
            data.page(1);
        }
    };

    var tellerWindowResetFilter = function (e) {
        //Убираем ограничения
        $("#TellerWindowEndExploitationPeriod").data("kendoDatePicker").min(new Date(1950, 0, 1));
        $("#TellerWindowBeginExploitationPeriod").data("kendoDatePicker").max(new Date(2049, 11, 31));

        //Очищаем контролы формы фильтрации кассовых окон
        $("#SelectedWindowTellerNumbers").data("kendoMultiSelect").value([]);
        $("#TellerWindowBeginExploitationPeriod").data("kendoDatePicker").value('');
        $("#TellerWindowEndExploitationPeriod").data("kendoDatePicker").value('');

        //Снимаем подсветку о неприменённости фильтра
        $("#cash-boxes-search-params .filter-change").removeClass("filter-change");
        $("#cash-boxes-search-params .filter-change-not-imp").removeClass("filter-change-not-imp");
        //Убираем сообщение о неприменённости фильтра
        $("#teller-window-filter-change-div").html('');
        //Фиксируем текущие значения формы фильтрации как начальные
        initialTellerWindowModule.init();

        //Кладём текущие значения формы фильтрации кассовых окон в куки
        cookieModule.saveSearchParams("#cash-boxes-search-params");

        var gridDataSource = $("#teller-windows-grid").data("kendoGrid");
        var data = gridDataSource.dataSource;
        data.page(1);
    };

    var terminalFilter = function (e) {
        if ($('#terminal-search-params .grok-validation-error').length === 0) {
            var isShowCurrent = $('input[name=IsShowCurrent]:checked').val().toLowerCase() === "true";
            //Если фильтрация идёт только по действующим терминалам
            if (isShowCurrent) {
                //Очищаем date-picker-ы "Дата с" и "Дата по"
                $("#TerminalBeginExploitationPeriod").data("kendoDatePicker").value('');
                $("#TerminalEndExploitationPeriod").data("kendoDatePicker").value('');
            } else {
                //Проверка правильности начала и конца периода запроса
                var beginPeriod = $("#TerminalBeginExploitationPeriod").data("kendoDatePicker").value();
                var endPeriod = $("#TerminalEndExploitationPeriod").data("kendoDatePicker").value();
                if ((beginPeriod != null) && (endPeriod != null))
                    if ($("#TerminalBeginExploitationPeriod").data("kendoDatePicker").value() > $("#TerminalEndExploitationPeriod").data("kendoDatePicker").value()) {
                        $("#terminal-filter-change-div").html('<span class="error-message">' +
                            'Дата начала периода превосходит его конец' +
                            '</span>');
                        return;
                    }
            }

            //Снимаем подсветку о неприменённости фильтра
            $("#terminal-search-params .filter-change").removeClass("filter-change");
            $("#terminal-search-params .filter-change-not-imp").removeClass("filter-change-not-imp");
            //Убираем сообщение о неприменённости фильтра
            $("#terminal-filter-change-div").html('');
            //Фиксируем текущие значения формы фильтрации как начальные
            initialTerminalParamsModule.init();

            //Кладём текущие значения формы фильтрации терминалов в куки
            cookieModule.saveSearchParams("#terminal-search-params");

            var gridDataSource = $("#terminals-grid").data("kendoGrid");
            var data = gridDataSource.dataSource;
            data.page(1);
        }
    };

    var resetTerminalFilter = function () {
        //Убираем ограничения
        $("#TerminalEndExploitationPeriod").data("kendoDatePicker").min(new Date(1950, 0, 1));
        $("#TerminalBeginExploitationPeriod").data("kendoDatePicker").max(new Date(2049, 11, 31));

        //Очищаем контролы формы фильтрации терминалов (привязок терминалов к станции)
        $("#SelectedTerminalNumbers").data("kendoMultiSelect").value([]);
        $("#SelectedTerminalTypes").data("kendoMultiSelect").value([]);
        $("#SelectedWindowTellerNumbersInTerminalTab").data("kendoMultiSelect").value([]);

        //Делаем недоступными контролы для фильтрации по дате ввода и вывода терминалов
        $("#TerminalBeginExploitationPeriod").data("kendoDatePicker").value('');
        $("#TerminalBeginExploitationPeriod").data("kendoDatePicker").enable(false);

        $("#TerminalEndExploitationPeriod").data("kendoDatePicker").value('');
        $("#TerminalEndExploitationPeriod").data("kendoDatePicker").enable(false);

        //Устанавливаем поиск только текущих привязок терминалов к станции
        $("#terminal-show-current-radiogroup .filterShowCurrent").prop('checked', true);

        //Снимаем подсветку о неприменённости фильтра
        $("#terminal-search-params .filter-change").removeClass("filter-change");
        $("#terminal-search-params .filter-change-not-imp").removeClass("filter-change-not-imp");
        //Убираем сообщение о неприменённости фильтра
        $("#terminal-filter-change-div").html('');
        //Фиксируем текущие значения формы фильтрации как начальные
        InitialTerminalParamsModule.init();

        //Кладём текущие значения формы фильтрации терминалов в куки
        cookieModule.saveSearchParams("#terminal-search-params");

        var gridDataSource = $("#terminals-grid").data("kendoGrid");
        var data = gridDataSource.dataSource;
        data.page(1);
    };

    return {
        tellerWindowFilter: tellerWindowFilter,
        tellerWindowResetFilter: tellerWindowResetFilter,
        terminalFilter: terminalFilter,
        resetTerminalFilter: resetTerminalFilter
    };
}(CookieModule, InitialTellerWindowModule, InitialTerminalParamsModule));

//Модуль манипулирования интерфесом страницы - разметкой, стилями и контролами
var InterfaceModule = (function (cookieModule, viewModelsModule) {
    var isMouseOverAnchor;

    var initDatePickersConstraints = function () {
        $("#scheduleDateInput").kendoDatePicker({
            start: "year",
            depth: "year",
            format: "MMMM yyyy"
        });

        $("#TellerWindowBeginExploitationPeriod, #TellerWindowEndExploitationPeriod, #TerminalBeginExploitationPeriod, #TerminalEndExploitationPeriod").kendoDatePicker({
            start: "day",
            depth: "day",
            format: "dd.MM.yyyy"
        });
    };

    var setProceedsPlanGridEditableProperty = function () {
        $("#proceeds-plan-grid").attr("data-editable",
                                  $("#is-proceeds-plans-grid-editable").attr("data-is-editable").toLowerCase());
    };    

    var mouseOverEditTerminalListener = function (e) {
        InterfaceModule.isMouseOverAnchor = true;
    };

    var mouseOutEditTerminalListener = function (e) {
        InterfaceModule.isMouseOverAnchor = false;
    };

    var terminalSelected = function (e) {
        //Если курсор не находится над иконкой "Редактировать"
        if (InterfaceModule.isMouseOverAnchor === false) {

            var selectedRow = $("#terminals-grid").data("kendoGrid").select();
            var gridDataSource = $("#terminals-grid").data("kendoGrid");
            var dataItem = gridDataSource.dataItem(selectedRow);
            var currentDate = $("#current-date-str").attr("data-date");

            //Инициализируем ссылку добавления привязки терминала к окну и 
            //делаем её активной
            var url = $('#add-terminal-to-teller-window-url').attr('data-url') +
                '?sectorStationTerminalId=' + dataItem.SectorStationTerminalId +
                '&currentDate=' + currentDate;
            $('#add-terminal-to-teller-window').attr('href', url);
            $('#add-terminal-to-teller-window').removeClass("k-state-disabled");

            //Загружаем данные для выделенного терминала в таблицу привязок 
            $('#current-sector-station-terminal-id').attr('data-current-sector-station-terminal-id', dataItem.SectorStationTerminalId);
            var tellerWindowTerminalGridSource = $('#teller-window-teminal-grid').data("kendoGrid").dataSource;
            tellerWindowTerminalGridSource.page(1);
        }
    };

    var tabStripActivateHandler = function () {
        cookieModule.saveCurrentTabIndex();

        var fixedTabIndex = cookieModule.getFixedTabIndex();
        var currentTabIndexStr = $("#teller-tabstrip").data("kendoTabStrip").select().index().toString();

        $("li input").hide();

        if (fixedTabIndex === currentTabIndexStr) {
            viewModelsModule.tabStripViewModel.tabFixed();
        } else {
            viewModelsModule.tabStripViewModel.tabUnfixed();
        }
        
        //Загрузить данные для активной вкладки
        viewModelsModule.tabStripViewModel.defineCurrentTab();
    };

    var showOnlyCurrent = function () {
        $("#TerminalBeginExploitationPeriod").data("kendoDatePicker").enable(false);
        $("#TerminalEndExploitationPeriod").data("kendoDatePicker").enable(false);

        viewModelsModule.terminalFilterViewModel.isShowCurrentChanged();
    };

    var showAll = function () {
        $("#TerminalBeginExploitationPeriod").data("kendoDatePicker").enable(true);
        $("#TerminalEndExploitationPeriod").data("kendoDatePicker").enable(true);

        viewModelsModule.terminalFilterViewModel.isShowCurrentChanged();
    };

    var checkButtonsAvailAbility = function () {
        if ($('#are-buttons-disabled').attr('data-are-disabled').toLowerCase() === "true") {
            $('a.k-button').not("#schedileExportLink").hide();
        }

        //Накладываем css-стиль неактивности на неактивные кнопки на вкладке графиков
        $("#scheduleButtonsPanel input:disabled").addClass("k-state-disabled");
    };

    return {
        isMouseOverAnchor: isMouseOverAnchor,
        initDatePickersConstraints: initDatePickersConstraints,
        setProceedsPlanGridEditableProperty: setProceedsPlanGridEditableProperty,        
        mouseOverEditTerminalListener: mouseOverEditTerminalListener,
        mouseOutEditTerminalListener: mouseOutEditTerminalListener,
        terminalSelected: terminalSelected,
        tabStripActivateHandler: tabStripActivateHandler,
        showOnlyCurrent: showOnlyCurrent,
        showAll: showAll,
        checkButtonsAvailAbility: checkButtonsAvailAbility
    };
}(CookieModule, ViewModelsModule));

//Модуль для описания собственных элементов редактирования для kendo-grid
var CustomGridCellEditorModule = (function () {
    var setCustomEditorsForProceedsPlanGrid = function () {
        //Устанавливаем собственные эдиторы для ячеек таблицы планов выручки,
        //используя kendo numeric textbox. Делается это для обеспечения ввода 
        //только целых чисел
        var columns = $("#proceeds-plan-grid").data("kendoGrid").columns;
        $.each(columns, function () {
            if (this.field === "CommonPlan" ||
                this.field === "PaidPlan") {
                this.editor = function (container, options) {
                    $('<input type="text" required="required"' +
                        'data-required-msg="Значение не может быть неопределённым"' +
                        'data-type="number"' +
                        ' name="' + options.field + '"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            min: 0,
                            format: "#",
                            decimals: 0,
                            step: 1
                        });
                };
            }
        });
    };

    var setValidatorsForCustomEditors = function () {
        //Обработчик на событие потери фокуса в ячейки таблицы планов выручки
        $("#proceeds-plan-grid").on("blur", "input[required=required]", function (e) {
            var input = $(this).siblings("input");
            var message = $(this).attr("data-required-msg");

            if (this.value && input && message) {
                input.removeClass("grok-validation-error");
                if (input.data("kendoTooltip") !== undefined) {
                    input.data("kendoTooltip").destroy();
                }
            } else {
                input.addClass("grok-validation-error");
                input.kendoTooltip({
                    content: message,
                    width: 150,
                    position: "top"
                });
            }
        });
    };

    var init = function () {
        if ($("#proceeds-plan-grid").length > 0) {
            setCustomEditorsForProceedsPlanGrid();
            setValidatorsForCustomEditors();
        }
    };

    return {
        init: init
    };
});

//Модуль для инициализации валидации
var ValidationModule = (function () {
    var init = function () {
        //Подключаем валидацию для datepicker-ов на вкладке кассовых окон
        ValidationHelper.init({
            formSelector: "#terminal-search-params"
        });

        //Подключаем валидацию для datepicker-ов на вкладке терминалов
        ValidationHelper.init({
            formSelector: "#cash-boxes-search-params"
        });
    };

    return {
        init: init
    };
});

//Модуль для обработки приходящих в гриды данных
var DataBindingModule = (function (cookieModule) {
    //Признаки активности кнопок согласования графиков кассиров
    var areAddScheduleButtonsEnabled = $("#are-add-schedule-buttons-enabled").attr("data-are-enabled").toLowerCase() === "true";
    var areSendReconcileScheduleButtonsEnabled = $("#are-send-reconcile-schedule-buttons-enabled").attr("data-are-enabled").toLowerCase() === "true";
    var areCancelReconcileScheduleButtonsEnabled = $("#are-cancel-reconcile-schedule-buttons-enabled").attr("data-are-enabled").toLowerCase() === "true";
    var isUserSectorChief = $("#is-user-sector-chief").attr("data-is-in-role").toLowerCase() === "true";
    var isUserProfGroupOrganizator = $("#is-user-prof-group-organizator").attr("data-is-in-role").toLowerCase() === "true";

    var scheduleBound = function (e) {
        //Сохраняем текущий месяц в куках
        cookieModule.saveScheduleDate();
        //Сохраняем в сессию текущее состояние грида
        cookieModule.saveGridState("#schedules-grid");

        var grid = $("#schedules-grid");
        var gridDataSource = grid.data("kendoGrid");
        var dataItem = gridDataSource.dataItem(grid.find("tr:first"));

        var exportButton = $("#schedileExportLink");

        var gridBody = $("#schedules-grid tbody tr");
        var addButton = $("#scheduleAddBtn");
        var deleteButtons = $(".scheduleDeleteButton");
        var reconcileButton = $("#scheduleReconcileButton");
        var cancelReconcileButton = $("#scheduleCancelReconcileButton");
        var confirmReconcileButton = $("#scheduleConfirmReconcileButton");

        if (dataItem === undefined ||
            dataItem === null) {
            //Для тех, кто может добавлять и удалять графики
            if (areAddScheduleButtonsEnabled) {
                //Кнопка Добавить активна
                addButton.removeAttr("disabled");
                addButton.removeClass("k-state-disabled");
            }

            exportButton.attr("scheduleId", "-1");
            exportButton.attr("disabled", "disabled");
            exportButton.addClass("k-state-disabled");

            //Кнопка На согласование неактивна
            reconcileButton.attr("disabled", "disabled");
            reconcileButton.addClass("k-state-disabled");

            //Кнопка Отменить согласование неактивна
            cancelReconcileButton.attr("disabled", "disabled");
            cancelReconcileButton.addClass("k-state-disabled");

            //Кнопка Подтвердить согласование неактивна
            confirmReconcileButton.attr("disabled", "disabled");
            confirmReconcileButton.addClass("k-state-disabled");

            //Добавляем пустую строчку
            var colspan = gridDataSource.thead.find("th").length;
            var emptyRow = "<tr><td colspan='" + colspan + "'></td></tr>";
            gridDataSource.tbody.html(emptyRow);

            //Включение горизонтального скролла для ie9                                  
            var totalWidth = 0;
            for (var i = 0; i < gridDataSource.columns.length; i++) {
                var width = parseInt(gridDataSource.columns[i].width);
                totalWidth = totalWidth + width;
            }
            //Если колонка с кнопками 'Удалить' скрыта
            var isHiddenFirstColumn = $("a.scheduleDeleteButton").length === 0;
            if (isHiddenFirstColumn) {
                totalWidth = totalWidth - 50;
            }
            gridDataSource.table.width(totalWidth);

            return;
        } else {
            exportButton.attr("scheduleId", dataItem.ScheduleId);
            exportButton.removeAttr("disabled");
            exportButton.removeClass("k-state-disabled");
        }

        //Если график не согласован
        if (dataItem.ApprovalStatusId === ScheduleStatus.notReconcile) {
            gridBody.attr('title', 'График не согласован');
            //Кнопки Удалить активны
            deleteButtons.html('<img src="/Content/img/delete.png">');

            //Для тех, кто может добавлять и удалять графики
            if (areAddScheduleButtonsEnabled) {
                //Кнопка Добавить активна
                addButton.removeAttr("disabled");
                addButton.removeClass("k-state-disabled");

            }

            //Для тех, кто может отправлять на согласование и отменять согласование
            if (areSendReconcileScheduleButtonsEnabled) {
                //Кнопка На согласование активна
                reconcileButton.removeAttr("disabled");
                reconcileButton.removeClass("k-state-disabled");
            }
            if (areCancelReconcileScheduleButtonsEnabled) {
                //Кнопка Отменить согласование неактивна
                cancelReconcileButton.attr("disabled", "disabled");
                cancelReconcileButton.addClass("k-state-disabled");
            }
        }

        //Если график ожидает согласования начальника участка
        if (dataItem.ApprovalStatusId === ScheduleStatus.reconciledBySectionChief) {
            gridBody.addClass("scheduleApprovedBySectionChief");
            gridBody.attr('title', 'На согласовании начальником участка');
            //Кнопки Удалить неактивны
            deleteButtons.html('<img src="/Content/img/disabled_delete.png">');

            //Для тех, кто может добавлять и удалять графики
            if (areAddScheduleButtonsEnabled) {
                //Кнопка Добавить неактивна
                addButton.attr("disabled", "disabled");
                addButton.addClass("k-state-disabled");
            }

            //Для тех, кто может отправлять на согласование и отменять согласование
            if (areSendReconcileScheduleButtonsEnabled) {
                //Кнопка На согласование неактивна
                reconcileButton.attr("disabled", "disabled");
                reconcileButton.addClass("k-state-disabled");
            }
            if (areCancelReconcileScheduleButtonsEnabled) {
                //Кнопка Отменить согласование активна
                cancelReconcileButton.removeAttr("disabled");
                cancelReconcileButton.removeClass("k-state-disabled");
            }

            //Если пользователь начальник участка
            if (isUserSectorChief) {
                //Кнопка Подтвердить согласование активна
                confirmReconcileButton.removeAttr("disabled");
                confirmReconcileButton.removeClass("k-state-disabled");
            } else {
                //Кнопка Подтвердить согласование неактивна
                confirmReconcileButton.attr("disabled", "disabled");
                confirmReconcileButton.addClass("k-state-disabled");
            }
        }

        //Если график ожидает согласования профгруппорга
        if (dataItem.ApprovalStatusId === ScheduleStatus.reconciledByProfessionalGroupOrganizer) {
            gridBody.addClass("scheduleApprovedByProfessionalGriupOrganizator");
            gridBody.attr('title', 'На согласовании профгруппоргом');
            //Кнопки Удалить неактивны
            deleteButtons.html('<img src="/Content/img/disabled_delete.png">');

            //Для тех, кто может добавлять и удалять графики
            if (areAddScheduleButtonsEnabled) {
                //Кнопка Добавить неактивна
                addButton.attr("disabled", "disabled");
                addButton.addClass("k-state-disabled");
            }

            //Для тех, кто может отправлять на согласование и отменять согласование
            if (areSendReconcileScheduleButtonsEnabled) {
                //Кнопка На согласование неактивна
                reconcileButton.attr("disabled", "disabled");
                reconcileButton.addClass("k-state-disabled");
            }
            if (areCancelReconcileScheduleButtonsEnabled) {
                //Кнопка Отменить согласование активна
                cancelReconcileButton.attr("disabled", "disabled");
                cancelReconcileButton.addClass("k-state-disabled");
            }

            //Если пользователь профгруппорг
            if (isUserProfGroupOrganizator) {
                //Кнопка Подтвердить согласование активна
                confirmReconcileButton.removeAttr("disabled");
                confirmReconcileButton.removeClass("k-state-disabled");
            } else {
                //Кнопка Подтвердить согласование неактивна
                confirmReconcileButton.attr("disabled", "disabled");
                confirmReconcileButton.addClass("k-state-disabled");
            }
        }

        //Если график согласован
        if (dataItem.ApprovalStatusId === ScheduleStatus.reconciled) {
            gridBody.addClass("scheduleApprovedByAll");
            gridBody.attr('title', 'Согласован');
            //Кнопки Удалить неактивны
            deleteButtons.html('<img src="/Content/img/disabled_delete.png">');

            //Для тех, кто может добавлять и удалять графики
            if (areAddScheduleButtonsEnabled) {
                //Кнопка Добавить неактивна
                addButton.attr("disabled", "disabled");
                addButton.addClass("k-state-disabled");
            }

            //Для тех, кто может отправлять на согласование и отменять согласование
            if (areSendReconcileScheduleButtonsEnabled) {
                //Кнопка На согласование неактивна
                reconcileButton.attr("disabled", "disabled");
                reconcileButton.addClass("k-state-disabled");
            }
            if (areCancelReconcileScheduleButtonsEnabled) {
                //Кнопка Отменить согласование активна
                cancelReconcileButton.attr("disabled", "disabled");
                cancelReconcileButton.addClass("k-state-disabled");
            }

            //Кнопка Подтвердить согласование неактивна
            confirmReconcileButton.attr("disabled", "disabled");
            confirmReconcileButton.addClass("k-state-disabled");
        }
    };

    var tellerWindowsBound = function (e) {
        cookieModule.saveGridState("#teller-windows-grid");
    };

    var terminalsBound = function (e) {
        cookieModule.saveGridState("#terminals-grid");                
    };

    var proceesdsPlanBound = function (e) {
        //Сохраняем состояние контролов формы фильтрации в куках
        cookieModule.saveProceedsPlansDates();

        //Сохраняем в сессию текущее состояние грида
        cookieModule.saveGridState("#proceeds-plan-grid");
    };

    var terminalGridDataBinding = function (e) {
        $('#current-sector-station-terminal-id').attr('data-current-sector-station-terminal-id', '-1');
        $("#teller-window-teminal-grid").data("kendoGrid").dataSource.read();
        $('#add-terminal-to-teller-window').addClass("k-state-disabled");
    };

    return {
        tellerWindowsBound: tellerWindowsBound,
        terminalsBound: terminalsBound,
        scheduleBound: scheduleBound,
        proceesdsPlanBound: proceesdsPlanBound,
        terminalGridDataBinding: terminalGridDataBinding
    };
});

//Модуль источников данных
var HubModule = (function (cookieModule, viewModelsModule) {
    var tellerWindowGridSource = new kendo.data.DataSource({
        type: "json",
        serverPaging: true,
        serverSorting: true,
        sort: { field: "Number", dir: "asc" },
        pageSize: 10,
        transport: {
            read: {
                url: $("#cash-boxes-url").attr("data-url"),
                dataType: "json",
                type: "POST"
            },
            parameterMap: function (options) {
                var filterParams = cookieModule.getSearchParams("#cash-boxes-search-params");

                return {
                    page: options.page,
                    pageSize: options.pageSize,
                    orderby: (options.sort.length > 0) ? options.sort[0].field + ' ' + options.sort[0].dir : "",
                    filter: filterParams
                };
            }
        },
        schema: {
            model: {
                id: "Id",
                fields: {
                    Number: {},
                    Placement: {},
                    Profile: {},
                    TerminalType: {},
                    TerminalNumber: {},
                    Shiftness: {},
                    WorkDays: {},
                    WorkTime: {},
                    Breaks: {},
                    BreakTime: {},
                    Specialization: {},
                    Status: {},
                    Payment: {},
                    BeginDate: {},
                    EndDate: {},
                    Description: {}
                }
            },
            data: "Items",
            total: "Count"
        },
        error: function (e) {
            if (!window.unloading) {
                alert('При получении данных возникла ошибка');
            }
        }
    });

    var terminalGridSource = new kendo.data.DataSource({
        type: "json",
        serverPaging: true,
        serverSorting: true,
        sort: { field: "Number", dir: "asc" },
        pageSize: 10,
        transport: {
            read: {
                url: $("#terminals-url").attr("data-url"),
                dataType: "json",
                type: "POST"
            },
            parameterMap: function (options) {
                var filterParams = cookieModule.getSearchParams("#terminal-search-params");

                return {
                    page: options.page,
                    pageSize: options.pageSize,
                    orderby: (options.sort.length > 0) ? options.sort[0].field + ' ' + options.sort[0].dir : "",
                    filter: filterParams
                };
            }
        },
        schema: {
            model: {
                id: "Id",
                fields: {
                    Number: {},
                    TypeName: {},
                    ModelName: {},
                    AgentName: {},
                    Placement: {},
                    ProfileName: {},
                    PurposeName: {},
                    BeginDate: {},
                    EndDate: {},
                    Comment: {},
                    StatusName: {}
                }
            },
            data: "Items",
            total: "Count"
        },
        error: function (e) {
            if (!window.unloading) {
                alert('При получении данных возникла ошибка');
            }
        }
    });

    var tellerWindowTerminalGridSource = new kendo.data.DataSource({
        type: "json",
        serverPaging: true,
        serverSorting: true,
        sort: { field: "BeginDate", dir: "asc" },
        pageSize: 10,
        transport: {
            read: {
                url: $("#terminal-in-teller-window-url").attr("data-url"),
                dataType: "json",
                type: "POST"
            },
            parameterMap: function (options) {
                var sectorStationTerminalId = $('#current-sector-station-terminal-id').attr('data-current-sector-station-terminal-id');
                if (sectorStationTerminalId === "") {
                    sectorStationTerminalId = -1;
                }

                return {
                    page: options.page,
                    pageSize: options.pageSize,
                    orderby: (options.sort.length > 0) ? options.sort[0].field + ' ' + options.sort[0].dir : "",
                    sectorStationTerminalId: sectorStationTerminalId
                };
            }
        },
        schema: {
            model: {
                id: "Id",
                fields: {
                    TellerWindowNumber: {},
                    BeginDate: {},
                    EndDate: {}
                }
            },
            data: "Items",
            total: "Count"
        },
        error: function (e) {
            if (!window.unloading) {
                alert('При получении данных возникла ошибка');
            }
        }
    });

    var schedulesSource = new kendo.data.DataSource({
        type: "json",
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        sort: { field: "EmployeeName", dir: "asc" },
        pageSize: 5,
        transport: {
            read: {
                url: $("#schedules-url").attr("data-url"),
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8'
            },
            parameterMap: function (options) {
                var date = $("#scheduleDateInput").data("kendoDatePicker").value();
                if (date === null || date === undefined) {
                    date = viewModelsModule.scheduleViewModel.scheduleDate();
                }

                var sectorId = $("#sector-id").attr("data-value");
                var stationId = $("#station-id").attr("data-value");
                
                options.month = date.getMonth();
                options.year = date.getFullYear();
                options.sectorId = sectorId;
                options.stationId = stationId;

                return JSON.stringify(options);
            }
        },
        schema: {
            model: {
                id: "ScheduleDetailId",
                fields: {
                    ScheduleId: {},
                    ScheduleDetailId: {},
                    EmployeeName: {},
                    EmployeePosition: {},
                    EmployeeNumber: {},
                    TellerWindowNumber: {},
                    TotalHours: {},
                    TotalNightHours: {},
                    TotalHolidayHours: {},
                    ApprovalDate: {},
                    ApprovalStatus: {},
                    Day1: {},
                    Day2: {},
                    Day3: {},
                    Day4: {},
                    Day5: {},
                    Day6: {},
                    Day7: {},
                    Day8: {},
                    Day9: {},
                    Day10: {},
                    Day11: {},
                    Day12: {},
                    Day13: {},
                    Day14: {},
                    Day15: {},
                    Day16: {},
                    Day17: {},
                    Day18: {},
                    Day19: {},
                    Day20: {},
                    Day21: {},
                    Day22: {},
                    Day23: {},
                    Day24: {},
                    Day25: {},
                    Day26: {},
                    Day27: {},
                    Day28: {},
                    Day29: {},
                    Day30: {},
                    Day31: {}
                }
            },
            data: "Items",
            total: "Count"
        },
        error: function (e) {
            if (!window.unloading) {
                alert('При получении данных возникла ошибка');
            }
        },
        requestStart: function () {
            var addButton = $("#scheduleAddBtn");
            var exportButton = $("#schedileExportLink");
            var reconcileButton = $("#scheduleReconcileButton");
            var cancelReconcileButton = $("#scheduleCancelReconcileButton");
            var confirmReconcileButton = $("#scheduleConfirmReconcileButton");

            addButton.attr("disabled", "disabled");
            addButton.addClass("k-state-disabled");

            exportButton.attr("disabled", "disabled");
            exportButton.addClass("k-state-disabled");

            reconcileButton.attr("disabled", "disabled");
            reconcileButton.addClass("k-state-disabled");

            cancelReconcileButton.attr("disabled", "disabled");
            cancelReconcileButton.addClass("k-state-disabled");

            confirmReconcileButton.attr("disabled", "disabled");
            confirmReconcileButton.addClass("k-state-disabled");
        }
    });

    var tellerWindowSchedulesSource = new kendo.data.DataSource({
        type: "json",
        serverPaging: false,
        serverSorting: false,
        transport: {
            read: {
                url: $("#tellerWindowSchedules-url").attr("data-url"),
                dataType: "json",
                type: "POST"
            },
            parameterMap: function (options) {                
                var date = $("#scheduleDateInput").data("kendoDatePicker").value();
                if (date === null || date === undefined) {
                    date = viewModelsModule.scheduleViewModel.scheduleDate();
                }

                var sectorId = $("#sector-id").attr("data-value");
                var stationId = $("#station-id").attr("data-value");

                return {
                    month: date.getMonth(),
                    year: date.getFullYear(),
                    sectorId: sectorId,
                    stationId: stationId
                };
            }
        },
        schema: {
            model: {
                fields: {
                    Numbers: {},
                    Monday: {},
                    Tuesday: {},
                    Wednesday: {},
                    Thursday: {},
                    Friday: {},
                    Saturday: {},
                    Sunday: {},
                }
            },
            data: "Schedules",
        },
        error: function (e) {
            if (!window.unloading) {
                alert('При получении данных возникла ошибка');
            }
        },
    });

    var proceedsPlanSource = new kendo.data.DataSource({
        pageSize: 13,
        type: "json",
        serverPaging: true,
        serverSorting: true,
        sort: { field: "Date", dir: "asc" },
        autoSync: true,
        transport: {
            read: {
                url: $("#read-proceeds-plan-url").attr("data-url"),
                dataType: "json",
                type: "POST",
                data: function (options) {
                    //Определяем начальные и конечные месяцы интервала по кукам 
                    var beginDate = $("#proceeds-plan-begin-date").data("kendoDatePicker").value();
                    var endDate = $("#proceeds-plan-end-date").data("kendoDatePicker").value();

                    //Если куки пусты
                    if (beginDate === null || beginDate === undefined) {
                        beginDate = viewModelsModule.proceedsPlanViewModel.proceedsPlanBeginDate();
                    }
                    if (endDate === null || endDate === undefined) {
                        endDate = viewModelsModule.proceedsPlanViewModel.proceedsPlanEndDate();
                    }

                    //Определяем идентификаторы станции и участка
                    var sectorId = $("#sector-id").attr("data-value");
                    var stationId = $("#station-id").attr("data-value");

                    var beginDateResult = "";
                    if ((beginDate != null) && (!isNaN(beginDate))) {
                        beginDateResult = $.padStr(beginDate.getFullYear()) + "." + $.padStr(1 + beginDate.getMonth()) + "." + $.padStr(1);
                    }

                    var endDateResult = "";
                    if ((endDate != null) && (!isNaN(endDate))) {
                        endDateResult = $.padStr(endDate.getFullYear()) + "." + $.padStr(1 + endDate.getMonth()) + "." + $.padStr(1);
                    }

                    return {
                        page: options.page,
                        pageSize: options.pageSize,
                        orderby: (options.sort.length > 0) ? options.sort[0].field + ' ' + options.sort[0].dir : "",
                        beginDate: beginDateResult,
                        endDate: endDateResult,
                        sectorId: sectorId,
                        stationId: stationId
                    };
                }
            },
            update: {
                url: $("#update-proceeds-plan-url").attr("data-url"),
                type: "POST",
                data: function (options) {                    
                    return options;
                },
                complete: function (e) {
                    //Узнаём идентификатор созданного/изменённого плана выручки
                    var id = parseInt(e.statusText);
                    //Получаем параметры отредактирвоанного плана выручки
                    var obj = $.parseParams(this.data);

                    if (id !== NaN) {
                        //Получаем все планы выручки
                        var allData = $("#proceeds-plan-grid").data("kendoGrid").dataSource.data();
                        //Находим среди них отредактированный
                        var dataItems = $.grep(allData, function (element) {
                            return element.SectorStationId === parseInt(obj.SectorStationId) &&
                                element.Year === parseInt(obj.Year) &&
                                element.Month === parseInt(obj.Month);
                        });

                        //Если план выручки был создан
                        if (dataItems.length === 1 && dataItems[0].StationMonthProceedsId === -1) {
                            //Присваиваем ему идентификатор
                            dataItems[0].StationMonthProceedsId = id;                           
                        }
                    }
                }
            }
        },
        schema: {
            model: {
                id: "StationMonthProceedsId",
                fields: {
                    StationMonthProceedsId: {},
                    SectorStationId: {},
                    Year: {},
                    Month: {},
                    Date: { editable: false },
                    CommonPlan: {
                        type: "number",
                        validation: {
                            required: { message: "Значение не может быть неопределённым" },
                            min: 0,
                            max: 100000000
                        },
                        defaultValue: 0
                    },
                    PaidPlan: {
                        type: "number",
                        validation: {
                            required: { message: "Значение не может быть неопределённым" },
                            min: 0,                                                    
                            compareWithCommonPlan: function (input) {
                                input.attr("data-compareWithCommonPlan-msg", "Платный план является частью общего плана и не может его превышать!");

                                var newPaidPlan = parseInt($(input).val());
                                var tr = $(input[0]).closest("tr");
                                var grid = $("#proceeds-plan-grid").data("kendoGrid");
                                var dataItem = grid.dataItem(tr);

                                return dataItem.CommonPlan >= newPaidPlan;
                            }                            
                        },                        
                        defaultValue: 0
                    }
                }
            },
            data: "Items",
            total: "Count"
        },
        requestEnd: function (e) {
            if (e.response !== undefined &&
                e.response.status !== undefined &&
                e.response.status === JsonStatus.error) {
                alert(e.response.message);
            }
        },
        error: function (e) {
            if (!window.unloading) {
                alert(e.errorThrown);
            }
        }
    });       

    return {
        tellerWindowGridSource: tellerWindowGridSource,
        terminalGridSource: terminalGridSource,
        tellerWindowTerminalGridSource: tellerWindowTerminalGridSource,
        schedulesSource: schedulesSource,
        tellerWindowSchedulesSource: tellerWindowSchedulesSource,
        proceedsPlanSource: proceedsPlanSource        
    };
});