﻿$(function () {
    //Таки определяем идентификатор направления/участка 
    CookieModule.directionSectorId = $.urlParam("id");
    var hubModule = HubModule(CookieModule, DatePickersModule);

    var viewModel = kendo.observable({
        //Функция, вычисляющая начальную дату месяца по умолчанию
        proceedsPlanBeginDate: DatePickersModule.proceedsPlanBeginDate,
        //Функция, вычисляющая конечную дату месяца по умолчанию
        proceedsPlanEndDate: DatePickersModule.proceedsPlanEndDate,
        //Функция, инициирубщая фильтрацию в таблице планов по выручке
        //при изменении начального месяца периода
        changeBeginMonth: DatePickersModule.changeBeginMonth,
        //Функция, инициирубщая фильтрацию в таблице планов по выручке
        //при изменени конечного месяца периода
        changeEndMonth: DatePickersModule.changeEndMonth,
        //Источник данных для таблицы планов по выручке
        proceedsPlanSource: hubModule.proceedsPlanSource,
        //Обработчик прихода данных в таблицу планов графиков
        proceesdsPlanBound: function (e) {
            //Сохраняем состояние контролов формы фильтрации в куках
            CookieModule.saveProceedsPlansDates();

            //Сохраняем в сессию текущее состояние грида
            CookieModule.saveProceedsPlansGridState();
        }
    });

    kendo.bind($("#proceeds-plan-container"), viewModel);

    //Восстанавливаем из сессии состояние грида с планами выручки только после активации вкладки
    $(document).on("proceedPlansTabActivated", CookieModule.loadProceedsPlansGridState);

    //Если событие активации вкладки уже было, вызываем проверку наличия таймшита вручную        
    if ($("#is-proceed-plans-activated").attr("data-is-activated") === "1") {
        CookieModule.loadProceedsPlansGridState();
    }
});

//Модуль для управления хранением настроек в куки
var CookieModule = (function () {
    var directionSectorId;

    var loadProceedsPlansGridState = function () {
        var proceedsPlanGrid = $("#proceeds-plan-grid").data("kendoGrid");

        var proceedsPlanGridStateFromSession = $.session.get("sectorProceedsPlanGridState" + "-" + this.directionSectorId);
        if (proceedsPlanGridStateFromSession !== undefined) {
            var initialState = JSON.parse(proceedsPlanGridStateFromSession);
            if (initialState) {
                proceedsPlanGrid.dataSource.query(initialState);
            } else {
                proceedsPlanGrid.dataSource.read();
            }
        } else {
            proceedsPlanGrid.dataSource.read();
        }
        
        $(document).off("proceedPlansTabActivated");
    };

    var getProceedsPlanBeginDate = function () {
        return $.session.get("sectorProceedsPlanBeginDate" + "-" + this.directionSectorId);
    };

    var getProceedsPlanEndDate = function () {
        return $.session.get("sectorProceedsPlanEndDate" + "-" + this.directionSectorId);
    };

    var saveProceedsPlansDates = function () {
        var currentProceedsPlanBeginDate = $("#proceeds-plan-begin-date").data("kendoDatePicker").value();
        var currentProceedsPlanEndDate = $("#proceeds-plan-end-date").data("kendoDatePicker").value();
        $.session.set("sectorProceedsPlanBeginDate" + "-" + this.directionSectorId, currentProceedsPlanBeginDate);
        $.session.set("sectorProceedsPlanEndDate" + "-" + this.directionSectorId, currentProceedsPlanEndDate);
    };

    var saveProceedsPlansGridState = function () {
        var dataSource = $("#proceeds-plan-grid").data("kendoGrid").dataSource;

        var currentGridState = kendo.stringify({
            page: dataSource.page(),
            pageSize: dataSource.pageSize(),
            sort: dataSource.sort(),
            group: dataSource.group()
        });

        $.session.set("sectorProceedsPlanGridState" + "-" + this.directionSectorId, currentGridState);
    };

    return {
        directionSectorId: directionSectorId,
        loadProceedsPlansGridState: loadProceedsPlansGridState,
        getProceedsPlanBeginDate: getProceedsPlanBeginDate,
        getProceedsPlanEndDate: getProceedsPlanEndDate,
        saveProceedsPlansDates: saveProceedsPlansDates,
        saveProceedsPlansGridState: saveProceedsPlansGridState
    };
}());

//Модуль операций с datePicker-ами
var DatePickersModule = (function (cookieModule) {
    var proceedsPlanGridReadData = function () {
        var invalidDatePickersCount = $("#proceeds-plan-search-params .grok-validation-error");

        if (invalidDatePickersCount && invalidDatePickersCount.length === 0) {
            var proceedsPlanGridDataSource = $("#proceeds-plan-grid").data("kendoGrid").dataSource;
            proceedsPlanGridDataSource.page(1);
        }
    };

    var proceedsPlanBeginDate = function () {
        var savedProceedsPlanBeginDate = CookieModule.getProceedsPlanBeginDate();
        if (savedProceedsPlanBeginDate) {
            return new Date(savedProceedsPlanBeginDate);
        }

        var date = new Date();
        date.setMonth(date.getMonth() - 6);

        return date;
    };

    var proceedsPlanEndDate = function () {
        var savedProceedsPlanEndDate = CookieModule.getProceedsPlanEndDate();
        if (savedProceedsPlanEndDate) {
            return new Date(savedProceedsPlanEndDate);
        }

        var date = new Date();
        date.setMonth(date.getMonth() + 6);

        return date;
    };

    var changeBeginMonth = function () {
        var proceedsPlanBeginDateInput = $("#proceeds-plan-begin-date");

        Utils.Validation.validateMonthDatePicker(proceedsPlanBeginDateInput, proceedsPlanGridReadData);
    };

    var changeEndMonth = function () {
        var proceedsPlanEndDateInput = $("#proceeds-plan-end-date");

        Utils.Validation.validateMonthDatePicker(proceedsPlanEndDateInput, proceedsPlanGridReadData);
    };

    return {
        proceedsPlanBeginDate: proceedsPlanBeginDate,
        proceedsPlanEndDate: proceedsPlanEndDate,
        changeBeginMonth: changeBeginMonth,
        changeEndMonth: changeEndMonth
    };
}(CookieModule));

//Модуль источников данных
var HubModule = (function (cookieModule, datePickersModule) {
    var proceedsPlanSource = new kendo.data.DataSource({
        pageSize: 13,
        type: "json",
        serverPaging: true,
        serverSorting: true,
        sort: { field: "Date", dir: "asc" },
        autoSync: true,
        transport: {
            read: {
                url: $("#read-proceeds-plan-url").attr("data-url"),
                dataType: "json",
                type: "POST",
                data: function (options) {
                    //Определяем начальные и конечные месяцы интервала
                    var beginDate = $("#proceeds-plan-begin-date").data("kendoDatePicker").value();
                    var endDate = $("#proceeds-plan-end-date").data("kendoDatePicker").value();

                    if (beginDate === null) {
                        beginDate = datePickersModule.proceedsPlanBeginDate();
                    }
                    if (endDate === null) {
                        endDate = datePickersModule.proceedsPlanEndDate();
                    }
                    //Определяем идентификаторы станции и участка
                    var sectorId = $("#sector-id").attr("data-value");
                    var stationId = $("#station-id").attr("data-value");

                    return {
                        page: options.page,
                        pageSize: options.pageSize,
                        orderby: (options.sort.length > 0) ? options.sort[0].field + ' ' + options.sort[0].dir : "",
                        beginDate: $.padStr(beginDate.getFullYear()) + "." +
                            $.padStr(1 + beginDate.getMonth()) + "." +
                            $.padStr(1),
                        endDate: $.padStr(endDate.getFullYear()) + "." +
                            $.padStr(1 + endDate.getMonth()) + "." +
                            $.padStr(1),
                        sectorId: sectorId,
                        stationId: stationId
                    };
                }
            }
        },
        schema: {
            model: {
                id: "StationMonthProceedsId",
                fields: {
                    StationMonthProceedsId: {},
                    SectorStationId: {},
                    Year: {},
                    Month: {},
                    Date: {},
                    CommonPlan: {},
                    PaidPlan: {}
                }
            },
            data: "Items",
            total: "Count"
        },
        requestEnd: function (e) {
            if (e.response !== undefined &&
                e.response.status !== undefined &&
                e.response.status === JsonStatus.error) {
                alert(e.response.message);
            }
        },
        error: function (e) {
            if (!window.unloading) {
                alert(e.errorThrown);
            }
        }
    });

    return {
        proceedsPlanSource: proceedsPlanSource
    };
});