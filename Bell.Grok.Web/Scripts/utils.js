﻿var Utils;
(function (utils) {
    return utils;
})(Utils || (Utils = {}));

//Модуль для работы с плагином jquery-msg
Utils.Notification = function ($) {
    var show;

    //Функция. инициализирующая параметры для плагина
    show = function (message) {
        $.msg({
            content: message,
            bgPath: '/Plugins/jquery-msg/',
            fadeIn: 500,
            fadeOut: 200,
            timeOut: 2000
        });
    };   

    return {
        show: show
    };
}($);

Utils.Serializer = function ($) {
    //Функция сериализации данных формы в объект
    var serializeFormToObject = function(formSelector) {
        var paramObj = {};
        $.each($(formSelector).serializeArray(), function(_, kv) {
            if (paramObj.hasOwnProperty(kv.name)) {
                paramObj[kv.name] = $.makeArray(paramObj[kv.name]);
                paramObj[kv.name].push(kv.value);
            }
            else {
                paramObj[kv.name] = kv.value;
            }
        });

        return paramObj;
    };

    return {
        serializeFormToObject: serializeFormToObject
    };
    
}($);

Utils.Validation = function($) {
    var validateMonthDatePicker = function(datePicker, readCallback) {
        var value = datePicker.val();

        //Строим регулярное выражение
        var monthsWords = "(январь?|февраль?|март?|апрель?|май|июнь?|июль?|август?|сентябрь?|октябрь?|ноябрь?|декабрь?)";
        var months = "((" + monthsWords + "))";
        var years = "\\d{4}";
        var separator = "\\s";
        var pattern = months + separator + years;
        var regexp = new RegExp(pattern, "i");

        //Проверяем с помощью регулярного выражения введённую дату
        if (!value.match(regexp)) {
            datePicker.addClass("grok-validation-error");
            datePicker.kendoTooltip({
                content: "Недопустимая дата. Необходимый формат: " + "[Название месяца полностью][Пробел][Год(yyyy)]",
                width: 150,
                position: "top"
            });
        } else {
            datePicker.removeClass("grok-validation-error");
            if (datePicker.data("kendoTooltip") !== undefined) {
                datePicker.data("kendoTooltip").destroy();
            }

            readCallback();            
        }
    };

    var validateTimesheetDetailCell = function(value, cell) {
        if (value > 0) {            
            cell.removeClass("grok-validation-td");
            if (cell.data("kendoTooltip") !== undefined) {
                cell.data("kendoTooltip").destroy();
            }
        } else {
            cell.addClass("grok-validation-td");
            cell.kendoTooltip({
                content: "Введите ненулевое количество часов",
                width: 150,
                position: "top"
            });           
        }
    };

    var integerOnlyValidate = function (input) {
        input.attr("data-integerOnly-msg", "Число должно быть целым!!!");

        if (input.length > 0) {
            var value = input[0].value;
            value = value.replace(/,/g, '.')
            var tt = Math.round(value);
            var ll = Math.round(value) == value;
            var oo = value != "";
            var rr = !isNaN(value);
            return value != "" &&
                !isNaN(value) &&
                Math.round(value) == value;
        } else {
            return true;
        }        
    };

    return {
        validateMonthDatePicker: validateMonthDatePicker,
        integerOnlyValidate: integerOnlyValidate,
        validateTimesheetDetailCell: validateTimesheetDetailCell
    };
}($);

jQuery.extend({
    padStr: function(i) {
         return (i < 10) ? "0" + i : "" + i;
    },
    areArraysEqual: function(arr1, arr2) {
        return $(arr1).not(arr2).length === 0 &&
               $(arr2).not(arr1).length === 0;
    },
    urlParam: function(name){
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (!results)
        { 
            return 0; 
        }
        return results[1] || 0;
    },
    isEven: function(n) {
        return n == parseFloat(n) && (n % 2 == 0);
    },
    arrayToString: function(array) {       
        var result = "";
        $.each(array, function (index, value) {
            result += value + ";";
        });

        return result;
    },
    //Функция, определяющая новую текущую страницу после удаления записи из таблицы
    calculateCurrentPage: function(dataSource) {
        var currentPage = dataSource.page();
        var totalCount = dataSource.total();
        var pageSize = dataSource.pageSize();
        var reminder = totalCount % pageSize;

        //Обновление таблицы кассовых окон и красивый показ сообщения об успехе операции
        if (reminder === 1 &&
            currentPage > 1) {
            return currentPage - 1;
        } else {
            return currentPage;
        }
    },
    //Функция, определяющая, идёт ли работа в ie9 или ниже
    isBrowserInternetExplorer9orUnder: function() {
        var ver = -1; 
        if (navigator.appName == 'Microsoft Internet Explorer') {
            var ua = navigator.userAgent;
            var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null) {
                ver = parseFloat(RegExp.$1);
            }
            
            if (ver > -1) {
                return (ver >= 10.0)
                    ? false
                    : true;                
            }
        }

        return false;
    }
});