﻿$(function ($) {
    var isAuthenticated = $("#is-user-authenticated").attr("data-is-authenticated").toLowerCase() === "true";
    if (!isAuthenticated) {
        $("#menu-container").hide();
        return;
    }

    var mod = Controller.instance;

    var AdminMenu = mod.create(AdminMenuPage);
    var KendoMenuInstance = mod.create(KendoMenuModule);

    var adminMenu = new AdminMenu({ el: $("#menu-container") });
    var kendoMenuInstance = new KendoMenuInstance({});

    $.subscribe("initAdminMenuEvent", kendoMenuInstance.makeKendoMenu, adminMenu);

    adminMenu.initAdminMenu();

});

//Модуль страницы
var AdminMenuPage = {
    elements: {
        "#admin-menu": "adminMenuPanel",
        "#menu-container": "adminMenuContainer"
    },

    events: {
        "init adminMenu": "initAdminMenu"
    },

    //Функция, испускающая пользовательское событие для модуль kendo-menu
    initAdminMenu: function () {
        $.publish("initAdminMenuEvent", {
            panel: this.adminMenuPanel.selector,
            container: this.el.selector
        });
    }
};

//Модуль kendo-menu
var KendoMenuModule = {
    elements: {

    },

    events: {

    },

    //Функция, формирующая kendo-menu на основе данных, получаемых от сервера
    makeKendoMenu: function (topic, data) {
        var link = $(data.panel).data("url");

        $.post(link, null, function (response) {
            if (response.status == JsonStatus.ok) {
                $(data.container).css("display", "block");
                $(data.panel).kendoMenu({ dataSource: response });
            } else {
                $(data.container).css("display", "none");
            }
        })
            .error(function () {
                alert("get actions error");
            });
    },
};