﻿$(function () {
    IndividualTellerWindowScheduleModule.init();

    var popupMenuClickHandler = IndividualTellerWindowScheduleModule.popupMenuClickHandler;

    $("#popup-menu").kendoMenuEx({
        dataSource: [{
            text: 'Технологически перерыв',
            data: { id: TellerWindowScheduleHourTypes.TechnologicalBreak },
            click: popupMenuClickHandler
        }, {
            text: 'Обеденный перерыв',
            data: { id: TellerWindowScheduleHourTypes.DinnerBreak },
            click: popupMenuClickHandler
        }, {
            text: 'Убрать перерыв',
            data: { id: TellerWindowScheduleHourTypes.Work },
            click: popupMenuClickHandler
        }],
        anchor: ".teller-windows-schedule-grid",
        selector: "td[role=gridcell]",
        filter: "span[can-break-have=true]"
    });

    $("#return-to-passport-station-link").on("click", IndividualTellerWindowScheduleModule.returnLinkClick);
});

//Агрегационная функция по рассчёту рекомендуемого количества окон для каждого часа в срезе
function RecommendedTellerWindowsLoad(gridIndex, columnIndex) {
    var grid = $("#weekdays-grid-" + gridIndex.toString()).data("kendoGrid");
    var data = grid.dataSource.data();
    var result = 0;    

    for (var i = 0; i < data.length; i++) {              
        if (data[i].DoesWorkByDefault[columnIndex] === true) {
            result++;
        }             
    }
    
    return result;
}

//Агрегационная функция по рассчёту фактического количества окон для каждого часа в срезе
function FactTellerWindowsLoad(gridIndex, columnIndex) {
    var grid = $("#weekdays-grid-" + gridIndex.toString()).data("kendoGrid");
    var data = grid.dataSource.data();
    var result = 0;

    for (var i = 0; i < data.length; i++) {
        if (data[i].HourTypes[columnIndex].HourTypeId !==
            TellerWindowScheduleHourTypes.Rest) {
            result++;
        }
    }

    return result;
}

var IndividualTellerWindowScheduleModule = (function () {
    //Обработчик выбора пункта всплывающего меню со списком перерывов
    var popupMenuClickHandler = function (event) {        
        var changedMenuItem = $(event.currentTarget).data("data");
        var newHourTypeId = changedMenuItem.id;
        var container = $(event.currentTarget).closest("div");

        var td = container[0].purposeSelector;
        var tr = $(td).closest("tr");
        var oldHourTypeId = parseInt($(td).find("span").attr("typecode"));
        var table = $(td).closest(".teller-windows-schedule-grid");
        var grid = $(table).data("kendoGrid");
        var dataItem = grid.dataItem(tr);
        var columnIndex = td[0].cellIndex - 1;
        var hourTypeCode, searchedHourTypeId;       

        //Если выбран другой тип рабочего часа
        if (newHourTypeId !== oldHourTypeId) {
            hourTypeCode = getHourTypeCode(newHourTypeId);
            searchedHourTypeId = newHourTypeId;                       
        } else {
            hourTypeCode = getHourTypeCode(oldHourTypeId);
            searchedHourTypeId = oldHourTypeId;                        
        }
        
        if (hourTypeCode !== undefined &&
            searchedHourTypeId !== undefined) {
            //Изменяем тип часа 
            dataItem.HourTypes[columnIndex] = { HourTypeId: searchedHourTypeId, HourTypeCode: hourTypeCode };

            //Изменяем символ кода в разметке
            $(td).find("span").text(hourTypeCode);
        }

        defineModifications(td, columnIndex, dataItem);
    };

    //Функция, инициализирующие заголовки с названиями сезонов
    var initHeaders = function () {        
        var tellerWindowScheduleDayTypes = $("#teller-window-schedule-day-types").data("values");
               
        var headers = $("div.weekdays-grid-conteiner").find("h4");

        for (var j = 0; j < headers.length; j++) {
            if (tellerWindowScheduleDayTypes[j]) {
                $("#teller-window-schedules-day-type-header-" + j.toString()).text(tellerWindowScheduleDayTypes[j].TellerWindowScheduleDayTypeName);
            }
        }        
    };

    //Функция, определяющая символьный код типа часа по его идентификатору
    var getHourTypeCode = function (id) {
        var hourtypes = $("#hour-types").data("values");

        for (var i = 0; i < hourtypes.length; i++) {
            if (hourtypes[i].HourTypeId == id) {
                return hourtypes[i].HourTypeCode;
            }
        }

        return '';
    };

    //Функция, которая ищет объект в списке кассовых окон среза по их идентификатору
    var findTellerWindow = function(grid, value) {
        var tellerWindows = $.grep(grid.dataSource.allTellerWindows, function (element) {
            return element.Value === value;
        });
        
        if (tellerWindows.length === 1) {
            return tellerWindows[0];
        } else {
            return null;
        }            
    };

    //Функция, возвращающая список незанятых кассовых окон в срезе
    var getFreeTellerWindows = function(grid, value) {
        return $.grep(grid.dataSource.allTellerWindows, function (element) {
            return element.free === true ||
                   element.Value === value;
        });
    };

    //Собственный редактор для ячеек колонки с номерами кассовых окон
    var tellerWindowNumberEditor = function (container, options) {
        var gridDiv = $(container[0]).closest(".teller-windows-schedule-grid");
        var grid = $(gridDiv).data("kendoGrid");

        var currentTellerWindowId = -1;
        var currentTellerWindow = findTellerWindow(grid, options.model.TellerWindowNumber.Value);
        if (currentTellerWindow) {
            //Определяем номер текущего окна в ячейке
            currentTellerWindowId = currentTellerWindow.Value;
        }

        //Получаем список незанятых в других ячейках окон
        var freeTellerWindows = getFreeTellerWindows(grid, currentTellerWindowId);

        $('<input data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                dataValueField: "Value",
                dataTextField: "Text",
                dataSource: freeTellerWindows,
                change: function (e) {
                    var value = parseInt(this.value());

                    if (value !== NaN) {
                        var changedTellerWidnow = findTellerWindow(grid, value);
                        //Если выбранное окно не совпадает с текущим
                        if (changedTellerWidnow &&
                            changedTellerWidnow.Value !== currentTellerWindow.Value) {
                            if (value !== 0) {
                                //Помечаем выбранное окно как занятое
                                changedTellerWidnow.free = false;
                            }
                            //Уже бывшее текущее помечаем как свободное
                            currentTellerWindow.free = true;
                        }
                    }
                }
            });
    };

    //Функция, проверяющая наличие изменение в ячейке - и как следствие во всей строке
    var defineModifications = function(cell, cellIndex, dataItem) {
        //Определяем, имеется ли в ячейке несохранённое значение
        var isModified = dataItem.HourTypes[cellIndex].HourTypeId !==
                         dataItem.OldHourTypes[cellIndex].HourTypeId;

        if (isModified) {
            //Показываем красную рамку, если данные в ячейке модифицированы
            $(cell).addClass("not-saved");
        } else {
            //Если нет несохранённого изменения - снимаем красную рамку
            $(cell).removeClass("not-saved");
        }

        //Ищем ячейки с несохранёнными изменениями в строке
        var modifiedCells = $(cell).closest("tr").find("td.not-saved");
        //Если в строке есть ячейки с несохранёнными изменениями - помечаем строку, 
        //как модифицированную (то есть данные строки войдут в параметры post-запроса)
        if (modifiedCells.length > 0) {
            dataItem.dirty = true;
        } else {
            dataItem.dirty = false;
        }
    };
    
    //Собственный редактор для ячеек таблицы колонок с признаками работы кассового окна в определённый час
    var columnEditor = function (container, options) {
        //Определяем номер колонки
        var hourNumberRegObject = options.field.match(/\d+/);
        var hourNumber = parseInt(hourNumberRegObject[0]);
        if (hourNumber === NaN) {
            return;
        }

        //Определяем текущий тип часа
        var currentHourTypeId = options.model.HourTypes[hourNumber].HourTypeId;

        var hourTypeCode = '';               

        if (currentHourTypeId === TellerWindowScheduleHourTypes.Rest) {
            //Если текущий тип часа - отдых
            hourTypeCode = getHourTypeCode(TellerWindowScheduleHourTypes.Work);
            //Изменяем тип часа на рабочий
            options.model.HourTypes[hourNumber] = { HourTypeId: TellerWindowScheduleHourTypes.Work, HourTypeCode: hourTypeCode };                                   
            
            $(container[0]).attr("class", "recommended-work-hour");           

        } else if (currentHourTypeId !== TellerWindowScheduleHourTypes.Rest) {
            //Если текущий тип часа - не отдых
            hourTypeCode = getHourTypeCode(TellerWindowScheduleHourTypes.Rest);
            //Изменяем тип часа на отдых
            options.model.HourTypes[hourNumber] = { HourTypeId: TellerWindowScheduleHourTypes.Rest, HourTypeCode: hourTypeCode };           

            $(container[0]).attr("class", "recommended-rest-hour");           
        }

        //Перерассчитываем флаги возможности наличия перерыва для каждого часа
        options.model.CanBreakHave[0] = false;
        
        for (var i = 1; i < 23; i++) {
            options.model.CanBreakHave[i] = 
                options.model.HourTypes[i - 1].HourTypeId !== TellerWindowScheduleHourTypes.Rest &&
                options.model.HourTypes[i].HourTypeId !== TellerWindowScheduleHourTypes.Rest &&
                options.model.HourTypes[i + 1].HourTypeId !== TellerWindowScheduleHourTypes.Rest;            
        }
        
        options.model.CanBreakHave[23] = false;        
                        
        //Получаем соседние ячейки
        var prevCell = $(container[0]).prev("td:has(span)");        
        var nextCell = $(container[0]).next("td:has(span)");
        
        if (prevCell.length > 0) {            
            if (options.model.HourTypes[hourNumber - 1].HourTypeId !== TellerWindowScheduleHourTypes.Rest &&
                options.model.HourTypes[hourNumber].HourTypeId === TellerWindowScheduleHourTypes.Rest) {
                //Если значение текущего часа - отдых, то у предыдущего не может быть перерывов
                hourTypeCode = getHourTypeCode(TellerWindowScheduleHourTypes.Work);
                options.model.HourTypes[hourNumber - 1] = { HourTypeId: TellerWindowScheduleHourTypes.Work, HourTypeCode: hourTypeCode };                
            }

            //Изменяем разметку ячейки предыдущего часа - так как kendo автоматически не изменяет разметку не текущей ячейки
            $(prevCell[0]).find("span").attr("can-break-have", options.model.CanBreakHave[hourNumber - 1])
                                       .attr("typecode", options.model.HourTypes[hourNumber - 1].HourTypeId)
                                       .text(options.model.HourTypes[hourNumber - 1].HourTypeCode);
            
            //Проверка на наличие модификаций в предыдущей ячейке
            if (options.model.HourTypes[hourNumber - 1].HourTypeId ===
                options.model.OldHourTypes[hourNumber - 1].HourTypeId) {
                //Если значение типа часа не изменилось - крайно рамки быть не должно
                $(prevCell[0]).removeClass("not-saved");
            } else {
                //Если значение типа часа изменилось - то обводим ячейку красной рамкой
                $(prevCell[0]).addClass("not-saved");
            }
        }

        if (nextCell.length > 0) {
            if (options.model.HourTypes[hourNumber + 1].HourTypeId !== TellerWindowScheduleHourTypes.Rest &&
                options.model.HourTypes[hourNumber].HourTypeId === TellerWindowScheduleHourTypes.Rest) {
                //Если значение текущего часа - отдых, то у следующего не может быть перерывов
                hourTypeCode = getHourTypeCode(TellerWindowScheduleHourTypes.Work);
                options.model.HourTypes[hourNumber + 1] = { HourTypeId: TellerWindowScheduleHourTypes.Work, HourTypeCode: hourTypeCode };
            }

            //Изменяем разметку ячейки следующего часа - так как kendo автоматически не изменяет разметку не текущей ячейки
            $(nextCell[0]).find("span").attr("can-break-have", options.model.CanBreakHave[hourNumber + 1])
                                       .attr("typecode", options.model.HourTypes[hourNumber + 1].HourTypeId)
                                       .text(options.model.HourTypes[hourNumber + 1].HourTypeCode);
            
            //Проверка на наличие модификаций в следующей ячейке
            if (options.model.HourTypes[hourNumber + 1].HourTypeId ===
                options.model.OldHourTypes[hourNumber + 1].HourTypeId) {
                //Если значение типа часа не изменилось - крайно рамки быть не должно
                $(nextCell[0]).removeClass("not-saved");
            } else {
                //Если значение типа часа изменилось - то обводим ячейку красной рамкой
                $(nextCell[0]).addClass("not-saved");
            }
        }
        
        defineModifications(container[0], hourNumber, options.model);

        //kendo определяет конец редактирования ячейки по событию потери фокуса        
        $(container[0]).trigger("blur");
        return;
    };
    
    //Построение набора колонок
    var columns = function (gridIndex) {
        var result = [];

        //Колонка с номером кассового окна
        result.push({
            field: "TellerWindowNumber",
            headerTemplate: "Номер<br>окна",
            width: "55px",
            editor: tellerWindowNumberEditor,
            attributes: {
                "class": "teller-window-number-cell"
            },
            template: "#= TellerWindowNumber.Text #"
        });

        //Колонки с индикаторами работы кассовых окон в определённый час среза
        for (var i = 0; i < 24; i++) {
            result.push({
                field: "HourTypes[" + i + "]",
                title: i.toString(),
                editor: columnEditor,                
                template: "<span " +
                                "typecode=\"#: HourTypes[" + i + "].HourTypeId #\" " +                                
                                "can-break-have=\"#: CanBreakHave[" + i + "] #\">" +
                                "#: HourTypes[" + i + "].HourTypeCode #" +
                          "</span>",
                footerTemplate: "<span class=\"recommended-load\" fact-load=\"#= FactTellerWindowsLoad(" + gridIndex + ", " + i + ") #\">" +
                                "#= RecommendedTellerWindowsLoad(" + gridIndex + ", " + i + ") #</span>"
            });
        }

        return result;
    };

    //Накладываем индикацию работы кассовых окон в определённые часы среза
    var putIndication = function() {        
        $("span[typecode=" + TellerWindowScheduleHourTypes.Rest + "]").closest("td").addClass("recommended-user-hour");
        $("span[typecode=" + TellerWindowScheduleHourTypes.Work + "]").closest("td").addClass("recommended-work-hour");
        $("span[typecode=" + TellerWindowScheduleHourTypes.TechnologicalBreak + "]").closest("td").addClass("recommended-work-hour");
        $("span[typecode=" + TellerWindowScheduleHourTypes.DinnerBreak + "]").closest("td").addClass("recommended-work-hour");
    };

    //Функция, инициализирующая подвальную строку таблицы
    var initGridFooter = function(grid) {
        //Инициализируем строку с данными по рекомендуемой нагрузке
        var currentGridIndex = grid.dataSource.gridIndex;
        $("#weekdays-grid-" + currentGridIndex.toString()).find("tr.k-footer-template td:eq(0)")
                                                          .html("<span>Реком. нагр.</span>");
        var recommendedLoadSpans = $("#weekdays-grid-" + currentGridIndex.toString()).find("span.recommended-load");

        if (grid.dataSource.AreRecommendedData) {
            //Если есть рекомендуемые данные из ЭСАПР - выводим их с подсветкой (совпадает ли реальная нагрузка с рекомендуемой)
            for (var i = 0; i < recommendedLoadSpans.length; i++) {
                var factLoad = parseInt($(recommendedLoadSpans[i]).attr("fact-load"));
                var recommededLoad = parseInt($(recommendedLoadSpans[i]).text());

                if (factLoad < recommededLoad) {
                    $(recommendedLoadSpans[i]).closest("td")
                                              .attr("title", "Недостаточное количество кассовых окон")
                                              .append("<span class=\"teller-windows-deficiency\"></span>");
                } else if (factLoad > recommededLoad) {
                    $(recommendedLoadSpans[i]).closest("td")
                                              .attr("title", "Избыточное количество кассовых окон")
                                              .append("<span class=\"teller-windows-surplus\"></span>");
                } else {
                    $(recommendedLoadSpans[i]).closest("td")
                                              .attr("title", "Оптимальное количество кассовых окон");
                }
            }
        } else {
            //Если данных из ЭСАПР нет - ставим прочерки в "подвальной" строке            
            $("#weekdays-grid-" + currentGridIndex.toString()).find(".recommended-load").each(function () {
                $(this).text("-");
            });
        }
    };

    //Функция, помечающая выбранные кассовые окна в соответствующем срезе
    var markBisyTellerWindows = function (grid) {
        var data = grid.dataSource.data();

        for (var i = 0; i < data.length; i++) {
            if (data[i].TellerWindowNumber.Value !== 0) {
                var bisyTellerWindow = findTellerWindow(grid, data[i].TellerWindowNumber.Value);
                if (bisyTellerWindow) {
                    bisyTellerWindow.free = false;
                }
            }
        }
    };

    //Функция, помечающая все кассовые окна в срезе, как невыбранные
    var initTellerWindowFreedom = function(grid) {
        $.each(grid.dataSource.allTellerWindows, function (index, value) {
            value.free = true;
        });
    };

    //Функция, устанавливающая всплывающую подсказку с распиcанием работы окна 
    //для ячеек колонки с номерами окон
    var initTellerWindowScheduleHints = function(grid) {
        var data = grid.dataSource.data();
        
        for (var i = 0; i < data.length; i++) {
            var td = grid.tbody.find("td.teller-window-number-cell:eq(" + i + ")");
            if (td) {
                $(td).attr("title", data[i].WorkPeriods);
            }
        }
    };

    //Из названия и так понятно, что делает эта функция
    var initGrids = function () {        
        var season = $("#season-id").attr("data-value");
        var year = $("#year").attr("data-value");
        var sectorStationId = $("#sector-station-id").attr("data-value");        
        var tellerWIndowScheduleDayTypes = $("#teller-window-schedule-day-types").data("values");
        var tellerWindows = $("#teller-window-numbers").data("values");
        
        for (var j = 0; j < tellerWIndowScheduleDayTypes.length; j++) {
            var gridIndex = j;

            var dataSource = new kendo.data.DataSource({
                type: "json",
                transport: {
                    read: {
                        url: $("#teller-window-individual-schedules-url").attr("data-url"),
                        dataType: "json",
                        type: "POST"                                               
                    },
                    update: {
                        url: $("#teller-window-individual-schedules-save-url").attr("data-url"),
                        type: "POST",
                        contentType: 'application/json; charset=utf-8'                        
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "read") {
                            return {
                                seasonId: season,
                                tellerWindowScheduleDayTypeId: tellerWIndowScheduleDayTypes[j].TellerWindowScheduleDayTypeId,
                                year: year,
                                sectorStationId: sectorStationId
                            };
                        } else if (options.models) {
                            return JSON.stringify(options.models);
                        } else {
                            return null;
                        }
                    }
                },
                schema: {                   
                    model: {
                        id: "TellerWindowScheduleContentModelId",
                        fields: {
                            TellerWindowNumber: {},
                            HourTypes: []
                        }
                    },
                    data: "Items"                    
                },
                batch: true,
                requestStart: function (e) {
                    if (e.type === "read") {
                        var areDataInBase = tellerWIndowScheduleDayTypes[this.gridIndex].AreDataInBase;
                        if (areDataInBase === false) {
                            $("#weekdays-grid-" + this.gridIndex.toString()).mask("Идёт формирование расписания работы кассовых окон...", 500);
                        }
                    } else if (e.type === "update") {
                        $("#weekdays-grid-" + this.gridIndex.toString()).mask("Сохранение данных...", 500);
                    }
                },
                requestEnd: function(e) {
                    if (!e.response) {
                        return;
                    }

                    if (e.type === "read" && e.response.AreRecommendedData !== undefined) {
                        //Определяем, имеются ли данные из ЭСАПР для срезов данного сезона 
                        this.AreRecommendedData = e.response.AreRecommendedData;

                        if (e.response.AreRecommendedData === false) {                            
                            $("#teller-window-schedules-day-type-header-" + this.gridIndex).append('<span> - </span>')
                                                                                           .append('<span style="color: #f00">' +
                                                                                                   'данные из ЭСАПР отсутствуют' +
                                                                                                   '</span>');                            
                        }
                    }
                    
                    if (e.type === "update") {
                        if (e.response && e.response.length > 0) {
                            var data = this.data();
                            $.each(e.response, function (index, value) {
                                //Находим изменённую строку таблицы
                                var dataItems = $.grep(data, function (element) {
                                    return element.TellerWindowScheduleContentModelId ===
                                           value.TellerWindowScheduleContentModelId;
                                });

                                if (dataItems.length === 1) {
                                    //Меняем вспывающую подсказку с расписанием работы окна
                                    dataItems[0].WorkPeriods = value.WorkPeriods;
                                }
                            });
                        }                        
                    }
                },
                error: function () {
                    if (!window.unloading) {
                        alert('При получении данных возникла ошибка');
                    }
                }
            });

            //Расширяем датасорс: сообщаем ему индекс
            dataSource.gridIndex = gridIndex;            
            //Расширяем датасорс: даём список кассовых окон станции
            dataSource.allTellerWindows = [];

            $.each(tellerWindows, function (index, value) {                
                dataSource.allTellerWindows.push({
                    Value: value.Value,
                    Text: value.Text,
                    free: true
                });
            });                              
                        
            $("#weekdays-grid-" + gridIndex.toString()).kendoGrid({
                dataSource: dataSource,
                columns: columns(gridIndex),
                resizable: true,
                editable: true,
                navigatable: true,
                toolbar: [{ name: "save", text: "Сохранить" },
                            { name: "cancel", text: "Отмена" }],
                dataBound: function (e) {
                    putIndication();
                    initGridFooter(this);
                    markBisyTellerWindows(this);
                    initTellerWindowScheduleHints(this);
                    
                    $("#weekdays-grid-" + this.dataSource.gridIndex.toString()).unmask();
                }          
            });            
        }
    };

    //Обработчик нажатия на кнопку "Отмена" на панели таблицы среза.
    //Стандартный обработчик kendo grid для этого события не вызывается, если
    //режим редактирования kendo grid - incell
    var cancelButtonHandler = function(e) {
        var gridDiv = $(e.currentTarget).closest(".teller-windows-schedule-grid");
        var grid = $(gridDiv).data("kendoGrid");

        initTellerWindowFreedom(grid);
        markBisyTellerWindows(grid);
    };

    //Обработчик клика на ссылку возврата в паспорт станции
    var returnLinkClick = function(e) {
        if (($(".k-dirty").length > 0) ||
            ($(".not-saved").length > 0)) {
            if (!confirm('На форме есть несохранённые изменения. Вы действительно хотите вернуться в паспорт станции?')) {
                e.preventDefault();
            }
        }
    };

    var init = function () {
        initHeaders();
        initGrids();

        $(document).on("click", ".k-grid-cancel-changes", cancelButtonHandler);
    };

    return {        
        init: init,
        popupMenuClickHandler: popupMenuClickHandler,
        returnLinkClick: returnLinkClick
    };
})($);