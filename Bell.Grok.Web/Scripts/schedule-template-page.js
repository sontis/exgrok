﻿var scheduleTemplateModule = function($) {
    //Субмодуль для хранения url-ов
    var urlsModule = function() {
        var createUrl = $("#create-url").attr("data-url");
        var editUrl = $("#edit-url").attr("data-url");
        var readUrl = $("#read-url").attr("data-url");
        var activeUrl = $("#active-url").attr("data-url");
        var deactivateUrl = $("#deactivate-url").attr("data-url");
        var reviewUrl = $("#review-url").attr("data-url");

        return {
            createUrl: createUrl,
            editUrl: editUrl,
            readUrl: readUrl,
            activeUrl: activeUrl,
            deactivateUrl: deactivateUrl,
            reviewUrl: reviewUrl
        };
    }();

    //Субмодуль для хранения констант
    var constantsModule = function() {
        var maskMessage = "Загрузка...";
        var delayInterval = 500;
        var preferRadix = 10;
        var successCreateMessage = "Шаблон графика кассиров успешно добавлен";
        var invalidYearCreateMessage = "Шаблон графика кассиров добавлен, но по причине отсутствия года в календаре системы, праздники не показываются";
        var successEditMessage = "Шаблон графика кассиров успешно изменён";
        var dateFormat = "dd.MM.yyyy";
        var monthDateFormat = "MMMM yyyy";
        var clickEventName = "click";
        var radioButtonChangeEventName = "change";
        var fileErrorMessage = "Ошибка загрузки файла";
        var ajaxRequestErrorMessage = "get actions error";
        var currentFormFilterStateCookieName = "CurrentFormFilterState";
        var currentGridStateCookie = "CurrentGridState";

        return {
            maskMessage: maskMessage,
            delayInterval: delayInterval,
            preferRadix: preferRadix,
            successCreateMessage: successCreateMessage,
            successEditMessage: successEditMessage,
            dateFormat: dateFormat,
            clickEventName: clickEventName,
            fileErrorMessage: fileErrorMessage,
            ajaxRequestErrorMessage: ajaxRequestErrorMessage,
            monthDateFormat: monthDateFormat,
            invalidYearCreateMessage: invalidYearCreateMessage,
            radioButtonChangeEventName: radioButtonChangeEventName,
            currentFormFilterStateCookieName: currentFormFilterStateCookieName,
            currentGridStateCookie: currentGridStateCookie
        };
    }();

    //Субмодуль для хранения селекторов
    var selectorsModule = function() {
        var addButton = "#add-button";
        var editButton = "#edit-button";
        var window = "#schedule-template-create-popup";
        var saveButton = ".save-button";
        var cancelButton = ".cancel-button";
        var datePicker = "input.datepicker";
        var form = ".parent-window form";
        var errorBlock = "#errors-block";
        var fullName = "#FullName";
        var multiSelect = "select[multiple=multiple]";
        var scheduleTemplatesGrid = "#schedule-templates-grid";
        var scheduleTemplateLocations = "#locations-span";
        var actionTime = "#action-time-datepicker";
        var actionTimeContainer = "#action-time-datepicker-container";
        var globalContainer = "#global";
        var file = "input[type=file]";
        var fileList = "ul.k-upload-files";
        var uploadButton = ".k-upload-button";
        var uploadTotalStatusIndicator = ".k-upload-status-total";
        var waitMessageContainer = ".wait-message-container";
        var changeColumn = "#changeColumn";
        var blockColumn = "#blockColumn";
        var activateButtons = "[name=schedule-template-activate]";
        var deactivateButtons = "[name=schedule-template-deactivate]";
        var nonActiveTemplateTableRow = "tr:has(input[name=schedule-template-activate])";
        var activeTemplateTableRow = "tr:has(input[name=schedule-template-deactivate])";
        var filterDatePicker = ".k-datepicker input";
        var showActivityModeRadioButtons = "[name=ShowByActivity]";
        var filterForm = "#action-time-datepicker-container";

        return {
            addButton: addButton,
            editButton: editButton,
            window: window,
            saveButton: saveButton,
            cancelButton: cancelButton,
            datePicker: datePicker,
            form: form,
            errorBlock: errorBlock,
            fullName: fullName,
            multiSelect: multiSelect,
            scheduleTemplatesGrid: scheduleTemplatesGrid,
            scheduleTemplateLocations: scheduleTemplateLocations,
            actionTime: actionTime,
            actionTimeContainer: actionTimeContainer,
            globalContainer: globalContainer,
            file: file,
            fileList: fileList,
            uploadTotalStatusIndicator: uploadTotalStatusIndicator,
            uploadButton: uploadButton,
            waitMessageContainer: waitMessageContainer,
            changeColumn: changeColumn,
            blockColumn: blockColumn,
            activateButtons: activateButtons,
            deactivateButtons: deactivateButtons,
            nonActiveTemplateTableRow: nonActiveTemplateTableRow,
            activeTemplateTableRow: activeTemplateTableRow,
            filterDatePicker: filterDatePicker,
            showActivityModeRadioButtons: showActivityModeRadioButtons,
            filterForm: filterForm
        };
    }();

    //Субмодуль для kendo-window
    var kendoWindowModule = function(selectors) {
        var window = $(selectors.window).kendoWindow({
            actions: {},
            minWidth: "500px",
            minHeight: "550px",
            maxWidth: "500px",
            maxHeight: "550px",
            title: "Шаблон графика кассиров",
            modal: true,
            visible: false,
            close: function() {
                $(selectors.saveButton).off();
                $(selectors.cancelButton).off();
            }
        }).data("kendoWindow");

        //Функция, делающая неактивными все контролы на всплывающем окне 
        var disableControls = function() {
            $(selectors.fullName).attr('disabled', 'disabled');
            $(selectors.fullName).addClass("k-state-disabled");
            $(selectors.datePicker).each(function() {
                $(this).data("kendoDatePicker").enable(false);
            });
            $(selectors.multiSelect).each(function() {
                $(this).data("kendoMultiSelect").enable(false);
            });
            $(selectors.cancelButton).attr("disabled", "disabled");
            $(selectors.cancelButton).addClass("k-state-disabled");
            $(selectors.file).data("kendoUpload").disable();

            $("<div class=\"wait-message-container\">" +
                "<span class=\"wait-message\">" +
                "Подождите, пожалуйста, операция может занять несколько минут..." +
                "</span>" +
                "<\div>"
            ).insertBefore(selectors.window + " table");
        };

        //Функция, делающая активными все контролы на всплывающем окне 
        var enableControls = function() {
            $(selectors.fullName).removeAttr('disabled', 'disabled');
            $(selectors.fullName).removeClass("k-state-disabled");
            $(selectors.datePicker).each(function() {
                $(this).data("kendoDatePicker").enable(true);
            });
            $(selectors.multiSelect).each(function() {
                $(this).data("kendoMultiSelect").enable(true);
            });
            $(selectors.cancelButton).removeAttr("disabled");
            $(selectors.cancelButton).removeClass("k-state-disabled");
            $(selectors.file).data("kendoUpload").enable();

            $(selectors.waitMessageContainer).remove();
        };

        return {
            window: window,
            disableControls: disableControls,
            enableControls: enableControls
        };
    }(selectorsModule);

    //Субмодуль источника данных для таблицы шаблонов графиков кассиров
    var scheduleTemplateDataSourceModule = function(selectors, urls) {                
        var dataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            serverSorting: true,
            sort: { field: "ShortName", dir: "asc" },
            pageSize: 10,
            transport: {
                read: {
                    url: urls.readUrl,
                    dataType: "json",
                    type: "POST"
                },
                parameterMap: function(options) {                            
                    return {
                        page: options.page,
                        pageSize: options.pageSize,
                        orderby: (options.sort.length > 0) ? options.sort[0].field + ' ' + options.sort[0].dir : "",
                        filter: $(selectors.actionTimeContainer).serialize()
                    };
                }
            },
            schema: {
                model: {
                    id: "ScheduleTemplateId",
                    fields: {
                        ShortName: {},
                        FullName: {},
                        BeginDate: {},
                        EndDate: {},
                        Author: {},
                        LastAuthor: {},
                        CreationDateTime: {},
                        ChangeDateTime: {},
                        Directions: {},
                        Sectors: {}
                    }
                },
                data: "Items",
                total: "Count"
            },
            error: function() {
                if (!window.unloading) {
                    alert('При получении данных возникла ошибка');
                }
            },
            requestStart: function() {
                $(selectors.scheduleTemplateLocations).text('');
                $(selectors.editButton).addClass("k-state-disabled");
                $(selectors.editButton).attr("disabled", "disabled");
            }
        });

        return {
            dataSource: dataSource
        };
    }(selectorsModule, urlsModule);

    //Субмодуль для таблицы шаблонов графиков кассиров
    var scheduleTemplateGridModule = function (selectors, dataSource, constants) {
        var grid = $(selectors.scheduleTemplatesGrid).kendoGrid({
            autoBind: false,
            dataSource: dataSource,
            resizable: true,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            selectable: "row",
            pageable: {
                messages: {
                    display: "{0} - {1} из {2}",
                    empty: "Нет элементов для отображения",
                    page: "Страница",
                    of: "из {0}",
                    itemsPerPage: "элементов на страницу",
                    first: "К первой странице",
                    previous: "К предыдущей странице",
                    next: "К следующей странице",
                    last: "К последней странице",
                    refresh: "Обновить"
                }
            },
            columns: [
                { field: "ShortName", title: "Краткое наименование", width: "200px" },
                { field: "FullName", title: "Полное наименование", width: "200px" },
                { field: "BeginDate", title: "Дата начала", width: "125px" },
                { field: "EndDate", title: "Дата окончания", width: "125px" },
                { field: "Author", title: "Автор" },
                { field: "LastAuthor", title: "Внёс изменения" },
                { field: "CreationDateTime", title: "Дата загрузки", width: "125px" },
                { field: "ChangeDateTime", title: "Дата и время изменения", width: "180px" },
                { title: "Файл шаблона", template: kendo.template($(selectors.changeColumn).html()), sortable: false, width: "110px" },
                { title: "Статус", template: kendo.template($(selectors.blockColumn).html()), sortable: false }],
            change: function() {
                var selectedRow = $(selectors.scheduleTemplatesGrid).data("kendoGrid").select();
                var gridData = $(selectors.scheduleTemplatesGrid).data("kendoGrid");
                var dataItem = gridData.dataItem(selectedRow);

                $(selectors.scheduleTemplateLocations).addClass("k-widget");
                $(selectors.scheduleTemplateLocations).addClass("k-header");

                if (dataItem.Directions === "") {
                    if (dataItem.Sectors === "") {
                        $(selectors.scheduleTemplateLocations).text(' Шаблон действует на всех направлениях ');
                    } else {
                        $(selectors.scheduleTemplateLocations).text(dataItem.Sectors);
                    }
                } else if (dataItem.Sectors === "") {
                    $(selectors.scheduleTemplateLocations).text(dataItem.Directions);
                } else {
                    $(selectors.scheduleTemplateLocations).text(dataItem.Directions + ", " + dataItem.Sectors);
                }

                $(selectors.editButton).removeClass("k-state-disabled");
                $(selectors.editButton).removeAttr("disabled");
            },
            dataBound: function(e) {
                $(selectors.nonActiveTemplateTableRow).addClass('rowBlocked');
                $(selectors.nonActiveTemplateTableRow).attr('title', 'Шаблон графика неактивен');
                $(selectors.activeTemplateTableRow).attr('title', 'Шаблон графика активен');

                //Сохраняем в сессию текущее состояние грида
                var currentGridState = kendo.stringify({
                    page: dataSource.page(),
                    pageSize: dataSource.pageSize(),
                    sort: dataSource.sort(),
                    group: dataSource.group()                            
                });
                var currentFormFilterState = $(selectors.filterForm).serialize();

                $.session.set(constants.currentGridStateCookie, currentGridState);
                $.session.set(constants.currentFormFilterStateCookieName, currentFormFilterState);
            }
        }).data("kendoGrid");
    }(selectorsModule, scheduleTemplateDataSourceModule.dataSource, constantsModule);

    //Субмодуль обработчиков событий
    var handlersModule = function(selectors, urls, kendoWindowWrapper, scheduleTemplatesDataSource, constants) {
        var kendoWindow = kendoWindowWrapper.window;

        //Функция, обрабатывающая потерю выделения в таблице шаблонов графиков кассиров
        var unselect = function() {
            $(selectors.scheduleTemplateLocations).text('');
            $(selectors.editButton).addClass("k-state-disabled");
            $(selectors.editButton).attr("disabled", "disabled");
        };
        //Функция редактирования шаблона графика кассиров (кнопка Сохранить на всплывающем окне)
        var saveButton = function(event) {
            validationInit();

            var data = $(selectors.form).serialize();
            ValidationHelper.clearErrors();

            var operationUrl = event.data.url;
            var isCreate = event.data.isCreate;

            if ($(selectors.form).valid()) {
                $.post(operationUrl, data, function(response) {
                    if (response.status == JsonStatus.ok) {
                        kendoWindow.close();
                        scheduleTemplatesDataSource.read();
                        unselect();

                        if (isCreate) {
                            Utils.Notification.show(constants.successCreateMessage);
                        } else {
                            Utils.Notification.show(constants.successEditMessage);
                        }
                    }

                    if (response.status == JsonStatus.invalid) {
                        ValidationHelper.showValidationErrors(response.errors, selectors.form);
                    }

                    if (response.status === JsonStatus.error) {
                        alert(response.message);
                    }
                })
                    .error(function(e) {
                        alert(constants.ajaxRequestErrorMessage);
                    });
            }
        };
        //Функция закрытия всплывающего окна (кнопка Отмена на всплывающем окне)
        var cancelButton = function() {
            kendoWindow.close();
        };
        //Функция инициализации kendoDatePicker-ов
        var datePickerInit = function() {
            if (!$(selectors.datePicker).data("kendoDatePicker")) {
                $(selectors.datePicker).kendoDatePicker({
                    format: constants.dateFormat,
                });
            } else {
                $(selectors.datePicker).each(function() {
                    $(this).data("kendoDatePicker").value('');
                });
            }
        };
        //Функция инициализации kendoMultiSelect-ов
        var multiSelectInit = function() {
            if (!$(selectors.multiSelect).data("kendoMultiSelect")) {
                $(selectors.multiSelect).kendoMultiSelect({
                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                });
            } else {
                $(selectors.multiSelect).each(function() {
                    $(this).data("kendoMultiSelect").value([]);
                });
            }
        };
        //Функция инициализации kendo-upload-ра
        var uploaderInit = function() {
            var acceptedFileTypes = [
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            ];
            var acceptedFileExtensions = [
                ".xlsx"
            ];


            $(selectors.file).kendoUpload({
                async: {
                    saveUrl: urls.createUrl,
                    autoUpload: true
                },
                localization: {
                    "select": "Загрузить",
                    "cancel": "Отмена",
                    "retry": "Повторить",
                    "remove": "Удалить",
                    "uploadSelectedFiles": "Загрузить",
                    "statusUploading": "загрузка",
                    "statusUploaded": "загружен",
                    "statusFailed": "произошла ошибка"
                },
                multiple: false,
                success: function(e) {
                    //Обработка успешной закачки файла                            
                    var response = e.response;

                    if (response.serverStatus === JsonStatus.ok) {
                        kendoWindow.close();
                        scheduleTemplatesDataSource.read();
                        unselect();

                        if (response.isValidYear) {
                            Utils.Notification.show(constants.successCreateMessage);
                        } else {
                            alert(constants.invalidYearCreateMessage);
                        }
                    }

                    if (response.serverStatus === JsonStatus.invalid) {
                        $(selectors.fileList).remove();
                        $(selectors.uploadTotalStatusIndicator).remove();
                        ValidationHelper.showValidationErrors(response.errors, selectors.form);
                    }

                    if (response.serverStatus === JsonStatus.error) {
                        $(selectors.fileList).remove();
                        $(selectors.uploadTotalStatusIndicator).remove();
                        alert(response.message);
                    }
                },
                error: function(e) {
                    if (!window.unloading) {
                        alert(constants.fileErrorMessage);
                    }
                },
                upload: function(e) {
                    ValidationHelper.clearErrors();

                    if (e.files[0].rawFile && e.files[0].rawFile.type) {
                        //Не все браузеры понимают атрибут accept тэга input типа file, поэтому
                        //проверяем самостоятельно, исходя из метаинформации, любезно предоставленной
                        //kendo upload-ом
                        var fileType = e.files[0].rawFile.type;

                        if ($.inArray(fileType, acceptedFileTypes) === -1) {
                            alert("Файл \'" + e.files[0].name +
                                "\' не является документом MS EXCEL 2007. Формат не xlsx.");

                            e.preventDefault();
                            return;
                        }
                    } else {
                        //Специально для ie<=9 чиполинный код, определяющий расширение файла
                        //и в соответствии с ним принимающий решение о валидности файла
                        var fileExtension = e.files[0].extension;

                        if ($.inArray(fileExtension, acceptedFileExtensions) === -1) {
                            alert("Файл \'" + e.files[0].name +
                                "\' не является документом MS EXCEL 2007. Формат не xlsx.");

                            e.preventDefault();
                            return;
                        }
                    }

                    //Формирование параметров, отправляемых на сервер                            
                    e.data = Utils.Serializer.serializeFormToObject(selectors.form);

                    //Делаем неактивными все контролы, находящиеся на окне - защита от дурака
                    kendoWindowWrapper.disableControls();
                },
                select: function(e) {
                },
                remove: function() {
                },
                complete: function() {
                    //делаем активными все контролы
                    kendoWindowWrapper.enableControls();
                }
            });
        };
        //Функция обработки создания нового шаблона графика кассиров и закачки excel-файла (кнопка загрузить во всплывающем окне)
        var loadButton = function(e) {
            validationInit();
            ValidationHelper.clearErrors();

            if (!$(selectors.form).valid()) {
                e.preventDefault();
            }
        };
        //Функция инициализации jquery validation
        var validationInit = function() {
            $.validator.unobtrusive.parse(selectors.form);
            ValidationHelper.init({
                validationSummarySelector: selectors.errorBlock
            });
            ValidationHelper.clearErrors();
        };
        //Функция, инициализирующая всплывающее окно для создания шаблона
        var addButton = function() {
            $.ajax({
                url: urls.createUrl,
                type: 'GET',
                beforeSend: function() {
                    //Показываем ajax-кругляшок на время выполнения запроса
                    $(selectors.globalContainer)
                        .mask(constants.maskMessage,
                            parseInt(constants.delayInterval, constants.preferRadix));
                },
                success: function(response) {
                    kendoWindow.content(response);
                    kendoWindow.center();
                    kendoWindow.open();

                    datePickerInit();
                    multiSelectInit();
                    uploaderInit();

                    $(selectors.uploadButton)
                        .after("<input type=\"button\" value=\"Отмена\" class=\"cancel-button k-button button-left-indent \"/>");

                    $(selectors.file).on(constants.clickEventName, loadButton);
                    $(selectors.cancelButton).on(constants.clickEventName, cancelButton);
                },
                error: function() {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function() {
                    $(selectors.globalContainer)
                        .unmask();
                }
            });
        };
        //Функция, инициализирующая всплывающее окно для редактирования шаблона
        var editButton = function() {
            var selectedRow = $(selectors.scheduleTemplatesGrid).data("kendoGrid").select();
            var gridData = $(selectors.scheduleTemplatesGrid).data("kendoGrid");
            var dataItem = gridData.dataItem(selectedRow);
            var scheduleTemplateId = dataItem.ScheduleTemplateId;

            $.ajax({
                url: urls.editUrl,
                type: 'GET',
                data: { scheduleTemplateId: scheduleTemplateId },
                beforeSend: function() {
                    //Показываем ajax-кругляшок на время выполнения запроса
                    $(selectors.globalContainer)
                        .mask(constants.maskMessage,
                            parseInt(constants.delayInterval, constants.preferRadix));
                },
                success: function(response) {
                    kendoWindow.content(response);
                    kendoWindow.center();
                    kendoWindow.open();

                    datePickerInit();
                    multiSelectInit();

                    $(selectors.saveButton).on(constants.clickEventName, { url: urls.editUrl, isCreate: false }, saveButton);
                    $(selectors.cancelButton).on(constants.clickEventName, cancelButton);
                },
                error: function() {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function() {
                    $(selectors.globalContainer)
                        .unmask();
                }
            });
        };
        //Функция, активирующая шаблон графика работы кассиров
        var activateButtons = function(e) {
            var gridData = $(selectors.scheduleTemplatesGrid).data("kendoGrid");
            var dataItem = gridData.dataItem($(e.currentTarget).closest("tr"));

            if (confirm('Вы действительно хотите активировать шаблон ' + dataItem.ShortName + '?')) {
                $.post(urls.activeUrl, { scheduleTemplateId: dataItem.ScheduleTemplateId }, function(result) {
                    if (result.status === JsonStatus.ok) {
                        dataItem.IsActive = result.isActive;
                        scheduleTemplatesDataSource.read();
                        unselect();
                        Utils.Notification.show('Шаблон ' + dataItem.ShortName + ' активирован');
                    }

                    if (result.status === JsonStatus.error) {
                        alert(result.message);
                    }
                })
                    .error(function() {
                        alert("get actions error");
                    });
            }
        };
        //Функция, деактивирующая шаблон графика работы кассиров
        var deactivateButtons = function(e) {
            var gridData = $(selectors.scheduleTemplatesGrid).data("kendoGrid");
            var dataItem = gridData.dataItem($(e.currentTarget).closest("tr"));

            if (confirm('Вы действительно хотите Деактивировать шаблон ' + dataItem.ShortName + '?')) {
                $.post(urls.deactivateUrl, { scheduleTemplateId: dataItem.ScheduleTemplateId }, function(result) {
                    if (result.status === JsonStatus.ok) {
                        dataItem.IsActive = result.isActive;
                        scheduleTemplatesDataSource.read();
                        unselect();
                        Utils.Notification.show('Шаблон ' + dataItem.ShortName + ' деактивирован');
                    }

                    if (result.status === JsonStatus.error) {
                        alert(result.message);
                    }
                })
                    .error(function() {
                        alert("get actions error");
                    });
            }
        };
        //Обработчик нажатия на Enter в фильтре шаблонов по периоду их активности
        var inputKeyPressed = function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                e.preventDefault();
            }
        };
        //Обработчик смены значений радиобаттонов выбора режима показа шаблонов (активные, неактивные, все)
        var showModeChanged = function() {
            scheduleTemplatesDataSource.page(1);
            unselect();
        };
        //Функция инициализации страницы
        var init = function () {
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth();
            var day = today.getDate();
            var currentActionTime = new Date(year, month, day);

            //Фильтр по дате действия шаблона графика кассиров
            $(selectors.actionTime).kendoDatePicker({
                start: "year",
                depth: "year",
                format: constants.monthDateFormat,
                value: currentActionTime,
                change: function() {
                    var value = $(selectors.actionTime).val();

                    //Строим регулярное выражение
                    var monthsWords = "(январь?|февраль?|март?|апрель?|май|июнь?|июль?|август?|сентябрь?|октябрь?|ноябрь?|декабрь?)";
                    var months = "((" + monthsWords + "))";
                    var years = "\\d{4}";
                    var separator = "\\s";
                    var pattern = months + separator + years;
                    var regexp = new RegExp(pattern, "i");

                    //Проверяем с помощью регулярного выражения введённую дату
                    if (!value.match(regexp)) {
                        $(selectors.actionTime).addClass("grok-validation-error");
                        $(selectors.actionTime).kendoTooltip({
                            content: "Недопустимая дата. Необходимый формат: " + "[Название месяца полностью][Пробел][Год(yyyy)]",
                            width: 150,
                            position: "top"
                        });
                    } else {
                        $(selectors.actionTime).removeClass("grok-validation-error");
                        if ($(selectors.actionTime).data("kendoTooltip") !== undefined) {
                            $(selectors.actionTime).data("kendoTooltip").destroy();
                        }

                        scheduleTemplatesDataSource.page(1);
                        unselect();
                    }
                }
            });
                    
            //Восстанавливаем из сессии форму фильтрации
            var filterparams = $.session.get(constants.currentFormFilterStateCookieName);
            if (filterparams !== null && filterparams !== undefined) {
                $(selectors.filterForm).deserialize(filterparams);
            }

            //Восстанавливаем из сессии состояние грида.
            //Если в сессии ничего нет, то грузим грид по умолчанию
            var scheduleTemplatesGrid = $(selectors.scheduleTemplatesGrid).data("kendoGrid");
            var gridStateFromSession = $.session.get(constants.currentGridStateCookie);
            if (gridStateFromSession !== undefined) {
                var initialState = JSON.parse(gridStateFromSession);
                if (initialState) {
                    scheduleTemplatesGrid.dataSource.query(initialState);
                } else {
                    scheduleTemplatesGrid.dataSource.read();
                }
            } else {
                scheduleTemplatesGrid.dataSource.read();
            }                    
        };

        return {
            addButton: addButton,
            editButton: editButton,
            init: init,
            activateButtons: activateButtons,
            deactivateButtons: deactivateButtons,
            inputKeyPressed: inputKeyPressed,
            showModeChanged: showModeChanged
        };
    }(selectorsModule, urlsModule, kendoWindowModule, scheduleTemplateDataSourceModule.dataSource, constantsModule);

    return {
        urls: urlsModule,
        selectors: selectorsModule,
        kendoWindow: kendoWindowModule,
        scheduleTemplateDataSourceModule: scheduleTemplateDataSourceModule,
        scheduleTemplateGridModule: scheduleTemplateGridModule,
        handlers: handlersModule,
        constants: constantsModule,
    };
};


$(function() {
    kendo.culture("ru-RU");

    var scheduleTemplate = scheduleTemplateModule($);

    scheduleTemplate.handlers.init();
    $(scheduleTemplate.selectors.addButton)
        .click(scheduleTemplate.handlers.addButton);
    $(scheduleTemplate.selectors.editButton)
        .click(scheduleTemplate.handlers.editButton);

    $(scheduleTemplate.selectors.filterDatePicker)
        .keypress(scheduleTemplate.handlers.inputKeyPressed);

    $(scheduleTemplate.selectors.scheduleTemplatesGrid)
        .on(scheduleTemplate.constants.clickEventName,
            scheduleTemplate.selectors.activateButtons,
            scheduleTemplate.handlers.activateButtons);

    $(scheduleTemplate.selectors.scheduleTemplatesGrid)
        .on(scheduleTemplate.constants.clickEventName,
            scheduleTemplate.selectors.deactivateButtons,
            scheduleTemplate.handlers.deactivateButtons);

    $(scheduleTemplate.selectors.filterForm).
        on(scheduleTemplate.constants.radioButtonChangeEventName,
            scheduleTemplate.selectors.showActivityModeRadioButtons,
            scheduleTemplate.handlers.showModeChanged);            
});