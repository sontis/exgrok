﻿$(document).ready(function () {
    kendo.culture("ru-RU");

    var modelId = $("#modelId").attr("data-value");
    var modelCode = $("#modelCode").attr("data-value");
    var modelCanEdit = $("#modelCanEdit").attr("data-value").toLowerCase() === "true";
    var modelFilterBPAStandart = $("#modelFilterBPAStandart").attr("data-value");
    var modelFilterMKTKStandart = $("#modelFilterMKTKStandart").attr("data-value");     

    //Перзагрузка страницы при изменении фильтров
    $('.ddList').kendoDropDownList({
        change: function () {
            //Сохраняем в куках состояния фильтров
            StandartsCookieModule.saveFilters();
            //Сбрасываем в куках состояние грида, так как при фильтрации 
            //пейджинг и сортинг выставляются с нуля
            StandartsCookieModule.resetMainGridState();
            
            StandartsCookieModule.loadMainGridState();
        }
    });

    //Идентификаторы выделенных строк с нормативами и номер столбца
    var selectedItemIds = [];
    var selectedItemIdsOnPage = [];
    var selectedColumn = 0;

    var viewModel = kendo.observable({
        id: modelId,
        code: modelCode,
        //Флаг, доступно ли всплывающее окно для открытия. 
        //Сбрасывается после открытия окна и восстанавливается
        //после окончания процесса внесения изменений.
        isWindowEnabled: true,
        standartRowsSource: new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            serverSorting: true,
            sort: { field: "StationName", dir: "asc" },
            serverFiltering: true,            
            pageSize: 10,
            batch: true,
            transport: {
                read: {
                    url: $("#getStandartRows-url").attr("data-url"),
                    dataType: "json",
                    type: "POST"
                },
                parameterMap: function (options) {                    
                    var orderby = null;
                    var orderto = null;
                    if ((options.sort) && (options.sort.length > 0)) {
                        orderby = options.sort[0].field;
                        orderto = options.sort[0].dir;
                    }

                    var filterBpa = viewModel.filterBpa;
                    var filterMktk = viewModel.filterMktk;

                    return {
                        page: options.page,
                        pageSize: options.pageSize,
                        id: viewModel.id,
                        code: viewModel.code,
                        orderby: orderby,
                        orderto: orderto,
                        filterBpa: filterBpa,
                        filterMktk: filterMktk,
                    };
                },
            },
            schema: {
                model: {
                    id: "SectorStationId",
                    fields: {
                        StationName: {},
                        BPA: {},
                        MKTK: {},
                    }
                },
                data: "Items",
                total: "Count"
            },
            error:
                function () {
                    if (!window.unloading) {
                        alert('При получении данных возникла ошибка');
                    }
                }
        }),
        //глобальный флаг, показывающий onSelectionChange что запрос уже обрабатывался
        refreshing: false,
        ///Выделение элементов которые есть в selectedItemIds
        onDataBound: function () {
            //Сохраняем в куках состояние грида
            StandartsCookieModule.saveMainGridState();

            var newSelected = [];
            var mainGrid = $("#mainGrid").data("kendoGrid");

            for (var i = 0; i < mainGrid._data.length; i++) {
                var id = mainGrid._data[i].get('id');

                var index = selectedItemIds.indexOf(id);
                if (index > -1) {
                    newSelected.push($($("#mainGrid tbody tr")[i]).find('td')[selectedColumn]);
                }
            }

            this.set("refreshing", true);

            mainGrid.clearSelection();
            mainGrid.select(newSelected);

            this.set("refreshing", false);

        },
        ///Обработка изменений выделенных элементов
        onSelectionChange: function (arg) {
            var mainGrid = $("#mainGrid").data("kendoGrid");

            if (!this.get("refreshing")) {
                var selected = mainGrid.select();                
                var column = 0;

                var newSelected = [];
                for (var i = 0; i < selected.length; i++) {
                    if ((column === 0) && (selected[i].cellIndex !== 0)) {
                        column = selected[i].cellIndex;
                    }

                    if (column !== 0) {
                        if (selected[i].cellIndex === column) {
                            newSelected.push(selected[i]);
                        } else {
                        }
                    }
                }

                if (!((column === 0) && (selected.length > 0))) { //Ignore click on first column
                    if (selectedColumn != column) {
                        selectedItemIds = [];
                        selectedColumn = column;
                    } else {
                        for (var i = 0; i < mainGrid._data.length; i++) {
                            var id = mainGrid._data[i].get('id');

                            var index = selectedItemIds.indexOf(id);
                            if (index > -1) {
                                selectedItemIds.splice(index, 1);
                            }
                        }
                    }

                    var indexes = [];
                    var rowIndex;
                    for (var i = 0; i < selected.length; i++) {
                        if (selected[i].cellIndex !== 0) {
                            rowIndex = $(selected[i]).parents("tr")[0].rowIndex;
                            indexes.push(rowIndex);
                        }
                    }

                    selectedItemIdsOnPage = [];

                    for (var i = 0; i < indexes.length; i++) {
                        rowIndex = indexes[i];
                        var id = mainGrid._data[rowIndex].get('id');
                        selectedItemIds.push(id);
                        selectedItemIdsOnPage.push(id);
                    }
                } else {
                    var newSelected = [];

                    for (var i = 0; i < mainGrid._data.length; i++) {
                        var id = mainGrid._data[i].get('id');

                        var index = selectedItemIds.indexOf(id);
                        if (index > -1) {
                            newSelected.push($($("#mainGrid tbody tr")[i]).find('td')[selectedColumn]);
                        }
                    }
                }

                this.set("refreshing", true);

                mainGrid.clearSelection();
                mainGrid.select(newSelected);

                this.set("refreshing", false);
            }
        },
        //Обработчик кнопки выделения всех элементов
        selectAll: function () {

            var filterBpa = viewModel.filterBpa;
            var filterMktk = viewModel.filterMktk;

            $("#mainGrid").mask("Загрузка...");

            $.ajax({
                url: $("#getStandartRows-url").attr("data-url"),
                dataType: "json",
                type: "POST",
                data: JSON.stringify({
                    page: 1,
                    pageSize: 100000,
                    id: viewModel.id,
                    code: viewModel.code,
                    orderby: null,
                    orderto: null,
                    filterBpa: filterBpa,
                    filterMktk: filterMktk,
                }),
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    if (selectedItemIds.length === 0) {
                        selectedColumn = 1;
                    }
                    if (data.Items.length === selectedItemIds.length) {
                        if (selectedColumn === 1) {
                            selectedColumn = 2;
                        } else {
                            selectedColumn = 0;
                        }
                    }

                    selectedItemIds = [];

                    if (selectedColumn !== 0) {
                        for (var i = 0; i < data.Items.length; i++) {
                            selectedItemIds.push(data.Items[i].SectorStationId);
                        }
                    }

                    viewModel.onDataBound();
                    $("#mainGrid").unmask();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (!window.unloading) {
                        alert("Ошибка при пересылке данных");
                    }
                }
            });
        },
        //Появление окна для изменения нормативов
        сhange: function () {
            var isWindowEnabled = this.get('isWindowEnabled');
            
            if (selectedItemIds.length > 0 &&
                isWindowEnabled) {
                this.set('isWindowEnabled', false);
                this.set("changeValue", "");                
                popupWindow.open();

                setTimeout(function () { $("#changeInput").data("kendoNumericTextBox").focus(); }, 500);
            }
        },
        //Значение на которое следует изменить нормативы
        changeValue: "",
        changeReject: function () {
            popupWindow.close();
        },
        //Функция, вычисляющая количество страниц
        calculatePageCount: function (totalCount) {
            var dataSource = $("#mainGrid").data("kendoGrid").dataSource;
            var pageSize = dataSource.pageSize();
            var fullPageCount = Math.floor(totalCount / pageSize);
            var isNotFullPage = (totalCount % pageSize) > 0;
            var notFullPageCount = isNotFullPage
                                        ? 1
                                        : 0;

            return fullPageCount + notFullPageCount;            
        },
        //Функция, обновляющая значения фильтров после изменения значений в таблице
        refillFilters: function (standarts, selector) {
            if (standarts) {
                var dropDownList = $(selector).data("kendoDropDownList");
                var currentStandartValue = parseInt(dropDownList.value());
                var dropDownListData = dropDownList.dataSource.data();
                var length = dropDownListData.length;
                
                var item, i;
                //Удаляем все значения в фильтре
                for (i = length - 1; i >= 0; i--) {

                    item = dropDownListData[i];                    
                    dropDownList.dataSource.remove(item);
                }
                
                //Добавляем новый набор, пришедший с сервера
                dropDownList.dataSource.add({ value: -1, text: "Не выбрано" });
                $.each(standarts, function (index, element) {                    
                    dropDownList.dataSource.add({ value: element, text: element });                    
                });
                
                //Ищем выделенный до обновления элемент
                var dataItems = $.grep(dropDownListData, function (element) {
                    return element.value === currentStandartValue;
                });
                
                //Если значение фильтра по-прежнему присутствует в таблице - выделяем его
                if (dataItems.length === 1) {
                    dropDownList.select(function (dataItem) {
                        return dataItem.value === currentStandartValue;
                    });
                } else {
                //Иначе выделяем "Не выбрано"
                    dropDownList.select(0);                    
                }                    
                
            }            
        },
        //Сохранение изменений
        changeSubmit: function () {
            var self = this;

            if ((!viewModel.changeValue) && (viewModel.changeValue != "0")) {
                alert("Не введено значение");
                return;
            }

            if (parseInt(viewModel.changeValue) > 500) {
                alert("Введено слишком большое значение");
                return;
            }

            popupWindow.close();
            $("#mainGrid").mask("Сохранение...");

            $.ajax({
                url: $("#setStandartRows-url").attr("data-url"),
                type: "POST",
                data: JSON.stringify({ ids: selectedItemIds, value: viewModel.changeValue, column: selectedColumn, 
                    filterBpa: viewModel.filterBpa, filterMktk: viewModel.filterMktk, id: modelId, code: modelCode
                }),
                contentType: 'application/json; charset=utf-8',
                success: function (e) {
                    selectedItemIds = [];
                    Utils.Notification.show("Изменения сохранены");

                    var mainGrid = $("#mainGrid").data("kendoGrid");
                    var selected = mainGrid.select();
                    var indexes = [];
                    var rowIndex;
                    var column = 1;
                    
                    for (var i = 0; i < selected.length; i++) {
                        if (selected[i].cellIndex !== 0) {
                            rowIndex = $(selected[i]).parents("tr")[0].rowIndex;
                            indexes.push(rowIndex);
                            column = selected[i].cellIndex;
                        }
                    }
                    for (var i = 0; i < indexes.length; i++) {
                        rowIndex = indexes[i];

                        if (column === 1) {
                            mainGrid._data[rowIndex].set('MKTK', viewModel.changeValue);
                        } else {
                            mainGrid._data[rowIndex].set('BPA', viewModel.changeValue);
                        }

                    }
                    
                    //Обновление фильтров
                    if (e.allSectorBpaStandarts) {
                        viewModel.refillFilters(e.allSectorBpaStandarts, "#FilterBPAStandart");
                    }
                    if (e.allSectorMktkStandarts) {
                        viewModel.refillFilters(e.allSectorMktkStandarts, '#FilterMKTKStandart');
                    }

                    var bpaFilterValue = $("#FilterBPAStandart").data("kendoDropDownList").value();
                    var mktkFilterValue = $("#FilterMKTKStandart").data("kendoDropDownList").value();

                    viewModel.set("filterBpa", bpaFilterValue);
                    viewModel.set("filterMktk", mktkFilterValue);

                    //Анализ номера новой текущей страницы
                    if (e.newFilteredValuesCount !== undefined && e.newFilteredValuesCount !== null) {
                        var newPageCount = viewModel.calculatePageCount(e.newFilteredValuesCount);
                        var currentPageCount = mainGrid.dataSource.page();
                        
                        if (newPageCount > 0 && newPageCount < currentPageCount) {
                            mainGrid.dataSource.page(newPageCount);
                        } else {
                            mainGrid.dataSource.page(currentPageCount);
                        }
                    } else {
                        mainGrid.clearSelection();
                        $("#mainGrid").mask("Обновление данных...");
                        window.location.href = $("#this-url").attr("data-url") + "?id=" + modelId + "&code=" + modelCode + "&filterBpa=" + viewModel.filterBpa + "&filterMktk=" + viewModel.filterMktk;
                    }                                       
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (!window.unloading) {
                        alert("Ошибка при пересылке данных");
                    }
                },
                complete: function() {
                    $("#mainGrid").unmask();
                    self.set('isWindowEnabled', true);
                }
            });


        },
        //Значения фильтров
        filterBpa: modelFilterBPAStandart,
        filterMktk: modelFilterMKTKStandart,
    });

    var savedValue = StandartsCookieModule.getFilterBpa();
    if (savedValue != null) {
        viewModel.filterBpa = savedValue;
    }

    savedValue = StandartsCookieModule.getFilterMktk();
    if (savedValue != null) {
        viewModel.filterMktk = savedValue;
    }

    kendo.bind($("form"), viewModel);    

    //Окно для внесения изменений
    var popupWindow = $("#change-popup").kendoWindow({
        actions: {

        },
        minWidth:
            "250px",
        minHeight:
            "150px",
        maxWidth:
            "250px",
        maxHeight:
            "150px",
        modal:
            true,
        visible:
            false,
        open: function() {            
        },
        close: function () {            
        }
    }).data("kendoWindow");
    
    popupWindow.center();

    //Убирание выделения с других страниц
    $("#mainGrid").data("kendoGrid").element.on('click', 'tbody>tr>td:not(.k-edit-cell)', function (e) {
        if (!e.ctrlKey) {
            if ((e.currentTarget.cellIndex) && (e.currentTarget.cellIndex != 0)) {
                selectedItemIds = selectedItemIdsOnPage;
            }
        }
    });


    //Редактирование через нажатие на Enter
    if (modelCanEdit) {
        document.addEventListener("keyup",
            function (e) {
                if (event.keyCode == 13) {
                    viewModel.сhange();
                }
            },
            false);

        $("#changeInput").keyup(function (event) {
            if (event.keyCode == 13) {
                viewModel.changeSubmit();
            }
        });
    }

    //Восстанавливаем из сессии состояние грида с нормативами только после активации вкладки
    $(document).on("editStandartTabActivated", StandartsCookieModule.loadMainGridState);
    
    //Если событие активации вкладки уже было, вызываем проверку наличия таймшита вручную        
    if ($("#is-edit-standart-activated").attr("data-is-activated") === "1") {
        StandartsCookieModule.loadMainGridState();
    }
});

//Модуль для управления хранением настроек в куки
var StandartsCookieModule = (function () {
    //Определяем идентификатор направления/участка
    var directionSectorId = $.urlParam("id");
    //определяем тип страницы (направление или участок)
    var objectCode = $.urlParam("code");

    var loadMainGridState = function () {
        var mainGrid = $("#mainGrid").data("kendoGrid");        

        var mainGridStateFromSession = $.session.get("mainGridState" + "-" + objectCode + directionSectorId);
        if (mainGridStateFromSession !== undefined) {
            var initialState = JSON.parse(mainGridStateFromSession);
            if (initialState) {
                mainGrid.dataSource.query(initialState);
            } else {
                mainGrid.dataSource.read();
            }
        } else {
            mainGrid.dataSource.read();
        }
        
        $(document).off("editStandartTabActivated");
    };

    var getFilterBpa = function () {
        return $.session.get("filterBpa" + "-" + objectCode + directionSectorId);
    };

    var getFilterMktk = function () {
        return $.session.get("filterMktk" + "-" + objectCode + directionSectorId);
    };

    var saveFilters = function () {
        var currentFilterMktkValue = $("#FilterMKTKStandart").data("kendoDropDownList").value();
        var currentFilterBpaValue = $("#FilterBPAStandart").data("kendoDropDownList").value();
        $.session.set("filterMktk" + "-" + objectCode + directionSectorId, currentFilterMktkValue);
        $.session.set("filterBpa" + "-" + objectCode + directionSectorId, currentFilterBpaValue);
    };

    var saveMainGridState = function () {
        var dataSource = $("#mainGrid").data("kendoGrid").dataSource;

        var currentGridState = kendo.stringify({
            page: dataSource.page(),
            pageSize: dataSource.pageSize(),
            sort: dataSource.sort(),
            group: dataSource.group()
        });

        $.session.set("mainGridState" + "-" + objectCode + directionSectorId, currentGridState);
    };

    var resetMainGridState = function () {
        var initGridState = '{"page":1,' +
                            '"pageSize":10,' +
                            '"sort":[{"field":"StationName","dir":"asc"}]' +
                            ',"group":[]}';

        $.session.set("mainGridState" + "-" + objectCode + directionSectorId, initGridState);
    };

    return {        
        loadMainGridState: loadMainGridState,
        getFilterBpa: getFilterBpa,
        getFilterMktk: getFilterMktk,
        saveFilters: saveFilters,
        saveMainGridState: saveMainGridState,
        resetMainGridState: resetMainGridState
    };
}());