﻿TerminalNumberValidationStatus = {
    busy: 1,
    exists: 2,
    absent: 3,
    invalid: 4
};

$(function () {
    kendo.culture("ru-RU");    

    var viewModel = kendo.observable({
        typeChange: function (e) {
            TypeMarksModule.check();
        },
        submitPressed: function () {
        },
        nextPressed: function () {
            //Удаляем сообщения об ошибках
            $("#validationSummary").html('');

            //Валидируем номер терминала, а также даты ввода и вывода
            $("form").validate().element("#Number");
            $("form").validate().element("#BeginTime");
            $("form").validate().element("#EndTime");

            //Если введённые данные валидны
            if ($("#Number").hasClass("valid") &&
                $("#BeginTime").hasClass("valid") &&
                $("#EndTime").hasClass("valid")) {

                var terminalGetInfoUrl = $("#terminal-get-info").attr("data-url");
                var terminalNumber = $("#Number").val();
                var terminalBeginDate = kendo.toString($("#BeginTime").data("kendoDatePicker").value(), "dd.MM.yyyy");
                var terminalEndDate = kendo.toString($("#EndTime").data("kendoDatePicker").value(), "dd.MM.yyyy");

                //Запрашиваем информацию о терминале по его номеру
                $.post(terminalGetInfoUrl, { number: terminalNumber, beginDate: terminalBeginDate, endDate: terminalEndDate }, function (response) {
                    //Если терминал существует и используется в указанный промежуток времени или введены некорректные данные
                    if (response.status === TerminalNumberValidationStatus.busy || response.status === TerminalNumberValidationStatus.invalid) {
                        var validationSummary = $('#validationSummary ul.validation-summary-errors');
                        if (validationSummary.length == 0) {
                            $('#validationSummary').html('<ul class="validation-summary-errors"></ul>');
                            validationSummary = $('#validationSummary ul.validation-summary-errors');
                        }
                        validationSummary.append('<li>' + response.message + '</li>');
                    }

                    //Если терминал с таким номером уже существует и не занят в указанном промежутке времени
                    if (response.status == TerminalNumberValidationStatus.exists) {
                        //Делаем видимыми остальные контролы
                        ShowInvisiblePart();

                        //Скрываем сообщения валидации
                        $('#validationSummary').html('');

                        //Устанавливаем значения контролов в соответствии с данными терминала
                        $("#Type").val(response.body.Type);

                        //Обновление списка моделей в зависимости от типа
                        TypeMarksModule.check();

                        //Если у терминала есть модель
                        if (response.body.Model !== -1) {
                            //Сохраняем значение модели в разметке.
                            //Обработчик ajax-запроса в TypeMarksModule.check()
                            //использует это значение
                            $("#current-model").attr("data-model", response.body.Model);
                            $("#current-model").attr("data-is-actual", true);
                        }

                        $("#Profile").val(response.body.Profile);
                        $("#Status").val(response.body.Status);
                        $("#Agent").val(response.body.Agent);
                        $("#Purpose").val(response.body.Purpose);
                    }

                    //Если терминала с таким номером нет, то сбрасываем контролы в исходное положение
                    if (response.status === TerminalNumberValidationStatus.absent) {
                        //Делаем видимыми остальные контролы
                        ShowInvisiblePart();

                        //Скрываем сообщения валидации
                        $('#validationSummary').html('');

                        $("#Type").val("");

                        $("#Mark option").remove();
                        $("#Mark").attr("disabled", "disabled");

                        $("#Profile").val("");
                        $("#Status").val("");
                        $("#Agent").val("");
                        $("#Purpose").val("");
                    }
                })
                    .error(function () {
                        alert("get actions error");
                    });
            }
        }
    });

    kendo.bind($("form"), viewModel);
    
    var isAfterError = $("#is-after-error").attr("data-is-after-error").toLowerCase() === "true";
    if (isAfterError) {
        ShowInvisiblePart();
    }

    TypeMarksModule.check();

    //Костыль! Предназначен для того, чтобы невалидный kendo numerictextbox не скрывался при клиентской валидации
    $('form').bind('submit', function () {
        $('.k-numerictextbox').show();
    });

    //Собственное правило - необходимость модели для типов БПА и МКТК
    $.validator.addMethod("MarkIsRequered", function (value, element) {
        return $("#Mark").is("[disabled]") || $("#Mark").val() !== "";
    }, "Для этого типа терминала необходимо выбрать модель");
    $("#Mark").addClass("MarkIsRequered");

    /*var validator = $("form").data("validator");
    if (validator) {
        validator.settings.onkeyup = false; // disable validation on keyup
        validator.settings.onfocusout = false;  //disable validation onblur
    }            */
});

var ShowInvisiblePart = function () {
    //Скрываем кнопку Далее
    $("#next-button").hide();
    //Показываем кнопку Сохранить
    $(":submit").show();
    //Делаем номер терминала недоступным для редактирования
    $("#Number").attr("readonly", "readonly");
    //Делаем дату ввода терминала недоступной для редактирования
    $("#BeginTime").data("kendoDatePicker").readonly(true);
    //Делаем дату вывода терминала недоступной для редактировани
    $("#EndTime").data("kendoDatePicker").readonly(true);
    //Открываем невидимую часть формы
    $(".invisible-table tr:hidden").each(function () {
        $(this).show();
    });
};

var TypeMarksModule = (function () {
    //Функция обновления списка моделей в зависимости от типа терминала
    var check = function () {
        var typeMarksUrl = $('#terminal-type-marks-url').attr('data-url');
        var currentType = parseInt($('#Type').val());
        if (isNaN(currentType)) {
            return;
        }

        var selectSelector = '#Mark';

        //Заправшиваем список моделей для типа
        $.post(typeMarksUrl, { currentType: currentType }, function (result) {
            if (result.status === JsonStatus.ok) {
                //Очищаем список моделей
                $(selectSelector + ' option').remove();

                //Если у типа есть модели
                if (result.items.length > 0) {
                    $(selectSelector).removeAttr("disabled");
                    $(selectSelector).append(new Option("--Выберите--", ""));

                    //Формируем новый список моделей
                    $.each(result.items, function () {
                        $(selectSelector).append(new Option(this.Text, this.Value));
                    });

                    //Если тип терминала поменялся в результате валидации номера терминала
                    var isAnalyzeCurrentModel = $("#current-model").attr("data-is-actual").toLowerCase() === "true";
                    if (isAnalyzeCurrentModel) {
                        //Устанавливаем модель в соответствии с сохраннённым в разметке значением
                        var currentModel = $("#current-model").attr("data-model");
                        $("#Mark").val(currentModel);
                        $("#current-model").attr("data-is-actual", false);

                        //В случае перепривязки терминала блокируем возможность изменения типа и модели
                        $('#Type option:not(:selected)').attr('disabled', true);
                        $('#Mark option:not(:selected)').attr('disabled', true);
                    }
                } else {
                    //Если у типа нет моделей, делаем список моделей неактивным
                    $(selectSelector).attr("disabled", "disabled");
                    $("#Mark").valid();
                }
            }

            if (result.status === JsonStatus.error) {
                alert(result.message);
            }
        })
            .error(function () {
                alert("get actions error");
            });
    };

    return {
        check: check
    };
}($));