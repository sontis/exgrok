﻿
$(function () {
    $("form").on("submit", function (e) {
        var newProfileValue = $("#Profile option:selected").val();
        var oldProfileValue = $("#OldProfile").val();
        var terminalNumbers = $("#SetTerminalsListString").val();
        
        //Если в окне есть или будут терминалы и значение профиля изменилось и стало "на вход" или "на выход" -
        //предупреждаем об этом пользователя
        if (newProfileValue !== "" &&
            newProfileValue !== oldProfileValue &&
            terminalNumbers !== "") {
            if (!confirm("Данное кассовое окно имеет активные привязки к терминалам под следующими номерами: " + terminalNumbers + ".\n" +
                "Поэтому привязки к терминалам с неподходящим профилем будут закрыты. Продолжить? ")) {
                e.preventDefault();
            }
        }
    });
});
