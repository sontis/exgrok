﻿$(document).ready(function () {
    kendo.culture("ru-RU");

    var scheduleTemplateGridModule = ScheduleTemplateGridModule();
    $("#ScheduleTemlateId").on("change", scheduleTemplateGridModule.updateGrid);
});


//Функция, используемая kendo-templat-ом для вывода данных о
//рабочих дневных и ночных дней календаря
function buildCell(i, days, nights, holidays, monthNumber, dayTypes, areSplitted) {
    if (days[i] === undefined || days[i] === null) {
        if (monthNumber === -1 && i !== 0) {
            return "<table name=\"without-border-table\"></table>";
        } else {
            return "";
        }
    }

    var text = "<table>";
    var holidaysClassName = holidays[i] ? " holiday-cell\"" : "\"";
    if (days[i] === "") {
        days[i] = "&nbsp;";
    }
    if (nights[i] === "") {
        nights[i] = "&nbsp;";
    }
    if (dayTypes[i] === "") {
        dayTypes[i] = "&nbsp;";
    }
    if (areSplitted[i] === "") {
        areSplitted[i] = "&nbsp;";
    }

    text += "<tr><td class=\"pseudo-excel-cell" +
        holidaysClassName +
        ">" +
        dayTypes[i] +
        "</td></tr>" +
        "<tr><td class=\"pseudo-excel-cell" +
        holidaysClassName +
        ">" +
        days[i] +
        "</td></tr>" +
        "<tr><td  class=\"pseudo-excel-cell" +
        holidaysClassName +
        ">" +
        nights[i] +
        "</td></tr>" +
        "<tr><td  class=\"pseudo-excel-cell" +
        holidaysClassName +
        ">" +
        areSplitted[i] +
        "</td></tr>";

    text += "</table>";

    return text;
}

var ScheduleTemplateGridModule = function () {
    var url = $("#get-template-details-url").attr("data-url");

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "POST"
            },
            parameterMap: function () {
                return {
                    scheduleTemplateId: $("#ScheduleTemlateId").val(),
                    month: $("#Month").val()
                };
            }
        },
        schema: {
            model: {
                fields: {                    
                    Days: [],
                    Nights: []                    
                }
            },
            data: "Items"
        },        
        error: function () {
            if (!window.unloading) {
                alert('При получении данных возникла ошибка');
            }
        }
    });
    
    var buildColumns = function () {
        var columns = [];               

        for (var i = 0; i < 31; i++) {
            columns.push({
                title: (i + 1).toString(),
                width: "37px",
                template: "#= buildCell(" + i + ", Days, Nights, Holidays, MonthNumber, DayTypes, AreSplitted) #"                
            });
        }        
        
        return columns;
    };
    
    $("#schedule-template-detailes-grid").kendoGrid({        
        dataSource: dataSource,
        columns: buildColumns(),
        resizable: true,
        dataBound: function(e) {
            var data = this.dataSource.data();
            
            if (data.length > 0) {
                $("tr.k-footer-template td").html("<span style=\"margin-right: 10px;\">" +
                                                  "Рабочих дней: " +
                                                  data[0].WorkDaysNum +
                                                  "</span>" + 
                                                  "<span style=\"margin-right: 10px;\">" +
                                                  "Рабочих часов: " +
                                                  data[0].HoursNum +
                                                  "</span>" +
                                                  "<span style=\"margin-right: 10px;\">" +
                                                  "( ночных: " +
                                                  data[0].NightHoursNum +
                                                  ", праздничных: " +
                                                  data[0].HolidaysHoursNum +
                                                  ")" +
                                                  "</span>");
            }
        }
    });

    //Обработчик смены шаблона в списке
    var updateGrid = function (e) {
        var grid = $("#schedule-template-detailes-grid").data("kendoGrid");        

        if (grid) {
            grid.dataSource.read();            
        }
    };
    
    $("#schedule-template-detailes-grid").append("<div class=\"k-grid-footer\"> " +
                                                 "<div class=\"k-grid-footer-wrap\">" +
                                                 "<table>" +
                                                 "<tbody>" +
                                                 "<tr class=\"k-footer-template\">" +
                                                 "<td>" +
                                                 "</td>" +
                                                 "</tr>" +
                                                 "</tbody>" +
                                                 "</table>" +
                                                 "</div>" +
                                                 "</div>");

    return {
        updateGrid: updateGrid
    };
};