﻿var VacationsGridModule = (function() {
    var dataSource;
        
    var init = function() {
        dataSource = new kendo.data.DataSource({
            sort: { field: "BeginDate", dir: "asc" },
            transport: {
                read: function (options) {
                    $.ajax({
                        url: "Vacations/GetEmployeeVacations/",
                        type: "POST",
                        data: { employeeId: employeeId },
                        success: function (result) {
                            options.success(result);
                        }
                    });
                },
                destroy: function (options) {
                    $.ajax({
                        url: "Vacations/DeleteVacation/",
                        type: "POST",
                        data: options.data,
                        success: function (result) {
                            options.success(result);
                        }
                    });
                },
                create: function (options) {
                    options.data.employeeId = employeeId;

                    $.ajax({
                        url: "Vacations/CreateVacation/",
                        type: "POST",
                        data: options.data,
                        success: function (result) {
                            options.success(result);
                        }
                    });
                }
            },

            schema: {
                model: {
                    id: "VacationPeriodId",
                    fields: {
                        BeginDate: {
                            validation: {
                                required: true
                            },
                            type: "date",
                            format: "{0:dd.MM.yyyy}"
                        },
                        EndDate: {
                            validation: {
                                required: true
                            },
                            format: "{0:dd.MM.yyyy}"
                        },
                        Comment: {
                        }
                    }
                }
            },
            batch: false
        });

        $("#csVacationsGrid").kendoGrid({
            dataSource: dataSource,
            sortable: {
                mode: "single",
                allowUnsort: false
            },
            columns: [
                {
                    field: "BeginDate",
                    title: "Начало",
                    type: "date",
                    format: "{0:dd.MM.yyyy}",
                    editor: function (container, options) {
                        $('<input name="' + options.field + '"/>').appendTo(container);
                    }
                },
                {
                    field: "EndDate",
                    title: "Конец",
                    type: "date",
                    editor: function (container, options) {
                        $('<input name="' + options.field + '"/>').appendTo(container);
                    },
                    sortable: false
                },
                {
                    field: "Comment",
                    title: "Примечание",
                    sortable: false
                },
                {
                    command: { name: "destroy", text: "Удалить" },
                    width: "110px",
                    sortable: false
                }],
            editable:
            {
                mode: "popup",
                createAt: "bottom",
                confirmation: "Вы уверены что хотите удалить элемент?"
            }
        });
    };

    return {
        init: init,
        dataSource: dataSource
    };
});

var StationPriorityGridModule = (function() {
    var dataSource;

    var init = function() {
        dataSource = new kendo.data.DataSource({
            type: "json",
            autoSync: true,
            transport: {
                read: {
                    url: $("#stationPriorityUrl").attr("data-url"),
                    dataType: "json",
                    type: "POST",
                    data: function() {
                        return {
                            //Идентификатор сотрудника
                            employeeId: employeeId
                        };
                    }
                },            
                update: {
                    url: $("#update-employee-station-priority-url").attr("data-url"),
                    type: "POST",
                    data: function (options) {                    
                        return options;
                    }
                }
            },
            schema: {
                model: {
                    id: "Id",
                    fields: {
                        StationName: { editable: false },
                        Priority: { editable: false },
                        TransferTime: {}
                    }
                },
                data: "Items",
                total: "Count"
            },
            error: function() {
                if (!window.unloading) {
                    alert('При получении данных возникла ошибка');
                }
            },
            requestEnd: function (e) {
                if (e.response.type === "read") {
                    if (e.response.Count > 0) {
                        $("#csStationPriorityGrid").show();
                        $("#no-prioritets-span-container").hide();
                    } else {
                        $("#csStationPriorityGrid").hide();
                        $("#no-prioritets-span-container").show();
                    }
                } else if (e.response.type === "update") {
                    if (e.response.status !== undefined &&
                        e.response.status === JsonStatus.error) {
                        alert(e.response.message);
                    }
                }
            }
        });
        
        //Таблица приоритетов
        var masterRowCount = 0;
        var detailRowCount = 0;
        $("#csStationPriorityGrid").kendoGrid({
            dataSource: dataSource,
            selectable: true,
            editable: true,
            columns: [
                { field: "StationName", title: "Наименование станции" },
                { field: "Priority", title: "Приоритет" },
                { field: "TransferTime", title: "Время в пути" },
                { template: kendo.template($('#editColumn').html()) }],
            dataBound: function () {
                var stationPriorityViewModel = ViewModelsModule().stationPriorityViewModel;
                kendo.bind($("#csStationPriorityGrid input"), stationPriorityViewModel);

                var grid = $('#csStationPriorityGrid').data('kendoGrid');
                var allMasterRows = grid.tbody.find('>tr.k-master-row');
                masterRowCount = allMasterRows.length;

                var currentSelectedRowIndex = parseInt($("#currentSelectedRowId").attr("data-row-id"));

                for (var i = 0; i < masterRowCount; i++) {
                    var currentRow = allMasterRows.eq(i);

                    //grid.expandRow(currentRow);

                    var currentId = grid.dataItem(currentRow).Id;

                    //Выделяем станцию, у которой до обновления таблицы изменился приоритет            
                    if (currentId === currentSelectedRowIndex) {
                        grid.select(currentRow);
                    }

                    //Кнопка 'Вверх' для станции с самым высоким приоритетом должна быть недоступна
                    if (i === 0) {
                        var upButton = $(currentRow).find(".button_up");
                        upButton.attr("disabled", "disabled");
                        upButton.addClass("k-state-disabled");
                    }

                    //Кнопка 'Вниз' для станции с самым низким приоритетом должна быть недоступна
                    if (i === masterRowCount - 1) {
                        var downButton = $(currentRow).find(".button_down");
                        downButton.attr("disabled", "disabled");
                        downButton.addClass("k-state-disabled");
                    }
                }

                //Установка вертикального скроллбара на 300 пискселей выше выделенной строки
                $("body").scrollTo("#csStationPriorityGrid tr.k-state-selected", 0, { offset: -300 });
            },
            detailInit: function (e) {
                $("<div/>").appendTo(e.detailCell).kendoGrid({
                    dataSource: {
                        type: "json",
                        transport: {
                            read: {
                                url: $("#timeIntervalsForStationPriorityUrl").attr("data-url"),
                                dataType: "json",
                                type: "POST"
                            },
                            parameterMap: function () {
                                return {
                                    //Идентификатор приоритета станции
                                    stationPriorityId: e.data.Id
                                };
                            }
                        },
                        schema: {
                            model: {
                                id: "Id",
                                fields: {
                                    DayName: {},
                                    WorkTime: {}
                                }
                            },
                            data: "Items",
                            total: "Count"
                        },
                        error: function () {
                            if (!window.unloading) {
                                alert('При получении данных возникла ошибка');
                            }
                        }
                    },
                    columns: [
                        { field: "DayName", title: "Рабочие дни" },
                        { field: "WorkTime", title: "Время работы" }
                    ],
                    dataBound: function (e) {
                        //Была идея раскрывать все строки master-таблицы, но решено было отказаться от этого
                        //в силу большого количество мелких запросов и долгого времени полной загрузки таблицы
                        /*detailRowCount = detailRowCount + 1;                    
                        if (detailRowCount === masterRowCount) {
                            var selectedRow = $("#csStationPriorityGrid").data("kendoGrid").select();
                            if (selectedRow.length > 0) {                            
                                $("body").scrollTo("#csStationPriorityGrid tr.k-state-selected", 0, { offset: -300 });
                            }
    
                            masterRowCount = 0;
                            detailRowCount = 0;
                        }     */
                    }
                });
            }
        });
    };

    return {
        init: init  
    };
});

var CustomGridCellEditorsModule = (function () {
    var setTransferTimeEditorsForPriorityGrid = function () {
        //Устанавливаем собственные эдиторы для ячеек колонки с временем пути таблицы приоритетов,
        //используя kendo numeric textbox. Делается это для обеспечения ввода только целых чисел
        var columns = $("#csStationPriorityGrid").data("kendoGrid").columns;
        $.each(columns, function () {
            if (this.field === "TransferTime") {
                this.editor = function (container, options) {
                    $('<input type="text" required="required"' +
                        'data-required-msg="Значение не может быть неопределённым"' +
                        'data-type="number"' +
                        ' name="' + options.field + '"/>')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            min: 0,
                            max: 255,
                            format: "n0",
                            decimals: 0,
                            step: 1
                        });
                };
            }
        });        
    };
    
    var setValidatorsForTransferTimeEditors = function () {
        //Обработчик на событие потери фокуса в ячейки таблицы планов выручки
        $("#csStationPriorityGrid").on("blur", "input[required=required]", function (e) {
            var input = $(this).siblings("input");
            var message = $(this).attr("data-required-msg");

            if (this.value && input && message) {
                input.removeClass("grok-validation-error");
                if (input.data("kendoTooltip") !== undefined) {
                    input.data("kendoTooltip").destroy();
                }
            } else {
                input.addClass("grok-validation-error");
                input.kendoTooltip({
                    content: message,
                    width: 150,
                    position: "top"
                });
            }
        });
    };

    var init = function () {
        if ($("#csStationPriorityGrid").length > 0) {
            setTransferTimeEditorsForPriorityGrid();
            setValidatorsForTransferTimeEditors();
        }
    };

    return {
        init: init
    };
});

var ViewModelsModule = (function () {
    //Модуль назначения приоритетов
    var stationPriorityViewModel = kendo.observable({
        windowPriorityViewModel: kendo.observable({
            //Обработчик ежедневного назначения времени во всплывающем окне
            //Timepicker-ы в fieldset-ах 'Отрезки недели' и 'Опционально' становятся неактивными
            dailyChanged: function () {
                $("#daily-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(true);
                });
                $("#week-parts-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(false);
                });
                $("#optionally-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(false);
                });

                $("#priority-edit-popup span.error-message").text("");
            },
            //Обработчик назначения времени по отрезкам недели во всплывающем окне
            //Timepicker-ы в fieldset-ах 'Ежедневно' и 'Опционально' становятся неактивными
            weekPartsChanged: function () {
                $("#daily-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(false);
                });
                $("#week-parts-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(true);
                });
                $("#optionally-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(false);
                });

                $("#priority-edit-popup span.error-message").text("");
            },
            //Обработчик опционального назначения времени во всплывающем окне
            //Timepicker-ы в fieldset-ах 'Ежедневно' и 'Отрезки недели' становятся неактивными
            optionallyChanged: function () {
                $("#daily-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(false);
                });
                $("#week-parts-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(false);
                });
                $("#optionally-panel input[data-role=timepicker]").each(function () {
                    $(this).data("kendoTimePicker").enable(true);
                });

                $("#priority-edit-popup span.error-message").text("");
            },
            //Обработчик нажатия на кнопку 'Сохранить' во всплывающем окне
            saveButtonHandler: function () {
                //Если в выбранном fieldset-е нет timepicker-ов с некорректными значениями, то продолжаем проверку
                if ($("#priority-edit-popup .grok-validation-error:enabled").length === 0) {
                    var isAllInputsEmpty = $('.priority-time-period-panel:not(:has(input:disabled)) :text').filter(function () {
                        return $(this).val() !== "";
                    }).length === 0;

                    //Если ничего не введено, то на этом всё заканчивается
                    if (isAllInputsEmpty) {
                        $("#all-empty-error-message").remove();
                        $('<span id="all-empty-error-message" class="error-message">' +
                          'Необходимо ввести хотя бы один интервал</span>')
                            .insertBefore('#daily-panel');
                    } else {
                        $("#all-empty-error-message").remove();
                        var errorCount = 0;

                        //Проверяем временные интервалы на активном fieldset-е
                        $(".priority-time-period-panel:not(:has(input:disabled)) .priority-time-interval").each(function () {
                            var lengthOfFilledInputs = $(this).find(':text').filter(function () {
                                return $(this).val() !== "";
                            }).length;

                            //Проверка на ввод обеих границ интервала 
                            if (lengthOfFilledInputs === 1) {
                                $(this).find(".error-message").text('Необходимо ввести обе границы интервала');
                                errorCount = errorCount + 1;
                            } else {
                                $(this).find(".error-message").text('');
                            }

                            //Проверка того, что нижняя граница интервала меньше верхней
                            if (lengthOfFilledInputs === 2) {
                                var lowerValue = $(this).find('input.lower-limit').data("kendoTimePicker").value();
                                var upperValue = $(this).find('input.upper-limit').data("kendoTimePicker").value();

                                if (lowerValue >= upperValue) {
                                    $(this).find(".error-message").text('Нижняя граница интервала должна быть меньше верхней');
                                    errorCount = errorCount + 1;
                                } else {
                                    $(this).find(".error-message").text('');
                                }
                            }
                        });

                        //Ошибок нет. Отправляем данные на форму
                        if (errorCount === 0) {
                            var windowContent = $('.priority-time-period-panel:not(:has(input:disabled)) :text, #priority-edit-popup :hidden').serialize();
                            var deserializedWindowContent = decodeURIComponent(windowContent);

                            var updatePriorityIntervalsUrl = $('#updatePriorityIntervalsUrl').attr("data-url");

                            $.ajax({
                                type: 'POST',
                                url: updatePriorityIntervalsUrl,
                                data: deserializedWindowContent,
                                dataType: 'json',
                                beforeSend: function () {
                                    //Показываем ajax-кругляшок на время выполнения запроса
                                    $("#priority-edit-popup")
                                        .mask("Загрузка...",
                                                parseInt($("#DelayInterval").attr('data-delay'), 10));
                                },
                                success: function (result) {
                                    if (result.status === JsonStatus.ok) {
                                        var selectedRow = $("#csStationPriorityGrid").data("kendoGrid").select();
                                        var gridData = $("#csStationPriorityGrid").data("kendoGrid");
                                        var dataItem = gridData.dataItem(selectedRow);

                                        //Скрываем ajax-кругляшок
                                        $("#priority-edit-popup").unmask();
                                        //Закрываем окно
                                        $("#priority-edit-popup").data("kendoWindow").close();

                                        //Добавление иконки удаления интервалов приоритета
                                        dataItem.HasIntervals = true;

                                        if (selectedRow.find(".button_remove_intervals").length === 0) {
                                            selectedRow.find("td:has(:button)").append("<input type=\"button\" class=\"button_remove_intervals\" value=\"\" title=\"Удалить интервалы\" " +
                                                "alt=\"Удалить интервалы\" name=\"station-priority-button\" id=\"priority-delete-intervals-\"" + dataItem.Id + "\" " +
                                                "data-bind=\"events: { click: eraseTimeIntervalsForPriority }\"> ");
                                            //Установка обработчика иконки удаления приоритета интервала
                                            selectedRow.find(".button_remove_intervals").on("click", stationPriorityViewModel.eraseTimeIntervalsForPriority);
                                        }

                                        //Открываем в таблице строчку с приоритетом, для которого
                                        //проводилось редактирование
                                        $("#csStationPriorityGrid").data("kendoGrid").expandRow(selectedRow);
                                        //Обновлям вложенную таблицу
                                        var childGrid = $(selectedRow).next(".k-detail-row").find(".k-grid").data("kendoGrid");
                                        childGrid.dataSource.read();
                                    }

                                    if (result.status === JsonStatus.invalid) {
                                        alert('Ошибка привязки модели: ' + result.message);
                                    }

                                    if (result.status === JsonStatus.error) {
                                        alert(result.message);
                                    }
                                    Utils.Notification.show('Данные приоритетов успешно сохранены');
                                },
                                error: function (xhr, status) {
                                    if (!window.unloading) {
                                        alert('Ошибка при выполнении запроса: ' +
                                            status +
                                            '\nКод ответа: ' +
                                            xhr.status);
                                    }
                                },
                                complete: function () {
                                    $("#priority-edit-popup")
                                        .unmask();
                                }
                            });

                        }
                    }
                }
            },
            //Функция, инициализирующая максимальное значение 
            //у timepicker-ов верхней границы временных интервалов
            //приоритетов станций (чтобы 0:00 отображалось в конце списка)
            assignMaxToValue: function (target) {
                if ($(target).hasClass("upper-limit")) {
                    var value = $(target).val();

                    if (value !== null && value !== undefined) {
                        var values = value.split(':');

                        if (values.length === 2) {
                            var hours = parseInt(values[0]);
                            var minutes = parseInt(values[1]);

                            if (hours === 0 && minutes === 0) {
                                var max = $(target).data("kendoTimePicker").max();
                                $(target).data("kendoTimePicker").value(max);
                            }
                        }
                    }
                }
            },
            //Обработчик потери фокуса для timepicker-ов во всплывающем окне
            timePickerBlur: function (e) {
                var target = e.target;
                stationPriorityViewModel.windowPriorityViewModel.assignMaxToValue(target);

                //Проверяем с помощью регулярного выражения введённое время            
                var targetValue = $(target).val();
                if (!targetValue.match(/^([01]?\d|2[0-3]):?([0-5]\d)$/) && targetValue !== "") {
                    $(target).addClass("grok-validation-error");
                    $(target).kendoTooltip({
                        content: "Недопустимое время. Необходимый формат: hh:mm",
                        width: 150,
                        position: "top"
                    });
                } else {
                    $(target).removeClass("grok-validation-error");
                    if ($(target).data("kendoTooltip") !== undefined) {
                        $(target).data("kendoTooltip").destroy();
                    }
                }
            },
            //Функция, прибавляющая сутки для значений 
            //timepicker-ов верхней границы временных интервалов
            //приоритетов станций в случае установки их значения 0:00
            setMaxvalueForUpperLimits: function () {
                $("#priority-edit-popup input.upper-limit").each(function () {
                    var currentDateTime = new Date();
                    var currentDate = new Date(currentDateTime.getFullYear(), currentDateTime.getMonth(), currentDateTime.getDate());

                    var newMax = new Date(currentDate.getTime() + 24 * 60 * 60 * 1000);
                    var newMin = new Date(currentDate.getTime() + 30 * 60 * 1000);
                    $(this).data("kendoTimePicker").max(newMax);
                    $(this).data("kendoTimePicker").min(newMin);
                });
            }
        }),
        //Обработчик нажатия на стрелочку "Вверх" - повышение приоритета станции
        stationPriorityUp: function (e) {
            var selectedRow = $("#csStationPriorityGrid").data("kendoGrid").select();
            var selectedRowDataId = $("#csStationPriorityGrid").data("kendoGrid").dataItem(selectedRow).id;
            var index = selectedRow.index();
            var gridData = $("#csStationPriorityGrid").data("kendoGrid");
            var dataItem = gridData.dataItem(selectedRow);
            var pickUpPriorityUrl = $('#pickUpStationPriorityUrl').attr("data-url");

            //Запрос на сервер на повышение приоритета
            $.post(pickUpPriorityUrl, { employeeId: dataItem.EmployeeId, priorityLevel: dataItem.Priority }, function (result) {
                if (result.status === JsonStatus.ok) {
                    //В случае успеха таблица перезагружается
                    gridData.dataSource.read();
                    $("#currentSelectedRowId").attr("data-row-id", selectedRowDataId.toString());
                }

                if (result.status === JsonStatus.error) {
                    alert(result.message);
                }
            })
                            .error(function () {
                                alert("get actions error");
                            });
        },
        //Обработчик нажатия на стрелочку "Вниз" - понижение приоритета станции
        stationPriorityDown: function (e) {
            var selectedRow = $("#csStationPriorityGrid").data("kendoGrid").select();
            var selectedRowDataId = $("#csStationPriorityGrid").data("kendoGrid").dataItem(selectedRow).id;
            var index = selectedRow.index();
            var gridData = $("#csStationPriorityGrid").data("kendoGrid");
            var dataItem = gridData.dataItem(selectedRow);
            var pickDownPriorityUrl = $('#pickDownStationPriorityUrl').attr("data-url");

            //Запрос на сервер на понижение приоритета
            $.post(pickDownPriorityUrl, { employeeId: dataItem.EmployeeId, priorityLevel: dataItem.Priority }, function (result) {
                if (result.status === JsonStatus.ok) {
                    //В случае успеха таблица перезагружается
                    gridData.dataSource.read();
                    $("#currentSelectedRowId").attr("data-row-id", selectedRowDataId.toString());
                }

                if (result.status === JsonStatus.error) {
                    alert(result.message);
                }
            })
                            .error(function () {
                                alert("get actions error");
                            });
        },
        //Обработчик нажатия на кнопку "Удалить интервалы"
        eraseTimeIntervalsForPriority: function (e) {
            var selectedRow = $("#csStationPriorityGrid").data("kendoGrid").select();
            var gridData = $("#csStationPriorityGrid").data("kendoGrid");
            var dataItem = gridData.dataItem(selectedRow);
            var deleteIntervalsForPriorityUrl = $("#deleteIntervalsForPriorityUrl").attr("data-url");

            if (confirm('Вы действительно хотите удалить все временные интервалы для станции ' + dataItem.StationName + '?')) {
                $.post(deleteIntervalsForPriorityUrl, { stationPriorityId: dataItem.Id }, function (result) {
                    if (result.status === JsonStatus.ok) {
                        //Убираем иконку удаления интервалов и обновляем соответствующие метаданные
                        dataItem.HasIntervals = false;
                        selectedRow.find(".button_remove_intervals").remove();

                        $("#csStationPriorityGrid").data("kendoGrid").expandRow(selectedRow);
                        //Обновлям вложенную таблицу
                        var childGrid = $(selectedRow).next(".k-detail-row").find(".k-grid").data("kendoGrid");
                        childGrid.dataSource.read();
                        Utils.Notification.show('Временные интервалы для станции ' + dataItem.StationName + ' удалены');
                    }

                    if (result.status === JsonStatus.error) {
                        alert(result.message);
                    }
                })
                    .error(function () {
                        alert("get actions error");
                    });
            }
        },
        //Обработчик нажатия на кнопку "Редактировать" - редактирование временных интервалов приоритета
        editPriority: function (e) {
            var id = parseInt(e.target.id.split('-')[1]);
            $("#StationPriorityID").val(id);
            var stationName = $(e.target).attr('stationName');

            var windowSelector = $("#priority-edit-popup");
            var stationPriorityViewModelObject = this;

            //Выводим всплывающее окно
            if (!$(windowSelector).data("kendoWindow")) {
                $(windowSelector).show();
                $(windowSelector).kendoWindow({
                    width: "900px",
                    height: "600px",
                    title: stationName,
                    modal: true,
                    activate: function () {
                        stationPriorityViewModelObject.windowPriorityViewModel.setMaxvalueForUpperLimits();

                        var getIntervalsUrl = $("#timeIntervalsForWindowUrl").attr("data-url");
                        var stationPriorityId = $("#StationPriorityID").val();

                        //Посылаем запрос серверу для установки уже введённых временных интервалов
                        //во всплывающем окне
                        $.post(getIntervalsUrl, { stationPriorityId: stationPriorityId }, function (response) {
                            if (response.body !== null && response.body !== undefined) {
                                $("#optionally-panel :radio").prop('checked', true);
                                stationPriorityViewModelObject.windowPriorityViewModel.optionallyChanged();

                                var params = response.body.split('&');
                                //Если временные интервалы для данного приоритета станции уже есть
                                if (params.length > 0) {
                                    params.forEach(function (entry) {
                                        var entries = entry.split('=');
                                        if (entries.length === 2) {
                                            var entrySelector = "[name=" + entries[0] + "]";
                                            $(entrySelector).data("kendoTimePicker").value(entries[1]);
                                            stationPriorityViewModel.windowPriorityViewModel.assignMaxToValue(entrySelector);
                                        }
                                    });
                                }
                            }
                        });
                    },
                    close: function () {
                        //Очищаем все time-picker-ы и делаем их фон белым
                        $("#priority-edit-popup :text[data-role=timepicker]").each(function () {
                            $(this).val("");
                            $(this).removeClass("grok-validation-error");
                        });

                        //Переводим radio-buttons,  
                        //устанавливаем доступность/недоступность timepicker-ов в исходное состояние 
                        //и убираем сообщения об ошибках
                        $('#daily-panel :radio').prop('checked', true);
                        stationPriorityViewModelObject.windowPriorityViewModel.dailyChanged();
                    }
                });
            } else {
                var window = $(windowSelector).data("kendoWindow")
                $('#priority-edit-popup_wnd_title').text(stationName);
                window.options.title = stationName;
                window.open();
            }

            //Устанавливаем обработчик события blur (потеря фокуса) для timepicker-ов
            //Поскольку событие не кендошное, то и обработчик устанавливается не с помощью kendo mvvm data-bind
            $("#priority-edit-popup").delegate("input:text", "blur", this.windowPriorityViewModel.timePickerBlur);
        },
        //Обработчик нажатия на кнопку "Удалить" - удаление приоритета
        deletePriority: function (e) {            
            var id = parseInt(e.target.id.split('-')[2]);
            var url = $("#delete-employee-station-priority-url").attr("data-url");
            var stationName = $(e.target).attr('stationName');

            if (confirm('Вы действительно хотите удалить приоритет для станции ' + stationName)) {
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: 'json',
                    data: { employeePriorityId: id },
                    beforeSend: function () {
                        //Показываем ajax-кругляшок на время выполнения запроса
                        $(".priority-tab")
                            .mask("Идёт удаление приоритета станции...",
                                parseInt($("#DelayInterval").attr("data-delay")));
                    },
                    success: function (result) {
                        if (result.status === JsonStatus.ok) {
                            var grid = $("#csStationPriorityGrid").data("kendoGrid");
                            if (grid) {
                                grid.clearSelection();
                                grid.dataSource.read();
                                Utils.Notification.show('Приоритет для станции "' + stationName + '" удален');
                            }
                        }

                        if (result.status === JsonStatus.error) {
                            alert(result.message);
                        }
                    },
                    error: function () {
                        alert("get actions error");
                    },
                    complete: function () {
                        $(".priority-tab")
                            .unmask();
                    }
                });                
            }
        },
        //Обработчик нажатия на кнопку "Создать приоритет"
        createPriority: function (e) {
            $("#priority-create-popup").data("kendoWindow").center();
            $("#priority-create-popup").data("kendoWindow").open();
        },
        //Обработчик нажатия на кнопку "Сохранить" на всплывающем окне создагния приоритета
        addPriority: function (e) {
            var employeeId = $("#employee-id").attr("data-id");
            var stationId = $("#stations-for-priority").data("kendoDropDownList").value();
            var stationName = $("#stations-for-priority").data("kendoDropDownList").text();
            var transferTime = $("#priority-transfer-time").data("kendoNumericTextBox").value();
            var url = $("#create-employee-station-priority-url").attr("data-url");
            var data = JSON.stringify({
                EmployeeId: employeeId,
                StationId: stationId,
                TransferTime: transferTime
            });

            $.ajax({
                url: url,
                data: data,
                type: "POST",
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    if (result.status === JsonStatus.ok) {
                        Utils.Notification.show("Приоритет для станции \"" + stationName + "\" создан");
                        $("#priority-create-popup").data("kendoWindow").close();
                        $("#csStationPriorityGrid").data("kendoGrid").dataSource.read();
                    }

                    if (result.status === JsonStatus.error) {
                        alert(result.message);
                    }
                },
                error: function () {
                    alert("get actions error");
                }
            });
        },
        //Обработчик нажатия на кнопку "Отмена" на всплывающем окне создания приоритета
        addPriorityCancel: function (e) {
            $("#priority-create-popup").data("kendoWindow").close();
        }        
    });

    var vacationViewModel = kendo.observable({
        items: $("#csVacationsGrid").data("kendoGrid").dataSource,
        beginDate: '',
        endDate: '',
        comment: '',
        validation: {
            status: JsonStatus.ok,
            message: ''
        },
        add: function () {
            var ds = this.get("items");
            var beginDateVal = this.get('beginDate');
            var endDateVal = this.get('endDate');

            if ((beginDate != '') && (endDate != '')) {
                var beginDate = new Date(beginDateVal);
                var endDate = new Date(endDateVal);

                if ((beginDate != 'Invalid Date') && (beginDate.getTime() > 0) && (endDate != 'Invalid Date') && (endDate.getTime() > 0)) {
                    if (beginDate <= endDate) {
                        //Сложная (историческая) валидация с применением сервера. Последняя линия обороны против некорректных данных
                        this.validate();

                        if (this.validation.status === JsonStatus.invalid) {
                            alert(this.validation.message);
                            return;
                        }

                        ds.add({ BeginDate: kendo.toString(beginDate, "dd.MM.yyyy"), EndDate: kendo.toString(endDate, "dd.MM.yyyy"), Comment: this.get('comment') });
                        ds.sync();
                        this.set('beginDate', '');
                        this.set('endDate', '');
                        this.set('comment', '');
                        ds.sort({ field: "BeginDate", dir: "asc" });
                    }
                    else {
                        alert('Начало периода не может быть больше чем конец периода');
                    }
                }
                else {
                    alert('Неверный формат даты. Проверьте правильность заполнения полей');
                }
            }
            else {
                alert('Начало и конец периода должны быть заполнены');
            }
        },
        //Функция исторической валидации отпусков
        validate: function () {            
            //Модель отпуска
            var model = {
                BeginDate: kendo.toString(this.beginDate, "dd.MM.yyyy"),
                EndDate: kendo.toString(this.endDate, "dd.MM.yyyy"),
                Comment: this.comment,
                EmployeeId: employeeId,
                CashierContractBeginDate: $("#ContractEffectiveFrom").val(),
                CashierContractEndDate: $("#ContractEffectiveTo").val()
            };

            var that = this;

            //Запрос исторической валидации
            $.ajax({
                async: false,
                url: "Vacations/ValidateVacation",
                type: "POST",
                data: model,
                success: function (result) {
                    //Если интервал отпуска валиден
                    if (result.status === JsonStatus.ok) {
                        that.validation.status = JsonStatus.ok;
                        return;
                    }

                    that.validation.status = JsonStatus.invalid;

                    //Если интервал отпуска некорректен
                    if (result.status === JsonStatus.invalid) {
                        that.validation.message = result.message;
                        return;
                    }

                    that.validation.message = "Ошибка валидации отпуска";
                },
                error: function () {
                    if (!window.unloading) {
                        that.validation.status = JsonStatus.invalid;
                        that.validation.message = "Ошибка валидации отпуска";
                    }
                }
            });
        }
    });

    return {
        stationPriorityViewModel: stationPriorityViewModel,
        vacationViewModel: vacationViewModel
    };
});

var PageInterfaceModule = (function() {
    var saveCurrentTabState = function() {
        var selectedItem = $("#tabStrip").data("kendoTabStrip").select();
        var currentIndex = selectedItem.index();
        $.session.set('TabStripCurrentIndex-' + window.location.pathname, currentIndex);
    };

    var setCurrentTab = function() {
        var currentQueryString = window.location.pathname;
        var tabStripCurrentIndex = $.session.get('TabStripCurrentIndex-' + currentQueryString);

        if (tabStripCurrentIndex !== null && tabStripCurrentIndex !== undefined) {
            $("#tabStrip").data("kendoTabStrip").select(tabStripCurrentIndex);
        }
    };

    var initHandlers = function() {
        $("#SectorId").data("kendoDropDownList").bind("change", function (e) {
            //При изменении участка нужно перегрузить список станций
            var url = $("#sector-stations-url").attr("data-url");
            var sectorId = $("#SectorId").data("kendoDropDownList").value();

            var selectSelector = '#StationId';
            //Снимаем kendo-обёртку для выпадающего списка со станциями
            $(selectSelector).data("kendoDropDownList").destroy();

            $.post(url, { sectorId: sectorId }, function (result) {
                if (result.status === JsonStatus.ok) {
                    //Удаляем все станции прежнего участка
                    $(selectSelector + ' option').remove();

                    if (result.items.length > 0) {
                        $(selectSelector).removeAttr("disabled");
                        $(selectSelector).append(new Option("--Не определено--", ""));

                        //Добавляем станции нового участка
                        $.each(result.items, function () {
                            $(selectSelector).append(new Option(this.Text, this.Value));
                        });

                        //Снова оборачиваем список в kendo-обёртку
                        var dropDownList = $(selectSelector).kendoDropDownList().data("kendoDropDownList");
                        //Если в списке станций присутствует текущая (которая была в базе на момент загрузки формы),
                        //выделяем эту станцию для наглядности
                        var initValue = $("#station-id").attr("data-id");
                        dropDownList.value(initValue);
                    }
                }

                if (result.status === JsonStatus.error) {
                    alert(result.message);
                }
            })
               .error(function () {
                   alert("get actions error");
               });
        });
    };

    var initTooltips = function() {
        $('span.csMismatch.k-dropdown, span.csMismatch.k-numerictextbox, span.csMismatch.k-datepicker').kendoTooltip({
            content: function (e) {
                var target = e.target; // element for which the tooltip is shown            
                if (target.hasClass('k-dropdown')) {
                    target = $(target).parent().find('select');
                }
                if (target.hasClass('k-numerictextbox') || target.hasClass('k-datepicker')) {
                    target = $(target).parent().find('input.csMismatch:not(.k-formatted-value)');
                }

                var value = target.attr('value1C');
                if (!value) value = 'Данные отсутствуют';
                var result = 'Значение из 1С: ' + value;
                return result;
            }
        });
    };
    
    //Функция, инициализирующая всплывающая окно создания приоритета станции для кассира
    var initPriorityCreatePopup = function() {
        $("#priority-create-popup").kendoWindow({
            actions: {},
            minWidth: "400px",
            minHeight: "180px",
            maxWidth: "400px",
            maxHeight: "180px",
            title: "Создание приоритета станции",
            modal: true,
            visible: false,
            open: function(e) {
                var dataSource = new kendo.data.DataSource({
                    transport: {
                        read: {
                            type: "POST",
                            url: $("#stations-for-priority-url").attr("data-url")
                        },
                        parameterMap: function() {
                            var sectorId = $("#sector-id").attr("data-id");
                            var employeeId = $("#employee-id").attr("data-id");

                            return {
                                sectorId: sectorId,
                                employeeId: employeeId
                            };
                        }
                    },
                    requestEnd: function(e) {
                        if (e.response) {
                            if (e.response.length === 0) {
                                alert("Нет станций с неназначенным приоритетом");
                                $("#priority-create-popup").data("kendoWindow").close();
                            }
                        }
                    },
                    error: function(e) {
                        alert("Произошла ошибка при выполнении запроса");
                    }
                });

                $("#stations-for-priority").kendoDropDownList({
                    dataTextField: "Text",
                    dataValueField: "Value",
                    dataSource: dataSource,
                    dataBound: function() {
                        this.select(0);
                    }
                });
                $("#priority-transfer-time").kendoNumericTextBox({
                    min: 0,
                    max: 255,
                    value: 0,
                    step: 1,
                    decimals: 0,
                    format: "n0"
                });
            },
            close: function(e) {
                $("#stations-for-priority").data("kendoDropDownList").destroy();
                $("#priority-transfer-time").data("kendoNumericTextBox").destroy();
                $("#priority-create-popup").find("span.k-select").remove();
            }
        }).data("kendoWindow");
    };

    var init = function() {
        $("#tabStrip").kendoTabStrip({
            activate: saveCurrentTabState
        });
        
        $('.numericRate').kendoNumericTextBox({
            step: 0.5,
            decimals: 1,
            min: 0.5,
            max: 9
        });
        
        $('.numeric').kendoNumericTextBox({
            format: '0',
            step: 1,
            decimals: 0
        });
        
        $(".datePicker").kendoDatePicker();        
        $('.ddList').kendoDropDownList();
        $('.multiSelect').kendoMultiSelect();
        
        initHandlers();
        
        $('#StationId').removeAttr("disabled");
        $('#StationId').kendoDropDownList();

        initTooltips();
        setCurrentTab();
        initPriorityCreatePopup();
    };

    return {
        saveCurrentTabState: saveCurrentTabState,
        setCurrentTab: setCurrentTab,
        init: init
    };
});

var CustomValidationModule = (function () {
    var overrideSettings = function() {
        //Невидимый input#Rate всё же будет проверен на валидность
        //input.numeric будут игнорироваться jquery validation
        $("form").data("validator").settings.ignore = ":hidden:not(.k-numerictextbox input), .numeric";        
    };

    var overrideMethods = function() {
        //kendo numerictextbox используется в качестве разделителя дробной части запятую,
        //на что негативно реагирует jquery validation, сгенерированный mvc из атрибутов валидации 
        //модели представления. Поэтому кастомизируем правило проверки валидности числовых значений
        $.validator.methods.number = function (value, element) {
            value = parseFloat(value.replace(",", "."));            
            return this.optional(element) || !isNaN(value);
        };
    };

    var overrideHandlers = function() {
        //kendo numerictextbox скрывает input#Rate (оригинальное поле ввода ставки кассира из разметки) и заменяет его
        //на свой input. Скрипт validation.helper не знает про это и пытается наложить подсветку и сообщение о невалидности 
        //на скрытый элемент input#Rate, который kendo нагло подменил на свой. Поэтому приходится обозначать "руками" 
        //невалидность input-а, который подсовывает kendo вместо оригинального
        $("form").on("submit", function (e) {            
            var isRateNotValid = $("#Rate").hasClass("grok-validation-error");
            var prevInput = $("#Rate").prev("input:text");
            var errorMessage = $("span[for=Rate]").text();

            if (prevInput.length > 0) {
                if (isRateNotValid) {
                    $(prevInput).addClass("grok-validation-error");
                    $(prevInput).kendoTooltip({
                        content: errorMessage,
                        width: 150,
                        position: "top"
                    });
                } else {
                    $(prevInput).removeClass("grok-validation-error");

                    if ($(prevInput).data("kendoTooltip") !== undefined) {
                        $(prevInput).data("kendoTooltip").destroy();
                    }
                }
            }
        });
    };

    var init = function() {
        overrideSettings();
        overrideMethods();
        overrideHandlers();
    };

    return {
        init: init  
    };
});

$(document).ready(function() {
    kendo.culture("ru-RU");

    var vacationsGrid = VacationsGridModule();
    vacationsGrid.init();    
    
    var stationPriorityGrid = StationPriorityGridModule();
    stationPriorityGrid.init();
    var customGridCellEditors = CustomGridCellEditorsModule();
    customGridCellEditors.init();
    
    var viewModels = ViewModelsModule();

    kendo.bind($("#csNewVacation"), viewModels.vacationViewModel);
    kendo.bind($("#priority-tab"), viewModels.stationPriorityViewModel);
        
    var pageInterface = PageInterfaceModule();
    pageInterface.init();          

    var customValidation = CustomValidationModule();
    customValidation.init();

    ValidationHelper.init({});    
});