﻿var grok = {
    doubleNumeric: {
        init: function (select) {
            select.addClass("doubleNumeric");
            select.html("<input class='firstInput' value='__,_'>/<input class='secondInput' value='__,_'>");

            var first = select.find(".firstInput");
            first.focus(grok.doubleNumeric._focus);
            first.keypress(grok.doubleNumeric._keypress);
            first.keydown(grok.doubleNumeric._keydown);
            first.bind('paste', grok.doubleNumeric._paste);

            var second = select.find(".secondInput");
            second.focus(grok.doubleNumeric._focus);
            second.keypress(grok.doubleNumeric._keypress);
            second.keydown(grok.doubleNumeric._keydown);
            second.bind('paste', grok.doubleNumeric._paste);
        },

        set: function (select, first, second) {
            var firstNumber = parseFloat(first);
            var secondNumber = parseFloat(second);

            var firstValue = "__,_";
            var secondValue = "__,_";

            if (firstNumber) {
                if (firstNumber > 24) {
                    firstValue = "24,0";
                } else if (firstNumber < 0) {
                    firstValue = "0,0";
                } else {
                    firstValue = "";
                    if (firstNumber >= 10) {
                        firstValue = firstValue + Math.floor(firstNumber / 10);
                    }

                    firstValue = firstValue + Math.floor(firstNumber - (Math.floor(firstNumber / 10) * 10));
                    firstValue = firstValue + ',';
                    firstValue = firstValue + Math.floor((firstNumber - Math.floor(firstNumber)) * 10);
                }
            }
            if (secondNumber) {
                if (secondNumber > 24) {
                    secondValue = "24,0";
                } else if (secondNumber < 0) {
                    secondValue = "0,0";
                } else {
                    secondValue = "";
                    if (secondNumber >= 10) {
                        secondValue = secondValue + Math.floor(secondNumber / 10);
                    }

                    secondValue = secondValue + Math.floor(secondNumber - (Math.floor(secondNumber / 10) * 10));
                    secondValue = secondValue + ',';
                    secondValue = secondValue + Math.floor((secondNumber - Math.floor(secondNumber)) * 10);
                }
            }

            select.find(".firstInput")[0].value = firstValue;
            select.find(".secondInput")[0].value = secondValue;
        },

        get: function (select) {
            return { first: grok.doubleNumeric.getFirst(select), second: grok.doubleNumeric.getSecond(select) };
        },

        getFirst: function (select) {
            var stringValue = select.find(".firstInput")[0].value.split('.').join("");
            var floatValue = parseFloat(stringValue.split(',').join('.'));
            return floatValue;
        },

        getSecond: function (select) {
            var stringValue = select.find(".secondInput")[0].value.split('.').join("");
            var floatValue = parseFloat(stringValue.split(',').join('.'));
            return floatValue;
        },

        _focus: function (e) {
            e.target.selectionStart = 0;
            e.target.selectionEnd = 0;
        },

        _keypress: function (e) {

            var position = e.target.selectionStart;
            var nextPosition = position;
            var xchar = String.fromCharCode(e.keyCode);

            var value = e.target.value;

            if (position == 0) {
                if ((xchar === '0') || (xchar === '1') || (xchar === '2')) {
                    if (value.length == 4) {
                        value = xchar + value.substr(1, 3);
                        nextPosition = 1;
                    } else {
                        value = xchar + value;
                        nextPosition = 1;
                    }
                } else {
                    if (!isNaN(xchar)) {
                        value = xchar + value.substr(value.length - 2, 2);
                        nextPosition = 1;
                    }
                }
            }
            if (position == 1) {
                if (value.length == 3) {
                    if (!isNaN(xchar)) {
                        value = value.substr(0, 2) + xchar;
                        nextPosition = 0;
                    } else {
                        nextPosition = 2;
                    }
                } else {
                    if (!isNaN(xchar)) {
                        value = value.substr(0, 1) + xchar + value.substr(2, 2);
                        nextPosition = 2;
                    }
                }
            }
            if (position == 2) {
                if (value.length == 3) {
                    if (!isNaN(xchar)) {
                        value = value.substr(0, 2) + xchar;
                        nextPosition = 0;
                    }
                } else {
                    if (!isNaN(xchar)) {
                        value = value.substr(0, 3) + xchar;
                        nextPosition = 0;
                    } else {
                        nextPosition = 3;
                    }
                }
            }
            if (position == 3) {
                value = value.substr(0, 3) + xchar;
                nextPosition = 0;
            }

            e.target.value = value;
            e.target.selectionStart = nextPosition;
            e.target.selectionEnd = nextPosition;
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
        },

        _keydown: function (e) {
            if (e.keyCode === 8 || e.keyCode === 46) {
                return false;
            }
        },
        
        _paste:function(e) {
            return false;
        }
    }
};