﻿$(function () {
    kendo.culture("ru-RU");

    var viewModel = kendo.observable({
        typeChange: function (e) {
            TypeMarksModule.check();
        }
    });

    kendo.bind($("form"), viewModel);

    TypeMarksModule.check();

    //Костыль! Предназначен для того, чтобы невалидный kendo numerictextbox не скрывался при клиентской валидации
    $('form').bind('submit', function () {
        $('.k-numerictextbox').show();
    });

    var typeMarksUrl = $('#terminal-type-marks-url').attr('data-url');
    //Перехватчик ajax-запросов (в момент их окончания)
    $(document).ajaxComplete(function (event, xhr, settings) {
        //Если запрос получает список моделей для типа терминала
        if (settings.url === typeMarksUrl) {
            var mark = $("#mark-value").attr("data-mark-value");

            //Если у типа есть модели
            if (mark !== "") {
                $("#Mark").val(mark);
                //Отключение данного обработчика, так как он необходим только при загрузке страницы, 
                //чтобы корректно установить значение модели существующего терминала
                $(event.currentTarget).unbind('ajaxComplete');
            }
        }
    });
});

var TypeMarksModule = (function () {
    //Функция обновления списка моделей в зависимости от типа терминала
    var check = function () {
        var typeMarksUrl = $('#terminal-type-marks-url').attr('data-url');
        var currentType = parseInt($('#Type').val());
        var selectSelector = '#Mark';

        //Заправшиваем список моделей для типа
        $.post(typeMarksUrl, { currentType: currentType }, function (result) {
            if (result.status === JsonStatus.ok) {
                //Очищаем список моделей
                $(selectSelector + ' option').remove();

                //Если у типа есть модели
                if (result.items.length > 0) {
                    $(selectSelector).removeAttr("disabled");

                    //Формируем новый список моделей
                    $.each(result.items, function () {
                        $(selectSelector).append(new Option(this.Text, this.Value));
                    });
                } else {
                    //Если у типа нет моделей, делаем список моделей неактивным
                    $(selectSelector).attr("disabled", "disabled");
                }
            }

            if (result.status === JsonStatus.error) {
                alert(result.message);
            }
        })
            .error(function () {
                alert("get actions error");
            });
    };

    return {
        check: check
    };
}($));