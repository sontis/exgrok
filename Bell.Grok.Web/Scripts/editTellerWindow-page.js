﻿//Перевод времени в формат hh:mm
var stringFromTime = function (date) {
    if (date instanceof Date) {
        return doubleDigit(date.getHours()) + ":" + doubleDigit(date.getMinutes());
    } else {
        return date;
    }
};
//Парсинг даты из формата dd.MM.yyyy
var dateFromString = function (dateString) {
    if (dateString) {
        var dateParts = dateString.split(".");

        return new Date((dateParts[2]), (dateParts[1] - 1), dateParts[0]);
    }
    return null;
};
//Добавление нуля к одиночным цифрам
var doubleDigit = function (value) {
    if (value.toString().length == 2) {
        return value;
    } else {
        return "0" + value;
    }
};
//перевод даты в формат dd.MM.yyyy
var stringFromDate = function (date) {
    if (date) {
        return doubleDigit(date.getDate()) + "." + doubleDigit(date.getMonth() + 1) + "." + date.getFullYear().toString();
    } else {
        return null;
    }
};
//перевод даты в формат yyyy.MM.dd
var revertDate = function (date) {
    if (date) {
        var groups = date.split('.');
        return groups[2] + "." + groups[1] + "." + groups[0];
    } else {
        return null;
    }
};
//Заполнение Режима работы в периоде работы кассы по её идентификатору
var updateOperationPeriod = function (operationPeriod, shiftnessTexts) {
    for (var i = 0; i < shiftnessTexts.length; i++) {
        if (shiftnessTexts[i].value == operationPeriod.shiftnessId) {
            operationPeriod.set("shiftness", shiftnessTexts[i].text);
        }
    }
};

var updateTimePickers = function () {
    var currentDateTime = new Date();
    var currentDate = new Date(currentDateTime.getFullYear(), currentDateTime.getMonth(), currentDateTime.getDate());

    var newMax = new Date(currentDate.getTime() + 24 * 60 * 60 * 1000);
    var newMin = new Date(currentDate.getTime() + 30 * 60 * 1000);
    $("#workTime-timepicker-end").data("kendoTimePicker").max(newMax);
    $("#workTime-timepicker-end").data("kendoTimePicker").min(newMin);
    $("#breakTime-timepicker-end").data("kendoTimePicker").max(newMax);
    $("#breakTime-timepicker-end").data("kendoTimePicker").min(newMin);

};

$(function () {
    kendo.culture("ru-RU");

    var viewModel = kendo.observable({
        //Период работы
        operationPeriods: modelOperationPeriod,
        //Время начала текущего периода работы 
        currentBeginDate: "",
        //Время конца текущего периода работы
        currentEndDate: "",
        //Идентификатор текущего периода работы
        currentId: "",
        //Заголовок панели редактирования текущего периода (два значания для создания и редактиования)
        currentLegend: "",
        //Видимость панели редактирования периода работы
        currentVisible: false,
        //Рижим текущего периода работы
        currentShiftness: 1,
        //Рабочее время текущего периода работы по дням недели
        currentWorkTimes: new kendo.data.ObservableArray([[], [], [], [], [], [], [], []]),
        //Перерывы текущего периода работы по дням недели
        currentBreakTimes: new kendo.data.ObservableArray([[], [], [], [], [], [], [], []]),
        //Видимость панели редактирования времени работы
        currentWorkTimeVisible: false,
        //Видимость панели редактирования перерыва
        currentBreakTimeVisible: false,
        //Идентификатор текужего времени работы или перерыва
        currentTimeId: "",
        //День недели текущего времени работы или перерыва
        currentWeekDay: 1,
        //Изначальной значение дня недели текущего времени работы или перерыва
        currentOriginalWeekDay: 0,
        //Тип текущего перерыва
        currentBreak: 1,
        //Время начала текущего времени работы или перерыва
        currentBeginTime: "",
        //Время завершения текущего времени работы или перерыва
        currentEndTime: "",
        //Видимость сообщения об ошибке редактирования периода работы
        isOperationPeriodEndNeeded: false,
        //Видимость сообщения об ошибке редактирования периода работы
        isOperationPeriodError: false,
        //Сообщение об ошибке при редактировании периода работы
        operationPeriodError: "",
        //Заголовок панели редактирования времени работы или перерыва
        currentOperationPeriodLegend: "",
        //Видимость кнопок добавления времени работы и перерывов
        currentOperationPeriodButtonVisible: true,
        //Видимость сообщения об ошибке редактирования времени работы или перерыва
        isTimeError: false,
        //Сообщение об ошибке при редактировании времени работы или перерыва
        timeError: "",
        //Удаление периода работы
        deleteOperationPeriod: function (e) {
            var operationPeriod = e.data;

            var operationPeriods = this.get("operationPeriods");

            var index = operationPeriods.indexOf(operationPeriod);

            if (operationPeriod.id === this.get("currentId")) {
                this.set("currentVisible", false);
                this.set("currentWorkTimeVisible", false);
                this.set("currentBreakTimeVisible", false);
                this.set("currentWorkTimeVisible", false);
                this.set("currentBreakTimeVisible", false);
            }

            operationPeriods.splice(index, 1);
        },
        //Редактиование периода работы
        detailOperationPeriod: function (e) {
            var operationPeriod = e.data;

            var operationPeriods = this.get("operationPeriods");
            var needClose = false;
            for (var i = 0; i < operationPeriods.length; i++) {
                if (!(operationPeriods[i].endDate))
                    if (operationPeriods[i].id != operationPeriod.id) {
                        needClose = true;
                    }
            }

            this.set("currentId", operationPeriod.id);
            this.set("currentBeginDate", dateFromString(operationPeriod.beginDate));
            this.set("currentEndDate", dateFromString(operationPeriod.endDate));
            this.set("currentVisible", true);
            this.set("currentShiftness", operationPeriod.shiftnessId);
            this.set("currentWorkTimeVisible", false);
            this.set("currentBreakTimeVisible", false);
            this.set("currentLegend", "Редактирование периода работы");
            this.set("currentOperationPeriodButtonVisible", true);
            this.set("isOperationPeriodError", false);
            this.set("isTimeError", false);
            this.set("isOperationPeriodEndNeeded", needClose);

            var workTimes = new Array();
            for (var i = 0; i < 8; i++) {
                workTimes[i] = new Array();
                for (var j = 0; j < operationPeriod.workTimes[i].length; j++) {
                    workTimes[i][j] = kendo.observable({
                        id: operationPeriod.workTimes[i][j].id,
                        beginTime: operationPeriod.workTimes[i][j].beginTime,
                        endTime: operationPeriod.workTimes[i][j].endTime
                    });
                }
            }

            var breakTimes = new Array();
            for (var i = 0; i < 8; i++) {
                breakTimes[i] = new Array();
                for (var j = 0; j < operationPeriod.breakTimes[i].length; j++) {
                    breakTimes[i][j] = kendo.observable({
                        id: operationPeriod.breakTimes[i][j].id,
                        beginTime: operationPeriod.breakTimes[i][j].beginTime,
                        endTime: operationPeriod.breakTimes[i][j].endTime,
                        breakId: operationPeriod.breakTimes[i][j].breakId
                    });
                }
            }

            this.set("currentWorkTimes", new kendo.data.ObservableArray(workTimes));
            this.set("currentBreakTimes", new kendo.data.ObservableArray(breakTimes));

            updateTimePickers();

        },
        //Добавление периода работы
        addOperationPeriod: function () {
            var id = 0;

            this.set("currentId", id);
            this.set("currentBeginDate", "");
            this.set("currentEndDate", "");
            this.set("currentVisible", true);
            this.set("currentShiftness", 1);
            this.set("currentWorkTimes", new kendo.data.ObservableArray([[], [], [], [], [], [], [], []]));
            this.set("currentBreakTimes", new kendo.data.ObservableArray([[], [], [], [], [], [], [], []]));
            this.set("currentWorkTimeVisible", false);
            this.set("currentBreakTimeVisible", false);
            this.set("currentLegend", "Добавление периода работы");
            this.set("currentOperationPeriodButtonVisible", true);
            this.set("isOperationPeriodError", false);
            this.set("isTimeError", false);

            var needClose = false;
            var operationPeriods = this.get("operationPeriods");
            for (var i = 0; i < operationPeriods.length; i++) {
                if (!(operationPeriods[i].endDate)) {
                    needClose = true;
                }
            }
            this.set("isOperationPeriodEndNeeded", needClose);

            updateTimePickers();
        },
        //Отмена редактирования/добавления данных периода работы
        cancelOperationPeriod: function () {
            this.set("currentVisible", false);
        },
        //Сохранение редактируеммых данных периода работы
        detailOperationPeriodSubmit: function () {

            if (!this.get("currentBeginDate")) {
                this.set("operationPeriodError", "Необходимо указать дату начала периода");
                this.set("isOperationPeriodError", true);
                return;
            }
            var beginDate = stringFromDate(this.get("currentBeginDate"));

            var endDate;
            if (this.get("currentBeginDate")) {
                endDate = stringFromDate(this.get("currentEndDate"));

                if (!endDate) {
                    if (this.get("isOperationPeriodEndNeeded")) {
                        this.set("operationPeriodError", "Необходимо указать дату конца периода");
                        this.set("isOperationPeriodError", true);
                        return;
                    }
                }

                if (this.get("currentEndDate")) {
                    if (this.get("currentEndDate") < this.get("currentBeginDate")) {
                        this.set("operationPeriodError", "Конец периода позднее начала");
                        this.set("isOperationPeriodError", true);
                        return;
                    }
                }

                var fl = false;
                var currentBegin = revertDate(stringFromDate(this.get("currentBeginDate")));
                var operationPeriodsForRead = this.get("operationPeriods");
                var currenId = this.get("currentId");
                if (!endDate) {
                    for (i = 0; i < operationPeriodsForRead.length; i++) {
                        if (operationPeriodsForRead[i].id != currenId) {
                            if (revertDate(operationPeriodsForRead[i].endDate) > currentBegin) {
                                fl = true;
                            }
                        }
                    }
                } else {
                    var currentEnd = revertDate(stringFromDate(this.get("currentEndDate")));
                    for (i = 0; i < operationPeriodsForRead.length; i++) {
                        if (operationPeriodsForRead[i].id != currenId) {
                            if (operationPeriodsForRead[i].endDate) {
                                if ((revertDate(operationPeriodsForRead[i].beginDate) >= currentBegin) && (revertDate(operationPeriodsForRead[i].beginDate) < currentEnd)) {
                                    fl = true;
                                }
                                if ((revertDate(operationPeriodsForRead[i].endDate) > currentBegin) && (revertDate(operationPeriodsForRead[i].endDate) <= currentEnd)) {
                                    fl = true;
                                }
                                if ((revertDate(operationPeriodsForRead[i].beginDate) <= currentBegin) && (revertDate(operationPeriodsForRead[i].endDate) >= currentEnd)) {
                                    fl = true;
                                }
                            } else {
                                if (revertDate(operationPeriodsForRead[i].beginDate) < currentEnd) {
                                    fl = true;
                                }
                            }
                        }
                    }
                }
                if (fl) {
                    this.set("operationPeriodError", "Введенный режим работы кассового окна пересекается с уже добавленным режимом");
                    this.set("isOperationPeriodError", true);
                    return;
                }

            } else {
                endDate = null;
            }

            var i;
            var operationPeriods;
            var operationPeriod;
            //Проверка на то что период добавляется
            if (this.get("currentId") === 0) {
                var id = 0;
                operationPeriods = this.get("operationPeriods");
                for (i = 0; i < operationPeriods.length; i++) {
                    if (id > operationPeriods[i].id) {
                        id = operationPeriods[i].id;
                    }
                }
                operationPeriod = kendo.observable({
                    id: id - 1,
                    beginDate: beginDate,
                    endDate: endDate,
                    shiftnessId: this.get("currentShiftness"),
                    workTimes: this.get("currentWorkTimes"),
                    breakTimes: this.get("currentBreakTimes"),
                    shiftness: ""
                });
                updateOperationPeriod(operationPeriod, this.get("shiftnessTexts"));
                this.get("operationPeriods").push(operationPeriod);
            } else {
                operationPeriod = null;
                var currentId = this.get("currentId");
                operationPeriods = this.get("operationPeriods");
                for (i = 0; i < operationPeriods.length; i++) {
                    if (currentId === operationPeriods[i].id) {
                        operationPeriod = operationPeriods[i];
                    }
                }


                var index = operationPeriods.indexOf(operationPeriod);

                operationPeriod.set("beginDate", beginDate);
                operationPeriod.set("endDate", endDate);
                operationPeriod.set("shiftnessId", this.get("currentShiftness"));
                operationPeriod.set("workTimes", this.get("currentWorkTimes"));
                operationPeriod.set("breakTimes", this.get("currentBreakTimes"));

                updateOperationPeriod(operationPeriod, this.get("shiftnessTexts"));
            }
            this.set("currentVisible", false);
        },
        //Редактирование рабочего времени
        detailWorkTime: function (e) {
            var workTime = e.data;

            var weekId = 1;
            for (var i = 1; i < this.get("currentWorkTimes").length; i++) {
                if (this.get("currentWorkTimes")[i].indexOf(workTime) > -1) {
                    weekId = i + 1;
                }
            }

            this.set("currentTimeId", workTime.id);
            this.set("currentWeekDay", weekId);
            this.set("currentOriginalWeekDay", weekId);
            this.set("currentWorkTimeVisible", true);
            this.set("currentBreakTimeVisible", false);
            this.set("currentBeginTime", workTime.beginTime);
            this.set("currentEndTime", workTime.endTime);
            this.set("currentOperationPeriodLegend", "Редактирование времени работы");
            this.set("currentOperationPeriodButtonVisible", false);
            this.set("isTimeError", false);
        },
        //Сохранение редактируемых данных рабочего времени
        detailWorkTimeSubmit: function () {
            var id = 0;
            var currentWorkTimes = this.get("currentWorkTimes");
            var currentBreakTimes = this.get("currentBreakTimes");
            var weekDay = this.get("currentOriginalWeekDay");
            var i;

            if ((this.get("currentBeginTime") == null) || (this.get("currentEndTime") == null)) {
                this.set("timeError", "Неверно указано время");
                this.set("isTimeError", true);
                return;
            }

            if (stringFromTime(this.get("currentBeginTime")) > stringFromTime(this.get("currentEndTime"))) {
                if (stringFromTime(this.get("currentEndTime")) != "00:00") {
                    this.set("timeError", "Начало времени превышает конец");
                    this.set("isTimeError", true);
                    return;
                }
            }

            if (stringFromTime(this.get("currentBeginTime")) === stringFromTime(this.get("currentEndTime"))) {
                this.set("timeError", "Начало времени совпадает с концом");
                this.set("isTimeError", true);
                return;
            }

            var fl = false;
            var currentDay = this.get("currentWeekDay") - 1;
            var currentBegin = stringFromTime(this.get("currentBeginTime"));
            var currentEnd = stringFromTime(this.get("currentEndTime"));
            if (currentEnd === "00:00") {
                currentEnd = "24:00";
            }
            for (i = 0; i < currentWorkTimes[currentDay].length; i++) {
                if ((weekDay === 0) || (currentWorkTimes[currentDay][i].id != this.get("currentTimeId"))) {
                    if ((currentWorkTimes[currentDay][i].beginTime >= currentBegin) && (currentWorkTimes[currentDay][i].beginTime < currentEnd)) {
                        fl = true;
                    }
                    if ((currentWorkTimes[currentDay][i].endTime > currentBegin) && (currentWorkTimes[currentDay][i].endTime <= currentEnd)) {
                        fl = true;
                    }
                    if ((currentWorkTimes[currentDay][i].beginTime <= currentBegin) && (currentWorkTimes[currentDay][i].endTime >= currentEnd)) {
                        fl = true;
                    }
                }
            }

            if (fl) {
                this.set("timeError", "Рабочее время не должно пересекаться с другими");
                this.set("isTimeError", true);
                return;
            }            

            //Ищем перерывы, которые находились в данном рабочем интервале до начала его редактирования
            var breakInCurrentWorkTime = [];
            for (i = 0; i < currentWorkTimes[currentDay].length; i++) {
                if (currentWorkTimes[currentDay][i].id == this.get("currentTimeId")) {
                    for (j = 0; j < currentBreakTimes[currentDay].length; j++) {
                        if (((currentWorkTimes[currentDay][i].beginTime <= currentBreakTimes[currentDay][j].beginTime) && (currentWorkTimes[currentDay][i].endTime > currentBreakTimes[currentDay][j].endTime)) ||
                            ((currentWorkTimes[currentDay][i].beginTime < currentBreakTimes[currentDay][j].beginTime) && (currentWorkTimes[currentDay][i].endTime >= currentBreakTimes[currentDay][j].endTime))) {
                            breakInCurrentWorkTime.push(currentBreakTimes[currentDay][j]);
                        }
                    }

                    break;
                }
            }
            
            //Сверяем найденные перерывы с новыми границами текущего рабочего интервала
            for (i = 0; i < breakInCurrentWorkTime.length; i++) {
                if ((breakInCurrentWorkTime[i].beginTime < currentBegin) ||
                    (breakInCurrentWorkTime[i].endTime > currentEnd)) {
                    this.set("timeError", "В текущий день появятся перерывы, не входящие ни в один рабочий интервал");
                    this.set("isTimeError", true);
                    return;
                }
            }
            

            if (weekDay > 0) {
                var index = -1;
                id = this.get("currentTimeId");

                for (i = 0; i < currentWorkTimes[weekDay - 1].length; i++) {
                    if (currentWorkTimes[weekDay - 1][i].id === this.get("currentTimeId")) {
                        index = i;
                    }
                }

                currentWorkTimes[weekDay - 1].splice(index, 1);
            } else {
                for (i = 0; i < currentWorkTimes.length; i++) {
                    for (var j = 0; j < currentWorkTimes[i].length; j++) {
                        if (currentWorkTimes[i][j].id < id) {
                            id = currentWorkTimes[i][j].id;
                        }
                    }
                }
                id = id - 1;
            }

            currentWorkTimes[currentDay].push(kendo.observable({
                id: id,
                beginTime: currentBegin,
                endTime: currentEnd,
            }));

            //Сброс модели для инициализации обновления
            this.set("currentWorkTimes", [[], [], [], [], [], [], [], []]);
            this.set("currentWorkTimes", currentWorkTimes);

            this.set("currentWorkTimeVisible", false);
            this.set("currentBreakTimeVisible", false);
            this.set("currentOperationPeriodButtonVisible", true);
        },
        //Добавление рабочего времени
        addWorkTime: function () {

            var weekId = 1;

            this.set("currentTimeId", 0);
            this.set("currentWeekDay", weekId);
            this.set("currentOriginalWeekDay", 0);
            this.set("currentWorkTimeVisible", true);
            this.set("currentBreakTimeVisible", false);
            this.set("currentBeginTime", "09:00");
            this.set("currentEndTime", "18:00");
            this.set("currentOperationPeriodLegend", "Добавление времени работы");
            this.set("currentOperationPeriodButtonVisible", false);
            this.set("isTimeError", false);
        },
        //Удаление рабочего времени
        deleteWorkTime: function (e) {
            var currentWorkTimes = this.get("currentWorkTimes");
            var currentBreakTimes = this.get("currentBreakTimes");
            id = e.data.id;

            for (var weekDay = 1; weekDay <= currentWorkTimes.length; weekDay++) {
                var index = -1;
                for (var i = 0; i < currentWorkTimes[weekDay - 1].length; i++) {
                    if (currentWorkTimes[weekDay - 1][i].id === id) {
                        index = i;
                    }
                }
                if (index > -1) {                                                     
                    //Удаляем перерывы, входящие в рабочее время    
                    for (var j = 0; j < currentBreakTimes[weekDay - 1].length; j++) {
                        if (((currentWorkTimes[weekDay - 1][index].beginTime <= currentBreakTimes[weekDay - 1][j].beginTime) && (currentWorkTimes[weekDay - 1][index].endTime > currentBreakTimes[weekDay - 1][j].endTime)) ||
                            ((currentWorkTimes[weekDay - 1][index].beginTime < currentBreakTimes[weekDay - 1][j].beginTime) && (currentWorkTimes[weekDay - 1][index].endTime >= currentBreakTimes[weekDay - 1][j].endTime))) {                            
                            currentBreakTimes[weekDay - 1].splice(j, 1);
                            j = j - 1;
                        }
                    }

                    currentWorkTimes[weekDay - 1].splice(index, 1);                                       
                }
            }

            //Сброс модели для инициализации обновления
            this.set("currentWorkTimes", [[], [], [], [], [], [], [], []]);
            this.set("currentWorkTimes", currentWorkTimes);
            
            this.set("currentBreakTimes", [[], [], [], [], [], [], [], []]);
            this.set("currentBreakTimes", currentBreakTimes);
        },
        //Редактирование перерыва
        detailBreakTime: function (e) {
            var breakTime = e.data;

            var weekId = 1;
            for (var i = 1; i < this.get("currentBreakTimes").length; i++) {
                if (this.get("currentBreakTimes")[i].indexOf(breakTime) > -1) {
                    weekId = i + 1;
                }
            }

            this.set("currentTimeId", breakTime.id);
            this.set("currentWeekDay", weekId);
            this.set("currentOriginalWeekDay", weekId);
            this.set("currentBreak", breakTime.breakId);
            this.set("currentWorkTimeVisible", false);
            this.set("currentBreakTimeVisible", true);
            this.set("currentBeginTime", breakTime.beginTime);
            this.set("currentEndTime", breakTime.endTime);
            this.set("currentOperationPeriodLegend", "Редактирование времени перерыва");
            this.set("currentOperationPeriodButtonVisible", false);
            this.set("isTimeError", false);
        },
        //сохранение данных редактируемого пеерыва
        detailBreakTimeSubmit: function () {
            var id = 0;
            var currentBreakTimes = this.get("currentBreakTimes");
            var currentWorkTimes = this.get("currentWorkTimes");
            var weekDay = this.get("currentOriginalWeekDay");
            var i;

            if ((this.get("currentBeginTime") == null) || (this.get("currentEndTime") == null)) {
                this.set("timeError", "Неверно указано время");
                this.set("isTimeError", true);
                return;
            }

            if (stringFromTime(this.get("currentBeginTime")) > stringFromTime(this.get("currentEndTime"))) {
                if (stringFromTime(this.get("currentEndTime")) != "00:00") {
                    this.set("timeError", "Начало времени превышает конец");
                    this.set("isTimeError", true);
                    return;
                }
            }

            if (stringFromTime(this.get("currentBeginTime")) === stringFromTime(this.get("currentEndTime"))) {
                this.set("timeError", "Начало времени совпадает с концом");
                this.set("isTimeError", true);
                return;
            }

            var fl = false;
            var currentDay = this.get("currentWeekDay") - 1;
            var currentBegin = stringFromTime(this.get("currentBeginTime"));
            var currentEnd = stringFromTime(this.get("currentEndTime"));
            if (currentEnd === "00:00") {
                currentEnd = "24:00";
            }
            for (i = 0; i < currentBreakTimes[currentDay].length; i++) {
                if ((weekDay === 0) || (currentBreakTimes[currentDay][i].id != this.get("currentTimeId"))) {
                    if ((currentBreakTimes[currentDay][i].beginTime >= currentBegin) && (currentBreakTimes[currentDay][i].beginTime < currentEnd)) {
                        fl = true;
                    }
                    if ((currentBreakTimes[currentDay][i].endTime > currentBegin) && (currentBreakTimes[currentDay][i].endTime <= currentEnd)) {
                        fl = true;
                    }
                    if ((currentBreakTimes[currentDay][i].beginTime <= currentBegin) && (currentBreakTimes[currentDay][i].endTime >= currentEnd)) {
                        fl = true;
                    }
                }
            }

            if (fl) {
                this.set("timeError", "Время перерыва не должно пересекаться с другими");
                this.set("isTimeError", true);
                return;
            }

            fl = true;
            for (i = 0; i < currentWorkTimes[currentDay].length; i++) {               
                if ((weekDay === 0) || (currentWorkTimes[currentDay][i].id != this.get("currentTimeId"))) {
                    if (((currentWorkTimes[currentDay][i].beginTime <= currentBegin) && (currentWorkTimes[currentDay][i].endTime > currentEnd)) ||
                        ((currentWorkTimes[currentDay][i].beginTime < currentBegin) && (currentWorkTimes[currentDay][i].endTime >= currentEnd))) {
                        fl = false;
                    }
                }
            }

            if (fl) {
                this.set("timeError", "Время перерыва должно входить в одно рабочее время");
                this.set("isTimeError", true);
                return;
            }

            if (weekDay > 0) {
                var index = -1;
                id = this.get("currentTimeId");

                for (i = 0; i < currentBreakTimes[weekDay - 1].length; i++) {
                    if (currentBreakTimes[weekDay - 1][i].id === this.get("currentTimeId")) {
                        index = i;
                    }
                }

                currentBreakTimes[weekDay - 1].splice(index, 1);
            } else {
                for (i = 0; i < currentBreakTimes.length; i++) {
                    for (var j = 0; j < currentBreakTimes[i].length; j++) {
                        if (currentBreakTimes[i][j].id < id) {
                            id = currentBreakTimes[i][j].id;
                        }
                    }
                }
                id = id - 1;
            }

            currentBreakTimes[currentDay].push(kendo.observable({
                id: id,
                beginTime: currentBegin,
                endTime: currentEnd,
                breakId: this.get("currentBreak")
            }));

            //Сброс модели для инициализации обновления
            this.set("currentBreakTimes", [[], [], [], [], [], [], [], []]);
            this.set("currentBreakTimes", currentBreakTimes);

            this.set("currentWorkTimeVisible", false);
            this.set("currentBreakTimeVisible", false);
            this.set("currentOperationPeriodButtonVisible", true);
        },
        //Добавление перерыва
        addBreakTime: function () {

            var weekId = 1;

            this.set("currentTimeId", 0);
            this.set("currentWeekDay", weekId);
            this.set("currentOriginalWeekDay", 0);
            this.set("currentBreak", 1);
            this.set("currentWorkTimeVisible", false);
            this.set("currentBreakTimeVisible", true);
            this.set("currentBeginTime", "13:00");
            this.set("currentEndTime", "14:00");
            this.set("currentOperationPeriodLegend", "Добавление времени перерыва");
            this.set("currentOperationPeriodButtonVisible", false);
            this.set("isTimeError", false);
        },
        //Удаление перерыва
        deleteBreakTime: function (e) {
            var currentBreakTimes = this.get("currentBreakTimes");
            id = e.data.id;

            for (var weekDay = 1; weekDay <= currentBreakTimes.length; weekDay++) {
                var index = -1;
                for (var i = 0; i < currentBreakTimes[weekDay - 1].length; i++) {
                    if (currentBreakTimes[weekDay - 1][i].id === id) {
                        index = i;
                    }
                }
                if (index > -1) {
                    currentBreakTimes[weekDay - 1].splice(index, 1);
                }
            }

            //Сброс модели для инициализации обновления
            this.set("currentBreakTimes", [[], [], [], [], [], [], [], []]);
            this.set("currentBreakTimes", currentBreakTimes);
        },
        //Отмена редактирования/добавления данных рабочего времени или перерыва
        cancelOperationTime: function () {
            this.set("currentWorkTimeVisible", false);
            this.set("currentBreakTimeVisible", false);
            this.set("currentOperationPeriodButtonVisible", true);
        },
        //Сохранени данных всех периодов работы кассовых окон в отправляемую на сервер форму
        saveTellerWindow: function () {
            $("#OperationPeriodsData").val(JSON.stringify(this.get("operationPeriods")));
        },
        //Режимы работы кассовых окон
        shiftnessTexts: modelShiftnessTexts,
        //Типы перерывов
        breakTexts: modelBreakTexts
    });

    //Заполнение режима работы по идентификатору
    for (var i = 0; i < viewModel.operationPeriods.length; i++) {
        updateOperationPeriod(viewModel.operationPeriods[i], viewModel.shiftnessTexts);
    }

    kendo.bind($("form"), viewModel);


    //Костыль! Предназначен для того, чтобы невалидный kendo numerictextbox не скрывался при клиентской валидации
    $('form').bind('submit', function () {
        $('.k-numerictextbox').show();

        if ($("#modelIsNew").attr("data-value")) {
            $("form").validate().element("#Number");
        }

        ValidateMultiselectSpecializations();
    });

    var multiselectSpecializations = $("#Specializations").data("kendoMultiSelect");
    multiselectSpecializations.bind("change", function (e) {
        ValidateMultiselectSpecializations();
    });
});

var ValidateMultiselectSpecializations = function () {
    if ($("#Specializations").data("kendoMultiSelect").value().length === 0) {
        $("span[data-valmsg-for=Specializations]").
            removeClass("field-validation-valid").
            addClass("field-validation-error").
            html("<span for=\"Specializations\" class=\"\" style=\"\">" + $("#Specializations").attr("data-val-required") + "</span>");
    } else {
        if ($("span[data-valmsg-for=Specializations]").hasClass("field-validation-error") &&
            $("#Specializations").data("kendoMultiSelect").value().length > 0) {
            $("span[data-valmsg-for=Specializations]").
                removeClass("field-validation-error").
                addClass("field-validation-valid").
                html("");
        }
    }
};