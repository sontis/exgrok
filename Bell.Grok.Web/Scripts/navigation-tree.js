﻿$(function () {    
    var isAdminUser = $("#is-user-is-administrator").attr("data-is-admin").toLowerCase() === "true";
    var isAuthenticated = $("#is-user-authenticated").attr("data-is-authenticated").toLowerCase() === "true";
    var isWorkDepartmentUser = $("#current-controller").attr("data-current-controller") === "ScheduleTemplate" ||
                               $("#current-controller").attr("data-current-controller") === "WorkDepartmentUser";
    //Скрипт должен выполняться только для авторизованных пользователей, не являющихся админами
    if (!isAuthenticated || isAdminUser || isWorkDepartmentUser) {
        return;
    }
        
    kendo.culture("ru-RU");
    
    //Определяем по url текущий выделенный элемент и его родителя
    var selectedNode = $("#selected-node-unified-tree-id").attr("data-unified-id");
    var parentNode = $("#parent-node-unified-tree-id").attr("data-unified-id");
    
    //Запрашиваем у сервера ветку, в которой находится текущий выделенный элемент
    if (selectedNode !== "" ) {
        
        $.session.set('SelectedNode', selectedNode);
        $.session.delete('ExpandedNodes');

        $.ajax({
            async: false,
            type: "POST",
            url: '/update-nodes',
            data: { node: selectedNode, parentNode: parentNode },
            success: function (responce) {
                if (responce.Branch !== undefined) {
                    var i;
                    //Сохраняем в куках уникальные идентификаторы узлов откраемой ветки
                    for (i = 0; i < responce.Branch.length; i++) {
                        DirectionTreeModule.updateTreeViewState(responce.Branch[i], "add");
                    }
                }
            },
            error: function (xhr, status) {
                if (!window.unloading) {
                    alert('Ошибка при выполнении запроса: ' +
                        status + '\nКод ответа: ' +
                        xhr.status);
                }
            },
            dataType: "json"
        });
    }

    var mod = Controller.instance;

    var DirectionTree = mod.create(DirectionTreeModule);
    var CashierContractDatepicker = mod.create(CashierContractDatepickerModule);

    var directionTree = new DirectionTree({ el: $("#direction-tree") });
    var cashierContractDatepicker = new CashierContractDatepicker({ el: $("#cashier-contract-datepicker-container") });
});

var CashierContractDatepickerModule = {
    elements: {
        "#cashier-contract-datepicker": "cashierContractDatepicker",
        "#current-date": "currentDate"
    },
    
    events: {
        "blur cashierContractDatepicker": "cashierContractDatepickerBlur"
    },
    
    //Обработчик потери фокуса для datepicker-а фильтрации кассиров по дате
    cashierContractDatepickerBlur: function () {
        var cashierContractDatepickerSelector = this.trimParentSelector(this.cashierContractDatepicker.selector);

        var value = $(cashierContractDatepickerSelector).val();
        //Проверяем с помощью регулярного выражения введённую дату
        if (!value.match(/^((((31\.(0?[13578]|1[02]))|((29|30)\.(0?[1,3-9]|1[0-2])))\.(1[6-9]|[2-9]\d)?\d{2})|(29\.0?2\.(((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))|(0?[1-9]|1\d|2[0-8])\.((0?[1-9])|(1[0-2]))\.((1[6-9]|[2-9]\d)?\d{2}))$/)) {
            $(cashierContractDatepickerSelector).addClass("grok-validation-error");
            $(cashierContractDatepickerSelector).kendoTooltip({
                content: "Недопустимая дата. Необходимый формат: dd.MM.yyyy",
                width: 150,
                position: "top"
            });
        }
    },
    
    init: function () {        
        var cashierContractDatepickerSelector = this.trimParentSelector(this.cashierContractDatepicker.selector);
        var currentDateSelector = this.trimParentSelector(this.currentDate.selector);
        
        $(cashierContractDatepickerSelector).kendoDatePicker({
            format: "dd.MM.yyyy",
            //Событие изменения даты (kendo-event)
            change: function() {
                var parentNode = $("#parent-node-unified-tree-id").attr("data-unified-id");
                var currentDate = kendo.toString(this.value(), "dd.MM.yyyy");

                //Если введённое значение корректно распарсилось
                if (currentDate !== null) {
                    //Убираем подсветку ошибки
                    $(cashierContractDatepickerSelector).removeClass("grok-validation-error");
                    if ($(cashierContractDatepickerSelector).data("kendoTooltip") !== undefined) {
                        $(cashierContractDatepickerSelector).data("kendoTooltip").destroy();
                    }

                    //Делаем переход на домашнюю страницу с раскрытием ветки дерева
                    var redirectUrl = $("#get-home-after-change-date-url").attr("data-home-after-change-date-url") +
                        "?parentnode=" + parentNode +
                        "&date=" + currentDate;
                    
                    $(location).attr("href", redirectUrl);
                }
            }
        });
                
        //Берём время для фильтрации кассиров из сессии        
        var currentCashierDatePickerValue = $(currentDateSelector).attr("data-date");
        if (currentCashierDatePickerValue !== "" && currentCashierDatePickerValue !== undefined) {
            //Если в сессии есть значение времени фильтрации кассиров            
            $(cashierContractDatepickerSelector).data("kendoDatePicker").value(currentCashierDatePickerValue);            
        } else {
            //Если в сессии нет значения времени фильтрации кассиров, то
            //используем для этого текущую дату
            var today = new Date();
            var year = today.getFullYear();
            var month = today.getMonth();
            var day = today.getDate();

            $(cashierContractDatepickerSelector).data("kendoDatePicker").value(new Date(year, month, day));
        }
    }
};

//Модуль kendo-treeview
var DirectionTreeModule = {
    elements: {
        "#treeview": "treeView",
        "#get-directions-url": "directionsUrl",
        "#get-sectors-url": "sectorsUrl",
        "#get-sectors-structure-url": "sectorStructureUrl",
        "#get-stations-url": "stationsUrl",
        "#get-cashiers-url": "cashiersUrl"
    },

    events: {

    },       

    //Функция, инициализирующая дерево навигации
    init: function () {
        var treeViewSelector = this.trimParentSelector(this.treeView.selector);

        var directionUrlSelector = this.trimParentSelector(this.directionsUrl.selector);
        var directionUrl = $(directionUrlSelector).attr('data-get-directions-url');

        var sectorUrlSelector = this.trimParentSelector(this.sectorsUrl.selector);
        var sectorUrl = $(sectorUrlSelector).attr('data-get-sectors-url');

        var sectorStructureUrlSelector = this.trimParentSelector(this.sectorStructureUrl.selector);
        var sectorStructureUrl = $(sectorStructureUrlSelector).attr('data-get-sectors-structure-url');

        var stationUrlSelector = this.trimParentSelector(this.stationsUrl.selector);
        var stationUrl = $(stationUrlSelector).attr('data-get-stations-url');
        
        var cashiersUrlSelector = this.trimParentSelector(this.cashiersUrl.selector);
        var cashiersUrl = $(cashiersUrlSelector).attr('data-get-cashiers-url');

        var updateTreeViewState = this.updateTreeViewState;
        var searchInTreeData = this.searchInTreeData;

        var cashiers = {
            transport: {
                read: {
                    url: cashiersUrl,
                    dataType: "json",
                    type: "Post",
                    data: function(e) {
                        var optionsArray = e.ObjectId.split(' ');
                        var changedDate = kendo.toString($("#cashier-contract-datepicker").data("kendoDatePicker").value(), "dd.MM.yyyy");

                        return {
                            sectorId: optionsArray[0],
                            date: changedDate,
                            character: optionsArray[1]
                        };
                    }
                }
            },
            schema: {
                model: {
                    id: "ObjectId",
                    fields: {
                        "ObjectName": {
                            type: "string"
                        },
                        "UnifiedTreeId": {
                            type: "string"
                        },
                        "ObjectUrl": {
                            type: "string"
                        }
                    }                    
                }
            }
        };

        var stations = {
            transport: {
                read: {
                    url: stationUrl,
                    dataType: "json",
                    type: "Post",
                    data: function(e) {
                        var optionsArray = e.SectorStructureItemId.split(' ');
                        var changedDate = kendo.toString($("#cashier-contract-datepicker").data("kendoDatePicker").value(), "dd.MM.yyyy");

                        return {
                            type: optionsArray[0],
                            sectorId: optionsArray[1],
                            date: changedDate,
                            scheduleStatuses: $("#ScheduleStatus").data("kendoMultiSelect")
                                                ? $.arrayToString($("#ScheduleStatus").data("kendoMultiSelect").value())
                                                : null
                        };
                    }
                }
            },                                                
            schema: {
                model: {
                    id: "ObjectId",                    
                    url: "ObjectUrl",                    
                    hasChildren: "HasChildren",
                    fields: {
                        "ObjectName": {
                            type: "string"
                        },
                        "UnifiedTreeId": {
                            type: "string"
                        }
                    },
                    children: cashiers
                },
                data: "Items"
            }
        };

        var sectorStructure = {
            transport: {
                read: {
                    url: sectorStructureUrl,
                    dataType: "json",
                    type: "Post",
                    data: function (e) {
                        var changedDate = kendo.toString($("#cashier-contract-datepicker").data("kendoDatePicker").value(), "dd.MM.yyyy");

                        return {
                            sectorId: e.SectorId,
                            date: changedDate
                        };                        
                    }
                }
            },
            schema: {
                model: {
                    id: "SectorStructureItemId",
                    hasChildren: "HasChildren",
                    fields: {
                        "SectorStructureItemName": {
                            type: "string"
                        },
                        "UnifiedTreeId": {
                            type: "string"
                        }
                    },
                    children: stations
                },
                data: "Items"                
            }
        };
        
        var sectors = {
            transport: {
                read: {
                    url: sectorUrl,
                    dataType: "json",
                    type: "Post",
                    data: function (e) {
                        return {
                            directionId: e.DirectionId,
                            scheduleStatuses: $("#ScheduleStatus").data("kendoMultiSelect") 
                                                ? $.arrayToString($("#ScheduleStatus").data("kendoMultiSelect").value())
                                                : null
                        };
                    }
                }
            },
            schema: {
                model: {
                    id: "SectorId",
                    hasChildren: "HasChildren",
                    fields: {
                        "SectorName": {
                            type: "string"
                        },
                        "UnifiedTreeId": {
                            type: "string"
                        }
                    },                    
                    children: sectorStructure
                },
                data: "Items"
            }                                        
        };

        var directionTreeViewDataSource = new kendo.data.HierarchicalDataSource({
            transport: {
                read: {
                    url: directionUrl,
                    dataType: "json",
                    type: "Post",                    
                    data: function (e) {
                        return {
                            scheduleStatuses: $("#ScheduleStatus").data("kendoMultiSelect")
                                                ? $.arrayToString($("#ScheduleStatus").data("kendoMultiSelect").value())
                                                : null
                        };
                    }
                }
            },            
            schema: {
                model: {                                                           
                    id: "DirectionId",
                    hasChildren: "HasChildren",
                    fields: {                                                 
                        "DirectionName": {
                            type: "string"
                        },
                        "UnifiedTreeId": {
                            type: "string"
                        }
                    },
                    children: sectors
                },
                data: "Items"
            }            
        });

        $(treeViewSelector).kendoTreeView(
            {
                dataSource: directionTreeViewDataSource,
                dataTextField: ["DirectionName", "SectorName", "SectorStructureItemName", "ObjectName"],
                dataUrlField: ["ObjectUrl"],
                dataImageUrlField: ["ImgUrl"],
                loadOnDemand: true,
                dataBound: function (e) {                    
                    var treeView = $(treeViewSelector).data("kendoTreeView");
                    var dataSource = $(treeViewSelector).data('kendoTreeView').dataSource;
                    
                    //Извлекаем из кук уникальные идентификаторы узлов открываемой ветки
                    var expandedNodes = $.session.get('ExpandedNodes');
                    //Конвертируем строку в массив
                    expandedNodes = expandedNodes ? expandedNodes.split(';') : [];
                    
                    var data = dataSource._data;

                    var node, searchItem;
                    var isCollapseAllIsDefined = $('#is-collapsed-all').attr('data-is-collapsed-all') !== undefined;
                    var isCollapseAll = false;
                    if (isCollapseAllIsDefined) {
                        isCollapseAll = $('#is-collapsed-all').attr('data-is-collapsed-all').toLowerCase() === "true";
                    }

                    if (data !== null && data !== undefined && expandedNodes.length > 0) {
                        //Берём очередной уникальный идентификатор узла ветки, которая должна быть открыта
                        var cur = expandedNodes.shift();
                        //Удаляем его из кук
                        updateTreeViewState(cur, "delete");
                        
                        //Если домашняя страница (не после обновления дат), то
                        //дерево не раскрывается
                        if (isCollapseAll) {
                            //Чистим куки
                            $.session.delete('ExpandedNodes');
                            return;
                        }

                        //Ищем элемент в дереве по его уникальному идентификатору
                        searchItem = searchInTreeData(data, cur, searchInTreeData);
                        //Если элемент найден
                        if (searchItem !== null && searchItem !== undefined) {
                            //Получаем html-реинкарнацию найденного элемента в дереве
                            node = treeView.findByUid(searchItem.uid);
                            //Открываем соответствующую ветку
                            treeView.expand(node);
                        }

                        //Получаем уникальный идентификатор выделенного элемента (обычно лист)
                        var selectedUnifiedTreeId = $.session.get('SelectedNode');
                        //Ищем выделенный элемент в дереве
                        var selectedNode = searchInTreeData(data, selectedUnifiedTreeId, searchInTreeData);

                        //Если элемент, который будет выделен, найден
                        if (selectedNode !== undefined && selectedNode !== null) {
                            //Получаем html-реинкарнацию найденного элемента в дереве
                            node = treeView.findByUid(selectedNode.uid);
                            //Выделяем найденный элемент
                            treeView.select(node);
                            
                            //Чистим куки
                            $.session.delete('ExpandedNodes');
                        }
                    }
                }
            });        
    },       
    
    //Функция поиска элемента в дереве
    //<param name="data">Данные из datasourse дерева</param>
    //<param name="nodeId">Единый идентификатор узла дерева</param>
    //<param name="selfHandle">Указатель на функцию поиска (на себя) для продолжения поиска среди вложенных узлов</param>
    searchInTreeData: function (data, nodeId, selfHandle) {
        var j;        
        for (j = 0; j < data.length; j++) {            
            if (nodeId === data[j].UnifiedTreeId) {                
                return data[j];
            }
            
            if (data[j].children._data !== undefined) {
                if (data[j].children._data.length > 0) {
                    return selfHandle(data[j].children._data, nodeId, selfHandle);
                }
            }
        }

        return null;
    },
    
    //Функция для сохранения/удаления единых идентификаторов узлов в сессии
    //<param name="id">Единый идентификатор узла дерева</param>
    //<param name="type">Тип операции</param>
    updateTreeViewState: function (id, type) {
        var expandedNodes = $.session.get('ExpandedNodes'); 
        expandedNodes = expandedNodes ? expandedNodes.split(';') : [];
        var itemIndex = $.inArray(id, expandedNodes);

        if (type == "add" && itemIndex == -1) {
            expandedNodes.push(id);                                                
        } else if (type == "delete" && itemIndex >= 0) {
            expandedNodes.splice(itemIndex, 1);
        }

        $.session.set('ExpandedNodes', expandedNodes.join(';'));    
   }
};