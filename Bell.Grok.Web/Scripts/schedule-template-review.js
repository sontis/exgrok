﻿//Функция, используемая kendo-templat-ом для вывода данных о
//рабочих дневных и ночных дней календаря
function buildCell(i, days, nights, holidays, monthNumber, dayTypes, areSplitted) {
    if (days[i] === undefined || days[i] === null) {
        if (monthNumber === -1 && i !== 0) {
            return "<table name=\"without-border-table\"></table>";
        } else {
            return "";
        }
    }

    var text = "<table>";
    var holidaysClassName = holidays[i] ? " holiday-cell\"" : "\"";
    if (days[i] === "") {
        days[i] = "&nbsp;";
    }
    if (nights[i] === "") {
        nights[i] = "&nbsp;";
    }
    if (dayTypes[i] === "") {
        dayTypes[i] = "&nbsp;";
    }
    if (areSplitted[i] === "") {
        areSplitted[i] = "&nbsp;";
    }

    text += "<tr><td class=\"pseudo-excel-cell" +
        holidaysClassName +
        ">" +
        dayTypes[i] +
        "</td></tr>" +
        "<tr><td class=\"pseudo-excel-cell" +
        holidaysClassName +
        ">" +
        days[i] +
        "</td></tr>" +
        "<tr><td  class=\"pseudo-excel-cell" +
        holidaysClassName +
        ">" +
        nights[i] +
        "</td></tr>" +
        "<tr><td  class=\"pseudo-excel-cell" +
        holidaysClassName +
        ">" +
        areSplitted[i] +
        "</td></tr>"

    text += "</table>";

    return text;
}

//Функция, используемая kendo-templat-ом для вывода данных о
//суммарных значениях рабочих дневных и ночных дней календаря
function buildTotalCell(hoursNum, nightHoursNum) {
    var text = "<table>";

    text += "<tr><td class=\"pseudo-excel-cell\">" +
        hoursNum +
        "</td></tr>" +
        "<tr><td  class=\"pseudo-excel-cell\">" +
        nightHoursNum +
        "</td></tr>";

    text += "</table>";

    return text;
}

var scheduleTemplateModeModule = function ($, scheduleTemplateModeId) {
    //Субмодуль для хранения url-ов
    var urlsModule = function () {
        var readUrl = $("#read-url").attr("data-url");

        return {
            readUrl: readUrl
        };
    }();

    //Субмодуль для хранения селекторов
    var selectorsModule = function () {
        var scheduleTemplateDetailesGrid = "#schedule-template-detailes-grid";

        return {
            scheduleTemplateDetailesGrid: scheduleTemplateDetailesGrid
        };
    }();

    //Субмодуль для хранения констант
    var constantsModule = function () {
        var scheduleTemplateModeIdStr = scheduleTemplateModeId.toString();

        return {
            scheduleTemplateModeId: scheduleTemplateModeIdStr
        };
    }();

    //Субмодуль источника данных для таблицы режима шаблона графиков кассиров
    var scheduleTemplateModeDataSourceModule = function (urls, constants) {
        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: urls.readUrl,
                    dataType: "json",
                    type: "POST"
                },
                parameterMap: function (options) {
                    return {
                        scheduleTemplateModeId: constants.scheduleTemplateModeId
                    };
                }
            },
            schema: {
                model: {
                    fields: {
                        MonthName: {},
                        WorkDaysNum: {},
                        Days: [],
                        Nights: [],
                        HoursNum: {},
                        HolidaysHoursNum: {}
                    }
                },
                data: "Items"
            },
            error: function () {
                if (!window.unloading) {
                    alert('При получении данных возникла ошибка');
                }
            }
        });

        return {
            dataSource: dataSource
        };
    }(urlsModule, constantsModule);

    //Субмодуль для таблицы режима шаблона графиков кассиров
    var scheduleTemplateGridModule = function (selectors, dataSource, constants) {
        var buildColumns = function () {
            var columns = [];

            columns.push({
                field: "MonthName",
                title: "Месяц",
                width: "70px"
            });
            columns.push({
                field: "WorkDaysNum",
                width: "70px",
                headerTemplate: "Рабочих<br>дней"
            });

            for (var i = 0; i < 31; i++) {
                columns.push({
                    title: (i + 1).toString(),
                    width: "50px",
                    template: "#= buildCell(" + i + ", Days, Nights, Holidays, MonthNumber, DayTypes, AreSplitted) #",
                });
            }

            columns.push({
                width: "60px",
                template: "#= buildTotalCell(HoursNum, NightHoursNum) #",
                title: "Часы",
            });
            columns.push({
                template: "#= buildTotalCell(HolidaysHoursNum, \"&nbsp;\") #",
                headerTemplate: "Праздничные<br>часы",
                width: "110px"
            });

            return columns;
        };

        var grid = $(selectors.scheduleTemplateDetailesGrid + "-" + constants.scheduleTemplateModeId).kendoGrid({
            dataSource: dataSource,
            columns: buildColumns(),
            resizable: true,
            dataBound: function (e) {
                $("td:has([name=without-border-table])").addClass("without-border-cell");
            }
        });
    }(selectorsModule, scheduleTemplateModeDataSourceModule.dataSource, constantsModule);
};

$(function () {
    $(".mode-number").each(function () {
        var scheduleTemplateModeId = $(this).attr("data-number");
        scheduleTemplateModeModule($, scheduleTemplateModeId);
    });
});