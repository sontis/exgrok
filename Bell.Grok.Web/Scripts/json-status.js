﻿JsonStatus = {
    ok: 1,
    invalid: 2,
    error: 3,
    unauthorized: 4
};