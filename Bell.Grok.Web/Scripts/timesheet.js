﻿$(function () {
    kendo.culture("ru-RU");

    //Инициализируем все субмодули
    var timesheetModule = TimesheetModule($);
    var urls = timesheetModule.urls();
    var constants = timesheetModule.constants();
    var selectors = timesheetModule.selectors();
    var datePickerWrapper = timesheetModule.datePicker(selectors, constants);
    var dataSourceWrapper = timesheetModule.dataSource(urls, constants, selectors);
    var gridWrapper = timesheetModule.grid(selectors, dataSourceWrapper.dataSource, constants, urls);
    var upload = timesheetModule.upload(selectors, urls, constants);
    var changeModePopup = timesheetModule.changeModePopup(selectors, constants);
    var employeeTable = timesheetModule.employeeTable(selectors, urls, constants);
    var employeePopup = timesheetModule.employeePopup(selectors, employeeTable);

    var handlers = timesheetModule.handlers(urls, constants, selectors, datePickerWrapper,
        dataSourceWrapper, gridWrapper, upload, changeModePopup,
        employeePopup);

    //На всякий случай блокируем событие отправки формы с датапикером для месяца
    $(selectors.timesheetSearchParamsForm).on("submit", function (e) {
        e.preventDefault();
    });

    datePickerWrapper.init();
    upload.init();

    //Устанавливаем обработчики событий
    $(selectors.generatePrepaymentButton).on("click", handlers.generateAllPrepayments);
    $(selectors.generateFullTimesheetButton).on("click", handlers.generateAllPrepayments);
    $(selectors.clearSecondTimesheetPartButton).on("click", handlers.clearSecondTimesheetPart);
    $(selectors.addEmployeeButton).on("click", handlers.addEmployee);
    $(selectors.exportEmployeeButton).on("click", handlers.exportTimesheet);
    $(selectors.compileTimesheetButton).on("click", handlers.showChangeModePopup);

    $(selectors.uploadContainer).hide();
    $(selectors.buttonsContainer).hide();

    $(selectors.changeModeButton).on("click", function (e) {
        $(selectors.changeModePopup).data("kendoWindow").close();
        var isAuto = $(selectors.modeRadioButton).filter(':checked').val() === "Auto";
        $(document).trigger(constants.changeModePopupClosedEventName, { isAuto: isAuto });
    });
    $(selectors.changeModeCancelButton).on("click", function (e) {
        $(selectors.changeModePopup).data("kendoWindow").close();
    });
    $(document).on(constants.changeModePopupClosedEventName, handlers.getTimesheet);
    //Проверка табеля проводится только при первой активизации вкладки
    $(document).on(constants.tabActivationEventName, handlers.checkTimesheet);
    $(document).on(constants.monthChangingEventName, handlers.checkTimesheet);
    $(document).on("click", selectors.deleteEmployeeButtons, handlers.deleteEmployee);
    $(document).on("click", selectors.generateIndividualPrepaymentButtons, handlers.generateIndividualPrepayments);
    $(document).on("click", selectors.generateIndividualFullTimesheetButtons, handlers.generateIndividualPrepayments);
    $(document).on("click", selectors.clearIndividualSecondTimesheetPartButtons, handlers.clearIndividualSecondTimesheetPart);
    $(document).on("click", selectors.cancelButtons, gridWrapper.hideOvertimeColumns);

    var buttonPanelViewModel = kendo.observable({
        areButtonsVisible: constants.areButtonsVisible,
        isUploadButtonVisible: constants.isUploadButtonVisible,
        exportClasses: function () {
            return this.areButtonsVisible
                ? "k-button button-left-indent"
                : "k-button";
        }
    });

    kendo.bind($(selectors.globalContainer), buttonPanelViewModel);

    //Если событие активации вкладки уже было, вызываем проверку наличия таймшита вручную        
    if ($(selectors.isTimesheetTabActivatedDiv).attr(constants.isActivatedAttr) === "1") {
        handlers.checkTimesheet();
    }
});

//Функция для построения ячейки с ФИО и должностью сотрудника
function buildEmployeeCell(fullname, position) {
    var result = "<table>" +
        "<tr>" +
        "<td rowspan=\"6\">" +
        fullname + ",<br>" + position +
        "</td>" +
        "</tr>" +
        "</table>";

    return result;
}

//Функция для построения ячейки с табельным номером сотрудника
function buildNumberCell(number) {
    var result = "<table>" +
        "<tr>" +
        "<td rowspan=\"6\">" +
        number +
        "</td>" +
        "</tr>" +
        "</table>";

    return result;
}

//Модуль данной страницы (частичного представления)
var TimesheetModule = function ($) {
    //Субмодуль с url-адресами
    var urlsModule = function () {
        var readUrl = $("#read-url").attr("data-url");
        var uploadFile = $("#upload-file").attr("data-url");
        var loadFile = $("#load-file").attr("data-url");
        var removeFile = $("#remove-file").attr("data-url");
        var getFiles = $("#get-files").attr("data-url");
        var checkTimesheetUrl = $("#check-timesheet-url").attr("data-url");
        var formTimesheetUrl = $("#form-timesheet-url").attr("data-url");
        var generatePrepaymentUrl = $("#generate-prepayment").attr("data-url");
        var generateIndividualPrepaymentsUrl = $("#generate-individual-prepayment").attr("data-url");
        var readEmployeesUrl = $("#read-employees").attr("data-url");
        var addEmployeeToTimesheetUrl = $("#add-employee-to-timesheet").attr("data-url");
        var deleteEmployeeFromTimesheetUrl = $("#delete-employee-from-timesheet").attr("data-url");
        var exportUrl = $("#export-timesheets").attr("data-url");
        var timesheetDetailsUrl = $("#get-timesheet-details").attr("data-url");
        var saveTimesheetDetailsUrl = $("#save-timesheet-details").attr("data-url");
        var timesheetPayrollsOrHookiesUrl = $("#get-timesheet-payrolls-or-hookies").attr("data-url");
        var clearSecondTimesheetPartUrl = $("#clear-second-timesheet-part").attr("data-url");
        var clearIndividualTimesheetPartUrl = $("#clear-individual-timesheet-part").attr("data-url");

        return {
            readUrl: readUrl,
            uploadFile: uploadFile,
            loadFile: loadFile,
            removeFile: removeFile,
            getFiles: getFiles,
            checkTimesheetUrl: checkTimesheetUrl,
            formTimesheetUrl: formTimesheetUrl,
            generatePrepaymentUrl: generatePrepaymentUrl,
            generateIndividualPrepaymentsUrl: generateIndividualPrepaymentsUrl,
            readEmployeesUrl: readEmployeesUrl,
            addEmployeeToTimesheetUrl: addEmployeeToTimesheetUrl,
            deleteEmployeeFromTimesheetUrl: deleteEmployeeFromTimesheetUrl,
            exportUrl: exportUrl,
            timesheetDetailsUrl: timesheetDetailsUrl,
            saveTimesheetDetailsUrl: saveTimesheetDetailsUrl,
            timesheetPayrollsOrHookiesUrl: timesheetPayrollsOrHookiesUrl,
            clearSecondTimesheetPartUrl: clearSecondTimesheetPartUrl,
            clearIndividualTimesheetPartUrl: clearIndividualTimesheetPartUrl
        };
    };

    //Субмодуль с константами
    var constantsModule = function () {
        var sectorId = $("#sector-id").attr("data-value");
        var areButtonsVisible = function () {
            return $("#are-buttons-visible").attr("data-are-visible").toLowerCase() === "true";
        }();
        var isUploadButtonVisible = function () {
            return $("#is-upload-button-visible").attr("data-is-visible").toLowerCase() === "true";
        }();
        var isDeleteUploadButtonVisible = function () {
            return $("#is-delete-upload-button-visible").attr("data-is-visible").toLowerCase() === "true";
        }();
        var successCreateMessage = "Файл успешно загружен";
        var fileErrorMessage = "Произошла ошибка во время загрузки";
        var emptyName = "Не введено название файла";
        var deleteFileError = "Произошла ошибка во время удаления файла";
        var loadFileError = "Произошла ошибка во время загрузки файла";
        var maskMessageCheck = "Проверка табеля...";
        var maskMessageForm = "Идёт составление табеля...";
        var maskMessageGeneratePrepayment = "Идёт генерация авансов...";
        var maskMessageGenerateFullTimesheet = "Идёт генерация полного табеля...";
        var maskMessageGenerateIndividualPrepayment = "Идёт генерация аванса...";
        var maskMessageGenerateIndividualFullTimesheet = "Идёт генерация индивидуального табеля...";
        var maskMessageAddEmployee = "Идёт добавление сотрудника в табель...";
        var maskMessageDeleteEmployee = "Идёт удаление сотрудника из табеля...";
        var maskMessageUpdateTimesheet = "Идёт сохранение данных...";
        var maskMessageClearSecondTimesheetPart = "Идёт сброс вторых половин табелей...";
        var maskMessageClearIndividualTimesheetPart = "Идёт сброс второй половины индивидуального табеля...";
        var maskMessageExportFile = "Формирование экспортного файла...";
        var maskMessageFileDelete = "Идёт удаление файла из базы данных...";
        var delayInterval = 500;
        var ajaxRequestErrorMessage = "get actions error";
        var changeModePopupClosedEventName = "changeModePopupClosed";        
        var tabActivationEventName = "timesheetTabActivated";
        var monthChangingEventName = "monthChanged";
        var isActivatedAttr = "data-is-activated";
        var timesheetHeaderText = "Табель учёта времени работы";
        var timesheetAbsentHeaderText = "Табель учёта времени работы за данный месяц для выбранного участка не составлен";        

        return {
            sectorId: sectorId,
            areButtonsVisible: areButtonsVisible,
            isUploadButtonVisible: isUploadButtonVisible,
            isDeleteUploadButtonVisible: isDeleteUploadButtonVisible,
            maskMessageCheck: maskMessageCheck,
            maskMessageForm: maskMessageForm,
            delayInterval: delayInterval,
            ajaxRequestErrorMessage: ajaxRequestErrorMessage,
            changeModePopupClosedEventName: changeModePopupClosedEventName,            
            tabActivationEventName: tabActivationEventName,
            successCreateMessage: successCreateMessage,
            fileErrorMessage: fileErrorMessage,
            emptyName: emptyName,
            deleteFileError: deleteFileError,
            loadFileError: loadFileError,
            monthChangingEventName: monthChangingEventName,
            maskMessageGeneratePrepayment: maskMessageGeneratePrepayment,
            maskMessageGenerateFullTimesheet: maskMessageGenerateFullTimesheet,
            maskMessageGenerateIndividualPrepayment: maskMessageGenerateIndividualPrepayment,
            maskMessageGenerateIndividualFullTimesheet: maskMessageGenerateIndividualFullTimesheet,
            maskMessageAddEmployee: maskMessageAddEmployee,
            maskMessageDeleteEmployee: maskMessageDeleteEmployee,
            isActivatedAttr: isActivatedAttr,
            maskMessageUpdateTimesheet: maskMessageUpdateTimesheet,
            maskMessageClearSecondTimesheetPart: maskMessageClearSecondTimesheetPart,
            maskMessageClearIndividualTimesheetPart: maskMessageClearIndividualTimesheetPart,
            maskMessageExportFile: maskMessageExportFile,
            maskMessageFileDelete: maskMessageFileDelete,
            timesheetHeaderText: timesheetHeaderText,
            timesheetAbsentHeaderText: timesheetAbsentHeaderText,
            
        };
    };

    //Субмодуль с селекторами
    var selectorsModule = function () {
        var timesheetMonth = "#timesheet-month";
        var timesheetsGrid = "#timesheets-grid";
        var uploader = "#uploader";
        var uploadFileButton = "#uploadButon";
        var deleteFileButton = ".deleteFileButton";
        var viewFileButton = ".viewFileButton";
        var uploadForm = "#uploadForm";
        var uploadFileName = "#uploadFileName";
        var uploadedFiles = "#uploadedFiles";
        var editFileColumn = "#editFileColumn";
        var globalContainer = "#timesheet-container";
        var changeModeButton = "#change-mode-button";
        var changeModeCancelButton = "#change-mode-cancel-button";
        var changeModePopup = "#change-mode-popup";
        var modeRadioButton = "input[name=GenerationDataMode]";
        var uploadContainer = "#upload-container";
        var generatePrepaymentButton = "#generate-prepayment-button";
        var buttonsContainer = "#buttons-container";
        var generateFullTimesheetButton = "#generate-full-timesheet-button";
        var clearSecondTimesheetPartButton = "#clear-second-timesheet-part-button";
        var generateIndividualPrepaymentButtons = ".generate-individual-prepayment";
        var generateIndividualFullTimesheetButtons = ".generate-individual-full-timesheet";
        var clearIndividualSecondTimesheetPartButtons = ".clear-individual-second-timesheet-part";
        var addEmployeeButton = "#add-employee";
        var employeePopup = "#employee-popup";
        var employeeSearchParamsForm = "#employee-search-params";
        var findEmployeeButton = "#find-employee";
        var employeeGrid = "#employee-grid";
        var chooseEmployeeButton = "#choose-employee";
        var currentTimesheetId = "#current-timesheet-id";
        var deleteEmployeeButtons = ".delete-employee";
        var isTimesheetTabActivatedDiv = "#is-timesheet-tab-activated";
        var exportEmployeeButton = "#export-employee";
        var timesheetDetailTemplate = "#timesheetDetailTemplate";
        var timesheetDaysGrid = ".timesheet-days-grid";
        var timesheetDaysGridContainer = "td.k-detail-cell";
        var timesheetPayrollGrid = ".timesheet-payroll-grid";
        var timesheetHookyGrid = ".timesheet-hooky-grid";
        var timesheetRowWithoutSchedule = "tr.k-master-row:not(:has(input:hidden.schedule-exists))";
        var timesheetRowWithSchedule = "tr.k-master-row:has(input:hidden.schedule-exists)";
        var compileTimesheetButton = "#compile-timesheet-button";
        var timesheetSearchParamsForm = "#timesheet-search-params";
        var cancelButtons = ".k-grid-cancel-changes";

        return {
            timesheetMonth: timesheetMonth,
            timesheetsGrid: timesheetsGrid,
            uploadFileButton: uploadFileButton,
            deleteFileButton: deleteFileButton,
            viewFileButton: viewFileButton,
            uploader: uploader,
            uploadForm: uploadForm,
            uploadFileName: uploadFileName,
            uploadedFiles: uploadedFiles,
            editFileColumn: editFileColumn,
            globalContainer: globalContainer,
            changeModeButton: changeModeButton,
            changeModeCancelButton: changeModeCancelButton,
            changeModePopup: changeModePopup,
            modeRadioButton: modeRadioButton,
            uploadContainer: uploadContainer,
            generatePrepaymentButton: generatePrepaymentButton,
            buttonsContainer: buttonsContainer,
            generateFullTimesheetButton: generateFullTimesheetButton,
            generateIndividualPrepaymentButtons: generateIndividualPrepaymentButtons,
            generateIndividualFullTimesheetButtons: generateIndividualFullTimesheetButtons,
            addEmployeeButton: addEmployeeButton,
            employeePopup: employeePopup,
            employeeSearchParamsForm: employeeSearchParamsForm,
            findEmployeeButton: findEmployeeButton,
            employeeGrid: employeeGrid,
            chooseEmployeeButton: chooseEmployeeButton,
            currentTimesheetId: currentTimesheetId,
            deleteEmployeeButtons: deleteEmployeeButtons,
            isTimesheetTabActivatedDiv: isTimesheetTabActivatedDiv,
            exportEmployeeButton: exportEmployeeButton,
            timesheetDetailTemplate: timesheetDetailTemplate,
            timesheetDaysGrid: timesheetDaysGrid,
            timesheetDaysGridContainer: timesheetDaysGridContainer,
            timesheetPayrollGrid: timesheetPayrollGrid,
            timesheetHookyGrid: timesheetHookyGrid,
            clearSecondTimesheetPartButton: clearSecondTimesheetPartButton,
            clearIndividualSecondTimesheetPartButtons: clearIndividualSecondTimesheetPartButtons,
            timesheetRowWithoutSchedule: timesheetRowWithoutSchedule,
            timesheetRowWithSchedule: timesheetRowWithSchedule,
            compileTimesheetButton: compileTimesheetButton,
            timesheetSearchParamsForm: timesheetSearchParamsForm,
            cancelButtons: cancelButtons
        };
    };

    //Субмодуль для контрола выбора месяца табеля
    var datePickerModule = function (selectors, constants) {
        var changeValue = function (e) {
            var timesheetDateInput = $(selectors.timesheetMonth);

            Utils.Validation.validateMonthDatePicker(timesheetDateInput, function () {
                //Учичтожаем контролы для табеля прошлого месяца
                var grid = $(selectors.timesheetsGrid).data("kendoGrid");

                if (grid) {
                    var dataSource = grid.dataSource;
                    dataSource._page = 1;
                    dataSource._skip = 0;

                    grid.destroy();
                    $(selectors.timesheetsGrid).empty();
                }

                $(selectors.uploadContainer).hide();
                $(selectors.buttonsContainer).hide();

                //Начинаем процесс проверки составления табеля сначала
                $(document).trigger(constants.monthChangingEventName);
            });
        };

        var defineValue = function () {
            //TODO: считывание значения месяца из кук

            var date = new Date();
            return date;
        };

        var init = function () {
            $(selectors.timesheetMonth).kendoDatePicker({
                start: "year",
                depth: "year",
                format: "MMMM yyyy",
                value: defineValue(),
                change: changeValue
            });
        };

        return {
            init: init
        };
    };

    //Субмодуль загрузчика файлов
    var uploadModule = function (selectors, urls, constants) {
        var init = function () {
            var maxRequestLength = parseInt($("#max-request-length").attr("data-length"));
            if (isNaN(maxRequestLength)) {
                maxRequestLength = 4096;
            }
            maxRequestLength *= 1024;

            //Всплыввающее окно для загрузки файла в список прявязанных к табелю
            var popup = $(selectors.uploadForm).kendoWindow({
                minWidth: "350px",
                minHeight: "230px",
                maxWidth: "350px",
                maxHeight: "230px",
                title: "Прикрепить файл",
                modal: true,
                visible: false,
                actions: [
                    "Close"
                ],
                close: function () {
                    $(".k-upload-files.k-reset").find("li").remove();
                    $(selectors.uploadFileName)[0].value = "";
                }
            }).data("kendoWindow");            

            //Кнопка добавления нового файла
            $(selectors.uploadFileButton).click(function () {
                $(selectors.uploadForm).data("kendoWindow").center();
                $(selectors.uploadForm).data("kendoWindow").open();
            });

            //Контрол загрузки файлов
            $(selectors.uploader).kendoUpload({
                async: {
                    saveUrl: urls.uploadFile,
                    autoUpload: true
                },
                localization: {
                    "select": "Загрузить",
                    "cancel": "Отмена",
                    "retry": "Повторить",
                    "remove": "Удалить",
                    "uploadSelectedFiles": "Загрузить",
                    "statusUploading": "загрузка",
                    "statusUploaded": "загружен",
                    "statusFailed": "произошла ошибка"
                },
                multiple: false,
                success: function (e) {
                    if (e.response && e.response.status === JsonStatus.ok) {
                        $(selectors.uploadForm).data("kendoWindow").close();
                        $(".k-upload-files.k-reset").find("li").remove();
                        $(".k-upload-status-total").remove();
                        $(selectors.uploadFileName)[0].value = "";
                        $(selectors.uploadedFiles).data("kendoGrid").dataSource.read();
                    }

                    if (e.response && (e.response.status === JsonStatus.error ||
                        e.response.status === JsonStatus.invalid)) {
                        $(".k-upload-files.k-reset").find("li").remove();
                        $(".k-upload-status-total").remove();
                        alert(e.response.message);
                    }
                },
                error: function (e) {
                    if (!window.unloading) {
                        if (e.XMLHttpRequest.responseText) {
                            if (!$.isBrowserInternetExplorer9orUnder()) {
                                var data = $($.parseHTML(e.XMLHttpRequest.responseText));
                                var isTitleFound = false;
                                data.each(function() {
                                    var element = $(this);
                                    if (element.length > 0 &&
                                        element[0].nodeName &&
                                        element[0].nodeName.toLowerCase() === "title") {
                                        isTitleFound = true;
                                        alert(element.text());
                                    }
                                });
                                
                                if (!isTitleFound) {
                                    alert(constants.fileErrorMessage);
                                }
                            } else {
                                var lines = e.XMLHttpRequest.responseText.split('\n');
                                for (var i = 0; i < lines.length; i++) {
                                    if (lines[i] !== "") {
                                        alert(lines[i]);
                                        break;
                                    }
                                }
                            }
                        }                                                                        
                    }
                },
                upload: function (e) {                    
                    ValidationHelper.clearErrors();
                    
                    if (e.files.length > 0) {
                        if (e.files[0].size &&
                            e.files[0].size > maxRequestLength) {
                            alert("Размер загружаемого файла не должен превышать 10 мегабайт");
                            e.preventDefault();
                            return;
                        }
                    }

                    var name = $(selectors.uploadFileName)[0].value;
                    if (!name) {
                        alert(constants.emptyName);
                        e.preventDefault();
                        return;
                    }

                    var monthDate = $(selectors.timesheetMonth).data("kendoDatePicker").value();

                    e.data = {
                        name: name,
                        sectorId: constants.sectorId,
                        year: monthDate.getFullYear(),
                        month: (1 + monthDate.getMonth())
                    };
                },
                select: function (e) {
                    var executedFormats =
                    [".bat",
                     ".com",
                     ".exe",
                     ".pif"];

                    var extension = "";
                    if (e.files.length > 0) {
                        extension = e.files[0].extension.toLowerCase();
                    }

                    if ($.inArray(extension, executedFormats) !== -1) {
                        alert("Расширение файла \"" + extension + "\" является недопустимым");
                        e.preventDefault();
                    }
                },
                remove: function () {
                },
                complete: function () {                    
                }
            });

            //Список привязанных к табелю файлов
            $(selectors.uploadedFiles).kendoGrid({
                dataSource: {
                    type: "json",
                    transport: {
                        read: {
                            url: urls.getFiles,
                            dataType: "json",
                            type: "POST",
                        },
                        parameterMap: function (options) {
                            var monthDate = $(selectors.timesheetMonth).data("kendoDatePicker").value();

                            return {
                                page: options.page,
                                pageSize: options.pageSize,
                                sectorId: constants.sectorId,
                                year: monthDate.getFullYear(),
                                month: (1 + monthDate.getMonth())
                            };
                        },
                    },
                    schema: {
                        model: {
                            fileId: "fileId",
                            fields: {
                                name: { editable: false },
                            }
                        },
                        data: "Items",
                        total: "Count"
                    },
                    serverPaging: true,
                    pageSize: 5,
                },
                autoBind: false,
                columns: [
                    { "template": kendo.template($(selectors.editFileColumn).html()), width: 80 },
                    { field: "name", title: "Название" }
                ],
                pageable: {
                    messages: {
                        display: "{0} - {1} из {2}",
                        empty: "Нет элементов для отображения",
                        page: "Страница",
                        of: "из {0}",
                        itemsPerPage: "элементов на страницу",
                        first: "К первой странице",
                        previous: "К предыдущей странице",
                        next: "К следующей странице",
                        last: "К последней странице",
                        refresh: "Обновить"
                    }
                },
                dataBound: function () {
                    $(selectors.deleteFileButton).click(function (e) {
                        var $tr = $(e.target).closest("tr");
                        var grid = $(selectors.uploadedFiles).data("kendoGrid");
                        var dataItem = grid.dataItem($tr);
                        var fileId = dataItem.fileId;

                        if (confirm('Вы действительно хотите удалить файл "' + dataItem.name + '" из списка прикреплённых к табелю?')) {
                            $.ajax({
                                url: urls.removeFile,
                                type: "POST",
                                dataType: 'json',
                                data: { fileId: fileId },
                                beforeSend: function () {
                                    //Показываем ajax-кругляшок на время выполнения запроса
                                    $(selectors.globalContainer)
                                        .mask(constants.maskMessageFileDelete,
                                            parseInt(constants.delayInterval));
                                },
                                success: function(response) {
                                    if (response.status === JsonStatus.ok) {
                                        var dataSource = $(selectors.uploadedFiles).data("kendoGrid").dataSource;
                                        var currentPage = $.calculateCurrentPage(dataSource);
                                        dataSource.page(currentPage);
                                    }

                                    if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                                        alert(response.message);
                                    }
                                },
                                error: function(e) {
                                    if (!window.unloading) {
                                        alert(constants.deleteFileError);
                                    }
                                },
                                complete: function () {                                  
                                    $(selectors.globalContainer)
                                        .unmask();
                                }
                            });
                        }
                    });

                    $(selectors.viewFileButton).click(function (e) {
                        var $tr = $(e.target).closest("tr");
                        var grid = $(selectors.uploadedFiles).data("kendoGrid");
                        var dataItem = grid.dataItem($tr);
                        var fileId = dataItem.fileId;

                        window.location = urls.loadFile + "?fileId=" + fileId;
                    });

                    if (!constants.isDeleteUploadButtonVisible) {
                        $(selectors.deleteFileButton).hide();
                    }
                },
            });
        };

        return {
            init: init
        };
    };

    //Субмодуль источника данных таблицы табелей
    var timesheetDataSourceModule = function (urls, constants, selectors) {
        var dataSource = new kendo.data.DataSource({
            type: "json",
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            sort: { field: "EmployeeFullName", dir: "asc" },
            pageSize: 5,
            transport: {
                read: {
                    url: urls.readUrl,
                    dataType: "json",
                    type: "POST",
                    contentType: 'application/json; charset=utf-8'
                },
                parameterMap: function (options) {
                    var monthDate = $(selectors.timesheetMonth).data("kendoDatePicker").value();
                    var monthDateStr = "";
                    if ((monthDate != null) && (!isNaN(monthDate))) {
                        monthDateStr = $.padStr(monthDate.getFullYear()) + "." + $.padStr(1 + monthDate.getMonth()) + "." + $.padStr(1);
                    }

                    options.monthDate = monthDateStr;
                    options.sectorId = parseInt(constants.sectorId);

                    return JSON.stringify(options);
                }
            },
            schema: {
                model: {
                    id: "TimesheetDetailId",
                    fields: {
                        EmployeeFullName: {},
                        EmployeePosition: {},
                        EmployeeNumber: {},
                        FirstHalfMonthDays: {},
                        FirstHalfMonthHours: {},
                        FirstHalfMonthNightHours: {},
                        SecondHalfMonthDays: {},
                        SecondHalfMonthHours: {},
                        SecondHalfMonthNightHours: {},
                        TotalMonthDays: {},
                        TotalMonthHours: {},
                        TotalMonthNightHours: {}
                    }
                },
                data: "Items",
                total: "Count"
            },
            requestEnd: function(e) {
                if (e.response &&
                    e.response.TimesheetId) {
                    $(selectors.currentTimesheetId).attr("data-id", e.response.TimesheetId);
                }
            },
            error: function () {
                if (!window.unloading) {
                    alert('При получении данных возникла ошибка');
                }
            }
        });

        return {
            dataSource: dataSource
        };
    };

    //Субмодуль таблицы табелей
    var timesheetGridModule = function (selectors, dataSource, constants, urls) {
        //Функция формирования массива колонок
        var buildColumns = function () {
            var columns = [];

            columns.push({
                width: "130px",
                field: "EmployeeFullName",
                headerTemplate: "ФИО,<br>должность",
                template: "#= buildEmployeeCell(EmployeeFullName, EmployeePosition) #"
            });
            columns.push({
                field: "EmployeeNumber",
                headerTemplate: "Табельный<br>номер",
                width: "110px",
                template: "#= buildNumberCell(EmployeeNumber) #"
            });
            columns.push({
                field: "FirstHalfMonthDays",
                headerTemplate: "Первая<br>половина<br>месяца<br>(дней)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                field: "FirstHalfMonthHours",
                headerTemplate: "Первая<br>половина<br>месяца<br>(часов)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                field: "FirstHalfMonthNightHours",
                headerTemplate: "Первая<br>половина<br>месяца<br>(ночных)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                field: "SecondHalfMonthDays",
                headerTemplate: "Вторая<br>половина<br>месяца<br>(дней)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                field: "SecondHalfMonthHours",
                headerTemplate: "Вторая<br>половина<br>месяца<br>(часов)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                field: "SecondHalfMonthNightHours",
                headerTemplate: "Вторая<br>половина<br>месяца<br>(ночных)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                field: "TotalMonthDays",
                headerTemplate: "Всего<br>(дней)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                field: "TotalMonthHours",
                headerTemplate: "Всего<br>(часов)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                field: "TotalMonthNightHours",
                headerTemplate: "Всего<br>(ночных)",
                filterable: false,
                sortable: false,
                width: "65px"
            });
            columns.push({
                template: kendo.template($("#editColumn").html()),
                width: "50px",
                sortable: false,
                filterable: false
            });

            return columns;
        };

        //Функция, которая находит объект модели PaymarkModel по коду (PaymarkCode)
        var findPaymarkByCode = function (paymarks, paymarkCode) {
            //Определяем код явки(неявки)
            var paymark = null;
            for (var j = 0; j < paymarks.length; j++) {
                if (paymarks[j].PayCode === paymarkCode) {
                    paymark = paymarks[j];
                    break;
                }
            }

            return paymark;
        };

        //Функция, которая находит код явки для другого числа месяца из этой же колонки
        var getSecondPaymark = function(data, dataElementIndex, columnIndex) {
            var secondPaymark = null;
            if (dataElementIndex === 0) {
                secondPaymark = findPaymarkByCode(data[3].Codes, data[3].Days[columnIndex]);
            }

            if (dataElementIndex === 3) {
                secondPaymark = findPaymarkByCode(data[0].Codes, data[0].Days[columnIndex]);
            }

            return secondPaymark;
        };

        //Кастомные эдиторы для кодов явок/неявок, а так же для количество общих и ночных часов
        var detailCellEditor = function (container, options) {
            //Достаём текущие данные грида
            var grid = $(container).closest(selectors.timesheetDaysGrid).data("kendoGrid");
            var dataDetailSource = grid.dataSource;
            var data = dataDetailSource.data();

            //Определяем номер индекса в списочном свойстве
            var indexRegObject = options.field.match(/\d+/);
            var index = parseInt(indexRegObject[0]);

            //Если индекс больше реального количества дней в этой половине месяца
            if (index + 1 > options.model.DaysCount) {
                return;
            }

            var paymark;
            var columnIndex = container[0].cellIndex;

            if (options.model.Codes.length > 0) {
                //Если это строка с кодами оплаты, то выкатываем выпадающий список
                if (indexRegObject.length > 0) {
                    $('<input data-bind="value:' + options.field + '">')
                        .appendTo(container)
                        .kendoDropDownList({
                            dataTextField: "PayMark",
                            dataValueField: "PayCode",
                            value: options.model.Days[index],
                            dataSource: options.model.Codes,
                            change: function (e) {
                                var value = parseInt(this.value());
                                var i, row, cell, span;

                                //Определяем код явки(неявки)
                                paymark = findPaymarkByCode(options.model.Codes, value);

                                //Если для этого кода не вводятся часы (выходной день)                                 
                                if (paymark && !paymark.IsAccount) {
                                    //сбрасываем значения часов в нули
                                    data[options.model.Index + 1].Days[index] = 0;
                                    data[options.model.Index + 2].Days[index] = 0;
                                    data[options.model.Index + 1].Overtimes[index] = 0;

                                    //И вообще не показываем часы
                                    data[options.model.Index + 1].DoesHoursInputHaveForDay[index] = false;
                                    data[options.model.Index + 2].DoesHoursInputHaveForDay[index] = false;

                                    //Очищаем текст в ячейках с часами и помечаем эти ячейки как изменённые
                                    for (i = 1; i < 3; i++) {
                                        row = (container).closest(selectors.timesheetDaysGrid + " tbody").find("tr:eq(" + (options.model.Index + i) + ")");
                                        cell = row.find("td:eq(" + columnIndex + ")");
                                        cell.prepend("<span class=\"k-dirty\"></span>");
                                        span = cell.find("span:last");
                                        span.removeClass("model-days");
                                        span.html("&nbsp;");
                                                                                
                                        cell = row.find("td:eq(" + (columnIndex + 1) + ")");
                                        span = cell.find("span:last");
                                        span.removeClass("model-overtimes");
                                    }
                                } else {
                                    //Показываем часы
                                    data[options.model.Index + 1].DoesHoursInputHaveForDay[index] = true;
                                    data[options.model.Index + 2].DoesHoursInputHaveForDay[index] = true;

                                    //Помечаем ячейки с часами как изменённые
                                    for (i = 1; i < 3; i++) {
                                        row = (container).closest(selectors.timesheetDaysGrid + " tbody").find("tr:eq(" + (options.model.Index + i) + ")");
                                        cell = row.find("td:eq(" + columnIndex + ")");
                                        cell.prepend("<span class=\"k-dirty\"></span>");
                                        span = cell.find("span:last");
                                        span.addClass("model-days");
                                        span.text(data[options.model.Index + i].Days[index]);
                                                                                
                                        cell = row.find("td:eq(" + (columnIndex + 1) + ")");
                                        span = cell.find("span:last");
                                        span.addClass("model-overtimes");
                                    }                                                                        
                                }
                                
                                //Если для этого кода вводятся сверхурочные часы
                                if (paymark && paymark.HasOvertime) {
                                    //Показываем невидимую колонку, содержащую ячейку для сверхурочных часов
                                    grid.showColumn((columnIndex + 1));
                                    //Этот день месяца содержит сверхурочные часы
                                    data[options.model.Index + 1].DoesOvertimeContain[index] = true;
                                    
                                    //Помечаем ячейку со сверхурочными, как изменённую
                                    row = (container).closest(selectors.timesheetDaysGrid + " tbody").find("tr:eq(" + (options.model.Index + 1) + ")");
                                    cell = row.find("td:eq(" + (columnIndex + 1) + ")");
                                    cell.prepend("<span class=\"k-dirty\"></span>");
                                    span = cell.find("span:last");                                    
                                    span.text(data[options.model.Index + 1].Overtimes[index]);
                                } else {
                                    //Для решения о сокрытии колонки со сверхурочными нужно проверить другое число месяца
                                    //из этой же колонки. Находим сначала код явки для другого числа месяца из этой же колонки                                    
                                    var secondPaymark = getSecondPaymark(data, options.model.Index, index);

                                    //Проверям коды явки обоих чисел месяца данной колонки
                                    if ((paymark &&
                                            paymark.HasOvertime) ||
                                        (secondPaymark &&
                                            secondPaymark.HasOvertime)) {
                                        grid.showColumn((columnIndex + 1));
                                    } else {
                                        //Если оба кода явки не содержат сверхурочных часов, то колонку со сверхурочными можно спрятать
                                        grid.hideColumn((columnIndex + 1));
                                    }
                                    
                                    //Этот день месяца не содержит сверхурочных часов
                                    data[options.model.Index + 1].DoesOvertimeContain[index] = false;
                                    
                                    //Помечаем ячейку со сверхурочными, как изменённую
                                    row = (container).closest(selectors.timesheetDaysGrid + " tbody").find("tr:eq(" + (options.model.Index + 1) + ")");
                                    cell = row.find("td:eq(" + (columnIndex + 1) + ")");
                                    cell.find("span.k-dirty").remove();
                                    span = cell.find("span:last");                                    
                                    span.html("&nbsp;");
                                }
                            }
                        });
                }
            } else {
                //Если это строка с количеством часов или ночных часов,
                //то выкатываем numeric text box
                var codeDataItem = options.model.Index < 3
                                        ? data[0]
                                        : data[3];

                //Определяем код явки(неявки)
                paymark = findPaymarkByCode(codeDataItem.Codes, codeDataItem.Days[index]);

                //Если для этого кода не вводятся часы (выходной день), то прерываем редактирование
                if (paymark &&
                    !paymark.IsAccount) {
                    return;
                } else {
                    //Иначе обновляем флаг показа числа для данного дня
                    data[options.model.Index].DoesHoursInputHaveForDay[index] = true;
                }
                                              
                var max = 24;
                var min = 0;
                
                //Значения сверхурочных есть только в строках с общими часами
                var overtimeValue = options.model.Overtimes[index];
                if (overtimeValue !== undefined && overtimeValue !== null) {
                    max -= overtimeValue;
                }

                if (options.model.IsTotalHours) {
                    //Устанавливаем минимум для общего количества часов - не меньше ночных часов
                    var currentNightHours = data[options.model.Index + 1].Days[index];
                    if (currentNightHours !== undefined && currentNightHours !== null) {
                        min = currentNightHours;
                    }
                } else {
                    //Устанавливаем максимум для ночных часов - не больше общего количества часов
                    var currentTotalHours = data[options.model.Index - 1].Days[index];
                    if (currentTotalHours !== undefined && currentTotalHours !== null) {
                        max = currentTotalHours;
                    }
                }

                $('<input type="text" required="required"' +
                    'data-required-msg="Значение не может быть неопределённым"' +
                    'data-type="number"' +
                    ' name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: "n",
                        min: min,
                        max: max,
                        decimals: 2,
                        step: 0.1,
                        spinners: false,
                        change: function (e) {
                            var value = this.value();

                            if (value > 0 &&
                                container.hasClass("grok-validation-td")) {
                                //Убираем невалидную подсветку и подсказку для ячейки
                                Utils.Validation.validateTimesheetDetailCell(value, container);
                            }
                        }
                    });
            }
        };
        
        //Кастомный эдитор для сверхурочных часов
        var detailOvertimeCellEditor = function (container, options) {
            //Достаём текущие данные грида
            var grid = $(container).closest(selectors.timesheetDaysGrid).data("kendoGrid");
            var dataDetailSource = grid.dataSource;
            var data = dataDetailSource.data();                                   

            //Определяем номер колонки
            var indexRegObject = options.field.match(/\d+/);
            var index = parseInt(indexRegObject[0]);
                        
            //Строка с кодами явок (неявок)
            var codeDataItem = options.model.Index < 3
                                    ? data[0]
                                    : data[3];

            //Определяем код явки(неявки)
            var paymark = findPaymarkByCode(codeDataItem.Codes, codeDataItem.Days[index]);

            //Если индекс больше реального количества дней в этой половине месяца или код явки - не сверхурочные
            if (index + 1 > options.model.DaysCount ||
                !options.model.DoesHoursInputHaveForDay[index] ||
                !paymark.HasOvertime) {
                return;
            }

            var max = 24 - options.model.Days[index];
            var min = 0;

            if (options.model.IsTotalHours === true) {
                $('<input type="text" required="required"' +
                    'data-required-msg="Значение не может быть неопределённым"' +
                    'data-type="number"' +
                    ' name="' + options.field + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: "n",
                        min: min,
                        max: max,
                        decimals: 2,
                        step: 0.1,
                        spinners: false,
                        change: function (e) {
                            var value = this.value();

                            if (value > 0 &&
                                container.hasClass("grok-validation-td")) {
                                //Убираем невалидную подсветку и подсказку для ячейки
                                Utils.Validation.validateTimesheetDetailCell(value, container);
                            }
                        }
                    });
            } else {
                return;
            }
        };

        var buildDetailColumns = function () {
            var columns = [];
            var headerTemplate, template, field, editor, hidden;

            for (var k = 0; k < 32; k++) {
                var i = Math.floor(k / 2);

                if ($.isEven(k)) {                    
                    template = "# if(Codes.length > 0) { #" +
                                        "# for (var j = 0; j < Codes.length; j++) { #" +
                                            "# if(Codes[j].PayCode === Days[" + i + "]) { #" +
                                                "<span>" + "#: Codes[j].PayMark #" + "</span>" +
                                            "# } else {#" +
                                            "# }#" +
                                        "# } #" +
                                        "# } else {#" +
                                            "# if(Days[" + i + "] === undefined ||" +
                                                 "DoesHoursInputHaveForDay[" + i + "] === false) { #" +
                                                "<span class=\"model-days\">" + "&nbsp;" + "</span>" +
                                            "# } else {#" +
                                                "<span class=\"model-days\">#: Days[" + i + "]# </span>" +
                                            "# }#" +
                                        "# }#";
                    field = "Days[" + i + "]";

                    if (i === 15) {
                        headerTemplate = "<div class=\"sub-th-top\">" + "X" + "</div>" +
                            "<div class=\"sub-th-bottom\">" + (i + 16) + "</div>";
                    } else {
                        headerTemplate = "<div class=\"sub-th-top\">" + (i + 1) + "</div>" +
                            "<div class=\"sub-th-bottom\">" + (i + 16) + "</div>";
                    }

                    editor = detailCellEditor;
                    hidden = false;
                } else {                    
                    template = "# if(Codes.length > 0) { #" +
                                        "<span>" + "&nbsp;" + "</span>" +
                                        "# } else {#" +
                                            "# if(IsTotalHours === true) { #" +
                                                "# if(Overtimes[" + i + "] === undefined ||" +
                                                 "DoesHoursInputHaveForDay[" + i + "] === false) { #" +
                                                    "<span class=\"model-overtimes\">" + "&nbsp;" + "</span>" +
                                                "# } else {#" +
                                                    "<span class=\"model-overtimes\">#: Overtimes[" + i + "]# </span>" +
                                                "# }#" +                                                
                                            "# } else {#" +
                                                "<span>" + "&nbsp;" + "</span>" +
                                            "# }#" +
                                        "# }#";
                    field = "Overtimes[" + i + "]";

                    if (i === 15) {
                        headerTemplate = "<div class=\"sub-th-top\">" + "X" + "</div>" +
                            "<div class=\"sub-th-bottom\">" + (i + 16).toString() + " / C" + "</div>";
                    } else {
                        headerTemplate = "<div class=\"sub-th-top\">" + (i + 1).toString() + " / C" + "</div>" +
                            "<div class=\"sub-th-bottom\">" + (i + 16).toString() + " / C" + "</div>";
                    }

                    editor = detailOvertimeCellEditor;
                    hidden = true;
                }

                columns.push({                    
                    editor: editor,
                    headerTemplate: headerTemplate,
                    template: template,
                    field: field,
                    hidden: hidden
                });
            }

            return columns;
        };

        var checkGenerateTimesheetButtons = function (detailRow) {
            var masterRow = detailRow.prev("tr.k-master-row");
            var hiddenElement = masterRow.find(":hidden.schedule-exists");
            if (hiddenElement.length === 0) {
                detailRow.find(selectors.generateIndividualPrepaymentButtons)
                    .remove();
                detailRow.find(selectors.generateIndividualFullTimesheetButtons)
                    .remove();
            }
        };

        var detailInit = function (e) {
            var detailRow = e.detailRow;

            var detailDataSource = new kendo.data.DataSource({
                type: "json",
                transport: {
                    read: {
                        url: urls.timesheetDetailsUrl,
                        dataType: "json",
                        type: "POST"
                    },
                    update: {
                        url: urls.saveTimesheetDetailsUrl,
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        beforeSend: function () {
                            //Показываем ajax-кругляшок на время выполнения запроса                                
                            $(detailRow).find(selectors.timesheetDaysGridContainer)
                                        .mask(constants.maskMessageUpdateTimesheet,
                                              parseInt(constants.delayInterval));
                        },
                        complete: function () {
                            var parentTable = $(selectors.timesheetsGrid).data("kendoGrid");
                            parentTable.dataSource.read();
                            //Расширяем kendo-grid дополнительным свойством, хранящим номер текущей
                            //отредактированной строки, которую необходимо будет раскрыть после 
                            //обновления основной таблицы                                       
                            parentTable.currentEditedRowIndex = $("tr.k-master-row").index(detailRow.prev("tr.k-master-row"));
                            parentTable.currentEditedPage = parentTable.dataSource.page();

                            //Убираем ajax-кругляшок
                            $(detailRow).find(selectors.timesheetDaysGridContainer)
                                        .unmask();
                        }
                    },
                    parameterMap: function (options, operation) {
                        if (operation === "read") {
                            return {
                                //Идентификатор табеля сотрудника
                                timesheetDetailId: e.data.TimesheetDetailId
                            };
                        } else if (options.models) {
                            var rows = detailRow.find(selectors.timesheetDaysGrid)
                                                .data("kendoGrid").dataSource.data();
                            //Перед отправкой убираем из данных грида значения справочников с кодами явок и неявок
                            var clone = rows.slice(0);
                            clone[0].Codes = " ";
                            clone[3].Codes = " ";                                                        

                            return JSON.stringify(clone);
                        } else {
                            return null;
                        }
                    }
                },
                batch: true,
                schema: {
                    model: {
                        id: "Index",
                        fields: {
                            Days: [],
                            Codes: [],
                            Overtimes: []
                        }
                    },
                    data: "Items",
                    total: "Count"
                },
                requestEnd: function (event) {
                    if (event.response !== undefined &&
                        event.response.status !== undefined &&
                        event.response.status === JsonStatus.error) {
                        alert(event.response.message);
                    }
                },
                error: function (event) {
                    if (!window.unloading) {
                        alert(event.errorThrown);
                    }
                }
            });

            var payrollDataSource = new kendo.data.DataSource({
                type: "json",
                pageSize: 3,
                transport: {
                    read: {
                        url: urls.timesheetPayrollsOrHookiesUrl,
                        dataType: "json",
                        type: "POST"
                    },
                    parameterMap: function () {
                        return {
                            //Идентификатор табеля сотрудника
                            timesheetDetailId: e.data.TimesheetDetailId,
                            isPresence: true
                        };
                    }
                },
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            Code: {},
                            HoursNumber: {},
                            DaysNumber: {}
                        }
                    },
                    data: "Items",
                    total: "Count"
                },
                error: function (event) {
                    if (!window.unloading) {
                        alert(event.errorThrown);
                    }
                }
            });

            var hookyDataSource = new kendo.data.DataSource({
                type: "json",
                pageSize: 3,
                transport: {
                    read: {
                        url: urls.timesheetPayrollsOrHookiesUrl,
                        dataType: "json",
                        type: "POST"
                    },
                    parameterMap: function () {
                        return {
                            //Идентификатор табеля сотрудника
                            timesheetDetailId: e.data.TimesheetDetailId,
                            isPresence: false
                        };
                    }
                },
                schema: {
                    model: {
                        id: "Id",
                        fields: {
                            Code: {},
                            HoursNumber: {},
                            DaysNumber: {}
                        }
                    },
                    data: "Items",
                    total: "Count"
                },
                error: function (event) {
                    if (!window.unloading) {
                        alert(event.errorThrown);
                    }
                }
            });

            detailRow.find(selectors.timesheetDaysGrid).kendoGrid({
                dataSource: detailDataSource,
                columns: buildDetailColumns(),
                editable: constants.areButtonsVisible,
                navigatable: true,
                toolbar: [{ name: "save", text: "Сохранить" },
                           { name: "cancel", text: "Отмена" },
                           { template: kendo.template($("#generate-individual-timesheet-buttons").html()) }],
                dataBound: function () {
                    //Делаем границы между строками ярче
                    detailRow.find(selectors.timesheetDaysGrid + " tbody tr:eq(0) td").addClass("bottom-border");
                    detailRow.find(selectors.timesheetDaysGrid + " tbody tr:eq(1) td").addClass("bottom-border");
                    detailRow.find(selectors.timesheetDaysGrid + " tbody tr:eq(3) td").addClass("bottom-border");
                    detailRow.find(selectors.timesheetDaysGrid + " tbody tr:eq(4) td").addClass("bottom-border");
                    //Убираем разделитель справа в заголовке таблицы
                    detailRow.find(selectors.timesheetDaysGrid + " th:last div").addClass("header-right-border");
                    
                    //Проверка на необходимость отображения изначально невидимых колонок для тех кодов оплаты,
                    //которые содержат сверхурочные часы
                    var data = this.dataSource.data();
                    var firsthalfMonthDaysCount = data[0].Days.length;
                    var paymark;
                    for (var i = 0; i < firsthalfMonthDaysCount; i++) {
                        paymark = findPaymarkByCode(data[0].Codes, data[0].Days[i]);
                        if (paymark && paymark.HasOvertime) {
                            this.showColumn(i * 2 + 1);
                        }
                    }

                    var secondhalfMonthDaysCount = data[3].Days.length;
                    for (i = 0; i < secondhalfMonthDaysCount; i++) {
                        paymark = findPaymarkByCode(data[3].Codes, data[3].Days[i]);
                        if (paymark && paymark.HasOvertime) {
                            this.showColumn(i * 2 + 1);
                        }
                    }

                    //Делаем первую ячейку таблицы доступной для редактирования (устанавливаем фокус для клавишной навигации)
                    //Только для ролей, имеющих права на редактирование
                    if (constants.areButtonsVisible) {
                        var firstDetailTableCell = detailRow.find("td[role=gridcell]").first();
                        detailRow.find(selectors.timesheetDaysGrid).data("kendoGrid").editCell(firstDetailTableCell);
                    }
                },
                saveChanges: function (e) {
                    var data = this.dataSource.data();
                    var isValid = true;
                    var i, row, cell, paymark;

                    //Проверка на недопущение ввода нулевых часов.
                    if (data) {
                        var firsthalfMonthDaysCount = data[0].Days.length;
                        for (i = 0; i < firsthalfMonthDaysCount; i++) {
                            paymark = findPaymarkByCode(data[0].Codes, data[0].Days[i]);
                            if (paymark &&
                                paymark.IsAccount &&
                                data[1].Days[i] === 0) {
                                row = this.tbody.find("tr:eq(" + 1 + ")");
                                cell = row.find("td:has(span.model-days):eq(" + i + ")");

                                //Делаем подсветку и всплывающую подсказку для невалидной ячейки
                                Utils.Validation.validateTimesheetDetailCell(data[1].Days[i], cell);

                                isValid = false;
                            }
                            
                            if (paymark &&
                                paymark.HasOvertime &&
                                data[1].Overtimes[i] === 0) {
                                row = this.tbody.find("tr:eq(" + 1 + ")");
                                cell = row.find("td:has(span.model-overtimes):eq(" + i + ")");
                                
                                //Делаем подсветку и всплывающую подсказку для невалидной ячейки
                                Utils.Validation.validateTimesheetDetailCell(data[1].Overtimes[i], cell);

                                isValid = false;
                            }
                        }

                        var secondhalfMonthDaysCount = data[3].Days.length;
                        for (i = 0; i < secondhalfMonthDaysCount; i++) {
                            paymark = findPaymarkByCode(data[3].Codes, data[3].Days[i]);
                            if (paymark &&
                                paymark.IsAccount &&
                                data[4].Days[i] === 0) {
                                row = this.tbody.find("tr:eq(" + 4 + ")");
                                cell = row.find("td:has(span.model-days):eq(" + i + ")");

                                //Делаем подсветку и всплывающую подсказку для невалидной ячейки
                                Utils.Validation.validateTimesheetDetailCell(data[4].Days[i], cell);

                                isValid = false;
                            }
                            
                            if (paymark &&
                                paymark.HasOvertime &&
                                data[4].Overtimes[i] === 0) {
                                row = this.tbody.find("tr:eq(" + 4 + ")");
                                cell = row.find("td:has(span.model-overtimes):eq(" + i + ")");

                                //Делаем подсветку и всплывающую подсказку для невалидной ячейки
                                Utils.Validation.validateTimesheetDetailCell(data[4].Overtimes[i], cell);

                                isValid = false;
                            }
                        }

                        if (!isValid) {
                            alert("Введите ненулевое количество часов");
                            e.preventDefault();
                        }
                    }
                }
            });

            //Проверяем, необходимо ли отображать кнопки на тулбаре                        
            if (!constants.areButtonsVisible) {
                detailRow.find("a.k-button").hide();
            }

            detailRow.find(selectors.timesheetPayrollGrid).kendoGrid({
                dataSource: payrollDataSource,
                columns: [{ headerTemplate: "Код<br>оплаты", field: "Code" },
                          { title: "Часов", field: "HoursNumber" },
                          { title: "Дней", field: "DaysNumber" }],
                pageable: {
                    messages: {
                        display: "{0} - {1} из {2}",
                        empty: "Нет элементов для отображения",
                        page: "Страница",
                        of: "из {0}",
                        itemsPerPage: "элементов на страницу",
                        first: "К первой странице",
                        previous: "К предыдущей странице",
                        next: "К следующей странице",
                        last: "К последней странице",
                        refresh: "Обновить"
                    }
                },
                dataBound: function () {
                    var data = this.dataSource.data();

                    //Добавляем к кодам из одной цифры ноль
                    for (var i = 0; i < data.length; i++) {
                        var row = this.tbody.find("tr:eq(" + i + ")");
                        var cell = row.find("td:eq(0)");
                        var text = $.trim(cell.text());
                        if (text.length === 1) {
                            cell.text("0" + text);
                        }
                    }
                }
            });

            detailRow.find(selectors.timesheetHookyGrid).kendoGrid({
                dataSource: hookyDataSource,
                columns: [{ headerTemplate: "Код<br>неявок", field: "Code" },
                          { title: "Часов", field: "HoursNumber" },
                          { title: "Дней", field: "DaysNumber" }],
                pageable: {
                    messages: {
                        display: "{0} - {1} из {2}",
                        empty: "Нет элементов для отображения",
                        page: "Страница",
                        of: "из {0}",
                        itemsPerPage: "элементов на страницу",
                        first: "К первой странице",
                        previous: "К предыдущей странице",
                        next: "К следующей странице",
                        last: "К последней странице",
                        refresh: "Обновить"
                    }
                },
                dataBound: function () {
                    var data = this.dataSource.data();

                    //Добавляем к кодам из одной цифры ноль
                    for (var i = 0; i < data.length; i++) {
                        var row = this.tbody.find("tr:eq(" + i + ")");
                        var cell = row.find("td:eq(0)");
                        var text = $.trim(cell.text());
                        if (text.length === 1) {
                            cell.text("0" + text);
                        }
                    }
                }
            });

            checkGenerateTimesheetButtons(detailRow);
        };

        //Обработчик кнопки "Отмена" на панели задач вложенной таблицы
        //Скрывает колонки, которые до внесения изменений не содержали 
        //сверхурочных часов
        var hideOvertimeColumns = function (e) {
            var grid = $(e.target).closest("div.k-grid")
                                  .data("kendoGrid");
            var data = grid.dataSource.data();

            var codes = data[0].Codes;

            for (var i = 0; i < 16; i++) {
                var value = parseInt(data[0].Days[i]);
                var secondValue = parseInt(data[3].Days[i]);

                //Определяем коды явки(неявки)
                var paymark = findPaymarkByCode(codes, value);
                var secondPaymark = findPaymarkByCode(codes, secondValue);

                if (!(paymark &&
                        paymark.HasOvertime) &&
                    !(secondPaymark &&
                        secondPaymark.HasOvertime)) {
                    grid.hideColumn(i * 2 + 1);
                }                
            }
        };

        var init = function () {
            $(selectors.timesheetsGrid).kendoGrid({
                dataSource: dataSource,
                columns: buildColumns(),
                resizable: true,
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "содержит"
                        }
                    },
                    messages: {
                        info: "Элементы со значением",
                        filter: "Применить",
                        clear: "Сбросить"
                    }
                },
                pageable: {
                    messages: {
                        display: "{0} - {1} из {2}",
                        empty: "Нет элементов для отображения",
                        page: "Страница",
                        of: "из {0}",
                        itemsPerPage: "элементов на страницу",
                        first: "К первой странице",
                        previous: "К предыдущей странице",
                        next: "К следующей странице",
                        last: "К последней странице",
                        refresh: "Обновить"
                    }
                },
                detailTemplate: kendo.template($(selectors.timesheetDetailTemplate).html()),
                detailInit: detailInit,
                dataBound: function (e) {
                    //Подсвечиваем сотрудников, у которых нет планового графика на этот месяц
                    $(selectors.timesheetRowWithoutSchedule).addClass('rowBlocked');
                    $(selectors.timesheetRowWithoutSchedule).attr('title', 'У сотрудника отсутствует плановый график за этот месяц');
                    $(selectors.timesheetRowWithSchedule).attr('title', 'Для сотрудника составлен плановый график за этот месяц');

                    //Раскрываем отредактированную строчку                         
                    if (this.currentEditedRowIndex !== undefined &&
                        this.currentEditedRowIndex !== null &&
                        this.currentEditedPage === this.dataSource.page()) {
                        this.expandRow("tr.k-master-row:eq(" + this.currentEditedRowIndex + ")");
                    }
                    
                    var timesheetId = parseInt($(selectors.currentTimesheetId).attr("data-id"));

                    //Устанавливаем необходимый атрибут(номер табеля) на кнопку 'Экспорт'
                    if (timesheetId !== NaN) {
                        var exportButton = $(selectors.exportEmployeeButton);
                        exportButton.attr("timesheetId", timesheetId);
                    }

                    //Проверяем, необходимо ли отображать кнопки для удаления
                    if (!constants.areButtonsVisible) {
                        $(selectors.deleteEmployeeButtons).hide();
                    }
                }
            });
        };

        return {
            init: init,
            hideOvertimeColumns: hideOvertimeColumns
        };
    };

    //Субмодуль для всплывающего окна с вопросов о режиме генерации табелей
    var changeModePopupModule = function (selectors, constants) {
        var window = $(selectors.changeModePopup).kendoWindow({
            actions: {},
            minWidth: "500px",
            minHeight: "120px",
            maxWidth: "500px",
            maxHeight: "120px",
            title: "Выбор режима заполнения табеля",
            modal: true,
            visible: false
        }).data("kendoWindow");

        return {
            window: window
        };
    };

    //Субмодуль для всплывающего окна выбора сотрудника для добавления в табель
    var employeePopupModule = function (selectors, employeeTable) {
        //Функция, запускающая kendo-window
        var showWindow = function () {
            if (!$(selectors.employeePopup).data("kendoWindow")) {
                $(selectors.employeePopup).show();
                $(selectors.employeePopup).kendoWindow({
                    width: "900px",
                    height: "650px",
                    title: "Сотрудники",
                    modal: true,
                    close: function (e) {
                        var grid = $(selectors.employeeGrid).data("kendoGrid");
                        if (grid) {
                            var dataSource = grid.dataSource;
                            dataSource._page = 1;
                            dataSource._skip = 0;

                            grid.destroy();
                            $(selectors.employeeGrid).empty();
                        }
                    }
                });
            } else {
                $(selectors.employeePopup).data("kendoWindow").open();
            }

            $(selectors.chooseEmployeeButton).attr('disabled', 'disabled');
            employeeTable.initTable();
        };

        return {
            showWindow: showWindow
        };
    };

    //Субмодуль источника данных и таблицы работников
    var employeeTableModule = function (selectors, urls, constants) {
        $(selectors.findEmployeeButton).on("click", function () {
            var dataSource = $(selectors.employeeGrid).data("kendoGrid").dataSource;
            dataSource.page(1);
        });

        //Обработчик выбора сотрудника в таблице 
        $(selectors.chooseEmployeeButton).on("click", function () {
            var employeeGrid = $(selectors.employeeGrid).data("kendoGrid");
            var employeeGridSelectedItem = employeeGrid.select();
            var employeeDataItem = employeeGrid.dataItem(employeeGridSelectedItem);
            var timesheetGrid = $(selectors.timesheetsGrid).data("kendoGrid");
            var timesheetId = parseInt($(selectors.currentTimesheetId).attr("data-id")); 

            $.ajax({
                url: urls.addEmployeeToTimesheetUrl,
                type: 'POST',
                beforeSend: function () {
                    //Показываем ajax-кругляшок на время выполнения запроса
                    $(selectors.globalContainer)
                        .mask(constants.maskMessageAddEmployee,
                            parseInt(constants.delayInterval));
                },
                data: {
                    timesheetId: timesheetId,
                    employeeId: employeeDataItem.EmployeeId
                },
                success: function (response) {
                    //Новый пользователь в табеле
                    if (response.status === JsonStatus.ok) {
                        //После добавления сотрудника раскрывать строки в гриде не нужно
                        timesheetGrid.currentEditedRowIndex = null;
                        timesheetGrid.currentEditedPage = null;

                        $(selectors.employeePopup).data("kendoWindow").close();
                        timesheetGrid.dataSource.read();
                        Utils.Notification.show('Сотрудник ' + response.result.employeeName + ' добавлен в табель');
                    }

                    if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                        alert(response.message);
                    }
                },
                error: function () {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function () {
                    $(selectors.globalContainer)
                        .unmask();
                }
            });
        });

        //Функция, инициализирующая таблицу
        var initTable = function () {
            var dataSource = new kendo.data.DataSource({
                type: "json",
                serverPaging: true,
                serverSorting: true,
                sort: { field: "EmployeeNumber", dir: "asc" },
                pageSize: 10,
                change: function () {
                    $(selectors.chooseEmployeeButton).addClass('k-state-disabled');
                    $(selectors.chooseEmployeeButton).attr('disabled', 'disabled');
                },
                transport: {
                    read: {
                        url: urls.readEmployeesUrl,
                        dataType: "json",
                        type: "POST"
                    },
                    parameterMap: function (options) {                                                
                        var timesheetId = parseInt($(selectors.currentTimesheetId).attr("data-id"));

                        return {
                            page: options.page,
                            pageSize: options.pageSize,
                            orderby: (options.sort.length > 0) ? options.sort[0].field + ' ' + options.sort[0].dir : "",
                            filter: $(selectors.employeeSearchParamsForm).serialize(),
                            timesheetId: timesheetId
                        };
                    }
                },
                schema: {
                    model: {
                        id: "EmployeeId",
                        fields: {
                            EmployeeId: {},
                            EmployeeNumber: {},
                            EmployeeFirstName: {},
                            EmployeeMiddleName: {},
                            EmployeeLastName: {},
                            Sector: {},
                            Position: {},
                            Description: {}
                        }
                    },
                    data: "Items",
                    total: "Count"
                },
                error: function () {
                    if (!window.unloading) {
                        alert('При получении данных возникла ошибка');
                    }
                }
            });

            $(selectors.employeeGrid).kendoGrid({
                scrollable: false,
                dataSource: dataSource,
                sortable: {
                    mode: "single",
                    allowUnsort: false
                },
                selectable: "row",
                change: function () {
                    $(selectors.chooseEmployeeButton).removeClass('k-state-disabled');
                    $(selectors.chooseEmployeeButton).removeAttr('disabled');
                },
                pageable: {
                    messages: {
                        display: "{0} - {1} из {2}",
                        empty: "Нет элементов для отображения",
                        page: "Страница",
                        of: "из {0}",
                        itemsPerPage: "элементов на страницу",
                        first: "К первой странице",
                        previous: "К предыдущей странице",
                        next: "К следующей странице",
                        last: "К последней странице",
                        refresh: "Обновить"
                    }
                },
                columns: [
                    { field: "EmployeeNumber", title: "Табельный номер" },
                    { field: "EmployeeFirstName", title: "Имя" },
                    { field: "EmployeeMiddleName", title: "Отчество" },
                    { field: "EmployeeLastName", title: "Фамилия" },
                    { field: "Sector", title: "Производственный участок" },
                    { field: "Position", title: "Должность" }]
            }).data("kendoGrid");
        };

        return {
            initTable: initTable
        };
    };

    //Субмодуль обработчков событий
    var handlersModule = function (urls, constants, selectors, datePickerWrapper,
        dataSourceWrapper, gridWrapper, upload, changeModePopup,
        employeePopup) {

        //Обработчик составления табеля (если его не было)
        var getTimesheet = function (e, data) {
            var isAuto = data.isAuto;

            $.ajax({
                url: urls.formTimesheetUrl,
                type: 'POST',
                beforeSend: function () {
                    //Показываем ajax-кругляшок на время выполнения запроса
                    $(selectors.globalContainer)
                        .mask(constants.maskMessageForm,
                            parseInt(constants.delayInterval));
                },
                data: {
                    sectorId: constants.sectorId,
                    monthDate: $(selectors.timesheetMonth).val(),
                    isAuto: isAuto
                },
                success: function (response) {
                    //Таки табель составлен
                    if (response.status === JsonStatus.ok) {
                        if (response.message) {
                            alert(response.message);
                        }

                        gridWrapper.init();
                        $(selectors.uploadContainer).show();
                        $(selectors.buttonsContainer).show();

                        //Скрываем кнопку "Составить табель"
                        $(selectors.compileTimesheetButton).hide();
                        $(selectors.globalContainer + " h3").text(constants.timesheetHeaderText);
                    }

                    if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                        alert(response.message);
                    }
                },
                error: function () {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function () {
                    //При дальнейших сменах вкладки нам не надо проверять табель
                    $(document).off(constants.tabActivationEventName);

                    $(selectors.globalContainer)
                        .unmask();
                }
            });
        };

        //Обработчик нажатия на кнопку "Составить табель"
        var showChangeModePopup = function (e) {            
            changeModePopup.window.center();
            changeModePopup.window.open();
        };

        //Обработчик проверки наличия табеля
        var checkTimesheet = function (e) {
            //Проверка табеля
            $.ajax({
                url: urls.checkTimesheetUrl,
                type: 'POST',
                beforeSend: function () {
                    //Показываем ajax-кругляшок на время выполнения запроса
                    $(selectors.globalContainer)
                        .mask(constants.maskMessageCheck,
                            parseInt(constants.delayInterval));
                },
                data: {
                    sectorId: constants.sectorId,
                    monthDate: $(selectors.timesheetMonth).val()
                },
                success: function (response) {
                    if (response.status === JsonStatus.ok) {
                        //Если табеля нет, то показываем кнопку "Составить табель"
                        if (!response.result.isExists) {
                            //Если у пользователя есть права на составление табеля
                            if (constants.areButtonsVisible) {
                                $(selectors.compileTimesheetButton).show();
                            }
                            $(selectors.globalContainer + " h3").text(constants.timesheetAbsentHeaderText);                            
                        } else {
                            //Иначе просто показываем
                            gridWrapper.init();
                            $(selectors.uploadContainer).show();
                            $(selectors.buttonsContainer).show();
                            //Запрашиваем список файлов
                            $(selectors.uploadedFiles).data("kendoGrid").dataSource.read();
                            
                            //Скрываем кнопку "Составить табель"
                            $(selectors.compileTimesheetButton).hide();
                            $(selectors.globalContainer + " h3").text(constants.timesheetHeaderText);
                        }
                    }

                    if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                        alert(response.message);
                    }
                },
                error: function () {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function () {
                    $(selectors.globalContainer)
                        .unmask();
                    
                    $(document).off(constants.tabActivationEventName, checkTimesheet);
                }
            });
        };

        //Функция для запроса генерации табелей из графиков 
        var generateAllPrepayments = function (e) {
            var message, isPrepayment;
            if ("#" + e.target.id === selectors.generatePrepaymentButton) {
                message = constants.maskMessageGeneratePrepayment;
                isPrepayment = true;
            } else if ("#" + e.target.id === selectors.generateFullTimesheetButton) {
                message = constants.maskMessageGenerateFullTimesheet;
                isPrepayment = false;
            } else {
                e.preventDefault();
                return;
            }

            //Генерация авансов
            $.ajax({
                url: urls.generatePrepaymentUrl,
                type: 'POST',
                beforeSend: function () {
                    //Показываем ajax-кругляшок на время выполнения запроса
                    $(selectors.globalContainer)
                        .mask(message,
                            parseInt(constants.delayInterval));
                },
                data: {
                    sectorId: constants.sectorId,
                    monthDate: $(selectors.timesheetMonth).val(),
                    isPrepayment: isPrepayment
                },
                success: function (response) {
                    if (response.status === JsonStatus.ok) {
                        var grid = $(selectors.timesheetsGrid).data("kendoGrid");
                        grid.currentEditedRowIndex = null;
                        grid.currentEditedPage = null;

                        if (grid) {
                            var datasource = grid.dataSource;
                            datasource.read();
                        }
                    }

                    if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                        alert(response.message);
                    }
                },
                error: function () {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function () {
                    $(selectors.globalContainer)
                        .unmask();
                }
            });
        };

        var clearSecondTimesheetPart = function (e) {
            $.ajax({
                url: urls.clearSecondTimesheetPartUrl,
                type: 'POST',
                beforeSend: function () {
                    //Показываем ajax-кругляшок на время выполнения запроса
                    $(selectors.globalContainer)
                        .mask(constants.maskMessageClearSecondTimesheetPart,
                            parseInt(constants.delayInterval));
                },
                data: {
                    sectorId: constants.sectorId,
                    monthDate: $(selectors.timesheetMonth).val()
                },
                success: function (response) {
                    if (response.status === JsonStatus.ok) {
                        var grid = $(selectors.timesheetsGrid).data("kendoGrid");
                        grid.currentEditedRowIndex = null;
                        grid.currentEditedPage = null;

                        if (grid) {
                            var datasource = grid.dataSource;
                            datasource.read();
                        }
                    }

                    if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                        alert(response.message);
                    }
                },
                error: function () {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function () {
                    $(selectors.globalContainer)
                        .unmask();
                }
            });
        };

        //Функция для запроса генерации индивидуально табеля из графика
        var generateIndividualPrepayments = function (e) {
            e.preventDefault();
            var message, isPrepayment;

            if (e.currentTarget.className.indexOf(selectors.generateIndividualPrepaymentButtons.substring(1)) !== -1) {
                message = constants.maskMessageGenerateIndividualPrepayment;
                isPrepayment = true;
            } else if (e.currentTarget.className.indexOf(selectors.generateIndividualFullTimesheetButtons.substring(1)) !== -1) {
                message = constants.maskMessageGenerateIndividualFullTimesheet;
                isPrepayment = false;
            } else {
                e.preventDefault();
                return;
            }

            var grid = $(selectors.timesheetsGrid).data("kendoGrid");
            var dataItem = grid.dataItem($(e.currentTarget).closest("tr.k-detail-row")
                                                           .prev("tr.k-master-row"));

            $.ajax({
                url: urls.generateIndividualPrepaymentsUrl,
                type: 'POST',
                beforeSend: function () {
                    //Показываем ajax-кругляшок на время выполнения запроса 
                    $(e.currentTarget).closest(selectors.timesheetDaysGridContainer)
                        .mask(message,
                            parseInt(constants.delayInterval));
                },
                data: {
                    timesheetDetailId: dataItem.TimesheetDetailId,
                    isPrepayment: isPrepayment
                },
                success: function (response) {
                    if (response.status === JsonStatus.ok) {
                        if (grid) {
                            grid.currentEditedRowIndex = $("tr.k-master-row").index($(e.currentTarget).closest("tr.k-detail-row")
                                                                                                      .prev("tr.k-master-row"));
                            grid.currentEditedPage = grid.dataSource.page();

                            var datasource = grid.dataSource;
                            datasource.read();
                        }
                    }

                    if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                        alert(response.message);
                    }
                },
                error: function () {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function () {
                    $(e.currentTarget).closest(selectors.timesheetDaysGridContainer)
                        .unmask();
                }
            });
        };

        var clearIndividualSecondTimesheetPart = function (e) {
            e.preventDefault();

            var grid = $(selectors.timesheetsGrid).data("kendoGrid");
            var dataItem = grid.dataItem($(e.currentTarget).closest("tr.k-detail-row")
                                                           .prev("tr.k-master-row"));

            $.ajax({
                url: urls.clearIndividualTimesheetPartUrl,
                type: 'POST',
                beforeSend: function () {
                    //Показываем ajax-кругляшок на время выполнения запроса
                    $(e.currentTarget).closest(selectors.timesheetDaysGridContainer)
                        .mask(constants.maskMessageClearIndividualTimesheetPart,
                            parseInt(constants.delayInterval));
                },
                data: {
                    timesheetDetailId: dataItem.TimesheetDetailId
                },
                success: function (response) {
                    if (response.status === JsonStatus.ok) {
                        if (grid) {
                            grid.currentEditedRowIndex = $("tr.k-master-row").index($(e.currentTarget).closest("tr.k-detail-row")
                                                                                                      .prev("tr.k-master-row"));
                            grid.currentEditedPage = grid.dataSource.page();

                            var datasource = grid.dataSource;
                            datasource.read();
                        }
                    }

                    if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                        alert(response.message);
                    }
                },
                error: function () {
                    if (!window.unloading) {
                        alert(constants.ajaxRequestErrorMessage);
                    }
                },
                complete: function () {
                    $(e.currentTarget).closest(selectors.timesheetDaysGridContainer)
                        .unmask();
                }
            });
        };

        //Функция, инициирующая добавление сотрудника в табель
        var addEmployee = function (e) {
            employeePopup.showWindow();
        };

        //Функция, инициирующая удаление сотрудника из табеля
        var deleteEmployee = function (e) {
            var grid = $(selectors.timesheetsGrid).data("kendoGrid");
            var dataItem = grid.dataItem($(e.currentTarget).closest("tr"));

            if (confirm('Вы действительно хотите удалить пользователя "' + dataItem.EmployeeFullName + '" из табеля?')) {
                $.ajax({
                    url: urls.deleteEmployeeFromTimesheetUrl,
                    type: 'POST',
                    beforeSend: function () {
                        //Показываем ajax-кругляшок на время выполнения запроса
                        $(selectors.globalContainer)
                            .mask(constants.maskMessageDeleteEmployee,
                                parseInt(constants.delayInterval));
                    },
                    data: {
                        timesheetDetailId: dataItem.TimesheetDetailId
                    },
                    success: function (response) {
                        if (response.status === JsonStatus.ok) {
                            if (grid) {
                                //После удаления сотрудника раскрывать строки в гриде не нужно
                                grid.currentEditedRowIndex = null;
                                grid.currentEditedPage = null;

                                var datasource = grid.dataSource;
                                var currentPage = $.calculateCurrentPage(datasource);
                                datasource.page(currentPage);
                                Utils.Notification.show('Сотрудник "' + dataItem.EmployeeFullName + '" удалён из табеля');
                            }
                        }

                        if (response.status === JsonStatus.error || response.status === JsonStatus.invalid) {
                            alert(response.message);
                        }
                    },
                    error: function () {
                        if (!window.unloading) {
                            alert(constants.ajaxRequestErrorMessage);
                        }
                    },
                    complete: function () {
                        $(selectors.globalContainer)
                            .unmask();
                    }
                });
            }
        };

        //Функция, инициирующая печать табеля
        var exportTimesheet = function (e) {
            var grid = $(selectors.timesheetsGrid);
            var gridData = grid.data("kendoGrid").dataSource.data();
            var totalNumber = gridData.length;

            if (totalNumber === 0) {
                e.preventDefault();
                return;
            }

            $(selectors.globalContainer).mask(constants.maskMessageExportFile);

            var exportButton = $(selectors.exportEmployeeButton);
            $.fileDownload(urls.exportUrl + "/" + exportButton.attr("timesheetId"),
            {
                successCallback: function (url) {
                    $(selectors.globalContainer).unmask();
                },
                failCallback: function (html, url) {
                    $(selectors.globalContainer).unmask();
                }
            });
        };

        return {
            getTimesheet: getTimesheet,
            checkTimesheet: checkTimesheet,
            generateAllPrepayments: generateAllPrepayments,
            generateIndividualPrepayments: generateIndividualPrepayments,
            clearSecondTimesheetPart: clearSecondTimesheetPart,
            clearIndividualSecondTimesheetPart: clearIndividualSecondTimesheetPart,
            addEmployee: addEmployee,
            deleteEmployee: deleteEmployee,
            exportTimesheet: exportTimesheet,
            showChangeModePopup: showChangeModePopup
        };
    };

    return {
        urls: urlsModule,
        constants: constantsModule,
        selectors: selectorsModule,
        datePicker: datePickerModule,
        dataSource: timesheetDataSourceModule,
        grid: timesheetGridModule,
        upload: uploadModule,
        changeModePopup: changeModePopupModule,
        handlers: handlersModule,
        employeePopup: employeePopupModule,
        employeeTable: employeeTableModule
    };
};