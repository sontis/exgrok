﻿ScheduleStatus = {
    notReconcile: 1,
    reconciledBySectionChief: 2,
    reconciledByProfessionalGroupOrganizer: 3,
    reconciled: 4
};
    