﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bell.Grok.Model;

namespace Bell.Grok.Web.Helpers
{    
    public static class DatesToMontsListConverter
    {
        /// <summary>
        /// Метод, рассчитывающий список месяцев из начальной и конечной дат 
        /// (1-ые числа начального и конечного метода интервала)
        /// </summary>
        /// <param name="beginDate">Начальная дата</param>
        /// <param name="endDate">Конечная дата</param>
        /// <returns>Список месяцев из указанного интервала</returns>
        public static List<SimpleMonthModel> ConvertDatesToMonthsList(DateTime beginDate, DateTime endDate)
        {
            var iterationDate = beginDate;
            var lastDate = endDate.AddMonths(1);
            var resultList = new List<SimpleMonthModel>();

            while (iterationDate < lastDate)
            {
                resultList.Add(new SimpleMonthModel()
                    {
                        Year = iterationDate.Year,
                        Month = iterationDate.Month
                    });

                iterationDate = iterationDate.AddMonths(1);
            }

            return resultList;
        }

        public static SimpleMonthModel ConvertDateToMonth(DateTime date)
        {
            return new SimpleMonthModel()
                {
                    Year = date.Year,
                    Month = date.Month
                };
        }
    }
}