﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;

namespace Bell.Grok.Web.Helpers
{    
    /// <summary>
    /// Простой класс, возвращающий пункты меню администратора
    /// </summary>
    public static class GrokMenuProvider
    {
        /// <summary>
        /// Метод, возвращающий пункты меню
        /// </summary>
        /// <param name="urlHelper">Объект UrlHepler для генерации url</param>
        /// <returns>Множество объектов модели MenuModel</returns>
        public static IEnumerable<MenuModel> GetItems(UrlHelper urlHelper)
        {            
            return new List<MenuModel>
                {
                    new MenuModel {ItemText = "Ведение пользователей", ItemUrl = urlHelper.Action("List", "Users", new { area = "Admin"})}
                };
        }

        /// <summary>
        /// Метод, возвращающий пункты меню для роли Сотрудник ОТиЗП
        /// </summary>
        /// <param name="urlHelper">Объект UrlHepler для генерации url</param>
        /// <returns>Множество объектов модели MenuModel</returns>
        public static IEnumerable<MenuModel> GetWorkDepartmentItems(UrlHelper urlHelper)
        {
            return new List<MenuModel>
                {
                    new MenuModel { ItemText = "Просмотр информации о станциях и кассирах", ItemUrl = urlHelper.Action("Index", "Home")},
                    new MenuModel { ItemText = "Шаблоны графиков кассиров билетных", ItemUrl = urlHelper.Action("Index", "ScheduleTemplate")}
                };
        }

        /// <summary>
        /// Метод, возвращающий заголовок меню
        /// </summary>
        /// <returns>Заголовок меню</returns>
        public static string GetHeaderName()
        {
            return "Настройка системы";
        }

        /// <summary>
        /// Метод, возвращающий заголовок меню для роли Сотрудник ОТиЗП
        /// </summary>
        /// <returns>Заголовок меню</returns>
        public static string GetWorkDepartmentHeaderName()
        {
            return "Главное меню";
        }
    }
}