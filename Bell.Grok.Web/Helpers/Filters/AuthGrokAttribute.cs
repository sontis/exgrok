﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.Routing;
using Bell.Grok.Model;


namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Фильтр авторизации
    /// </summary>
    public class AuthGrokAttribute : FilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// допустимые роли
        /// </summary>
        private Roles[] AllowedRoles;

        public AuthGrokAttribute(params Roles[] roles)
        {
            AllowedRoles = roles;
        }

        /// <summary>
        /// Метод, проверяющий, авторизован ли пользователь
        /// </summary>
        /// <param name="httpContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        /// <returns>Истина, если пользователь авторизован, и ложь в противном случае</returns>
        protected bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Request.IsAuthenticated;
        }
        
        /// <summary>
        /// Метод, проверяющий, принадлежит ли пользователь хотя бы одной из допустимого набора
        /// </summary>
        /// <param name="httpContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        /// <returns>Истина, если пользователь принадлежит ли пользователь хотя бы одной из допустимого набора, и ложь в противном случае</returns>
        private bool IsUserInRole(HttpContextBase httpContext)
        {
            if (AllowedRoles.Length > 0)
            {
                for (int i = 0; i < AllowedRoles.Length; i++)
                {
                    if (httpContext.User.IsInRole(AllowedRoles[i].ToText()))
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Метод, реализующий логику фильтра авторизации
        /// </summary>
        /// <param name="filterContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!AuthorizeCore(filterContext.HttpContext))
            {                
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }            

            if (!IsUserInRole(filterContext.HttpContext))
            {                
                filterContext.Result = new  HttpStatusCodeResult(403, "Access denied!");
            }  
        }
    }
}