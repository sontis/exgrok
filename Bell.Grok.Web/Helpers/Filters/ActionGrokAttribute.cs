﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Bell.Grok.Business;
using Bell.Grok.Services.Logging;
using Bell.Grok.Web.Services;

namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Фильтр для логгирования действий пользователей на сайте.
    /// </summary>
    public class ActionGrokAttribute : FilterAttribute, IActionFilter, IResultFilter
    {
        /// <summary>
        /// Перечислимый тип, описывающий формат имени действия
        /// </summary>
        public enum ActionNameFormat
        {
            ActionNameOnly, //[ActionName]
            AcionAndControllerNames, //[ControllerName].[ActionName]
            FullName //[AreaName].[ControllerName].[ActionName]
        }

        ActionNameFormat _actionNameFormat;

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="actionNameFormat">Параметр, описывающий формат имени действия</param>
        public ActionGrokAttribute(ActionNameFormat actionNameFormat = ActionNameFormat.FullName)
        {
            _actionNameFormat = actionNameFormat;
        }

        /// <summary>
        /// Метод OnActionExecuting вызывается перед вызовом метода действия.
        /// Регистрирует действия пользователя. И отправляет информацию  о данном действии в файл.
        /// </summary>
        /// <param name="filterContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userRepository = new UserRepository();

            var areaName = filterContext.RouteData.Values["area"];
            var controllerName = filterContext.RouteData.Values["controller"];
            var actionName = filterContext.RouteData.Values["action"];

            var actionNameBuilder = new StringBuilder(actionName.ToString());
            actionNameBuilder.Insert(0, (_actionNameFormat == ActionNameFormat.AcionAndControllerNames || _actionNameFormat == ActionNameFormat.FullName) ? (controllerName.ToString() + ".") : string.Empty);
            actionNameBuilder.Insert(0, (_actionNameFormat == ActionNameFormat.FullName && areaName != null) ? areaName.ToString() + "." : string.Empty);                        

            #region Logging
            var login = filterContext.HttpContext.User.Identity.Name;
            var userFullName = userRepository.GetFullNameByLogin(login);
            var userIpAddress = filterContext.HttpContext.Request.UserHostAddress;

            var actionParams = new Dictionary<string, object>()
                {
                    {"PeerIPAddress",  userIpAddress == null ? "Unknown IP" : IPAddress.Parse(userIpAddress).MapToIPv4().ToString()},
                    {"ActionName", actionNameBuilder.ToString()},
                    {"UserLogin", login},
                    {"UserFullName", userFullName}
                };

            GrokLogger.LogActivity(actionParams, true);
            #endregion
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }

        private const string FileDownloadCookieName = "fileDownload";
        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (filterContext.Result is FileResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                filterContext.HttpContext.Response.SetCookie(new HttpCookie(FileDownloadCookieName, "true") { Path = "/" });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (filterContext.HttpContext.Request.Cookies[FileDownloadCookieName] != null)
                    filterContext.HttpContext.Response.Cookies[FileDownloadCookieName].Expires = DateTime.Now.AddYears(-1);
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {

        }
    }
}