﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Bell.Grok.Model;

namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Фильтр авторизации для ajax-запросов
    /// </summary>
    public class AjaxAuthGrokAttribute : FilterAttribute, IAuthorizationFilter
    {
        /// <summary>
        /// допустимые роли
        /// </summary>
        private Roles[] AllowedRoles;

        /// <summary>
        /// Имена контроллеров, для действий которых при недостаточности прав
        /// возвращается invalid status, а не unathorized
        /// </summary>
        private List<string> InvalidStatusControllerNames;

        public AjaxAuthGrokAttribute(params Roles[] roles)
        {
            AllowedRoles = roles;

            //Для запроса действий меню при недостаточности прав не должен возвращаться статус "Unathorized",
            //так как в этом случае необходимо просто не показывать меню без всяких сообщений
            InvalidStatusControllerNames = new List<string>()
                {
                    "Menu"
                };
        }

        /// <summary>
        /// Метод, проверяющий, авторизован ли пользователь
        /// </summary>
        /// <param name="httpContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        /// <returns>Истина, если пользователь авторизован, и ложь в противном случае</returns>
        protected bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Request.IsAuthenticated;
        }
        
        /// <summary>
        /// Метод, проверяющий, принадлежит ли пользователь хотя бы одной из допустимого набора
        /// </summary>
        /// <param name="httpContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        /// <returns>Истина, если пользователь принадлежит ли пользователь хотя бы одной из допустимого набора, и ложь в противном случае</returns>
        private bool IsUserInRole(HttpContextBase httpContext)
        {
            if (AllowedRoles.Length > 0)
            {
                for (int i = 0; i < AllowedRoles.Length; i++)
                {
                    if (httpContext.User.IsInRole(AllowedRoles[i].ToText()))
                    {
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Метод, реализующий логику фильтра авторизации.
        /// В отличие от реализации этого метода в AuthGrokAttribute в filterContext 
        /// в случае неудачи записывается json со специальным статусом
        /// </summary>
        /// <param name="filterContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!AuthorizeCore(filterContext.HttpContext))
            {
                 filterContext.Result = Json(new
                    {
                        status = JsonStatuses.Unauthorized,
                        message = "Пользователь не авторизован!"
                    });

                return;
            }            

            if (!IsUserInRole(filterContext.HttpContext))
            {
                var controllerName = filterContext.RouteData
                                                  .Values["controller"]
                                                  .ToString();

                if (InvalidStatusControllerNames.Contains(controllerName, StringComparer.OrdinalIgnoreCase))
                {
                    filterContext.Result = Json(new
                    {
                        status = JsonStatuses.Invalid
                    });
                }
                else
                {
                    filterContext.Result = Json(new
                    {
                        status = JsonStatuses.Unauthorized,
                        message = "У пользователя нет прав на выполнение этой операции"
                    });
                }                
            }  
        }

        /// <summary>
        /// Метод, формирующий json-ответ
        /// </summary>
        /// <param name="data">Данные для json-объекта</param>
        /// <returns>json-объект</returns>
        private ActionResult Json(object data)
        {
            return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = data,
                    ContentType = "text/html"
                };
        }

    }
}
