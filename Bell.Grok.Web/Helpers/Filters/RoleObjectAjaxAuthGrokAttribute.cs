﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Web.Services;

namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Фильтр авторизации для ajax-запросов по таблице RoleObject базы данных
    /// </summary>
    public class RoleObjectAjaxAuthGrokAttribute : FilterAttribute, IAuthorizationFilter
    {
        //Идентификатор действия
        private UserActions Action;
        //Идентификатор объекта, над которым производится действие
        private DataObjects DataObject;

        /// <summary>        
        /// </summary>
        /// <param name="action">Объект перечисления - копии таблицы RoleObjectRight БД, 
        ///                        являющийся идентификатором действия</param>
        /// <param name="dataDataObject">Объект перечисления - копии таблицы DataObject БД,
        ///                        являющийся идентификатором объекта </param>
        public RoleObjectAjaxAuthGrokAttribute(UserActions action, DataObjects dataDataObject)
        {
            Action = action;
            DataObject = dataDataObject;
        }

        /// <summary>
        /// Метод, проверяющий, авторизован ли пользователь
        /// </summary>
        /// <param name="httpContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        /// <returns>Истина, если пользователь авторизован, и ложь в противном случае</returns>
        protected bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Request.IsAuthenticated;
        }

        /// <summary>
        /// Метод, проверяющий наличие прав пользователя на действие над объектом
        /// </summary>
        /// <param name="httpContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        /// <returns>истина. если у пользователя есть право над действие над объектои, и ложь в противном случае</returns>
        private bool CanUserActWithObject(HttpContextBase httpContext)
        {
            var login = httpContext.User.Identity.Name;

            return GrokRoleObjectsService.CanUserMakeAction(Action, login, DataObject);                
        }

        /// <summary>
        /// Метод, реализующий логику фильтра авторизации.
        /// В отличие от реализации этого метода в AuthGrokAttribute в filterContext 
        /// в случае неудачи записывается json со специальным статусом
        /// </summary>
        /// <param name="filterContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!AuthorizeCore(filterContext.HttpContext))
            {
                filterContext.Result = Json(new
                {
                    status = JsonStatuses.Unauthorized,
                    message = "Пользователь не авторизован!"
                });
            }    

            if (!CanUserActWithObject(filterContext.HttpContext))
            {
                filterContext.Result = Json(new
                {
                    status = JsonStatuses.Unauthorized,
                    message = "У пользователя нет прав на выполнение этой операции"
                });
            }
        }

        /// <summary>
        /// Метод, формирующий json-ответ
        /// </summary>
        /// <param name="data">Данные для json-объекта</param>
        /// <returns>json-объект</returns>
        private ActionResult Json(object data)
        {
            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = data,
                ContentType = "application/json"
            };
        }
    }
}