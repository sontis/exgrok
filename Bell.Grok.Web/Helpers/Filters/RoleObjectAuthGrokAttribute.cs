﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Web.Services;

namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Фильтр авторизации по таблице RoleObject базы данных
    /// </summary>
    public class RoleObjectAuthGrokAttribute : FilterAttribute, IAuthorizationFilter
    {
        //Идентификатор действия
        private UserActions Action;
        //Идентификатор объекта, над которым производится действие
        private DataObjects DataObject;

        /// <summary>        
        /// </summary>
        /// <param name="action">Объект перечисления - копии таблицы RoleObjectRight БД, 
        ///                        являющийся идентификатором действия</param>
        /// <param name="dataDataObject">Объект перечисления - копии таблицы DataObject БД,
        ///                        являющийся идентификатором объекта </param>
        public RoleObjectAuthGrokAttribute(UserActions action, DataObjects dataDataObject)
        {
            Action = action;
            DataObject = dataDataObject;
        }

        /// <summary>
        /// Метод, проверяющий, авторизован ли пользователь
        /// </summary>
        /// <param name="httpContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        /// <returns>Истина, если пользователь авторизован, и ложь в противном случае</returns>
        protected bool AuthorizeCore(HttpContextBase httpContext)
        {
            return httpContext.Request.IsAuthenticated;
        }

        /// <summary>
        /// Метод, проверяющий наличие прав пользователя на действие над объектом
        /// </summary>
        /// <param name="httpContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        /// <returns>истина. если у пользователя есть право над действие над объектои, и ложь в противном случае</returns>
        private bool CanUserActWithObject(HttpContextBase httpContext)
        {
            var login = httpContext.User.Identity.Name;

            return GrokRoleObjectsService.CanUserMakeAction(Action, login, DataObject);  
        }

        /// <summary>
        /// Метод, реализующий логику фильтра авторизации.
        /// В отличие от реализации этого метода в AuthGrokAttribute в filterContext 
        /// в случае неудачи записывается json со специальным статусом
        /// </summary>
        /// <param name="filterContext">Параметр, инкапсулирующий сведения об HTTP-запросе</param>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!AuthorizeCore(filterContext.HttpContext))
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }    

            if (!CanUserActWithObject(filterContext.HttpContext))
            {
                filterContext.Result = new  HttpStatusCodeResult(403, "Access denied!");
            }
        }       
    }
}