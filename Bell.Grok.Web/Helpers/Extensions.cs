﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Web.Helpers.Validation;

namespace System.Web.Routing
{
    public static class RoutingExtensions
    {
        /// <summary>
        /// Регистрирует маршрут для основного приложения (с учетом пространства имен контроллеров)
        /// </summary>
        /// <param name="routes"></param>
        /// <param name="name"></param>
        /// <param name="url"></param>
        /// <param name="defaults"></param>
        public static void MapGrokRoute(this RouteCollection routes, string name, string url, object defaults)
        {
            routes.MapGrokRoute(name, url, defaults, null);
        }

        public static void MapGrokRoute(this RouteCollection routes, string name, string url, object defaults, object constraints)
        {
            routes.MapRoute(name, url, defaults, constraints, new string[] { "Bell.Grok.Web.Controllers" });
        }

        /// <summary>
        /// Извлекает ошибки валидации.
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public static IEnumerable<ValidationError> ExtractValidationErrors(this ModelStateDictionary modelState)
        {
            return from key in modelState.Keys
                   let state = modelState[key]
                   where state.Errors.Count > 0
                   select new ValidationError(key, state.Errors[0].ErrorMessage);
        }

        /// <summary>
        /// Проверяет расширение файла - является ли оно исполняемого формата
        /// </summary>
        /// <param name="extension">Расширение файла</param>
        /// <returns></returns>
        public static bool IsExtentionByExecutedFormat(this string extension)
        {
            var executedFormats = new List<string>()
                {
                    "bat",
                    "com",
                    "exe",
                    "pif"
                };

            return executedFormats.Contains(extension.ToLower());
        }
    }
}