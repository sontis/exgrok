﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Business;

namespace Bell.Grok.Web.Helpers
{
    public class EmployeeWeekWorkTimePriorityModelBinder : DefaultModelBinder
    {
        /// <summary>
        /// Метод класса DefaultModelBinder
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = new EmployeeWeekWorkTimePriorityModel();    
            model.EmployeeDayWorkTimePriorities = new List<EmployeeDayWorkTimePriorityModel>();

            var weekDayRepository = new WeekDayRepository();
            var queryString = controllerContext.HttpContext.Request.Form;

            //Проверка наличия идентификатор приоритета станции работника
            if (!string.IsNullOrWhiteSpace(queryString["StationPriorityID"]))
            {
                int stationPriorityId;
                if (int.TryParse(queryString["StationPriorityID"], out stationPriorityId))
                {
                    model.StationPriorityId = stationPriorityId;
                }
                else
                {
                    AddParameterError("StationPriorityID", bindingContext);
                }
            }
            else
            {
                AddParameterError("StationPriorityID", bindingContext);
            }

                
            var isDaily = !string.IsNullOrWhiteSpace(queryString["DailyBegin"]) &&
                            !string.IsNullOrWhiteSpace(queryString["DailyEnd"]);

            //Определён ли ежедневный интервал
            if (isDaily)
            {
                AddDailyIntervalsToModel(model, queryString, weekDayRepository, bindingContext);
            }
            else
            {
                //Определён ли интервал для рабочих днех
                var areWorkDays = !string.IsNullOrWhiteSpace(queryString["WorkDaysBegin"]) &&
                                    !string.IsNullOrWhiteSpace(queryString["WorkDaysEnd"]);
                //Определён ли интервал для выходных дней
                var areWeekendDays = !string.IsNullOrWhiteSpace(queryString["WeekendBegin"]) &&
                                        !string.IsNullOrWhiteSpace(queryString["WeekendEnd"]);
                if (areWorkDays)
                {
                    AddWorkDaysIntervalsToModel(model, queryString, weekDayRepository, bindingContext);
                }

                if (areWeekendDays)
                {
                    AddWeekendIntervalsToModel(model, queryString, weekDayRepository, bindingContext);
                }

                //Интервалы определены по конкретным дням
                if (!areWorkDays && !areWeekendDays)
                {
                    //Определён ли интервал для понедельника
                    var isMonday = !string.IsNullOrWhiteSpace(queryString["MondayBegin"]) &&
                                    !string.IsNullOrWhiteSpace(queryString["MondayEnd"]);

                    if (isMonday)
                    {
                        AddSpecifiedDayIntervalsToModel(model, "MondayBegin", "MondayEnd", queryString, weekDayRepository, (int)WeekDays.Monday, bindingContext);
                    }

                    //Определён ли интервал для вторника
                    var isTuesday = !string.IsNullOrWhiteSpace(queryString["TuesdayBegin"]) &&
                                    !string.IsNullOrWhiteSpace(queryString["TuesdayEnd"]);

                    if (isTuesday)
                    {
                        AddSpecifiedDayIntervalsToModel(model, "TuesdayBegin", "TuesdayEnd", queryString, weekDayRepository, (int)WeekDays.Tuesday, bindingContext);
                    }

                    //Определён ли интервал для среды
                    var isWednesday = !string.IsNullOrWhiteSpace(queryString["WednesdayBegin"]) &&
                                    !string.IsNullOrWhiteSpace(queryString["WednesdayEnd"]);

                    if (isWednesday)
                    {
                        AddSpecifiedDayIntervalsToModel(model, "WednesdayBegin", "WednesdayEnd", queryString, weekDayRepository, (int)WeekDays.Wednesday, bindingContext);
                    }

                    //Определён ли интервал для четверга
                    var isThursday = !string.IsNullOrWhiteSpace(queryString["ThursdayBegin"]) &&
                                    !string.IsNullOrWhiteSpace(queryString["ThursdayEnd"]);

                    if (isThursday)
                    {
                        AddSpecifiedDayIntervalsToModel(model, "ThursdayBegin", "ThursdayEnd", queryString, weekDayRepository, (int)WeekDays.Thursday, bindingContext);
                    }

                    //Определён ли интервал для пятницы
                    var isFriday = !string.IsNullOrWhiteSpace(queryString["FridayBegin"]) &&
                                    !string.IsNullOrWhiteSpace(queryString["FridayEnd"]);

                    if (isFriday)
                    {
                        AddSpecifiedDayIntervalsToModel(model, "FridayBegin", "FridayEnd", queryString, weekDayRepository, (int)WeekDays.Friday, bindingContext);
                    }

                    //Определён ли интервал для субботы
                    var isSaturday = !string.IsNullOrWhiteSpace(queryString["SaturdayBegin"]) &&
                                    !string.IsNullOrWhiteSpace(queryString["SaturdayEnd"]);

                    if (isSaturday)
                    {
                        AddSpecifiedDayIntervalsToModel(model, "SaturdayBegin", "SaturdayEnd", queryString, weekDayRepository, (int)WeekDays.Saturday, bindingContext);
                    }

                    var isSunday = !string.IsNullOrWhiteSpace(queryString["SundayBegin"]) &&
                                    !string.IsNullOrWhiteSpace(queryString["SundayEnd"]);

                    //Определён ли интервал для воскресенья
                    if (isSunday)
                    {
                        AddSpecifiedDayIntervalsToModel(model, "SundayBegin", "SundayEnd", queryString, weekDayRepository, (int)WeekDays.Sunday, bindingContext);
                    }
                }
            }
                
                       

            return model;
        }

        /// <summary>
        /// Метод для добавления ошибок в контекст в форматированном виде
        /// </summary>
        /// <param name="paramName">Имя параметра, значение которого оказалось невалидным</param>
        /// <param name="context">Контекст привязки</param>
        private void AddParameterError(string paramName, ModelBindingContext context)
        {
            context.ModelState.AddModelError(String.Empty, String.Format("Параметр {0} отсутствует в запросе, либо содержит неверное значение", paramName));
        }

        /// <summary>
        /// Метод, анализирующий начальную и конечную границы ЕЖЕДНЕВНОГО временного интервала и добавляющий их значения
        /// в случае их валидности в модель EmployeeWeekWorkTimePriorityModel
        /// </summary>
        /// <param name="model">Объект модели EmployeeWeekWorkTimePriorityModel</param>        
        /// <param name="queryString">Коллекция параметров, полученная из запроса</param>
        /// <param name="weekDayRepository">Репозиторий для работы со справочником, хранящем дни недели</param>
        /// <param name="context">Контекст привязки</param>
        private void AddDailyIntervalsToModel(EmployeeWeekWorkTimePriorityModel model, NameValueCollection queryString, 
                                              WeekDayRepository weekDayRepository, ModelBindingContext context)
        {
            TimeSpan dailyBegin;

            if (TimeSpan.TryParse(queryString["DailyBegin"], out dailyBegin))
            {
                TimeSpan dailyEnd;
                if (TimeSpan.TryParse(queryString["DailyEnd"], out dailyEnd))
                {
                    var intervals =
                        weekDayRepository.GetAllIds()
                                         .Select(w => new EmployeeDayWorkTimePriorityModel()
                                            {
                                                WeekDayId = w,
                                                BeginTime = dailyBegin,
                                                EndTime = dailyEnd
                                            }).ToList();
                    var extraIntervals = intervals.Where(i => i.WeekDayId > 7).ToList();
                    extraIntervals.ForEach(e => intervals.Remove(e));

                    model.EmployeeDayWorkTimePriorities.AddRange(intervals);
                }
                else
                {
                    AddParameterError("DailyEnd", context);
                }
            }
            else
            {
                AddParameterError("DailyBegin", context);
            }
        } 
        
        /// <summary>
        /// Метод, анализирующий начальную и конечную границы временного интервала РАБОЧИХ ДНЕЙ и добавляющий их значения
        /// в случае их валидности в модель EmployeeWeekWorkTimePriorityModel
        /// </summary>
        /// <param name="model">Объект модели EmployeeWeekWorkTimePriorityModel</param>        
        /// <param name="queryString">Коллекция параметров, полученная из запроса</param>
        /// <param name="weekDayRepository">Репозиторий для работы со справочником, хранящем дни недели</param>
        /// <param name="context">Контекст привязки</param>
        private void AddWorkDaysIntervalsToModel(EmployeeWeekWorkTimePriorityModel model,  NameValueCollection queryString, 
                                                 WeekDayRepository weekDayRepository, ModelBindingContext context)
        {
            TimeSpan workDaysBegin;

            if (TimeSpan.TryParse(queryString["WorkDaysBegin"], out workDaysBegin))
            {
                TimeSpan workDaysEnd;
                if (TimeSpan.TryParse(queryString["WorkDaysEnd"], out workDaysEnd))
                {
                    var intervals =
                        weekDayRepository.GetAllWorkDaysIds().Select(w => new EmployeeDayWorkTimePriorityModel()
                        {
                            WeekDayId = w,
                            BeginTime = workDaysBegin,
                            EndTime = workDaysEnd
                        });

                    model.EmployeeDayWorkTimePriorities.AddRange(intervals);
                }
                else
                {
                    AddParameterError("WorkDaysEnd", context);
                }
            }
            else
            {
                AddParameterError("WorkDaysBegin", context);
            }
        }

        /// <summary>
        /// Метод, анализирующий начальную и конечную границы временного интервала ВЫХОДНЫХ ДНЕЙ и добавляющий их значения
        /// в случае их валидности в модель EmployeeWeekWorkTimePriorityModel
        /// </summary>
        /// <param name="model">Объект модели EmployeeWeekWorkTimePriorityModel</param>
        /// <param name="queryString">Коллекция параметров, полученная из запроса</param>
        /// <param name="weekDayRepository">Репозиторий для работы со справочником, хранящем дни недели</param>
        /// <param name="context">Контекст привязки</param>
        private void AddWeekendIntervalsToModel(EmployeeWeekWorkTimePriorityModel model, NameValueCollection queryString,
                                                WeekDayRepository weekDayRepository, ModelBindingContext context)
        {
            TimeSpan weekendBegin;

            if (TimeSpan.TryParse(queryString["WeekendBegin"], out weekendBegin))
            {
                TimeSpan weekendEnd;
                if (TimeSpan.TryParse(queryString["WeekendEnd"], out weekendEnd))
                {
                    var intervals =
                        weekDayRepository.GetAllWeekendIds().Select(w => new EmployeeDayWorkTimePriorityModel()
                        {
                            WeekDayId = w,
                            BeginTime = weekendBegin,
                            EndTime = weekendEnd
                        });

                    model.EmployeeDayWorkTimePriorities.AddRange(intervals);
                }
                else
                {
                    AddParameterError("WeekendEnd", context);
                }
            }
            else
            {
                AddParameterError("WeekendBegin", context);
            }
        }

        /// <summary>
        /// Метод, анализирующий начальную и конечную границы временного интервала КОНКРЕТНОГО ДНЯ и добавляющий их значения
        /// в случае их валидности в модель EmployeeWeekWorkTimePriorityModel
        /// </summary>
        /// <param name="model">Объект модели EmployeeWeekWorkTimePriorityModel</param>
        /// <param name="beginName">Хэш-ключ начальной границы в коллекции параметров, полученной из запроса</param>
        /// <param name="endName">Хэш-ключ конечной границы в коллекции параметров, полученной из запроса</param>
        /// <param name="queryString">Коллекция параметров, полученная из запроса</param>
        /// <param name="weekDayRepository">Репозиторий для работы со справочником, хранящем дни недели</param>
        /// <param name="dayNumber">Номер дня недели</param>
        /// <param name="context">Контекст привязки</param>
        private void AddSpecifiedDayIntervalsToModel(EmployeeWeekWorkTimePriorityModel model,
                                                     string beginName,
                                                     string endName,
                                                     NameValueCollection queryString,
                                                     WeekDayRepository weekDayRepository, 
                                                     int dayNumber,
                                                     ModelBindingContext context)
        {
            TimeSpan begin;

            if (TimeSpan.TryParse(queryString[beginName], out begin))
            {
                TimeSpan end;
                if (TimeSpan.TryParse(queryString[endName], out end))
                {
                    var weekId = weekDayRepository.GetIdByNumber(dayNumber);

                    var employeeDayWorkTimePriorityModel = new EmployeeDayWorkTimePriorityModel()
                        {
                            WeekDayId = weekId,
                            BeginTime = begin,
                            EndTime = end
                        };

                    model.EmployeeDayWorkTimePriorities.Add(employeeDayWorkTimePriorityModel);
                }
                else
                {
                    AddParameterError(beginName, context);
                }
            }
            else
            {
                AddParameterError(endName, context);
            }
        }
    }
}