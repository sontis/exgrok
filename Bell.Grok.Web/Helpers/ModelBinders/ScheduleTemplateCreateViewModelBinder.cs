﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web.Helpers
{
    public class ScheduleTemplateCreateViewModelBinder : DefaultModelBinder
    {
        /// <summary>
        /// Метод класса DefaultModelBinder
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var model = new ScheduleTemplateViewModel();
            model.Directions = new List<int>();
            model.Sectors = new List<int>();
            
            var queryString = controllerContext.HttpContext.Request.Form;

            if (!string.IsNullOrWhiteSpace(queryString["ScheduleTemplateId"]))
            {
                int scheduleTemplateId;
                if (Int32.TryParse(queryString["ScheduleTemplateId"], out scheduleTemplateId))
                {
                    model.ScheduleTemplateId = scheduleTemplateId;
                }                
            }

            if (!string.IsNullOrWhiteSpace(queryString["FullName"]))
            {
                model.FullName = queryString["FullName"];
            }

            if (!string.IsNullOrWhiteSpace(queryString["BeginDate"]))
            {
                model.BeginDate = queryString["BeginDate"];
            }

            if (!string.IsNullOrWhiteSpace(queryString["EndDate"]))
            {
                model.EndDate = queryString["EndDate"];
            }

            if (!string.IsNullOrWhiteSpace(queryString["Directions"]))
            {
                var directions = queryString["Directions"].Split(',');                
                foreach (var direction in directions)
                {
                    int directionId;
                    if (Int32.TryParse(direction, out directionId))
                    {
                        model.Directions.Add(directionId);
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(queryString["Sectors"]))
            {
                var sectors = queryString["Sectors"].Split(',');
                foreach (var sector in sectors)
                {
                    int sectorId;
                    if (Int32.TryParse(sector, out sectorId))
                    {
                        model.Sectors.Add(sectorId);
                    }
                }
            }            

            var context = new ValidationContext(model, null, null);
            var results = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(model, context, results, true);

            if (!isValid)
            {
                foreach (var validationResult in results)
                {
                    bindingContext.ModelState.AddModelError(validationResult.MemberNames.Any() 
                                                                ? validationResult.MemberNames.ElementAt(0) 
                                                                : string.Empty,
                                                            validationResult.ErrorMessage);
                }
            }

            return model;
        }

    }
}
