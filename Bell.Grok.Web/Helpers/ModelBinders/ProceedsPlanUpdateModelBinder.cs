﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;

namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Простенький связыватель данных для параметров 
    /// редактирования планов по выручке
    /// </summary>
    public class ProceedsPlanUpdateModelBinder : DefaultModelBinder
    {
        /// <summary>
        /// Метод класса DefaultModelBinder
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var queryString = controllerContext.HttpContext.Request.Form;
            var proceedsPlanGridModel = new ProceedsPlanGridModel();
            var style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;
            var culture = CultureInfo.InvariantCulture;
            
            if (queryString["Year"] != null)
            {
                int year;
                if (int.TryParse(queryString["Year"], out year))
                {
                    proceedsPlanGridModel.Year = year;
                }
            }

            if (queryString["Month"] != null)
            {
                int month;
                if (int.TryParse(queryString["Month"], out month))
                {
                    proceedsPlanGridModel.Month = month;
                }
            }

            if (queryString["SectorStationId"] != null)
            {
                int sectorStationId;
                if (int.TryParse(queryString["SectorStationId"], out sectorStationId))
                {
                    proceedsPlanGridModel.SectorStationId = sectorStationId;
                }
            }

            if (queryString["StationMonthProceedsId"] != null)
            {
                int stationMonthProceedsId;
                if (int.TryParse(queryString["StationMonthProceedsId"], out stationMonthProceedsId))
                {
                    proceedsPlanGridModel.StationMonthProceedsId = stationMonthProceedsId;
                }
            }

            if (queryString["CommonPlan"] != null)
            {
                decimal commonPlan;
                if (decimal.TryParse(queryString["CommonPlan"], style, culture, out commonPlan))
                {
                    proceedsPlanGridModel.CommonPlan = commonPlan;
                }
            }

            if (queryString["PaidPlan"] != null)
            {
                decimal paidPlan;
                if (decimal.TryParse(queryString["PaidPlan"], style, culture, out paidPlan))
                {
                    proceedsPlanGridModel.PaidPlan = paidPlan;
                }
            }

            return proceedsPlanGridModel;
        }
    }
}