﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Расширение 
    /// </summary>
    public static class ValueTextModelExtension
    {
        /// <summary>
        /// Преобразование в тип MVC
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static SelectListItem ToSelectListItem(this KeyValuePair<int, string> item)
        {
            return new SelectListItem
                {
                    Value = item.Key.ToString(),
                    Text = item.Value
                };
        }

        public static SelectListItem ToSelectListItem(this KeyValuePair<byte, string> item)
        {
            return new SelectListItem
            {
                Value = item.Key.ToString(),
                Text = item.Value
            };
        }
    }
}