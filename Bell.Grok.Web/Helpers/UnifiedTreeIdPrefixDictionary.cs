﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Статический класс, содержащий словарь префиксов для единых идентификаторов узлов
    /// дерева навигации в зависимости от типа узла
    /// </summary>
    public static class UnifiedTreeIdPrefixDictionary
    {
        public static readonly Dictionary<string, string> TreeIdCodes = new Dictionary<string, string>
        {
            { "Direction", "D" },
            { "Sector", "S" },
            { "SectorStructure", "I" },
            { "CashierCharacter", "CC" },
            { "Cashier", "C" },
            { "Station", "T"}
        };        
    }
}