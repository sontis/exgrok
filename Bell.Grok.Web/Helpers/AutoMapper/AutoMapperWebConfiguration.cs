﻿using System.Web.Mvc;
using AutoMapper;
using Bell.Grok.Model;
using Bell.Grok.Web.Areas.Admin.ViewModel;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web.Helpers.AutoMapper
{
    /// <summary>
    /// Конфигуратор маппинга бизнес-моделей в модели представления
    /// </summary>
    public class AutoMapperWebConfiguration
    {
        /// <summary>
        /// Инициализация маппера
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new DirectionTreeProfile());
                cfg.AddProfile(new UserModelsProfile());
                cfg.AddProfile(new TellerWindowScheduleModelsProfile());                
            });
            Mapper.AssertConfigurationIsValid();
        }
    }

    /// <summary>
    /// Профиль для DirectionTree
    /// </summary>
    public class DirectionTreeProfile : Profile
    {
        /// <summary>
        /// Конфигурация профиля маппинга
        /// </summary>
        protected override void Configure()
        {
            Mapper.CreateMap<DirectionTreeModel, DirectionTreeViewModel>()
                .ForMember(d => d.UnifiedTreeId, o => o.Ignore())
                .ForMember(d => d.ObjectUrl, o => o.Ignore());
        }
    }

    public class UserModelsProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UserRoleViewModel, UserRoleModel>();

            Mapper.CreateMap<UserViewModel, UserModel>()
                  .ForMember(m => m.UserRole, o => o.MapFrom(u => Mapper.Map<UserRoleViewModel, UserRoleModel>(u.UserRole)));

            Mapper.CreateMap<UserCreateFromDictionaryViewModel, UserCreateFromDictionaryModel>()
                  .ForMember(m => m.UserRole, o => o.MapFrom(u => Mapper.Map<UserRoleViewModel, UserRoleModel>(u.UserRole)));

            Mapper.CreateMap<UserRoleModel, UserRoleViewModel>()
                  .ForMember(m => m.AllRoles, o => o.Ignore())
                  .ForMember(m => m.AllDirections, o => o.Ignore());            
        }
    }

    public class TellerWindowScheduleModelsProfile: Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<TellerWindowScheduleModel, ViewModel.TellerWindowScheduleRowModel>()
                  .ForMember(dest => dest.Year, o => o.MapFrom(src => new SelectListItem()
                      {
                          Value = src.Year.ToString(),
                          Text = src.Year.ToString()
                      }));

            Mapper.CreateMap<ViewModel.TellerWindowScheduleRowModel, TellerWindowScheduleModel>()
                  .ForMember(dest => dest.Year, o => o.MapFrom(src => src.Year.Value));

            Mapper.CreateMap<TellerWindowScheduleContentModel, TellerWindowScheduleContentRowModel>()
                  .ForMember(dest => dest.TellerWindowNumber, o => o.MapFrom(src => new SelectListItem()
                      {
                          Value = src.TellerWindowId.ToString(),
                          Text = src.TellerWindowNumber
                      }));

            Mapper.CreateMap<TellerWindowScheduleContentRowModel, TellerWindowScheduleContentModel>()
                  .ForMember(dest => dest.TellerWindowId, o => o.MapFrom(src => src.TellerWindowNumber.Value))
                  .ForMember(dest => dest.TellerWindowNumber, o => o.MapFrom(src => src.TellerWindowNumber.Text));
        }
    }
}