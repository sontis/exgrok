﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bell.Grok.Business;

namespace Bell.Grok.Web.Helpers.Validation
{
    /// <summary>
    /// Валидационный атрибут для логина при регистрации (с проверкой доступности данного логина)
    /// </summary> 
    public class RegistrationLoginAttribute : ValidationAttribute
    {
        /// <summary>
        /// Метод класса ValidationAttribute
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var validator = new AccountValidator();
            string error = validator.ValidateRegistrationLogin(value as string);

            return (error == null) ? ValidationResult.Success : new ValidationResult(error);
        }
    }
}