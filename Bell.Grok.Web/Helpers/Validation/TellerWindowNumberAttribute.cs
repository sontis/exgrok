﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Business;

namespace Bell.Grok.Web.Helpers.Validation
{
    /// <summary>
    /// Валидационный атрибут для номера кассового окна при его создании (с проверкой доступности данного номера)
    /// </summary>
    public class TellerWindowNumberAttribute : ValidationAttribute
    {
        /// <summary>
        /// Метод класса ValidationAttribute
        /// </summary>
        /// <param name="value"></param>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var validator = new TellerWindowValidator();
            var number = (int)value;
            //Если окно новое - то проверяем на уникальность номера кассы
            if ((bool)validationContext.ObjectInstance.GetType().GetProperty("IsNew").GetValue(validationContext.ObjectInstance))
            {
                var sectorId = (int)validationContext.ObjectInstance.GetType().GetProperty("SectorId").GetValue(validationContext.ObjectInstance);
                var stationId = (int)validationContext.ObjectInstance.GetType().GetProperty("StationId").GetValue(validationContext.ObjectInstance);
                string error = validator.ValidateNumber(number, sectorId, stationId);

                return (error == null) ? ValidationResult.Success : new ValidationResult(error);
            }
            return ValidationResult.Success;
        }

    }
}
