﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Web.Resources;

namespace Bell.Grok.Web.Helpers.Validation
{
    /// <summary>
    /// Валидационный атрибут для даты (с клиентской валидацией)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class DateAttribute : RegularExpressionAttribute, IClientValidatable
    {
        public DateAttribute()
            : base(@"^((((31\.(0?[13578]|1[02]))|((29|30)\.(0?[1,3-9]|1[0-2])))\" +
                   @".(1[6-9]|[2-9]\d)?\d{2})|(29\.0?2\.(((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))|(0?[1-9]|1\d|2[0-8])\" +
                   @".((0?[1-9])|(1[0-2]))\.((1[6-9]|[2-9]\d)?\d{4}))$")
        { }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("{0}: " + GROK.InvalidDateTime, name);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = GROK.InvalidDateTime,
                ValidationType = "regex"
            };
            rule.ValidationParameters.Add("pattern", Pattern);

            yield return rule;
        }
    }
}
