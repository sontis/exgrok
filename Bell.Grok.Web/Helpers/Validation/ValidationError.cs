﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bell.Grok.Web.Helpers.Validation
{
    /// <summary>
    /// Простой класс для описания ошибки валидации значения элемента модели
    /// </summary>
    public class ValidationError
    {
        /// <summary>
        /// Название невалидного элемента модели
        /// </summary>
        public string ElementName { get; set; }

        /// <summary>
        /// Причина невалидности элемента модели
        /// </summary>
        public string ErrorMessage { get; set; }
        
        public ValidationError(string elementName, string errorMessage)
        {
            ElementName = elementName;
            ErrorMessage = errorMessage;
        }
    }
}