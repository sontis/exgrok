﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bell.Grok.Web.Helpers
{
    /// <summary>
    /// Статусы для json-ответов    
    /// </summary>
    public enum JsonStatuses
    {
        Ok = 1,
        Invalid = 2,
        Error = 3,
        Unauthorized = 4
    }
}