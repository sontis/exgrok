﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Класс-наследник модели редактирования привязки терминала к кассовому окну.
    /// Необходим для использования атрибутов, находящихся в web-проекте
    /// </summary>
    public class TerminalToTellerWindowViewModel : TerminalToTellerWindowModel
    {
        [Required]
        [Date]
        [Display(Name = "Дата установки")]
        public override string BeginDate
        {
            get
            {
                return base.BeginDate;
            }
            set
            {
                base.BeginDate = value;
            }
        }

        [Date]
        [Display(Name = "Дата снятия")]
        public override string EndDate
        {
            get
            {
                return base.EndDate;
            }
            set
            {
                base.EndDate = value;
            }
        }

        /// <summary>
        /// Список номеров окон станции
        /// </summary>
        public List<SelectListItem> AllTellerWindowsNumbers { get; set; }        

        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// Здесь сосредоточена специфическая логика валидации модели
        /// Активно используется сервис TerminalInTellerWindowValidator из слоя бизнес-логики
        /// для исторической валидации интервалов привязок терминалов к кассовым окнам        
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            var beginDate = DateTime.Parse(BeginDate);
            var endDate = string.IsNullOrWhiteSpace(EndDate) ? DateTime.MaxValue : DateTime.Parse(EndDate);

            var validator = new TerminalInTellerWindowValidator();

            if (!validator.IsBeginDateLessEndDate(beginDate, endDate))
            {
                yield return new ValidationResult("Дата установки не может превышать дату снятия");
            }

            //Проверка на выход границ привязки терминала к окну за границы привязки терминала к станции
            var stationBindIntervalStatus = validator.ValidateTellerWindowTerminalBindIntervalWithStationBindInterval(beginDate, endDate,
                                                                                                              SectorStationTerminalId);
            if (!string.IsNullOrWhiteSpace(stationBindIntervalStatus))
            {
                yield return new ValidationResult(stationBindIntervalStatus);
            }

            //Проверка на выход границ привязки терминала к окну за границы действия окна
            var tellerWindowIntervalStatus =
                validator.ValidateTellerWindowTerminalBindIntervalWithTellerWindowPeriod(beginDate, endDate,
                    TellerWindow);

            if (!string.IsNullOrWhiteSpace(tellerWindowIntervalStatus))
            {
                yield return new ValidationResult(tellerWindowIntervalStatus);
            }

            //Проверка на недопущение одновременной установки одного терминала в разные окна
            var intersectStatus = validator.ValidateDateIntersectionsByTerminalId(beginDate, endDate, SectorStationTerminalId, TellerWindowTerminalId);

            if (!string.IsNullOrWhiteSpace(intersectStatus))
            {
                yield return new ValidationResult(intersectStatus);
            }
            
            //Проверка на уникальность привязки по набору параметров: кассовое окно, привязка терминала к станции, дата начала привязки
            var tellerWindowTerminalUniquenesStatus = validator.ValidateTellerWindowTerminalUniqueness(beginDate,
                                                                                                       SectorStationTerminalId,
                                                                                                       TellerWindow,
                                                                                                       TellerWindowTerminalId);
            if (!string.IsNullOrWhiteSpace(tellerWindowTerminalUniquenesStatus))
            {
                yield return new ValidationResult(tellerWindowTerminalUniquenesStatus);
            }

            var now = DateTime.Now;
            var currentDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            //Если срок действия привязки уходит в прошлое, то не валидируем совместимость 
            //профилей ККТ и КО
            if (endDate >= currentDate)
            {
                var profileCompatibilityStatus = validator.ValidateProfiles(TellerWindowTerminalId, TellerWindow);

                if (!string.IsNullOrWhiteSpace(profileCompatibilityStatus))
                {
                    yield return new ValidationResult(profileCompatibilityStatus);
                }
            }
            

            //Проверка на недопущение одновременной установки в одно окно нескольких терминалов
            /*var intersectByTellerWindowIdStatus = validator.ValidateDateIntersectionsByTellerWindowId(beginDate, endDate, TellerWindow, SectorStationTerminalId);
            if (!string.IsNullOrWhiteSpace(intersectByTellerWindowIdStatus))
            {
                yield return new ValidationResult(intersectByTellerWindowIdStatus);
            }*/
        }
    }
}
