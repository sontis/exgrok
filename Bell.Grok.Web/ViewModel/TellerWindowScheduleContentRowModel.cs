﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Модель строки таблицы содержимого раcписаний работы кассовых окон
    /// </summary>
    public class TellerWindowScheduleContentRowModel
    {
        public TellerWindowScheduleContentRowModel()
        {
            HourTypes = new List<HourTypeModel>();
            DoesWorkByDefault = new List<bool>();
            CanBreakHave = new List<bool>();
        }

        /// <summary>
        /// Идентификатор содержимого раcписания работы кассового окна
        /// </summary>
        public int TellerWindowScheduleContentModelId { get; set; }

        /// <summary>
        /// Идентификатор группы раcписаний работы кассовых окон
        /// </summary>
        public int TellerWindowScheduleId { get; set; }

        /// <summary>
        /// Идентификатор типа дней (рабочие или выходные)
        /// </summary>
        public int TellerWindowScheduleDayTypeId { get; set; }

        /// <summary>
        /// Номер кассового окна или название пункта продажи билетов (виртуальное кассовое окно) (выпадающий список в строке таблицы)
        /// </summary>
        public SelectListItem TellerWindowNumber { get; set; }

        /// <summary>
        /// Список типов часов для каждого часового интервала дня (была ли работа в этот час или нет)
        /// </summary>
        public List<HourTypeModel> HourTypes { get; set; }

        /// <summary>
        /// Копия HourTypes: для отслеживания изменений на клиенте
        /// </summary>
        public List<HourTypeModel> OldHourTypes
        {
            get
            {
                var result = new List<HourTypeModel>();
                HourTypes.ForEach(h => result.Add(new HourTypeModel()
                {
                    HourTypeId = h.HourTypeId,
                    HourTypeCode = h.HourTypeCode
                }));

                return result;
            }
        }

        /// <summary>
        /// Список флагов (для каждого часа): работает ли данное окно по данным ЭСАПРа
        /// </summary>
        public List<bool> DoesWorkByDefault { get; set; }

        /// <summary>
        /// Список флагов (для каждого часа): может ли быть перерыв в это час
        /// </summary>
        public List<bool> CanBreakHave { get; set; }

        /// <summary>
        /// Строка с перечислением периодов работы кассового окна
        /// </summary>
        public string WorkPeriods { get; set; }
    }
}