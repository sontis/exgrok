﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;

namespace Bell.Grok.Web.ViewModel
{
    public abstract class TerminalViewModel : TerminalModel
    {
        protected const string TimeFormat = "dd.MM.yyyy";

        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Date]
        [Display(Name = "Дата ввода")]
        public override string BeginTime
        {
            get
            {
                return base.BeginTime;
            }
            set
            {
                base.BeginTime = value;
            }
        }

        [Date]
        [Display(Name = "Дата вывода")]
        public override string EndTime
        {
            get
            {
                return base.EndTime;
            }
            set
            {
                base.EndTime = value;
            }
        }

        /// <summary>
        /// Все типы терминалов (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllTypes { get; set; }

        /// <summary>
        /// Все модели терминалов (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllMarks { get; set; }

        /// <summary>
        /// Все профили терминалов (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllProfiles { get; set; }

        /// <summary>
        /// Все статусы терминалов (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllStatuses { get; set; }

        /// <summary>
        /// Все назначения терминалов (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllPurposes { get; set; }

        /// <summary>
        /// Все агенты терминалов (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllAgents { get; set; }

        /// <summary>
        /// Все места расположения (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllPlacements { get; set; }

    }
}
