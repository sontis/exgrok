﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bell.Grok.Model;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Вью-модель узла направления в дереве
    /// </summary>
    public class DirectionTreeViewModel : DirectionTreeModel
    {
        /// <summary>
        /// Единый идентификатор узла в дереве
        /// </summary>
        public string UnifiedTreeId { get; set; }
        /// <summary>
        /// Url на страницу редактирования нормативной нагрузки
        /// </summary>
        public string ObjectUrl { get; set; }
    }
}