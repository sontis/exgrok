﻿using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Bell.Grok.Business;

namespace Bell.Grok.Web.ViewModel
{
    public class CashierViewModel : CashierModel, IValidatableObject
    {
        [Required]
        [Display(Name = "Ставка")]
        public override decimal? Rate { get; set; }

        [Required]
        [Date]
        [Display(Name = "Дата начала договора")]        
        public override string ContractEffectiveFrom
        {
            get
            {
                return base.ContractEffectiveFrom;
            }
            set
            {
                base.ContractEffectiveFrom = value;
            }
        }

        [Date]
        [Display(Name = "Дата окончания договора")]        
        public override string ContractEffectiveTo
        {
            get
            {
                return base.ContractEffectiveTo;
            }
            set
            {
                base.ContractEffectiveTo = value;
            }
        }

        /// <summary>
        /// Строка с датой из фильтра списка кассиров
        /// </summary>
        public string CurrentFilterDate { get; set; }

        /// <summary>
        /// Метод IValidatableObject
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {            
            var beginTime = DateTime.Parse(ContractEffectiveFrom);
            var endTime = string.IsNullOrWhiteSpace(ContractEffectiveTo) ? DateTime.MaxValue : DateTime.Parse(ContractEffectiveTo);

            if (beginTime > endTime)
            {
                yield return new ValidationResult("Дата начала договора не может быть больше даты окончания договора");
            }

            var employeeValidator = new EmployeeValidator();

            var vacationIntervalsIntervalsValidateStatus = employeeValidator.ValidateVacationIntervals(beginTime, endTime, EmployeeId);

            if (!string.IsNullOrWhiteSpace(vacationIntervalsIntervalsValidateStatus))
            {
                yield return new ValidationResult(vacationIntervalsIntervalsValidateStatus);
            }            
        }
    }
}