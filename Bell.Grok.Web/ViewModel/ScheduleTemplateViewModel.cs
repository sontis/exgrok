﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Класс-наследник модели редактирования шаблона графика кассиров.
    /// Необходим для использования атрибутов, находящихся в web-проекте
    /// </summary>
    public class ScheduleTemplateViewModel : ScheduleTemplateModel
    {
        [Display(Name = "Дата начала действия")]
        [Date]
        [Required(ErrorMessage = "Поле {0} является обязательным для заполнения")]
        public override string BeginDate
        {
            get
            {
                return base.BeginDate;
            }
            set
            {
                base.BeginDate = value;
            }
        }

        [Display(Name = "Дата окончания действия")]
        [Date]
        [Required(ErrorMessage = "Поле {0} является обязательным для заполнения")]
        public override string EndDate
        {
            get
            {
                return base.EndDate;
            }
            set
            {
                base.EndDate = value;
            }
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            var beginDate = DateTime.Parse(BeginDate);
            var endDate = DateTime.Parse(EndDate);

            if (beginDate > endDate)
            {
                yield return
                    new ValidationResult("Дата начала действия не может превышать дату окончания действия");
            }
        }

        /// <summary>
        /// Список всех идентификаторов направлений
        /// </summary>
        public List<SelectListItem> AllDirections { get; set; }

        /// <summary>
        /// Список всех идентификаторов участков
        /// </summary>
        public List<SelectListItem> AllSectors { get; set; }
    }
}
