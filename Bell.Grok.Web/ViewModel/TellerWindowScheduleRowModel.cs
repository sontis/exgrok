﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Модель строки таблицы срезов расписаний кассовых окон
    /// </summary>
    public class TellerWindowScheduleRowModel 
    {
        /// <summary>
        /// Идентификатор среза расписаний работы кассовых окон в хранилище ГРОК
        /// </summary>
        public int TellerWindowScheduleModelId { get; set; }

        /// <summary>
        /// Год, для сезона которого составлен срез расписаний (раскрывающийся список)
        /// </summary>
        public SelectListItem Year { get; set; }

        /// <summary>
        /// Идентификатор станции, для которой предназначен срез расписаний работы кассовых окон
        /// </summary>
        public int SectorStationId { get; set; }

        /// <summary>
        /// Сезон, для которого составлен срез расписаний
        /// </summary>
        public SeasonModel Season { get; set; }
    }
}