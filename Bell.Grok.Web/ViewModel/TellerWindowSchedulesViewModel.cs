﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Модель представления редактирования сезонных срезов работы кассовых окон
    /// </summary>
    public class TellerWindowSchedulesViewModel 
    {
        /// <summary>
        /// Год
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// Идентификатор сезона
        /// </summary>
        public SeasonModel Season { get; set; }

        /// <summary>
        /// Идентификатор станции
        /// </summary>
        public int SectorStationId { get; set; }        

        /// <summary>
        /// Url для возвращения на главную страницу паспорта станции
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// Список типов часов срезов расписаний работы кассовых окон
        /// </summary>
        public List<HourTypeModel> HourTypes { get; set; }

        /// <summary>
        /// Список типов дней срезов расписаний работы кассовых окон
        /// </summary>
        public List<TellerWindowScheduleDayTypeModel> TellerWindowScheduleDayTypes { get; set; }

        /// <summary>
        /// Список номеров кассовых окон станции
        /// </summary>
        public List<SelectListItem> TellerWindowNumbers { get; set; }
    }
}
