﻿using System.ComponentModel.DataAnnotations;
using Bell.Grok.Model;
using Bell.Grok.Web.Resources;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Класс-наследник модели представления формы авторизации.
    /// Необходим для использования атрибутов, находящихся в web-проекте
    /// </summary>
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceName = "Enter", ErrorMessageResourceType = typeof(GROK))]
        [StringLength(20, ErrorMessage = "Логин не может быть длиннее 20 символов")]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceName = "Enter", ErrorMessageResourceType = typeof(GROK))]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Длина пароля от 6 до 20 символов")]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        /// <summary>
        /// Признак, хранить ли куки между сеансами браузера
        /// </summary>
        [Display(Name = "Запомнить меня?")]
        public bool RememberMe { get; set; }

        /// <summary>
        /// Url для перенаправления в случае успешной аутентификации
        /// </summary>
        public string ReturnUrl { get; set; }

    }
}
