﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель представления редактирования нормативных стандартов станций выбранного объекта
    /// </summary>
    public class EditStandartViewModel
    {
        /// <summary>
        /// Идентификатор выбранного объекта
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Код выбранного объекта
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Все возможные значения фильтра по БПА
        /// </summary>
        public List<SelectListItem> AllBPAStandartNames { get; set; }

        /// <summary>
        /// Все возможные значения фильтра по МКТК
        /// </summary>
        public List<SelectListItem> AllMKTKStandartNames { get; set; }

        /// <summary>
        /// Фильтр по БПА
        /// </summary>
        public int FilterBPAStandart { get; set; }

        /// <summary>
        /// Фильтр по МКТК
        /// </summary>
        public int FilterMKTKStandart { get; set; }

        /// <summary>
        /// Разрешение на редактирование стандартов
        /// </summary>
        public bool CanEdit { get; set; }
    }
}
