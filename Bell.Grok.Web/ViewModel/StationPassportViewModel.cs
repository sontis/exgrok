﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;

namespace Bell.Grok.Web.ViewModel
{
    public class StationPassportViewModel : StationPassportModel
    {
        [Date]
        public override string TellerWindowBeginExploitationPeriod
        {
            get
            {
                return base.TellerWindowBeginExploitationPeriod;
            }
            set
            {
                base.TellerWindowBeginExploitationPeriod = value;
            }
        }

        [Date]
        public override string TellerWindowEndExploitationPeriod
        {
            get
            {
                return base.TellerWindowEndExploitationPeriod;
            }
            set
            {
                base.TellerWindowEndExploitationPeriod = value;
            }
        }

        [Date]
        public override string TerminalBeginExploitationPeriod
        {
            get
            {
                return base.TerminalBeginExploitationPeriod;
            }
            set
            {
                base.TerminalBeginExploitationPeriod = value;
            }
        }

        [Date]
        public override string TerminalEndExploitationPeriod
        {
            get
            {
                return base.TerminalEndExploitationPeriod;
            }
            set
            {
                base.TerminalEndExploitationPeriod = value;
            }
        }

        /// <summary>
        /// Месяц выбранного планового граффика
        /// </summary>
        public string Month { get; set; }

        /// <summary>
        /// Год выбранного планового граффика
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// Список сезонов срезов работы кассовых окон
        /// </summary>
        public List<SeasonModel> Seasons { get; set; }

        /// <summary>
        /// Список кассовых окон станции
        /// </summary>
        public List<SelectListItem> WindowTellerNumbers { get; set; }
    }
}
