﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Business;
using Bell.Grok.Model;

namespace Bell.Grok.Web.ViewModel
{
    public class AddSchedulesViewModel : AddSchedulesModel
    {
        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// Здесь сосредоточена специфическая логика валидации модели
        /// Активно используется сервис ScheduleValidator из слоя бизнес-логики
        /// для соблюдения требований по уникальности     
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            var scheduleValidator = new ScheduleValidator();

            var templateUniquenessForEmployeeStatus = scheduleValidator.CheckTemplateUniquenessForEmployee(EmployeeId,                                                                                                           
                                                                                                           Year,
                                                                                                           Month);

            if (!string.IsNullOrWhiteSpace(templateUniquenessForEmployeeStatus))
            {
                yield return new ValidationResult(templateUniquenessForEmployeeStatus);
            }
        }

        /// <summary>
        /// Все доступные Employee
        /// </summary>
        public List<SelectListItem> AllEmployees { get; set; }

        /// <summary>
        /// Все доступные шаблоны рассписаний
        /// </summary>
        public List<SelectListItem> AllScheduleTemlates { get; set; }

        /// <summary>
        /// Все кассовые окна
        /// </summary>
        public List<SelectListItem> AllTellerWindows { get; set; }

        /// <summary>
        /// Название месяца, на который составляется плановый график
        /// </summary>
        public string MonthName
        {
            get
            {
                return ((Months) Month).ToText().ToLower();
            }
        }
    }
}