﻿
namespace Bell.Grok.Model
{
    /// <summary>
    /// Модель меню администратора и сотрудника ОТиЗП
    /// </summary>
    public class MenuModel
    {
        /// <summary>
        /// Текст пункта меню
        /// </summary>
        public string ItemText { get; set; }
        /// <summary>
        /// Url страницы, на которую ссылается пункт меню
        /// </summary>
        public string ItemUrl { get; set; }
    }    
}