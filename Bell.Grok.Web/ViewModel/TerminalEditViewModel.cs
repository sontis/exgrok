﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Business;
using Bell.Grok.Web.Helpers.Validation;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Класс-наследник модели редактирования терминала.
    /// Необходим для использования атрибутов, находящихся в web-проекте
    /// </summary>
    public class TerminalEditViewModel : TerminalViewModel
    {                
        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// Здесь сосредоточена специфическая логика валидации модели
        /// Активно используется сервис TerminalValidator из слоя бизнес-логики
        /// для исторической валидации интервалов привязок терминалов к станциям
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override IEnumerable<ValidationResult> Validate(ValidationContext context)
        {            
            var beginTime = DateTime.Parse(BeginTime);
            var endTime = string.IsNullOrWhiteSpace(EndTime) ? DateTime.MaxValue : DateTime.Parse(EndTime);

            var terminalValidator = new TerminalValidator();

            if (!terminalValidator.IsBeginDateLessEndDate(beginTime, endTime))
            {
                yield return new ValidationResult("Дата ввода не может превышать дату вывода");
            }

            var historyValidationStatus = terminalValidator.ValidateNumber(Number, beginTime, endTime, TimeFormat, SectorStationTerminalId);

            if (!string.IsNullOrWhiteSpace(historyValidationStatus))
            {
                yield return new ValidationResult(historyValidationStatus);
            }

            var tellerWindowBindIntervalsValidateStatus = terminalValidator.ValidateTellerWindowBindIntervals(
                beginTime, endTime, SectorStationTerminalId, TimeFormat);

            if (!string.IsNullOrWhiteSpace(tellerWindowBindIntervalsValidateStatus))
            {
                yield return new ValidationResult(tellerWindowBindIntervalsValidateStatus);
            }

            var isBindToStationClosed = !string.IsNullOrWhiteSpace(EndTime);
            var unfinishedTellerWindowTerminalsValidateStatus =
                terminalValidator.ValidateUnfinishedTellerWindowTerminalIntervals(isBindToStationClosed,
                                                                                  SectorStationTerminalId,
                                                                                  endTime, TimeFormat);

            if (!string.IsNullOrWhiteSpace(unfinishedTellerWindowTerminalsValidateStatus))
            {
                yield return new ValidationResult(unfinishedTellerWindowTerminalsValidateStatus);
            }
        }
    }
}
