﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// ViewModel для отображения фильтра в навигационном дереве
    /// </summary>
    public class NavigationTreeModel
    {
        /// <summary>
        /// Все возможные значения фильтра
        /// </summary>
        public List<SelectListItem> ScheduleStatuses;

        /// <summary>
        /// Выбрнные значения фильтра
        /// </summary>
        public List<string> ScheduleStatus;

        /// <summary>
        /// Доступность фильтра
        /// </summary>
        public bool FilterEnabled { get; set; }
    }
}
