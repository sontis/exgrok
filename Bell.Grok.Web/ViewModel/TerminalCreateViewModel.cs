﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Bell.Grok.Business;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Класс-наследник модели создания терминала.
    /// Необходим для использования атрибутов, находящихся в web-проекте
    /// </summary>
    public class TerminalCreateViewModel : TerminalViewModel
    {
        [Required(ErrorMessage = "Поле является обязательным для заполнения")]        
        [Display(Name = "№")]
        public override string Number
        {
            get
            {
                return base.Number;
            }
            set
            {
                base.Number = value;
            }
        }        

        /// <summary>
        /// Метод интерфейса IValidatableObject
        /// Здесь сосредоточена специфическая логика валидации модели
        /// Активно используется сервис TerminalValidator из слоя бизнес-логики
        /// для исторической валидации интервалов привязок терминалов к станциям
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            var beginDate = DateTime.Parse(BeginTime);
            var endDate = string.IsNullOrWhiteSpace(EndTime) ? DateTime.MaxValue : DateTime.Parse(EndTime);

            var terminalValidator = new TerminalValidator();
            if (!terminalValidator.IsBeginDateLessEndDate(beginDate, endDate))
            {
                yield return new ValidationResult("Дата ввода не может превышать дату вывода");
            }

            var terminalRepository = new TerminalRepository();

            if (terminalRepository.TerminalWithNumberExists(Number))
            {
                var historyValidationStatus = terminalValidator.ValidateNumber(Number, beginDate, endDate, TimeFormat);

                if (!string.IsNullOrWhiteSpace(historyValidationStatus))
                {
                    yield return new ValidationResult(historyValidationStatus);
                }                
            }              
        }
    }
}
