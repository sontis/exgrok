﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Bell.Grok.Business;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers.Validation;

namespace Bell.Grok.Web.ViewModel
{
    /// <summary>
    /// Класс-наследник модели редактирования кассового окна.
    /// Необходим для использования атрибутов, находящихся в web-проекте
    /// </summary>
    public class TellerWindowEditViewModel : TellerWindowEditModel
    {
        public bool IsNew { get; set; }

        protected const string TimeFormat = "dd.MM.yyyy";

        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [TellerWindowNumber]
        [Remote("TellerWindowNumber", "Validation", HttpMethod = "POST", AdditionalFields = "SectorId,StationId,IsNew")]
        [Display(Name = "№ окна")]
        public override int Number
        {
            get
            {
                return base.Number;
            }
            set
            {
                base.Number = value;
            }
        }

        [Required(ErrorMessage = "Поле является обязательным для заполнения")]
        [Date]
        [Display(Name = "Дата установки статуса")]
        public override string BeginTime
        {
            get
            {
                return base.BeginTime;
            }
            set
            {
                base.BeginTime = value;
            }
        }

        [Date]
        [Display(Name = "Дата отмены статуса")]
        public override string EndTime
        {
            get
            {
                return base.EndTime;
            }
            set
            {
                base.EndTime = value;
            }
        }

        //поле для передачи данных режимов работы кассового окна
        public string OperationPeriodsData { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            var beginTime = DateTime.Parse(BeginTime);
            var endTime = string.IsNullOrWhiteSpace(EndTime) ? DateTime.MaxValue : DateTime.Parse(EndTime);
            
            if (beginTime > endTime)
            {
                yield return
                    new ValidationResult("Дата установки статуса не может превышать дату окончания статуса");
            }

            if (!IsNew)
            {
                var tellerWinowValidator = new TellerWindowValidator();

                var terminalBindIntervalsValidateStatus = tellerWinowValidator.ValidateTerminalBindIntervals(
                    beginTime, endTime, TellerWindowId, TimeFormat);

                if (!string.IsNullOrWhiteSpace(terminalBindIntervalsValidateStatus))
                {
                    yield return new ValidationResult(terminalBindIntervalsValidateStatus);
                }

                var isWindowClosed = !string.IsNullOrWhiteSpace(EndTime);
                var unfinishedTellerWindowTerminalsValidateStatus =
                    tellerWinowValidator.ValidateUnfinishedTellerWindowTerminalIntervals(isWindowClosed,
                        TellerWindowId,
                        endTime, TimeFormat);

                if (!string.IsNullOrWhiteSpace(unfinishedTellerWindowTerminalsValidateStatus))
                {
                    yield return new ValidationResult(unfinishedTellerWindowTerminalsValidateStatus);
                }
            }
        }

        /// <summary>
        /// Все специализации кассовых окон (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllSpecializations { get; set; }

        /// <summary>
        /// Все места расположения (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllPlacementNames { get; set; }

        /// <summary>
        /// Все профили (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllProfiles { get; set; } 

        /// <summary>
        /// Все статусы кассовых окон (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllStatuses { get; set; }

        /// <summary>
        /// Все способы оплаты (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllPaymentMethods { get; set; }

        /// <summary>
        /// Все способы оплаты (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllTellerWindowShiftnesses { get; set; }

        /// <summary>
        /// Все способы оплаты (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllTellerWindowBreakes { get; set; }

        /// <summary>
        /// Все дни недели (для выпадающего списка)
        /// </summary>
        public List<SelectListItem> AllWeekDays { get; set; }

        /// <summary>
        /// Строка со списком терминалов, установленных в данном окне
        /// в текущий момент или в будущем
        /// </summary>
        public string SetTerminalsListString { get; set; }

        /// <summary>
        /// Копия значения профиля - необходима для отслеживания изменения профиля
        /// </summary>
        public int? OldProfile { get; set; }        
    }
}
