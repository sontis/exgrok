﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bell.Grok.Business;
using Bell.Grok.Model;

namespace Bell.Grok.Web.Services
{
    /// <summary>
    /// Класс-сервис, проверяющий наличие прав пользователя на выполнение определённой операции.
    /// Явлется надстройкой над RoleObjectRepository для оптимизации обращения к базе.
    /// Данные считываеются единожды и помещаются в кэш.
    /// </summary>
    public static class GrokRoleObjectsService
    {
        private static readonly List<RoleObjectModel> RoleObjects;

        /// <summary>
        /// Статический конструктор. Вызывается один раз при первом обращении к методу
        /// CanUserMakeAction
        /// </summary>
        static GrokRoleObjectsService()
        {
            var roleObjectRepository = new RoleObjectRepository();
            RoleObjects = roleObjectRepository.GetAllRoleObjects();
        }        

        /// <summary>
        /// Метод, проверяющий наличие прав пользователя на выполнение определённой операции
        /// </summary>
        /// <param name="action">Идентификатор действия</param>
        /// <param name="userName">Логин текущего пользователя</param>
        /// <param name="dataObject">Идентификатор информационного объекта</param>
        /// <returns></returns>
        public static bool CanUserMakeAction(UserActions action, string userName, DataObjects dataObject)
        {
            var userRoleRepository = new UserRoleRepository();
            var roleId = userRoleRepository.GetRoleByLogin(userName);

            if (roleId.HasValue && RoleObjects != null)
            {
                return RoleObjects.Any(r => r.Action == action &&
                                            r.DataObject == dataObject &&
                                            r.RoleId == roleId);
            }
            else
            {
                return false;
            }
        }
    }
}