﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;

namespace Bell.Grok.Web.Services
{
    public static class SessingsService
    {
        public static int GetIdleWaitTime()
        {
            return (int) FormsAuthentication.Timeout.TotalSeconds;
        }
    }
}