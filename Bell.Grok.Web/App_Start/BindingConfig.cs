﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bell.Grok.Model;
using Bell.Grok.Web.Helpers;
using Bell.Grok.Web.ViewModel;

namespace Bell.Grok.Web
{
    public class BindingConfig
    {
        public static void RegisterModelBinders(ModelBinderDictionary binders)
        {
            binders.Add(typeof(EmployeeWeekWorkTimePriorityModel), new EmployeeWeekWorkTimePriorityModelBinder());   
            binders.Add(typeof(ScheduleTemplateViewModel), new ScheduleTemplateCreateViewModelBinder());
            binders.Add(typeof(ProceedsPlanGridModel), new ProceedsPlanUpdateModelBinder());
        }
    }
}