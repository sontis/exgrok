﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Bell.Grok.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapGrokRoute("Home", "", new { controller = "Home", action = "Index" });
            routes.MapGrokRoute("HomeGetDirections", "get-directions", new { controller = "Home", action = "ReadDirections" });
            routes.MapGrokRoute("HomeGetSectors", "get-sectors", new { controller = "Home", action = "ReadSectors" });
            routes.MapGrokRoute("HomeGetSectorStructure", "get-sector-structure", new { controller = "Home", action = "ReadSectorStructure" });
            routes.MapGrokRoute("HomeGetStations", "get-stations", new { controller = "Home", action = "ReadStationsAndCashiersCharacters" });
            routes.MapGrokRoute("HomeUpdateNodes", "update-nodes", new { controller = "Home", action = "UpdateExpandedNodes" });
            routes.MapGrokRoute("HomeAfterChangeDate", "after-change-date", new { controller = "Home", action = "IndexAfterDateChange" });
            routes.MapGrokRoute("HomeGetChashiers", "get-cashiers", new { controller = "Home", action = "ReadCashiers" });
            //routes.MapGrokRoute("GetActions", "Menu/GetActions", new { controller = "Menu", action = "GetActions" });

            routes.MapGrokRoute("Shared", "shared/{action}", new { controller = "Shared" });

            #region Account

            routes.MapGrokRoute("Login", "Login", new { controller = "Account", action = "Login" });
            routes.MapGrokRoute("Logout", "Logout", new { controller = "Account", action = "Logout" });

            #endregion

            #region Cashier

            routes.MapGrokRoute("DeleteEmployeeStationPriority", "Cashier/delete-employee-station-priority", new { controller = "Cashier", action = "DeleteStationPrioritet" });
            routes.MapGrokRoute("UpdateEmployeeStationPriority", "Cashier/update-employee-station-priority", new { controller = "Cashier", action = "UpdateStationPriority" });
            routes.MapGrokRoute("CreateEmployeeStationPriority", "Cashier/create-employee-station-priority", new { controller = "Cashier", action = "CreateStationPriority" });
            routes.MapGrokRoute("GetStationsForPriority", "Cashier/get-stations-for-priority", new { controller = "Cashier", action = "GetStationsForPriority" });
            routes.MapGrokRoute("CashierGetStationsOnSector", "Cashier/get-sector-stations", new { controller = "Cashier", action = "GetSectorStations" });
            routes.MapGrokRoute("CashierDefault", "Cashier/{id}", new { controller = "Cashier", action = "Index" });
            routes.MapGrokRoute("CashierActions", "Cashier/Vacations/{action}", new { controller = "Cashier" });
            routes.MapGrokRoute("CashierStationPriority", "Cashier/station-priority", new { controller = "Cashier", action = "GetStationPriorities" });
            routes.MapGrokRoute("CashierPickUpStationPriority", "Cashier/pick-up-station-priority", new { controller = "Cashier", action = "PickUpStationPriority" });
            routes.MapGrokRoute("CashierPickDownStationPriority", "Cashier/pick-down-station-priority", new { controller = "Cashier", action = "PickDownStationPriority" });
            routes.MapGrokRoute("CashierUpdateWorkTimePriorities", "Cashier/update-work-time-priorities", new { controller = "Cashier", action = "UpdatePriorityIntervals" });
            routes.MapGrokRoute("CashierTimeIntervalsForStationPriority", "Cashier/time-intervals-for-station-priority", new { controller = "Cashier", action = "GetTimeIntervalsForStationPriority" });
            routes.MapGrokRoute("CashierTimeIntervalsForWindow", "Cashier/time-intervals-for-window", new { controller = "Cashier", action = "GetTimeIntervalsForWindow" });
            routes.MapGrokRoute("CashierDeleteIntervalsForPriority", "Cashier/delete-intervals-for-priority", new { controller = "Cashier", action = "DeleteIntervalsForPriority" });
            routes.MapGrokRoute("CashierVacationValidate", "Cashier/validate-vacation", new { controller = "Cashier", action = "ValidateVacation" });            

            #endregion

            #region Station

            routes.MapGrokRoute("StationIndex", "Station/{SectorId}/{StationId}", new { controller = "Station", action = "Index" });
            routes.MapGrokRoute("StationCashBoxes", "Station/cash-boxes", new { controller = "Station", action = "ReadCashBoxes" });
            routes.MapGrokRoute("StationEditTellerWindow", "Station/edit-teller-window/{SectorId}/{StationId}", new { controller = "Station", action = "EditTellerWindow" });
            routes.MapGrokRoute("StationEditTellerWindowSuccess", "Station/edit-window-teller-success/{SectorId}/{StationId}", new { controller = "Station", action = "EditTellerWindowSuccess" });
            routes.MapGrokRoute("StationDeleteTellerWindow", "Station/delete-teller-window", new { controller = "Station", action = "DeleteTellerWindow" });
            routes.MapGrokRoute("StationTerminals", "Station/terminals", new { controller = "Station", action = "ReadTerminals" });
            routes.MapGrokRoute("StationCreateTerminal", "Station/create-terminal/{SectorId}/{StationId}", new { controller = "Station", action = "CreateTerminal" });
            routes.MapGrokRoute("StationTerminalTypeMarks", "Station/terminal-type-marks", new { controller = "Station", action = "GetTerminalTypeMarks" });
            routes.MapGrokRoute("StationCreateTerminalSuccess", "Station/create-terminal-success/{SectorId}/{StationId}", new { controller = "Station", action = "CreateTerminalSuccess" });
            routes.MapGrokRoute("StationEditTerminal", "Station/edit-terminal/{SectorId}/{StationId}", new { controller = "Station", action = "EditTerminal" });
            routes.MapGrokRoute("StationEditTerminalSuccess", "Station/edit-terminal-success/{SectorId}/{StationId}", new { controller = "Station", action = "EditTerminalSuccess" });            
            routes.MapGrokRoute("StationAddTerminalToTellerWindow", "Station/add-terminal-to-teller-window/{SectorId}/{StationId}", new { controller = "Station", action = "AddTerminalToTellerWindow" });
            routes.MapGrokRoute("StationTerminalInTellerWindow", "Station/terminal-in-teller-window", new { controller = "Station", action = "ReadTerminalInTellerWindow" });
            routes.MapGrokRoute("StationAddTerminalToTellerWindowSuccess", "Station/add-terminal-to-teller-window-success/{SectorId}/{StationId}", new { controller = "Station", action = "AddTerminalToTellerWindowSuccess" });
            routes.MapGrokRoute("StationEditTerminalToTellerWindow", "Station/edit-terminal-to-teller-window/{SectorId}/{StationId}", new { controller = "Station", action = "EditTerminalToTellerWindow" });
            routes.MapGrokRoute("StationEditTerminalToTellerWindowSuccess", "Station/edit-terminal-to-teller-window-success/{SectorId}/{StationId}", new { controller = "Station", action = "EditTerminalToTellerWindowSuccess" });
            routes.MapGrokRoute("StationDeleteTellerWindowTerminal", "Station/delete-teller-window-terminal", new { controller = "Station", action = "DeleteTellerWindowTerminal" });            
            routes.MapGrokRoute("SectorReadEmployeeShifts", "Station/read-employee-shifts", new { controller = "Station", action = "ReadEmployeeShifts" });
            routes.MapGrokRoute("SectorReadEmployeeShiftSchedule", "Station/read-employee-shift-schedule", new { controller = "Station", action = "ReadEmployeeShiftSchedule" });
            routes.MapGrokRoute("SectorSetEmployeeShiftSchedule", "Station/set-employee-shift-schedule", new { controller = "Station", action = "SetEmployeeShiftSchedule" });
            routes.MapGrokRoute("SectorCreateEmployeeShift", "Station/create-employee-shift", new { controller = "Station", action = "CreateEmployeeShift" });
            routes.MapGrokRoute("SectorDeleteEmployeeShift", "Station/delete-employee-shift", new { controller = "Station", action = "DeleteEmployeeShift" });
            routes.MapGrokRoute("SectorEditEmployeeShift", "Station/change-employee-shift-number", new { controller = "Station", action = "ChangeEmployeeShiftNumber" });
            routes.MapGrokRoute("TellerWindowSchedulesEdit", " Station/teller-window-schedules-edit", new { controller = "Station", action = "TellerWindowSchedulesEdit" });

            #endregion

            #region Menu

            routes.MapGrokRoute("MenuActions", "menu/actions", new { controller = "Menu", action = "GetActions" });

            #endregion

            #region Validation

            routes.MapGrokRoute("ValidateRegistrationLogin", "validation/registration-login", new { controller = "Validation", action = "RegistrationLogin" });
            routes.MapGrokRoute("ValidateTellerWindowNumber", "validation/teller-window-number", new { controller = "Validation", action = "TellerWindowNumber" });

            #endregion

            #region Terminal

            routes.MapGrokRoute("TerminalGetInfo", "Terminal/get-info", new { controller = "Terminal", action = "GetTerminalInfo" });

            #endregion

            #region ScheduleTemplate

            routes.MapGrokRoute("ScheduleTemplate", "schedule-template", new { controller = "ScheduleTemplate", action = "Index" });
            routes.MapGrokRoute("ScheduleTemplateCreate", "schedule-template-create", new { controller = "ScheduleTemplate", action = "Create" });
            routes.MapGrokRoute("ScheduleTemplatesEdit", "schedule-template-edit", new { controller = "ScheduleTemplate", action = "Edit" });
            routes.MapGrokRoute("ScheduleTemplatesRead", "schedule-templates-read", new { controller = "ScheduleTemplate", action = "ReadScheduleTemplates" });
            routes.MapGrokRoute("ScheduleTemplateActive", "schedule-template-active", new { controller = "ScheduleTemplate", action = "ActivateScheduleTemplate" });
            routes.MapGrokRoute("ScheduleTemplateDeactive", "schedule-template-deactive", new { controller = "ScheduleTemplate", action = "DeactivateScheduleTemplate" });
            routes.MapGrokRoute("ScheduleTemplateReview", "schedule-template-review", new { controller = "ScheduleTemplate", action = "Review" });
            routes.MapGrokRoute("ScheduleTemplateReviewDetailsRead", "schedule-template-review-detail-read", new { controller = "ScheduleTemplate", action = "ReadScheduleTemplateDetails" });
            routes.MapGrokRoute("ScheduleTemplateMonthReviewDetailsRead", "schedule-template-month-review-details-read", new { controller = "ScheduleTemplate", action = "ReadMonthScheduleTemplateDetails" });

            #endregion

            #region Schedule
            routes.MapGrokRoute("GetAllSchedules", "Schedule/getallschedules", new { controller = "Schedule", action = "GetAllSchedules" });
            routes.MapGrokRoute("GetAllTellerWindowSchedules", "Schedule/getalltellerwindowschedules", new { controller = "Schedule", action = "GetAllTellerWindowSchedules" });
            routes.MapGrokRoute("DeleteSchedule", "Schedule/deleteschedule", new { controller = "Schedule", action = "DeleteSchedule" });
            routes.MapGrokRoute("AddShedules", "addshedules", new { controller = "Schedule", action = "AddShedules" });
            routes.MapGrokRoute("AddSchedulesSuccess", "addschedulessuccess", new { controller = "Schedule", action = "AddSchedulesSuccess" });
            routes.MapGrokRoute("SendScheduleToReconcile", "Schedule/sendtoreconcile", new { controller = "Schedule", action = "SendScheduleToReconcile" });
            routes.MapGrokRoute("CancelReconcileSchedule", "Schedule/cancelreconcileschedule", new { controller = "Schedule", action = "CancelReconcileSchedule" });
            routes.MapGrokRoute("ExportFile", "Schedule/exportfile/{id}", new { controller = "Schedule", action = "Export", id = "" });
            routes.MapGrokRoute("ConfirmScheduleReconcileBySectorChief", "Schedule/confirmschedulereconcilebysectorchief", new { controller = "Schedule", action = "ConfirmReconcileScheduleBySectionChief" });
            routes.MapGrokRoute("ConfirmScheduleReconcileByProfessionalGroupOrginizer", "Schedule/Ccnfirmschedulereconcilebyprofessionalgrouporginizer", new { controller = "Schedule", action = "ConfirmReconcileByProfessionalGroupOrginizer" });
            #endregion

            #region WorkDepartmentUser

            routes.MapGrokRoute("WorkDepartmentUser", "work-department-user", new { controller = "WorkDepartmentUser", action = "Index" });

            #endregion

            #region ProceedsPlan

            routes.MapGrokRoute("ProceedsPlanRead", "proceeds-plan-read", new { controller = "ProceedsPlan", action = "ReadProceedsPlans" });
            routes.MapGrokRoute("ProceedsPlanUpdate", "proceeds-plan-update", new { controller = "ProceedsPlan", action = "UpdateProceedsPlan" });

            #endregion

            #region DirectionSector

            routes.MapGrokRoute("Index", "DirectionSector/index", new { controller = "DirectionSector", action = "Index" });
            routes.MapGrokRoute("ProceedsPlansReview", "DirectionSector/proceeds-plans-review", new { controller = "DirectionSector", action = "ProceedsPlansReview" });
            routes.MapGrokRoute("Timesheet", "DirectionSector/timesheet", new { controller = "DirectionSector", action = "Timesheet" });
            routes.MapGrokRoute("TimesheetsRead", "DirectionSector/timesheet-read", new { controller = "DirectionSector", action = "GetTimesheets" });
            routes.MapGrokRoute("IsTimesheetForMonth", "DirectionSector/is-timesheet-for-month", new { controller = "DirectionSector", action = "IsTimesheetForMonth" });
            routes.MapGrokRoute("FormTimesheet", "DirectionSector/form-timesheet", new { controller = "DirectionSector", action = "FormTimesheet" });
            routes.MapGrokRoute("UploadFile", "DirectionSector/upload-file", new { controller = "DirectionSector", action = "UploadFile" });
            routes.MapGrokRoute("LoadFile", "DirectionSector/load-file", new { controller = "DirectionSector", action = "LoadFile" });
            routes.MapGrokRoute("RemoveFile", "DirectionSector/remove-file", new { controller = "DirectionSector", action = "RemoveFile" });
            routes.MapGrokRoute("GetFiles", "DirectionSector/get-files", new { controller = "DirectionSector", action = "GetFiles" });
            routes.MapGrokRoute("GeneratePrepayments", "DirectionSector/generate-prepayments", new { controller = "DirectionSector", action = "GeneratePrepayments" });
            routes.MapGrokRoute("GenerateIndividualPrepayments", "DirectionSector/generate-individual-prepayments", new { controller = "DirectionSector", action = "GenerateIndividualPrepayments" });
            routes.MapGrokRoute("AddEmployeeToTimesheet", "DirectionSector/add-employee", new { controller = "DirectionSector", action = "AddEmployee" });            
            routes.MapGrokRoute("DeleteEmployeeFromTimesheet", "DirectionSector/delete-employee", new { controller = "DirectionSector", action = "DeleteEmployee" });
            routes.MapGrokRoute("ExportTimesheet", "DirectionSector/export-timesheet/{id}", new { controller = "DirectionSector", action = "Export", id = "" });
            routes.MapGrokRoute("TimesheetDetails", "DirectionSector/get-timesheet-details", new { controller = "DirectionSector", action = "GetTimesheetDetails" });
            routes.MapGrokRoute("SaveTimesheetDetails", "DirectionSector/save-timesheet-details", new { controller = "DirectionSector", action = "SaveTimesheetDetails" });
            routes.MapGrokRoute("TimesheetPayrollsOrHookies", "DirectionSector/get-timesheet-payrolls-or-hookies", new { controller = "DirectionSector", action = "GetTimesheetPayrollsOrHookies" });
            routes.MapGrokRoute("ClearSecondTimesheetparts", "DirectionSector/clear-second-timesheet-parts", new { controller = "DirectionSector", action = "ClearSecondTimesheetparts" });
            routes.MapGrokRoute("ClearIndividualSecondTimesheetpart", "DirectionSector/clear-individual-second-timesheetpart", new { controller = "DirectionSector", action = "ClearIndividualSecondTimesheetpart" });
            routes.MapGrokRoute("DSEditStandart", "DirectionSector/edit-standart", new { controller = "DirectionSector", action = "EditStandart" });
            routes.MapGrokRoute("DSGetStandartRows", "DirectionSector/get-standart-rows", new { controller = "DirectionSector", action = "GetStandartRows" });
            routes.MapGrokRoute("DSSetStandartRows", "DirectionSector/set-standart-rows", new { controller = "DirectionSector", action = "SetStandartRows" });

            #endregion

            #region TellerWindowSchedule

            routes.MapGrokRoute("ReadTellerWindowLSchedules", "teller-windows-schedules-read", new { controller = "TellerWindowSchedule", action = "ReadTellerWindowSchedules" });
            routes.MapGrokRoute("CreateTellerWindowSchedule", "teller-window-schedules-create", new { controller = "TellerWindowSchedule", action = "CreateTellerWindowSchedule" });
            routes.MapGrokRoute("ReadIndividualTellerWindowSchedules", "read-individual-teller-window-schedules", new { controller = "TellerWindowSchedule", action = "ReadIndividualTellerWindowSchedules" });
            routes.MapGrokRoute("UpdateIndividualTellerWindowSchedules", "update-individual-teller-window-schedules", new { controller = "TellerWindowSchedule", action = "UpdateIndividualTellerWindowSchedules" });

            #endregion
        }
    }
}