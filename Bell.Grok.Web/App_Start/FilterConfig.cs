﻿using System.Web;
using System.Web.Mvc;
using Bell.Grok.Web.Helpers;

namespace Bell.Grok.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ActionGrokAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}