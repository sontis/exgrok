﻿(function($){

    $.session = {        
        get: function(key)
        {
            return window.sessionStorage.getItem(key);
        },

        set: function(key, value)
        {
            try {
                window.sessionStorage.setItem(key, value);
            } catch (e) {}            
            return this;
        },
        
        'delete': function(key){
            return this.remove(key);
        },

        remove: function(key)
        {
            try {
                window.sessionStorage.removeItem(key);
            } catch (e) {};            
            return this;
        },

        _clearSession: function()
        {
            try {
                window.sessionStorage.clear();
            } catch (e) {
                for (var i in window.sessionStorage) {
                    window.sessionStorage.removeItem(i);
                }
            }
        },

        clear: function()
        {
            this._clearSession();            
            return this;
        }

    };    

})(jQuery);
