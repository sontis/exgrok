﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Bell.Grok.Business;
using Bell.Grok.Business.Logging;
using Bell.Grok.Services.Exceptions;
using Bell.Grok.Web.Helpers.AutoMapper;
using Bell.Grok.Web.Services;

namespace Bell.Grok.Web
{    
    public class MvcApplication : System.Web.HttpApplication
    {
        private const string StationObjectName = "Station";
        private const string CashierObjectName = "Employee";

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BindingConfig.RegisterModelBinders(ModelBinders.Binders);

            TerminalHistoryService.Init();            

            AutoMapperWebConfiguration.Configure();
        }

        protected void Application_End()
        {
            
        }

        void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();

            #region Logging

            if (exception.GetType() != typeof (RepositoryException))
            {
                AppExceptionManager.HandleException(exception);
            }

            #endregion
        }

        public override string GetVaryByCustomString(HttpContext context, string arg)
        {
            var objectVersionRepository = new ObjectVersionRepository();            

            var cookie = context.Request.Cookies[".GROKCOOKIE"];
            var cookieValue = cookie == null
                               ? string.Empty
                               : cookie.Value;                      

            switch (arg.ToLower())
            {
                case "sessionid":
                    return cookieValue;
                case "sessionid;cashierversion":
                    return cookieValue +
                           objectVersionRepository.GetObjectVersion(CashierObjectName);
                case "sessionid;stationversion":
                    return cookieValue +                   
                           objectVersionRepository.GetObjectVersion(StationObjectName);
                case "sessionid;cashierversion;stationversion":
                    return cookieValue +
                           objectVersionRepository.GetObjectVersion(CashierObjectName) +
                           objectVersionRepository.GetObjectVersion(StationObjectName);
                default:
                    return string.Empty;
            }            
        }
    }
}